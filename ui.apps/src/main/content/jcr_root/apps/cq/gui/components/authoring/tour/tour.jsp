<%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="java.util.Iterator,
                  org.apache.sling.api.resource.ResourceUtil,
                  org.apache.sling.api.resource.ValueMap,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ComponentHelper,
                  com.adobe.granite.ui.components.Tag,
                  com.adobe.granite.ui.components.Value,
                  com.day.cq.security.Authorizable"%>
    <%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils,
    com.unilever.platform.aem.foundation.configuration.ConfigurationService,
	com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility,
	com.day.cq.wcm.api.PageManager,
	com.day.cq.wcm.api.Page,
	org.apache.commons.lang.StringUtils" %><%

    // Tour is shown based on (1) "authoringShowTour" user preference
    // and we are using form POST mechanism to save the user preference

    String dataPath = StringUtils.substringAfter(request.getHeader("referer"), "editor.html");
	dataPath = StringUtils.substringBefore(dataPath,".html");
	String d2Documentation = StringUtils.EMPTY;
	String d2DocumentationTitle = StringUtils.EMPTY;
	String d2DocumentationSubTitle = StringUtils.EMPTY;
	if(dataPath==null){
    dataPath=StringUtils.EMPTY;
	}
if(StringUtils.isNotBlank(dataPath)){
	com.unilever.platform.aem.foundation.configuration.ConfigurationService configurationService = sling.getService(com.unilever.platform.aem.foundation.configuration.ConfigurationService.class);


	PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
	Page currentPage = pageManager.getContainingPage(dataPath);
	d2Documentation= i18n.getVar(String.valueOf(GlobalConfigurationUtility.getValueFromConfiguration(configurationService,currentPage,"d2DocumentationHomeUrl","d2Documentation")));
    d2DocumentationTitle= i18n.getVar(String.valueOf(GlobalConfigurationUtility.getValueFromConfiguration(configurationService,currentPage,"d2DocumentationHomeUrl","title")));
    d2DocumentationSubTitle= i18n.getVar(String.valueOf(GlobalConfigurationUtility.getValueFromConfiguration(configurationService,currentPage,"d2DocumentationHomeUrl","subtitle")));
}
    Authorizable user = resourceResolver.adaptTo(Authorizable.class);
    ValueMap userPrefVm = ResourceUtil.getValueMap(resourceResolver.getResource(user.getHomePath() + "/preferences"));
    String userPrefPath = request.getContextPath() + user.getHomePath() + ".userproperties.html?path=preferences";

    String userPrefName = "cq.authoring.editor.showTour";
    boolean showTourPref = userPrefVm.get(userPrefName, true);

    // Tour is shown based on (2) the GET request sent by the client
    // (if user explicitly clicks the "Help" link, then the tour should be rendered)
    String forceTourLoading = request.getParameter("forceLoading");
    if (forceTourLoading == null) {
        forceTourLoading = "false";
    }

    boolean renderTour = showTourPref || forceTourLoading.equals("true");

    if (renderTour) {

    String skipLabel = i18n.get("Skip");
    String backLabel = i18n.get("Back");
    String nextLabel = i18n.get("Next");
    String doneLabel = i18n.get("Done!");
    String remindMeLaterLabel = i18n.get("Remind me later");
    String dontShowAgainLabel = i18n.get("Don't show again");

    int stepNb = 0;

%><div class="cq-Tour coral-Modal"><%
    %><form class="cq-Tour-form" action="<%= userPrefPath %>" method="POST"><%
    %><div class="cq-Tour-content coral-Modal-body"><%

    for (Iterator<Resource> items = cmp.getItemDataSource().iterator(); items.hasNext();) {
        Resource step = items.next();

        Config cfg = new Config(step);

        String id = cfg.get("id", String.class);
        String additionnalClass = cfg.get("class", String.class);
        String title = cfg.get("jcr:title", String.class);
        String desc = cfg.get("jcr:description", String.class);
        String[] imageData = cfg.get("imageData", null);

        AttrBuilder attrs = new AttrBuilder(request, xssAPI);
        attrs.add("id",id);
        attrs.addClass("cq-Tour-step");
        attrs.addClass(additionnalClass);

        AttrBuilder notesAttrs = new AttrBuilder(request, xssAPI);
        notesAttrs.addClass("cq-Tour-stepNotes");
        if (imageData == null) {
            notesAttrs.addClass("cq-Tour-stepNotes--fullHeight");
        }

        %><div <%= attrs.build() %>><%

        %><section <%= notesAttrs.build() %>><%
            %><h1 class="cq-Tour-stepTitle"> <%= xssAPI.filterHTML(i18n.getVar(title)) %> </h1><%
            %><p class="cq-Tour-stepDescription">

<%if(StringUtils.isNotBlank(d2Documentation) && title.equals("LEARN MORE?")){%>
 <span class="cq-Tour-subTitle u-bold"><%=d2DocumentationTitle%></span>
      <br>
      <%=d2DocumentationSubTitle%><br>
<a onclick="window.open('<%=d2Documentation%>')" href="#" class="coral-Link"><%=i18n.get("documentation.learnMore")%></a>
      <br>
      <br>
<%}%>

<%= xssAPI.filterHTML(i18n.getVar(desc)) %></p><%
        %></section><%

        if (imageData != null) {
            if (imageData.length > 1) {
                %><div class="cq-Tour-stepImages"><%
                for (String image : imageData) {
                    %><img class="cq-Tour-stepImage"
                           src="data:image/png;base64,<%=outVar(xssAPI, i18n, image)%>"/><%
                }
                %></div><%
            } else {
                %><div class="cq-Tour-stepImages"><%
                   %><img class="cq-Tour-stepImage" src="data:image/png;base64,<%=outVar(xssAPI, i18n, imageData[0])%>"/><%
                %></div><%
            }
        }

        %></div><%
            if(!desc.isEmpty()){
            ++stepNb;
        }
        }

        %><nav class="cq-Tour-nav"><%

            %><button class="cq-Tour-skipTourButton coral-Button coral-Button--quiet u-coral-pullLeft"
                      type="button"
                      title="<%= skipLabel %>"
                      data-target="#skipPopover"
                      data-toggle="popover"
                      data-point-from="top">
                <%= skipLabel %>
            </button><%

            %><div id="skipPopover" class="coral-Popover coral--light">
                    <div class="coral-Popover-content">
                    <div class="endor-List">
                        <a class="endor-List-item endor-List-item--interactive coral-Link cq-Tour-remindMeLaterButton"><%= remindMeLaterLabel %></a>
                        <a class="endor-List-item endor-List-item--interactive coral-Link cq-Tour-dontShowAgainButton"><%= dontShowAgainLabel %></a>
                    </div>
                </div>
            </div><%

            %><button class="cq-Tour-previousStepButton coral-Button coral-Button--quiet u-coral-pullLeft"
                      type="button"
                      title="<%= backLabel %>">
                <%= backLabel %>
            </button><%

            %><button class="cq-Tour-nextStepButton coral-Button coral-Button--primary u-coral-pullRight"
                      type="button"
                      title="<%= nextLabel %>">
                <%= nextLabel %>
            </button><%

            %><button class="cq-Tour-endTourButton coral-Button coral-Button--primary u-coral-pullRight"
                      type="submit"
                      title="<%= doneLabel %>">
                <%= doneLabel %>
            </button><%

            %><div class="cq-Tour-stepAnchors"><%
            for (int i = 0; i < stepNb; i++) {
                %><a class="cq-Tour-stepAnchor" data-step-index="<%= i %>" href="#"></a><%
            }
            %></div><%

        %></nav><%

    %></div><%
    %><input type="hidden" name="<%= xssAPI.encodeForHTMLAttr(userPrefName) %>" value="false"/><%
    %></form><%
%></div><%
    }
%>