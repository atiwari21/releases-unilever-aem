<%--
    ADOBE CONFIDENTIAL

    Copyright 2013 Adobe Systems Incorporated
    All Rights Reserved.

    NOTICE:  All information contained herein is, and remains
    the property of Adobe Systems Incorporated and its suppliers,
    if any.  The intellectual and technical concepts contained
    herein are proprietary to Adobe Systems Incorporated and its
    suppliers and may be covered by U.S. and Foreign Patents,
    patents in process, and are protected by trade secret or copyright law.
    Dissemination of this information or reproduction of this material
    is strictly forbidden unless prior written permission is obtained
    from Adobe Systems Incorporated.


    The script to render a single comment event.

--%><%@page session="false" contentType="text/html; charset=utf-8"%><%
%><%@page import="com.adobe.granite.comments.Comment,
                  com.adobe.granite.security.user.UserProperties,
                  com.adobe.granite.security.user.UserPropertiesManager,
                  com.adobe.granite.security.user.UserPropertiesService,
                  com.day.cq.commons.date.RelativeTimeFormat,
                  com.day.cq.i18n.I18n,
                  org.apache.sling.api.resource.Resource,
                  javax.jcr.RepositoryException,
                  javax.jcr.Session,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.security.Privilege,
                  java.util.Locale,
                  java.util.ResourceBundle,
                  org.apache.commons.lang.StringUtils" %><%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0" %><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%
%><%@taglib prefix="ui" uri="http://www.adobe.com/taglibs/granite/ui/1.0" %><%
%><cq:defineObjects /><%
%><ui:includeClientLib categories="cq.projects.admin.tasks.taskcomment"/><%

    UserPropertiesManager userPropertiesManager = resource.adaptTo(UserPropertiesManager.class);
    I18n i18n = new I18n(slingRequest);
    Locale locale = slingRequest.getLocale();

    Comment comment = resource.adaptTo(Comment.class);

    String creator = comment.getAuthorName();
    String annotationData = comment.getAnnotationData();

    String taskAction = null;
    boolean taskCompleted = false;
    final String TASKACTIONID = "taskAction:";
    if (StringUtils.isNotBlank(annotationData) && annotationData.startsWith(TASKACTIONID)) {
        taskCompleted = true;
        taskAction = annotationData.substring(TASKACTIONID.length());
    }

    ResourceBundle resourceBundle = slingRequest.getResourceBundle(locale);
    RelativeTimeFormat tf = new RelativeTimeFormat("r", resourceBundle);
    String dateText = comment.getCreated() != null ? tf.format(comment.getCreated().getTimeInMillis(), true) : "<unknown>";

    String description = comment.getMessage();

    Resource photo = null;
    UserProperties profile = userPropertiesManager.getUserProperties(creator, UserPropertiesService.PROFILE_PATH);

    if (profile != null) {
        photo = profile.getResource(UserProperties.PHOTOS);
    }

    boolean canDelete = false;
    try {
        AccessControlManager acm = resourceResolver.adaptTo(Session.class).getAccessControlManager();
        if (acm != null) {
            Privilege p = acm.privilegeFromName(Privilege.JCR_REMOVE_NODE);
            canDelete = acm.hasPrivileges(resource.getPath(), new Privilege[]{p});
        }
    } catch (RepositoryException e) {
        log.error("Unable to get access manager", e);
    }

    String userName = profile != null? profile.getDisplayName(): creator;

    // support multi-line text.
    String xssDescription = "";
    if (description != null) {
        String[] splitDescription = description.split("\n");
        boolean first = true;

        StringBuffer result = new StringBuffer();
        for(String snippet : splitDescription) {
            if (!first) {
                result.append("<br/>");
            }
            first = false;
            result.append(xssAPI.filterHTML(snippet));

        }
        xssDescription = result.toString();
    }

    String thumbnailPath = "/libs/granite/security/clientlib/themes/default/resources/sample-user-thumbnail.36.png";
    if (photo != null) {
        thumbnailPath = photo.getPath() + "/primary/image.prof.thumbnail.36.png";
    }

    String commentLabel = null;
    if (taskCompleted) {
        if (StringUtils.isBlank(taskAction)) {
            commentLabel = xssAPI.filterHTML(i18n.get("Task completed {0} by {1}", "example: Task completed {5 days ago} by {Alison Parker}", dateText, userName));
        } else {
            commentLabel = xssAPI.filterHTML(i18n.get("Task completed {0} by {1} with action ''{2}''", "example: Task completed {5 days ago} by {Alison Parker} with action 'Approve'", dateText, userName, taskAction));
        }
    } else {
        commentLabel = xssAPI.filterHTML(i18n.get("{0} by {1}", "example: {5 days ago} by {Alison Parker}", dateText, userName));
    }


%><section class="task-comment">
    <div class="task-comment-icon">
        <img src="<%= thumbnailPath %>" alt="">
    </div>
    <div class="smallText task-comment-text">
        <div><%= commentLabel %>
            <% if (canDelete) { %>
            <span class="task-comment-delete coral-Link" data-target="<%= comment.getPath()%>"><a><span><%=i18n.get("Delete")%></span></a></span>
            <% } %>
        </div>
    </div>
    <div class="task-comment-balloon"><%= xssDescription %></div>
</section>