/*
 ADOBE CONFIDENTIAL

 Copyright 2012 Adobe Systems Incorporated
 All Rights Reserved.

 NOTICE:  All information contained herein is, and remains
 the property of Adobe Systems Incorporated and its suppliers,
 if any.  The intellectual and technical concepts contained
 herein are proprietary to Adobe Systems Incorporated and its
 suppliers and may be covered by U.S. and Foreign Patents,
 patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material
 is strictly forbidden unless prior written permission is obtained
 from Adobe Systems Incorporated.
 */
(function($, undefined) {

    function setThumbnail(name, src, hasChanged) {
        var hasImage = name || src;
        var name = name || "";
        var date = new Date();
        if ((src !== undefined) && !hasChanged) {
            src += "?ck=" + date.getTime();
        }

        var newImg = $("<img class='image'>").attr({src:src, title: name});
        $(".foundation-layout-thumbnail-image img").replaceWith(newImg);

        var $thumbnail = $(".fileupload input[type='file']");

        if (!hasImage) {
            $thumbnail.val("");
        }

        // If the user has not changed the file, it will not be uploaded nor the existing file removed on the server
        if (hasChanged) {
            $thumbnail.addClass("file-changed");
        }

        $(".coral-FileUpload-trigger span").text(hasImage ? Granite.I18n.get("Change Thumbnail") : Granite.I18n.get("Add Thumbnail"));

    }

    $(document).on("foundation-contentloaded", function(e) {
        // Thumbnail upload
        $(".coral-FileUpload input[type='file']").change(function (e) {
            var fileName = CQ.projects.utils.getFileName(e.target);
            if (fileName) {
                if (fileName.match(/\.(jpg|jpeg|png|gif)/i)) {
                    if (e.target.files && window.FileReader) {
                        var file = e.target.files[0];
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            setThumbnail(fileName, e.target.result, true);
                        }
                        reader.readAsDataURL(file);
                    } else {
                        setThumbnail(fileName, null, true);
                    }
                }
            }
        });

        // update the image if there has been project data provided
        var path = $(".foundation-layout-thumbnail").data("projectcoverpath");
        if (path) {
            path += "?ck=" + new Date().getTime();
            var thumbnail = $(".foundation-layout-thumbnail .foundation-layout-thumbnail-image img");
            var newImg = $("<img class='image'>").attr({src:path});
            thumbnail.replaceWith(newImg);
        }


    });

})(Granite.$);
