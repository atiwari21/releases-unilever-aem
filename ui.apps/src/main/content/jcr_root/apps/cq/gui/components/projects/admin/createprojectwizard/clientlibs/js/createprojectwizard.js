/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2013 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */
(function(window, document, Granite, $) {
    "use strict";
	
    var ns = ".cq-projects-admin-createproject";
    var ui = $(window).adaptTo("foundation-ui");

    function submit(wizard) {

        var ajaxOptions = {
            type: wizard.prop("method"),
            url: wizard.prop("action"),
            contentType: wizard.prop("enctype")
        };

        ui.wait();

        var jqxhr = CQ.projects.ajaxSubmitFileForm(ajaxOptions, wizard);

        jqxhr.done(function(html) {
            ui.clearWait();

            var $html = $(html);

            var modal = CQ.projects.modal.successTemplate.clone();
            modal.find(".coral-Modal-header h2").html($html.find(".foundation-form-response-title").next().html());
            modal.find(".coral-Modal-body").html($html.find(".foundation-form-response-description").next().html());

            var footer = modal.find(".coral-Modal-footer");

            var redirect = $html.find("a.foundation-form-response-redirect");
            var edit = $html.find("a.cq-projects-admin-createproject-edit");

            $('<a class="coral-Button" data-dismiss="modal"></a>')
                .prop("href", redirect.prop("href"))
                .text(redirect.text())
                .appendTo(footer);



            modal.appendTo("body").modal("show");
        }).fail(function(xhr, error, errorThrown) {
            ui.clearWait();

            if (error === "error") {
                var $html = $(xhr.responseText);
                ui.alert($html.find(".foundation-form-response-title").next().html() || Granite.I18n.get("Error"),
                         $html.find(".foundation-form-response-description").next().html(),
                         "error");
            } else {
                ui.alert(Granite.I18n.get("Error"), errorThrown, "error");
            }
        });
    }

    $(document).on("foundation-contentloaded" + ns, function(e) {
        var wizard = $(".cq-projects-admin-createproject", e.target);

        if (!wizard.length) return;

        wizard.off("submit" + ns)
              .on("submit" + ns, function(e) {
                  e.preventDefault();
                  submit(wizard);
        });
    });

})(window, document, Granite, Granite.$);

(function(document, $) {
    "use strict";
    var ns = ".cq-projects-admin-createproject-templates";

    $(document).on("foundation-selections-change" + ns, ".foundation-collection[role=listbox]", function(e) {
        var collection = $(this);

        collection.find(".foundation-collection-item").each(function() {
            var el = $(this);
            var isSelected = el.is(".foundation-selections-item");
            var input = el.find("input.projectinput[type=hidden]");
            input.prop("disabled", !isSelected);
        });
    });

})(document, Granite.$);


(function($, undefined) {
    "use strict";      


    $(document).on("foundation-contentloaded", function(e) {
		
        var properties = $(".cq-projects-admin-createproject-properties");
        var selected = $(".foundation-selections-item");

        properties.find(".card-asset img").attr("src", selected.find("img:not(.multiplied)").attr("src"));
        properties.find(".card-asset h4").text(selected.find("h4").text());
        properties.find(".card-asset .description").text(selected.find(".description").text());
    });

})(Granite.$);
