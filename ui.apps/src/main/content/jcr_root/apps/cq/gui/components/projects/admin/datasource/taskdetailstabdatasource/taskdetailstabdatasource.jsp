<%--
  ADOBE CONFIDENTIAL

  Copyright 2015 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%@page session="false"
            import="com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ExpressionHelper,
                  com.adobe.granite.ui.components.ExpressionResolver,
                  com.adobe.granite.ui.components.ds.AbstractDataSource,
                  com.adobe.granite.ui.components.ds.DataSource,
                  com.adobe.granite.ui.components.ds.EmptyDataSource,
                  org.apache.sling.api.resource.Resource,
                  java.util.Iterator,
                  java.util.List,
                  java.util.ArrayList,
                  org.apache.commons.lang.StringUtils,
                  org.apache.sling.api.resource.ResourceUtil,
                  org.apache.sling.api.resource.ValueMap,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.Session,
                  javax.jcr.security.Privilege,
                  javax.jcr.RepositoryException,
                  org.slf4j.Logger"%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@include file="/libs/cq/gui/components/projects/admin/datasource/projectdshelper.jsp" %><%
    ExpressionHelper ex = new ExpressionHelper(sling.getService(ExpressionResolver.class), pageContext);

    Resource commentCollectionParentResource = resourceResolver.getResource(request.getParameter("item"));

    ValueMap vmTask = commentCollectionParentResource.adaptTo(ValueMap.class);
    String workflowInstanceId = vmTask.get("wfInstanceId", String.class);
    if (workflowInstanceId != null) {
        String projectPath = vmTask.get("project.path", String.class);
        Resource workflowInstanceFolder = getWorkflowInstanceFolder(resourceResolver, projectPath, workflowInstanceId);
        if (workflowInstanceFolder != null && !ResourceUtil.isNonExistingResource(workflowInstanceFolder)) {
            commentCollectionParentResource = workflowInstanceFolder;
        }
    }

    AccessControlManager acm = resourceResolver.adaptTo(Session.class).getAccessControlManager();

    final Resource items = resource.getChild(Config.ITEMS);
    DataSource ds = EmptyDataSource.instance();

    final List<Resource> result = new ArrayList<Resource>();
    for(Resource child : items.getChildren()) {
        ValueMap vm = ResourceUtil.getValueMap(child);
         Boolean isCommentTab = vm.get("isCommentTab", Boolean.class);
        if (isCommentTab!= null && isCommentTab) {
            String subFolderName = vm.get("subFolderName", String.class);
            String targetPath = commentCollectionParentResource.getPath();
            if (StringUtils.isNotBlank(subFolderName)) {
                Resource target = commentCollectionParentResource.getChild(subFolderName);
                if (target != null && !ResourceUtil.isNonExistingResource(target)) {
                    log.warn("Configured target path {} for comments not found, skipping tab", target.getPath());
                    targetPath = target.getPath();
                } else {
                    targetPath = null;
                }
            }
            if (targetPath != null && checkReadAccess(acm, targetPath, log)) {
                result.add(child);
            } else if (StringUtils.isBlank(workflowInstanceId) && StringUtils.equals(subFolderName, "default")) {
               result.add(child);
            }
        } else {
            // not a comment tab
            result.add(child);
            }
    }


    if (true) {
        ds = new AbstractDataSource() {
            public Iterator<Resource> iterator() {
                return result.iterator();
            }
        };
    }

    request.setAttribute(DataSource.class.getName(), ds);

%><%!
  private boolean checkReadAccess(AccessControlManager acm, String resourcePath, Logger log) {
    if (acm != null) {
        Privilege p = null;
        try {
            p = acm.privilegeFromName(Privilege.JCR_READ);
            return acm.hasPrivileges(resourcePath, new Privilege[]{p});
        } catch (RepositoryException e) {
            log.warn("Failed to read permissions for {}, assuming no access", resourcePath);
        }
    }
    return false;
}
%>
