(function($, Granite, undefined) {
    'use strict';

    var taskDetailsNS = ".taskdetails";
    var completeButton = ".completeDetailsTask";
                var shotListTaskComplete = ".completeShotListTask";
    var cancelButton = ".cancelDetailsTask";
    var updateButton = ".updateDetailsTask";
    var uploadNS = ".taskdtails.upload";
    var uploadButton = ".cq-damadmin-admin-actions-asset-upload-activator";
    var addCommentButton = ".task-comment-addnew";
    var deleteCommentButton = ".task-comment-delete";
    var payloadDisplay = ".payloadDetailsTask";
                var shotListTask = ".shotListTask";
    var taskReviewButton = ".payloadDetailsTask";

    var ui = $(window).adaptTo("foundation-ui");

    $(document).off("fileuploadsuccess"+uploadNS, "span.coral-FileUpload");
    $(document).on("fileuploadsuccess"+uploadNS, "span.coral-FileUpload", function(e) {
        var newFileName = e.item.fileName;
        var taskId = getUrlVar("item");

        var targetUrl = Granite.HTTP.externalize("/libs/granite/taskmanager/updatetask?taskId=" + taskId + "&_charset_=utf-8");
        // get existing contentPath

        // attempt to use the foundation-content-path:
        var existingContentPath = $(".foundation-content-path").data("foundation-content-path");

        if (!existingContentPath || 0 == existingContentPath.length) {
            existingContentPath = $("[name=contentPath]").val();
        }

        if (existingContentPath && endsWith(existingContentPath, newFileName)) {
            // the content path did not change
            return;
        } else {
            ui.wait();

            // could be that the filename has changed but same path
            // could be that the path used to be a folder and now is a full path

            var lastSlash = existingContentPath.lastIndexOf('/');

            var newContentPath;
            // if we find a "." after the last slash we assume we have a file in the current payload which is being replaced
            if (existingContentPath.substr(lastSlash).indexOf('.') != -1 ) {
                newContentPath = existingContentPath.substr(0, lastSlash) + "/" + newFileName;
            } else {
                newContentPath = existingContentPath + "/" + newFileName;
            }
            var data = {contentPath : newContentPath};

            var ajaxOptions = {
                url: targetUrl,
                type: "post",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data, status, request) {
                    setTimeout(function(){
                        ui.clearWait();
                        var contentApi = $(".foundation-content").adaptTo("foundation-content");
                        contentApi.refresh();
                    }, 1000);
                },
                error: function(jqXHR, message, error) {
                    ui.clearWait();
                    ui.alert(Granite.I18n.get("Error"), message, "error");
                }
            };
            $.ajax(ajaxOptions);
        }
    });

    $(document)
        .off("foundation-contentloaded" + taskDetailsNS)
        .on("foundation-contentloaded" + taskDetailsNS, function(e) {
            var form = $("#updatetaskform");

            var statusValue = $("[name='status']", form).val();
            if ( statusValue !== "ACTIVE" ) {
                $(completeButton).attr("disabled", "disabled");
                $(updateButton).attr("disabled", "disabled");
                $(taskReviewButton).attr("disabled", "disabled");
                //GRANITE-4258 until it's resolved we hide
                $(uploadButton).closest(".coral-FileUpload").remove();
            } else {
                $(completeButton).removeAttr("disabled");
                $(updateButton).removeAttr("disabled");
                $(uploadButton).removeAttr("disabled");
                $(taskReviewButton).removeAttr("disabled");
            }
    });

    $(document).on("foundation-contentloaded" + taskDetailsNS, function(e) {
        var $form = $("form.task");
        var projectPath = $form.data("project");
        // find the project path field
        var $cuiSelectList = $(".coral-Autocomplete .coral-SelectList", $form);
        var originalURL = $cuiSelectList.attr("data-task-original-url");
        var updatedURL = $cuiSelectList.attr("data-granite-autocomplete-src");
        if (originalURL === undefined || originalURL === "") {
            $cuiSelectList.attr("data-task-original-url", updatedURL);
            originalURL = updatedURL;
        }
        $cuiSelectList.attr("data-granite-autocomplete-src", originalURL + "&projectPath="+projectPath);
    });

    $(document)
        .off("foundation-model-dataloaded" + taskDetailsNS)
        .on("foundation-model-dataloaded" + taskDetailsNS, function(e) {

            var form = $("#updatetaskform");

            if ( form.data("model").get("status") !== "ACTIVE" ) {
                $(completeButton).attr("disabled", "disabled");
                $(updateButton).attr("disabled", "disabled");
                //GRANITE-4258 until it's resolved we hide
                $(uploadButton).closest(".coral-FileUpload").remove();
            } else {
                $(completeButton).removeAttr("disabled");
                $(updateButton).removeAttr("disabled");
                $(uploadButton).removeAttr("disabled");
            }
    });

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    function disableButton() {
        $('.foundation-content button.primary[type=submit]')
            .attr("disabled", true)
            .removeClass("primary");
    }

    function checkTaskAction() {
        var actionSelect = $("#taskCompletionDialog").find("select");
        if ( actionSelect.length > 0 ) {
            if ( actionSelect.val() == "invalid_action" ) {
                return false;
            }
        }
        return true;
    }

    function submitForm($form, completeTask) {
        var postUrl = $form.attr("action");
        var taskId = getUrlVar("item");

        var taskModel = new Granite.TaskManagement.domain.TaskModel({id:taskId});
        taskModel.postUrl = postUrl;
        taskModel.set($form.serializeAsJSON(), {silent: true});

        // now fix the assignee -> formfield is named 'asignee' to match the persisted task,
        // however taskmanager expects ownerId
        taskModel.set("ownerId", taskModel.get("assignee"));
        taskModel.set("assignee", undefined);

        taskModel.set("title", taskModel.get("name"))

        if (completeTask) {
            var actionSelect = $("#taskCompletionDialog").find("select");
            if ( actionSelect.length > 0 ) {
                taskModel.set("action", actionSelect.val());
            }
            taskModel.set("comment", $("[name=comment]", "#taskCompletionDialog").val());

            return taskModel.completeWithUpdate();
        } else {
            return taskModel.updateDetails();
        }
    }

    function returnToPrevious(form) {
        var panelSelector = form.data("foundationFormOutputReplace") || form.data("foundationFormOutputPush");
        if (panelSelector) {
            var contentAPI = $(panelSelector).adaptTo("foundation-content");
            if (contentAPI) {
                contentAPI.back();
                $(document).trigger("foundation-history-change");
            }
        }
    }

    $(document).fipo("tap"+cancelButton, "click"+cancelButton, cancelButton, function(e) {
        var form = $("#updatetaskform");

        // the redirect should bring us to the same place ...
        if (form.data("redirect")) {
            window.location = form.data("redirect");
        }
    });

    function getUrlVar(key){
        var result = new RegExp(key + "=([^&]*)", "i").exec(window.location.search);
        return result && decodeURIComponent(result[1]) || "";
    }

    $(document).fipo("tap" + updateButton, "click" + updateButton, updateButton, function (e) {
        ui.wait();
        var form = $("#updatetaskform");

        e.preventDefault();
        var targettedObj = {};
        var pageList = [];
			$("input[name='./pages']").each(function(){
                var pageValue=$(this).val();
                if(pageValue!=''){
				pageList.push($(this).val());
                }

            });
		var taskPath = getUrlVar("item");
          targettedObj["pageList"] = pageList;
        targettedObj["taskPath"] = taskPath;
        	$.ajax({
                	url: "/bin/content/projects/tasks/pageList",
                    async: false,
                    method: "POST",
                    data: JSON.stringify(targettedObj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                }).always(function() {


                }).done(function() {

                }).fail(function(xhr) { 

                }); 

        submitForm(form, false).done(function (data, textStatus, xhr) {
                ui.clearWait();
                if (form.data("redirect")) {
                    window.location = form.data("redirect") + "&_charset_=utf-8&_ck=" + Date.now();
                } else {
                    returnToPrevious(form);
                }
            }).fail(function (xhr) {
                ui.clearWait();
             window.setTimeout(function() {
                form.trigger("foundation-form-submit-callback", [xhr]);
                 }, 3000);
            });
    });
                
                function completeTask() {
                                var form = $("#updatetaskform");

        $("#taskCompletionDialog").modal({
            buttons: [
                {
                    label: Granite.I18n.get("Cancel"),
                    className: "coral-Button",
                    click: function (evt) {
                        this.hide();
                    }
                },
                {
                    id: "completeButton",
                    label: Granite.I18n.get("Complete"),
                    className: "coral-Button coral-Button--primary",
                    click: function () {
                        var that = this;

                        if (!checkTaskAction()) {
                            // this should somehow indicate that the
                            // selected action is invalid.
                            return;
                        }

                        submitForm(form, true)
                            .done(function(dataFromComment, textStatusFromComment, xhrFromComment) {
                                dismissCompletionDialog(that, form);
                            })
                            .fail(function (xhr) {
                                that.hide();
                                form.trigger("foundation-form-submit-callback", [xhr]);
                            });
                    }
                }
            ]
        }).modal("show");
                }

    function dismissCompletionDialog(that, form) {
        that.hide();
        var api = form.adaptTo("model-foundation-form");
        if (!api.setModel()) {
            if (form.data("redirect")) {
                window.location = form.data("redirect") + "&_charset_=utf-8&_ck=" + Date.now();
            } else {
                returnToPrevious(form);
            }
        }
    }

    $(document).fipo("tap" + completeButton, "click" + completeButton, completeButton, function (e) {
        completeTask();
    });

                $(document).fipo("tap" + shotListTaskComplete, "click" + shotListTaskComplete, shotListTaskComplete, function (e) {
                                ui.wait();
                                
                                var ajaxFinalizeShotlist = $.ajax({
                                                url: "/project.shotlist.html" + $("#updatetaskform").data("project"),
                                                type: 'POST',
                                                async: false,
                                                data: {
                                                                finalize: true,
                                                                outputFolder: $("div.hiddenParams").data("workflow"),
                                                                '_charset_':'utf-8'
                                                                },
                                                success:function(data, status, jqxhr) {
                                                                ui.clearWait();
                                                                completeTask();
                                                },
                                                error:function(jqxhr, status, err) {
                                                                ui.clearWait();
                ui.alert(err, Granite.I18n.get("Unable to finalize shot list"), "error");
                                                }
        });
                });
                
    $(document).fipo("tap" + addCommentButton, "click" + addCommentButton, addCommentButton, function(e) {
        var $theButton = $(this);
        var $form = $theButton.closest("form");
        var $commentTarget = $($theButton.data("commentTargetRef"));

        var $mesageField = $form.find("[name='message']");
        var messageText = $mesageField.val();
        if (messageText === undefined || messageText === "" || messageText.trim() === "") {
            return;
        }

        // show the wait spinner
        ui.wait();

        var project = $("#updatetaskform").data("project");

        $theButton.attr('disabled','disabled');
        $mesageField.val("");


        var data = {
            "message" : messageText,
            "task" : $form.find("[name=':postUrl']").val(),
            ":operation" : "granite:comment",
            "_charset_" : "utf-8",
            ":redirect" : Granite.HTTP.externalize($form.find("[name=':redirect']").val()),
            "subFolder" : $form.find("[name='subFolder']").val()
        };

        var ajaxOptions = {
            url: Granite.HTTP.externalize(project),
            type: "post",
            data: data,
            success: function(data, status, request) {
                if (status === "success" ) {
                    $commentTarget.empty().append($(data).children());
                }
                $theButton.removeAttr("disabled");
                ui.clearWait();
            },
            error: function(jqXHR, message, error) {
                $mesageField.val(messageText);
                $theButton.removeAttr("disabled");
                ui.clearWait();
                ui.alert(error, Granite.I18n.get("Unable to add the new comment"), "error");
            }
        };
        $.ajax(ajaxOptions);

     });

    $(document).fipo("tap" + deleteCommentButton, "click" + deleteCommentButton, deleteCommentButton, function(e) {
        // show the wait spinner
        ui.wait();

        var $theButton = $(this);
        var $form = $theButton.closest("form");

        var data =  {
            ":operation" : "delete",
            "_charset_" : "utf-8",
            ":redirect" : Granite.HTTP.externalize($form.find("[name=':redirect']").val()) + "&_charset_=utf-8"
        };

        var postUrl = Granite.HTTP.externalize($(this).data("target"));

        var ajaxOptions = {
            url: postUrl,
            type: "post",
            data: data,
            success: function(data, status, request) {
                if (status === "success" ) {
                    $theButton.closest(".cq-tasks-comments").empty().append($(data).children());
                }
                $theButton.removeAttr("disabled");
                ui.clearWait();
            },
            error: function(jqXHR, message, error) {
                ui.clearWait();
                ui.alert(error, Granite.I18n.get("Unable to delete the comment"), "error");
            }
        };
        $.ajax(ajaxOptions);

    });

    $(document).fipo("tap", "click", payloadDisplay, function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $target = $(e.target);
        var url = $target.closest("button").data("href");
        var project = $target.closest("form#updatetaskform").data("project");
        var task = getUrlVar("item");
        var contentPath = $("#updatetaskform input[name='contentPath']").val();
        if (project.length) {
            url += contentPath + "?item=" + task + "&project=" + project + "&content=" + contentPath;
        } else {
            url += contentPath + "?item=" + task + "&content=" + contentPath;
        }
        if ( url) {
            window.location = encodeURI(Granite.HTTP.externalize(url)  + "&_charset_=utf-8");
        }
    });

                $(document).fipo("tap", "click", shotListTask, function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $target = $(e.target).closest(shotListTask);
        var url = $target.data("href");
        var project = $target.closest("form#updatetaskform").data("project");
        var task = getUrlVar("item");
        url += "/etc/commerce/products?item=" + project + "&task=" + task;
        if ( url && project) {
            window.location = Granite.HTTP.externalize(url);
        }
    });

    $(document).off("dropzonedragover" + uploadNS)
        .on("dropzonedragover" + uploadNS, function(event) {
           var message = Granite.I18n.get("Drag and drop to upload");
            var dragAndDropMessage = $('<div class=\"drag-drop-message\"><h1 > <span>{</span>' + message + '<span>}</span></h1></div>');
            $('#updatetaskform').overlayMask('show',
                dragAndDropMessage);
        });

    $(document).off("dropzonedrop" + uploadNS)
        .on("dropzonedrop"  + uploadNS, clearDropzoneOverlay);

    $(document).off("dropzonedragleave" + uploadNS)
        .on("dropzonedragleave" + uploadNS, clearDropzoneOverlay);

    function clearDropzoneOverlay() {
        $('#updatetaskform').overlayMask('hide');
    }


$(document).ready(function() {


   populateMultifield();


});

function populateMultifield(){
    var taskPath = getUrlVar("item");
    var jsonURL = "http://" + $(location).attr('host') + taskPath + ".json";

    $.ajax({
                	url: jsonURL,
                    async: false,
                    method: "GET",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                }).always(function() {


                }).done(function(result) {
                var pageListProperty=result.pageList;
        if(pageListProperty!=''){
                var pageListArray = pageListProperty.split(",");
        }
				var i = 0;

        if($(".js-coral-Multifield-input").length < 1){


                for(i=0; i<pageListArray.length; i++){

                    if(pageListArray[i]!=""){
					var page = pageListArray[i];


					 $('.js-coral-Multifield-list').append('<li class="js-coral-Multifield-input coral-Multifield-input"><section class="coral-Form-fieldset" data-name="./pageListJson" data-eaem-nested=""><div><div class="coral-Form-fieldwrapper"><span class="coral-Form-field coral-PathBrowser" data-init="pathbrowser" data-root-path="/content" data-option-loader="granite.ui.pathBrowser.pages.hierarchyNotFile" data-picker-src="/libs/wcm/core/content/common/pathbrowser/column.html/content?predicate=hierarchyNotFile" data-crumb-root="Content Root" data-picker-multiselect="false"><span class="coral-InputGroup coral-InputGroup--block"><input class="coral-InputGroup-input coral-Textfield js-coral-pathbrowser-input" type="text" name="./pages" autocomplete="off" value="'+page+'" aria-owns="coral-48" id="coral-49" aria-haspopup="true" aria-expanded="false"><span class="coral-InputGroup-button"><button class="coral-Button coral-Button--square js-coral-pathbrowser-button" type="button" title="Browse"><i class="coral-Icon coral-Icon--sizeS coral-Icon--folderSearch"></i></button></span></span><ul id="coral-48" class="coral-SelectList" role="listbox" aria-hidden="true" tabindex="-1"></ul></span></div></div></section><button type="button" class="js-coral-Multifield-remove coral-Multifield-remove coral-Button coral-Button--square coral-Button--quiet"><i class="coral-Icon coral-Icon--sizeS coral-Icon--delete coral-Button-icon"></i></button><button type="button" class="js-coral-Multifield-move coral-Multifield-move coral-Button coral-Button--square coral-Button--quiet"><i class="coral-Icon coral-Icon--sizeS coral-Icon--moveUpDown coral-Button-icon"></i></button></li>');                    
					$(".js-coral-Multifield-add").click();
                    }
        }

            $(".js-coral-pathbrowser-input[value='']").each(function(){ 
					$(this).closest('li.js-coral-Multifield-input').remove();
            });

        }
                }).fail(function(xhr) { 
					alert("failed");
                }); 

}
})(Granite.$, Granite);

