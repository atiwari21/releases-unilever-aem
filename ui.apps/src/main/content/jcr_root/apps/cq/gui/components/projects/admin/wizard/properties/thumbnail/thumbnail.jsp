<%--
  ADOBE CONFIDENTIAL

  Copyright 2013 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page session="false"
          import="com.adobe.granite.ui.components.Tag,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.AttrBuilder,
                  org.apache.commons.lang.StringUtils, org.apache.jackrabbit.util.Text"%><%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><ui:includeClientLib categories="cq.projects.admin.wizard.thumbnail" /><%

// in the use case where we're used as a properties component we can
// get information about the project from the item
// if we don't have a project then our path will be null
String projectPath = request.getParameter("item");

Tag tag = cmp.consumeTag();

AttrBuilder attrs = tag.getAttrs();

String xssTitle = "";
String xssDescription = "";
if (cmp.getValue() != null) {
    if (StringUtils.isNotBlank(projectPath)) {
        String coverUrl = request.getContextPath() + Text.escapePath(projectPath) + ".thumb.319.319.jpg";
        attrs.add("data-projectcoverpath", xssAPI.getValidHref(coverUrl));
    }
    xssDescription = xssAPI.encodeForHTML(cmp.getValue().get("jcr:description", ""));
    xssTitle = xssAPI.encodeForHTML(cmp.getValue().get("jcr:title", ""));
}

attrs.addClass("foundation-layout-thumbnail");
%><div <%= attrs.build() %>>
	<div class="foundation-layout-thumbnail-image grid">
	    <article class="card-asset">
	        <a>
	            <span class="image"><img alt="<%= i18n.get("Project thumbnail") %>"></span>
	            <div class="label">
                    <h4><%=xssTitle%></h4>
	                <p class="description"><%=xssDescription%></p>
	            </div>
	        </a>
        </article>
        
    </div>
</div>
