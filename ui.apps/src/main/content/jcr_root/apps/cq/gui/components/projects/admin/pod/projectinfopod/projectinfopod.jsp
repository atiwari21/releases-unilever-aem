<%--
  ADOBE CONFIDENTIAL

  Copyright 2013 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page session="false"
          import="com.adobe.cq.projects.api.Project,
                  com.adobe.cq.projects.api.ProjectLink,
                  com.adobe.granite.security.user.UserProperties,
                  com.adobe.granite.security.user.UserPropertiesManager,
                  com.adobe.granite.security.user.UserPropertiesService,
                  org.apache.commons.lang.StringUtils,
                  org.apache.jackrabbit.api.security.user.Authorizable,
                  org.apache.jackrabbit.api.security.user.UserManager,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
                  org.apache.sling.jcr.base.util.AccessControlUtil,
                  javax.jcr.RepositoryException,
                  javax.jcr.Session,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.security.Privilege,
                  java.net.URLEncoder,
                  java.text.SimpleDateFormat,
                  java.util.ArrayList,
                  java.util.Calendar,
                  java.util.List,
                  com.day.cq.commons.jcr.JcrConstants" %>
                      <%
%><%@include file="/libs/granite/ui/global.jsp"%><%
    AccessControlManager acm = null;

    Session userSession = resourceResolver.adaptTo(Session.class);
    try {
        acm = userSession.getAccessControlManager();
    } catch (RepositoryException e) {
        log.error("Unable to get access manager", e);
    }

    List<String> permissions = new ArrayList<String>();
    permissions.add("cq-project-admin-actions-open-activator");

    if (hasPermission(acm, resource, Privilege.JCR_REMOVE_NODE)) {
        permissions.add("cq-projects-admin-actions-delete-link-activator");
    }

    ProjectLink link = resource.adaptTo(ProjectLink.class);
    Project project = link.getProject();
    Resource projectResource = project.adaptTo(Resource.class);
    Resource projectContent = projectResource.getChild(JcrConstants.JCR_CONTENT);
    ValueMap projectValueMap = projectResource.adaptTo(ValueMap.class);
    ValueMap projectContentVM = projectContent.adaptTo(ValueMap.class);
    Calendar startDate = projectContentVM.get("project.dueDate", Calendar.class);

    String creatorImagePath = null;
    String xssEmail = null;
    String nameHtml = "";


    String creatorId = projectValueMap.get("jcr:createdBy", String.class);
    UserPropertiesService upService = sling.getService(UserPropertiesService.class);
    UserPropertiesManager upm = upService.createUserPropertiesManager(userSession, resourceResolver);
    UserManager um = AccessControlUtil.getUserManager(userSession);
    Authorizable authorizable = null;
    if (um != null) {
        authorizable = um.getAuthorizable(creatorId);
    }
    UserProperties up = authorizable != null ? upm.getUserProperties(authorizable, "profile") : null;
    String image = (up == null) ? "" : up.getResourcePath(UserProperties.PHOTOS, "/primary/image.prof.thumbnail.48.png", "");
    if (StringUtils.isBlank(image)) {
        if (authorizable != null && authorizable.isGroup()) {
            image = "/libs/granite/security/clientlib/themes/default/resources/sample-group-thumbnail.36.png";
        } else {
            image = "/libs/granite/security/clientlib/themes/default/resources/sample-user-thumbnail.100.png";
        }
    }

    if (authorizable != null) {
        String name = getFullName(authorizable,  up);
        if (name != null) {
            nameHtml = xssAPI.encodeForHTML(name);
        }
    }

    String email = (up == null) ? "" : up.getProperty(UserProperties.EMAIL);
    if (email != null) {
        xssEmail = xssAPI.encodeForHTML(email);
    }

    creatorImagePath = request.getContextPath() + image;


    String urlEncodedProjectPath = URLEncoder.encode(projectResource.getPath(), "UTF-8");
    String projectPropertiesHref = request.getContextPath() + "/apps/cq/core/content/projects/properties.html?item=" + urlEncodedProjectPath;

    String xssTitle = xssAPI.encodeForHTML(project.getTitle());

    String xssDescription = null;
    String description = project.getDescription();
    if (description != null) {
        description = description.replaceAll("\n", "<br>");
        xssDescription = xssAPI.encodeForHTML(description);
    }

    SimpleDateFormat fmt = new SimpleDateFormat("MMM dd");
    String xssDueDate = null;
    if (startDate != null) {
        xssDueDate = xssAPI.filterHTML(fmt.format(startDate.getTime()));
    }

    ValueMap cardMap = resource.adaptTo(ValueMap.class);
    int cardWeight = cardMap.get("cardWeight", 0);
%>
<article class="card-pod cq-projects-Pod cq-projects-Pod-projectInfo foundation-collection-item"
    data-path="<%=resource.getPath()%>"
    data-link="<%=resource.getPath()%>"
    data-gridlayout-sortkey="<%= cardWeight %>"
    itemprop="item" itemscope>
    <i class="select"></i>
    <div class="card">
        <nav class="endor-ActionBar">
            <div class="endor-ActionBar-left">
                <div class="endor-ActionBar-item">
                    <a class="endor-ActionBar-item coral-Button coral-Button--secondary coral-Button--quiet cq-projects-Pod-title" href="<%=xssAPI.getValidHref(projectPropertiesHref)%>"><%= xssTitle %></a>
                </div>
            </div>
        </nav>
        <div class="cq-projects-Pod-content u-coral-padding">
            <div class="foundation-layout-media"><%
                if (creatorImagePath != null && creatorImagePath.length() > 0) {
                   %><img src="<%=xssAPI.getValidHref(creatorImagePath)%>" class="cq-projects-Pod-projectInfo-img foundation-layout-media-img"><%
                } %>
                <div class="foundation-layout-media-bd"><h4><%= nameHtml %></h4><%
                    if ( xssEmail != null ) {
                       %><span class="email"><%= xssEmail%></span><%
                    }
                %></div>
            </div>
            <div class="cq-projects-Pod-projectInfo-description"><span><%= xssDescription %></span></div>
            <div class="cq-projects-Pod-projectInfo-bottom"><%
            if (xssDueDate != null) {
                %><i class="coral-Icon coral-Icon--clock"></i>
                <span><%= i18n.get("Due on {0}", "Show the due date of the project", xssDueDate) %></span><%
            } %>

            </div>
        </div>
        <div class="cq-projects-Pod-footer">
            <button class="coral-Button--secondary coral-Button--quiet u-coral-pullRight projects-pod-action" data-href="<%= projectPropertiesHref %>">
                <i class="coral-Icon coral-Icon--more cq-projects-Pod-icon" ></i>
            </button>
        </div>
    </div>
    <div class="foundation-collection-quickactions" data-foundation-collection-quickactions-rel="<%= StringUtils.join(permissions, " ") %>">
    </div>
</article>
<%!
    boolean hasPermission(AccessControlManager acm, Resource resource, String privilege) {
        try {
            if (acm != null) {
                Privilege p = acm.privilegeFromName(privilege);
                return acm.hasPrivileges(resource.getPath(), new Privilege[]{p});
            }
        } catch (RepositoryException e) {
            // if we have a error then we will return false.
        }
        return false;
    }

    private String getFullName(Authorizable authorizable, UserProperties up) {
        try {
            // if we have no user profile, we return the ID
            if (up == null) return authorizable.getID();

            String givenName = up.getProperty(UserProperties.GIVEN_NAME);
            String familyName = up.getProperty(UserProperties.FAMILY_NAME);
            String name = "";
            if (givenName != null) name += givenName;
            if (givenName != null && familyName != null) name += " ";
            if (familyName != null) name += familyName;
            if (name.length() == 0) name = authorizable.getID();
            return name;
        } catch (RepositoryException e) {
            return "";
        }
    }
%>