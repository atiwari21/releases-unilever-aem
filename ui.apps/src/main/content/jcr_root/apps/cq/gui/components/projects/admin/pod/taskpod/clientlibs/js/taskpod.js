/*
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2013 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */

(function ($, undefined) {
    "use strict";

    var ns = "cq-projects-tasklist-link";
    var rel = ".js-cq-projects-Pod-tasksPod";
    var allcards = ".js-cq-projects-Pod-tasksPod .taskchart";

    function renderChart(centerRadius, width, height, showLegend) {
        $(allcards).each(function(index, element) {
            var parent = "#"+$(element).attr("id");

            var taskTotal = $(rel).data("taskCount");

            // by default if there are no tasks make
            // the pie chart show a completed task,
            // i.e. green status
            var taskActive = 1;
            var taskOverdue = 0;
            var taskCompleted = 0;

            if (taskTotal && taskTotal != 0) {
                taskOverdue = $(rel).data("taskOverdue");
                taskActive = $(rel).data("taskActive");
                taskCompleted = taskTotal - (taskActive + taskOverdue);
            }


            var percentActive = (!taskTotal || taskTotal === 0) ? 0 : Math.round(10000 * taskActive / taskTotal) / 100;
            var percentComplete = (!taskTotal || taskTotal === 0) ? 0 : Math.round(10000 * taskCompleted / taskTotal) / 100;
            var percentOverdue = (!taskTotal || taskTotal === 0) ? 0 : Math.round(10000 * taskOverdue / taskTotal) / 100;

            var overdueLabel = Granite.I18n.get("{0}% Overdue", [ percentOverdue ]);
            var activeLabel = Granite.I18n.get("{0}% Active", [ percentActive ]);
            var completedLabel = Granite.I18n.get("{0}% Completed", [ percentComplete ]);

            var data = {
                value: [ taskOverdue, taskActive, taskCompleted],
                label: [ overdueLabel, activeLabel, completedLabel ],
                colors: [ '#fa786e', '#a5cd73', '#7dafe6'],
                x: [1, 1, 1]
            };

            var chart = dv.chart()
                .layers([
                    dv.geom.bar()
                ])
                .coord(dv.coord.polar().innerRadius(centerRadius))
                .data(data)
                .width(width)
                .height(height)
                .duration(0);

            if (showLegend) {
                chart.guide('fill', dv.guide.legend().orientation("bottom"));
            }

            chart.guide(['x', 'y'], 'none')
                .position('fill')
                .map("fill", "label", dv.scale.color().values(data.colors))
                .map('x', 'x', dv.scale.ordinal())
                .map('y', 'value')
                .parent(parent)
                .render();

            $(parent + " .plot-bg").css({'visibility': 'hidden'});
        });
    }

    $(document).on("foundation-contentloaded change:displayMode", function(event) {
        $(allcards).empty();
        var collection = $(allcards).closest(".foundation-collection");
        if (collection.length) {
            var dataFoundationLayout = collection.data("foundation-layout");
            if (collection.hasClass("foundation-layout-card")
                || (dataFoundationLayout && dataFoundationLayout.name == "foundation-layout-card")) {
                // card view
                renderChart(-5, 100, 150, false);
            } else {
                // list view
                renderChart(5, 28, 28, false);
            }
        }
    });

})(Granite.$);