<%--
  ADOBE CONFIDENTIAL

  Copyright 2012 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page session="false"
          import="org.apache.commons.lang.StringUtils,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
                  javax.jcr.RepositoryException,
                  javax.jcr.Session,
                  javax.jcr.security.AccessControlManager,
                  javax.jcr.security.Privilege,
                  java.util.ArrayList,
                  java.util.List,
                  org.apache.jackrabbit.JcrConstants" %><%
%><%
    AccessControlManager acm = null;
    try {
        acm = resourceResolver.adaptTo(Session.class).getAccessControlManager();
    } catch (RepositoryException e) {
        log.error("Unable to get access manager", e);
    }

    List<String> permissions = new ArrayList<String>();
    permissions.add("cq-project-admin-actions-open-activator");

    if (hasPermission(acm, resource, Privilege.JCR_REMOVE_NODE)) {
        permissions.add("cq-projects-admin-actions-delete-link-activator");
    }

    boolean canCreateTasks = hasPermission(acm, resource, Privilege.JCR_ADD_CHILD_NODES);

    String projectPath = slingRequest.getRequestPathInfo().getSuffix();
    Resource projectResource = resourceResolver.getResource(projectPath);
    if (     projectResource == null
         || !projectResource.isResourceType("cq/gui/components/projects/admin/card/projectcard") ) {
        return;
    }

    String ctx = request.getContextPath();
    final String xssHref = xssAPI.getValidHref(ctx + "/apps/cq/core/content/projects/showtasks.html?item=" + projectResource.getPath());
    final String wizardHref = ctx + "/apps/cq/core/content/projects/addtask.html?project=" + projectPath;

    Resource tasks = projectResource.getChild(JcrConstants.JCR_CONTENT + "/tasks");
    long xssTaskCount = 0;
    long xssActiveTasks = 0;
    long xssTotalOverdue = 0;
    long xssTaskCompleted = 0;
    long xssTaskPercentCompleted = 100;

    if (tasks != null) {
        ValueMap props = tasks.adaptTo(ValueMap.class);
        if (props.containsKey("totalTasks")) {
            xssTaskCount = props.get("totalTasks", Long.class);
        }

        if (props.containsKey("activeTasks")) {
            xssActiveTasks = props.get("activeTasks", Long.class);
        }

        if (props.containsKey("overdueTasks")) {
            xssTotalOverdue = props.get("overdueTasks", Long.class);
        }

        // completed are the total - (active + overdue)
        xssTaskCompleted = xssTaskCount - (xssActiveTasks + xssTotalOverdue);


        if (xssTaskCount != 0) {
            xssTaskPercentCompleted = (long) (100.0*(double)xssTaskCompleted/xssTaskCount);
        }
    }

    ValueMap cardMap = resource.adaptTo(ValueMap.class);
    int cardWeight = cardMap.get("cardWeight", 0);

%>

<article class="card-pod cq-projects-Pod js-cq-projects-Pod-tasksPod foundation-collection-item"
         data-path="<%=resource.getPath()%>"
         data-link="<%=resource.getPath()%>"
         data-task-count="<%=xssTaskCount%>"
         data-task-active="<%=xssActiveTasks%>"
         data-task-overdue="<%= xssTotalOverdue %>"
         data-gridlayout-sortkey="<%= cardWeight %>"
         itemprop="item" itemscope>
    <i class="select"></i>
    <div class="card">
        <nav class="endor-ActionBar">
            <div class="endor-ActionBar-left">
                <div class="endor-ActionBar-item">
                    <a class="endor-ActionBar-item coral-Button coral-Button--secondary coral-Button--quiet cq-projects-Pod-title" href="<%= xssAPI.getValidHref(xssHref) %>"><%= i18n.get("Tasks") %> <span>(<%= Long.toString(xssTaskCount) %>)</span></span></a>
                </div>
            </div>
            <% if (canCreateTasks) {
            %><div class="endor-ActionBar-right">
                <div class="endor-ActionBar-item">
                    <a href="<%=wizardHref%>" class="coral-Button--square coral-Button coral-Button--secondary coral-Button--quiet cq-projects-Pod-icon">
                        <i class="coral-Icon coral-Icon--add cq-projects-Pod-icon"></i>
                    </a>
                </div>
            </div><%
            } %>
        </nav>
        <div class="cq-projects-Pod-content">
            <div class="u-coral-clearFix task-data">
                <div class="chart-area">
                    <div id="<%= resource.getName() %>" class="taskchart"></div>
                    <div class="taskpercent"><h1><%=xssTaskPercentCompleted%>%</h1></div>
                </div>
                <ul>
                    <% if (xssTotalOverdue > 0) { %>
                    <li>
                        <div class="task-info">
                            <span class="endor-Badge overdue"><%=xssTotalOverdue%></span>
                            <h4><%= i18n.get("Overdue")%></h4>
                        </div>
                    </li>
                    <% } %>
                    <li>
                        <div class="task-info">
                            <span class="endor-Badge active"><%=xssActiveTasks%></span>
                            <h4><%= i18n.get("Active")%></h4>
                        </div>
                    </li>
                    <li>
                        <div class="task-info">
                            <span class="endor-Badge completed"><%=xssTaskCompleted%></span>
                            <h4><%= i18n.get("Completed")%></h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="cq-projects-Pod-footer">
            <button class="coral-Button--secondary coral-Button--quiet u-coral-pullRight projects-pod-action" data-href="<%= xssAPI.getValidHref(xssHref) %>">
                <i class="coral-Icon coral-Icon--more cq-projects-Pod-icon" ></i>
            </button>
        </div>
    </div>

    <div class="foundation-collection-quickactions" data-foundation-collection-quickactions-rel="<%= StringUtils.join(permissions, " ") %>">
    </div>
</article><%!
    boolean hasPermission(AccessControlManager acm, Resource resource, String privilege) {
        try {
            if (acm != null) {
                Privilege p = acm.privilegeFromName(privilege);
                return acm.hasPrivileges(resource.getPath(), new Privilege[]{p});
            }
        } catch (RepositoryException e) {
            // if we have a error then we will return false.
        }
        return false;
    }
%>

