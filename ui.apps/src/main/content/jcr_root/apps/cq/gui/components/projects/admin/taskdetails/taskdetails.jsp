<%--
  ADOBE CONFIDENTIAL

  Copyright 2012 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page session="false" %><%
%><%@page import="com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Value,
                  com.adobe.granite.xss.XSSAPI,
                  org.apache.jackrabbit.util.Text,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ResourceUtil" %>
<%@include file="/libs/granite/ui/global.jsp" %><%
%><ui:includeClientLib categories="taskmanagement.core, cq.projects.admin.tasks.taskdetails"/><%
    Config cfg = new Config(resource);

    AttrBuilder attrs = new AttrBuilder(request, xssAPI);

    attrs.add("id", cfg.get("id", String.class));
    attrs.addClass("foundation-form mode-edit vertical coral-Form--vertical task coral-FixedColumn");
    attrs.addOthers(cfg.getProperties(), "id", "redirect");

    String projectPath = "";
    String projectRedirectUrl = "";
    String[] items = request.getParameterValues("item");
    String xssRedirectUrl = "";
    if (items != null && items.length > 0) {
        // find the project
        Resource projectResource = findProjectResource( resourceResolver.getResource(items[0]) );


        if (projectResource!=null) {
            projectPath = projectResource.getPath();
            projectRedirectUrl = request.getContextPath()
                    + "/apps/cq/core/content/projects/showtasks.html"
                    + "?item="
                    + Text.escapePath(projectPath);
        } else {
            projectRedirectUrl = Text.escapePath(request.getContextPath()
                + "/assets.html/content/dam");
        }

        xssRedirectUrl = xssAPI.encodeForHTMLAttr(xssAPI.getValidHref(projectRedirectUrl));

    }

    boolean redirect  = cfg.get("redirect", Boolean.class);
    try {
        request.setAttribute(Value.CONTENTPATH_ATTRIBUTE, items[0]);
    %><form <%= attrs.build() %> method="post" action="/libs/granite/taskmanager/updatetask" <%
        if (redirect) { %>
            data-redirect="<%= xssRedirectUrl %>"<%
        } %>data-project="<%= projectPath %>">
        <sling:include resource="<%= resource %>" resourceType="granite/ui/components/foundation/container"/>
    </form><%
    if (items != null) {
        for (String taskPath : items) {
        %><div class="hidden task-path" data-task-path="<%= xssAPI.encodeForHTMLAttr(taskPath) %>"></div><%
        }
    }
    } finally {
        request.removeAttribute(Value.CONTENTPATH_ATTRIBUTE);
    }
%>

<%!
    private Resource findProjectResource(Resource contentResource) {
        if (contentResource == null || ResourceUtil.isNonExistingResource(contentResource)) {
            return null;
        }
        if (contentResource.isResourceType("cq/gui/components/projects/admin/card/projectcard")) {
            return contentResource;
        }
        return findProjectResource(contentResource.getParent());
    }
%>