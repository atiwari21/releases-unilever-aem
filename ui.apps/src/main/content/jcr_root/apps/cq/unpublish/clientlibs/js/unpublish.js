(function(document, $) {

    var editMode, later, params, paths, activator, contentPath;

    var DOCUMENT_REFERRER_KEY = "document.referrer";
    function unpublish(paths) {
        var replicationUrl = Granite.HTTP.externalize($('.unpublishwizard-url').data('replication-url'));
        var cmd = encodeURI("Deactivate");
        var settings = {
            "type": "POST",
            "data": {
                "_charset_": "utf-8",
                "cmd": cmd,
                "path": paths
            },
            "complete": deactivateCallback
        };
        $.ajax(replicationUrl, settings);
    }
    
    function unpublishPathsFromPreview(paths) {
    	var replicationToPreviewUrl = Granite.HTTP.externalize('/bin/unpublish/preview');
        var targettedObj = {};
        targettedObj["paths"] = paths;
        targettedObj["binunpublishpreview"] = "false";
        $.ajax({
            url: replicationToPreviewUrl,
            async: false,
            method: "POST",
            data: JSON.stringify(targettedObj),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data, textStatus, jQxhr) {

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("Error occured while unpublishing from preview" + errorThrown);
            },
            complete: deactivateCallback


        });
    }
    
    function unpublishPathsFromProduction(paths) {
    	var replicationToPreviewUrl = Granite.HTTP.externalize('/bin/unpublish/production');
        var targettedObj = {};
        targettedObj["paths"] = paths;
        targettedObj["binunpublishpreview"] = "false";
        $.ajax({
            url: replicationToPreviewUrl,
            async: false,
            method: "POST",
            data: JSON.stringify(targettedObj),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data, textStatus, jQxhr) {

            },
            error: function(jqXhr, textStatus, errorThrown) {
                console.log("Error occured while unpublishing from production" + errorThrown);
            },
            complete: deactivateCallback


        });
    }
    
    function referencesRetrieved(xhr, status) {
        if (status === "success") {
            var json = $.parseJSON(xhr.responseText);
            var pages = json["pages"];
            if (pages.length == 0 && !later) {
                //unpublish directly
                unpublish(paths);
            } else if(pages.length > 0) {
                var countRefs = 0;
                $.each(pages, function(i, value) {
                    if (pages[i].published && pages[i].isPage === "true" && pages[i].path !== pages[i].srcPath) {
                        countRefs++;
                    }
                });

                if (countRefs > 0) {
                    var ui = $(window).adaptTo("foundation-ui");
                    var message = Granite.I18n.get("Selected items are referenced by {0} page(s).", countRefs);
                    ui.prompt(Granite.I18n.get("Unpublish"), message, "notice", [{
                        text: Granite.I18n.get("Cancel")
                    }, {
                        text: Granite.I18n.get("Continue"),
                        warning: true,
                        handler: function() {
                            unpublish(paths);
                        }
                    }]);
                }else{
                	unpublish(paths);
                }
            } else {
                //go to wizzard
                redirectToUnpublishWizard();
            }
        }else{
            //error handling
            var notificationSlider = (Granite.author && Granite.author.notifications) ||
                new Granite.UI.NotificationSlider($(notificationSelector));
            var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to retrieve references for selected pages."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }

	    function referencesRetrievedPreview(xhr, status) {
		
        if (status === "success") {
            var json = $.parseJSON(xhr.responseText);
            var pages = json["pages"];
            if (pages.length == 0 && !later) {
                //unpublish directly
                unpublishPathsFromPreview(paths);
            } else if(pages.length > 0) {
                var countRefs = 0;
                $.each(pages, function(i, value) {
                    if (pages[i].published && pages[i].isPage === "true" && pages[i].path !== pages[i].srcPath) {
                        countRefs++;
                    }
                });

                if (countRefs > 0) {
                    var ui = $(window).adaptTo("foundation-ui");
                    var message = Granite.I18n.get("Selected items are referenced by {0} page(s).", countRefs);
                    ui.prompt(Granite.I18n.get("Unpublish"), message, "notice", [{
                        text: Granite.I18n.get("Cancel")
                    }, {
                        text: Granite.I18n.get("Continue"),
                        warning: true,
                        handler: function() {
                        	unpublishPathsFromPreview(paths);
                        }
                    }]);
                }else{
                	unpublishPathsFromPreview(paths);
                }
            } else {
                //go to wizzard
                redirectToUnpublishWizard();
                unpublishPathsFromPreview(paths);
            }
        }else{
            //error handling
            var notificationSlider = (Granite.author && Granite.author.notifications) ||
                new Granite.UI.NotificationSlider($(notificationSelector));
            var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to retrieve references for selected pages."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }
	
	    function referencesRetrievedProduction(xhr, status) {
		
        if (status === "success") {
            var json = $.parseJSON(xhr.responseText);
            var pages = json["pages"];
            if (pages.length == 0 && !later) {
                //unpublish directly
            	unpublishPathsFromProduction(paths);
            } else if(pages.length > 0) {
                var countRefs = 0;
                $.each(pages, function(i, value) {
                    if (pages[i].published && pages[i].isPage === "true" && pages[i].path !== pages[i].srcPath) {
                        countRefs++;
                    }
                });

                if (countRefs > 0) {
                    var ui = $(window).adaptTo("foundation-ui");
                    var message = Granite.I18n.get("Selected items are referenced by {0} page(s).", countRefs);
                    ui.prompt(Granite.I18n.get("Unpublish"), message, "notice", [{
                        text: Granite.I18n.get("Cancel")
                    }, {
                        text: Granite.I18n.get("Continue"),
                        warning: true,
                        handler: function() {
                        	unpublishPathsFromProduction(paths);
                        }
                    }]);
                }else{
                	unpublishPathsFromProduction(paths);
                }
            } else {
                //go to wizzard
                redirectToUnpublishWizard();
                unpublishPathsFromProduction(paths);
            }
        }else{
            //error handling
            var notificationSlider = (Granite.author && Granite.author.notifications) ||
                new Granite.UI.NotificationSlider($(notificationSelector));
            var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to retrieve references for selected pages."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }
	
    CQ_UI_siteadmin_quickUnpublish = function(childPaths, edit, hasPermission) {
        var gPath = Granite.HTTP.getPath();
        var lastDot = gPath.lastIndexOf(".");
        var extSuffixe = gPath.substr(lastDot);
        var suffixeSlash = extSuffixe.indexOf('/');

        contentPath = extSuffixe.substr(suffixeSlash);
        if (suffixeSlash == -1) {
            contentPath = '/content';
        }

        var referencesUrl = Granite.HTTP.externalize($('.unpublishwizard-url').data('references-url'));
        editMode = edit || false;
        var data = { };
        paths = childPaths;
        if(!paths || paths.length == 0){
            paths = [];
            var item = $(activator.data("foundationCollectionItem"));

            var itemPath = contentPath;

            if (item) {
                itemPath = item.data("path");
            }
            paths.push(encodeURI(itemPath));
        }
        data["path"] = paths;
        data["predicate"] = "wcmcontent";

        if(!hasPermission || later){
            redirectToUnpublishWizard();
            return;
        }

        $.ajax(referencesUrl, {
            "data": data,
            "cache": false,
            "dataType": "json",
            "complete": referencesRetrieved
        });
    };

	
	    CQ_UI_siteadmin_quickUnpublishFromPreview = function(childPaths, edit, hasPermission) {
        var gPath = Granite.HTTP.getPath();
        var lastDot = gPath.lastIndexOf(".");
        var extSuffixe = gPath.substr(lastDot);
        var suffixeSlash = extSuffixe.indexOf('/');

        contentPath = extSuffixe.substr(suffixeSlash);
        if (suffixeSlash == -1) {
            contentPath = '/content';
        }

        var referencesUrl = Granite.HTTP.externalize($('.unpublishwizard-url').data('references-url'));
        editMode = edit || false;
        var data = { };
        paths = childPaths;
        if(!paths || paths.length == 0){
            paths = [];
            var item = $(activator.data("foundationCollectionItem"));

            var itemPath = contentPath;

            if (item) {
                itemPath = item.data("path");
            }
            paths.push(encodeURI(itemPath));
        }
        data["path"] = paths;
        data["predicate"] = "wcmcontent";

        if(!hasPermission || later){
            redirectToUnpublishWizard();
            return;
        }

        $.ajax(referencesUrl, {
            "data": data,
            "cache": false,
            "dataType": "json",
            "complete": referencesRetrievedPreview
        });
    };
	
	    CQ_UI_siteadmin_quickUnpublishFromProduction = function(childPaths, edit, hasPermission) {
        var gPath = Granite.HTTP.getPath();
        var lastDot = gPath.lastIndexOf(".");
        var extSuffixe = gPath.substr(lastDot);
        var suffixeSlash = extSuffixe.indexOf('/');

        contentPath = extSuffixe.substr(suffixeSlash);
        if (suffixeSlash == -1) {
            contentPath = '/content';
        }

        var referencesUrl = Granite.HTTP.externalize($('.unpublishwizard-url').data('references-url'));
        editMode = edit || false;
        var data = { };
        paths = childPaths;
        if(!paths || paths.length == 0){
            paths = [];
            var item = $(activator.data("foundationCollectionItem"));

            var itemPath = contentPath;

            if (item) {
                itemPath = item.data("path");
            }
            paths.push(encodeURI(itemPath));
        }
        data["path"] = paths;
        data["predicate"] = "wcmcontent";

        if(!hasPermission || later){
            redirectToUnpublishWizard();
            return;
        }

        $.ajax(referencesUrl, {
            "data": data,
            "cache": false,
            "dataType": "json",
            "complete": referencesRetrievedProduction
        });
    };
    function redirectToUnpublishWizard(){
        params = "";
        for (var i = 0; i < paths.length; i++) {
            if (params.length > 0) {
                params += "&";
            }
            params += "item=" + paths[i];
        }
        var wizardUrl = Granite.HTTP.externalize($('.unpublishwizard-url').data('url'));
        
        if (params.length > 0) {
            location.href = wizardUrl + "?" + params + (later?("&later"):"") + (editMode?("&editmode"):"");
        } 
    }
    
    function deactivateCallback(xhr, status){
        var $notificationSelector = $(".endor-Page-content.endor-Panel.foundation-content"),
            $foundationContent = $(".foundation-content"),
            notificationSlider = (Granite.author && Granite.author.notifications) ||
                new Granite.UI.NotificationSlider($notificationSelector);
        var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";

        if (status === "success" || status === "parsererror") {
            if($foundationContent.length > 0){
                var contentApi = $foundationContent.adaptTo("foundation-content");
                contentApi.refresh();
            }else{
                $('.pageinfo .popover').show();
            }
            notificationSlider.notify({
                content: Granite.I18n.get("The page has been unpublished"),
                type: "info",
                closable: false
            });
        }else{
            //error handling
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to unpublish the selected page(s)."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }


    $(document).off("foundation-contentloaded.cards_unpublish").on("foundation-contentloaded.cards_unpublish", function(e) {

        if($(".cq-siteadmin-admin-actions-unpublish-activator").length > 0){
            $(document).off("tap.unpublish click.unpublish").fipo("tap.unpublish", "click.unpublish", ".cq-siteadmin-admin-actions-unpublishnow-activator, .cq-siteadmin-admin-actions-unpublishlater-activator", function(e) {
                CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                activator = $(this);
                later = $(this).data("later") || false;

                var selectedItems = $(".foundation-collection").find(".foundation-selections-item");
                var childPaths = [];
                for (var i = 0; i < selectedItems.length; i++) {
                    var $item = $(selectedItems[i]);
                    var path = $item.data("path");
                    childPaths.push(encodeURI(path));
                }
                activator.closest(".coral-Popover").hide();
                CQ_UI_siteadmin_quickUnpublish(childPaths, false, true);
            });
        }

        if($(".cq-siteadmin-admin-actions-quickunpublish-activator").length > 0){
            $(document).on('click', '.unpublish-preview-button', function(event) {
            	$('.unpublish-preview-cls').show();
            	$('.unpublish-everything-cls').hide();
            	$('.unpublish-production-cls').hide();
            	$(document).on('click', '.unpublish-preview', function(event) {
                CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                activator = $(this);
                var childPaths = [];
                childPaths.push(encodeURI(activator.data("path")));
                var isEditor = activator.data("edit") || false;
                CQ_UI_siteadmin_quickUnpublishFromPreview(childPaths, isEditor, true);
            });
            });
            $(document).on('click', '.unpublish-production-button', function(event) {
            	$('.unpublish-production-cls').show();
            	$('.unpublish-everything-cls').hide();
            	$('.unpublish-preview-cls').hide();
            	$(document).on('click', '.unpublish-production', function(event) {
                CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                activator = $(this);
                var childPaths = [];
                childPaths.push(encodeURI(activator.data("path")));
                var isEditor = activator.data("edit") || false;
                CQ_UI_siteadmin_quickUnpublishFromProduction(childPaths, isEditor, true);
            });
            });
            $(document).on('click', '.unpublish-everything-button', function(event) {
            	$('.unpublish-everything-cls').show();
            	$('.unpublish-preview-cls').hide();
            	$('.unpublish-production-cls').hide();
            	$(document).on('click', '.unpublish-everything', function(event) {
            		CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                    activator = $(this);
                    var childPaths = [];
                    childPaths.push(encodeURI(activator.data("path")));
                    var isEditor = activator.data("edit") || false;
                    CQ_UI_siteadmin_quickUnpublish(childPaths, isEditor, true);
            	});
            });
        }
    });

}(document, Granite.$));