<%--
  ADOBE CONFIDENTIAL

  Copyright 2012 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@page session="false" contentType="text/html; charset=utf-8"%><%
%><%@page import="org.apache.sling.api.resource.Resource,
                    com.adobe.granite.ui.components.AttrBuilder,
                    com.adobe.granite.ui.components.Config,
                    com.adobe.granite.xss.XSSAPI,
                    com.day.cq.i18n.I18n,
                    com.day.cq.wcm.api.Page,
                    com.day.cq.commons.JSONItem,
                    java.util.Collection,
                    com.day.cq.wcm.api.Template" %><%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%
%><%@taglib prefix="ui" uri="http://www.adobe.com/taglibs/granite/ui/1.0"%><%
%><cq:defineObjects /><%
%><ui:includeClientLib categories="cq.gui.siteadmin.deactivate"/><%
    // dom attributes
    Config cfg = new Config(resource);
    AttrBuilder spanAttrs = new AttrBuilder(request, xssAPI);
    spanAttrs.addOther("replication-url", xssAPI.getValidHref(cfg.get("replicationURL", "/bin/replicate.json")));
    spanAttrs.addOther("references-url", xssAPI.getValidHref(cfg.get("referencesURL", "/bin/wcm/references.json")));
    spanAttrs.addOther("url", xssAPI.getValidHref(cfg.get("wizardURL", "/libs/wcm/core/content/sites/unpublishpagewizard.html")));
    spanAttrs.addClass("hide unpublishwizard-url");
%><span <%= spanAttrs.build() %>></span>