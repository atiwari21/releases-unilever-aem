(function(document, $) {

    var editMode, later, params, paths, activator, contentPath;

    var DOCUMENT_REFERRER_KEY = "document.referrer";

    var notificationSelector = ".endor-Page-content.endor-Panel.foundation-content";

    function activateCallback(xhr, status) {
        var notificationSlider = (Granite.author && Granite.author.notifications) ||
            new Granite.UI.NotificationSlider($(notificationSelector));
        var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";
        if (status === "success" || status === "parsererror") {
            if ($(".foundation-content").length > 0) {
                var contentApi = $(".foundation-content").adaptTo("foundation-content");
                contentApi.refresh();
            } else {
                $('#pageinfo-popover').popover("hide");
            }
            notificationSlider.notify({
                content: Granite.I18n.get("The page has been published"),
                type: "info",
                closable: false
            });
        } else {
            //error handling
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to publish the selected page(s)."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }

    function referencesRetrieved(xhr, status) {
        if (status === "success") {
            var json = $.parseJSON(xhr.responseText);
            var assets = json["assets"];
            if (assets.length == 0 && !later) {
                //publish directly
                var replicationUrl = Granite.HTTP.externalize($('.publishwizard-url').data('replication-url'));
                var cmd = encodeURI("Activate");
                var settings = {
                    "type": "POST",
                    "data": {
                        "_charset_": "utf-8",
                        "cmd": cmd,
                        "path": paths
                    },
                    "complete": activateCallback
                };
                $.ajax(replicationUrl, settings);
            } else {
                //go to wizzard
                redirectToPublishWizard();
            }
        } else {
            //error handling
            var notificationSlider = (Granite.author && Granite.author.notifications) ||
                new Granite.UI.NotificationSlider($(notificationSelector));
            var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to retrieve references for selected pages."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }

    function referencesRetrievedPreview(xhr, status) {
        if (status === "success") {
            var json = $.parseJSON(xhr.responseText);
            var assets = json["assets"];
            if (assets.length == 0 && !later) {
                //publish directly
                var replicationToPreviewUrl = Granite.HTTP.externalize('/bin/publish/preview');
                var targettedObj = {};
                targettedObj["paths"] = paths;
                console.log(paths);
                targettedObj["binpublishpreview"] = "false";
                $.ajax({
                    url: replicationToPreviewUrl,
                    async: false,
                    method: "POST",
                    data: JSON.stringify(targettedObj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(data, textStatus, jQxhr) {

                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("Error occured while publishing to preview" + errorThrown);
                    },
                    complete:activateCallback


                });
               /* var replicationUrl = Granite.HTTP.externalize($('.publishwizard-url').data('replication-url'));
                var cmd = encodeURI("Activate");
                var settings = {
                    "type": "POST",
                    "data": {
                        "_charset_": "utf-8",
                        "cmd": cmd,
                        "path": paths
                    },
                    "complete": activateCallback
                };
                $.ajax(replicationUrl, settings);
                */
            } else {
                //go to wizzard
                redirectToPublishWizard();
                var replicationToPreviewUrl = Granite.HTTP.externalize('/bin/publish/preview');
                var targettedObj = {};
                targettedObj["paths"] = paths;
                console.log(paths);
                targettedObj["binpublishpreview"] = "false";
                $.ajax({
                    url: replicationToPreviewUrl,
                    async: false,
                    method: "POST",
                    data: JSON.stringify(targettedObj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(data, textStatus, jQxhr) {

                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("Error occured while publishing to preview" + errorThrown);
                    },
                    complete:activateCallback


                });
            }
        } else {
            //error handling
            var notificationSlider = (Granite.author && Granite.author.notifications) ||
                new Granite.UI.NotificationSlider($(notificationSelector));
            var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to retrieve references for selected pages."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }

    function referencesRetrievedProduction(xhr, status) {
        if (status === "success") {
            var json = $.parseJSON(xhr.responseText);
            var assets = json["assets"];
            if (assets.length == 0 && !later) {
                //publish directly
                var replicationToPreviewUrl = Granite.HTTP.externalize('/bin/publish/production');
                var targettedObj = {};
                targettedObj["paths"] = paths;
                console.log(paths);
                targettedObj["binpublishpreview"] = "false";
                $.ajax({
                    url: replicationToPreviewUrl,
                    async: false,
                    method: "POST",
                    data: JSON.stringify(targettedObj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(data, textStatus, jQxhr) {

                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("Error occured while publishing to production" + errorThrown);
                    },
                    complete:activateCallback


                });
               /* var replicationUrl = Granite.HTTP.externalize($('.publishwizard-url').data('replication-url'));
                var cmd = encodeURI("Activate");
                var settings = {
                    "type": "POST",
                    "data": {
                        "_charset_": "utf-8",
                        "cmd": cmd,
                        "path": paths
                    },
                    "complete": activateCallback
                };
                $.ajax(replicationUrl, settings);
                */
            } else {
                //go to wizzard
                redirectToPublishWizard();
                var replicationToPreviewUrl = Granite.HTTP.externalize('/bin/publish/production');
                var targettedObj = {};
                targettedObj["paths"] = paths;
                console.log(paths);
                targettedObj["binpublishpreview"] = "false";
                $.ajax({
                    url: replicationToPreviewUrl,
                    async: false,
                    method: "POST",
                    data: JSON.stringify(targettedObj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(data, textStatus, jQxhr) {

                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log("Error occured while publishing to production" + errorThrown);
                    },
                    complete:activateCallback


                });
            }
        } else {
            //error handling
            var notificationSlider = (Granite.author && Granite.author.notifications) ||
                new Granite.UI.NotificationSlider($(notificationSelector));
            var notificationClass = Granite.author !== undefined ? "admin-notification" : "notification-alert--absolute admin-notification";
            notificationSlider.notify({
                content: Granite.I18n.get("Failed to retrieve references for selected pages."),
                type: "error",
                closable: false,
                className: notificationClass
            });
        }
    }

    function redirectToPublishWizard() {
        params = "";
        for (var i = 0; i < paths.length; i++) {
            if (params.length > 0) {
                params += "&";
            }
            params += "item=" + paths[i];
        }
        var wizardUrl = Granite.HTTP.externalize($('.publishwizard-url').data('url'));

        if (params.length > 0) {
            location.href = wizardUrl + "?" + params + (later ? ("&later") : "") + (editMode ? ("&editmode") : "");
        }
    }

    CQ_UI_siteadmin_quickPublish = function(refPaths, edit, hasPermission) {
        var gPath = Granite.HTTP.getPath();
        var lastDot = gPath.lastIndexOf(".");
        var extSuffixe = gPath.substr(lastDot);
        var suffixeSlash = extSuffixe.indexOf('/');

        contentPath = extSuffixe.substr(suffixeSlash);
        if (suffixeSlash == -1) {
            contentPath = '/content';
        }

        var referencesUrl = Granite.HTTP.externalize($('.publishwizard-url').data('references-url'));
        editMode = edit || false;
        var data = {};
        data["path"] = refPaths;
        if (refPaths.length == 0) {
            var item = $(activator.data("foundationCollectionItem"));
            var itemPath = contentPath;
            if (item) {
                itemPath = item.data("path");
            }
            refPaths.push(encodeURI(itemPath));
        }
        paths = refPaths;

        if (!hasPermission) {
            redirectToPublishWizard();
            return;
        }

        $.ajax(referencesUrl, {
            "data": data,
            "cache": false,
            "dataType": "json",
            "complete": referencesRetrieved
        });
    };

    CQ_UI_siteadmin_quickPublishPreview = function(refPaths, edit, hasPermission) {
        var gPath = Granite.HTTP.getPath();
        var lastDot = gPath.lastIndexOf(".");
        var extSuffixe = gPath.substr(lastDot);
        var suffixeSlash = extSuffixe.indexOf('/');

        contentPath = extSuffixe.substr(suffixeSlash);
        if (suffixeSlash == -1) {
            contentPath = '/content';
        }

        var referencesUrl = Granite.HTTP.externalize($('.publishwizard-url').data('references-url'));
        editMode = edit || false;
        var data = {};
        data["path"] = refPaths;
        if (refPaths.length == 0) {
            var item = $(activator.data("foundationCollectionItem"));
            var itemPath = contentPath;
            if (item) {
                itemPath = item.data("path");
            }
            refPaths.push(encodeURI(itemPath));
        }
        paths = refPaths;

        if (!hasPermission) {
            redirectToPublishWizard();
            return;
        }

        $.ajax(referencesUrl, {
            "data": data,
            "cache": false,
            "dataType": "json",
            "complete": referencesRetrievedPreview
        });
    };
    CQ_UI_siteadmin_quickPublishProduction = function(refPaths, edit, hasPermission) {
        var gPath = Granite.HTTP.getPath();
        var lastDot = gPath.lastIndexOf(".");
        var extSuffixe = gPath.substr(lastDot);
        var suffixeSlash = extSuffixe.indexOf('/');

        contentPath = extSuffixe.substr(suffixeSlash);
        if (suffixeSlash == -1) {
            contentPath = '/content';
        }

        var referencesUrl = Granite.HTTP.externalize($('.publishwizard-url').data('references-url'));
        editMode = edit || false;
        var data = {};
        data["path"] = refPaths;
        if (refPaths.length == 0) {
            var item = $(activator.data("foundationCollectionItem"));
            var itemPath = contentPath;
            if (item) {
                itemPath = item.data("path");
            }
            refPaths.push(encodeURI(itemPath));
        }
        paths = refPaths;

        if (!hasPermission) {
            redirectToPublishWizard();
            return;
        }

        $.ajax(referencesUrl, {
            "data": data,
            "cache": false,
            "dataType": "json",
            "complete": referencesRetrievedProduction
        });
    };
    $(document).off("foundation-contentloaded.cards_publish").on("foundation-contentloaded.cards_publish", function(e) {
    	$(document).on('click', '#pageinfo-trigger', function(event) {
    		var canReplicatePreviewPublish = $('#pageinfo-popover').find('.endor-List').children("button.preview-quickpublish-activator").attr('data-canreplicate');
    		        $('.preview-quickpublish-activator').show();
    		            $('.production-quickpublish-activator').show();
    		            $('.unpublish-preview-button').show();
    		            $('.unpublish-production-button').show();
    		        if(canReplicatePreviewPublish && canReplicatePreviewPublish == 'false'){
    		        $('.preview-quickpublish-activator').hide();
    		            $('.production-quickpublish-activator').hide();
    		            $('.unpublish-preview-button').hide();
    		            $('.unpublish-production-button').hide();
    		        }
    		        });
        if ($(".cq-siteadmin-admin-actions-publish-activator").length > 0) {
            $(document).off("tap.publish click.publish").fipo("tap.publish", "click.publish", ".cq-siteadmin-admin-actions-publishnow-activator, .cq-siteadmin-admin-actions-publishlater-activator", function(e) {
                CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                activator = $(this);
                later = activator.data("later") || false;

                var selectedItems = $(".foundation-collection").find(".foundation-selections-item");
                var childPaths = [];
                for (var i = 0; i < selectedItems.length; i++) {
                    var $item = $(selectedItems[i]);
                    var path = $item.data("path");
                    childPaths.push(encodeURI(path));
                }

                CQ_UI_siteadmin_quickPublish(childPaths, false, true);

            });
        }

        if ((".cq-siteadmin-admin-actions-quickpublish-activator").length > 0) {
            $(document).on('click', '.preview-quickpublish-activator', function(event) {
                CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                activator = $(this);
                var childPaths = [];
                childPaths.push(encodeURI(activator.data("path")));
                var isEditor = activator.data("edit") || false;
                var hasPermission = activator.data("canreplicate") || !isEditor;
                CQ_UI_siteadmin_quickPublishPreview(childPaths, isEditor, hasPermission);
            });
            $(document).on('click', '.production-quickpublish-activator', function(event) {
                CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                activator = $(this);
                var childPaths = [];
                childPaths.push(encodeURI(activator.data("path")));
                var isEditor = activator.data("edit") || false;
                var hasPermission = activator.data("canreplicate") || !isEditor;
                CQ_UI_siteadmin_quickPublishProduction(childPaths, isEditor, hasPermission);
            });
            $(document).on('click', '.publish-quickpublish-activator', function(event) {
                CUI.util.state.setSessionItem(DOCUMENT_REFERRER_KEY, location.href);
                activator = $(this);
                var childPaths = [];
                childPaths.push(encodeURI(activator.data("path")));
                var isEditor = activator.data("edit") || false;
                var hasPermission = activator.data("canreplicate") || !isEditor;
                CQ_UI_siteadmin_quickPublish(childPaths, isEditor, hasPermission);
            });

        }

    });

})(document, Granite.$);