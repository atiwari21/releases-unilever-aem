(function (document, $) {
    "use strict";

    var FOUNDATION_LAYOUT_LIST = ".foundation-layout-list";
    var SITE_ADMIN_CHILD_PAGES = "cq-siteadmin-admin-childpages";
    var TITLE_SELECTOR = "[data-title='Title']";
    var ARTICLE_TITLE_SELECTOR = ".label .main";
    var COMMAND_URL = Granite.HTTP.externalize("/bin/addPreviewProductionReplication");
    $(document).on("foundation-mode-change", function(e, mode, group){
        //not on sites list, may be assets, return
        if((group != SITE_ADMIN_CHILD_PAGES) || (mode == "selection") ){
            return;
        }

        //group is cq-siteadmin-admin-childpages for sites
        var $collection = $(".foundation-collection[data-foundation-mode-group=" + group + "]");

        if (!$collection.is(FOUNDATION_LAYOUT_LIST)) {
            return;
        }

        //adjust the width of title column to accommodate Path column
        $(".list .card-page .main").css("width", "15%");

        var $hTitle = $("." + SITE_ADMIN_CHILD_PAGES).find("header").find(TITLE_SELECTOR);

        var $previewElement,$productionElement;
        if($('.replicatePreview').attr("data-default") || $('.replicateProduction').attr("data-default")){
            $('.replicatePreview').css("width", "15%");
            $('.replicateProduction').css("width", "15%");
        $("article").each(function(index, article){
            var $article = $(article);
            var jsonGetData = [];
            var lastReplicatedToPreview,lastReplicatedToProduction;
            $.ajax({
				url: COMMAND_URL + "?path=" + $article.data("path"),
				method: "GET",
				async: false

			}).done(function(data) {
                 jsonGetData = JSON.parse(data);
			});
            var repPreview = '';
            var repProduction = '';
            if(jsonGetData.replicationToPreview){
                repPreview = jsonGetData.replicationToPreview;
            }
            if(jsonGetData.replicationToProduction){
                repProduction = jsonGetData.replicationToProduction;
            }
            $previewElement = $("<div/>").attr('class', 'replicatePreview')
                                .append($("<h4/>").attr("class", "foundation-collection-item-title")
                                            .html(repPreview));
            $productionElement = $("<div/>").attr('class', 'replicateProduction')
                                .append($("<h4/>").attr("class", "foundation-collection-item-title")
                                            .html(repProduction));

            //insert path data after title
            if($('.replicatePreview').attr("data-default")){
            $article.find(".published").after($previewElement);
            }
            if($('.replicateProduction').attr("data-default") && $('.replicatePreview').attr("data-default")){
            $article.find(".replicatePreview").after($productionElement);
            }else if($('.replicateProduction').attr("data-default")){
          $article.find(".published").after($productionElement);
            }
        })
        }
    });
})(document, jQuery);