(function(window, document, $, URITemplate) {
    var ui = $(window).adaptTo("foundation-ui");
    var replicateURL = Granite.HTTP.externalize("/bin/replicate");
    var COMMAND_URL = Granite.HTTP.externalize("/bin/publish/preview");
    var COMMAND_URL_PROD = Granite.HTTP.externalize("/bin/publish/production");
    var COMMAND_URL_UNPUBLISH = Granite.HTTP.externalize("/bin/unpublish/preview");
    var COMMAND_URL_PROD_UNPUBLISH = Granite.HTTP.externalize("/bin/unpublish/production");
    var NUM_CHILDREN_CHECK = 20;

    function unpublishFromPreview(paths) {
    	var targettedObj = {};
        targettedObj["paths"] = paths;
        targettedObj["binunpublishpreview"] = "false";
        $.ajax({
        	url: COMMAND_URL_UNPUBLISH,
            async: false,
            method: "POST",
            data: JSON.stringify(targettedObj),
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).always(function() {
            ui.clearWait();
        }).done(function() {
            var contentApi = $(".foundation-content").adaptTo("foundation-content");
            contentApi.refresh();

            ui.notify(null, Granite.I18n.get("The page has been unpublished"));
        }).fail(function(xhr) {
            var title = Granite.I18n.get("Error");
            var message = Granite.I18n.getVar($(xhr.responseText).find("#Message").html());
            if(xhr.status == 200 && !message){
            	ui.notify(null, Granite.I18n.get("The page has been unpublished"));
            }else{
            ui.alert(title, message, "error");
            }
        });

    }
    
    function unpublishFromProduction(paths) {
    	var targettedObj = {};
        targettedObj["paths"] = paths;
        targettedObj["binunpublishpreview"] = "false";
        $.ajax({
        	url: COMMAND_URL_PROD_UNPUBLISH,
            async: false,
            method: "POST",
            data: JSON.stringify(targettedObj),
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).always(function() {
            ui.clearWait();
        }).done(function() {
            var contentApi = $(".foundation-content").adaptTo("foundation-content");
            contentApi.refresh();

            ui.notify(null, Granite.I18n.get("The page has been unpublished"));
        }).fail(function(xhr) {
            var title = Granite.I18n.get("Error");
            var message = Granite.I18n.getVar($(xhr.responseText).find("#Message").html());
            if(xhr.status == 200 && !message){
            	ui.notify(null, Granite.I18n.get("The page has been unpublished"));
            }else{
            ui.alert(title, message, "error");
            }
        });

    }
    
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.publishToPreview",
        handler: function(name, el, config, collection, selections) {
            ui.wait();

            var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });

            if (!paths.length) return;
            var referencePromise = $.ajax({
                url: URITemplate.expand(config.data.referenceSrc, {
                    path: paths
                }),
                cache: false,
                dataType: "json"
            });

            referencePromise.done(function(json) {
                if (!(json.assets.length == 0)) {
                    // redirect to wizard
                    window.location.href = URITemplate.expand(config.data.wizardSrc, {
                        item: paths
                    });
                }
                var targettedObj = {};
                targettedObj["paths"] = paths;
                targettedObj["binpublishpreview"] = "false";
                // publish directly
                $.ajax({
                	url: COMMAND_URL,
                    async: false,
                    method: "POST",
                    data: JSON.stringify(targettedObj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                }).always(function() {
                    ui.clearWait();
                }).done(function() {
                    var contentApi = $(".foundation-content").adaptTo("foundation-content");
                    contentApi.refresh();

                    ui.notify(null, Granite.I18n.get("The page has been published"));
                }).fail(function(xhr) {
                    var title = Granite.I18n.get("Error");
                    var message = Granite.I18n.getVar($(xhr.responseText).find("#Message").html());
                    if(xhr.status == 200 && !message){
                    	ui.notify(null, Granite.I18n.get("The page has been published"));
                    }else{
                    ui.alert(title, message, "error");
                    }
                });
            });

            referencePromise.fail(function(xhr) {
                ui.clearWait();

                var title = Granite.I18n.get("Error");
                var message = Granite.I18n.get("Failed to retrieve references for selected items.");
                ui.alert(title, message, "error");
            });
        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.publishToProduction",
        handler: function(name, el, config, collection, selections) {
            ui.wait();

            var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });

            if (!paths.length) return;
            var referencePromise = $.ajax({
                url: URITemplate.expand(config.data.referenceSrc, {
                    path: paths
                }),
                cache: false,
                dataType: "json"
            });

            referencePromise.done(function(json) {
                if (!(json.assets.length == 0)) {
                    // redirect to wizard
                    window.location.href = URITemplate.expand(config.data.wizardSrc, {
                        item: paths
                    });
                }
                var targettedObj = {};
                targettedObj["paths"] = paths;
                targettedObj["binpublishpreview"] = "false";
                // publish directly
                $.ajax({
                	url: COMMAND_URL_PROD,
                    async: false,
                    method: "POST",
                    data: JSON.stringify(targettedObj),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8"
                }).always(function() {
                    ui.clearWait();
                }).done(function() {
                    var contentApi = $(".foundation-content").adaptTo("foundation-content");
                    contentApi.refresh();

                    ui.notify(null, Granite.I18n.get("The page has been published"));
                }).fail(function(xhr) {
                    var title = Granite.I18n.get("Error");
                    var message = Granite.I18n.getVar($(xhr.responseText).find("#Message").html());
                    if(xhr.status == 200 && !message){
                    	ui.notify(null, Granite.I18n.get("The page has been published"));
                    }else{
                    ui.alert(title, message, "error");
                    }
                });
            });

            referencePromise.fail(function(xhr) {
                ui.clearWait();

                var title = Granite.I18n.get("Error");
                var message = Granite.I18n.get("Failed to retrieve references for selected items.");
                ui.alert(title, message, "error");
            });
        }
    });
    /**
     * On content load, display a notification in case we have been redirect from
     * the publish or unpublish page wizard.
     */
    $(document).on("foundation-contentloaded", ".cq-siteadmin-admin-childpages", function(e) {
        var message = CUI.util.state.getSessionItem("cq-page-published-message");
        if (message) {
            CUI.util.state.removeSessionItem("cq-page-published-message");
            ui.notify(null, Granite.I18n.getVar(message));
        }
    });
    
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.unpublishFromPreview",
        handler: function(name, el, config, collection, selections) {
            ui.wait();

            var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });
            var countRefs = 0;
            var countChildren = 0;

            if (!paths.length) return;

            var referencePromise = $.ajax({
                url: URITemplate.expand(config.data.referenceSrc, {
                    path: paths,
                    predicate: "wcmcontent"
                }),
                cache: false,
                dataType: "json"
            });

            $.each(paths, function() {
                var path = this;
                if( countChildren > NUM_CHILDREN_CHECK ) {
                    return;
                }
                var childrenPromise = $.ajax({
                    url: "/bin/wcm/siteadmin/tree.json",
                    data: {
                        path: path,
                        ncc: NUM_CHILDREN_CHECK
                    },
                    cache: false,
                    dataType: "json"
                });

                childrenPromise.done(function(children) {
                    countChildren += children.length;
                    $.each(children, function() {
                        var child = this;
                        if(!child.leaf) {
                            countChildren += child.sub;
                        }
                    });
                }).fail(function(xhr) {
                    ui.clearWait();

                    var title = Granite.I18n.get("Error");
                    var message = Granite.I18n.get("Failed to retrieve child pages for selected items.");
                    ui.alert(title, message, "error");
                });
            });

            referencePromise.done(function(json) {
                if (json.pages.length == 0) {
                    // unpublish directly
                    unpublishFromPreview(paths);
                } else {
                    $.each(json.pages, function(i, value) {
                        if (json.pages[i].published && json.pages[i].isPage === "true" && json.pages[i].path !== json.pages[i].srcPath) {
                            countRefs++;
                        }
                    });
                }
            }).fail(function(xhr) {
                ui.clearWait();

                var title = Granite.I18n.get("Error");
                var message = Granite.I18n.get("Failed to retrieve references for selected items.");
                ui.alert(title, message, "error");
            }).always(function() {
                if (countRefs > 0 || countChildren > 0) {
                    ui.clearWait();

                    if(countChildren > NUM_CHILDREN_CHECK) {
                        countChildren = NUM_CHILDREN_CHECK + "+";
                    }
                    var messageRefs = countRefs === 0 ? "" : Granite.I18n.get("Selected items are referenced by {0} page(s).", countRefs);
                    var messageChildren  = countChildren === 0 ? "" : Granite.I18n.get("Upon unpublishing, other {0} child page(s) will get unpublished.", countChildren);
                    var message = messageRefs + " </br> " + messageChildren;
                    ui.prompt(Granite.I18n.get("Unpublish"), message, "notice", [{
                        text: Granite.I18n.get("Cancel")
                    }, {
                        text: Granite.I18n.get("Continue"),
                        warning: true,
                        handler: function() {
                            ui.wait();
                            unpublishFromPreview(paths);
                        }
                    }]);
                } else {
                    ui.wait();
                    unpublishFromPreview(paths);
                }
            });

        }
    });
    
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.unpublishFromProduction",
        handler: function(name, el, config, collection, selections) {
            ui.wait();

            var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });
            var countRefs = 0;
            var countChildren = 0;

            if (!paths.length) return;

            var referencePromise = $.ajax({
                url: URITemplate.expand(config.data.referenceSrc, {
                    path: paths,
                    predicate: "wcmcontent"
                }),
                cache: false,
                dataType: "json"
            });

            $.each(paths, function() {
                var path = this;
                if( countChildren > NUM_CHILDREN_CHECK ) {
                    return;
                }
                var childrenPromise = $.ajax({
                    url: "/bin/wcm/siteadmin/tree.json",
                    data: {
                        path: path,
                        ncc: NUM_CHILDREN_CHECK
                    },
                    cache: false,
                    dataType: "json"
                });

                childrenPromise.done(function(children) {
                    countChildren += children.length;
                    $.each(children, function() {
                        var child = this;
                        if(!child.leaf) {
                            countChildren += child.sub;
                        }
                    });
                }).fail(function(xhr) {
                    ui.clearWait();

                    var title = Granite.I18n.get("Error");
                    var message = Granite.I18n.get("Failed to retrieve child pages for selected items.");
                    ui.alert(title, message, "error");
                });
            });

            referencePromise.done(function(json) {
                if (json.pages.length == 0) {
                    // unpublish directly
                    unpublishFromProduction(paths);
                } else {
                    $.each(json.pages, function(i, value) {
                        if (json.pages[i].published && json.pages[i].isPage === "true" && json.pages[i].path !== json.pages[i].srcPath) {
                            countRefs++;
                        }
                    });
                }
            }).fail(function(xhr) {
                ui.clearWait();

                var title = Granite.I18n.get("Error");
                var message = Granite.I18n.get("Failed to retrieve references for selected items.");
                ui.alert(title, message, "error");
            }).always(function() {
                if (countRefs > 0 || countChildren > 0) {
                    ui.clearWait();

                    if(countChildren > NUM_CHILDREN_CHECK) {
                        countChildren = NUM_CHILDREN_CHECK + "+";
                    }
                    var messageRefs = countRefs === 0 ? "" : Granite.I18n.get("Selected items are referenced by {0} page(s).", countRefs);
                    var messageChildren  = countChildren === 0 ? "" : Granite.I18n.get("Upon unpublishing, other {0} child page(s) will get unpublished.", countChildren);
                    var message = messageRefs + " </br> " + messageChildren;
                    ui.prompt(Granite.I18n.get("Unpublish"), message, "notice", [{
                        text: Granite.I18n.get("Cancel")
                    }, {
                        text: Granite.I18n.get("Continue"),
                        warning: true,
                        handler: function() {
                            ui.wait();
                            unpublishFromProduction(paths);
                        }
                    }]);
                } else {
                    ui.wait();
                    unpublishFromProduction(paths);
                }
            });

        }
    });
}(window, document, Granite.$, Granite.URITemplate));