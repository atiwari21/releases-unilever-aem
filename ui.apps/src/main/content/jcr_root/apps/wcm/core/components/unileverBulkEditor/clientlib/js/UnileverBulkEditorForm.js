/**
 * @class UnileverBulkEditorForm
 * @extends CQ.Ext.form.FormPanel
 * The UnileverBulkEditorForm provides UnileverBulkEditor surrounded by a HTML form. This is the standalone
 * version of the UnileverBulkEditor, HTML form is required for import button.
 * @constructor
 * Creates a new UnileverBulkEditorForm
 * @param {Object} config The config of the bulk editor object
 */
var UnileverBulkEditorForm = UnileverBulkEditorForm || {};
UnileverBulkEditorForm = CQ.Ext.extend(CQ.Ext.form.FormPanel, {
    constructor: function(config) {
        config = (!config ? {} : config);

        //this.bulkEditor = new CQ.wcm.UnileverBulkEditor(config);
        config.xtype = "unileverbulkeditor";
        var renderTo = config["renderTo"];
        delete config["renderTo"];

        var defaults = {
            "renderTo": renderTo,
            //"region": "center",
            "items": [ config ],
            "hideBorders": true,
            "border": false,
            "stateful": false,
            "layout": "fit"
        };

        //CQ.Util.applyDefaults(config, defaults);

        // init component by calling super constructor
        UnileverBulkEditorForm.superclass.constructor.call(this, defaults);
    }
});

CQ.Ext.reg("unileverbulkeditorform", UnileverBulkEditorForm);