<%@ page import="com.day.cq.commons.servlets.HtmlStatusResponseHelper,
                 org.apache.sling.api.servlets.HtmlResponse,
                 java.util.Enumeration,
                 org.apache.sling.api.resource.Resource,
				 org.apache.commons.lang3.StringUtils,
                 javax.jcr.Node,
                 com.day.cq.commons.jcr.JcrUtil,
                 javax.jcr.Property,
				 javax.jcr.PropertyType,
                 javax.jcr.Session,
                 com.day.text.Text,
				 java.util.Calendar" %>
<%@ page session="false" %>
<%
%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0" %>
<%!
	boolean updateProperty(Property p, String value) throws Exception {
                     if (p != null) {
                         if (p.isMultiple()) {
                             String[] propertyValueArray = value.split(","); // Splits by comma and returns an array of strings
                             p.setValue(propertyValueArray);
                         } else {
                             p.setValue(value);
                         }
                         return true;
                     }
                     return false;
                 }
	boolean createNodeProperty(Node n, String propertyName, String value) throws Exception {
                    if (StringUtils.isNotBlank(value)) {
                        if (value.startsWith("{") && value.endsWith("}")) {
                            //Multivalue property. Store as String[]
                            String localValue = StringUtils.stripStart(value,"{");
                            localValue = StringUtils.stripEnd(localValue,"}");
                            String[] propertyValueArray = localValue.split(","); // TODO: Needs separate handling for type String[]
                            n.setProperty(propertyName,propertyValueArray,PropertyType.STRING);
                            setModifiedProperty(n);
                        } else {
                            n.setProperty(propertyName,value,PropertyType.STRING);
                            setModifiedProperty(n);
                        }

                        return true;
                    }
                    return false;
                 }
	void setModifiedProperty(Node n)  throws Exception {
		n.setProperty("cq:lastModified", Calendar.getInstance());
		n.setProperty("jcr:lastModified", Calendar.getInstance());
		n.setProperty("jcr:lastModifiedBy", n.getSession().getUserID());
		n.setProperty("cq:lastModifiedBy", n.getSession().getUserID());
	}

%>
<%
%><sling:defineObjects/><%
    HtmlResponse htmlResponse = null;
    try {
        Session session = resourceResolver.adaptTo(Session.class);
        boolean updated = false;
        Enumeration names = request.getParameterNames();
        while (names.hasMoreElements()) {
            try {
                String path = (String) names.nextElement();
                if( path.startsWith("/")) {
                    boolean isDelete = false;
                    if( path.indexOf("@Delete") != -1) {
                        isDelete = true;
                        path = path.substring(0, path.indexOf("@Delete"));
                    }

                    String value = request.getParameter(path);
                    Resource r = resourceResolver.getResource(path);

                    String propertyName = Text.getName(path);
                    if (r == null) {
                        //resource does not exist. 2 cases:
                        // - maybe it is a non existing property? property has to be created
                        // - maybe it is a new row? node has to be created first
                        String rowPath = Text.getRelativeParent(path, 1);
                        Resource rowResource = resourceResolver.getResource(rowPath);
                        if (rowResource != null) {
                            //add property to node
                            Node rowNode = rowResource.adaptTo(Node.class);
                            if(rowNode != null ) {
                                //rowNode.setProperty(propertyName,value);
                                //updated = true;
                                updated = createNodeProperty(rowNode, propertyName, value);
                            }
                        } else {
                            //create node and add property
                            String parentPath = Text.getRelativeParent(path, 2);
                            Resource parentResource = resourceResolver.getResource(parentPath);
                            if (parentResource != null) {
                                Node parentNode = parentResource.adaptTo(Node.class);
                                if (parentNode != null) {
                                    String nodeToCreateName = Text.getName(rowPath);
                                    Node rowNode = parentNode.addNode(nodeToCreateName);
                                    //rowNode.setProperty(propertyName,value);
                                    //updated = true;
                                    updated = createNodeProperty(rowNode, propertyName, value);
                                }
                            }
                        }
                    } else {
                        if( isDelete ) {
                            Node n = r.adaptTo(Node.class);
                            if( n != null) {
                                n.remove();
                                updated = true;
                            }
                        } else {
                            //path should already be the property path
                            Property p = r.adaptTo(Property.class);
                            //if (p != null) {
                            //p.setValue(value);
                            //updated = true;
                            //}
                            updated = updateProperty(p, value);
                            Node n = r.adaptTo(Node.class);
                            if( n != null && updated) {
                            	setModifiedProperty(n);
							}else{
								int pathWithoutProp = r.getPath().lastIndexOf("/");
								n = resourceResolver.getResource(r.getPath().substring(0, pathWithoutProp)).adaptTo(Node.class);
								if( n != null && updated) {
	                            	setModifiedProperty(n);
								}
							}
                        }
                    }
                }
            } catch (Exception e) {}
        }

        if (updated) {
            session.save();
        }

        htmlResponse = HtmlStatusResponseHelper.createStatusResponse(true,
                "Data saved");
    } catch (Exception e) {
        htmlResponse = HtmlStatusResponseHelper.createStatusResponse(false,
                "Error while saving data", e.getMessage());
    }

    htmlResponse.send(response, true);
%>
