CQ.Ext.ns("Unilever-IEA");

CQ.form.MultiFieldPanel = CQ.Ext.extend(CQ.Ext.Panel, {
    panelValue: '',

    constructor: function(config){
        config = config || {};
        CQ.form.MultiFieldPanel.superclass.constructor.call(this, config);
    },

    initComponent: function () {
        CQ.form.MultiFieldPanel.superclass.initComponent.call(this);

        this.panelValue = new CQ.Ext.form.Hidden({
            name: this.name
        });

        this.add(this.panelValue);

        var dialog = this.findParentByType('dialog');

        dialog.on('beforesubmit', function(){
            var value = this.getValue();

            if(value){
                this.panelValue.setValue(value);
            }
        },this);

    },

    afterRender : function(){
        CQ.form.MultiFieldPanel.superclass.afterRender.call(this);

        this.items.each(function(){
            if(!this.contentBasedOptionsURL
                    || this.contentBasedOptionsURL.indexOf(CQ.form.Selection.PATH_PLACEHOLDER) < 0){
                return;
            }

            this.processPath(this.findParentByType('dialog').path);
        })
    },

    getValue: function () {
        var pData = {};

        this.items.each(function(i){
            if(i.xtype == "label" || i.xtype == "hidden" || !i.hasOwnProperty("dName")){

                return;
            }

            pData[i.dName] = i.getValue();
        });
		var jsonString = $.isEmptyObject(pData) ? "" : JSON.stringify(pData)
        return cleanupJson(jsonString);
    },

    setValue: function (value) {
        value = prepareJson(value);
        this.panelValue.setValue(value);
        var pData = JSON.parse(value);

        this.items.each(function(i){

            if(i.xtype == "label" || i.xtype == "hidden" || !i.hasOwnProperty("dName")){
                return;
            }
           	i.setValue(pData[i.dName]);
        });
    },

    validate: function(){
        return true;
    },

    getName: function(){
        return this.name;
    }
});

function prepareJson(str) {

    if(typeof str =='object')
    {
      str = JSON.stringify(str);
    }

    if(str.indexOf("[\"")==-1 && str.startsWith('[')) {
		str= str.replace(/\"/g,"\\\"").replace("[","[\"").replace("]","\"]");
    } 
    return str;
}

function cleanupJson(str) {
	if(str.indexOf("[\"")!=-1 && str.indexOf("[]")==-1) {
        str= str.replace("[\"","[").replace("\"]","]").replace(/\\"/g,'"').replace(/}","{/g,'},{');
    } 

  //  console.log("string before--"+str);

    return str;
}

CQ.Ext.reg("uni-multi-field", CQ.form.MultiFieldPanel);