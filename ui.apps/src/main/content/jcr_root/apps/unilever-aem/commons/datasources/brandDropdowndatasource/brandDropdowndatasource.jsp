<%@include file="/libs/foundation/global.jsp"%>
<%@page contentType="text/html"%>
<%@page import="java.util.Map"%>
<%@page import="org.osgi.service.cm.ConfigurationAdmin"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.apache.sling.api.resource.ResourceResolver"%>
<%@page import="com.day.cq.wcm.api.PageManager"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="org.osgi.service.cm.Configuration"%>
<%@page import="com.day.cq.wcm.api.Page"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.day.cq.wcm.api.PageFilter"%>
<%@page import="java.util.Locale"%>
<%@page import="com.adobe.cq.commerce.common.ValueMapDecorator"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.adobe.granite.ui.components.ds.ValueMapResource"%>
<%@page import="org.apache.sling.api.resource.ValueMap"%>
<%@page import="com.adobe.granite.ui.components.ds.SimpleDataSource"%>
<%@page import="com.adobe.granite.ui.components.ds.DataSource"%>
<%@page import="org.apache.sling.api.resource.ResourceMetadata"%>
<%@page import="org.apache.jackrabbit.api.security.user.UserManager"%>
<%@page import="org.apache.jackrabbit.api.security.user.Group"%>
<%@page import="org.apache.jackrabbit.api.security.user.Authorizable"%>
<%@page import="javax.jcr.RepositoryException"%>
<%@page import="javax.jcr.Session"%>
<%@page import="org.apache.commons.lang.StringUtils"%>



<%

List<String> brandNameList = new ArrayList<String>();
List<String> accessibleGroups = new ArrayList<String>();
List<String> brands = new ArrayList<String>();

List<String> avaliableGroups = new ArrayList<String>();

ConfigurationAdmin configAdmin=sling.getService(ConfigurationAdmin.class);
List<Resource> fakeResourceList = new ArrayList<Resource>();

UserManager userManager = resourceResolver.adaptTo(UserManager.class);
Session session = resourceResolver.adaptTo(Session.class);
try {
    Authorizable auth = userManager.getAuthorizable(session.getUserID());
    Iterator<Group> groups = auth.memberOf();
    while(groups.hasNext())
    {

        Group group=(Group)groups.next();
        accessibleGroups.add(group.getID());

    }   
} catch (RepositoryException e) {
    e.printStackTrace();
}

ValueMap vm = null;
String[] tenantInfo = null;
String env = StringUtils.EMPTY;
try {
if (configAdmin != null) {
    Configuration tenantConf = configAdmin.getConfiguration("com.sapient.platform.iea.aem.core.filter.TenantComputationFilter");
	Configuration envConf = configAdmin.getConfiguration("com.unilever.platform.aem.foundation.core.service.impl.MarketWorkflowConfigImpl");

    if (tenantConf != null && tenantConf.getProperties() != null && tenantConf.getProperties().get("validSiteBase") != null ) {
        tenantInfo = (String[])tenantConf.getProperties().get("validSiteBase");
    }
    else{
         tenantInfo = null;
    }
    if (envConf != null && envConf.getProperties() != null && envConf.getProperties().get("envName") != null ) {
        env = (String)envConf.getProperties().get("envName");
    }
    else{
        env = StringUtils.EMPTY;
    }
}
}

catch (IOException e) {
		out.print("Configuration for pid com.sapient.platform.iea.aem.core.filter.TenantComputationFilter is Null");
        }
//env = "qa";


if(tenantInfo!=null){
for(int j = 0; j< tenantInfo.length; j++){
            String brandContentPath = tenantInfo[j];
            String[] brandPathSplit = brandContentPath.split("/");
            if("content".equals(brandPathSplit[1])){
                brandNameList.add(brandContentPath);
            }
        }
}


for(int k=0; k<brandNameList.size(); k++){
    String[] brandPathSplit = brandNameList.get(k).split("/");
    String brand = brandPathSplit[2].toUpperCase();
    ValueMap vm1 = null;
    Page rootPage = pageManager.getPage(brandNameList.get(k));
    if (rootPage != null) {
        Iterator<Page> childPages = rootPage.listChildren(new PageFilter(request));


        boolean duplicateFound = false;


        while (childPages.hasNext()) {
            vm = new ValueMapDecorator(new HashMap<String, Object>());
            duplicateFound = false;
            Page child = childPages.next();

            String childPath = child.getPath();
            String localeTitlePath = childPath + "/jcr:content";
            Resource pageResource = resourceResolver.getResource(localeTitlePath);
            if (pageResource != null) {
                ValueMap childProperties = pageResource.adaptTo(ValueMap.class);
                String pageTitle = childProperties.get("jcr:title", String.class);
                Locale locale = child.getLanguage(false);
                for(int i = 0; i < fakeResourceList.size(); i++){
                    vm1 = fakeResourceList.get(i).adaptTo(ValueMap.class);
                    if(vm1.get("value").equals(brand+"-"+locale)){
                        duplicateFound = true;
                    }
                }
               for(int z=0; z<accessibleGroups.size(); z++){
                if((locale != null) && !(duplicateFound) && (!("Global").equalsIgnoreCase(pageTitle)) && (brand+"_"+locale+"_"+env+"_"+"author").equalsIgnoreCase(accessibleGroups.get(z))){

                vm.put("value", brand+"-"+locale);
                vm.put("text", brand+"-"+pageTitle);

                fakeResourceList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));
                }
               }
            }
        }

    }

}

DataSource ds = new SimpleDataSource(fakeResourceList.iterator());
    request.setAttribute(DataSource.class.getName(), ds);

    %>

