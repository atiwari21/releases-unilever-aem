<%--
  ADOBE CONFIDENTIAL

  Copyright 2013 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Field,
                  com.adobe.granite.ui.components.Tag" %>
<%@page import="com.unilever.platform.foundation.components.helper.ComponentUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%><%

	
	String projectPath = request.getParameter("item") + "/jcr:content";

    Resource  currentResource=resourceResolver.getResource(projectPath);
	String[] brandsConfigurationArr= ComponentUtil.getPropertyValueArray(currentResource.getValueMap(), "brandsConfiguration");
	String dueDate = "";

	for(String brandsConfiguration:brandsConfigurationArr){
		String brandNames = "";
		JSONObject obj=new JSONObject(brandsConfiguration);
		dueDate=(String)obj.get("dueDate");
		JSONArray multipleBrands=(JSONArray)obj.get("multipleBrands");
		for(int j=0; j<multipleBrands.length();j++){
			JSONObject brandNameObj = (JSONObject)multipleBrands.get(j);
			String brandName = (String)brandNameObj.get("brandName");

			String[] brandNameArray = (String[])brandName.split("\\-");

			String resourcePath = "/content/"+brandNameArray[0].toLowerCase()+"/"+brandNameArray[1].toLowerCase()+"/jcr:content";

			Resource  brandResource=resourceResolver.getResource(resourcePath);
			if(brandResource!=null){
			ValueMap brandResourceProperties = brandResource.adaptTo(ValueMap.class);
			String brandTitle = brandResourceProperties.get("jcr:title", "");
			brandNames = brandNames +  " , " + brandNameArray[0]+"-"+brandTitle;
			}

		}
		String market = cmp.getValue().val(cmp.getExpressionHelper().getString(" Market " + brandNames));
		String dueDateValue = cmp.getValue().val(cmp.getExpressionHelper().getString(" Due Date: " + dueDate ));
		%>
		<div><b>Markets Data:</b>
		</br>
		<%= xssAPI.encodeForHTML(market) %></br>
		<%= xssAPI.encodeForHTML(dueDateValue) %></br></div>
		</br>
		<%

	} 



%>