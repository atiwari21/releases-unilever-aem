<%@page import="java.util.Map"%>
<%@page session="false" import="com.unilever.platform.aem.foundation.configuration.ConfigurationService,
    com.day.cq.wcm.api.Page,java.util.Map,org.apache.commons.collections.MapUtils"%><%--**********************************************************************
  *
  * ADOBE CONFIDENTIAL
  * __________________
  *
  *  Copyright 2012 Adobe Systems Incorporated
  *  All Rights Reserved.
  *
  * NOTICE:  All information contained herein is, and remains
  * the property of Adobe Systems Incorporated and its suppliers,
  * if any.  The intellectual and technical concepts contained
  * herein are proprietary to Adobe Systems Incorporated and its
  * suppliers and are protected by trade secret or copyright law.
  * Dissemination of this information or reproduction of this material
  * is strictly forbidden unless prior written permission is obtained
  * from Adobe Systems Incorporated.
  **********************************************************************--%><%
%><%@page contentType="text/javascript" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%
    String serviceURL = properties.get("serviceURL", currentStyle.get("serviceURL", String.class));
    ConfigurationService globalConfigService=sling.getService(ConfigurationService.class);
    String pagePath=slingRequest.getParameter("path");
    Page domainPage=slingRequest.getResourceResolver().getResource(pagePath).adaptTo(Page.class);
    Map<String,String> clientContextConfig=globalConfigService.getCategoryConfiguration(domainPage,"clientContext");
    Map<String,String> locationStoreConfig=globalConfigService.getCategoryConfiguration(domainPage,"locationstore");
    String brand= MapUtils.getString(clientContextConfig, "brand", "");
    String locale= MapUtils.getString(clientContextConfig, "locale", "");
    String entity= MapUtils.getString(locationStoreConfig, "entity", "location");
    String googleMapsUrl=MapUtils.getString(clientContextConfig, "googleMapsUrl", "https://www.googleapis.com/geolocation/v1/geolocate");
    String googleAPIKey= MapUtils.getString(clientContextConfig, "googleAPIKey", "AIzaSyDSCVKiPNzEYMFUu7BYAYmkeg-XDG0Hsjg");
    String locationProperty= globalConfigService.getConfigValue(domainPage, "locationstore", "properties");
    %>

var locationBrand = "<%= brand %>";
var locationLocale = "<%= locale %>";
var locationEntity = "<%= entity %>";
var locationStoreName = "locationstore";

var store = CQ_Analytics.StoreRegistry.getStore(locationStoreName);
if (!store) {
	var googleMapsUrl = "<%= googleMapsUrl %>";
	var googleAPIKey = "<%= googleAPIKey %>";
	var locationProperty = "<%= locationProperty %>";
	var locationProperties = locationProperty.split(",");
	var locationServiceURL = "<%= serviceURL %>"

    locationServiceURL = locationServiceURL.replace("{brand}", locationBrand);
	locationServiceURL = locationServiceURL.replace("{entity}", locationEntity);
    locationServiceURL = locationServiceURL.replace("{locale}", locationLocale);
	if (navigator.geolocation) {

		navigator.geolocation.getCurrentPosition(showPosition, failPosition());

	}
	function showPosition(position) {
		locationServiceURL = locationServiceURL.replace("{lng}", position.coords.longitude);
		locationServiceURL = locationServiceURL.replace("{lat}", position.coords.latitude);

		CQ_Analytics.JSONPStore.registerNewInstance(locationStoreName, locationServiceURL, {});

	};
	function failPosition() {
        var googleAPIUrl=googleMapsUrl+"?key="+googleAPIKey;
		$.post(googleAPIUrl, "", function (result) {
locationServiceURL = locationServiceURL.replace("{lng}",result.location.lng);
locationServiceURL=locationServiceURL.replace("{lat}",result.location.lat);
			var storecreated = CQ_Analytics.JSONPStore.registerNewInstance(locationStoreName, locationServiceURL, {});
		}, "json");
	};

}
