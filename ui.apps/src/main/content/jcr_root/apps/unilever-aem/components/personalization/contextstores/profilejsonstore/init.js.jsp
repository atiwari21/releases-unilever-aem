<%@page session="false" import="com.unilever.platform.aem.foundation.configuration.ConfigurationService,
    com.day.cq.wcm.api.Page,java.util.Map,org.apache.commons.collections.MapUtils"%><%--**********************************************************************
  *
  * ADOBE CONFIDENTIAL
  * __________________
  *
  *  Copyright 2012 Adobe Systems Incorporated
  *  All Rights Reserved.
  *
  * NOTICE:  All information contained herein is, and remains
  * the property of Adobe Systems Incorporated and its suppliers,
  * if any.  The intellectual and technical concepts contained
  * herein are proprietary to Adobe Systems Incorporated and its
  * suppliers and are protected by trade secret or copyright law.
  * Dissemination of this information or reproduction of this material
  * is strictly forbidden unless prior written permission is obtained
  * from Adobe Systems Incorporated.
  **********************************************************************--%><%
%><%@page contentType="text/javascript" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%
    String serviceURL = properties.get("serviceURL", currentStyle.get("serviceURL", String.class));
    
    %>

var unileverprofilestore = "unileverprofilestore";

var store = CQ_Analytics.StoreRegistry.getStore(unileverprofilestore);
if (!store) {
var profileServiceURL = "<%= serviceURL %>";
CQ_Analytics.JSONPStore.registerNewInstance(unileverprofilestore, profileServiceURL, {});
}
