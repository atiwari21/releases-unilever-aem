<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@include file="/libs/foundation/global.jsp" %>
<%@page import="java.util.HashMap,java.util.LinkedHashMap,java.util.Map,
org.apache.commons.collections.MapUtils,org.apache.commons.lang.StringUtils,org.json.JSONObject,org.slf4j.Logger,org.slf4j.LoggerFactory,
com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache,com.sapient.platform.iea.aem.core.constants.CommonConstants,com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper,com.unilever.platform.aem.foundation.configuration.ConfigurationConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%!
public static String[] getPropertyValueArray(ValueMap properties, String propertyKey) {
		String[] propertyValObj = properties.get(propertyKey, String[].class);
		String[] propertyValArr = null;
		if (propertyValObj != null) {
			if (propertyValObj instanceof String[]) {
				propertyValArr = (String[]) propertyValObj;
			} else {
				propertyValArr = new String[] { propertyValObj.toString() };
			}
		}
		return propertyValArr;
	}
%>
<%
String cat=properties.get("category")!=null?properties.get("category").toString():"";
String[] configurationArr=properties.get("configuration")!=null?getPropertyValueArray(properties, "configuration"):null;
		
		Map<String,String> catConfigurationMap = new LinkedHashMap<String,String>();
		if(configurationArr!=null){
			for(String configuration:configurationArr){			
				JSONObject configurationJSON=new JSONObject(configuration);	
				String key=configurationJSON.getString(ConfigurationConstants.KEY_PROPERTY_NAME);
				String configKeyType = StringUtils.contains(configurationJSON.toString(), "categoryType")? configurationJSON.getString("categoryType"):"";
				if(StringUtils.isBlank(configKeyType)){
				    configKeyType = "STRING";
				}
				String value="RICHTEXT".equals(configKeyType)?configurationJSON.getString("text"):configurationJSON.getString(ConfigurationConstants.VALUE_PROPERTY_NAME);
				catConfigurationMap.put(key+","+configKeyType, value);
			}
		}
		request.setAttribute("category", cat);
		request.setAttribute("categoryMap", catConfigurationMap);
%>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<div>
<h3>Category: ${category}</h3>
    <table>
  <tr>
    <th>Key</th>
    <th>Value</th>
    <th>Type</th>
  </tr>
<c:forEach var="categoryKey" items="${categoryMap}">
<c:set var="categorySplit" value="${fn:split(categoryKey.key, ',')}" />
  <tr>
    <td>${categorySplit[0]}</td>
    <td>${categoryKey.value}</td>
    <td>${categorySplit[1]}</td>
  </tr>
</c:forEach>
</table>
</div>