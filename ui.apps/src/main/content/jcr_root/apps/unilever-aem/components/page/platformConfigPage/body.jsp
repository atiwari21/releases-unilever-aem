<%@ include file="/apps/unilever-iea/commons/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils" %>

<c:set var="componentName" value="${component.name}" />
<c:set var="wrapperClass" value="en-flexible" />
<c:set var="wrapperClassAdaptive" value="en-flexible navOpen" />
<c:if test="${not empty properties.wrapperClass}">
    <c:set var="wrapperClass" value="${properties.wrapperClass}" />
</c:if>

<c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] == 'EDIT'}">
	<c:set var="ediModeClass" value=" wcmmode-edit "/>
</c:if>

<c:set var="bodyClass" value=""/>
<c:if test="${not empty properties.bodyClass}">
    <c:set var="bodyClass" value="${properties.bodyClass} "/>
</c:if>
<!--[if IE 7 ]><body class="ie ie7 lt-ie8 lt-ie9 lt-ie10 no-mq ${bodyClass} ${ediModeClass}"><![endif]-->
<!--[if IE 8 ]><body class="ie ie8 lt-ie9 lt-ie10 no-mq ${bodyClass} ${ediModeClass}"><![endif]-->
<!--[if IE 9 ]><body class="ie ie9 lt-ie10 js ${bodyClass} ${ediModeClass}"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<body class="${bodyClass}${ediModeClass} js">
<!--<![endif]-->

	<div id="wrapper" class="${wrapperClass}">
		<c:if test="${not properties.hideHeader}">
            <cq:include script="header.jsp" />
        </c:if>
        <cq:include script="content.jsp" />
     </div>


	<c:if test="${isAdaptive == 'false'}">                
        <cq:include script="includeJS.jsp" />
	</c:if>
</body>