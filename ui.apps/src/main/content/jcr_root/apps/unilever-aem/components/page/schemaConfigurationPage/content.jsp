<%@include file="/libs/foundation/global.jsp" %>
<%@page import="java.util.Locale,java.util.ResourceBundle,com.day.cq.i18n.I18n"
%>
<cq:defineObjects />
<cq:setContentBundle />
<c:set var="count" value="0" scope="request" />
<c:set var="title" value="${currentPage.title}" />
<section class="en-hero">

    <%
    Locale pageLocale = currentPage.getLanguage(false);
//If above bool is set to true. CQ looks in to page path rather than jcr:language property.
    ResourceBundle resourceBundle = slingRequest.getResourceBundle(pageLocale);
    I18n i18n = new I18n(resourceBundle);
    String parentPath = "/aem/products.html";   
    if(currentPage.getPath().contains("/schema")){
        parentPath = parentPath + currentPage.getPath().substring(0,currentPage.getPath().lastIndexOf("/schema"));
    }else{
        parentPath="#";
    }
%>
	<style>
        .container.schema-conf-container{
             width: 90% !important;
        }
                table.schema{
                    width: 100%;
                    table-layout: fixed;
                }
		input[type="number"],
        input[type="text"]{
            width: 100%;
        }
    </style>

    <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
       && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
           <h1>
               <fmt:message key="contentSection" />
           </h1>
    </c:if>
    <div class="container schema-conf-container">
        <div class="row">
            <div class="col-md-12 no-gutter">
                <form action="<%= currentPage.getPath()+".schemaconfiguration.html?wcmmode=disabled"%>" method="POST">
					<table border="1" class="schema">
    <tr>
        <td width="250">
            <%=i18n.get("schemaConfiguration.table.pimFieldDescription")%>
        </td>
        <td width="250">
            <%=i18n.get("schemaConfiguration.table.pimField")%>
        </td>
        <td width="250">
            <%=i18n.get("schemaConfiguration.table.description")%>
        </td>
        <td width="75">
            <%=i18n.get("schemaConfiguration.table.column")%>
        </td>        
        <td width="75">
            <%=i18n.get("schemaConfiguration.table.dataApplicableBrand")%>
		</td>
		<td width="75">
             <%=i18n.get("schemaConfiguration.table.mandatory")%>
		</td>
		<td width="75">
            <%=i18n.get("schemaConfiguration.table.mandatoryVariant")%>
		</td>
		<td width="75">
            <%=i18n.get("schemaConfiguration.table.mandatoryDefaultValue")%>
		</td>	
        <td width="75">
            <%=i18n.get("schemaConfiguration.table.uniqueValues")%>
		</td>
		<td width="75">
            <%=i18n.get("schemaConfiguration.table.dataMustContain")%>
		</td>
		<td width="75">
            <%=i18n.get("schemaConfiguration.table.minLength")%>
		</td>
		<td width="75">
            <%=i18n.get("schemaConfiguration.table.maxLength")%>
		</td>
		<td width="75">
            <%=i18n.get("schemaConfiguration.table.readOnly")%>

		</td>
		<td width="75">
            <%=i18n.get("schemaConfiguration.table.regex")%>
		</td>
	</tr>
					</table>
                    
                    <cq:include path="par" resourceType="foundation/components/parsys"/>
                    <input type="hidden" name="count" value="${count}">
                    <input type="submit" value="Submit"/>
                    <input type="button" id="cancelSchema" value="Cancel" onclick="location.href='<%= currentPage.getPath()%>.html?wcmmode=disabled'"/>
                    <input type="button" id="back" value="Back" onclick="location.href='<%=parentPath%>'"/>
                </form>
            </div>
        </div>
    </div>
</section>
<%@include file="/apps/iea/commons/clientlibs.jsp"%>
<c:set var="includeCategory" value="css" scope="request" />
<cq:include path="includeLib" resourceType="iea/components/includeClientLib" />
<c:set var="includeCategory" value="js" scope="request" />
<cq:include path="includeLib" resourceType="iea/components/includeClientLib" />
<cq:includeClientLib js="granite.csrf.standalone"/>
