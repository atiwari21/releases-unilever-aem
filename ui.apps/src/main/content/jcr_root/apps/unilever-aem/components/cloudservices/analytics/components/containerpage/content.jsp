<%@page contentType="text/html"
            pageEncoding="utf-8"%><%
%><%@include file="/libs/foundation/global.jsp"%><div>
<cq:includeClientLib categories="iea.widgets"/>
<div>
    <h3>container tag Settings</h3>   
    <ul>
        <li><div class="li-bullet"><strong>Uat Domain </strong><br><%= xssAPI.encodeForHTML(properties.get("nonprod_domain", "")) %></div></li>
        <li><div class="li-bullet"><strong>Prod Domain </strong><br><%= xssAPI.encodeForHTML(properties.get("prod_domain", "")) %></div></li>
    </ul>
</div>
<cq:include path="analyticConfig" resourceType="unilever-aem/components/content/analyticConfig"/>