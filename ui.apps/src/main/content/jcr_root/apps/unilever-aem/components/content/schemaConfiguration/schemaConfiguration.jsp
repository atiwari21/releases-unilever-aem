<%@include file="/libs/foundation/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils,
				javax.jcr.Node,
				org.apache.sling.api.SlingHttpServletRequest,
				org.apache.commons.collections.MapUtils,
				org.apache.commons.lang.StringUtils"
%>

<cq:defineObjects />
<cq:setContentBundle />

<%
	Object parentProductSchema = currentPage.getProperties().get("parentProductSchemaPath");
	String parentProductSchemaPath = parentProductSchema != null ? parentProductSchema.toString() : "";
	String parentPageTemplatePath = "";
	String componentPath = resource.getPath();
	String componentAbsolutePath = componentPath.substring(currentPage.getPath().length() + 1);
	Node parentCompNode = null;
	String path = "";

	Boolean parentPageExist = false;
	Boolean enabledOnParentPage = false;

	Page parentPage = null;

	if(StringUtils.isNotBlank(parentProductSchemaPath)){
		parentPage = pageManager.getPage(parentProductSchemaPath);
    }else{
		Resource rs = currentPage.getContentResource();
        Resource rs1 = rs.getChild("cq:LiveSyncConfig");
        if(rs1 != null){
            path = rs1.getValueMap().get("cq:master").toString();
            parentPage = currentPage.getPageManager().getPage(path);
        }
    }

	if(parentPage != null){
        parentPageTemplatePath = parentPage.getTemplate().getPath();        
	}

	if(parentPageTemplatePath.equals("/apps/unilever-aem/templates/schemaConfigurationPageTemplate")){
		parentPageExist = true;
        parentCompNode = resourceResolver.getResource(parentPage.getPath() + "/" + componentAbsolutePath).adaptTo(Node.class);
    }

	if(parentCompNode != null && parentCompNode.hasProperty("dataApplicableBrand")){
		enabledOnParentPage = Boolean.parseBoolean(parentCompNode.getProperty("dataApplicableBrand").getValue().toString());
    }
%>
<c:set var="count" value="${count + 1}" scope="request"/>
<c:set var="componentPath" value="<%= componentPath %>" />
<c:set var="parentPageExist" value="<%= parentPageExist %>" />
<c:set var="enabledOnParentPage" value="<%= enabledOnParentPage %>" />
<c:if test="${count <= 1}"> 
</c:if>    
<c:choose>
    <c:when test="${parentPageExist}">        
        <c:if test="${enabledOnParentPage}">            
            <input type="hidden" name="componentPath${count}" value="${componentPath}"/>
            <cq:include script="schemaConfigForm.jsp" />
        </c:if>       
    </c:when>    
    <c:otherwise>        
        <input type="hidden" name="componentPath${count}" value="${componentPath}"/>
		<cq:include script="schemaConfigForm.jsp" />
    </c:otherwise>
</c:choose>