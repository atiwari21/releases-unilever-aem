<%@include file="/libs/foundation/global.jsp" %>
<%@page import="com.sapient.platform.iea.aem.utils.ClientLibUtils,
				javax.jcr.Node,
				org.apache.sling.api.SlingHttpServletRequest,
				org.apache.commons.collections.MapUtils,
				org.apache.commons.lang.StringUtils"
%>
<cq:defineObjects />
<cq:setContentBundle />

<table border="1" class="schema">
	<tr>
        <td width="250">
            <input type="text" name="pimFieldDescription${count}" value="${properties.pimFieldDescription}" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
        </td>
        <td width="250">
            <input type="text" name="pimField${count}" value="${properties.pimField}" size="15" readOnly <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
        </td>
        <td width="250">
            <input type="text" name="description${count}" value="${properties.description}" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
        </td>
        <td width="75">
            <input type="text" name="column${count}" value="${properties.column}" size="4" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
        </td>        
        <td width="75" align="center">
            <input type="checkbox" name="dataApplicableBrand${count}" value="${properties.dataApplicableBrand}" <c:if test="${properties.dataApplicableBrand eq 'true'}">checked</c:if> <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
		</td>
		<td width="75" align="center">
            <input type="checkbox" name="mandatory${count}" value="${properties.mandatory}" <c:if test="${properties.mandatory eq 'true'}">checked</c:if> <c:if test="${properties.readOnly eq 'true'}">disabled</c:if> />
		</td>
		<td width="75" align="center">
            <input type="checkbox" name="mandatoryVariant${count}" value="${properties.mandatoryVariant}" <c:if test="${properties.mandatoryVariant eq 'true'}">checked</c:if> <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
		</td>
		<td width="75">
            <input type="text" name="mandatoryDefaultValue${count}" value="${properties.mandatoryDefaultValue}" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
		</td>
	    <td width="75" align="center">
            <input type="checkbox" name="uniqueValues${count}" value="${properties.uniqueValues}" <c:if test="${properties.uniqueValues eq 'true'}">checked</c:if> <c:if test="${properties.readOnly eq 'true'}">disabled</c:if> />
		</td>
		<td width="75">
            <input type="text" name="dataMustContain${count}" value="${properties.dataMustContain}" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
		</td>
		<td width="75">
            <input type="text" name="minLength${count}" value="${properties.minLength}" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if> />
		</td>
		<td width="75">
            <input type="text" name="maxLength${count}" value="${properties.maxLength}" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if> />
		</td>
		<td width="75" align="center">
            <input type="checkbox" name="readOnly${count}" value="${properties.readOnly}" <c:if test="${properties.readOnly eq 'true'}">checked</c:if> <c:if test="${properties.readOnly eq 'true'}">disabled</c:if> />
		</td>
		<td width="75">
            <input type="text" name="regex${count}" value="${properties.regex}" <c:if test="${properties.readOnly eq 'true'}">disabled</c:if>/>
		</td>
	</tr>
</table>