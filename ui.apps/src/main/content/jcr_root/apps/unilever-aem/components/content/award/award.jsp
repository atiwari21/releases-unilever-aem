<%@include file="/libs/foundation/global.jsp" %>
<%@page import="java.util.*,java.util.Date,java.text.SimpleDateFormat,java.util.HashMap,
java.util.LinkedHashMap,java.util.Map,org.apache.commons.collections.MapUtils,org.apache.commons.lang.StringUtils,
org.json.JSONObject,org.slf4j.Logger,org.slf4j.LoggerFactory,com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache,
com.sapient.platform.iea.aem.core.constants.CommonConstants,com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper,
com.unilever.platform.aem.foundation.configuration.ConfigurationConstants,com.day.cq.wcm.foundation.Image,com.day.cq.commons.Doctype,
com.day.cq.tagging.Tag,com.day.cq.tagging.TagManager,com.unilever.platform.aem.foundation.commerce.api.UNIProduct,
com.unilever.platform.foundation.components.helper.ProductHelper,com.day.cq.wcm.api.PageManager"%>
<%!
public static final String RESOURCE_TYPE = "sling:resourceType";
public static final String RESOURCE_TYPE_VALUE = "/apps/unilever-iea/components/product";
public static final String NAME = "name";
public static final String PRODUCT_PATH = "productPath";

public static String[] getPropertyValueArray(ValueMap properties, String propertyKey) {
	String[] propertyValObj = properties.get(propertyKey, String[].class);
	String[] propertyValArr = null;
	if (propertyValObj != null) {
		if (propertyValObj instanceof String[]) {
			propertyValArr = (String[]) propertyValObj;
		} else {
			propertyValArr = new String[] { propertyValObj.toString() };
		}
	}
	return propertyValArr;
}
%>
<%
SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
SimpleDateFormat stringDateformatter = new SimpleDateFormat("dd-MM-yyyy");
Map<String, Object> awardDetailsMap = new LinkedHashMap<String, Object>();
StringBuilder productString = new StringBuilder();
String imageAvailable = "false";
String[] configurationArr=properties.get("product")!=null?getPropertyValueArray(properties, "product"):null;
Map<String, Map<String,String>> configurationMap = new LinkedHashMap<String, Map<String,String>>();
Map<String,String> catConfigurationMap = new LinkedHashMap<String,String>();
if(configurationArr!=null){
		UNIProduct product = null;
		for(int i =0 ; i<configurationArr.length;i++){
			JSONObject configurationJSON=new JSONObject(configurationArr[i]);	
			String productPagePath=configurationJSON.getString(PRODUCT_PATH);
				if (StringUtils.isNotEmpty(productPagePath)) {
					product = ProductHelper.getUniProduct(pageManager.getPage(productPagePath));
					if(null != product){
						if (configurationArr.length - 1 == i) {
							productString.append(product.getName());
						} else {
							productString.append(product.getName() + ", ");
						}
                    }
				}
	}
	awardDetailsMap.put("associatedProducts", productString.toString());
	}

if(null != properties.get("type")){
	awardDetailsMap.put("awardType", properties.get("type"));
}
TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
StringBuilder tagString = new StringBuilder();
if(null != properties.get("cq:tags")){
	String[] tagsObj = (String[]) properties.get("cq:tags");
	List<Object> tagList = new ArrayList<Object>();
	for(int i =0 ; i<tagsObj.length;i++){
		Tag tag = tagManager.resolve(tagsObj[i]);
		if (tagsObj.length - 1 == i) {
			tagString.append(tag.getTitle());
		} else {
			tagString.append(tag.getTitle() + ", ");
		}
		tagList.add(tag.getTitle());
	}
	awardDetailsMap.put("associatedTags", tagString.toString());
}
if(null != properties.get("name")){
	awardDetailsMap.put("name", properties.get("name"));
}
if(null != properties.get("awardYear")){
	awardDetailsMap.put("awardYear", properties.get("awardYear"));
}
if(null != properties.get("altText")){
	awardDetailsMap.put("altText", properties.get("altText"));
}
if(null != properties.get("ctaText")){
	awardDetailsMap.put("ctaText", properties.get("ctaText"));
}
if(null != properties.get("description")){
	awardDetailsMap.put("description", properties.get("description"));
}
if(null != properties.get("openInNewWindow")){
	awardDetailsMap.put("openInNewWindow", properties.get("openInNewWindow"));
	if(properties.get("openInNewWindow").equals("true"))
	{
		awardDetailsMap.put("targetValue", "_blank");
	}else{
		awardDetailsMap.put("targetValue", "#");
	}
}
if(null != properties.get("image")){
	awardDetailsMap.put("image", properties.get("image"));
}
if(null != properties.get("url")){
	awardDetailsMap.put("url", properties.get("url"));
}
if(null != properties.get("startDate")){
	Date date = properties.get("startDate", Date.class);
    String startDateProp = properties.get("startDate", String.class);
    if(date != null){		
		awardDetailsMap.put("startDate", sdf.format(date));
    }else if(startDateProp != ""){
		date = stringDateformatter.parse(startDateProp);
        if(date != null){            
			awardDetailsMap.put("startDate", stringDateformatter.format(date));
        }        
    }
}
if(null != properties.get("endDate")){
	Date date = properties.get("endDate", Date.class);
    String endDateProp = properties.get("endDate", String.class);
	String awardExpired = "false";

    if(date != null){
		awardDetailsMap.put("endDate", sdf.format(date));
    }else if(endDateProp != ""){
		date = stringDateformatter.parse(endDateProp);
        if(date != null){				
			awardDetailsMap.put("endDate", stringDateformatter.format(date));
        }        
    }

    if(date != null){
    	Date currentDate = new java.util.Date();
		int comparison = date.compareTo(currentDate);
		awardExpired = comparison < 0 ? "true" : "false";
    }

	awardDetailsMap.put("isAwardExpired", awardExpired);
}

String imgPath = (String)properties.get("image");
Resource imgRes = resourceResolver.getResource(imgPath);
Image img = null;
if (imgRes != null) {
	img = new Image(imgRes);
	img.setDoctype(Doctype.fromRequest(request));
	img.setItemName(Image.NN_FILE, ".");
	img.setSelector("img");
	img.setNoPlaceholder(false);
	imageAvailable = "true";
	Long width = new Long(326);
	if (width != null) {
		img.set(Image.PN_HTML_WIDTH, String.valueOf(width));
	}
	Long height = new Long(230);
	if (height != null) {
		img.set(Image.PN_HTML_HEIGHT, String.valueOf(height));
	}
}
%>
<c:set var="awardName" value="<%= awardDetailsMap.get("name") %>" />
<c:set var="awardYear" value="<%= awardDetailsMap.get("awardYear") %>" />
<c:set var="awardType" value="<%= awardDetailsMap.get("awardType") %>" />
<c:set var="startDate" value="<%= awardDetailsMap.get("startDate") %>" />
<c:set var="endDate" value="<%= awardDetailsMap.get("endDate") %>" />
<c:set var="associatedTags" value="<%= awardDetailsMap.get("associatedTags") %>" />
<c:set var="associatedProducts" value="<%= awardDetailsMap.get("associatedProducts") %>" />
<c:set var="awardDescription" value="<%= awardDetailsMap.get("description") %>" />
<c:set var="awardUrl" value="<%= awardDetailsMap.get("url") %>" />
<c:set var="ctaText" value="<%= awardDetailsMap.get("ctaText") %>" />
<c:set var="expiryAlertText" value="Award Expired on " />
<c:set var="tagsLabel" value="Tags:" />
<c:set var="productsLabel" value="Products:" />
<c:set var="isAwardExpired" value="<%= awardDetailsMap.get("isAwardExpired") %>" />
<c:set var="targetValue" value="<%= awardDetailsMap.get("targetValue") %>" />
<c:set var="imageAvailable" value="<%= imageAvailable %>" />
<c:choose>
<c:when test="${imageAvailable eq 'true'}">
   <c:set var="width" value="width: 50%;" />
</c:when>
<c:otherwise>
	<c:set var="width" value="" />
</c:otherwise>
</c:choose>
<div class="c-award" style="overflow: hidden; direction:initial">
	<div class="c-award__exp-msg" style="background-color: black; padding: 1px 1px;"></div>
	<c:if test="${isAwardExpired eq 'true'}">
		<div class="c-award__exp-msg" style="background-color: #E4E4E4; padding: 10px 20px;">${expiryAlertText} ${endDate}</div>
	</c:if>
	<c:if test="${imageAvailable eq 'true'}">
		<div class="c-award__img" style="float: left; ${width} padding: 15px; box-sizing: border-box;">
			<% img.draw(out); %>
		</div>
	</c:if>
	<div class="c-award__content" style="float: left; ${width} font-size: 16px; padding: 15px; box-sizing: border-box;">
		<c:if test="${not empty awardName}">
			<h3 class="c-award__name" style="font-size: 20px; font-weight: bold; margin-bottom: 15px;">${awardName}</h3>
		</c:if>
		<div class="c-award__type" style="font-size: 20px; font-weight: bold; margin-bottom: 10px;">${awardYear} (${awardType})</div>
		<c:choose>
			<c:when test="${not empty endDate}">
				<div class="c-award__duration" style="margin-bottom: 15px;">${startDate} - ${endDate}</div>
			</c:when>
			<c:otherwise>
				<div class="c-award__duration" style="margin-bottom: 15px;">${startDate}</div>
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty awardDescription}">
			<div class="c-award__desc" style="margin-bottom: 20px;">${awardDescription}</div>
		</c:if>
		<c:if test="${not empty associatedTags}">
			<div class="c-award__tags" style="margin-bottom: 20px; overflow: hidden;">
				<div class="c-award__tags--label" style="float: left; padding-right: 10px; font-weight: bold;">${tagsLabel}</div>
				<div class="c-award__tags--list" style="float: left;">${associatedTags}</div>
			</div>
		</c:if>
		<c:if test="${not empty associatedProducts}">
			<div class="c-award__products" style="margin-bottom: 30px; overflow: hidden;">
				<div class="c-award__products--label" style="float: left; padding-right: 10px; font-weight: bold;">${productsLabel}</div>
				<div class="c-award__products--list" style="float: left;">${associatedProducts}</div>
			</div>
		</c:if>
		<c:if test="${not empty awardUrl}">
			<a href="${awardUrl}" target="${targetValue}" class="c-award__details" style="border: solid 1px #5a5a5a; color: #5a5a5a; background-color: #fff; padding: 0px 20px; line-height: 35px; outline: none; display: inline-block; text-decoration: none;">${ctaText}</a>
		</c:if>
	</div>
</div>