(function (document, $, ns) {
    "use strict";

    $(document).on("click", ".cq-dialog-submit", function (e) {
        var $form = $(this).closest("form.foundation-form"),
            formName = $form.find("div.cq-dialog-content").attr('data-formName');

        if(formName == "award"){
            var tags = $form.find("[name='./cq:tags']").val(),
        		startDate = new Date($form.find("[name='./startDate']").val()),
        		endDate = new Date($form.find("[name='./endDate']").val());

        	if(!tags){
           		$(window).adaptTo("foundation-ui").alert("Validation Failed", "Tags field is Mandatory, please fill it.");
       			e.preventDefault();
        	}
        	if(endDate.getTime() <= startDate.getTime()){
            	$(window).adaptTo("foundation-ui").alert("Validation Failed", "Start Date should be less than End Date, please correct it.");
       			e.preventDefault();
        	}
        }
    });
})(document, Granite.$, Granite.author);