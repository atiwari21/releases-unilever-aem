<%@include file="/libs/foundation/global.jsp" %>
<%@page import="java.util.HashMap,java.util.LinkedHashMap,java.util.Map,
org.apache.commons.collections.MapUtils,org.apache.commons.lang.StringUtils,org.json.*,org.slf4j.Logger,org.slf4j.LoggerFactory,
com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache,com.sapient.platform.iea.aem.core.constants.CommonConstants,com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper,com.unilever.platform.aem.foundation.configuration.ConfigurationConstants"%>
<%!
public static String[] getPropertyValueArray(ValueMap properties, String propertyKey) {
		String[] propertyValObj = properties.get(propertyKey, String[].class);
		String[] propertyValArr = null;
		if (propertyValObj != null) {
			if (propertyValObj instanceof String[]) {
				propertyValArr = (String[]) propertyValObj;
			} else {
				propertyValArr = new String[] { propertyValObj.toString() };
			}
		}
		return propertyValArr;
	}
%>
<%

String[] configurationArr=properties.get("configuration")!=null?getPropertyValueArray(properties, "configuration"):null;
		
		Map<String, Map<String,String>> configurationMap = new LinkedHashMap<String, Map<String,String>>();

		if(configurationArr!=null){
			for(String configuration:configurationArr){			
				JSONObject configurationJSON=new JSONObject(configuration);	
                Map<String,String> catConfigurationMap = new LinkedHashMap<String,String>();
                String cat=configurationJSON.getString("category");
                JSONArray categoryConfiguration=configurationJSON.getJSONArray("categoryConfiguration");
                for(int i=0;i<categoryConfiguration.length();++i){
					JSONObject obj=categoryConfiguration.getJSONObject(i);
                    String key=obj.getString(ConfigurationConstants.KEY_PROPERTY_NAME);
					String value=obj.getString(ConfigurationConstants.VALUE_PROPERTY_NAME);
                    catConfigurationMap.put(key, value);
                }

				configurationMap.put(cat,catConfigurationMap);
			}
		}


%>
<div style="height:100px;"><%=configurationMap%></div>