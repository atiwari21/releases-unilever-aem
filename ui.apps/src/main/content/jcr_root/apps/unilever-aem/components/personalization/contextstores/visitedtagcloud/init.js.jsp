<%@page session="false"%><%@ page import="com.day.cq.commons.TidyJSONWriter,
                 org.apache.sling.commons.json.io.JSONWriter,
                 com.day.cq.tagging.Tag,
				 org.apache.commons.lang.StringUtils,				 
				 java.net.URL,				 
                 java.io.StringWriter"
        contentType="text/javascript" %><%!
%><%@ include file="/libs/foundation/global.jsp" %><%
    StringWriter buf1 = new StringWriter();
	String parentPagePath = null;
	String actualPagePath = null;
    TidyJSONWriter writer = new TidyJSONWriter(buf1);
    writer.setTidy(true);
	Page cPage = null;
	Resource r = null;
    try {
        if (request.getParameter("parentPagePath") != null) {
			parentPagePath = request.getParameter("parentPagePath");
		    if(StringUtils.isNotBlank(parentPagePath)){            	
				actualPagePath = new URL(parentPagePath).getPath();                	
               	r = resourceResolver.resolve(actualPagePath);                	

        	}                    
            cPage = (r != null ? r.adaptTo(Page.class) : null);
        }

        writer.array();
        if(cPage != null){
            for (Tag tag : cPage.getTags()) {
                writer.value(tag.getTagID());
            }
        }
        writer.endArray();
    } catch (Exception e) {
        log.error("Error while generating JSON tagcloud initial data", e);
    }
%>
if( CQ_Analytics && CQ_Analytics.VisitedTagCloudMgr) {	
    CQ_Analytics.VisitedTagCloudMgr.init(<%=buf1%>);

}