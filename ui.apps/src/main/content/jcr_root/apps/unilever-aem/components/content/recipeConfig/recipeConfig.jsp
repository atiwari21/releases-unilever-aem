<%@include file="/libs/foundation/global.jsp" %>
<%@page import="java.util.*,java.util.Date,java.text.SimpleDateFormat,java.util.HashMap,java.util.LinkedHashMap,java.util.Map,
org.apache.commons.collections.MapUtils,org.apache.commons.lang.StringUtils,org.json.JSONObject,org.slf4j.Logger,org.slf4j.LoggerFactory,
com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache,com.sapient.platform.iea.aem.core.constants.CommonConstants,com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper,com.unilever.platform.aem.foundation.configuration.ConfigurationConstants"%>
<%!

%>
<%	
	Map<String, Object> awardDetailsMap = new LinkedHashMap<String, Object>();	
	if(null != properties.get("recipeLandingPagePath")){
       	awardDetailsMap.put("recipeLandingPagePath", properties.get("recipeLandingPagePath"));
    }	
	if(null != properties.get("recipeDetailTemplatePagePath")){
        awardDetailsMap.put("recipeDetailTemplatePagePath", properties.get("recipeDetailTemplatePagePath"));
    }
	if(null != properties.get("cq:tags")){
		String[] tagsObj = (String[]) properties.get("cq:tags",String[].class);
        List<Object> tagList = new ArrayList<Object>();
        for(int i =0 ; i<tagsObj.length;i++){
        	tagList.add(tagsObj[i]);
        }
       	awardDetailsMap.put("associatedTags", tagList);
    }
%>
<div style="height:100px;"><%=awardDetailsMap%></div>