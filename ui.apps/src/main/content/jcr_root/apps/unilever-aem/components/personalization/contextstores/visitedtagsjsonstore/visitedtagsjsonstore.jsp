<%@page session="false" 
		import="com.day.cq.wcm.api.Page,
    			org.apache.commons.lang.StringUtils,
				java.util.Locale,				
				java.util.ResourceBundle,
				com.day.cq.i18n.I18n"%><%!
%><%@include file="/libs/foundation/global.jsp"%><%!
%><%@taglib prefix="personalization" uri="http://www.day.com/taglibs/cq/personalization/1.0" %>
<cq:setContentBundle/>
<%
Locale pageLocale = currentPage.getLanguage(false);
ResourceBundle resourceBundle = slingRequest.getResourceBundle(pageLocale);
I18n i18n = new I18n(resourceBundle);
%>
<div class="cq-cc-store">	
    Visited Tag JSON Store	
	<div class="cq-cc-content">
        	<script type="text/javascript">
            	$CQ(function() {					
                	var store = CQ_Analytics.StoreRegistry.getStore("visitedtags");
                	if(!store) {
                    	CQ_Analytics.JSONPStore.registerNewInstance("visitedtags", "", {});
                	}
            	});
        	</script>
        <div class="cq-cc-store-property cq-cc-store-property-level1%>">
            <personalization:storePropertyTag propertyName="tags" store="visitedtags"/>
        </div>
    </div>
    <div class="cq-cc-clear"></div>
</div>