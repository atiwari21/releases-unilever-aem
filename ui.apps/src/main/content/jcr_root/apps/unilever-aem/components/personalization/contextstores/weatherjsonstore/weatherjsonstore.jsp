<%@page session="false" import="com.unilever.platform.aem.foundation.configuration.ConfigurationService,
    com.day.cq.wcm.api.Page,org.apache.commons.lang.StringUtils"%>
	<%@page import="java.util.Locale,java.util.ResourceBundle,com.day.cq.i18n.I18n"%><%--
  ************************************************************************
  ADOBE CONFIDENTIAL
  ___________________

  Copyright 2011 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
  ************************************************************************
  --%><%!
%><%@include file="/libs/foundation/global.jsp"%><%!
%><%@taglib prefix="personalization" uri="http://www.day.com/taglibs/cq/personalization/1.0" %>
<cq:setContentBundle/>
<%
Locale pageLocale = currentPage.getLanguage(false);
ResourceBundle resourceBundle = slingRequest.getResourceBundle(pageLocale);
I18n i18n = new I18n(resourceBundle);
%>
<div class="cq-cc-store"><%
    String store = properties.get("store", currentStyle.get("store",String.class));
    String serviceURL = properties.get("serviceURL", currentStyle.get("serviceURL",String.class));
    //TODO: thumbnail code is copied at 3 different places. Generalize
    String thumbnail = properties.get("thumbnail",String.class);
    boolean hasThumbnail = thumbnail != null;
    String thumbnailDynamicValue = null;
    String thumbnailProperty = thumbnail;
	// Display the defaultPlaceholderText
	if(true){%>
    <%=i18n.get("weatherstore.defaultPlaceholderText") %>  <%}%><%
    if( hasThumbnail ) {
        %><div class="cq-cc-thumbnail"><%
            if( isImage(thumbnail) || isURL(thumbnail)) {
                thumbnailProperty = "generatedThumbnail";
                if( isImage(thumbnail) ) {
                    thumbnailDynamicValue = request.getContextPath() + thumbnail;
                } else {
                    thumbnailDynamicValue = thumbnail;
                }
            }
            %><div class="cq-cc-store-property"><personalization:storePropertyTag propertyName="<%=thumbnailProperty%>" store="weatherstore"/></div><%
        %></div><%
    }

    %><div class="cq-cc-content"><%

    if( store != null && serviceURL != null ) {
        %><script type="text/javascript">
            $CQ(function() {
                var store = CQ_Analytics.StoreRegistry.getStore("locationstore");
                if( store ) {
                    store.addInitProperty("<%=thumbnailProperty%>", "<%=thumbnailDynamicValue%>");
                    store.setProperty("<%=thumbnailProperty%>", "<%=thumbnailDynamicValue%>");
                } else {
                    CQ_Analytics.JSONPStore.registerNewInstance("weatherstore", "<%=serviceURL%>", {
                        <% if( thumbnailDynamicValue != null ) { %>
                            "<%=thumbnailProperty%>": "<%=thumbnailDynamicValue%>"
                        <% } %>
                    });
                }
            });
        </script><%
    }
    String pagePath=slingRequest.getParameter("path");
    String[] props=null;
    if(StringUtils.isNotEmpty(pagePath)){
    Page domainPage=slingRequest.getResourceResolver().getResource(pagePath).adaptTo(Page.class);
    ConfigurationService globalConfigService=sling.getService(ConfigurationService.class);
    String locationProperty= globalConfigService.getConfigValue(domainPage, "weatherstore", "properties");
    props=locationProperty.split(",");
    }


//Object[] props = null;

    //props = props != null ? props : currentStyle.get("properties",String[].class);
    if(props != null ) {
        int i = 0;
        for(String prop: props) {
            %><div class="cq-cc-store-property cq-cc-store-property-level<%=(i++)%>"><personalization:storePropertyTag propertyName="<%=prop%>" store="weatherstore"/></div><%
        }
    }
  %></div>
    <div class="cq-cc-clear"></div>
</div><%!

    boolean isImage(String value) {
        return value != null && (value.toLowerCase().contains(".png")
                                        || value.toLowerCase().contains(".jpg")
                                        || value.toLowerCase().contains(".gif"));
    }

    boolean isURL(String value) {
        return value != null && value.indexOf("://") != -1;
    }
%>
