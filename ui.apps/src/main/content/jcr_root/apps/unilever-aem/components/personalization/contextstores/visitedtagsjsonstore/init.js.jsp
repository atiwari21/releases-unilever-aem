<%@page session="false" import="com.day.cq.wcm.api.Page,
    com.day.cq.tagging.Tag,
	org.apache.commons.lang.StringUtils,		
	java.net.URL"%><%
%><%@page contentType="text/javascript" %><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%
	String parentPagePath = null;
	String actualPagePath = null;
	String tagsLine = null;
    Page cPage = null;
	Resource r = null;	
    try {
        if (request.getParameter("parentPagePath") != null) {
			parentPagePath = request.getParameter("parentPagePath");
		    if(StringUtils.isNotBlank(parentPagePath)){            	
				actualPagePath = new URL(parentPagePath).getPath();                	
               	r = resourceResolver.resolve(actualPagePath);                	
        	}                    
            cPage = (r != null ? r.adaptTo(Page.class) : null);
        }        
        if(cPage != null){
            for (Tag tag : cPage.getTags()) {
                if(tagsLine != null){
                    tagsLine = tagsLine + "," + tag.getTitle();
                }else{
                    tagsLine = tag.getTitle();
                }
            }			
        }        
    } catch (Exception e) {
        log.error("Error while generating JSON tagcloud initial data", e);
    }
%>
var visitedTagStoreName = "visitedtags";
var store = CQ_Analytics.StoreRegistry.getStore(visitedTagStoreName);
if (store) {	
	store.setProperty("tags", "<%=tagsLine%>");    
}else{
	var newStore = CQ_Analytics.JSONPStore.registerNewInstance(visitedTagStoreName, '', {});
	if(newStore){		
		newStore.setProperty("tags", "<%=tagsLine%>");
	}
}