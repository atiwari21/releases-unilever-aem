if( window.CQ_Analytics
    && window.CQ_Analytics.ClientContextUI ) {
    $CQ(function() {
        CQ_Analytics.ClientContextUtils.onStoreRegistered("visitedtagcloud", function(sessionstore) {
            sessionstore.addListener("render",function(event, store, divId){
                $CQ("#" + divId).bind("dblclick",function(event) {
                    if( !store.editDialog) {
                        store.editDialog = CQ.WCM.getDialog(
                            "/apps/unilever-aem/components/personalization/contextstores/" +
                            store.getName()+
                            "/edit_dialog.xml");
                    }

                    if( store.editDialog ) {
                        store.editDialog.show();
                    }

                    event.preventDefault();
                });
            });
        });
    });
}