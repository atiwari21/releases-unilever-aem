<%--  
 <content.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================
   Flexible Layout Template
   A template which provides the flexibility to provide various layouts.
  ==============================================================================

--%>

<%@ include file="/libs/foundation/global.jsp" %>
<cq:setContentBundle />

<c:set var="title" value="${currentPage.title}" />
     <section class="en-hero">
         <c:if test="${requestScope['com.day.cq.wcm.api.WCMMode'] !='DISABLED' 
       && requestScope['com.day.cq.wcm.api.WCMMode'] !='PREVIEW'}">
        <h1>
            <fmt:message key="contentSection" />
        </h1>
    	</c:if>
		<div class="container">
			<div class="row">
			<div class="col-md-12 no-gutter">
				<cq:include path="par" resourceType="foundation/components/parsys"/>
            </div>
		</div>
		</div>
    </section>

    
	<%@include file="/apps/iea/commons/clientlibs.jsp"%>

	<c:set var="includeCategory" value="css" scope="request" />
    <cq:include path="includeLib"
                resourceType="iea/components/includeClientLib" />

    <c:set var="includeCategory" value="js" scope="request" />
    <cq:include path="includeLib"
            resourceType="iea/components/includeClientLib" />
