<%--
  ADOBE CONFIDENTIAL

  Copyright 2013 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%@page session="false"
                import="java.util.ArrayList,
                  java.util.HashMap,
                  java.util.Map,
				  java.util.Iterator,
                  org.apache.sling.api.wrappers.ValueMapDecorator,
                  com.adobe.granite.ui.components.ds.DataSource,
                  com.adobe.granite.ui.components.ds.AbstractDataSource,
                  com.adobe.granite.ui.components.ds.EmptyDataSource,
                  com.adobe.granite.ui.components.ds.SimpleDataSource,
                  com.adobe.granite.ui.components.ds.ValueMapResource,
				  org.apache.sling.api.resource.ResourceMetadata,
				  org.apache.commons.lang.StringUtils,
				  com.adobe.cq.commerce.pim.api.ProductServicesManager,
				  com.adobe.cq.commerce.pim.api.ProductImporter" %><%
%><%@include file="/libs/foundation/global.jsp"%><%

    ArrayList<Resource> resourceList = new ArrayList<Resource>();

	Page categoryParentPage = pageManager.getPage("/content/productSchemaCategories");
	if(categoryParentPage != null){
		Iterator<Page> pageIterator = categoryParentPage.listChildren();

        Map<String, Object> mapInitial = new HashMap<String, Object>();            

        mapInitial.put("text", "Select Category");
        mapInitial.put("value", "");

        ValueMapResource syntheticResourceInitial = new ValueMapResource(resourceResolver, new ResourceMetadata(), "", new ValueMapDecorator(mapInitial));
        resourceList.add(syntheticResourceInitial);

    	while (pageIterator.hasNext()){
			Map<String, Object> map = new HashMap<String, Object>();
            Page childPage = pageIterator.next();

        	map.put("text", childPage.getTitle());
        	map.put("value", childPage.getPath());

            ValueMapResource syntheticResource = new ValueMapResource(resourceResolver, new ResourceMetadata(), "", new ValueMapDecorator(map));
        	resourceList.add(syntheticResource);
    	}        
	}

    DataSource ds;

    // if no matching nodes where found
    if (resourceList.size() == 0){
        // return empty datasource
        ds = EmptyDataSource.instance();
    } else {
        // create a new datasource object
        ds = new SimpleDataSource(resourceList.iterator());
    }

    // place it in request for consumption by datasource mechanism
    request.setAttribute(DataSource.class.getName(), ds);
%>