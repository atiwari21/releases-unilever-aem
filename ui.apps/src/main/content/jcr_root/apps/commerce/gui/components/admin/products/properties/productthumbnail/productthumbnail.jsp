<%--
  ADOBE CONFIDENTIAL

  Copyright 2013 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%
%><%@include file="/libs/granite/ui/global.jsp" %><%
%><%@page session="false"
          import="java.util.Map,
                  java.util.Iterator,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.Tag,
                  com.day.cq.commons.jcr.JcrConstants,
                  com.day.cq.wcm.api.Page,
                  com.day.cq.wcm.api.PageManager,
                  com.adobe.cq.commerce.api.Product,
                  com.adobe.cq.commerce.api.asset.ProductAssetManager,
                  com.adobe.cq.commerce.common.CommerceHelper, org.apache.commons.lang.StringUtils" %><%

    Config cfg = cmp.getConfig();

    Resource itemRes = resourceResolver.getResource(slingRequest.getRequestPathInfo().getSuffix());
    if (itemRes == null) {
        itemRes = resourceResolver.getResource(request.getParameter("item"));
    }
    if (itemRes == null) {
        return;
    }

    PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
    Product product = itemRes.adaptTo(Product.class);
    String title = CommerceHelper.getCardTitle(itemRes, pageManager);

    // compute the 'name' attribute
    String name = cfg.get("name", "");
    Map<String,String> templateProperties = (Map<String,String>) request.getAttribute("cq-template-properties");
    if (templateProperties != null) {
        Iterator it = templateProperties.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String,String> pair = (Map.Entry)it.next();
            if (name.contains(pair.getKey())) {
                name = name.replace(pair.getKey(), pair.getValue());
            }
        }
    }

    // thumbnailUrl

    String imagePath = null;
    if (request.getParameter("imagePath") != null) {
        imagePath = request.getParameter("imagePath");
    }
    if (imagePath == null) {
        imagePath = (String) request.getAttribute("cq-product-image-path");
    }
    if (imagePath == null) {
        return;
    }

    ProductAssetManager productAssetManager = resourceResolver.adaptTo(ProductAssetManager.class);
    String assetThumbnailUrl = productAssetManager.getThumbnailUrl(imagePath, "319.319");
    if (StringUtils.isEmpty(assetThumbnailUrl)) {
        return;
    }

    // add the context path
    assetThumbnailUrl = request.getContextPath() + assetThumbnailUrl;

    // add the cache killer
    Resource productAssetRes = resourceResolver.resolve(imagePath);
    ValueMap productAssetProps = productAssetRes.adaptTo(ValueMap.class);
    long ck = productAssetProps.get(JcrConstants.JCR_LASTMODIFIED, (long) 0);
    assetThumbnailUrl = assetThumbnailUrl + "?cq_ck=" + ck;

    Tag tag = cmp.consumeTag();
    AttrBuilder attrs = tag.getAttrs();
    attrs.addClass("cq-wcm-pagethumbnail");
    attrs.add("data-cq-wcm-pagethumbnail-path", product.getPath());

    AttrBuilder imageAttrs = new AttrBuilder(request, xssAPI);
    imageAttrs.addClass(cfg.get("class", String.class));
    imageAttrs.add("src", xssAPI.getValidHref(assetThumbnailUrl));
    imageAttrs.add("alt", i18n.get("Product thumbnail"));

%><div <%= attrs.build() %>>
    <div class="foundation-layout-thumbnail coral-Form-field">
        <div class="foundation-layout-thumbnail-image grid">
            <article class="card-page">
                <a>
                    <span class="image"><img <%= imageAttrs.build() %>/></span>
                    <div class="label">
                        <h4><%= xssAPI.filterHTML(title) %></h4>
                        <p class="description"><%= xssAPI.filterHTML(product.getDescription()) %></p>
                    </div>
                </a>
            </article>
        </div>
    </div>



    <sling:include path="<%= resource.getPath() + "/assetpicker" %>" />
    <sling:include path="<%= resource.getPath() + "/altText" %>" />
    <sling:include path="<%= resource.getPath() + "/videoId" %>" />

    <div class="thumbnail-buttons">
        <div class="thumbnail-button">
            <div class="foundation-field-editable foundation-layout-util-vmargin">
                <div class="foundation-field-edit">
                    <sling:include path="<%= resource.getPath() + "/remove" %>" />
                </div>
            </div>
        </div>
    </div>
    <div class="foundation-field-editable foundation-layout-util-vmargin">
        <div class="foundation-field-edit product-image-separator"></div>
    </div>
</div>


