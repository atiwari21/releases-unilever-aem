/*
 * ADOBE CONFIDENTIAL
 *
 * Copyright 2012 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 */
(function(window, document, Granite, $, URITemplate) {
	"use strict";

	/**
	 * Returns the suffix from the given href url
	 */
	function getSuffix(href) {
		var lastDot = href.lastIndexOf("."), suffixStart = href.indexOf("/",
				lastDot);

		if (suffixStart >= 0) {
			return href.substr(suffixStart)
		} else {
			return "/content/catalogs";
		}
	}

	$(window).adaptTo("foundation-registry").register(
			"foundation.collection.action.action",
			{
				name : "cq.commerce.rolloutsection",
				handler : function(name, el, config, collection, selections) {
					if (!selections.length)
						return;

					var url = URITemplate.expand(config.data.href, {
						suffix : getSuffix(Granite.HTTP.getPath()),
						item : selections.map(function(item) {
							return $(item)
									.data("foundation-collection-item-id");
						})
					});
					var encodedUrlString = url.split("?");
					var encodedUrlFirstString = encodedUrlString[0];
					if (encodedUrlFirstString.indexOf("%2F") >= 0
							&& url.indexOf("_charset") >= 0
							&& url.indexOf("&item") >= 0) {
						var redirectUrl = encodedUrlFirstString.replace(/%2F/g,
								'/')
								+ '?' + encodedUrlString[1];

					}
					window.location = redirectUrl;

				}
			});

})(window, document, Granite, Granite.$, Granite.URITemplate);
