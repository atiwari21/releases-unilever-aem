<%
%><%@page session="false" contentType="text/html; charset=utf-8" %><%
%><%@page import="org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ValueMap,
                  com.adobe.granite.ui.components.AttrBuilder,
                  com.adobe.granite.ui.components.ComponentHelper,
                  com.adobe.granite.ui.components.Config,
                  org.json.JSONObject,
                  org.apache.jackrabbit.api.security.user.*,
                  com.adobe.granite.ui.components.Tag,
                  com.adobe.granite.ui.components.Value,
                  com.adobe.granite.ui.components.LayoutBuilder,
                  java.util.HashMap,
                  org.apache.commons.lang3.StringUtils,
                  java.util.Iterator,
                  java.util.Collection,
                  java.util.List,
                  java.util.Map,
                  com.day.cq.i18n.I18n,
                  javax.jcr.Node,
                  javax.jcr.Session,
                  com.unilever.platform.aem.foundation.utils.PIMInjestionUtility,
                  com.unilever.platform.aem.foundation.integration.dto.SchemaConfigurationDTO,
                  com.day.cq.wcm.core.utils.ScaffoldingUtils" %><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0" %><%
%><cq:defineObjects /><%

    ComponentHelper cmp = new ComponentHelper(pageContext);
    Config cfg = cmp.getConfig();

    String targetPath = (String) request.getAttribute(Value.CONTENTPATH_ATTRIBUTE);
    if (targetPath == null) {
        targetPath = slingRequest.getRequestPathInfo().getSuffix();
    }
    Resource target = resourceResolver.getResource(targetPath);
    if (target == null) {
        return;
    }
Node productNode = target.adaptTo(Node.class);


String productType=productNode.getProperty("type").getString();

    ValueMap targetProps = target.adaptTo(ValueMap.class);
    String scaffoldPath = targetProps.get("cq:scaffolding", String.class);

    if (scaffoldPath == null) {
        // search all scaffolds for a path match
        Resource scRoot = resourceResolver.getResource("/etc/scaffolding");
        Node root = scRoot == null ? null : scRoot.adaptTo(Node.class);
        if (root != null) {
            scaffoldPath = ScaffoldingUtils.findScaffoldByPath(root, targetPath);
        }
    }

    Resource scaffold = null;
    if (scaffoldPath != null) {
        scaffold = resourceResolver.getResource(scaffoldPath);
        scaffold = scaffold != null ? scaffold.getChild("jcr:content/cq:dialog") : null;
    }

    I18n i18n = new I18n(request);

    Tag tag = cmp.consumeTag();
    tag.setName("div");

    AttrBuilder attrs = tag.getAttrs();

    attrs.add("id", cfg.get("id", String.class));
    attrs.addRel(cfg.get("rel", String.class));
    attrs.addClass(cfg.get("class", String.class));
    attrs.add("title", i18n.getVar(cfg.get("title", String.class)));

    attrs.addOthers(cfg.getProperties(), "id", "rel", "class", "title");

    tag.printlnStart(out);


String[] parts = targetPath.split("\\/"); // String array, each element is text between dots

String localePath=  "";
String parentPath = "";

if(!parts[4].equals("unilever"))
{	
	localePath=  "/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5];
}else{
    localePath=  "/"+parts[1]+"/"+parts[2]+"/"+parts[3]+"/"+parts[4]+"/"+parts[5]+"/"+parts[6];
}

Resource locale=resourceResolver.getResource(localePath);
Node localeNode = locale.adaptTo(Node.class);

if(localeNode.hasProperty("schemaPath")){
    parentPath=localeNode.getProperty("schemaPath").getString() + "/jcr:content/par";
}else{
    parentPath= localePath + "/schema/jcr:content/par";
}

Resource parent = resourceResolver.getResource(parentPath);
JSONObject jsonData = new JSONObject();
String[] mapping=PIMInjestionUtility.getScaffoldingColumnMapping();
String joinedstring = StringUtils.join(mapping, "=");
String[] pimfields=joinedstring.split("=");
Map<String,String> pimhashmap = new HashMap<String,String>();
String locked="";
String lockOwner="";

Boolean hideEdit=false;

    Session session = resourceResolver.adaptTo(Session.class);
    UserManager userManager = resourceResolver.adaptTo(UserManager.class);

    String currentUser=session.getUserID()+"";
if(localeNode.hasProperty("locked"))
{
    locked=localeNode.getProperty("locked").getString();
}
if(localeNode.hasProperty("lockOwner"))

{
    lockOwner=localeNode.getProperty("lockOwner").getString();

}


for(int y=0;y<pimfields.length-1;y++)
{
    if(y%2==0)
    {
pimhashmap.put(pimfields[y],pimfields[y+1]);
    }
}

if(parent!=null)

{
Map<String, SchemaConfigurationDTO> mapRes = new HashMap<String, SchemaConfigurationDTO>();
PIMInjestionUtility.getPIMSchemaConfiguration(parent,mapRes);


Collection<SchemaConfigurationDTO> values = mapRes.values();
SchemaConfigurationDTO[] targetArray = values.toArray(new SchemaConfigurationDTO[values.size()]);



for(int i=0;i<targetArray.length;i++)
{
 Map<String,String> prop=new HashMap<String,String>();
prop.put("regex",targetArray[i].getRegex());
prop.put("maxLength",targetArray[i].getMaxLength());
prop.put("minLength",targetArray[i].getMinLength());
prop.put("mandatory",targetArray[i].getMandatory());
prop.put("isApplicable",targetArray[i].getIsApplicable());
prop.put("fieldName",targetArray[i].getFieldName().trim());  
prop.put("readOnly",targetArray[i].getReadOnly());
prop.put("newName",(String)pimhashmap.get(prop.get("fieldName")));
prop.put("mandatoryVariant",targetArray[i].getMandatoryVariant());
jsonData.put(i+"",prop);
}

}

if(localeNode.hasProperty("locked"))
{

if(currentUser.equals(lockOwner))
{
    if (scaffold != null) {
        %><sling:include path="<%= scaffold.getPath() + "/content" %>" resourceType="granite/ui/components/foundation/contsys" /><%
    } else {
        %><h3 class="u-cq-wizardError"><%= i18n.get("No scaffold definition found for {0}.", "targetPath inserted", targetPath) %></h3><%
    }
    localeNode.getProperty("locked").remove();
    localeNode.getProperty("lockOwner").remove();
    localeNode.save();

}
    else
    {hideEdit=true;
%><h3 class="u-cq-wizardError"><%= i18n.get("Product Information is Locked By {0}.", "", lockOwner) %></h3><%

    }





}

else
{
if (scaffold != null) {
        %><sling:include path="<%= scaffold.getPath() + "/content" %>" resourceType="granite/ui/components/foundation/contsys" /><%
    } else {
        %><h3 class="u-cq-wizardError"><%= i18n.get("No scaffold definition found for {0}.", "targetPath inserted", targetPath) %></h3><%
    }
}

tag.printlnEnd(out);

%>


<script type="text/javascript">

var obj=<%=jsonData %>;

var productType="<%=productType%>";



if(<%=hideEdit%>==true)
{
document.getElementsByClassName('endor-ActionBar-item coral-Button coral-Button--quiet coral-Button--graniteActionBar foundation-mode-change')[0].style.display='none';

}
    if(Object.keys(obj).length!=0)
{

  for(z=0;z<Object.keys(obj).length; z++)
           {
               str="./"+obj[z].fieldName;


if((obj[z].fieldName).indexOf("NutritionFacts") !=-1)

{

    index=obj[z].fieldName.indexOf(".");
    str="./nutritionalFactss/nutritionalFacts/"+obj[z].fieldName.substring(index+1);

}



 if((obj[z].fieldName).indexOf("#WrapperInfo") !=-1)

{

    index=obj[z].fieldName.indexOf(".");
    partone=obj[z].fieldName.substring(index+1);
    actual = partone.substr(0, 1).toLowerCase() + partone.substr(1);
    if(obj[z].fieldName.substr(0,1)=="1")
    {level="";
    }
    else
    {
    level=obj[z].fieldName.substr(0,1)-2;
    }
    str="./wrapperInfos/wrapperInfo"+level+"/"+actual;


}


if((obj[z].fieldName).indexOf("#recipientsInfo") !=-1)

{

    index=obj[z].fieldName.indexOf(".");
    partone=obj[z].fieldName.substring(index+1);
    actual = partone.substr(0, 1).toLowerCase() + partone.substr(1);
    if(obj[z].fieldName.substr(0,1)=="1")
    {level="";
    }
    else
    {
    level=obj[z].fieldName.substr(0,1)-2;
    }
    str="./recipientsInfos/recipientsInfo"+level+"/"+actual;



}

               if(obj[z].newName!=null)
{
str="./"+obj[z].newName;

}



               val=document.getElementsByName(str);

               if(val[0]!=null && obj[z].isApplicable=="Y")
               {
                              val[0].setAttribute("unilever-show","true");

               }

               if(productType=="Product" || productType=="product"  )
               {
                   var eanParent=document.getElementsByName("./skuParent");
                   eanParent[0].parentElement.style.display='none';
            if(val[0]!=null && obj[z].mandatory=="Y")
            {
                              val[0].setAttribute("unilever-mandatory","true");



               }
               }
if(productType=="variation")
               {

                   var eanParent=document.getElementsByName("./skuParent");
                   eanParent[0].setAttribute("unilever-show","true");
            if(val[0]!=null && obj[z].mandatoryVariant=="Y")
            {
                              val[0].setAttribute("unilever-mandatory","true");



               }



               }


            if(val[0]!=null && obj[z].readOnly=="Y")
               {
                              val[0].setAttribute("unilever-readonly","true");



               }
              if(val[0]!=null && obj[z].minLength!="")
               {
                              val[0].setAttribute("unilever-minLength",obj[z].minLength);
                              val[0].setAttribute("minlength","unilever-minlength");

               }

            if(val[0]!=null && obj[z].maxLength!="")
               {
                              val[0].setAttribute("unilever-maxlength",obj[z].maxLength);
                              val[0].setAttribute("maxlength","unilever-maxlength");

               }

            if(val[0]!=null && obj[z].regex!="")
               {
                              val[0].setAttribute("unilever-regex",obj[z].regex);
                              val[0].setAttribute("regex","unilever-regex");

               }
           }



    var inputs = document.querySelectorAll("input[type=text],textarea");

    for (x = 0 ; x <inputs.length ; x++)
    {

        if(inputs[x].getAttribute("unilever-show")==null)

        {  
                    var myname = inputs[x].getAttribute("name");
            if(myname!=null)
            {
                if(!myname.startsWith("./assets"))
                {

                     val=document.getElementsByName(myname);

                      val[0].parentElement.style.display='none';

                }}
        }

        if(inputs[x].getAttribute("unilever-mandatory")!=null)

        {  
                  var myname = inputs[x].getAttribute("name");
                  if(myname!=null)
                   {
                     val=document.getElementsByName(myname);

                      val[0].setAttribute("aria-required","true");
                      }
        }

        if(inputs[x].getAttribute("unilever-readonly")!=null)

        {  
                  var myname = inputs[x].getAttribute("name");

                  if(myname!=null)
                   {
                     val=document.getElementsByName(myname);

                      val[0].disabled="true";
                      }
        }




        }

}


</script>