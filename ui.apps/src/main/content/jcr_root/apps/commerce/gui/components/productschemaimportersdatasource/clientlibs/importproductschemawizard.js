(function(window, document, Granite, $) {
	"use strict";

	$(document).ready(function() {
		var $form = $(".cq-commerce-products-import");
		var actionUrl = $form.attr("action");

		if(actionUrl=="/bin/assignbaseassetpath")
		{
			var url = window.location.href.split('.html')[1];

			$.ajax({ type: 'GET',   
				url: '/bin/getassetpath',
				data: {
					path: url
				},
				dataType:'json',
				success : function(data)
				{ 

					document.getElementsByName("globalAssetBasePath")[0].value=data[0].value;
					document.getElementsByName("localAssetBasePath")[0].value=data[1].value;
					document.getElementsByName("imageFormat")[0].value=data[2].value;

				}
			});
		} else if(actionUrl=="/bin/assignhomepagepath"){
            var url = window.location.href.split('.html')[1];

			$.ajax({ type: 'GET',   
				url: '/bin/gethomepagepath',
				data: {
					path: url
				},
				dataType:'json',
				success : function(data)
				{ 
					document.getElementsByName("homePagePath")[0].value=data[0].value;
				}
			});
        }


		$('#schemaConfigId').on('click', function(event) {
			var url = window.location.href;
			$.ajax({
				url: '/bin/checkSchema.check.html',
				type: 'POST',
				dataType: 'text',
				contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				data: {
					path: url
				},
				success: function(data, textStatus, jQxhr) {
					if (data === 'found') {
						$('#createSchemaId').hide();
						$('#viewSchemaId').show();
						$('#deleteSchemaId').show();
						$('#exportPimId').show();
					} else {
						$('#createSchemaId').show();
						$('#viewSchemaId').hide();
						$('#deleteSchemaId').hide();
						$('#exportPimId').hide();
					}
				},
				error: function(jqXhr, textStatus, errorThrown) {
					$('#createSchemaId').hide();
					$('#viewSchemaId').hide();
					$('#deleteSchemaId').hide();
					$('#exportPimId').hide();
					console.log(errorThrown);
				}
			});
		});

		function checkCategory(event) {
			if ($('#categoriesSelect').data('select').getValue() === "") {
				$(window).adaptTo("foundation-ui").alert("Required", "Please select category");
				event.preventDefault();
				return false;
			}
			return true;
		}

		function checkMarket(event) {
			var marketSelected = false;
			var marketSelect = $('#marketsSelect').find('li');
			$.each(marketSelect, function(i, item) {
				if (this.getAttribute("aria-selected") === "true") {
					marketSelected = true;
				}
			});

			if (!marketSelected) {
				$(window).adaptTo("foundation-ui").alert("Required", "Please select market");
				event.preventDefault();
			}
		}

		$('#createSchema').on('click', function(event) {
			var url = window.location.href;
			var schemaExist = false;
			$.ajax({
				async: false,
				url: '/bin/checkSchema.check.html',
				type: 'POST',
				global: false,
				dataType: 'text',
				contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
				data: {
					path: url
				},
				success: function(data, textStatus, jQxhr) {
					if (data === 'found') {
						schemaExist = true;
					}
				},
				error: function(jqXhr, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
			if(schemaExist){
				event.preventDefault();
				$(window).adaptTo("foundation-ui").alert("Create schema", "Schema already created,if new schema needs to be created delete schema and create new schema");
			} else if (checkCategory(event)) {
				checkMarket(event);
				$(window).adaptTo("foundation-ui").waitTicker(Granite.I18n.get("Creating Schema....."), "");
			}
		});

		$('#deleteSchemaId').on('click', function(event) {
			event.preventDefault();
			var ui = $(window).adaptTo("foundation-ui");        
			var title = "Delete schema";        
			var message = "Click Yes to delete schema";
			ui.prompt(title, message, "notice", [            {                
				text: Granite.I18n.get("Yes"),
				                primary: true,
				                id: "yes"            
			},              {                
				text: Granite.I18n.get("No"),
				                id: "no"            
			}        ], function(btnId) {            
				if (btnId === "yes") {
					var url = $('#deleteSchemaId').attr('href');
					if (url != undefined) {
						$.ajax({
							url: url,
							type: 'POST',
							dataType: 'text',
							contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
							success: function(data, textStatus, jQxhr) {
								$(window).adaptTo("foundation-ui").alert(Granite.I18n.get("Delete schema"), Granite.I18n.get("Schema deleted successfully."));
							},
							error: function(jqXhr, textStatus, errorThrown) {
								console.log(errorThrown);
								$(window).adaptTo("foundation-ui").alert(Granite.I18n.get("Delete schema"), Granite.I18n.get("Schema deletion failed."));
							}
						});
					} else {
						$(window).adaptTo("foundation-ui").alert(Granite.I18n.get("Delete schema"), Granite.I18n.get("Schema deletion failed."));
					}
				}        
			});
		});

		$('#categoriesSelect').on('selected', function(event) {

			clearMarketList();
			//get the second select box (country) saved value
			$.getJSON(event.selected + ".marketschema.json").done(function(data) {
				if (_.isEmpty(data)) {
					return;
				}

				$.each(data, function(i, item) {
					fillMarkets(item.text, item.value);
				});

			});
		});

		function fillMarkets(text, value) {
			var option = $('<li class="coral-SelectList-item coral-SelectList-item--option" data-value="' + value + '" aria-selected="false">' + text + '</li>');
			$('#marketsSelect').data('select').addOption(option);
		}

		function clearMarketList() {
			//clear selection for markets
			if ($('#marketsSelect').find('li').length > 0) {
				$('#marketsSelect').data('select').setValue();
				$('#marketsSelect').find('li').remove();
			}

			if ($('#marketsSelect').find('option').length > 0) {
				$('#marketsSelect').find('option').remove();
			}
		}
	});

})(window, document, Granite, Granite.$);