//Attaching events to select and check boxes of touch ui dialogs also dynamically populates the checkbox and select
(function($, $document) {
    "use strict";
    var ui = $(window).adaptTo("foundation-ui");
    var COMMAND_URL = Granite.HTTP.externalize("/bin/lockAndUnlock");
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.lockCatalog",
        handler: function(name, el, config, collection, selections) {

            var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });
            var targettedObj = {};
            targettedObj["paths"] = paths;
            var jsonGetData = [];
            $.ajax({
                url: COMMAND_URL + "?paths=" + paths,
                method: "GET",
                async: false

            }).done(function(data) {
                jsonGetData = JSON.parse(data);
            });
            if (jsonGetData.status == 201) {
                var ui = $(window).adaptTo("foundation-ui");        
                var title = Granite.I18n.get("lockCatalog.lock");        
                var message = Granite.I18n.get("lockCatalog.lock.alert") +
                    " " + jsonGetData.user + "." + Granite.I18n.get("lockCatalog.unLock.message")+" "+ jsonGetData.user +" "+Granite.I18n.get("lockCatalog.communicate");        
                ui.prompt(title, message, "notice", [            {                
                    text: Granite.I18n.get("Yes"),
                                    primary: true,
                                    id: "yes"            
                },              {                
                    text: Granite.I18n.get("No"),
                                    id: "no"            
                }        ], function(btnId) {            
                    if (btnId === "yes") {                
                        targettedObj["lock"] = "false";                
                        $.ajax({
                            url: COMMAND_URL,
                            async: false,
                            method: "POST",
                            data: JSON.stringify(targettedObj),
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function(data, textStatus, jQxhr) {
                                if (data.status = 200) {
                                    $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("lockCatalog.lock"), Granite.I18n.get("lockCatalog.unLock.success"));
                                } else {
                                    $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("lockCatalog.lock"), Granite.I18n.get("lockCatalog.lock.error"));
                                }
                            },
                            error: function(jqXhr, textStatus, errorThrown) {
                                $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("lockCatalog.lock"), Granite.I18n.get("lockCatalog.lock.error"));
                            }


                        });            
                    }        
                });

            } else if (jsonGetData.status == 200) {
                var ui = $(window).adaptTo("foundation-ui");        
                var title = Granite.I18n.get("lockCatalog.lock");        
                var message = Granite.I18n.get("lockCatalog.lock.message");        
                ui.prompt(title, message, "notice", [            {                
                    text: Granite.I18n.get("Yes"),
                                    primary: true,
                                    id: "yes"            
                },              {                
                    text: Granite.I18n.get("No"),
                                    id: "no"            
                }        ], function(btnId) {            
                    if (btnId === "yes") {                
                        targettedObj["lock"] = "true";                
                        $.ajax({
                            url: COMMAND_URL,
                            async: false,
                            method: "POST",
                            data: JSON.stringify(targettedObj),
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function(data, textStatus, jQxhr) {
                                if (data.status = 200) {
                                    $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("lockCatalog.lock"), Granite.I18n.get("lockCatalog.lock.success"));
                                } else {
                                    $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("lockCatalog.lock"), Granite.I18n.get("lockCatalog.lock.error"));
                                }
                            },
                            error: function(jqXhr, textStatus, errorThrown) {
                                $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("lockCatalog.lock"), Granite.I18n.get("lockCatalog.lock.error"));
                            }


                        });            
                    }        
                });

            }


        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.importOverridenSchema",
        handler: function(name, el, config, collection, selections) {
        	var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });
        	window.location.href = "/libs/commerce/gui/content/products/importproductswizard.html"+paths[0];
        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.createSchema",
        handler: function(name, el, config, collection, selections) {
        	var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });
        	window.location.href = "/apps/commerce/gui/content/products/schema.html"+paths[0];
        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.exportSchema",
        handler: function(name, el, config, collection, selections) {
        	var paths = selections.map(function(v) {
                return $(v).data("foundationCollectionItemId");
            });
        	window.location.href = "/bin/exportpim.exportunileverpimmatrix.html"+paths[0];
        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.viewSchema",
        handler: function(name, el, config, collection, selections) {

        	var url = window.location.href;
        	var schemaExistForView = false;
            $.ajax({
                url: '/bin/checkSchema.check.html',
                type: 'POST',
                dataType: 'text',
                contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                data: {
                    path: url
                },
                success: function(data, textStatus, jQxhr) {
                    if (data === 'found') {
                    	schemaExistForView = true;
                    }
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
            window.setTimeout(function() {
            if(schemaExistForView){
            	var url = $('#viewSchemaId').attr('href');
            	$.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'text',
                    contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                    success: function(data, textStatus, jQxhr) {
                    	var locationUrl = window.location.href;
                    	window.location.href = locationUrl.replace(new RegExp('/aem/products.html', 'g'), '')+'/schema.html?wcmmode=disabled';
                    },
                    error: function(jqXhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                });
            }else{
            	$(window).adaptTo("foundation-ui").alert(Granite.I18n.get("manageSchema.columnView.viewSchema"), Granite.I18n.get("manageSchema.columnView.viewNoSchema"));
            }
            	}, 1000);
        }
    });
    $(window).adaptTo("foundation-registry").register("foundation.collection.action.action", {
        name: "cq.wcm.deleteSchema",
        handler: function(name, el, config, collection, selections) {

        	var url = window.location.href;
        	var schemaExist = false;
            $.ajax({
                url: '/bin/checkSchema.check.html',
                type: 'POST',
                dataType: 'text',
                contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                data: {
                    path: url
                },
                success: function(data, textStatus, jQxhr) {
                    if (data === 'found') {
                    	schemaExist = true;
                    }
                },
                error: function(jqXhr, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
            window.setTimeout(function() {
            if(schemaExist){
            var ui = $(window).adaptTo("foundation-ui");        
            var title = "Delete schema";        
            var message = "Click Yes to delete schema";
            ui.prompt(title, message, "notice", [            {                
                text: Granite.I18n.get("Yes"),
                                primary: true,
                                id: "yes"            
            },              {                
                text: Granite.I18n.get("No"),
                                id: "no"            
            }        ], function(btnId) {            
                if (btnId === "yes") {
                    var url = $('#deleteSchemaId').attr('href');
                    if (url != undefined) {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'text',
                            contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
                            success: function(data, textStatus, jQxhr) {
                                $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("manageSchema.columnView.deleteSchema"), Granite.I18n.get("manageSchema.columnView.deleteSchema.success"));
                            },
                            error: function(jqXhr, textStatus, errorThrown) {
                                console.log(errorThrown);
                                $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("manageSchema.columnView.deleteSchema"), Granite.I18n.get("manageSchema.columnView.deleteSchema.failure"));
                            }
                        });
                    } else {
                        $(window).adaptTo("foundation-ui").alert(Granite.I18n.get("manageSchema.columnView.deleteSchema"), Granite.I18n.get("manageSchema.columnView.deleteSchema.failure"));
                    }
                }        
            });
        }else{
        	$(window).adaptTo("foundation-ui").alert(Granite.I18n.get("manageSchema.columnView.deleteSchema"), Granite.I18n.get("manageSchema.columnView.deleteNoSchema"));
        }
            }, 1000);
        }
    });
})($, $(document));