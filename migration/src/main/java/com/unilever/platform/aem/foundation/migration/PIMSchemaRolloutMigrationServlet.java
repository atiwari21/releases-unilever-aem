/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.migration;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * PIMSchemaUpdateServlet
 */
@SlingServlet(label = "Unilever PIM schema Rollout & Update Proprties Servlet", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = {
  HttpConstants.METHOD_GET }, selectors = { "rolloutupdatedpimschema" }, extensions = { "html" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.migration.PIMSchemaRolloutMigrationServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever PIM schema Rollout & Update Proprties Servlet", propertyPrivate = false) })
public class PIMSchemaRolloutMigrationServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 private static final String CONTENT_TYPE = "text/html";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PIMSchemaRolloutMigrationServlet.class);
 
 private static final String DEFAULT_SCHEMA_PATH = "/content";
 
 private static final String PARENT_SCHEMA_PAGE_PATH = "/content/unilever/globalProductSchema/jcr:content/par";
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  response.setContentType(CONTENT_TYPE);
  response.setCharacterEncoding("UTF-8");
  
  ResourceResolver resourceResolver = request.getResourceResolver();
  Session session = resourceResolver.adaptTo(Session.class);
  PrintWriter out = response.getWriter();
  
  RequestParameterMap requestParametrs = request.getRequestParameterMap();
  
  String searchPath = requestParametrs.containsKey("sp") ? requestParametrs.getValue("sp").toString() : StringUtils.EMPTY;
  
  if (StringUtils.isBlank(searchPath)) {
   searchPath = DEFAULT_SCHEMA_PATH;
  }
  
  Query query = builder.createQuery(PredicateGroup.create(getQueryMap(searchPath)), resourceResolver.adaptTo(Session.class));
  SearchResult result = query.getResult();
  
  // iterating over the results
  for (Hit hit : result.getHits()) {
   Page page = getSchemaPagesFromSearchHit(hit, resourceResolver);
   if (page != null & !page.getPath().equals("/content/unilever/globalProductSchema")) {
    
    Resource pageParResource = resourceResolver.getResource(page.getPath() + "/jcr:content/par");
    
    Resource parentResource = resourceResolver.getResource(PARENT_SCHEMA_PAGE_PATH);
    if (parentResource != null && pageParResource != null) {
     for (Resource child : parentResource.getChildren()) {
      ValueMap map = child.getValueMap();
      String name = child.getName();
      
      Resource childParResource = pageParResource.getChild(name);
      
      if (map != null) {
       String columnName = map.containsKey("column") ? map.get("column", String.class) : StringUtils.EMPTY;
       String pimField = map.containsKey("pimField") ? map.get("pimField", String.class) : StringUtils.EMPTY;
       if (childParResource != null) {
        ValueMap childmap = childParResource.getValueMap();
        if (childmap != null) {
         String childcolumnName = childmap.containsKey("column") ? childmap.get("column", String.class) : null;
         String childpimField = childmap.containsKey("pimField") ? childmap.get("pimField", String.class) : null;
         if (childpimField.equals(pimField) && !childcolumnName.equals(columnName)) {
          Node node = childParResource.adaptTo(Node.class);
          try {
           node.setProperty("column", columnName);
           session.save();
           writeLogEntry(out, page.getPath(), "Modifying Column Name", "Updating resource : " + childParResource.getPath() + " pim field :"
             + childpimField + " old value : " + childcolumnName + " New value : " + columnName);
             
          } catch (RepositoryException e) {
           LOGGER.error("Unable to update column property for : " + childParResource.getPath(), e);
          }
         }
        }
       } else {
        // copy Resource to page
        try {
         JcrUtil.copy(child.adaptTo(Node.class), pageParResource.adaptTo(Node.class), name);
         session.save();
         writeLogEntry(out, page.getPath(), "Rolling out non existing resource", "Resource : " + child.getPath());
         Resource xyz = pageParResource.getChild(child.getName());
         if (xyz != null) {
          Node n = xyz.adaptTo(Node.class);
          if (n.canAddMixin("cq:LiveRelationship")) {
           n.addMixin("cq:LiveRelationship");
           session.save();
           writeLogEntry(out, page.getPath(), "Adding Live Copy Mixin Type", "resource : " + n.getPath());
          }
         }
        } catch (RepositoryException e) {
         LOGGER.error("Unable to copy resource : " + child.getPath() + " to page : " + page.getPath(), e);
        }
       }
      }
     }
    }
   }
  }
 }
 
 private Map<String, String> getQueryMap(String path) {
  Map<String, String> map = new HashMap<String, String>();
  
  map.put("path", path);
  map.put("type", "cq:PageContent");
  map.put("1_property", JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY);
  map.put("1_property.value", "unilever-aem/components/page/schemaConfigurationPage");
  
  map.put("p.offset", "0");
  map.put("p.limit", "-1");
  
  return map;
 }
 
 private void writeLogEntry(PrintWriter out, String pageName, String action, String message) {
  if (out != null) {
   out.write(pageName + " ---> " + action + " ---> " + message + "<br/>");
  }
 }
 
 private Page getSchemaPagesFromSearchHit(Hit h, ResourceResolver resourceResolver) {
  String pagePath = StringUtils.EMPTY;
  Page page = null;
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  try {
   pagePath = h.getPath();
   if (StringUtils.isNotBlank(pagePath) && (pageManager != null)) {
    page = pageManager.getContainingPage(resourceResolver.getResource(pagePath));
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in getting page from search result : ", e);
  }
  return page;
 }
}
