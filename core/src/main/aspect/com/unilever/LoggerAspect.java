package com.unilever;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
@Aspect
public class LoggerAspect {
 
    @Pointcut(value = "execution(public * com.unilever.*.*(..))")
    public void anyMethod() {
    }
 
    @Before(value = "anyMethod()")
    public void logEntry(JoinPoint joinPoint) {
        Logger logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
        logger.info("Method Started...."+joinPoint.getSignature().getName()+"()");
    }
 
    @After(value = "anyMethod()")
    public void logExit(JoinPoint joinPoint) {
        Logger logger = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
        logger.info(" Method Exited..."+joinPoint.getSignature().getName()+"()");
    }
}