/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion.product;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.tagging.TagConstants;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;
import com.unilever.platform.foundation.commerce.dto.ResultReport;
import com.unilever.platform.foundation.commerce.ingestion.ILoader;

/**
 * The Class CSVLoader.
 *
 * @author atiw22
 */
@Component(configurationFactory = true, metatype = true, label = "Unilever CSV Loader", description = "CSV loader configurations")
@Service
public class Loader extends AbstractLoader implements ILoader {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(Loader.class);
 
 /** The Constant TAG_PROPERTY_MAPPINGS. */
 @org.apache.felix.scr.annotations.Property(unbounded = PropertyUnbounded.ARRAY, label = "TAG Mappings", description = "List of tag mappings.")
 private static final String TAG_PROPERTY_MAPPINGS = "tag.mappings";
 
 /** The Constant DAM_PATH. */
 private static final String DAM_PATH = "/content/dam";
 
 private static final String DEFAULT_IMAGE_FORMAT = "PNG";
 
 /** The tags column list. */
 protected List<String> tagsColumnList = new ArrayList<String>();
 
 /** The tags mappings. */
 private String[] tagsMappings;
 
 /** The default tag mappings. */
 private String[] defaultTagMappings = { "cqTags" };
 
 /** The Constant TWO. */
 public static final int TWO = 2;
 
 /** The Constant FOUR. */
 public static final int FOUR = 4;
 
 /** The Constant LAST_MODIFIED_TIME. */
 public static final String LAST_MODIFIED_TIME = "lastmodifiedtime";
 
 /** The Constant SKU_PARENT. */
 public static final String SKU_PARENT = "skuParent";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.commerce.ingestion.product. AbstractLoader #activate(org.osgi.service.component.ComponentContext)
  */
 @Override
 @Activate
 protected void activate(ComponentContext ctx) {
  super.activate(ctx);
  
  tagsMappings = PropertiesUtil.toStringArray(ctx.getProperties().get(TAG_PROPERTY_MAPPINGS), defaultTagMappings);
  
  if (tagsMappings != null) {
   for (String s : tagsMappings) {
    tagsColumnList.add(s);
   }
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.commerce.ingestion.ILoader#saveData( org.apache.sling.api.resource.ResourceResolver, java.lang.String,
  * org.json.JSONArray, org.apache.sling.api.SlingHttpServletRequest, org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 public ResultReport saveProducts(ResourceResolver resourceResolver, String storePath, JSONArray jsonarray, boolean oldIngestion) throws IOException {
  
  ResultReport report = null;
  try {
   
   initializeDataElements();
   for (int i = 0; i < jsonarray.length(); i++) {
    saveProduct(resourceResolver, storePath, jsonarray.getJSONObject(i), oldIngestion);
   }
  } catch (JSONException e) {
   LOG.error("Error while fetching JSON Object", e);
  }
  report = getReport(report);
  return report;
 }
 
 /**
  * Save a product using jsonObject in to CRX repository.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param obj
  *         the obj
  * @param oldIngestion
  *         the old ingestion
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 public void saveProduct(ResourceResolver resourceResolver, String storePath, JSONObject obj, boolean oldIngestion) throws IOException {
  
  try {
   String op = StringUtils.EMPTY;
   String type = StringUtils.EMPTY;
   String sku = StringUtils.EMPTY;
   
   if (obj.has(UNICommerceConstants.OPERATION) && obj.getString(UNICommerceConstants.OPERATION) != null) {
    op = obj.getString(UNICommerceConstants.OPERATION).toLowerCase();
   }
   
   if (obj.has(UNICommerceConstants.TYPE) && obj.getString(UNICommerceConstants.TYPE) != null) {
    type = obj.getString(UNICommerceConstants.TYPE).toLowerCase();
   }
   
   if (obj.has(UNICommerceConstants.SKU) && obj.getString(UNICommerceConstants.SKU) != null) {
    sku = String.valueOf(obj.getString(UNICommerceConstants.SKU));
   }
   
   if (oldIngestion) {
    op = UNICommerceConstants.ADD_OPERATION;
   }
   
   String uniqueId = StringUtils.EMPTY;
   if (obj.has(UNICommerceConstants.UNIQUE_ID)) {
    uniqueId = String.valueOf(obj.getString(UNICommerceConstants.UNIQUE_ID));
   }
   
   if (op.equals(UNICommerceConstants.ADD_OPERATION)) {
    performAddOperation(type, resourceResolver, storePath, sku, obj, uniqueId);
   } else if (op.equals(UNICommerceConstants.UPDATE_OPERATION)) {
    performUpdateOperation(type, resourceResolver, storePath, sku, obj, uniqueId, oldIngestion);
   } else if (op.equals(UNICommerceConstants.DELETE_OPERATION)) {
    performDeleteOperation(type, resourceResolver, storePath, sku, obj, uniqueId, oldIngestion);
   } else {
    LOG.debug("No operation mentioned.");
    logMessage("Invalid Operation " + op + " is not supported.", false);
   }
  } catch (RepositoryException e) {
   LOG.error("Error while saving Json Object", e);
  }
 }
 
 /**
  * Adds the obj.
  *
  * @param type
  *         the type
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @param oldIngestion
  *         the old ingestion
  * @throws RepositoryException
  *          the repository exception
  */
 private void performAddOperation(String type, ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId)
   throws RepositoryException {
  if (type.equalsIgnoreCase(UNICommerceConstants.PRODUCT_TYPE)) {
   addProduct(resourceResolver, storePath, sku, obj, uniqueId);
  } else if (type.equalsIgnoreCase(UNICommerceConstants.VARIATION_TYPE)) {
   addVariation(resourceResolver, storePath, sku, obj, uniqueId);
  } else {
   LOG.debug("No supported type mentioned for add operation.");
   logMessage("Invalid Type " + type + " is not supported.", false);
  }
 }
 
 /**
  * Update obj.
  *
  * @param type
  *         the type
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @param oldIngestion
  *         the old ingestion
  * @throws RepositoryException
  *          the repository exception
  */
 private void performUpdateOperation(String type, ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId,
   boolean oldIngestion) throws RepositoryException {
  if (type.equals(UNICommerceConstants.PRODUCT_TYPE)) {
   updateProduct(resourceResolver, storePath, sku, obj, uniqueId, oldIngestion);
  } else if (type.equals(UNICommerceConstants.VARIATION_TYPE)) {
   updateVariation(resourceResolver, storePath, sku, obj, uniqueId, oldIngestion);
  } else {
   LOG.debug("No supported type mentioned for update operation.");
  }
 }
 
 /**
  * Delete obj.
  *
  * @param type
  *         the type
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @param oldIngestion
  *         the old ingestion
  * @throws RepositoryException
  *          the repository exception
  */
 private void performDeleteOperation(String type, ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId,
   boolean oldIngestion) throws RepositoryException {
  if (type.equals(UNICommerceConstants.PRODUCT_TYPE)) {
   deleteProduct(resourceResolver, storePath, sku, obj, uniqueId, oldIngestion);
  } else if (type.equals(UNICommerceConstants.VARIATION_TYPE)) {
   deleteVariant(resourceResolver, storePath, sku, obj, uniqueId, oldIngestion);
  } else {
   LOG.debug("No supported type mentioned for delete operation.");
  }
 }
 
 /**
  * Adds the product.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @throws RepositoryException
  *          the repository exception
  */
 private void addProduct(ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId)
   throws RepositoryException {
  
  String path = StringUtils.EMPTY;
  
  if (StringUtils.isNotBlank(uniqueId)) {
   path = getProductPath(storePath, uniqueId);
  } else {
   path = getProductPath(storePath, sku);
  }
  
  Resource product = resourceResolver.getResource(path);
  Node productNode = null;
  if (product != null) {
   productNode = product.adaptTo(Node.class);
   logMessage("Warning : Unable to create existing product : " + productNode.getPath(), false);
  } else {
   productNode = createProduct(path, resourceResolver.adaptTo(Session.class));
   setProperties(resourceResolver, productNode, obj, false);
   createImages(productNode, obj, resourceResolver, storePath);
  }
 }
 
 /**
  * Adds the variation.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @throws RepositoryException
  *          the repository exception
  */
 private void addVariation(ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId)
   throws RepositoryException {
  
  String parentSKU = String.valueOf(obj.getString(SKU_PARENT));
  Resource parent = resourceResolver.getResource(getProductPath(storePath, parentSKU));
  String localSku = sku;
  if (StringUtils.isNotBlank(uniqueId)) {
   localSku = uniqueId;
  }
  
  if (parent != null) {
   Resource child = parent.getChild(localSku);
   Node variantNode = null;
   if (child != null) {
    variantNode = child.adaptTo(Node.class);
    logMessage("Warning : Unable to create existing variant : " + variantNode.getPath(), false);
   } else {
    variantNode = createVariant(parent.adaptTo(Node.class), localSku);
    setProperties(resourceResolver, variantNode, obj, false);
    createImages(variantNode, obj, resourceResolver, storePath);
   }
  } else {
   logMessage("Error : Unable to create variant for missing parent product : " + localSku, true);
  }
 }
 
 /**
  * Update product.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @param oldIngestion
  *         the old ingestion
  * @throws RepositoryException
  *          the repository exception
  */
 private void updateProduct(ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId, boolean oldIngestion)
   throws RepositoryException {
  
  String path = StringUtils.EMPTY;
  boolean isUpdated = false;
  
  if (StringUtils.isNotBlank(uniqueId)) {
   path = getProductPath(storePath, uniqueId);
  } else {
   path = getProductPath(storePath, sku);
  }
  
  Resource product = resourceResolver.getResource(path);
  if (product != null) {
   Node productNode = product.adaptTo(Node.class);
   if (!oldIngestion) {
    isUpdated = isUpdatedProduct(obj, productNode);
   }
   
   if (!isUpdated) {
    setProperties(resourceResolver, productNode, obj, true);
    createImages(productNode, obj, resourceResolver, storePath);
    productUpdated(productNode);
   } else {
    logMessage("Error : Unable to update already modified product version : " + path, true);
   }
  } else {
   logMessage("Warning : Unable to update non existing product : " + path, false);
  }
 }
 
 /**
  * Update variation.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @param oldIngestion
  *         the old ingestion
  * @throws RepositoryException
  *          the repository exception
  */
 private void updateVariation(ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId, boolean oldIngestion)
   throws RepositoryException {
  
  String parentSKU = String.valueOf(obj.getString(SKU_PARENT));
  Resource parent = resourceResolver.getResource(getProductPath(storePath, parentSKU));
  boolean isUpdated = false;
  String localSku = sku;
  if (StringUtils.isNotBlank(uniqueId)) {
   localSku = uniqueId;
  }
  
  if (parent != null) {
   Resource child = parent.getChild(localSku);
   Node variantNode = null;
   if (child != null) {
    variantNode = child.adaptTo(Node.class);
    
    if (!oldIngestion) {
     isUpdated = isUpdatedProduct(obj, variantNode);
    }
    
    if (!isUpdated) {
     setProperties(resourceResolver, variantNode, obj, true);
     createImages(variantNode, obj, resourceResolver, storePath);
     variantUpdated(variantNode);
    } else {
     logMessage("Error : Unable to update already modified variant version : " + localSku, true);
    }
   } else {
    logMessage("Warning : Unable to update non existing variant : " + localSku, false);
   }
  } else {
   logMessage("Warning : Unable to update non existing variant : " + localSku, false);
  }
 }
 
 /**
  * Delete product.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @param oldIngestion
  *         the old ingestion
  * @throws RepositoryException
  *          the repository exception
  */
 private void deleteProduct(ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId, boolean oldIngestion)
   throws RepositoryException {
  
  String productPath = StringUtils.EMPTY;
  boolean isUpdated = false;
  
  if (StringUtils.isNotBlank(uniqueId)) {
   productPath = getProductPath(storePath, uniqueId);
  } else {
   productPath = getProductPath(storePath, sku);
  }
  
  Resource product = resourceResolver.getResource(productPath);
  if (product != null) {
   Node productNode = product.adaptTo(Node.class);
   
   if (!oldIngestion) {
    isUpdated = isUpdatedProduct(obj, productNode);
   }
   
   if (!isUpdated) {
    productDeleted(productNode);
    productNode.remove();
   } else {
    logMessage("Error : Unable to delete modified product version : " + productPath, true);
   }
  } else {
   logMessage("Warning : Unable to delete non existing product : " + productPath, false);
  }
 }
 
 /**
  * Delete variant.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @param obj
  *         the obj
  * @param uniqueId
  *         the unique id
  * @param oldIngestion
  *         the old ingestion
  * @throws RepositoryException
  *          the repository exception
  */
 private void deleteVariant(ResourceResolver resourceResolver, String storePath, String sku, JSONObject obj, String uniqueId, boolean oldIngestion)
   throws RepositoryException {
  
  String parentSKU = String.valueOf(obj.getString(SKU_PARENT));
  Resource parent = resourceResolver.getResource(getProductPath(storePath, parentSKU));
  boolean isUpdated = false;
  String localSku = sku;
  if (StringUtils.isNotBlank(uniqueId)) {
   localSku = uniqueId;
  }
  
  if (parent != null) {
   Resource child = parent.getChild(localSku);
   Node variantNode = null;
   if (child != null) {
    variantNode = child.adaptTo(Node.class);
    
    if (!oldIngestion) {
     isUpdated = isUpdatedProduct(obj, variantNode);
    }
    
    if (!isUpdated) {
     variantDeleted(variantNode);
     variantNode.remove();
    } else {
     logMessage("Error : Unable to delete modified variant version : " + localSku, true);
    }
   } else {
    logMessage("Warning : Unable to delete non existing variant : " + localSku, false);
   }
  } else {
   logMessage("Warning : Unable to delete non existing variant : " + localSku, false);
  }
 }
 
 /**
  * Sets the properties.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param node
  *         the node
  * @param obj
  *         the obj
  * @param update
  *         the update
  * @throws RepositoryException
  *          the repository exception
  */
 private void setProperties(ResourceResolver resourceResolver, Node node, JSONObject obj, boolean update) throws RepositoryException {
  if (update) {
   // Clear any existing, non-system properties:
   cleanProperties(node);
   resourceResolver.adaptTo(Session.class).save();
  }
  
  setPropertyForJson(obj, node, resourceResolver);
  
  List<String> tagsList = new ArrayList<String>();
  for (String tag : tagsColumnList) {
   String tagValue = PIMInjestionUtility.get(obj, tag);
   if (tagValue != null) {
    if (tagValue.contains(",")) {
     String[] tags = tagValue.split(",");
     for (String t : tags) {
      tagsList.add(t.trim());
     }
    } else {
     tagsList.add(tagValue.trim());
    }
   }
  }
  
  // Set tags to node
  setTags(node, resourceResolver, tagsList);
 }
 
 /**
  * Clean properties.
  *
  * @param node
  *         the node
  * @throws RepositoryException
  *          the repository exception
  */
 private void cleanProperties(Node node) throws RepositoryException {
  PropertyIterator existingProps = node.getProperties();
  while (existingProps.hasNext()) {
   Property prop = (Property) existingProps.next();
   String propName = prop.getName();
   if (!propName.startsWith("sling:") && !propName.startsWith("cq:") && !propName.startsWith("jcr:")) {
    prop.remove();
   }
  }
  
  // Remove any existing image:
  if (node.hasNode(UNICommerceConstants.ASSETS)) {
   node.getNode(UNICommerceConstants.ASSETS).remove();
  }
  
  NodeIterator nodeItr = node.getNodes();
  while (nodeItr.hasNext()) {
   Node childNode = (Node) nodeItr.next();
   if (childNode != null && !childNode.hasProperty("cq:commerceType")) {
    childNode.remove();
   }
  }
 }
 
 /**
  * Sets the property for json.
  *
  * @param obj
  *         the obj
  * @param node
  *         the node
  * @param resourceResolver
  *         the resource resolver
  * @throws RepositoryException
  *          the repository exception
  */
 private void setPropertyForJson(JSONObject obj, Node node, ResourceResolver resourceResolver) throws RepositoryException {
  
  Iterator<?> keys = obj.keys();
  while (keys.hasNext()) {
   String key = (String) keys.next();
   if (obj.get(key) instanceof JSONObject) {
    
    JSONObject jsonObj = obj.getJSONObject(key);
    if ((jsonObj != null) && (jsonObj.keys().hasNext())) {
     
     if ("additionalProperties".equals(key)) {
      setPropertyForJson(jsonObj, node, resourceResolver);
      
     } else {
      
      // create child node
      Node childNode = JcrUtil.createUniqueNode(node, key, UNICommerceConstants.NI_UNSTRUCTURED, resourceResolver.adaptTo(Session.class));
      
      if (childNode != null) {
       setPropertyForJson(jsonObj, childNode, resourceResolver);
      }
     }
    }
    
   } else if (obj.get(key) instanceof JSONArray) {
    
    JSONArray jsonArr = obj.getJSONArray(key);
    
    if (jsonArr.length() > 0) {
     Node childNode = JcrUtil.createUniqueNode(node, key + "s", UNICommerceConstants.NI_UNSTRUCTURED, resourceResolver.adaptTo(Session.class));
     
     for (int i = 0; i < jsonArr.length(); i++) {
      
      // create child node for object
      Node subchildNode = JcrUtil.createUniqueNode(childNode, key, UNICommerceConstants.NI_UNSTRUCTURED, resourceResolver.adaptTo(Session.class));
      if (subchildNode != null) {
       setPropertyForJson(jsonArr.getJSONObject(i), subchildNode, resourceResolver);
      }
     }
    }
   } else if (obj.get(key) instanceof String) {
    
    String value = obj.getString(key);
    
    if (value != null && "name".equals(key.trim())) {
     node.setProperty(JcrConstants.JCR_TITLE, value);
    }
    
    if (value != null && LAST_MODIFIED_TIME.equals(key.trim())) {
     // Don't add this value
    }
    
    if (value != null && StringUtils.isNotBlank(value)) {
     node.setProperty(key.trim(), value);
    }
   } else if (obj.get(key) instanceof Long) {
    
    long value = obj.getLong(key);
    if (value != 0) {
     node.setProperty(key.trim(), value);
    }
   } else if (obj.get(key) instanceof Float) {
    
    float value = (float) obj.getDouble(key);
    BigDecimal decimal = BigDecimal.valueOf(value);
    decimal = decimal.setScale(TWO, BigDecimal.ROUND_HALF_EVEN);
    node.setProperty(key.trim(), decimal);
    
   } else if (obj.get(key) instanceof Integer) {
    
    long value = obj.getInt(key);
    if (value != 0) {
     node.setProperty(key.trim(), value);
    }
   }
  }
 }
 
 /**
  * Sets the tags.
  *
  * @param node
  *         the node
  * @param resourceResolver
  *         the resource resolver
  * @param tagsList
  *         the tags list
  * @throws RepositoryException
  *          the repository exception
  */
 private void setTags(Node node, ResourceResolver resourceResolver, List<String> tagsList) throws RepositoryException {
  
  if (!tagsList.isEmpty()) {
   
   String[] tags = new String[tagsList.size()];
   tags = tagsList.toArray(tags);
   
   // Create Tags for Category, SubCategory and Brand
   if (!node.isNodeType(TagConstants.NT_TAGGABLE)) {
    node.addMixin(TagConstants.NT_TAGGABLE);
   }
   createMissingTags(resourceResolver, tags);
   node.setProperty(TagConstants.PN_TAGS, tags);
  }
 }
 
 /**
  * Creates the images.
  *
  * @param node
  *         the node
  * @param obj
  *         the obj
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @throws RepositoryException
  *          the repository exception
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 private void createImages(Node node, JSONObject obj, ResourceResolver resourceResolver, String storePath) throws RepositoryException {
  
  String images = PIMInjestionUtility.get(obj, UNICommerceConstants.IMAGES);
  String imagesAltText = PIMInjestionUtility.get(obj, UNICommerceConstants.IMAGES_ALT_TEXT);
  String customImages = PIMInjestionUtility.get(obj, "customizeImageIds");
  String customAltText = PIMInjestionUtility.get(obj, "customizeImageAltText");
  String imgType = PIMInjestionUtility.get(obj, "imageFormat");
  
  if (images != null) {
   String[] imageVideoArr = images.split(",");
   
   List<String> imagesList = new ArrayList<String>();
   List<String> videoList = new ArrayList<String>();
   
   for (String imgVid : imageVideoArr) {
    if (imgVid.contains("#")) {
     String[] array = imgVid.split("#");
     if (array.length == TWO) {
      imagesList.add(array[0].trim());
      videoList.add(array[1].trim());
     } else {
      imagesList.add(array[0].trim());
      videoList.add(StringUtils.EMPTY);
     }
    } else {
     imagesList.add(imgVid.trim());
     videoList.add(StringUtils.EMPTY);
    }
   }
   
   String[] imagesArr = new String[imagesList.size()];
   imagesArr = imagesList.toArray(imagesArr);
   
   String[] videoArr = new String[videoList.size()];
   videoArr = videoList.toArray(videoArr);
   
   String[] imagesAltTextArr = null;
   if (imagesAltText != null) {
    imagesAltTextArr = imagesAltText.split(",");
   }
   
   Node assetsFolder = createAssetFolder(node, resourceResolver, "assets");
   if (assetsFolder != null) {
    createMultipleImages(assetsFolder, resourceResolver, imagesArr, imagesAltTextArr, videoArr, storePath, imgType);
   }
  }
  
  if (customImages != null) {
   String[] customImageVideoArr = null;
   String[] imagesArr = null;
   String[] customVideoArr = null;
   if (customImages.contains("#")) {
    customImageVideoArr = customImages.split("#");
   }
   if (customImageVideoArr != null) {
    imagesArr = customImageVideoArr[0].split(",");
    customVideoArr = customImageVideoArr[1].split(",");
   } else {
    imagesArr = customImages.split(",");
   }
   
   String[] imagesAltTextArr = null;
   if (customAltText != null) {
    imagesAltTextArr = customAltText.split(",");
   }
   Node assetsFolder = createAssetFolder(node, resourceResolver, "customassets");
   createMultipleImages(assetsFolder, resourceResolver, imagesArr, imagesAltTextArr, customVideoArr, storePath, imgType);
  }
 }
 
 /**
  * Creates the multiple images.
  *
  * @param assetsFolder
  *         the assets folder
  * @param resourceResolver
  *         the resource resolver
  * @param imagesArr
  *         the images arr
  * @param imagesAltTextArr
  *         the images alt text arr
  * @param videoArr
  *         the video arr
  * @param storePath
  *         the store path
  * @throws RepositoryException
  *          the repository exception
  */
 private void createMultipleImages(Node assetsFolder, ResourceResolver resourceResolver, String[] imagesArr, String[] imagesAltTextArr,
   String[] videoArr, String storePath, String imgType) throws RepositoryException {
  
  int index = 0;
  for (String img : imagesArr) {
   String altText = StringUtils.EMPTY;
   String videoId = StringUtils.EMPTY;
   if ((imagesAltTextArr != null) && (index < imagesAltTextArr.length)) {
    altText = imagesAltTextArr[index];
   }
   if ((videoArr != null) && (index < videoArr.length)) {
    videoId = videoArr[index];
   }
   
   if (StringUtils.isNotEmpty(img)) {
    
    if (img.contains(".")) {
     img = PIMInjestionUtility.getImageIdFromDecimalValue(img);
    }
    
    String localAssetBasePath = PIMInjestionUtility.getAssetLocationPath(storePath, resourceResolver, "localAssetBasePath");
    String globalAssetBasePath = PIMInjestionUtility.getAssetLocationPath(storePath, resourceResolver, "globalAssetBasePath");
    String imageFormat = PIMInjestionUtility.getAssetLocationPath(storePath, resourceResolver, "imageFormat");
    
    if (StringUtils.isNotBlank(imgType)) {
     imageFormat = imgType;
    }
    
    List list = new LinkedList();
    if (StringUtils.isNotBlank(localAssetBasePath)) {
     if (StringUtils.isNotBlank(imageFormat)) {
      list = PIMInjestionUtility.getImageResourcesByID(img, localAssetBasePath, resourceResolver, imageFormat);
     } else {
      list = PIMInjestionUtility.getImageResourcesByID(img, localAssetBasePath, resourceResolver, DEFAULT_IMAGE_FORMAT);
     }
    }
    
    if (list.isEmpty() && StringUtils.isNotBlank(globalAssetBasePath)) {
     if (StringUtils.isNotBlank(imageFormat)) {
      list = PIMInjestionUtility.getImageResourcesByID(img, globalAssetBasePath, resourceResolver, imageFormat);
     } else {
      list = PIMInjestionUtility.getImageResourcesByID(img, globalAssetBasePath, resourceResolver, DEFAULT_IMAGE_FORMAT);
     }
    }
    
    if (list.isEmpty()) {
     list = PIMInjestionUtility.getImageResourcesByID(img, DAM_PATH, resourceResolver, DEFAULT_IMAGE_FORMAT);
    }
    
    if (list.isEmpty()) {
     list = PIMInjestionUtility.getImageResourcesByID(img, DAM_PATH, resourceResolver, "");
    }
    
    if (!list.isEmpty()) {
     ListIterator iter = list.listIterator(list.size());
     while (iter.hasPrevious()) {
      Asset asset = DamUtil.resolveToAsset((Resource) iter.previous());
      if (asset != null) {
       createImage(assetsFolder, asset.getPath(), String.valueOf(index), altText, videoId);
       break;
      }
      
     }
    } else {
     createImage(assetsFolder, StringUtils.EMPTY, String.valueOf(index), altText, videoId);
    }
   } else {
    createImage(assetsFolder, StringUtils.EMPTY, String.valueOf(index), altText, videoId);
   }
   index++;
  }
 }
 
 /**
  * Gets the product path.
  *
  * @param storePath
  *         the store path
  * @param sku
  *         the sku
  * @return the product path
  */
 private String getProductPath(String storePath, String sku) {
  String productSKU = sku;
  boolean variation = false;
  if (sku.contains(".")) {
   productSKU = sku.substring(0, sku.indexOf("."));
   variation = true;
  }
  
  String path = storePath + "/" + sku.substring(0, TWO) + "/" + sku.substring(0, FOUR) + "/" + productSKU;
  if (variation) {
   path += "/" + sku;
  }
  return path;
 }
 
 /**
  * Checks if is updated product.
  *
  * @param obj
  *         the obj
  * @param node
  *         the node
  * @return true, if is updated product
  */
 private boolean isUpdatedProduct(JSONObject obj, Node node) {
  boolean isUpdated = true;
  String lastModifiedDate = StringUtils.EMPTY;
  
  if (obj.has(LAST_MODIFIED_TIME) && obj.getString(LAST_MODIFIED_TIME) != null) {
   lastModifiedDate = obj.getString(LAST_MODIFIED_TIME);
  }
  
  try {
   if (node != null && node.hasProperty(JcrConstants.JCR_LASTMODIFIED) && StringUtils.isNotBlank(lastModifiedDate)) {
    Calendar lastModifiedValue = node.getProperty(JcrConstants.JCR_LASTMODIFIED).getValue().getDate();
    if (lastModifiedValue != null && lastModifiedValue instanceof Calendar) {
     SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.SSSZ");
     fmt.setCalendar(lastModifiedValue);
     String value = fmt.format(lastModifiedValue.getTime());
     if (lastModifiedDate.equals(value)) {
      isUpdated = false;
     }
    }
   }
  } catch (RepositoryException e) {
   LOG.error("Error while comparing last modified date of the product : ", e);
  }
  
  return isUpdated;
 }
}
