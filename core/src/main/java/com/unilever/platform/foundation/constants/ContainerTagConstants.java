/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.constants;

/**
 * The Class ContainerTagConstants.
 */
public class ContainerTagConstants {
 
 /** The Constant CONTAINER_TAG_CATEGORY. */
 public static final String CATEGORY = "category";
 
 /** The Constant CONTAINER_TAG_ACTION. */
 public static final String ACTION = "action";
 
 /** The Constant CONTAINER_TAG_INFORMATION. */
 public static final String LABEL = "label";
 
 /** The Constant CONTAINER_TAG_ATTR. */
 public static final String TYPE = "type";
 
 /** The Constant ATTRIBUTES. */
 public static final String ATTRIBUTES = "attributes";
 
 /** The Constant PRODUCT_INFO. */
 public static final String PRODUCT_INFO = "productInfo";
 
 /** The Constant PRODUCT_LOAD. */
 public static final String PRODUCT_LOAD = "productLoad";
 
 /** The Constant PRODUCT_CLICK. */
 public static final String PRODUCT_CLICK = "productClick";
 
 /** The Constant PRODUCT_OVERVIEW. */
 public static final String PRODUCT_QUICKVIEW = "productQuickView";
 
 /** The Constant PRODUCT_IMPRESSION. */
 public static final String PRODUCT_IMPRESSION = "productImpression";
 
 public static final String ZOOM = "zoom";
 
 /** The Constant SHOP_NOW. */
 public static final String SHOP_NOW = "shopNow";
 
 /** The Constant RATING_REVIEW. */
 public static final String RATING_REVIEW = "ratingreview";
 
 /** The Constant DROPDOWN. */
 public static final String DROPDOWN = "dropdown";
 
 /** The Constant PRODUCT_ID. */
 public static final String PRODUCT_ID = "productID";
 
 /** The Constant PRODUCT_NAME. */
 public static final String PRODUCT_NAME = "productName";
 
 /** The Constant PRICE. */
 public static final String PRICE = "price";
 
 /** The Constant BRAND. */
 public static final String BRAND = "brand";
 
 /** The Constant QAUNTITY. */
 public static final String QAUNTITY = "quantity";
 
 /** The Constant PRIMARY_CATEGORY. */
 public static final String PRIMARY_CATEGORY = "primaryCategory";
 
 /** The Constant PRODUCT_VARIATIONS. */
 public static final String PRODUCT_VARIATIONS = "productVariations";
 
 /** The Constant LIST_POSITION. */
 public static final String LIST_POSITION = "listPosition";
 
 /** The Constant LOAD_MORE. */
 public static final String LOAD_MORE = "loadMore";
 
 /** The Constant READ_MORE. */
 public static final String READ_MORE = "readMore";
 
 /** The Constant ARTICLE_CLICK. */
 public static final String ARTICLE_CLICK = "articleClick";
 
 /** The Constant RELATED_ARTICLE. */
 public static final String RELATED_ARTICLE = "relatedArticle";
 
 /** The Constant FILTER. */
 public static final String FILTER = "filter";
 
 /** The Constant PRODUCT_LIST_FILTER. */
 public static final String PRODUCT_LIST_FILTER = "productListFilter";
 
 /** The Constant PRODUCT. */
 public static final String PRODUCT = "Product";
 
 /** The Constant TAGS. */
 public static final String TAGS = "tags";
 
 /** The Constant PRODUCT_TAGS. */
 public static final String PRODUCT_TAGS = "productTags";
 
 /** The Constant MAY_LIKE. */
 public static final String MAY_LIKE = "mayLike";
 
 /** The Constant PAGE_URL. */
 public static final String PAGE_URL = "pageurl";
 
 /**
  * Instantiates a new container tag constants.
  */
 private ContainerTagConstants() {
  
 }
 
}
