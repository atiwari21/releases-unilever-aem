/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion.product;

import static com.adobe.cq.commerce.api.CommerceConstants.PN_COMMERCE_TYPE;
import static com.day.cq.commons.jcr.JcrConstants.JCR_LASTMODIFIED;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrObservationThrottle;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.tagging.InvalidTagFormatException;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.jcr.vault.util.Text;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.foundation.commerce.dto.ResultReport;

/**
 * The Class AbstractLoader.
 *
 * @author atiw22
 */
@Component(componentAbstract = true, metatype = true)
@Service
public abstract class AbstractLoader {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(AbstractLoader.class);
 
 /** The event admin. */
 @Reference
 EventAdmin eventAdmin = null;
 
 /** The save batch size. */
 private int saveBatchSize;
 
 /** The Constant DEFAULT_BATCH_SIZE. */
 private static final int DEFAULT_BATCH_SIZE = 1000;
 
 /** The Constant SAVE_BATCH_PROP_NAME. */
 @Property(label = "Save Batch Size", description = "Approximate number of nodes to batch between session saves", intValue = DEFAULT_BATCH_SIZE)
 public static final String SAVE_BATCH_PROP_NAME = "cq.commerce.importer.savebatchsize";
 
 /** The throttle batch size. */
 private int throttleBatchSize;
 
 /** The Constant DEFAULT_THROTTLE_SIZE. */
 private static final int DEFAULT_THROTTLE_SIZE = 50000;
 
 /** The Constant THROTTLE_BATCH_PROP_NAME. */
 @Property(label = "Throttle Batch Size", description = "Approximate number of nodes between pauses for observation manager", intValue = DEFAULT_THROTTLE_SIZE)
 public static final String THROTTLE_BATCH_PROP_NAME = "cq.commerce.importer.throttlebatchsize";
 
 /** The event batch size. */
 private int eventBatchSize = UNICommerceConstants.THOUNSAND;
 
 /** The message cap. */
 private int messageCap;
 
 /** The Constant DEFAULT_MESSAGE_LIMIT. */
 private static final int DEFAULT_MESSAGE_LIMIT = 1000;
 
 /** The Constant MESSAGE_LIMIT_PROP_NAME. */
 @Property(label = "Message Cap", description = "Maximum number of messages to return in response", intValue = DEFAULT_MESSAGE_LIMIT)
 public static final String MESSAGE_LIMIT_PROP_NAME = "cq.commerce.importer.messagecap";
 
 /** The save batch counter. */
 private int saveBatchCounter = 0;
 
 /** The throttle batch counter. */
 private int throttleBatchCounter = 0;
 
 /** The jcr throttle. */
 private JcrObservationThrottle jcrThrottle = null;
 
 /** The event list map. */
 private Map<String, Set<String>> eventListMap = new HashMap<String, Set<String>>();
 
 /** The ticker token message. */
 private String tickerTokenMessage = null;
 
 /** The ticker message value. */
 private String tickerMessageValue;
 
 /** The ticker complete flag. */
 private boolean tickerCompleteFlag;
 
 /** The base path location. */
 protected String basePathLocation = "/etc/commerce/products";
 
 /** The product counter. */
 private int productCounter;
 
 /** The variation counter. */
 private int variationCounter;
 
 /** The starting time. */
 private long startingTime;
 
 /** The messageslist. */
 private List<String> messageslist;
 
 /** The error counter. */
 private int errorCounter;
 
 /** The Constant PRODUCT_MODIFIED. */
 private static final String PRODUCT_MODIFIED = "com/adobe/cq/commerce/pim/PRODUCT_MODIFIED";
 
 /**
  * Activate.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  throttleBatchSize = PropertiesUtil.toInteger(ctx.getProperties().get(THROTTLE_BATCH_PROP_NAME), DEFAULT_THROTTLE_SIZE);
  saveBatchSize = PropertiesUtil.toInteger(ctx.getProperties().get(SAVE_BATCH_PROP_NAME), DEFAULT_BATCH_SIZE);
  messageCap = PropertiesUtil.toInteger(ctx.getProperties().get(MESSAGE_LIMIT_PROP_NAME), DEFAULT_MESSAGE_LIMIT);
 }
 
 /**
  * Initialize data elements.
  */
 protected void initializeDataElements() {
  startingTime = System.currentTimeMillis();
  productCounter = 0;
  variationCounter = 0;
  messageslist = new ArrayList<String>();
  errorCounter = 0;
 }
 
 /**
  * Gets the report.
  *
  * @param r
  *         the r
  * @return the report
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 public ResultReport getReport(ResultReport r) throws IOException {
  
  ResultReport report = r;
  long millis = System.currentTimeMillis() - startingTime;
  long seconds = millis / UNICommerceConstants.THOUNSAND;
  if (seconds > UNICommerceConstants.HUNDRED_TWENTY) {
   LOG.info("Imported " + productCounter + " products in " + seconds / UNICommerceConstants.SIXTY + " minutes.");
  } else {
   LOG.info("Imported " + productCounter + " products in " + seconds + " seconds.");
  }
  
  String summary = productCounter + " products and " + variationCounter + " variants created/updated.";
  if (getErrorCount() > 0) {
   summary += " " + getErrorCount() + " errors encountered.";
  }
  
  if (report == null) {
   report = new ResultReport();
  }
  
  report.setSummary(summary);
  report.setMessageList(messageslist);
  return report;
 }
 
 /**
  * Creates a product node at a given absolute path. Intervening nodes will be created as sling:Folder, as will any bucket nodes required to honor
  * BUCKET_MAX (unless NT_BUCKET has been overridden).
  *
  * <p>
  * Updates the message and event queues; saves the session if the save batch size has been reached.
  * </p>
  *
  * @param path
  *         the path
  * @param session
  *         the session
  * @return the node
  * @throws RepositoryException
  *          the repository exception
  */
 protected Node createProduct(String path, Session session) throws RepositoryException {
  //
  // Fetch or create parent:
  //
  String parentPath = Text.getRelativeParent(path, 1);
  Node parent = JcrUtil.createPath(parentPath, false, JcrResourceConstants.NT_SLING_FOLDER, JcrResourceConstants.NT_SLING_FOLDER, session, false);
  
  //
  // Now carry on creating the product:
  //
  Node product = JcrUtil.createUniqueNode(parent, Text.getName(path), JcrConstants.NT_UNSTRUCTURED, session);
  product.setProperty(PN_COMMERCE_TYPE, "product");
  product.setProperty(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, UNICommerceConstants.RESOURCE_TYPE_PATH);
  product.setProperty(JCR_LASTMODIFIED, Calendar.getInstance());
  
  productCounter++;
  logEvent("com/adobe/cq/commerce/pim/PRODUCT_ADDED", product.getPath());
  logMessage("Created product   " + product.getPath(), false);
  updateTicker(makeTickerMessage());
  
  checkpoint(session, false);
  return product;
 }
 
 /**
  * Make ticker message.
  *
  * @return the string
  */
 protected String makeTickerMessage() {
  return productCounter + " products imported/updated";
 }
 
 /**
  * Update ticker.
  *
  * @param tickerMessage
  *         the ticker message
  */
 protected void updateTicker(String tickerMessage) {
  this.tickerMessageValue = tickerMessage;
 }
 
 /**
  * Log event.
  *
  * @param eventName
  *         the event name
  * @param path
  *         the path
  */
 protected void logEvent(String eventName, String path) {
  Set<String> paths = eventListMap.get(eventName);
  if (paths == null) {
   paths = new HashSet<String>();
   eventListMap.put(eventName, paths);
  }
  paths.add(path);
 }
 
 /**
  * Log message.
  *
  * @param message
  *         the message
  * @param isError
  *         the is error
  */
 protected void logMessage(String message, boolean isError) {
  if (messageslist.size() < messageCap) {
   messageslist.add(message);
  }
  if (isError) {
   errorCounter++;
  }
 }
 
 /**
  * Should be called after each node creation. The flush parameter can be used to force session saving and event dispatch (for instance at the end of
  * an import), otherwise saves and event dispatch are batched.
  *
  * @param session
  *         the session
  * @param flush
  *         the flush
  */
 protected void checkpoint(Session session, boolean flush) {
  saveBatchCounter++;
  throttleBatchCounter++;
  
  if (saveBatchCounter > saveBatchSize || flush) {
   setCheckpoint(session);
  }
  
  for (String eventName : eventListMap.keySet()) {
   setEvent(eventName, flush);
  }
 }
 
 /**
  * Sets the event.
  *
  * @param eventName
  *         the event name
  * @param flush
  *         the flush
  */
 private void setEvent(String eventName, boolean flush) {
  Set<String> paths = eventListMap.get(eventName);
  if (paths.size() > eventBatchSize || (flush && !(paths.isEmpty()))) {
   if (eventAdmin != null) {
    Map<String, Object> eventProperties = new HashMap<String, Object>();
    eventProperties.put("paths", paths.toArray(new String[paths.size()]));
    eventAdmin.postEvent(new Event(eventName, eventProperties));
   }
   paths.clear();
  }
 }
 
 /**
  * Sets the checkpoint.
  *
  * @param session
  *         the new checkpoint
  */
 private void setCheckpoint(Session session) {
  if (StringUtils.isNotEmpty(tickerTokenMessage)) {
   try {
    Node node = JcrUtils.getOrCreateByPath("/tmp/commerce/tickers/import_" + tickerTokenMessage, JcrConstants.NT_UNSTRUCTURED, session);
    node.setProperty("message", tickerMessageValue);
    node.setProperty("errorCount", errorCounter);
    node.setProperty("complete", tickerCompleteFlag);
   } catch (Exception e) {
    LOG.error("ERROR updating ticker", e);
   }
  }
  
  try {
   session.save();
   saveBatchCounter = 0;
  } catch (Exception e) {
   logMessage("ERROR saving session", false);
   errorCounter += saveBatchCounter;
   LOG.error("ERROR saving session", e);
  }
  
  if (throttleBatchCounter > throttleBatchSize) {
   try {
    jcrThrottle.waitForEvents();
    throttleBatchCounter = 0;
   } catch (RepositoryException e) {
    // keep calm and carry on...
    LOG.error("Error during checkpoint : ", e);
   }
  }
 }
 
 /**
  * Mangle name.
  *
  * @param name
  *         the name
  * @return the string
  */
 protected static String mangleName(String name) {
  return StringUtils.isEmpty(name) ? "" : JcrUtil.createValidName(name.trim().replace(" ", "-"));
 }
 
 /**
  * Should be called after updating a product.
  *
  * <p>
  * Updates the message and event queues; saves the session if the save batch size has been reached.
  * </p>
  *
  * @param product
  *         the product
  * @throws RepositoryException
  *          the repository exception
  */
 protected void productUpdated(Node product) throws RepositoryException {
  productCounter++;
  product.setProperty(JCR_LASTMODIFIED, Calendar.getInstance());
  logEvent(PRODUCT_MODIFIED, product.getPath());
  logMessage("Updated product   " + product.getPath(), false);
  updateTicker(makeTickerMessage());
  checkpoint(product.getSession(), false);
 }
 
 /**
  * Should be called after deleting a product. *
  * <p>
  * Updates the message and event queues; saves the session if the save batch size has been reached.
  * </p>
  *
  * @param product
  *         the product
  * @throws RepositoryException
  *          the repository exception
  */
 protected void productDeleted(Node product) throws RepositoryException {
  productCounter++;
  logEvent("com/adobe/cq/commerce/pim/PRODUCT_DELETED", product.getPath());
  logMessage("Deleted product " + product.getPath(), false);
  updateTicker(makeTickerMessage());
  checkpoint(product.getSession(), false);
 }
 
 protected void variantDeleted(Node variant) throws RepositoryException {
  variationCounter++;
  Node baseProduct = getBaseProduct(variant);
  if (baseProduct != null) {
   logEvent(PRODUCT_MODIFIED, baseProduct.getPath());
  }
  logMessage("Deleted variation " + variant.getPath(), false);
  updateTicker(makeTickerMessage());
  checkpoint(variant.getSession(), false);
 }
 
 /**
  * Creates a variation with a given name within a given product.
  *
  * <p>
  * Updates the message and event queues; saves the session if the save batch size has been reached.
  * </p>
  *
  * @param parentProduct
  *         the parent product
  * @param name
  *         the name
  * @return the node
  * @throws RepositoryException
  *          the repository exception
  */
 protected Node createVariant(Node parentProduct, String name) throws RepositoryException {
  Node variant = JcrUtil.createUniqueNode(parentProduct, name, JcrConstants.NT_UNSTRUCTURED, parentProduct.getSession());
  variant.setProperty(PN_COMMERCE_TYPE, "variant");
  variant.setProperty(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, UNICommerceConstants.RESOURCE_TYPE_PATH);
  variant.setProperty(JCR_LASTMODIFIED, Calendar.getInstance());
  
  variationCounter++;
  Node baseProduct = getBaseProduct(parentProduct);
  if (baseProduct != null) {
   logEvent(PRODUCT_MODIFIED, baseProduct.getPath());
  }
  logMessage("Created variation " + variant.getPath(), false);
  checkpoint(parentProduct.getSession(), false);
  
  return variant;
 }
 
 /**
  * Should be called after updating a variant.
  *
  * <p>
  * Updates the message and event queues; saves the session if the save batch size has been reached.
  * </p>
  *
  * @param variant
  *         the variant
  * @throws RepositoryException
  *          the repository exception
  */
 protected void variantUpdated(Node variant) throws RepositoryException {
  variationCounter++;
  variant.setProperty(JCR_LASTMODIFIED, Calendar.getInstance());
  Node baseProduct = getBaseProduct(variant);
  if (baseProduct != null) {
   logEvent(PRODUCT_MODIFIED, baseProduct.getPath());
  }
  logMessage("Updated variation " + variant.getPath(), false);
  updateTicker(makeTickerMessage());
  checkpoint(variant.getSession(), false);
 }
 
 /**
  * Creates the asset folder.
  *
  * @param node
  *         the node
  * @param resourceResolver
  *         the resource resolver
  * @return the node
  * @throws RepositoryException
  *          the repository exception
  */
 protected Node createAssetFolder(Node node, ResourceResolver resourceResolver, String folderName) throws RepositoryException {
  Node assets = JcrUtil.createPath(node.getPath() + "/" + folderName, false, JcrResourceConstants.NT_SLING_FOLDER,
    JcrResourceConstants.NT_SLING_FOLDER, resourceResolver.adaptTo(Session.class), false);
  assets.setProperty(JcrConstants.JCR_LASTMODIFIED, Calendar.getInstance());
  
  return assets;
 }
 
 /**
  * Creates the image.
  *
  * @param product
  *         the product
  * @param url
  *         the url
  * @param id
  *         the id
  * @param text
  *         the text
  * @param caption
  *         the caption
  * @param suffix
  *         the suffix
  * @return the node
  * @throws RepositoryException
  *          the repository exception
  */
 protected Node createImage(Node product, String url, String suffix, String altText, String videoId) throws RepositoryException {
  Node image = product.addNode("asset" + suffix, JcrConstants.NT_UNSTRUCTURED);
  image.setProperty(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, "commerce/components/product/image");
  image.setProperty(JCR_LASTMODIFIED, Calendar.getInstance());
  image.setProperty("fileReference", url);
  image.setProperty("randomNumber", "72");
  image.setProperty("altText", altText);
  image.setProperty("videoId", videoId);
  
  Node baseProduct = getBaseProduct(product.getParent());
  if (baseProduct != null) {
   logEvent(PRODUCT_MODIFIED, baseProduct.getPath());
  }
  if (url.equals(StringUtils.EMPTY)) {
   logMessage("Warning:Correct image format not available for the Product", false);
  } else {
   logMessage("Created image     " + image.getPath(), false);
  }
  checkpoint(product.getSession(), false);
  
  return image;
 }
 
 /**
  * Returns the base product node of a product or variation.
  *
  * @param node
  *         the node
  * @return the base product
  * @throws RepositoryException
  *          the repository exception
  */
 protected Node getBaseProduct(Node node) throws RepositoryException {
  
  Node n = node;
  while (n != null && !n.getProperty(PN_COMMERCE_TYPE).getString().equals(UNICommerceConstants.PRODUCT)) {
   n = n.getParent();
  }
  return n;
 }
 
 /**
  * Gets the error count.
  *
  * @return the error count
  */
 protected int getErrorCount() {
  return errorCounter;
 }
 
 /**
  * Creates the missing tags.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param tags
  *         the tags
  */
 protected void createMissingTags(ResourceResolver resourceResolver, String[] tags) {
  TagManager tm = resourceResolver.adaptTo(TagManager.class);
  for (String tag : tags) {
   try {
    if (tm.canCreateTag(tag)) {
     logMessage("Invalid tag : " + tag, true);
    }
    Tag t = tm.resolve(tag);
    if (t == null) {
     logMessage("Invalid tag : " + tag, true);
    }
   } catch (InvalidTagFormatException e) {
    LOG.error("Invalid tag ID", e);
   }
  }
 }
 
 /**
  * Update logged events.
  *
  * @param oldPath
  *         the old path
  * @param newPath
  *         the new path
  */
 protected void updateLoggedEvents(String oldPath, String newPath) {
  for (String eventName : eventListMap.keySet()) {
   Set<String> paths = eventListMap.get(eventName);
   if (paths.remove(oldPath)) {
    paths.add(newPath);
   }
  }
 }
 
}
