/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion;

import java.io.IOException;

import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONArray;

import com.unilever.platform.foundation.commerce.dto.ResultReport;

/**
 * The Interface ILoader.
 *
 * Interface for Loader class
 */
public interface ILoader {
 
 /**
  * Saves the JSON data to CRX as product nodes.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storePath
  *         the store path
  * @param jsonarray
  *         the jsonarray
  * @param oldIngestion
  * @return the result report
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 ResultReport saveProducts(ResourceResolver resourceResolver, String storePath, JSONArray jsonarray, boolean oldIngestion) throws IOException;
}
