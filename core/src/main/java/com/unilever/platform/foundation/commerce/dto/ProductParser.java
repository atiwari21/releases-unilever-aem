/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.dto;

/**
 * The Class ProductParser.
 *
 * @author atiw22
 */
public class ProductParser {
 
 /** The column name. */
 private String columnName;
 
 /** The key name. */
 private String keyName;
 
 /** The property name. */
 private String propertyName;
 
 /** The index. */
 private int index;
 
 /**
  * Gets the column name.
  *
  * @return the columnName
  */
 public String getColumnName() {
  return columnName;
 }
 
 /**
  * Sets the column name.
  *
  * @param columnName
  *         the columnName to set
  */
 public void setColumnName(String columnName) {
  this.columnName = columnName;
 }
 
 /**
  * Gets the key name.
  *
  * @return the keyName
  */
 public String getKeyName() {
  return keyName;
 }
 
 /**
  * Sets the key name.
  *
  * @param keyName
  *         the keyName to set
  */
 public void setKeyName(String keyName) {
  this.keyName = keyName;
 }
 
 /**
  * Gets the property name.
  *
  * @return the propertyName
  */
 public String getPropertyName() {
  return propertyName;
 }
 
 /**
  * Sets the property name.
  *
  * @param propertyName
  *         the propertyName to set
  */
 public void setPropertyName(String propertyName) {
  this.propertyName = propertyName;
 }
 
 /**
  * Gets the index.
  *
  * @return the index
  */
 public int getIndex() {
  return index;
 }
 
 /**
  * Sets the index.
  *
  * @param index
  *         the index to set
  */
 public void setIndex(int index) {
  this.index = index;
 }
}
