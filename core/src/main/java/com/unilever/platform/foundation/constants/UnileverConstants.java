/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.constants;

/**
 * The Class UnileverConstants.
 */
public class UnileverConstants {
 
 /** The Constant AJAX_URL_SELCTOR. */
 public static final String AJAX_URL_SELCTOR = "data.ajax.json";
 
 /** The Constant AJAX_URL_KEY. */
 public static final String AJAX_URL_KEY = "ajaxURL";
 
 /** The Constant BRAND_NAME_KEY. */
 public static final String BRAND_NAME_KEY = "brandName";
 
 /** The Constant MARKET. */
 public static final String MARKET = "market";
 /** The Constant LOCALE. */
 public static final String LOCALE = "locale";
 
 /** The Constant SHOW_AS_NAVIGATION. */
 public static final String SHOW_AS_NAVIGATION = "showAsNavigation";
 
 /** The Constant SHOW_AS_NAVIGATION. */
 public static final String THUMBNAIL_IMAGE_SUFFIX = ".thumb.100.140.png";
 
 /** The Constant LABEL. */
 public static final String LABEL = "label";
 
 /** The Constant ANCHOR_NAVIGATION. */
 public static final String ANCHOR_NAVIGATION = "anchorLinkNavigation";
 
 /** The Constant MAX. */
 public static final String MAX = "max";
 
 /** The Constant LIMIT. */
 public static final String LIMIT = "limit";
 
 /** The Constant URL. */
 public static final String URL = "url";
 
 /** The Constant ASCENDING. */
 public static final String ASCENDING = "asc";
 
 /** The Constant DESCENDING. */
 public static final String DESCENDING = "desc";
 
 /** The Constant TITLE. */
 public static final String TITLE = "title";
 
 /** The Constant RANDOM_NUMBER. */
 public static final String RANDOM_NUMBER = "randomNumber";
 
 /** The Constant ANALYTICS_ACTION_KEY. */
 public static final String GLOBAL_CONFIG_ANALYTIC_ACTION = "analyticAction";
 
 /** The Constant ANALYTICS_CATEGORY_KEY. */
 public static final String GLOBAL_CONFIG_ANALYTIC_CATEGORY = "analyticCategory";
 
 /** The Constant ID. */
 public static final String ID = "id";
 
 /** The Constant PRODUCT. */
 public static final String PRODUCT = "Product";
 
 /** The Constant ARTICLE. */
 public static final String ARTICLE = "Article";
 
 /** The Constant SOCIAL_ARTICLE. */
 public static final String SOCIAL_ARTICLE = "Social Article";
 
 /** The Constant GLOBAL_CONFIGURATION_CATEGORY_NAME. */
 public static final String GLOBAL_CONFIGURATION_CATEGORY_NAME = "globalConfigCatName";
 
 /** The Constant CONTAINER_TAG. */
 public static final String CONTAINER_TAG = "containertag";
 
 /** The Constant CONTAINER_TAG_CATEGORY. */
 public static final String CONTAINER_TAG_CATEGORY = "category";
 
 /** The Constant CONTAINER_TAG_ACTION. */
 public static final String CONTAINER_TAG_ACTION = "action";
 
 /** The Constant CONTAINER_TAG_INFORMATION. */
 public static final String CONTAINER_TAG_INFORMATION = "information";
 
 /** The Constant CONTAINER_TAG_ATTR. */
 public static final String CONTAINER_TAG_ATTR = "attr";
 
 /** The Constant IS_RATING_AND_REVIEW_ENABLED. */
 public static final String IS_RATING_AND_REVIEW_ENABLED = "isRatingAndReviewEnabled";
 
 /** The Constant RATING_AND_REVIEW_WIDGET_TYPE. */
 public static final String RATING_AND_REVIEW_WIDGET_TYPE = "ratingAndReviewWidgetType";
 
 /** The Constant HOME_PAGE_TEMPLATE_NAME. */
 public static final String HOME_PAGE_TEMPLATE_NAME = "homePageTemplate";
 
 /** The Constant PRODUCT_TEMPLATE_NAME. */
 public static final String PRODUCT_TEMPLATE_NAME = "productDetailPageTemplate";
 
 /** The Constant PRODUCT_RANGE_TEMPLATE_NAME. */
 public static final String PRODUCT_RANGE_TEMPLATE_NAME = "productRangeLandingTemplate";
 
 /** The Constant ARTICLE_TEMPLATE_NAME. */
 public static final String ARTICLE_TEMPLATE_NAME = "articlePageTemplate";
 
 /** The Constant content. */
 public static final String CONTENT = "/content";
 /** The Constant is not adaptive property. */
 public static final String IS_NOT_ADAPTIVE = "isNotAdaptive ";
 
 /** The Constant BODY_COPY_HEADLINE. */
 public static final String BODY_COPY_HEADLINE = "bodycopyheadline";
 
 /** The Constant ARTICLE_TEASER_TITLE. */
 public static final String TEASER_TITLE = "teaserTitle";
 
 /** The Constant ARTICLE_TEASER_COPY. */
 public static final String TEASER_COPY = "teaserCopy";
 
 /** The Constant ARTICLE_TEASER_IMAGE. */
 public static final String TEASER_IMAGE = "teaserImage";
 
 /** The Constant TEASER_IMAGE_ALT_TEXT. */
 public static final String TEASER_IMAGE_ALT_TEXT = "teaserAltText";
 
 /** The Constant TEASER LONG COPY. */
 public static final String TEASER_LONG_COPY = "teaserLongCopy";
 
 /** The Constant ARTICLE_LAUNCH_DATE. */
 public static final String PUBLISH_DATE = "publishDate";
 
 /** The Constant ARTICLE_LAUNCH_DATE. */
 public static final String LAST_REVIEW_DATE = "lastReviewDate";
 
 /** The Constant RANGE_IMAGE. */
 public static final String RANGE_IMAGE = "rangeImage";
 
 /** The Constant SIX. */
 public static final int SIX = 6;
 
 /** The Constant for "/". */
 public static final String FORWARD_SLASH = "/";
 
 /** The Constant for "/". */
 public static final String HYPHEN_PARAM = "-";
 
 /** Constant for image. */
 public static final String IMAGE = "image";
 
 /** Constant for comma. */
 public static final String COMMA = ",";
 
 /** Constant for jpeg. */
 public static final String JPEG = "jpeg";
 
 /** /** Constant for jpg. */
 public static final String JPG = "jpg";
 
 /** Constant for jpeg. */
 public static final String GIF = "gif";
 
 /** Constant for jpeg. */
 public static final String PNG = "png";
 
 /** Constant for jpeg. */
 public static final String SPACE = " ";
 
 /** The Constant IS_NEW_DAYS_DIFFERENCE. */
 public static final String IS_NEW_PRODUCT_LAUNCH_DATE_LIMIT = "isNewProductLaunchDateLimit";
 
 /** The Constant PRODUCT_CATEGORY. */
 public static final String PRODUCT_CATEGORY = "product";
 
 /** The Constant FILTER_TAG_NAME_SPACE. */
 public static final String FILTER_TAG_NAME_SPACE = "filterTagsNamespace";
 
 /** The Constant JCR_CONTENT_CONTENT_TYPE. */
 public static final String JCR_CONTENT_CONTENT_TYPE = "jcr:content/contentType";
 
 /** The Constant JCR_SUB_CONTENT_CONTENT_TYPE. */
 public static final String JCR_SUB_CONTENT_CONTENT_TYPE = "jcr:content/subContentType";
 
 /** The Constant INCLUDE_LIST. */
 public static final String INCLUDE_LIST = "includeList";
 
 /** The Constant EXCLUDE_LIST. */
 public static final String EXCLUDE_LIST = "excludeList";
 
 /** The Constant X_FORWARD_PROTO. */
 public static final String X_FORWARD_PROTO_VAR = "X-Forwarded-AEM-Proto";
 
 /** The Constant CONTENT_TYPE. */
 public static final String CONTENT_TYPE = "contentType";
 
 /** The Constant CQ_TEMPLATE. */
 public static final String CQ_TEMPLATE = "/apps/dove/templates/productRangeLandingTemplate";
 
 /** The Constant JCR_CONTENT_CQ_TEMPLATE. */
 public static final String JCR_CONTENT_CQ_TEMPLATE = "jcr:content/cq:template";
 
 /** The Constant SOCIAL_CHANNELS. */
 public static final String SOCIAL_CHANNELS = "socialChannels";
 
 /** The Constant EGIFTING_CONFIG_CAT. */
 public static final String EGIFTING_CONFIG_CAT = "egifting";
 
 /** The Constant PRODUCT_PATH_CONFIG_KEY. */
 public static final String PRODUCT_PATH_CONFIG_KEY = "productPath";
 
 /** The Constant ERROR_MSG. */
 public static final String ERROR_MSG = "errormsg";
 
 /** The Constant DEFAULT_VALUE. */
 public static final String DEFAULT_VALUE = "defaultValue";
 
 /** The Constant FORM_ELEMENT. */
 public static final String FORM_ELEMENT = "formElement";
 
 /** The Constant ENCRYPT_MODE. */
 public static final String ENCRYPT_MODE = "encrypt";
 
 /** The Constant DCRYPT_MODE. */
 public static final String DECRYPT_MODE = "decrypt";
 
 /** The Constant DESCRIPTION. */
 public static final String DESCRIPTION = "description";
 
 /** The Constant Article Flags. */
 public static final String ARTICLE_FLAGS = "flags";
 
 /** The Constant ARTICLE_TAG_FLAG_NAMESPACE. */
 public static final String ARTICLE_FLAG_NAMESPACE = "flagsNamespace";
 
 /** The Constant SOCIAL_GALLERY_TAB_CONFIG_CAT. */
 public static final String SOCIAL_GALLERY_TAB_CONFIG_CAT = "socialGalleryTAB";
 
 /** The Constant SOCIAL_GALLERY_CAROUSEL_TAB_CONFIG_CAT. */
 public static final String SOCIAL_GALLERY_CAROUSEL_TAB_CONFIG_CAT = "socialGalleryCarouselTAB";
 
 /** The Constant SOCIAL_GALLERY_CAROUSEL_EXPANDED_TAB_CONFIG_CAT. */
 public static final String SOCIAL_GALLERY_CAROUSEL_EXPANDED_TAB_CONFIG_CAT = "socialGalleryCarouselExpandedTAB";
 
 /** The Constant MULTIPLE_RELATED_TWEETS_TAB_CONFIG_CAT. */
 public static final String MULTIPLE_RELATED_TWEETS_TAB_CONFIG_CAT = "multipleRelatedTweetsTAB";
 
 /** The Constant MULTIPLE_RELATED_TWEETS_TAB_CONFIG_CAT. */
 public static final String MULTIPLE_RELATED_TWEETS_CONFIG_CAT = "multipleRelatedTweets";
 
 /** The Constant SOCIAL_TAB_BASE_CONFIG_CAT. */
 public static final String SOCIAL_TAB_BASE_CONFIG_CAT = "socialTabBase";
 
 /** The Constant SOCIAL_GALLERY_VIDEO_CONFIG_CAT. */
 public static final String SOCIAL_GALLERY_VIDEO_CONFIG_CAT = "socialGalleryVideo";
 
 /** The Constant HTML_SNIPPET. */
 public static final String HTML_SNIPPET = "htmlSnippet";
 
 /** The Constant "Social Aggregator Service". */
 public static final String SOCIAL_AGGREGATOR_SERVICE = "socialAggregatorService";
 
 /** The Constant SOCIAL_CAROUSEL_CAT. */
 public static final String SOCIAL_CAROUSEL_CAT = "socialCarousel";
 
 /** The Constant RECIPE_URL. */
 public static final String RECIPE_URL = "recipeUrl";
 
 public static final String END_POINT = "endPoint";
 
 /** The Constant BRAND. */
 public static final String BRAND = "brand";
 
 /** The Constant LOCALE_NAME. */
 public static final String LOCALE_NAME = "locale";
 
 /** The Constant TIME_OUT. */
 public static final String TIME_OUT = "timeout";
 
 /** The Constant RMS_END_POINT. */
 public static final String RMS_END_POINT = "rmsEndPoint";
 
 /** The Constant SOCIAL_CAROUSEL_CAT. */
 public static final String RECIPES = "recipes";
 
 /** The Constant SOCIAL_CAROUSEL_CAT. */
 public static final String RECIPE = "recipe";
 
 /** The Constant ID. */
 public static final String RECIPE_ID = "recipeId";
 
 /** The Constant MATCHING_TAGS. */
 public static final String MATCHING_TAGS = "matchingTags";
 
 /** The Constant INCLUSION_TAGS. */
 public static final String INCLUSION_TAGS = "inclusionTags";
 
 /** The Constant EXCLUSION_TAGS. */
 public static final String EXCLUSION_TAGS = "exclusionTags";
 
 /** The Constant PROMOTION_TAGS. */
 public static final String PROMOTION_TAGS = "promotionTags";
 /**
  * Constants for retrieving TAG_SOURCE value from dialog fields
  */
 public static final String TAG_SOURCE = "tagSource";
 /**
  * Constants for retrieving ORDER_BY value from dialog fields
  */
 public static final String ORDER_BY = "orderBy";
 /**
  * Constants for retrieving OVERRIDE_GLOBAL_CONFIG value from dialog fields
  */
 public static final String OVERRIDE_GLOBAL_CONFIG = "overrideGlobalConfig";
 /**
  * Constants for retrieving CURRENT_PAGE_TAGS value from dialog fields
  */
 public static final String CURRENT_PAGE_TAGS = "currentPageTags";
 /**
  * Constants for retrieving SEARCH_TAGS value from dialog fields
  */
 public static final String SEARCH_TAGS = "searchTags";
 /** The Constant product launch date. */
 public static final String PRODUCT_LAUNCH_DATE = "date";
 /** The Constant please select. */
 public static final String PLEASE_SELECT = "Please Select";
 
 public static final String LONG_COPY = "longCopy";
 
 /** The Constant OPEN_IN_NEW_WINDOW. */
 public static final String OPEN_IN_NEW_WINDOW = "openInNewWindow";
 
 public static final String FILTER_PARENT_HEADING = "filterParentHeading";
 
 public static final String VALUE = "value";
 
 public static final String FILTER_OPTIONS = "filterOptions";
 
 public static final String FILTERS = "filters";
 
 public static final String FILTER_PARENT_TAG_ONE = "filterParentTagOne";
 
 public static final String CLASS = "class";
 
 public static final String DISPLAY_FORMAT = "displayFormat";
 
 public static final String SUB_HEADING = "subHeading";
 
 public static final String HEADING = "heading";
 
 public static final String CSS_STYLE_CLASS = "cssStyleClass";
 
 public static final String FILTER_DEFAULT_VALUE = "filterDefaultValue";
 
 public static final String FILTER_DISPLAY_FORMAT = "filterDisplayFormat";
 
 public static final String FILTER_SUB_HEADING = "filterSubHeading";
 
 public static final String FILTER_HEADING = "filterHeading";
 
 public static final String RESULT_SUFFIX = "resultSuffix";
 
 public static final String RESULT_SUFFIX_TEXT = "resultSuffixText";
 
 public static final String FILTER_OPTION_JSON = "filterOptionJson";
 
 public static final String PAGE_PATH = "pagePath";
 public static final String FILTER_PARENT_TAG = "filterParentTag";
 public static final String DOT = ".";
 public static final String CONTENT_TYPE_JSON = "contentTypeJson";
 public static final String SHORT_SUB_HEADING = "shortSubheading";
 public static final String LONG_SUB_HEADING = "longSubheading";
 public static final String BACKGROUND_IMAGE = "backgroundImage";
 public static final String FOREGROUND_IMAGE = "foregroundImage";
 public static final String CTA_LABEL = "ctaLabel";
 public static final String TEXT = "text";
 public static final String DISPLAY_TYPE = "displayType";
 public static final String VIEW_TYPE = "viewType";
 /** The Constant STORE_LOCATOR_URL. */
 public static final String STORE_LOCATOR_URL = "storeLocatorUrl";
 /** The Constant PRODUCT_UPC_CODE. */
 public static final String PRODUCT_UPC_CODE = "productUpcCode";
 /** The Constant POSTAL_CODE. */
 public static final String POSTAL_CODE = "postalCode";
 
 /** The Constant UNIQUE_PAGE_ID. */
 public static final String UNIQUE_PAGE_ID = "uniquePageId";
 
 /** The Constant SUB_CONTENT_TYPE. */
 public static final String SUB_CONTENT_TYPE = "subContentType";
 
 /** The Constant VIDEO_FILTER. */
 public static final String VIDEO_FILTER = "videoFilter";
 
 /** The Constant EXPERIENCE_PAGE. */
 public static final String EXPERIENCE_PAGE = "experiencepage";
 
 /** The Constant ENABLED_FEATURE_TAG_INTERACTION. */
 public static final String IS_ENABLED_FEATURE_TAG_INTERACTION = "isFeatureTagInteractionEnabled";
 
 /**
  * Instantiates a new unilever constants.
  */
 private UnileverConstants() {
  
 }
}
