/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.workflow;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Value;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
//Sling Imports
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.NameConstants;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.utils.ComponentUtil;

/**
 * The Class LauncherWorkflow.
 */
// This is a component so it can provide or consume services
@Component
@Service
@Properties({ @Property(name = Constants.SERVICE_DESCRIPTION, value = "market workflow process implementation"),
  @Property(name = Constants.SERVICE_VENDOR, value = "Adobe"), @Property(name = "process.label", value = "Market Launcher Workflow") })
public class LauncherWorkflow implements WorkflowProcess {
 
 private static final Logger LOGGER = LoggerFactory.getLogger(LauncherWorkflow.class);
 
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 @Reference
 private ConfigurationAdmin configAdmin;
 @Reference
 private SlingRequestProcessor requestProcessor;
 @Reference
 private RequestResponseFactory requestResponseFactory;
 @Reference
 Replicator replicator;
 private static final String THUMB_PATH_SPLIT = "\\/";
 private static final String MULTI_PRODUCT_TEMPLATE = "products";
 private static final String MULTI_PRODUCT_MODEL = "/etc/workflow/models/adding-multiple-products-workflow/jcr:content/model";
 private static final String CREATE_PAGE_MODEL = "/etc/workflow/models/create-a-new-article-page-workflow/jcr:content/model";
 private static final String PROP_BRAND_NAME = "brandName";
 private static final String PROP_DUE_DATE = "dueDate";
 private static final String PROP_MULTI_BRANDS = "multipleBrands";
 private static final String PROP_BRAND_CONFIGURATION = "brandsConfiguration";
 private static final String MULTI_PRODUCT_WORKFLOW = "Adding Multiple new Products";
 private static final String CREATE_PAGE_WORKFLOW = "Create a new article Page";
 private static final String EMPTY_STRING = "";
 private static final String MARKET_WORKFLOW_CONFIG = "com.unilever.platform.aem.foundation.core.service.impl.MarketWorkflowConfigImpl";
 private static final int TWO = 2;
 private static final int THREE = 3;
 private static final int FOUR = 4;
 ResourceResolver resourceResolver;
 
 /**
  * 
  * @param item
  * @param wfsession
  * @param args
  * @throws WorkflowException
  */
 public void execute(WorkItem item, WorkflowSession wfsession, MetaDataMap args) throws WorkflowException {
  
  try {
   
   resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
   
   String projectThumbPath = item.getWorkflowData().getPayload().toString();
   String[] parts = projectThumbPath.split(THUMB_PATH_SPLIT);
   String projectPath = "/" + parts[1] + "/" + parts[TWO] + "/" + parts[THREE] + "/" + parts[FOUR];
   String model = EMPTY_STRING;
   String workflowName = EMPTY_STRING;
   Resource contentFolder = resourceResolver.getResource(projectPath).getChild(JcrConstants.JCR_CONTENT);
   Node contentNode = contentFolder.adaptTo(Node.class);
   String projTemplate = contentNode.getProperty(NameConstants.NN_TEMPLATE).getString();
   if (projTemplate.contains(MULTI_PRODUCT_TEMPLATE)) {
    model = MULTI_PRODUCT_MODEL;
    workflowName = MULTI_PRODUCT_WORKFLOW;
   } else {
    model = CREATE_PAGE_MODEL;
    workflowName = CREATE_PAGE_WORKFLOW;
   }
   
   List<Value> brandsTemp = new ArrayList<>();
   if (!contentNode.getProperty(PROP_BRAND_NAME).isMultiple()) {
    brandsTemp.add(contentNode.getProperty(PROP_BRAND_NAME).getValue());
   } else {
    brandsTemp.addAll(Arrays.asList(contentNode.getProperty(PROP_BRAND_NAME).getValues()));
   }
   
  
   String brandNamemultiField = "";
   String dueDatemultiField = "";
   List<Map<String, String>> linkPostsList = ComponentUtil.getNestedMultiFieldProperties(contentFolder, PROP_BRAND_CONFIGURATION);
   int count = 0;
   if (linkPostsList != null) {
    for (Map<String, String> map : linkPostsList) {
     count++;
     String[] brandNamesMap = MapUtils.getString(map, PROP_MULTI_BRANDS, StringUtils.EMPTY).replace("}", "").replace("{", "").replace("[", "")
       .replace("]", "").split(",");
     String dueDateMap = MapUtils.getString(map, PROP_DUE_DATE, StringUtils.EMPTY);
     for (int i = 0; i < brandNamesMap.length; i++) {
      brandNamemultiField = brandNamemultiField + brandNamesMap[i].split(":")[1].replaceAll("\"", "") + ">";
      dueDatemultiField = dueDatemultiField + dueDateMap + ">";
     }
     
     if (count != linkPostsList.size()) {
      brandNamemultiField = brandNamemultiField + "#";
      dueDatemultiField = dueDatemultiField + "#";
     }
     
    }
   }
   
   String[] brandNames = new String[brandsTemp.size()];
   String[] dueDates = new String[brandsTemp.size()];
   brandNames = replaceCharsInLoop(brandNamemultiField.split("#"), true);
   dueDates = replaceCharsInLoop(dueDatemultiField.split("#"), true);
   
   List<String> listbrandName = new ArrayList<String>(Arrays.asList(brandNames));
   listbrandName.removeAll(Collections.singleton(null));
   brandNames = listbrandName.toArray(new String[listbrandName.size()]);
   
   List<String> listdueDate = new ArrayList<String>(Arrays.asList(dueDates));
   listdueDate.removeAll(Collections.singleton(null));
   dueDates = listdueDate.toArray(new String[listdueDate.size()]);
   
   brandNames = replaceCharsInLoop(brandNames, false);
   dueDates = replaceCharsInLoop(dueDates, false);
   
   Object envNameTemp = GlobalConfigurationUtility.getConfig(configAdmin, "envName", MARKET_WORKFLOW_CONFIG);
   
   String envName = envNameTemp.toString();
   for (int i = 0; i < brandNames.length; i++) {
    String url = projectPath;
    Map<String, Object> urlParameters = new HashMap<String, Object>();
    urlParameters.put("project", projectPath);
    urlParameters.put("modelId", model);
    urlParameters.put("linkType", "cq/gui/components/projects/admin/card/launchcard");
    urlParameters.put("workflowTitle", workflowName + "-" + brandNames[i]);
    urlParameters.put("contentPath", "/content/" + brandNames[i].toLowerCase().split("-")[0] + "/" + brandNames[i].toLowerCase().split("-")[1]);
    urlParameters.put("projectPath", projectPath);
    urlParameters.put("assignee",
      brandNames[i].toLowerCase().split("-")[0] + "_" + brandNames[i].toLowerCase().split("-")[1] + "_" + envName + "_" + "author");
    urlParameters.put(":operation", "startWorkflow");
    urlParameters.put("taskPriority", "Medium");
    urlParameters.put("taskDueDate", dueDates[i]);
    String response = initiateWorkflow(url, resourceResolver, urlParameters);
    LOGGER.info("Response received" + response);
    
   }
   resourceResolver.commit();
  } catch (Exception e) {
   LOGGER.error("Error while executing workflows :", e.getMessage() + "\n Exception Trace " + e);
   
  } finally {
   resourceResolver.close();
   
  }
 }
 
 /**
  * Replace chars in loop.
  *
  * @param arrayName
  *         the array name
  * @param checkSplit
  *         the checkSplit
  * @return the string[]
  */
 private String[] replaceCharsInLoop(String[] arrayName, boolean checkSplit) {
  String[] array = new String[arrayName.length];
  for (int i = 0; i < arrayName.length; i++) {
   if (checkSplit) {
    array = (String[]) ArrayUtils.addAll(array, arrayName[i].split(">"));
   } else {
    arrayName[i].replaceAll("\\s", "");
    arrayName[i].replaceAll("\"", "");
   }
  }
  if(!checkSplit){
   return arrayName;
  }
  return array;
 }
 
 /**
  * 
  * @param url
  * @param resolver
  * @param map
  * @throws BadLocationException
  *          , InterruptedException
  * @return
  */
 private String initiateWorkflow(String url, ResourceResolver resolver, Map<String, Object> map) throws BadLocationException, InterruptedException {
  EditorKit kit = new HTMLEditorKit();
  Document doc = kit.createDefaultDocument();
  doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
  String text = StringUtils.EMPTY;
  HttpServletRequest httpServletRequest = requestResponseFactory.createRequest("POST", url, map);
  httpServletRequest.setAttribute("Accept-Language", "en_us");
  httpServletRequest.setAttribute("Accept_Charset", "utf-8");
  ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
  HttpServletResponse httpServletResponse = requestResponseFactory.createResponse(arrayOutputStream);
  try {
   requestProcessor.processRequest(httpServletRequest, httpServletResponse, resolver);
   InputStream isFromFirstData = new ByteArrayInputStream(arrayOutputStream.toByteArray());
   Reader rd = new BufferedReader(new InputStreamReader(isFromFirstData));
   kit.read(rd, doc, 0);
   text = doc.getText(0, doc.getLength());
   text = text.replaceAll("\n", " ");
  } catch (ServletException e) {
   LOGGER.error("Servlet Exception:", e.getMessage() + "\n Exception Trace " + e);
  } catch (IOException e) {
   LOGGER.error("IO Exception", e.getMessage() + "\n Exception Trace " + e);
  }
  return text;
 }
 
}
