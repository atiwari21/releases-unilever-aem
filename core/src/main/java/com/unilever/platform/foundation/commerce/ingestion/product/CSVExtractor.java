/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion.product;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.pim.common.Csv;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;
import com.unilever.platform.foundation.commerce.dto.ErrorReport;
import com.unilever.platform.foundation.commerce.dto.Product;
import com.unilever.platform.foundation.commerce.dto.ProductParser;
import com.unilever.platform.foundation.commerce.dto.ResultReport;
import com.unilever.platform.foundation.commerce.ingestion.IExtractor;

/**
 * The Class CSVExtractor.
 *
 */
@Component(immediate = true, metatype = true, enabled = true)
@Service(value = CSVExtractor.class)
public class CSVExtractor implements IExtractor {
 
 private static final int TWO = 2;
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(CSVExtractor.class);
 
 /** The property map taken from osgi configuration. */
 protected Map<String, String> props = new LinkedHashMap<String, String>();
 
 /**
  * The column name index map contains information related to header with column numbers and object type.
  */
 protected Map<Integer, ProductParser> colNameIndexMap = new LinkedHashMap<Integer, ProductParser>();
 
 /** The input iterator used over the csv. */
 protected Iterator<String[]> inputIterator;
 
 /** The csv object for csv file. */
 protected Csv csv;
 
 /** The head row, first row in csv. */
 protected String[] headRow;
 
 protected List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
 
 /** The column mappings. */
 private String[] csvColumnMappings;
 
 /** The Constant PROPERTY_MAPPINGS. */
 @Property(unbounded = PropertyUnbounded.ARRAY, label = "CSV column Mappings", description = "List of csv column mappings.")
 private static final String PROPERTY_MAPPINGS = "column.mappings";
 
 /**
  * Activator for the service. Reads default mappings and store them in map.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  csvColumnMappings = PropertiesUtil.toStringArray(ctx.getProperties().get(PROPERTY_MAPPINGS),
    PIMInjestionUtility.getExtractorDefaultColumnMapping());
  
  if (csvColumnMappings != null) {
   for (String s : csvColumnMappings) {
    String[] prop = s.split("=");
    props.put(prop[0], prop[1]);
   }
  }
 }
 
 /**
  * Returns input stream and convert it in JSON array.
  *
  * @param stream
  *         the stream
  * @param request
  *         the request
  * @return the JSON array
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 @Override
 public JSONArray extractData(InputStream stream, SlingHttpServletRequest request) throws IOException {
  
  String separator = null;
  String delimiter = null;
  
  if (request != null) {
   separator = request.getParameter("separator");
   delimiter = request.getParameter("delimiter");
  }
  
  csv = new Csv();
  if (separator != null) {
   csv.setFieldSeparatorRead(separator.charAt(0));
  }
  if (delimiter != null) {
   csv.setFieldDelimiter(delimiter.charAt(0));
  }
  // reading the csv from input stream
  inputIterator = csv.read(stream, "UTF-8");
  
  if (!inputIterator.hasNext()) {
   return null;
  }
  
  // setting column map index for parsing the csv
  setColumnMap();
  
  if (!inputIterator.hasNext()) {
   return null;
  } else {
   // preparing the json array
   JSONArray jsonarr = new JSONArray();
   int row = 0;
   while (inputIterator.hasNext()) {
    // getting json object for each row
    JSONObject jsonObj = new JSONObject(getProduct(inputIterator.next(), row));
    jsonarr.put(jsonObj);
    row++;
   }
   return jsonarr;
  }
 }
 
 @Override
 public ResultReport getErrorReport() {
  ResultReport report = new ResultReport();
  report.setErrorReport(errorReport);
  return report;
 }
 
 /**
  * Sets the column map after parsing the first row of csv.
  */
 private void setColumnMap() {
  
  // first row is header
  if (inputIterator.hasNext()) {
   headRow = inputIterator.next();
   int colIndex = 0;
   for (String colName : headRow) {
    
    ProductParser parserObj = new ProductParser();
    parserObj.setColumnName(colName);
    if (colName.contains("#")) {
     
     String[] arr = colName.split("#");
     int index = Integer.parseInt(arr[0]);
     parserObj.setIndex(index);
     String[] prop = arr[1].split("\\.");
     parserObj.setPropertyName(prop[1]);
     parserObj.setKeyName(getPropertyKeyForColumn(prop[0]));
     
    } else if (colName.contains(".")) {
     
     String[] prop = colName.split("\\.");
     parserObj.setIndex(1);
     parserObj.setPropertyName(prop[1]);
     parserObj.setKeyName(getPropertyKeyForColumn(prop[0]));
     
    } else {
     parserObj.setIndex(0);
     parserObj.setPropertyName(null);
     parserObj.setKeyName(getPropertyKeyForColumn(colName));
    }
    colNameIndexMap.put(colIndex, parserObj);
    
    colIndex++;
   }
  }
 }
 
 /**
  * Gets the product object from each row of the csv.
  *
  * @param cols
  *         the cols
  * @return the unilever product
  */
 private Product getProduct(String[] cols, int row) {
  
  Product product = new Product();
  product.setType("product");
  int index = 0;
  for (String colValue : cols) {
   product = getProductByReflection(index, colValue, product, row);
   index++;
  }
  
  if ((product != null) && (StringUtils.isNotEmpty(product.getSkuParent()))) {
   String parentSku = String.valueOf(product.getSkuParent());
   if (!(StringUtils.isEmpty(parentSku))) {
    product.setType("variation");
   }
  }
  
  return product;
 }
 
 /**
  * Gets the product by reflection.
  *
  * @param index
  *         the index
  * @param colValue
  *         the col value
  * @param prod
  *         the product
  * @return the product by reflection
  */
 private Product getProductByReflection(int index, String colValue, Product prod, int row) {
  
  ErrorReport errObj = null;
  Product product = prod;
  try {
   ProductParser p = colNameIndexMap.get(index);
   if (p != null) {
    String colName = p.getColumnName();
    int in = p.getIndex();
    String keyName = p.getKeyName();
    String propName = p.getPropertyName();
    
    if (keyName != null) {
     
     // when mapping is present
     if (in == 0) {
      
      // Setting product for single-valued property
      product = (Product) setKeyName(product, keyName, colValue);
     } else {
      
      product = (Product) setCustomKeyObj(product, keyName, propName, colName, colValue, in);
     }
    } else {
     
     // Setting product when no mapping is present for property
     product = (Product) PIMInjestionUtility.setAdditionalKeyName(product, colName, colValue);
    }
    
   }
  } catch (NumberFormatException e) {
   errObj = new ErrorReport();
   errObj.setErrorMessage("Error while fetching column" + e.getMessage());
   errObj.setIndex(row + TWO);
   LOG.error("Error while creating product json : ", e);
  } catch (Exception e) {
   errObj = new ErrorReport();
   errObj.setErrorMessage("Error while fetching product data.");
   errObj.setIndex(row + TWO);
   LOG.error("Error while calling method : ", e);
  }
  
  if (errObj != null) {
   errorReport.add(errObj);
  }
  return product;
 }
 
 /**
  * Invokes the set property for the Product object.
  *
  * @param obj
  *         the obj @param keyName the key name @param colValue the col value @return the object @throws NoSuchMethodException the no such method
  *         exception @throws IllegalAccessException the illegal access exception @throws InvocationTargetException the invocation target
  *         exception @throws
  */
 private Object setKeyName(Object obj, String keyName, String colValue)
   throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
  
  String methodName = "set" + keyName;
  Method method = null;
  
  try {
   if (colValue != null) {
    if (PIMInjestionUtility.methodExists(obj, methodName, String.class)) {
     method = obj.getClass().getMethod(methodName, String.class);
     method.invoke(obj, colValue);
    } else if (PIMInjestionUtility.methodExists(obj, methodName, long.class)) {
     method = obj.getClass().getMethod(methodName, long.class);
     method.invoke(obj, Long.parseLong(colValue));
    } else if (PIMInjestionUtility.methodExists(obj, methodName, float.class)) {
     method = obj.getClass().getMethod(methodName, float.class);
     method.invoke(obj, Float.parseFloat(colValue));
    } else if (PIMInjestionUtility.methodExists(obj, methodName, int.class)) {
     method = obj.getClass().getMethod(methodName, int.class);
     method.invoke(obj, Integer.parseInt(colValue));
    } else {
     PIMInjestionUtility.setAdditionalKeyName(obj, keyName, colValue);
    }
   }
  } catch (NoSuchMethodException e) {
   LOG.error("Error while getting no method ", e);
  }
  
  return obj;
 }
 
 /**
  * Sets the custom key obj.
  *
  * @param obj
  *         the obj
  * @param keyName
  *         the key name
  * @param propName
  *         the prop name
  * @param colName
  *         the col name
  * @param colValue
  *         the col value
  * @param pos
  *         the pos
  * @return the object
  * @throws ClassNotFoundException
  *          the class not found exception
  * @throws NoSuchMethodException
  *          the no such method exception
  * @throws InvocationTargetException
  *          the invocation target exception
  * @throws IllegalAccessException
  *          the illegal access exception
  * @throws InstantiationException
  *          the instantiation exception
  */
 @SuppressWarnings({ "unchecked", "rawtypes" })
 private Object setCustomKeyObj(Object obj, String keyName, String propName, String colName, String colValue, int pos)
   throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
  
  if (colValue != null) {
   
   String className = UNICommerceConstants.DTO_NAMESPACE + keyName;
   Class cls = Class.forName(className);
   Object classObj = cls.newInstance();
   if (propName == null) {
    // set additional property
    classObj = PIMInjestionUtility.setAdditionalKeyName(classObj, colName, colValue);
   } else {
    // set property
    classObj = setKeyName(classObj, propName, colValue);
   }
   
   // checking whether object is of list type
   
   String methodName = "get" + keyName;
   Method method = obj.getClass().getMethod(methodName);
   Object getterObj = method.invoke(obj);
   
   String mName = "set" + keyName;
   if (getterObj instanceof List<?>) {
    
    List<Object> listObj;
    listObj = (List<Object>) getterObj;
    if ((getterObj != null) && (((List) getterObj).size() >= pos)) {
     
     Object customObj = ((List) getterObj).get(pos - 1);
     if (customObj != null) {
      customObj = setKeyName(customObj, propName, colValue);
     }
     listObj.set(pos - 1, customObj);
     
    } else {
     listObj.add(classObj);
    }
    
    Method listmethod = obj.getClass().getMethod(mName, List.class);
    listmethod.invoke(obj, listObj);
    
   } else {
    
    Method mtd = null;
    if (getterObj != null) {
     mtd = obj.getClass().getMethod(mName, classObj.getClass());
     getterObj = setKeyName(getterObj, propName, colValue);
     mtd.invoke(obj, getterObj);
    } else {
     mtd = obj.getClass().getMethod(mName, classObj.getClass());
     mtd.invoke(obj, classObj);
    }
   }
  }
  return obj;
 }
 
 /**
  * Gets the property key for column.
  *
  * @param columnName
  *         the column name
  * @return the property key for column
  */
 private String getPropertyKeyForColumn(String columnName) {
  
  String key = null;
  
  Set<String> keys = props.keySet();
  if (props.containsValue(columnName)) {
   for (String k : keys) {
    String value = props.get(k);
    if (value.equals(columnName)) {
     key = k;
    }
   }
  }
  return key;
 }
 
 /**
  * Sets the props.
  *
  * @param props
  *         the props to set
  */
 public void setProps(Map<String, String> props) {
  this.props = props;
 }
 
 @Override
 public void setErrorReport() {
  errorReport = new ArrayList<ErrorReport>();
  
 }
}
