/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.dto;

/**
 * The Class ErrorReport.
 *
 * @author atiw22
 */
public class ErrorReport {
 
 /** The index. */
 private int index;
 
 /** The error message. */
 private String errorMessage;
 
 /**
  * Gets the index.
  *
  * @return the index
  */
 public int getIndex() {
  return index;
 }
 
 /**
  * Sets the index.
  *
  * @param index
  *         the new index
  */
 public void setIndex(int index) {
  this.index = index;
 }
 
 /**
  * Gets the error message.
  *
  * @return the error message
  */
 public String getErrorMessage() {
  return errorMessage;
 }
 
 /**
  * Sets the error message.
  *
  * @param errorMessage
  *         the new error message
  */
 public void setErrorMessage(String errorMessage) {
  this.errorMessage = errorMessage;
 }
 
}
