/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class UNIProduct.
 */
public class Product {
 
 /** The action. */
 private String action;
 
 /** The type. */
 private String type;
 
 /** The product id. */
 private String productID;
 
 /** The label insight id. */
 private String labelInsightId;
 
 private String rewardVariant;
 
 /** The ils product name. */
 private String ilsProductName;
 
 /** The Unique id. */
 private String uniqueID;
 
 /** The sku. */
 private String sku;
 
 /** The short identifier value. */
 private String shortIdentifierValue;
 
 /** The name. */
 private String name;
 
 /** The child products. */
 private String childProducts;
 
 /** The size. */
 private String size;
 
 /** The price. */
 private float price;
 
 /** The sku parent. */
 private String skuParent;
 
 /** The production date. */
 private String productionDate;
 
 private String rewardAvailableFrom;
 
 private String rewardAvailableTo;
 
 private String aboutThisRewardDescription;
 
 private String quantity;
 
 /** The image ids. */
 private String imageIds;
 
 /** The alt text images. */
 private String altTextImages;
 
 /** The cq tags. */
 private String cqTags;
 
 /** The category. */
 private String category;
 
 /** The title. */
 private String title;
 
 /** The description. */
 private String description;
 
 /** The smart product id. */
 private String smartProductId;
 
 /** The product size. */
 private String productSize;
 
 /** The ingredients. */
 private String ingredients;
 
 /** The customize total price. */
 private float customizeProductPrice;
 
 /** The custom description. */
 private String customizeDescription;
 
 /** The customize gift wrap description. */
 private String customizeGiftWrapDescription;
 
 /** The wrapping price. */
 private float wrappingPrice;
 
 /** The custom image ids. */
 private String customizeImageIds;
 
 /** The custom alt text images. */
 private String customizeImageAltText;
 
 /** The allergy. */
 private String allergy;
 
 /** The aem page name. */
 private String aemPageName;
 
 /** The lastmodifiedtime. */
 private String lastmodifiedtime;
 
 /** The image format. */
 private String imageFormat;
 
 /** The recipients info. */
 private List<RecipientsInfo> recipientsInfo = new ArrayList<RecipientsInfo>();
 
 /** The wrapper info. */
 private List<WrapperInfo> wrapperInfo = new ArrayList<WrapperInfo>();
 
 /** The nutritional facts. */
 private List<NutritionalFacts> nutritionalFacts = new ArrayList<NutritionalFacts>();
 
 /** The additional properties. */
 private Map<String, Object> additionalProperties = new HashMap<String, Object>();
 
 /**
  * Gets the sku parent.
  *
  * @return the sku parent
  */
 public String getSkuParent() {
  return skuParent;
 }
 
 /**
  * Sets the sku parent.
  *
  * @param skuParent
  *         the new sku parent
  */
 public void setSkuParent(String skuParent) {
  this.skuParent = skuParent;
 }
 
 /**
  * Gets the cq tags.
  *
  * @return the cq tags
  */
 public String getCqTags() {
  return cqTags;
 }
 
 /**
  * Sets the cq tags.
  *
  * @param cqTags
  *         the new cq tags
  */
 public void setCqTags(String cqTags) {
  this.cqTags = cqTags;
 }
 
 /**
  * Gets the image ids.
  *
  * @return the image ids
  */
 public String getImageIds() {
  return imageIds;
 }
 
 /**
  * Sets the image ids.
  *
  * @param imageIds
  *         the new image ids
  */
 public void setImageIds(String imageIds) {
  this.imageIds = imageIds;
 }
 
 /**
  * Gets the type.
  *
  * @return the type
  */
 public String getType() {
  return type;
 }
 
 /**
  * Sets the type.
  *
  * @param type
  *         the new type
  */
 public void setType(String type) {
  this.type = type;
 }
 
 /**
  * Gets the product id.
  *
  * @return the product id
  */
 public String getProductID() {
  return productID;
 }
 
 /**
  * Sets the product id.
  *
  * @param productID
  *         The productID
  */
 public void setProductID(String productID) {
  this.productID = productID;
 }
 
 /**
  * Gets the sku.
  *
  * @return The sku
  */
 public String getSku() {
  return sku;
 }
 
 /**
  * Sets the sku.
  *
  * @param sku
  *         The sku
  */
 public void setSku(String sku) {
  this.sku = sku;
 }
 
 /**
  * Gets the name.
  *
  * @return The name
  */
 public String getName() {
  return name;
 }
 
 /**
  * Sets the name.
  *
  * @param name
  *         The name
  */
 public void setName(String name) {
  this.name = name;
 }
 
 /**
  * Gets the category.
  *
  * @return The category
  */
 public String getCategory() {
  return category;
 }
 
 /**
  * Sets the category.
  *
  * @param category
  *         The category
  */
 public void setCategory(String category) {
  this.category = category;
 }
 
 /**
  * Gets the title.
  *
  * @return The title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  *
  * @param title
  *         The title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the description.
  *
  * @return The description
  */
 public String getDescription() {
  return description;
 }
 
 /**
  * Sets the description.
  *
  * @param description
  *         The description
  */
 public void setDescription(String description) {
  this.description = description;
 }
 
 /**
  * Gets the production date.
  *
  * @return The productionDate
  */
 public String getProductionDate() {
  return productionDate;
 }
 
 /**
  * Sets the production date.
  *
  * @param productionDate
  *         The productionDate
  */
 public void setProductionDate(String productionDate) {
  this.productionDate = productionDate;
 }
 
 /**
  * Gets the additional properties.
  *
  * @return the additional properties
  */
 public Map<String, Object> getAdditionalProperties() {
  return this.additionalProperties;
 }
 
 /**
  * Sets the additional property.
  *
  * @param name
  *         the name
  * @param value
  *         the value
  */
 public void setAdditionalProperty(String name, Object value) {
  this.additionalProperties.put(name, value);
 }
 
 /**
  * Gets the action.
  *
  * @return the action
  */
 public String getAction() {
  return action;
 }
 
 /**
  * Sets the action.
  *
  * @param action
  *         the action to set
  */
 public void setAction(String action) {
  this.action = action;
 }
 
 /**
  * Gets the size.
  *
  * @return the size
  */
 public String getSize() {
  return size;
 }
 
 /**
  * Sets the size.
  *
  * @param size
  *         the new size
  */
 public void setSize(String size) {
  this.size = size;
 }
 
 /**
  * Gets the price.
  *
  * @return the price
  */
 public float getPrice() {
  return price;
 }
 
 /**
  * Sets the price.
  *
  * @param price
  *         the new price
  */
 public void setPrice(float price) {
  this.price = price;
 }
 
 /**
  * Gets the product size.
  *
  * @return the product size
  */
 public String getProductSize() {
  return productSize;
 }
 
 /**
  * Sets the product size.
  *
  * @param productSize
  *         the new product size
  */
 public void setProductSize(String productSize) {
  this.productSize = productSize;
 }
 
 /**
  * Gets the smart product id.
  *
  * @return the smart product id
  */
 public String getSmartProductId() {
  return smartProductId;
 }
 
 /**
  * Sets the smart product id.
  *
  * @param smartProductId
  *         the new smart product id
  */
 public void setSmartProductId(String smartProductId) {
  this.smartProductId = smartProductId;
 }
 
 /**
  * Gets the alt text images.
  *
  * @return the alt text images
  */
 public String getAltTextImages() {
  return altTextImages;
 }
 
 /**
  * Sets the alt text images.
  *
  * @param altTextImages
  *         the new alt text images
  */
 public void setAltTextImages(String altTextImages) {
  this.altTextImages = altTextImages;
 }
 
 /**
  * Gets the ingredients.
  *
  * @return the ingredients
  */
 public String getIngredients() {
  return ingredients;
 }
 
 /**
  * Sets the ingredients.
  *
  * @param ingredients
  *         the new ingredients
  */
 public void setIngredients(String ingredients) {
  this.ingredients = ingredients;
 }
 
 /**
  * Gets the recipients info.
  *
  * @return the recipients info
  */
 public List<RecipientsInfo> getRecipientsInfo() {
  return recipientsInfo;
 }
 
 /**
  * Sets the recipients info.
  *
  * @param recipientsInfo
  *         the new recipients info
  */
 public void setRecipientsInfo(List<RecipientsInfo> recipientsInfo) {
  this.recipientsInfo = recipientsInfo;
 }
 
 /**
  * Gets the wrapper info.
  *
  * @return the wrapper info
  */
 public List<WrapperInfo> getWrapperInfo() {
  return wrapperInfo;
 }
 
 /**
  * Sets the wrapper info.
  *
  * @param wrapperInfo
  *         the new wrapper info
  */
 public void setWrapperInfo(List<WrapperInfo> wrapperInfo) {
  this.wrapperInfo = wrapperInfo;
 }
 
 /**
  * Gets the customize total price.
  *
  * @return the customize total price
  */
 public float getCustomizeProductPrice() {
  return customizeProductPrice;
 }
 
 /**
  * Sets the customize total price.
  *
  * @param customizeTotalPrice
  *         the new customize total price
  */
 public void setCustomizeProductPrice(float customizeTotalPrice) {
  this.customizeProductPrice = customizeTotalPrice;
 }
 
 /**
  * Gets the custom description.
  *
  * @return the custom description
  */
 public String getCustomizeDescription() {
  return customizeDescription;
 }
 
 /**
  * Sets the custom description.
  *
  * @param customDescription
  *         the new custom description
  */
 public void setCustomizeDescription(String customDescription) {
  this.customizeDescription = customDescription;
 }
 
 /**
  * Gets the wrapping price.
  *
  * @return the wrapping price
  */
 public float getWrappingPrice() {
  return wrappingPrice;
 }
 
 /**
  * Sets the wrapping price.
  *
  * @param wrappingPrice
  *         the new wrapping price
  */
 public void setWrappingPrice(float wrappingPrice) {
  this.wrappingPrice = wrappingPrice;
 }
 
 /**
  * Gets the custom image ids.
  *
  * @return the custom image ids
  */
 public String getCustomizeImageIds() {
  return customizeImageIds;
 }
 
 /**
  * Sets the custom image ids.
  *
  * @param customImageIds
  *         the new custom image ids
  */
 public void setCustomizeImageIds(String customImageIds) {
  this.customizeImageIds = customImageIds;
 }
 
 /**
  * Gets the custom alt text images.
  *
  * @return the custom alt text images
  */
 public String getCustomizeImageAltText() {
  return customizeImageAltText;
 }
 
 /**
  * Sets the custom alt text images.
  *
  * @param customAltTextImages
  *         the new custom alt text images
  */
 public void setCustomizeImageAltText(String customAltTextImages) {
  this.customizeImageAltText = customAltTextImages;
 }
 
 /**
  * Gets the nutritional facts.
  *
  * @return the nutritional facts
  */
 public List<NutritionalFacts> getNutritionalFacts() {
  return nutritionalFacts;
 }
 
 /**
  * Sets the nutritional facts.
  *
  * @param nutritionalFacts
  *         the new nutritional facts
  */
 public void setNutritionalFacts(List<NutritionalFacts> nutritionalFacts) {
  this.nutritionalFacts = nutritionalFacts;
 }
 
 /**
  * Gets the child products.
  *
  * @return the child products
  */
 public String getChildProducts() {
  return childProducts;
 }
 
 /**
  * Sets the child products.
  *
  * @param childProducts
  *         the new child products
  */
 public void setChildProducts(String childProducts) {
  this.childProducts = childProducts;
 }
 
 /**
  * Gets the customize gift wrap description.
  *
  * @return the customize gift wrap description
  */
 public String getCustomizeGiftWrapDescription() {
  return customizeGiftWrapDescription;
 }
 
 /**
  * Sets the customize gift wrap description.
  *
  * @param customizeGiftWrapDescription
  *         the new customize gift wrap description
  */
 public void setCustomizeGiftWrapDescription(String customizeGiftWrapDescription) {
  this.customizeGiftWrapDescription = customizeGiftWrapDescription;
 }
 
 /**
  * Gets the allergy.
  *
  * @return the allergy
  */
 public String getAllergy() {
  return allergy;
 }
 
 /**
  * Sets the allergy.
  *
  * @param allergy
  *         the new allergy
  */
 public void setAllergy(String allergy) {
  this.allergy = allergy;
 }
 
 /**
  * Gets the unique id.
  *
  * @return the unique id
  */
 public String getUniqueID() {
  return uniqueID;
 }
 
 /**
  * Sets the unique id.
  *
  * @param uniqueID
  *         the new unique id
  */
 public void setUniqueID(String uniqueID) {
  this.uniqueID = uniqueID;
 }
 
 /**
  * Gets the short identifier value.
  *
  * @return the short identifier value
  */
 public String getShortIdentifierValue() {
  return shortIdentifierValue;
 }
 
 /**
  * Sets the short identifier value.
  *
  * @param shortIdentifierValue
  *         the new short identifier value
  */
 public void setShortIdentifierValue(String shortIdentifierValue) {
  this.shortIdentifierValue = shortIdentifierValue;
 }
 
 /**
  * Gets the label insight id.
  *
  * @return the label insight id
  */
 public String getLabelInsightId() {
  return labelInsightId;
 }
 
 /**
  * Sets the label insight id.
  *
  * @param labelInsightId
  *         the new label insight id
  */
 public void setLabelInsightId(String labelInsightId) {
  this.labelInsightId = labelInsightId;
 }
 
 /**
  * Gets the ils product name.
  *
  * @return the ils product name
  */
 public String getIlsProductName() {
  return ilsProductName;
 }
 
 /**
  * Sets the ils product name.
  *
  * @param ilsProductName
  *         the new ils product name
  */
 public void setIlsProductName(String ilsProductName) {
  this.ilsProductName = ilsProductName;
 }
 
 /**
  * Gets the aem page name.
  *
  * @return the aem page name
  */
 public String getAemPageName() {
  return aemPageName;
 }
 
 /**
  * Sets the aem page name.
  *
  * @param aemPageName
  *         the new aem page name
  */
 public void setAemPageName(String aemPageName) {
  this.aemPageName = aemPageName;
 }
 
 /**
  * Gets the lastmodifiedtime.
  *
  * @return the lastmodifiedtime
  */
 public String getLastmodifiedtime() {
  return lastmodifiedtime;
 }
 
 /**
  * Sets the lastmodifiedtime.
  *
  * @param lastmodifiedtime
  *         the new lastmodifiedtime
  */
 public void setLastmodifiedtime(String lastmodifiedtime) {
  this.lastmodifiedtime = lastmodifiedtime;
 }
 
 /**
  * Gets the image format.
  *
  * @return the image format
  */
 public String getImageFormat() {
  return imageFormat;
 }
 
 /**
  * Sets the image format.
  *
  * @param imageFormat
  *         the new image format
  */
 public void setImageFormat(String imageFormat) {
  this.imageFormat = imageFormat;
 }
 
 public String getQuantity() {
  return quantity;
 }
 
 public void setQuantity(String quantity) {
  this.quantity = quantity;
 }
 
 public String getRewardVariant() {
  return rewardVariant;
 }
 
 public void setRewardVariant(String rewardVariant) {
  this.rewardVariant = rewardVariant;
 }
 
 public String getRewardAvailableFrom() {
  return rewardAvailableFrom;
 }
 
 public void setRewardAvailableFrom(String rewardAvailableFrom) {
  this.rewardAvailableFrom = rewardAvailableFrom;
 }
 
 public String getRewardAvailableTo() {
  return rewardAvailableTo;
 }
 
 public void setRewardAvailableTo(String rewardAvailableTo) {
  this.rewardAvailableTo = rewardAvailableTo;
 }
 
 public String getAboutThisRewardDescription() {
  return aboutThisRewardDescription;
 }
 
 public void setAboutThisRewardDescription(String aboutThisRewardDescription) {
  this.aboutThisRewardDescription = aboutThisRewardDescription;
 }
}
