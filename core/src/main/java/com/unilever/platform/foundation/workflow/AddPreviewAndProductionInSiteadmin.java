/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.workflow;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.ListInfoProvider;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;

/**
 * The Class AddPreviewAndProductionInSiteadmin.
 */
@Component(metatype = false)
@Service(value = ListInfoProvider.class)
public class AddPreviewAndProductionInSiteadmin implements ListInfoProvider {
 
 /** The Constant STRING_CLOSE_BRACKET. */
 private static final String STRING_CLOSE_BRACKET = ")";
 
 /** The Constant STRING_OPEN_BRACKET. */
 private static final String STRING_OPEN_BRACKET = "(";
 
 /** The Constant BY. */
 private static final String BY = "By";
 
 /** The Constant LAST_REPLICATED_TO. */
 private static final String LAST_REPLICATED_TO = "lastReplicatedTo";
 
 /** The Constant PRODUCTION. */
 private static final String PRODUCTION = "Production";
 
 /** The Constant PREVIEW. */
 private static final String PREVIEW = "Preview";
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AddPreviewAndProductionInSiteadmin.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.day.cq.commons.ListInfoProvider#updateListGlobalInfo(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.commons.json.JSONObject, org.apache.sling.api.resource.Resource)
  */
 /**
  * @param request
  * @param info
  * @param resource
  * @throws JSONException
  */
 public void updateListGlobalInfo(SlingHttpServletRequest request, JSONObject info, Resource resource) throws JSONException {
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.day.cq.commons.ListInfoProvider#updateListItemInfo(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.commons.json.JSONObject, org.apache.sling.api.resource.Resource)
  */
 /**
  * @param request
  * @param info
  * @param resource
  * @throws JSONException
  */
 public void updateListItemInfo(SlingHttpServletRequest request, JSONObject info, Resource resource) throws JSONException {
  Page page = resource.adaptTo(Page.class);
  if (page != null) {
   Map<String, Object> pageProperties = page.getProperties();
   setLastReplicated(info, pageProperties, PREVIEW);
   setLastReplicated(info, pageProperties, PRODUCTION);
   Template template = page.getTemplate();
   String templateTitle = (template != null) ? template.getTitle() : StringUtils.EMPTY;
   info.put("template", templateTitle);
  }
 }
 
 /**
  * Sets the last replicated.
  *
  * @param info
  *         the info
  * @param pageProperties
  *         the page properties
  * @param key
  *         the key
  * @param enabled
  *         the enabled
  * @throws JSONException
  *          the JSON exception
  */
 private void setLastReplicated(JSONObject info, Map<String, Object> pageProperties, String key) throws JSONException {
  String date = getFormattedDateString(pageProperties, LAST_REPLICATED_TO + key);
  String lastReplicatedBy = MapUtils.getString(pageProperties, LAST_REPLICATED_TO + key + BY, StringUtils.EMPTY);
  String dateWithUserId = (StringUtils.isNotBlank(date)) ? date + STRING_OPEN_BRACKET + lastReplicatedBy + STRING_CLOSE_BRACKET : StringUtils.EMPTY;
  info.put(LAST_REPLICATED_TO + key, dateWithUserId);
 }
 
 /**
  * Gets the formatted date string.
  *
  * @param properties
  *         the properties
  * @param key
  *         the key
  * @return the formatted date string
  */
 public static String getFormattedDateString(Map<String, Object> properties, String key) {
  String value = StringUtils.EMPTY;
  String tempFormat = "dd-MMM-YYYY HH:mm";
  GregorianCalendar date = null;
  try {
   if (MapUtils.getObject(properties, key) instanceof GregorianCalendar) {
    date = ((GregorianCalendar) MapUtils.getObject(properties, key) != null) ? (GregorianCalendar) MapUtils.getObject(properties, key) : null;
   } else if (StringUtils.isNotBlank(MapUtils.getString(properties, key))) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(tempFormat);
    String publishDaate = MapUtils.getString(properties, key, StringUtils.EMPTY);
    Date dateObject = dateFormat.parse(publishDaate);
    Calendar calendar = GregorianCalendar.getInstance();
    calendar.setTime(dateObject);
    date = (calendar != null) ? (GregorianCalendar) calendar : null;
   }
   DateFormat dateFormat = new SimpleDateFormat(tempFormat);
   value = (date != null) ? dateFormat.format(date.getTime()) : StringUtils.EMPTY;
  } catch (ParseException e) {
   LOGGER.warn("parse exception found", e);
  }
  return value;
 }
}
