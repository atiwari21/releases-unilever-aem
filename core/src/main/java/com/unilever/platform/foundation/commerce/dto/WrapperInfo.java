/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class WrapperInfo.
 */
public class WrapperInfo {
 
 /** The text. */
 private String text;
 
 /** The id. */
 private String id;
 
 /** The wrapper image. */
 private String wrapperImage;
 
 /** The image alt text. */
 private String imageAltText;
 
 /** The additional properties. */
 private Map<String, Object> additionalProperties = new HashMap<String, Object>();
 
 /**
  * Gets the text.
  *
  * @return the text
  */
 public String getText() {
  return text;
 }
 
 /**
  * Gets the id.
  *
  * @return the id
  */
 public String getId() {
  return id;
 }
 
 /**
  * Sets the text.
  *
  * @param text
  *         the new text
  */
 public void setText(String text) {
  this.text = text;
 }
 
 /**
  * Sets the id.
  *
  * @param id
  *         the new id
  */
 public void setId(String id) {
  this.id = id;
 }
 
 /**
  * Gets the wrapper image.
  *
  * @return the wrapper image
  */
 public String getWrapperImage() {
  return wrapperImage;
 }
 
 /**
  * Sets the wrapper image.
  *
  * @param wrapperImage
  *         the new wrapper image
  */
 public void setWrapperImage(String wrapperImage) {
  this.wrapperImage = wrapperImage;
 }
 
 /**
  * Gets the image alt text.
  *
  * @return the image alt text
  */
 public String getImageAltText() {
  return imageAltText;
 }
 
 /**
  * Sets the image alt text.
  *
  * @param imageAltText
  *         the new image alt text
  */
 public void setImageAltText(String imageAltText) {
  this.imageAltText = imageAltText;
 }
 
 public Map<String, Object> getAdditionalProperties() {
  return additionalProperties;
 }
 
 public void setAdditionalProperties(Map<String, Object> additionalProperties) {
  this.additionalProperties = additionalProperties;
 }
 
}
