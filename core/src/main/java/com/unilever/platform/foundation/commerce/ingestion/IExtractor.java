/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion;

import java.io.IOException;
import java.io.InputStream;

import org.apache.sling.api.SlingHttpServletRequest;
import org.json.JSONArray;

import com.unilever.platform.foundation.commerce.dto.ResultReport;

/**
 * The Interface IExtractor.
 *
 * Interface for extractor class
 */
public interface IExtractor {
 
 /**
  * Returns JSONArray from input stream.
  *
  * @param stream
  *         the stream
  * @param request
  *         the request
  * @return the JSON array
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 JSONArray extractData(InputStream stream, SlingHttpServletRequest request) throws IOException;
 
 /**
  * Gets the error report.
  *
  * @return the error report
  */
 ResultReport getErrorReport();
 
 /**
  * Sets error report
  */
 void setErrorReport();
}
