/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion;

import org.apache.sling.api.SlingHttpServletRequest;
import org.json.JSONArray;

import com.unilever.platform.foundation.commerce.dto.ResultReport;

/**
 * The Interface ITransformer.
 *
 * Interface for Transformer class
 */
public interface ITransformer {
 
 /**
  * Validates the JSON Array against JSON Schema.
  *
  * @param jsonarray
  *         the jsonarray
  * @param extension
  *         the extension
  * @param request
  *         the request
  * @return List<ErrorReport>
  */
 ResultReport validateProducts(JSONArray jsonarray, String extension, SlingHttpServletRequest request, String storePath);
}
