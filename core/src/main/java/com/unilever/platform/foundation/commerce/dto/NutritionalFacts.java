/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class NutritionalFacts.
 */
public class NutritionalFacts {
 
 /** The additional properties. */
 private Map<String, Object> additionalProperties = new HashMap<String, Object>();
 
 /**
  * Gets the additional properties.
  *
  * @return the additional properties
  */
 public Map<String, Object> getAdditionalProperties() {
  return additionalProperties;
 }
 
 /**
  * Sets the additional properties.
  *
  * @param additionalProperties
  *         the additional properties
  */
 public void setAdditionalProperties(Map<String, Object> additionalProperties) {
  this.additionalProperties = additionalProperties;
 }
 
 /**
  * Sets the additional property.
  *
  * @param name
  *         the name
  * @param value
  *         the value
  */
 public void setAdditionalProperty(String name, Object value) {
  this.additionalProperties.put(name, value);
 }
 
}
