/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.workflow;

import java.util.GregorianCalendar;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.AgentIdFilter;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.unilever.platform.aem.foundation.configuration.ConfigurationHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.utils.PreviewProductionAgentsUtility;

/**
 * The Class ReplicateToProduction.
 */
@Component
@Service
@Property(name = "process.label", value = { "Customized publish production" })
public class ReplicateToProduction implements WorkflowProcess {
 
 private static final String JCR_CONTENT = "/jcr:content";
 
 /** The Constant TYPE_JCR_PATH. */
 public static final String TYPE_JCR_PATH = "JCR_PATH";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ReplicateToProduction.class);
 
 private static final String LAST_REPLICATED_TO_PRODUCTION = "lastReplicatedToProduction";
 
 private static final String LAST_REPLICATED_TO_PRODUCTION_BY = "lastReplicatedToProductionBy";
 
 private static final String LAST_REPLICATED_TO_PREVIEW = "lastReplicatedToPreview";
 
 private static final String LAST_REPLICATED_TO_PREVIEW_BY = "lastReplicatedToPreviewBy";
 /** The resource resolver factory. */
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 @Reference
 GlobalConfiguration globalConfiguration;
 @Reference
 Replicator replicator;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.day.cq.workflow.exec.WorkflowProcess#execute(com.day.cq.workflow.exec.WorkItem, com.day.cq.workflow.WorkflowSession,
  * com.day.cq.workflow.metadata.MetaDataMap)
  */
 @Override
 public void execute(WorkItem paramWorkItem, WorkflowSession paramWorkflowSession, MetaDataMap paramMetaDataMap) throws WorkflowException {
  com.day.cq.workflow.exec.WorkflowData workflowData = paramWorkItem.getWorkflowData();
  if (workflowData.getPayloadType().equals(TYPE_JCR_PATH)) {
   ResourceResolver resolver = null;
   try {
    Session jcrSession = paramWorkflowSession.getSession();
    resolver = ConfigurationHelper.getResourceResolver(resourceResolverFactory, "readService");
    PageManager pageManager = (resolver != null) ? resolver.adaptTo(PageManager.class) : null;
    Page page = (pageManager != null) ? pageManager.getContainingPage(workflowData.getPayload().toString()) : null;
    Node node = (jcrSession != null) ? (Node) jcrSession.getItem(page.getPath() + JCR_CONTENT) : null;
    String[] agentIds = PreviewProductionAgentsUtility.getProductionAgents(globalConfiguration);
    if (agentIds.length > 0 && node != null) {
     ReplicationOptions replicationOptions = new ReplicationOptions();
     AgentIdFilter agentIdFilter = new AgentIdFilter(agentIds);
     replicationOptions.setFilter(agentIdFilter);
     replicator.replicate(jcrSession, ReplicationActionType.ACTIVATE, page.getPath(), replicationOptions);
     node.setProperty(LAST_REPLICATED_TO_PRODUCTION, GregorianCalendar.getInstance());
     node.setProperty(LAST_REPLICATED_TO_PRODUCTION_BY, paramWorkItem.getWorkflow().getInitiator());
     node.setProperty(LAST_REPLICATED_TO_PREVIEW, GregorianCalendar.getInstance());
     node.setProperty(LAST_REPLICATED_TO_PREVIEW_BY, paramWorkItem.getWorkflow().getInitiator());
     resolver.commit();
    }
   } catch (LoginException e) {
    LOGGER.error("Login Exception found", e);
   } catch (RepositoryException e) {
    LOGGER.error("Repository Exception found", e);
   } catch (Exception e) {
    LOGGER.error("Exception found", e);
   } finally {
    if (resolver != null) {
     resolver.close();
    }
   }
  }
 }
}
