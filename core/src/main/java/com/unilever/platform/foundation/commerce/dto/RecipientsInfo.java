/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class RecipientsInfo.
 */
public class RecipientsInfo {
 
 /** The type. */
 private String type;
 
 /** The character count. */
 private int characterCount;
 
 /** The additional properties. */
 private Map<String, Object> additionalProperties = new HashMap<String, Object>();
 
 /**
  * Gets the type.
  *
  * @return the type
  */
 public String getType() {
  return type;
 }
 
 /**
  * Gets the character count.
  *
  * @return the character count
  */
 public int getCharacterCount() {
  return characterCount;
 }
 
 /**
  * Sets the type.
  *
  * @param type
  *         the new type
  */
 public void setType(String type) {
  this.type = type;
 }
 
 /**
  * Sets the character count.
  *
  * @param characterCount
  *         the new character count
  */
 public void setCharacterCount(int characterCount) {
  this.characterCount = characterCount;
 }
 
 /**
  * Gets the additional properties.
  *
  * @return the additional properties
  */
 public Map<String, Object> getAdditionalProperties() {
  return additionalProperties;
 }
 
 /**
  * Sets the additional properties.
  *
  * @param additionalProperties
  *         the additional properties
  */
 public void setAdditionalProperties(Map<String, Object> additionalProperties) {
  this.additionalProperties = additionalProperties;
 }
 
}
