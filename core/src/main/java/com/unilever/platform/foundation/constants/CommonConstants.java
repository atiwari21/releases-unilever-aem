/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.constants;

/**
 * The Class CommonConstants.
 */
public final class CommonConstants {
 
 /**
  * Instantiates a new common constants.
  */
 
 /** The Constant SLASH_CHAR. */
 public static final String SLASH_CHAR = "/";
 
 /** The Constant UNDERSCORE_CHAR. */
 public static final String UNDERSCORE_CHAR = "_";
 
 /** The Constant PAGE_EXTENSION. */
 public static final String PAGE_EXTENSION = ".html";
 
 /** The Constant COLON_CHAR. */
 public static final String COLON_CHAR = ":";
 
 /** The Constant DOT_SPLITTER. */
 public static final String DOT_SPLITTER = "\\.";
 
 /** The Constant DOT_CHAR. */
 public static final String DOT_CHAR = ".";
 
 /** The Constant COMMA_CHAR. */
 public static final String COMMA_CHAR = ",";
 
 /** The Constant STRING_BOOLEAN_TRUE. */
 public static final String BOOLEAN_TRUE = "true";
 
 /** The Constant STRING_BOOLEAN_FALSE. */
 public static final String BOOLEAN_FALSE = "false";
 
 /** The Constant ALT_LABEL. */
 public static final String ALT_TEXT = "alt";
 
 /** The Constant for setting checking product. */
 public static final String IS_PRODUCT = "isProduct";
 
 /** The Constant FOUR. */
 public static final int FOUR = 4;
 
 /** The Constant TWENTY. */
 public static final int TWENTY = 20;
 
 /** The Constant THREE. */
 public static final int THREE = 3;
 
 /** The Constant SIX. */
 public static final int SIX = 6;
 
 /** The Constant THEME. */
 public static final String THEME = "theme";
 
 /** The Constant UNLIMITED. */
 public static final int UNLIMITED = 100;
 
 /** The Constant FIFTY. */
 public static final int FIFTY = 50;
 
 /**
  * Instantiates a new common constants.
  */
 private CommonConstants() {
  
 }
 
}
