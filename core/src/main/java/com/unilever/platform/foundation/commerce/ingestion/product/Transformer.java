/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion.product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;
import com.unilever.platform.foundation.commerce.dto.ErrorReport;
import com.unilever.platform.foundation.commerce.dto.ResultReport;
import com.unilever.platform.foundation.commerce.ingestion.ITransformer;

/**
 * The Class CSVTransformer.
 *
 * @author atiw22
 */
@Component(configurationFactory = true, metatype = false)
@Service
public class Transformer implements ITransformer {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(Transformer.class);
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant DEFAULT_IMAGE_FORMAT. */
 private static final String DEFAULT_IMAGE_FORMAT = "PNG";
 
 /** The Constant ELEVEN. */
 private static final int ELEVEN = 11;
 
 /** The Constant DAM_PATH. */
 private static final String DAM_PATH = "/content/dam";
 
 /** The json schema. */
 private String jsonSchema = "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"id\":\"http://jsonschema.net\","
   + "\"type\":\"object\",\"properties\":{\"skuParent\":{\"id\":\"http://jsonschema.net/skuParent\",\"type\":\"string\"},"
   + "\"cqTags\":{\"id\":\"http://jsonschema.net/cqTags\",\"type\":\"string\"},\"description\":{\"id\":\"http://jsonschema.net/description\","
   + "\"type\":\"string\"},\"title\":{\"id\":\"http://jsonschema.net/title\",\"type\":\"string\"},"
   + "\"productionDate\":{\"id\":\"http://jsonschema.net/productionDate\",\"type\":\"string\",\"additionalProperties\":false},"
   + "\"size\":{\"id\":\"http://jsonschema.net/size\",\"type\":\"string\"},\"price\":{\"id\":\"http://jsonschema.net/price\","
   + "\"type\":\"number\"},\"name\":{\"id\":\"http://jsonschema.net/name\",\"type\":\"string\"},\"ingredients\":"
   + "{\"id\":\"http://jsonschema.net/ingredients\",\"type\":\"string\"},\"smartProductId\":"
   + "{\"id\":\"http://jsonschema.net/smartProductId\",\"type\":\"string\"},\"sku\":{\"id\":\"http://jsonschema.net/sku\","
   + "\"type\":\"string\"},\"imageIds\":{\"id\":\"http://jsonschema.net/imageIds\",\"type\":\"string\"}},"
   + "\"required\":[\"cqTags\",\"description\",\"name\",\"smartProductId\",\"sku\",\"imageIds\"]}";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.commerce.ingestion.ITransformer# validateProducts(org.json.JSONArray)
  */
 @Override
 public ResultReport validateProducts(JSONArray jsonarray, String extension, SlingHttpServletRequest request, String storePath) {
  
  ResultReport report = new ResultReport();
  List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
  if ((jsonarray != null) && (jsonarray.length() > 0)) {
   errorReport = validateJSONArray(jsonarray, extension, request, storePath);
  }
  report.setErrorReport(errorReport);
  return report;
 }
 
 /**
  * Validate json array.
  *
  * @param jsonarray
  *         the jsonarray
  * @param extension
  *         the extension
  * @param request
  *         the request
  * @param storePath
  *         the store path
  * @return the list
  */
 private List<ErrorReport> validateJSONArray(JSONArray jsonarray, String extension, SlingHttpServletRequest request, String storePath) {
  List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
  for (int i = 0; i < jsonarray.length(); i++) {
   List<ErrorReport> tagsErrorReport = new ArrayList<ErrorReport>();
   List<ErrorReport> imgErrorReport = new ArrayList<ErrorReport>();
   ErrorReport errObj = null;
   try {
    JSONObject obj = jsonarray.getJSONObject(i);
    if (obj.has(UNICommerceConstants.TYPE) && (obj.get(UNICommerceConstants.TYPE).toString().equalsIgnoreCase(UNICommerceConstants.PRODUCT))) {
     String jsonData = jsonarray.get(i).toString();
     if (("csv").equalsIgnoreCase(extension)) {
      errObj = validateJSONObj(jsonData, i);
      tagsErrorReport = validateTags(request, i + TWO, jsonData);
      imgErrorReport = validateImages(request, i + TWO, jsonData, true, storePath);
     } else {
      tagsErrorReport = validateTags(request, i + ELEVEN, jsonData);
      imgErrorReport = validateImages(request, i + ELEVEN, jsonData, false, storePath);
     }
    }
   } catch (JSONException e) {
    LOG.debug("Error while fetching JSON Object", e);
    errObj = new ErrorReport();
    errObj.setErrorMessage("Error while fetching data.");
    errObj.setIndex(i + TWO);
   } catch (Exception e) {
    LOG.debug("Error while reading JSON Array", e);
    errObj = new ErrorReport();
    errObj.setErrorMessage("Error Occurred.");
    errObj.setIndex(i + TWO);
   }
   if (errObj != null) {
    errorReport.add(errObj);
   }
   
   for (ErrorReport tagsError : tagsErrorReport) {
    errorReport.add(tagsError);
   }
   
   for (ErrorReport imgError : imgErrorReport) {
    errorReport.add(imgError);
   }
  }
  return errorReport;
 }
 
 /**
  * Validate json obj.
  *
  * @param jsonData
  *         the json data
  * @param index
  *         the index
  * @return the error report
  */
 private ErrorReport validateJSONObj(String jsonData, int index) {
  
  ErrorReport err = null;
  JsonNode schemaNode = null;
  try {
   
   schemaNode = JsonLoader.fromString(jsonSchema);
   JsonNode data = JsonLoader.fromString(jsonData);
   JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
   
   // load the schema and validate
   JsonSchema schema = factory.getJsonSchema(schemaNode);
   ProcessingReport report = schema.validate(data);
   
   Iterator<ProcessingMessage> itr = report.iterator();
   while (itr.hasNext()) {
    err = new ErrorReport();
    ProcessingMessage message = itr.next();
    err.setErrorMessage(message.getMessage());
    err.setIndex(index + TWO);
   }
  } catch (IOException e) {
   LOG.debug("Error while reading JSON schema", e);
  } catch (ProcessingException e) {
   LOG.debug("Error while comparing JSON against schema", e);
  }
  return err;
  
 }
 
 /**
  * Validate tags.
  *
  * @param request
  *         the request
  * @param index
  *         the index
  * @param jsonData
  *         the json data
  * @return the list
  */
 private List<ErrorReport> validateTags(SlingHttpServletRequest request, int index, String jsonData) {
  List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
  JSONObject obj = new JSONObject(jsonData);
  if (obj != null && obj.has("cqTags")) {
   String tagValue = PIMInjestionUtility.get(obj, "cqTags");
   String[] tags = null;
   
   List<String> tagsList = new ArrayList<String>();
   if (tagValue != null) {
    if (tagValue.contains(",")) {
     String[] tags1 = tagValue.split(",");
     for (String t : tags1) {
      tagsList.add(t.trim());
     }
    } else {
     tagsList.add(tagValue.trim());
    }
   }
   
   if (!tagsList.isEmpty()) {
    tags = new String[tagsList.size()];
    tags = tagsList.toArray(tags);
   }
   
   if (tags != null) {
    TagManager tm = request.getResourceResolver().adaptTo(TagManager.class);
    for (String tag : tags) {
     ErrorReport err = null;
     Tag t = tm.resolve(tag);
     if (t == null) {
      err = new ErrorReport();
      err.setErrorMessage("Tag does not exist : " + tag);
      err.setIndex(index);
     }
     if (err != null) {
      errorReport.add(err);
     }
    }
   }
  }
  return errorReport;
 }
 
 /**
  * Validate images.
  *
  * @param request
  *         the request
  * @param index
  *         the index
  * @param jsonData
  *         the json data
  * @param oldIngestion
  *         the old ingestion
  * @param storePath
  *         the store path
  * @return the list
  */
 private List<ErrorReport> validateImages(SlingHttpServletRequest request, int index, String jsonData, boolean oldIngestion, String storePath) {
  List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
  JSONObject obj = new JSONObject(jsonData);
  
  String images = PIMInjestionUtility.get(obj, UNICommerceConstants.IMAGES);
  String imgType = PIMInjestionUtility.get(obj, "imageFormat");
  ResourceResolver resourceResolver = request.getResourceResolver();
  
  if (images != null) {
   String[] imageVideoArr = images.split(",");
   
   List<String> imagesList = new ArrayList<String>();
   List<String> videoList = new ArrayList<String>();
   
   for (String imgVid : imageVideoArr) {
    if (imgVid.contains("#")) {
     String[] array = imgVid.split("#");
     if (array.length == TWO) {
      imagesList.add(array[0].trim());
      videoList.add(array[1].trim());
     } else {
      imagesList.add(array[0].trim());
      videoList.add(StringUtils.EMPTY);
     }
    } else {
     imagesList.add(imgVid.trim());
     videoList.add(StringUtils.EMPTY);
    }
   }
   
   String[] imagesArr = new String[imagesList.size()];
   imagesArr = imagesList.toArray(imagesArr);
   
   for (String img : imagesArr) {
    if (StringUtils.isNotEmpty(img)) {
     
     if (img.contains(".")) {
      img = PIMInjestionUtility.getImageIdFromDecimalValue(img);
     }
     
     String localAssetBasePath = PIMInjestionUtility.getAssetLocationPath(storePath, resourceResolver, "localAssetBasePath");
     String globalAssetBasePath = PIMInjestionUtility.getAssetLocationPath(storePath, resourceResolver, "globalAssetBasePath");
     String imageFormat = PIMInjestionUtility.getAssetLocationPath(storePath, resourceResolver, "imageFormat");
     
     if (StringUtils.isNotBlank(imgType)) {
      imageFormat = imgType;
     }
     
     if (StringUtils.isBlank(imageFormat) && oldIngestion) {
      imageFormat = DEFAULT_IMAGE_FORMAT;
     }
     
     List list = new LinkedList();
     if (StringUtils.isNotBlank(localAssetBasePath)) {
      list = PIMInjestionUtility.getImageResourcesByID(img, localAssetBasePath, resourceResolver, imageFormat);
     }
     
     if (list.isEmpty() && StringUtils.isNotBlank(globalAssetBasePath)) {
      list = PIMInjestionUtility.getImageResourcesByID(img, globalAssetBasePath, resourceResolver, imageFormat);
     }
     
     if (list.isEmpty()) {
      list = PIMInjestionUtility.getImageResourcesByID(img, DAM_PATH, resourceResolver, imageFormat);
     }
     
     if (list.isEmpty()) {
      ErrorReport err = null;
      err = new ErrorReport();
      err.setErrorMessage("Image does not exist for image id : " + img + " and format : " + imageFormat);
      err.setIndex(index);
      
      if (err != null) {
       errorReport.add(err);
      }
     }
    }
   }
  }
  return errorReport;
 }
}
