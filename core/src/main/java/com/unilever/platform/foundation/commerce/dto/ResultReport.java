/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class ResultReport.
 */
public class ResultReport {
 
 /** The summary. */
 private String summary;
 
 /** The message list. */
 private List<String> messageList = new ArrayList<String>();
 
 /** The error report. */
 private List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
 
 /**
  * Gets the summary.
  *
  * @return the summary
  */
 public String getSummary() {
  return summary;
 }
 
 /**
  * Sets the summary.
  *
  * @param summary
  *         the summary to set
  */
 public void setSummary(String summary) {
  this.summary = summary;
 }
 
 /**
  * Gets the message list.
  *
  * @return the messageList
  */
 public List<String> getMessageList() {
  return messageList;
 }
 
 /**
  * Sets the message list.
  *
  * @param messageList
  *         the messageList to set
  */
 public void setMessageList(List<String> messageList) {
  this.messageList = messageList;
 }
 
 /**
  * Gets the error report.
  *
  * @return the error report
  */
 public List<ErrorReport> getErrorReport() {
  return errorReport;
 }
 
 /**
  * Sets the error report.
  *
  * @param errorReport
  *         the new error report
  */
 public void setErrorReport(List<ErrorReport> errorReport) {
  this.errorReport = errorReport;
 }
 
}
