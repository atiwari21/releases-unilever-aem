/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.workflow;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The Class ReplicateToPreview.
 */
@Component
@Service
@Property(name = "process.label", value = { "Enable preview and production" })
public class EnablePreviewAndProduction implements WorkflowProcess {
 
 private static final String ENABLED = "enabled";
 
 private static final String ETC_REPLICATION_AGENTS_AUTHOR = "/etc/replication/agents.author/";
 
 private static final String JCR_CONTENT = "/jcr:content";
 
 /** The Constant TYPE_JCR_PATH. */
 public static final String TYPE_JCR_PATH = "JCR_PATH";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(EnablePreviewAndProduction.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.day.cq.workflow.exec.WorkflowProcess#execute(com.day.cq.workflow.exec.WorkItem, com.day.cq.workflow.WorkflowSession,
  * com.day.cq.workflow.metadata.MetaDataMap)
  */
 @Override
 public void execute(WorkItem paramWorkItem, WorkflowSession paramWorkflowSession, MetaDataMap paramMetaDataMap) throws WorkflowException {
  com.day.cq.workflow.exec.WorkflowData workflowData = paramWorkItem.getWorkflowData();
  if (workflowData.getPayloadType().equals(TYPE_JCR_PATH)) {
   Session jcrSession = paramWorkflowSession.getSession();
   setReplicationAgent(jcrSession, "Publish-0101", true);
   setReplicationAgent(jcrSession, "Publish-0202", true);
  }
 }
 
 /**
  * Sets the replication agent.
  * 
  * @param jcrSession
  *         the jcr session
  * @param agent
  *         the agent
  * @param agentEnabled
  *         the agent enabled
  */
 private void setReplicationAgent(Session jcrSession, String agent, boolean agentEnabled) {
  try {
   String agentResourcePath = ETC_REPLICATION_AGENTS_AUTHOR + agent + JCR_CONTENT;
   Node agentResourceNode = (jcrSession != null) ? (Node) jcrSession.getItem(agentResourcePath) : null;
   if (agentResourceNode != null) {
    agentResourceNode.setProperty(ENABLED, agentEnabled);
    agentResourceNode.save();
   }
  } catch (RepositoryException e) {
   LOGGER.error("Repository Exception", e);
  }
 }
 
}
