/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.json.JSONArray;

import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;
import com.unilever.platform.foundation.commerce.dto.ResultReport;
import com.unilever.platform.foundation.commerce.ingestion.product.CSVExtractor;
import com.unilever.platform.foundation.commerce.ingestion.product.ExcelExtractor;

/**
 * The Interface IExtractor.
 *
 * Interface for extractor class
 */
@Component(immediate = true, metatype = true)
@Service
public class ExtractorFactory implements IExtractor {
 
 /** The csvextractor. */
 @Reference
 CSVExtractor csvextractor;
 
 /** The excelextractor. */
 @Reference
 ExcelExtractor excelextractor;
 
 /**
  * Gets the extractor.
  *
  * @param extension
  *         the extension
  * @return the extractor
  */
 public IExtractor getExtractor(String extension) {
  IExtractor extractor = null;
  
  if (!StringUtils.isNotBlank(extension)) {
   extractor = null;
  }
  if (("csv").equalsIgnoreCase(extension)) {
   csvextractor = PIMInjestionUtility.getServiceReference(CSVExtractor.class);
   return csvextractor;
   
  } else if (("xlsm").equalsIgnoreCase(extension)) {
   excelextractor = PIMInjestionUtility.getServiceReference(ExcelExtractor.class);
   return excelextractor;
  }
  return extractor;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.commerce.ingestion.IExtractor# extractData(java.io.InputStream,
  * org.apache.sling.api.SlingHttpServletRequest)
  */
 @Override
 public JSONArray extractData(InputStream stream, SlingHttpServletRequest request) throws IOException {
  return null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.commerce.ingestion.IExtractor# getErrorReport()
  */
 @Override
 public ResultReport getErrorReport() {
  return null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.foundation.commerce.ingestion.IExtractor# setErrorReport()
  */
 @Override
 public void setErrorReport() {
  return;
 }
}
