/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.workflow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.ParticipantStepChooser;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Class UnileverDynamicParticipant.
 */
@Component
@Service
@Properties({ @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever implementation of a dynamic participant chooser."),
  @Property(name = Constants.SERVICE_VENDOR, value = "Adobe"),
  @Property(name = ParticipantStepChooser.SERVICE_PROPERTY_LABEL, value = "Unilever Dynamic Participant Chooser") })
public class UnileverDynamicParticipant implements ParticipantStepChooser {
 
 private static final String TYPE_JCR_PATH = "JCR_PATH";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UnileverDynamicParticipant.class);
 
 /** The resource resolver factory. */
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant REP_PRINCIPAL_NAME. */
 private static final String REP_PRINCIPAL_NAME = "rep:principalName";
 
 /** The Constant REP_PRIVILAGES. */
 private static final String REP_PRIVILAGES = "rep:privileges";
 
 /** The Constant REP_POLICY. */
 private static final String REP_POLICY = "/rep:policy";
 
 /** The Constant CONTENT_DAM. */
 private static final String CONTENT_DAM = "/content/dam";
 
 /** The Constant WORKFLOW_CONFIG_CAT. */
 private static final String WORKFLOW_CONFIG_CAT = "workflowConfiguration";
 
 /** The Constant CONTAINS. */
 private static final String CONTAINS = "contains";
 
 /** The Constant PAGE_LEVEL_ACCESS. */
 private static final String PAGE_LEVEL_ACCESS = "pageLavelAccessPath";
 
 /** The Constant PROCESS_ARGS. */
 private static final String PROCESS_ARGS = "PROCESS_ARGS";
 
 /** The Constant ETC_TAGS. */
 private static final String ETC_TAGS = "/etc/tags";
 
 private static final int TWO = 2;
 
 @Override
 public String getParticipant(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap paramMetaDataMap) throws WorkflowException {
  ResourceResolver resourceResolver = null;
  List<String> finalList = new ArrayList<String>();
  try {
   resourceResolver = getResourceResolver(workflowSession.getSession());
  } catch (Exception e) {
   LOGGER.error("Error in getting resolver", e);
  }
  WorkflowData workflowData = workItem.getWorkflowData();
  if (workflowData.getPayloadType().equals(TYPE_JCR_PATH)) {
   String configContains = StringUtils.EMPTY;
   boolean isPageWorkflow = true;
   String payloadPath = workflowData.getPayload().toString();
   Resource currentResource = resourceResolver.getResource(payloadPath);
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pageManager.getContainingPage(currentResource);
   String pathFromArgument = paramMetaDataMap.get(PROCESS_ARGS, String.class);
   String damOrTagPath = StringUtils.EMPTY;
   if (payloadPath.startsWith(CONTENT_DAM) || payloadPath.startsWith(ETC_TAGS)) {
    if (null != pathFromArgument) {
     String[] arr = pathFromArgument.split(",");
     damOrTagPath = getPathFromLevel(payloadPath, arr[0].trim());
     configContains = arr.length >= TWO ? arr[1].trim() : StringUtils.EMPTY;
    }
    isPageWorkflow = false;
   }
   String permissionPath = getPermissionLevelPath(payloadPath, damOrTagPath, currentPage);
   List<String> participants = getRecipient(resourceResolver, permissionPath);
   if (!participants.isEmpty()) {
    if (isPageWorkflow) {
     try {
      configContains = configurationService.getConfigValue(currentPage, WORKFLOW_CONFIG_CAT, CONTAINS);
     } catch (ConfigurationNotFoundException e) {
      LOGGER.error(e.getMessage() + ", Exception Trace:" + e);
     }
    }
    Iterator<String> listItr = participants.iterator();
    while (listItr.hasNext()) {
     String str = listItr.next();
     if (!finalList.contains(str) && str.contains(configContains)) {
      finalList.add(str);
     }
    }
   }
   if (!finalList.isEmpty()) {
    Collections.sort(finalList);
    LOGGER.info("Participant returned is: " + finalList.get(0));
    return finalList.get(0);
   } else {
    LOGGER.info("Participant List is empty. Hence admin is returned");
   }
  }
  return "admin";
 }
 
 /**
  * Gets the resource resolver.
  *
  * @param session
  *         the session
  * @return the resource resolver
  * @throws LoginException
  *          the login exception
  */
 private ResourceResolver getResourceResolver(Session session) throws LoginException {
  return resourceResolverFactory
    .getResourceResolver(Collections.<String, Object> singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session));
 }
 
 /**
  * Gets the participant List.
  *
  * @param resolver
  *         the resolver
  * @param permissionPath
  *         the permissionPath
  * @return participantList the participantList
  */
 private List<String> getRecipient(ResourceResolver resolver, String permissionPath) {
  List<String> participantList = new ArrayList<String>();
  if (StringUtils.isNotEmpty(permissionPath)) {
   Resource resource = resolver.getResource(permissionPath + REP_POLICY);
   if (null != resource) {
    NodeIterator iterator;
    try {
     iterator = resource.adaptTo(Node.class).getNodes();
     while (iterator.hasNext()) {
      Node node = iterator.nextNode();
      String principalName = node.getProperty(REP_PRINCIPAL_NAME).getString();
      Value[] privilagesValue = node.getProperty(REP_PRIVILAGES).getValues();
      Set<String> privilagesValues = new HashSet<String>();
      privilagesValues.add("jcr:all");
      privilagesValues.add("crx:replicate");
      privilagesValues.add("rep:write");
      privilagesValues.add("jcr:versionManagement");
      privilagesValues.add("jcr:modifyAccessControl");
      privilagesValues.add("jcr:modifyProperties");
      privilagesValues.add("jcr:lockManagement");
      privilagesValues.add("jcr:removeChildNodes");
      privilagesValues.add("jcr:removeNode");
      privilagesValues.add("jcr:addChildNodes");
      privilagesValues.add("jcr:nodeTypeManagement");
      for (int i = 0; i < privilagesValue.length; i++) {
       if (privilagesValues.contains(privilagesValue[i].getString())) {
        participantList.add(principalName);
        break;
       }
      }
     }
    } catch (RepositoryException e) {
     LOGGER.error(e.getMessage() + ", Exception Trace:" + e);
    }
   } else {
    LOGGER.info("No " + REP_POLICY + "Node exists at " + permissionPath);
   }
  }
  return participantList;
 }
 
 /**
  * Gets the permission Path.
  *
  * @param payloadPath
  *         the payloadPath
  * @param processArgument
  *         the processArgument
  * @param currentPage
  *         the currentPage
  * @return permissionPath the permissionPath
  */
 private String getPermissionLevelPath(String payloadPath, String damOrTagPath, Page currentPage) {
  String pageLevelAccessPath = StringUtils.EMPTY;
  String permissionPath = StringUtils.EMPTY;
  if (null != currentPage) {
   BrandMarketBean bm = new BrandMarketBean(currentPage);
   String marketPagePath = StringUtils.EMPTY;
   String brandName = bm.getBrand();
   String market = bm.getMarket();
   if (brandName != null && market != null) {
    marketPagePath = "/content/" + brandName + "/" + market;
   }
   try {
    pageLevelAccessPath = configurationService.getConfigValue(currentPage, WORKFLOW_CONFIG_CAT, PAGE_LEVEL_ACCESS);
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error(e.getMessage() + ", Exception Trace:" + e);
   }
   if (StringUtils.isEmpty(pageLevelAccessPath)) {
    permissionPath = marketPagePath;
   } else {
    permissionPath = getPathFromLevel(payloadPath, pageLevelAccessPath.trim());
   }
  } else if (payloadPath.startsWith(CONTENT_DAM) || payloadPath.startsWith(ETC_TAGS)) {
   if (StringUtils.isNotEmpty(damOrTagPath)) {
    permissionPath = damOrTagPath;
   } else {
    permissionPath = payloadPath;
   }
  }
  return permissionPath;
  
 }
 
 /**
  * Gets the path from level.
  *
  * @param resolver
  *         the resolver
  * @param payloadPath
  *         the payloadPath
  * @param depthLevel
  *         the depthLevel
  * @return permissionPath the permissionPath
  */
 private String getPathFromLevel(String payloadPath, String depthLevel) {
  String permissionPath = StringUtils.EMPTY;
  if (StringUtils.isNotEmpty(depthLevel)) {
   int depth = Integer.parseInt(depthLevel);
   String[] strArr = payloadPath.split("/");
   if (strArr.length < depth) {
    return payloadPath;
   }
   String splitter = strArr[depth];
   String initialPath = StringUtils.substringBefore(payloadPath, splitter);
   permissionPath = initialPath + splitter;
  }
  return permissionPath;
  
 }
}
