/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion.product;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.RepositoryException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.i18n.I18n;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.aem.foundation.integration.dto.SchemaConfigurationDTO;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;
import com.unilever.platform.foundation.commerce.dto.ErrorReport;
import com.unilever.platform.foundation.commerce.dto.Product;
import com.unilever.platform.foundation.commerce.dto.ProductParser;
import com.unilever.platform.foundation.commerce.dto.ResultReport;
import com.unilever.platform.foundation.commerce.ingestion.IExtractor;

@Component(immediate = true, enabled = true, metatype = true)
@Service(value = RewardsExcelExtractor.class)
public class RewardsExcelExtractor implements IExtractor {
 
 private static final int TWO = 2;
 
 private static final String REGULAR_EXPRESSION_MISSMATCH = "excelExtractor.regexMismatchMsg";
 
 private static final String LENGTH_FAILED = "excelExtractor.lengthFailedMsg";
 
 private static final String MUST_CONTAIN_MISSING = "excelExtractor.mustContainMsg";
 
 private static final String MANDATORY_FIELD_MISSING = "excelExtractor.mandatoryFieldMissingMsg";
 
 /** The Constant LOG. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RewardsExcelExtractor.class);
 
 /** The property map taken from osgi configuration. */
 protected Map<String, String> props = new LinkedHashMap<String, String>();
 
 private Map<String, SchemaConfigurationDTO> pimFieldMap = new LinkedHashMap<String, SchemaConfigurationDTO>();
 
 /**
  * The column name index map contains information related to header with column numbers and object type.
  */
 protected Map<Integer, ProductParser> colNameIndexMap = new LinkedHashMap<Integer, ProductParser>();
 
 protected List<ErrorReport> errorReport = new ArrayList<ErrorReport>();
 
 /** The Constant PROPERTY_MAPPINGS. */
 @Property(unbounded = PropertyUnbounded.ARRAY, label = "Excel column Mappings", description = "List of Reward Excel column mappings.")
 private static final String PROPERTY_MAPPINGS = "column.mappings";
 
 /**
  * Activator for the service. Reads default mappings and store them in map.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  String[] excelColumnMappings = PropertiesUtil.toStringArray(ctx.getProperties().get(PROPERTY_MAPPINGS),
    PIMInjestionUtility.getRewardDefaultColumnMapping());
  
  if (excelColumnMappings != null) {
   for (String s : excelColumnMappings) {
    String[] prop = s.split("=");
    props.put(prop[0], prop[1]);
   }
  }
 }
 
 /**
  * Returns input stream and convert it in JSON array.
  *
  * @param stream
  *         the stream
  * @param request
  *         the request
  * @return the JSON array
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 @Override
 public JSONArray extractData(InputStream stream, SlingHttpServletRequest request) throws IOException {
  
  I18n i18n = new I18n(request);
  ResourceResolver rs = request.getResourceResolver();
  String importLocation = PIMInjestionUtility.getImportLocation(request);
  JSONArray jsonArray = null;
  try {
   if (rs != null && StringUtils.isNotBlank(importLocation) && stream != null) {
    Resource schemaConfigurationResource = PIMInjestionUtility.getSchemaResource(importLocation, rs);
    if (schemaConfigurationResource != null && schemaConfigurationResource.getChild("jcr:content/par") != null) {
     schemaConfigurationResource = schemaConfigurationResource.getChild("jcr:content/par");
     setPimFieldsMappings(schemaConfigurationResource);
     jsonArray = getJsonArrayFromPimSheet(getPIMSheet(stream), i18n);
     
    } else {
     ErrorReport errObj = new ErrorReport();
     errObj.setErrorMessage(i18n.get("excelExtractor.createSchema"));
     errObj.setIndex(0);
     errorReport.add(errObj);
     jsonArray = new JSONArray();
    }
   } else {
    ErrorReport errObj = new ErrorReport();
    errObj.setErrorMessage(i18n.get("excelExtractor.validImportLocation"));
    errObj.setIndex(0);
    errorReport.add(errObj);
    jsonArray = new JSONArray();
   }
  } catch (RepositoryException re) {
   LOGGER.error("Error while extracting data from Excel", re);
  } catch (Exception e) {
   LOGGER.error("Error while parsing JSON schema", e);
  }
  return jsonArray;
 }
 
 private void setPimFieldsMappings(Resource schemaConfigurationResource) throws RepositoryException {
  List<String> removableKeys = new ArrayList<>();
  
  PIMInjestionUtility.getPIMSchemaConfiguration(schemaConfigurationResource, pimFieldMap);
  for (String key : pimFieldMap.keySet()) {
   if (!"Y".equals(pimFieldMap.get(key).getIsApplicable())) {
    removableKeys.add(key);
   }
  }
  
  for (String s : removableKeys) {
   pimFieldMap.remove(s);
  }
  
  // setting column map index for parsing the excel
  setColumnMap(pimFieldMap);
 }
 
 private JSONArray getJsonArrayFromPimSheet(XSSFSheet pimSheet, I18n i18n) {
  JSONArray jsonarr = new JSONArray();
  
  if (pimSheet != null) {
   boolean flag = false;
   for (int rowNum = 2; rowNum < pimSheet.getLastRowNum() + 1; rowNum++) {
    Row row = pimSheet.getRow(rowNum);
    if (pimFieldMap.containsKey("rewardID") && row != null) {
     int columnNumber = PIMInjestionUtility.titleToNumber(pimFieldMap.get("rewardID").getColumn()) - 1;
     String ean = row.getCell(columnNumber).getStringCellValue();
     if (StringUtils.isNotBlank(ean)) {
      // getting json object for each row
      JSONObject jsonObj = new JSONObject(getProduct(row, rowNum, i18n));
      jsonarr.put(jsonObj);
     } else {
      flag = true;
     }
    } else {
     flag = true;
    }
    if (flag) {
     break;
    }
   }
  }
  return jsonarr;
 }
 
 private Product getProduct(Row row, int rowNumber, I18n i18n) {
  Product product = new Product();
  product.setType("product");
  boolean isVariant = false;
  if (pimFieldMap.containsKey("rewardParentID")) {
   SchemaConfigurationDTO obj = pimFieldMap.get("rewardParentID");
   
   int index = PIMInjestionUtility.titleToNumber(obj.getColumn()) - 1;
   Cell cell = row.getCell(index);
   String colValue = StringUtils.EMPTY;
   if (cell != null) {
    switch (cell.getCellType()) {
     case Cell.CELL_TYPE_NUMERIC:
      colValue = String.valueOf(cell.getNumericCellValue());
      break;
     case Cell.CELL_TYPE_STRING:
      colValue = cell.getStringCellValue();
      break;
     default:
      break;
    }
   }
   
   if (StringUtils.isNotBlank(colValue)) {
    isVariant = true;
   }
  }
  
  for (String key : pimFieldMap.keySet()) {
   SchemaConfigurationDTO obj = pimFieldMap.get(key);
   int index = PIMInjestionUtility.titleToNumber(obj.getColumn()) - 1;
   
   Cell cell = row.getCell(index);
   String colValue = StringUtils.EMPTY;
   if (cell != null) {
    switch (cell.getCellType()) {
     case Cell.CELL_TYPE_NUMERIC:
      colValue = String.valueOf(cell.getNumericCellValue());
      break;
     case Cell.CELL_TYPE_STRING:
      colValue = cell.getStringCellValue();
      break;
     default:
      break;
    }
   }
   
   if (StringUtils.isBlank(colValue) && StringUtils.isNotBlank(obj.getMandatoryDefaultValue())) {
    colValue = obj.getMandatoryDefaultValue();
   }
   
   if (StringUtils.isBlank(colValue)) {
    // check mandatory
    if (isMandatory(obj) && !isVariant) {
     logErrorMessage(obj.getFieldName(), obj.getColumn(), rowNumber, MANDATORY_FIELD_MISSING, colValue, i18n);
    }
    
    // check mandatory for variant
    if (isMandatoryForVariant(obj) && isVariant) {
     logErrorMessage(obj.getFieldName(), obj.getColumn(), rowNumber, MANDATORY_FIELD_MISSING, colValue, i18n);
    }
   } else {
    // check must contain value
    if (!havingMustContainData(obj, colValue)) {
     logErrorMessage(obj.getFieldName(), obj.getColumn(), rowNumber, MUST_CONTAIN_MISSING, obj.getDataMustContain(), i18n);
    }
    
    // check min length
    if (StringUtils.isNotBlank(obj.getMinLength())) {
     int minLength = Integer.parseInt(obj.getMinLength());
     if (colValue.length() < minLength) {
      logErrorMessage(obj.getFieldName(), obj.getColumn(), rowNumber, LENGTH_FAILED, colValue, i18n);
     }
    }
    
    // check max length
    if (StringUtils.isNotBlank(obj.getMaxLength())) {
     int maxLength = Integer.parseInt(obj.getMaxLength());
     if (colValue.length() > maxLength) {
      logErrorMessage(obj.getFieldName(), obj.getColumn(), rowNumber, LENGTH_FAILED, colValue, i18n);
     }
    }
    
    // check regex validation
    String regexExpression = obj.getRegex();
    if (StringUtils.isNotBlank(regexExpression) && !isPatternMatching(colValue.trim(), regexExpression.trim())) {
     logErrorMessage(obj.getFieldName(), obj.getColumn(), rowNumber, REGULAR_EXPRESSION_MISSMATCH, colValue, i18n);
    }
   }
   product = getProductByReflection(index, colValue, product, rowNumber);
  }
  
  if (product != null && product.getSkuParent() != null && StringUtils.isNotBlank(String.valueOf(product.getSkuParent()))) {
   product.setType("variation");
  }
  
  return product;
 }
 
 private void logErrorMessage(String fieldName, String columnName, int rowNumber, String message, String colValue, I18n i18n) {
  ErrorReport errObj = new ErrorReport();
  errObj.setIndex(rowNumber + 1);
  switch (message) {
   case MANDATORY_FIELD_MISSING:
    errObj.setErrorMessage(MessageFormat.format(i18n.get(message), fieldName, columnName, String.valueOf(rowNumber + 1)));
    break;
   case MUST_CONTAIN_MISSING:
    errObj.setErrorMessage(MessageFormat.format(i18n.get(message), colValue, fieldName, columnName, String.valueOf(rowNumber + 1)));
    break;
   case LENGTH_FAILED:
    errObj.setErrorMessage(MessageFormat.format(i18n.get(message), fieldName, columnName, String.valueOf(rowNumber + 1)));
    break;
   case REGULAR_EXPRESSION_MISSMATCH:
    errObj.setErrorMessage(MessageFormat.format(i18n.get(message), fieldName, columnName, String.valueOf(rowNumber + 1)));
    break;
   default:
    break;
  }
  errorReport.add(errObj);
 }
 
 private boolean havingMustContainData(SchemaConfigurationDTO obj, String colValue) {
  boolean havingMustContainData = true;
  String dataMustContain = obj.getDataMustContain();
  if (StringUtils.isNotBlank(dataMustContain)) {
   String[] mustContainValArray = dataMustContain.trim().split(",");
   for (String s : mustContainValArray) {
    if (!colValue.contains(s)) {
     havingMustContainData = false;
     break;
    }
   }
  }
  return havingMustContainData;
 }
 
 private boolean isPatternMatching(String value, String regexPattern) {
  
  Pattern ptn = Pattern.compile(regexPattern, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
  Matcher mtch = ptn.matcher(value);
  if (mtch.find()) {
   return true;
  }
  return false;
 }
 
 private boolean isMandatoryForVariant(SchemaConfigurationDTO obj) {
  boolean isMandatory = false;
  if (obj != null && "Y".equals(obj.getMandatoryVariant())) {
   isMandatory = true;
  }
  return isMandatory;
 }
 
 private boolean isMandatory(SchemaConfigurationDTO obj) {
  boolean isMandatory = false;
  if (obj != null && "Y".equals(obj.getMandatory())) {
   isMandatory = true;
  }
  return isMandatory;
 }
 
 private void setColumnMap(Map<String, SchemaConfigurationDTO> pimFieldMap) {
  
  for (String key : pimFieldMap.keySet()) {
   SchemaConfigurationDTO obj = pimFieldMap.get(key);
   String colName = obj.getFieldName();
   
   ProductParser parserObj = new ProductParser();
   parserObj.setColumnName(colName);
   if (colName.contains("#")) {
    
    String[] arr = colName.split("#");
    int index = Integer.parseInt(arr[0]);
    parserObj.setIndex(index);
    String[] prop = arr[1].split("\\.");
    parserObj.setPropertyName(prop[1]);
    parserObj.setKeyName(getPropertyKeyForColumn(prop[0]));
    
   } else if (colName.contains(".")) {
    
    String[] prop = colName.split("\\.");
    parserObj.setIndex(1);
    parserObj.setPropertyName(prop[1]);
    parserObj.setKeyName(getPropertyKeyForColumn(prop[0]));
    
   } else {
    parserObj.setIndex(0);
    parserObj.setPropertyName(null);
    parserObj.setKeyName(getPropertyKeyForColumn(colName));
   }
   colNameIndexMap.put(PIMInjestionUtility.titleToNumber(obj.getColumn()) - 1, parserObj);
  }
 }
 
 private XSSFSheet getPIMSheet(InputStream stream) {
  XSSFSheet pimSheet = null;
  OPCPackage opcPackage = null;
  try {
   opcPackage = OPCPackage.open(stream);
   XSSFWorkbook hssfWorkbook = new XSSFWorkbook(opcPackage);
   pimSheet = hssfWorkbook.getSheet(PIMInjestionUtility.RIM_REWARDS_DATA_SHEET_NAME);
   
  } catch (Exception e) {
   LOGGER.error("Coudn't find PIM sheet", e);
   ErrorReport errObj = new ErrorReport();
   errObj.setErrorMessage("Coudn't find PIM sheet");
   errObj.setIndex(0);
   errorReport.add(errObj);
  }
  
  return pimSheet;
 }
 
 @Override
 public ResultReport getErrorReport() {
  ResultReport report = new ResultReport();
  report.setErrorReport(errorReport);
  return report;
 }
 
 @SuppressWarnings({ "unchecked", "rawtypes" })
 private Object setCustomKeyObj(Object obj, String keyName, String propName, String colName, String colValue, int pos)
   throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
  
  if (colValue != null) {
   
   String className = UNICommerceConstants.DTO_NAMESPACE + keyName;
   Class cls = Class.forName(className);
   Object classObj = cls.newInstance();
   if (propName == null) {
    // set additional property
    classObj = PIMInjestionUtility.setAdditionalKeyName(classObj, colName, colValue);
   } else {
    // set property
    classObj = setKeyName(classObj, propName, colValue);
   }
   
   // checking whether object is of list type
   
   String methodName = "get" + keyName;
   Method method = obj.getClass().getMethod(methodName);
   Object getterObj = method.invoke(obj);
   
   String mName = "set" + keyName;
   if (getterObj instanceof List<?>) {
    
    List<Object> listObj;
    listObj = (List<Object>) getterObj;
    if ((getterObj != null) && (((List) getterObj).size() >= pos)) {
     
     Object customObj = ((List) getterObj).get(pos - 1);
     if (customObj != null) {
      customObj = setKeyName(customObj, propName, colValue);
     }
     listObj.set(pos - 1, customObj);
     
    } else {
     listObj.add(classObj);
    }
    
    Method listmethod = obj.getClass().getMethod(mName, List.class);
    listmethod.invoke(obj, listObj);
    
   } else {
    
    Method mtd = null;
    if (getterObj != null) {
     mtd = obj.getClass().getMethod(mName, classObj.getClass());
     getterObj = setKeyName(getterObj, propName, colValue);
     mtd.invoke(obj, getterObj);
    } else {
     mtd = obj.getClass().getMethod(mName, classObj.getClass());
     mtd.invoke(obj, classObj);
    }
   }
  }
  return obj;
 }
 
 private Product getProductByReflection(int index, String colValue, Product prod, int row) {
  
  ErrorReport errObj = null;
  Product product = prod;
  try {
   ProductParser p = colNameIndexMap.get(index);
   if (p != null) {
    String colName = p.getColumnName();
    int in = p.getIndex();
    String keyName = p.getKeyName();
    String propName = p.getPropertyName();
    
    if (keyName != null) {
     
     // when mapping is present
     if (in == 0) {
      
      // Setting product for single-valued property
      product = (Product) setKeyName(product, keyName, colValue);
     } else {
      
      product = (Product) setCustomKeyObj(product, keyName, propName, colName, colValue, in);
     }
    } else {
     
     // Setting product when no mapping is present for property
     product = (Product) PIMInjestionUtility.setAdditionalKeyName(product, colName, colValue);
    }
    
   }
  } catch (NumberFormatException e) {
   errObj = new ErrorReport();
   errObj.setErrorMessage("Error while fetching column" + e.getMessage());
   errObj.setIndex(row + TWO);
   LOGGER.error("Error while creating product json : ", e);
  } catch (Exception e) {
   errObj = new ErrorReport();
   errObj.setErrorMessage("Error while fetching product data.");
   errObj.setIndex(row + TWO);
   LOGGER.error("Error while calling method : ", e);
  }
  
  if (errObj != null) {
   errorReport.add(errObj);
  }
  return product;
 }
 
 /**
  * Invokes the set property for the Product object.
  *
  * @param obj
  *         the obj @param keyName the key name @param colValue the col value @return the object @throws NoSuchMethodException the no such method
  *         exception @throws IllegalAccessException the illegal access exception @throws InvocationTargetException the invocation target
  *         exception @throws
  */
 private Object setKeyName(Object obj, String keyName, String colValue)
   throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
  
  String methodName = "set" + keyName;
  Method method = null;
  
  try {
   if (colValue != null && StringUtils.isNotBlank(colValue)) {
    if (PIMInjestionUtility.methodExists(obj, methodName, String.class)) {
     method = obj.getClass().getMethod(methodName, String.class);
     method.invoke(obj, colValue);
    } else if (PIMInjestionUtility.methodExists(obj, methodName, long.class)) {
     long value = Long.parseLong(colValue);
     method = obj.getClass().getMethod(methodName, long.class);
     method.invoke(obj, value);
    } else if (PIMInjestionUtility.methodExists(obj, methodName, float.class)) {
     float value = Float.parseFloat(colValue);
     method = obj.getClass().getMethod(methodName, float.class);
     method.invoke(obj, value);
    } else if (PIMInjestionUtility.methodExists(obj, methodName, int.class)) {
     int value = Integer.parseInt(colValue);
     method = obj.getClass().getMethod(methodName, int.class);
     method.invoke(obj, value);
    } else {
     PIMInjestionUtility.setAdditionalKeyName(obj, keyName, colValue);
    }
   }
  } catch (NoSuchMethodException e) {
   LOGGER.error("Error while getting no method ", e);
  }
  
  return obj;
 }
 
 /**
  * Gets the property key for column.
  *
  * @param columnName
  *         the column name
  * @return the property key for column
  */
 private String getPropertyKeyForColumn(String columnName) {
  
  String key = null;
  
  Set<String> keys = props.keySet();
  if (props.containsValue(columnName)) {
   for (String k : keys) {
    String value = props.get(k);
    if (value.equals(columnName)) {
     key = k;
    }
   }
  }
  return key;
 }
 
 @Override
 public void setErrorReport() {
  errorReport = new ArrayList<ErrorReport>();
 }
}
