/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.workflow;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.AgentIdFilter;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.unilever.platform.aem.foundation.configuration.ConfigurationHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.utils.PreviewProductionAgentsUtility;

/**
 * The Class ReplicateToProduction.
 */
@Component
@Service
@Property(name = "process.label", value = { "Customized unpublish production" })
public class DeActivateToProduction implements WorkflowProcess {
 
 /** The Constant TYPE_JCR_PATH. */
 public static final String TYPE_JCR_PATH = "JCR_PATH";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(DeActivateToProduction.class);
 
 /** The resource resolver factory. */
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 @Reference
 GlobalConfiguration globalConfiguration;
 @Reference
 Replicator replicator;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.day.cq.workflow.exec.WorkflowProcess#execute(com.day.cq.workflow.exec.WorkItem, com.day.cq.workflow.WorkflowSession,
  * com.day.cq.workflow.metadata.MetaDataMap)
  */
 @Override
 public void execute(WorkItem paramWorkItem, WorkflowSession paramWorkflowSession, MetaDataMap paramMetaDataMap) throws WorkflowException {
  com.day.cq.workflow.exec.WorkflowData workflowData = paramWorkItem.getWorkflowData();
  ResourceResolver resolver = null;
  try {
   if (workflowData.getPayloadType().equals(TYPE_JCR_PATH)) {
    Session jcrSession = paramWorkflowSession.getSession();
    resolver = ConfigurationHelper.getResourceResolver(resourceResolverFactory, "readService");
    PageManager pageManager = (resolver != null) ? resolver.adaptTo(PageManager.class) : null;
    Page page = (pageManager != null) ? pageManager.getContainingPage(workflowData.getPayload().toString()) : null;
    Resource pageResource = (resolver != null && page != null) ? resolver.getResource(page.getPath() + CommonConstants.JCR_CONSTANTS) : null;
    Node node = (pageResource != null) ? pageResource.adaptTo(Node.class) : null;
    String[] agentIds = PreviewProductionAgentsUtility.getProductionAgents(globalConfiguration);
    if (agentIds.length > 0 && node != null) {
     ReplicationOptions replicationOptions = new ReplicationOptions();
     AgentIdFilter agentIdFilter = new AgentIdFilter(agentIds);
     replicationOptions.setFilter(agentIdFilter);
     replicator.replicate(jcrSession, ReplicationActionType.DEACTIVATE, page.getPath(), replicationOptions);
     node.setProperty("lastReplicatedToPreview", (Value) null);
     node.setProperty("lastReplicatedToPreviewBy", (Value) null);
     node.setProperty("lastReplicatedToProduction", (Value) null);
     node.setProperty("lastReplicatedToProductionBy", (Value) null);
     resolver.commit();
    }
   }
  } catch (Exception e) {
   LOGGER.error("Exception found", e);
  } finally {
   if (resolver != null) {
    resolver.close();
   }
  }
 }
}
