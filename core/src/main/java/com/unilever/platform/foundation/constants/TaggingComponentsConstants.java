/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.constants;

/**
 * The Class TaggingComponentsConstants.
 */
public class TaggingComponentsConstants {
 /** The Constant EXTERNAL_URL_REQUIRED. */
 public static final String EXTERNAL_URL_REQUIRED = "externalUrlRequired";
 
 /** The Constant TRUE. */
 public static final String TRUE = "[true]";
 
 /** The Constant FIXED_LIST. */
 public static final String FIXED_LIST = "fixedList";
 
 /** The Constant TAGS. */
 public static final String TAGS = "tags";
 
 public static final String IMAGE_FLAG = "imageFlag";
 
 public static final String COPY_FLAG = "copyFlag";
 
 public static final String LONG_COPY_FLAG = "longCopyFlag";
 
 public static final String PUBLISH_DATE_FLAG = "publishDateFlag";
 
 public static final String LAST_REVIEW_DATE_FLAG = "lastReviewDateFlag";
 
 /** The Constant READ_TIME. */
 public static final String READ_TIME = "readTime";
 
 public static final String PARETNT_PAGE_DETAILS_FLAG = "parentPageDetailsFlag";
 
 /** The Constant TEASER IMAGE FLAG. */
 public static final String TEASER_IMAGE_FLAG = "teaserImageFlag";
 
 /** The Constant TEASER COPY FLAG. */
 public static final String TEASER_COPY_FLAG = "teaserCopyFlag";
 
 /** The Constant TEASER LONG COPY. */
 public static final String TEASER_LONG_COPY_FLAG = "teaserLongCopyFlag";
 
 /** The Constant TEASER PUBLISH DATE. */
 public static final String TEASER_PUBLISH_DATE_FLAG = "teaserPublishDateFlag";
 
 /** The Constant READ_TIME_FLAG. */
 public static final String READ_TIME_FLAG = "readTimeFlag";
 
 /** The Constant PAGE PARENT DETAILS FLAG. */
 public static final String PARENT_DETAILS_FLAG = "parentPageDetailsFlag";
 
 /** The Constant AUTHOR NAME */
 public static final String AUTHOR_NAME_FLAG = "authorNameFlag";
 
 /**
  * Instantiates a new tagging components constants.
  */
 private TaggingComponentsConstants() {
  
 }
}
