/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.unilever.platform.aem.foundation.utils.CacheUtil;

/**
 * The Class FrontEndConfigurationService.
 */
@Component(immediate = true, metatype = true)
@Service(value = FrontEndConfigurationService.class)
@Properties({
    @Property(name =ConfigurationConstants.MAX_RECORD_IN_CACHE, value = "100", label = "Cache Size"),
    @Property(name = ConfigurationConstants.TTL_FOR_CONF_CACHE_DAYS, value = "1", label = "TTL in days")
})
public class FrontEndConfigurationServiceImpl implements FrontEndConfigurationService {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(FrontEndConfigurationServiceImpl.class);
 
 /** The config cache. */
 private LoadingCache<String, Map<String, String>> configCache;
 
 /** The resolver factory. */
 // Inject a Sling ResourceResolverFactory
 @Reference
 private ResourceResolverFactory resolverFactory;

 /** The Constant DEFAULT_MAX_NUMBER_IN_CACHE. */
 private static final int DEFAULT_MAX_NUMBER_IN_CACHE = 100;
 
 /** The Constant DEFAULT_TTL_FOR_CONF_CACHE. */
 private static final int DEFAULT_TTL_FOR_CONF_CACHE = 1;
 
 /**
  * Activate.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 public void activate(ComponentContext ctx) {
  LOGGER.info("Inside FrontEndConfigurationServiceImpl activate " + ctx);
  int maxRecord = ctx.getProperties().get(ConfigurationConstants.MAX_RECORD_IN_CACHE) != null
    ? Integer.parseInt(ctx.getProperties().get(ConfigurationConstants.MAX_RECORD_IN_CACHE).toString()) : DEFAULT_MAX_NUMBER_IN_CACHE;
  int ttlInMin = ctx.getProperties().get(ConfigurationConstants.TTL_FOR_CONF_CACHE) != null
    ? Integer.parseInt(ctx.getProperties().get(ConfigurationConstants.TTL_FOR_CONF_CACHE).toString()) : DEFAULT_TTL_FOR_CONF_CACHE;
    configCache = CacheBuilder.newBuilder().maximumSize(maxRecord).expireAfterAccess(ttlInMin, TimeUnit.DAYS)
            .build(new CacheLoader<String, Map<String, String>>() {
             @Override
             public Map<String, String> load(String cacheName) throws Exception {
              LOGGER.info("Inside Guava CacheBuilder load method");
              return new HashMap<String,String>();
             }
            });
    startConfigurationCaching();
 }
 
 /**
  * Start configuration caching.
  *
  * @param resolverFactory
  *         the resolver factory
  * @return the map
  */
 private void startConfigurationCaching() {
     LOGGER.info("Inside startConfigurationCaching of ConfigurationServiceImpl");
     ResourceResolver resourceResolver = null;
     
     try {
      resourceResolver = ConfigurationHelper.getResourceResolver(resolverFactory, "configurationService");
      CacheUtil.updateFrontEndConfigCache(resourceResolver,this.configCache);
      
     } catch (LoginException e) {
      LOGGER.error("Error getting ResourceResolver in ConfigurationServiceImpl ", e);
     } finally {
      if (resourceResolver != null && resourceResolver.isLive()) {
       resourceResolver.close();
      }
     }
 }
 

/**
  * Gets the config cache.
  *
  * @return the config cache
  */
 public LoadingCache<String, Map<String, String>> getConfigCache() {
  return this.configCache;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.ConfigurationService#inValidateCache(java.lang.String)
  */
 @Override
 public void inValidateCache() {
  LOGGER.debug("configCache before invalidating is, " + this.configCache);
  this.configCache.invalidateAll();
  LOGGER.debug("configCache after invalidating is, " + this.configCache);
 }

@Override
public void inValidateCache(String key) {
   this.configCache.invalidate(key);
    
}
}
