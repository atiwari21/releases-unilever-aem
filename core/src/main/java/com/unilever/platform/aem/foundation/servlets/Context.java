/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * This is use to hold 'context' claim data available in JWT token.
 *
 * @author Saba Parveen
 * 
 */
public class Context {
 
 private Site site;
 
 /**
  * @return {@code Site}
  */
 public Site getSite() {
  return site;
 }
 
 /**
  * @param site
  * 
  */
 public void setSite(Site site) {
  this.site = site;
 }
 
 /**
  * Method to parse json string into Context class.
  * 
  * @param str
  * @return
  * @throws JsonParseException
  * @throws JsonMappingException
  * @throws IOException
  */
 public static Context parse(String str) throws IOException {
  Context obj = null;
  
  obj = new ObjectMapper().readValue(str, Context.class);
  
  return obj;
 }
 
 @Override
 public String toString() {
  StringBuilder br = new StringBuilder();
  br.append("{site :");
  br.append(site.toString());
  br.append("}");
  return br.toString();
 }
}
