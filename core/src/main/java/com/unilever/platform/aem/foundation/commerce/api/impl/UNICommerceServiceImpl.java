/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceConstants;
import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.AbstractJcrCommerceService;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.adobe.cq.commerce.common.ServiceContext;
import com.day.cq.commons.ImageResource;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMException;
import com.unilever.platform.aem.foundation.commerce.api.UNICommerceService;
import com.unilever.platform.aem.foundation.commerce.api.UNICommerceSession;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;

/**
 * The Class UNICommerceServiceImpl to implement commerce service.
 */
public class UNICommerceServiceImpl extends AbstractJcrCommerceService implements UNICommerceService {
 
 /** The Constant LOG. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UNICommerceServiceImpl.class);
 
 /**
  * The resource.
  */
 private final Resource resource;
 
 /**
  * The resolver.
  */
 private final ResourceResolver resolver;
 
 /**
  * Constant String Literal customBodyClass.
  */
 public static final String CUSTOM_BODY_CLASS = "customBodyClass";
 /**
  * Constant String Literal fileReference.
  */
 public static final String FILE_REFERENCE = "fileReference";
 
 /**
  * Instantiates a new UNI commerce service impl.
  *
  * @param serviceContext
  *         the service context
  * @param resource
  *         the res
  */
 public UNICommerceServiceImpl(ServiceContext serviceContext, Resource resource) {
  super(serviceContext);
  this.resource = resource;
  this.resolver = resource.getResourceResolver();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#login(org.apache.sling.api. SlingHttpServletRequest, org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 public UNICommerceSession login(SlingHttpServletRequest request, SlingHttpServletResponse response) throws CommerceException {
  return new UNICommerceSessionImpl(this, request, response, this.resource);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#isAvailable(java.lang.String)
  */
 @Override
 public boolean isAvailable(String serviceType) {
  return "commerce-service".equals(serviceType);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getProduct(java.lang.String)
  */
 @Override
 public Product getProduct(final String productPath) {
  Product product = null;
  StringBuilder path = new StringBuilder();
  
  path.append(productPath);
  LOGGER.debug("product path is : " + productPath);
  
  Resource productResouce = this.resolver.getResource(path.toString());
  if (productResouce != null && UNIProductImpl.isAProductOrVariant(productResouce)) {
   return new UNIProductImpl(productResouce);
  }
  return product;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getCountries()
  */
 @Override
 public List<String> getCountries() throws CommerceException {
  List<String> countries = new ArrayList<String>();
  countries.add("*");
  return countries;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getCreditCardTypes()
  */
 @Override
 public List<String> getCreditCardTypes() throws CommerceException {
  List<String> ccTypes = new ArrayList<String>();
  ccTypes.add("*");
  return ccTypes;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getOrderPredicates()
  */
 @Override
 public List<String> getOrderPredicates() throws CommerceException {
  List<String> predicates = new ArrayList<String>();
  predicates.add(CommerceConstants.OPEN_ORDERS_PREDICATE);
  return predicates;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.common.AbstractJcrCommerceService# productRolloutHook(com.adobe.cq.commerce.api.Product, com.day.cq.wcm.api.Page,
  * com.adobe.cq.commerce.api.Product)
  */
 @Override
 public void productRolloutHook(Product productData, Page productPage, Product product) throws CommerceException {
  try {
   
   // Copy product tags to pages
   copyProductTagsToPDP(productData, productPage);
   
   // removing additional image nodes
   removeProperty(CommerceHelper.findProductResource(productPage), "image");
   
   // adding metadata to the product page
   addMetaData(productPage, productData);
   
   // replacing multiple hyphens and underscore in URL with hyphen
   String newproductname = productPage.getName().replaceAll("_", "-");
   newproductname = newproductname.replaceAll("--+", "-").toLowerCase();
   if (!newproductname.equalsIgnoreCase(productPage.getName())) {
    rename(productPage.adaptTo(Node.class), newproductname);
   }
   
   String productName = productData.getTitle();
   if (StringUtils.isNotBlank(productName)) {
    productName = JcrUtil.createValidName(productName.trim().replace(" ", "-")).toLowerCase();
    productName = productName.replaceAll(" ", "-");
    productName = productName.replaceAll(" +", "-");
    productName = productName.replaceAll("_", "-");
    productName = productName.replaceAll("--+", "-").toLowerCase();
    
    if (!productName.equalsIgnoreCase(newproductname)) {
     rename(productPage.adaptTo(Node.class), productName);
    }
    
   }
   
  } catch (Exception e) {
   throw new CommerceException("Product rollout hook failed: ", e);
  }
 }
 
 /**
  * Copy product tags to pdp.
  *
  * @param productData
  *         the product data
  * @param productPage
  *         the product page
  * @throws RepositoryException
  *          the repository exception
  * @throws WCMException
  *          the WCM exception
  */
 private void copyProductTagsToPDP(Product productData, Page productPage) throws RepositoryException, WCMException {
  boolean changed = false;
  Node pageContentNode = productPage.getContentResource().adaptTo(Node.class);
  UNIProduct prod = (UNIProduct) productData;
  if (prod != null) {
   Tag[] tags = prod.getTags(this.resolver);
   String[] tagsArray = getProductTagsArray(tags);
   
   if (tagsArray != null && !ArrayUtils.isEmpty(tagsArray)) {
    pageContentNode.setProperty(TagConstants.PN_TAGS, tagsArray);
    changed = true;
   }
  }
  
  if (changed) {
   productPage.getPageManager().touch(productPage.adaptTo(Node.class), true, Calendar.getInstance(), false);
  }
 }
 
 /**
  * Gets the product tags array.
  *
  * @param tags
  *         the tags
  * @return the product tags array
  */
 private String[] getProductTagsArray(Tag[] tags) {
  String[] tagsArray = null;
  if (tags != null && tags.length > 0) {
   tagsArray = new String[tags.length];
   int i = 0;
   for (Tag t : tags) {
    tagsArray[i] = t.getTagID();
    i++;
   }
  }
  
  return tagsArray;
 }
 
 /**
  * Gets the theme tags.
  *
  * @param productPage
  *         the product page
  * @param existingPropValue
  *         the existing prop value
  * @return the theme tags
  */
 private String getThemeTags(Page productPage, String existingPropValue) {
  Set<String> set = new LinkedHashSet<String>();
  StringBuilder sb = new StringBuilder();
  
  if (StringUtils.isNotBlank(existingPropValue)) {
   String[] existingArray = existingPropValue.trim().split("\\s+");
   for (String existingProp : existingArray) {
    if (StringUtils.isNotBlank(existingProp)) {
     set.add(existingProp);
    }
   }
  }
  
  String themeTagsConfig = "";
  try {
   themeTagsConfig = this.resolver.adaptTo(ConfigurationService.class).getConfigValue(productPage, "pdpThemeTags", "tags");
   String[] themeTagsArray = themeTagsConfig.split(",");
   
   Tag[] pageTags = productPage.getTags();
   if (themeTagsArray != null) {
    for (String parentTags : themeTagsArray) {
     parentTags = parentTags.trim();
     if (StringUtils.isNotBlank(parentTags)) {
      for (Tag tag : pageTags) {
       String tagId = tag.getTagID();
       if (tagId.equals(parentTags)) {
        String childTag = tagId.substring(tagId.lastIndexOf("/") + 1);
        set.add(childTag);
       } else if (tagId.startsWith(parentTags)) {
        String[] childTagArray = tagId.substring(parentTags.lastIndexOf("/") + 1).split("/");
        for (String childTags : childTagArray) {
         set.add(childTags);
        }
       }
      }
     }
    }
   }
   
   for (String resultString : set) {
    sb.append(resultString + " ");
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Not found for Category: " + "pdpThemeTags" + " and key: " + "tags", e);
  }
  
  return sb.toString().trim();
 }
 
 /**
  * Rename.
  *
  * @param node
  *         the node
  * @param newName
  *         the new name
  * @throws RepositoryException
  *          the repository exception
  */
 private void rename(Node node, String newName) throws RepositoryException {
  if (!node.getParent().hasNode(newName)) {
   node.getSession().move(node.getPath(), node.getParent().getPath() + "/" + newName);
  }
 }
 
 /**
  * Removes the property.
  *
  * @param resource
  *         the resource
  * @param property
  *         the property
  */
 private void removeProperty(Resource resource, String property) {
  Node pageImageNode = null;
  try {
   Node productNode = resource.adaptTo(Node.class);
   pageImageNode = JcrUtils.getOrAddNode(productNode, property, JcrConstants.NT_UNSTRUCTURED);
   if (pageImageNode != null) {
    pageImageNode.remove();
   }
   for (Iterator iter = resource.listChildren(); iter.hasNext();) {
    Resource product = (Resource) iter.next();
    if (product != null) {
     removeProperty(product, property);
    }
   }
   
  } catch (RepositoryException e) {
   LOGGER.error("Error while deleting image node.", e);
  }
 }
 
 /**
  * Adds the alias name.
  *
  * @param productName
  *         the product name
  * @param node
  *         the node
  * @param page
  *         the page
  * @throws RepositoryException
  *          the repository exception
  */
 private void addAliasName(String productName, Node node, Page page) throws RepositoryException {
  ConfigurationService configService = this.resolver.adaptTo(ConfigurationService.class);
  Map<String, String> aliasPageNameConfigmapping = null;
  boolean enabled = false;
  boolean reset = true;
  if (configService != null) {
   aliasPageNameConfigmapping = getPageNameConfig(configService, page);
   if (aliasPageNameConfigmapping.containsKey("enabled")) {
    enabled = Boolean.parseBoolean(aliasPageNameConfigmapping.get("enabled"));
   }
   if (aliasPageNameConfigmapping.containsKey("reset")) {
    reset = Boolean.parseBoolean(aliasPageNameConfigmapping.get("reset"));
   }
  }
  
  if (reset) {
   node.setProperty("sling:alias", (Value) null);
  }
  
  if (enabled) {
   // adding Alias Name for the page
   String newproductname = productName.replaceAll("_", "-");
   newproductname = newproductname.replaceAll(" ", "-");
   newproductname = newproductname.replaceAll(" +", "-");
   newproductname = newproductname.replaceAll("--+", "-").toLowerCase();
   node.setProperty("sling:alias", newproductname);
  }
 }
 
 /**
  * Adds the meta data.
  *
  * @param page
  *         the page
  * @param product
  *         the product
  */
 private void addMetaData(Page page, Product product) {
  
  UNIProduct prod = (UNIProduct) product;
  if ((prod != null) && (page != null)) {
   try {
    Node node = page.getContentResource().adaptTo(Node.class);
    String desc = prod.getDescription();
    String pageTite = prod.getPageTitle();
    String productName = prod.getName();
    
    if (desc != null) {
     node.setProperty(JcrConstants.JCR_DESCRIPTION, desc);
    }
    if (pageTite != null) {
     node.setProperty(NameConstants.PN_PAGE_TITLE, pageTite);
    }
    
    if (StringUtils.isNotBlank(productName)) {
     node.setProperty(JcrConstants.JCR_TITLE, productName);
     // Add alias name
     addAliasName(productName, node, page);
    }
    
    addLandingAttributes(page, node, prod);
    addCustomBodyAttribute(page, node);
    
    Node thumbImageNode = JcrUtils.getOrAddNode(node, "image", JcrConstants.NT_UNSTRUCTURED);
    if ((thumbImageNode != null) && (!prod.getImages().isEmpty())) {
     thumbImageNode.setProperty(FILE_REFERENCE, prod.getImages().get(0).getFileReference());
    }
    
    updateProductDataMapping(page, node);
    
   } catch (RepositoryException e) {
    LOGGER.error("Error while adding description in rollout.", e);
   }
  }
 }
 
 /**
  * Update product data mapping.
  *
  * @param page
  *         the page
  * @param node
  *         the node
  * @throws RepositoryException
  *          the repository exception
  */
 private void updateProductDataMapping(Page page, Node node) throws RepositoryException {
  String productPath = node.getProperty("cq:productMaster").getValue().getString();
  Node productResource = CommerceHelper.findProductResource(page).adaptTo(Node.class);
  if ((productResource != null) && (productPath != null) && StringUtils.isNotEmpty(productPath)) {
   String productData = productResource.getProperty("productData").getValue().getString();
   if (!productPath.equals(productData)) {
    productResource.setProperty("productData", productPath);
   }
  }
 }
 
 /**
  * Adds the custom body attribute.
  *
  * @param page
  *         the page
  * @param node
  *         the node
  * @throws RepositoryException
  *          the repository exception
  */
 private void addCustomBodyAttribute(Page page, Node node) throws RepositoryException {
  String customBodyClass = StringUtils.EMPTY;
  if (node.hasProperty(CUSTOM_BODY_CLASS)) {
   customBodyClass = node.getProperty(CUSTOM_BODY_CLASS).getValue().getString();
  }
  String themeTags = getThemeTags(page, customBodyClass);
  if (StringUtils.isNotEmpty(themeTags)) {
   node.setProperty(CUSTOM_BODY_CLASS, themeTags);
  } else {
   node.setProperty(CUSTOM_BODY_CLASS, (Value) null);
  }
 }
 
 /**
  * Gets the page name config.
  *
  * @param configService
  *         the config service
  * @param page
  *         the page
  * @return the page name config
  */
 private Map<String, String> getPageNameConfig(ConfigurationService configService, Page page) {
  Map<String, String> aliasPageNameConfigmapping = null;
  try {
   aliasPageNameConfigmapping = configService.getCategoryConfiguration(page, "aliasPageNameConfig");
  } catch (ConfigurationNotFoundException e1) {
   LOGGER.error("Error while getting aliasPageName configuration.", e1);
  }
  return aliasPageNameConfigmapping;
 }
 
 /**
  * Adds the teaser image attributes.
  *
  * @param prod
  *         the prod
  * @param contentNode
  *         the content node
  * @throws RepositoryException
  *          the repository exception
  */
 private void addTeaserImageAttributes(UNIProduct prod, Node contentNode) throws RepositoryException {
  List<ImageResource> images = prod.getImages();
  String teaserImage = StringUtils.EMPTY;
  String teaserImageAltText = StringUtils.EMPTY;
  if (!images.isEmpty()) {
   ImageResource image = images.get(0);
   Node imageNode = image.adaptTo(Node.class);
   teaserImage = imageNode.hasProperty(FILE_REFERENCE) ? imageNode.getProperty(FILE_REFERENCE).getValue().toString() : StringUtils.EMPTY;
   teaserImageAltText = imageNode.hasProperty("altText") ? imageNode.getProperty("altText").getValue().toString() : StringUtils.EMPTY;
  }
  
  contentNode.setProperty("teaserImage", teaserImage);
  contentNode.setProperty("teaserAltText", teaserImageAltText);
 }
 
 /**
  * Adds the landing attributes.
  *
  * @param productPage
  *         the product page
  * @param contentNode
  *         the content node
  * @param prod
  *         the prod
  */
 private void addLandingAttributes(Page productPage, Node contentNode, UNIProduct prod) {
  
  ConfigurationService configService = this.resolver.adaptTo(ConfigurationService.class);
  Map<String, String> mapping = GlobalConfigurationUtility.getMapFromConfiguration(configService, productPage, "landingAttributesMapping");
  try {
   addTeaserAttributes(contentNode, prod, mapping);
   addTeaserImageAttributes(prod, contentNode);
  } catch (RepositoryException e) {
   LOGGER.error("Error while adding teaser property", e);
  }
 }
 
 /**
  * Adds the teaser attributes.
  *
  * @param contentNode
  *         the content node
  * @param prod
  *         the prod
  * @param mapping
  *         the mapping
  * @throws RepositoryException
  *          the repository exception
  */
 private void addTeaserAttributes(Node contentNode, UNIProduct prod, Map<String, String> mapping) throws RepositoryException {
  if (mapping != null) {
   for (String key : mapping.keySet()) {
    String value = StringUtils.EMPTY;
    String mappedProperty = mapping.get(key);
    if (StringUtils.isNotBlank(mappedProperty)) {
     value = prod.getProperty(mappedProperty.trim(), String.class);
    }
    
    if (value != null) {
     contentNode.setProperty(key, value);
    } else {
     contentNode.setProperty(key, StringUtils.EMPTY);
    }
   }
  } else {
   contentNode.setProperty("teaserTitle", StringUtils.EMPTY);
   contentNode.setProperty("teaserCopy", StringUtils.EMPTY);
   contentNode.setProperty("teaserLongCopy", StringUtils.EMPTY);
  }
 }
}
