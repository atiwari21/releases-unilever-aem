/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class BrandMarketBean.
 */
public class BrandMarketBean {
 
 /** The brand. */
 private String brand;
 
 /** The market. */
 private String market;
 
 /** The Constant TWO */
 private static final int NUMERIC_TWO = 2;
 /** The Constant THREE */
 private static final int NUMERIC_THREE = 3;
 
 /**
  * Instantiates a new brand market bean.
  *
  * @param page
  *         the page
  */
 public BrandMarketBean(Page page) {
  String[] pagePathArr = page.getPath().split(CommonConstants.SLASH_CHAR_LITERAL);
  this.brand = pagePathArr[NUMERIC_TWO];
  this.market = pagePathArr.length > NUMERIC_THREE ? pagePathArr[NUMERIC_THREE] : "global";
  
 }
 
 /**
  * Instantiates a new brand market bean.
  *
  * @param brandName
  *         the brand name
  * @param market
  *         the market
  */
 public BrandMarketBean(String brandName, String market) {
  this.brand = brandName;
  this.market = market;
 }
 
 /**
  * Gets the brand.
  *
  * @return the brand
  */
 public String getBrand() {
  return brand;
 }
 
 /**
  * Sets the brand.
  *
  * @param brand
  *         the new brand
  */
 public void setBrand(String brand) {
  this.brand = brand;
 }
 
 /**
  * Gets the market.
  *
  * @return the market
  */
 public String getMarket() {
  return market;
 }
 
 /**
  * Sets the market.
  *
  * @param market
  *         the new market
  */
 public void setMarket(String market) {
  this.market = market;
 }
 
}
