/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.commerce;

import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;

/**
 * The Class UNICommerceConstants.
 *
 * @author atiw22
 */
public class UNICommerceConstants {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(UNICommerceConstants.class);
 
 /** The Constant PRODUCTSROOT. */
 public static final String PRODUCTSROOT = "/etc/commerce/products/";
 
 /** The Constant PROVIDER. */
 public static final String PROVIDER = "provider";
 
 /** The Constant PRODUCT. */
 public static final String PRODUCT = "product";
 
 /** The Constant CSVPATH. */
 public static final String CSVPATH = "csvPath";
 
 /** The Constant STOREPATH. */
 public static final String STOREPATH = "storePath";
 
 /** The Constant STORENAME. */
 public static final String STORENAME = "storeName";
 
 /** The Constant PREFIX. */
 public static final String PREFIX = "stream2file";
 
 /** The Constant SUFFIX. */
 public static final String SUFFIX = ".tmp";
 
 /** The Constant OPERATION. */
 public static final String OPERATION = "action";
 
 /** The Constant SKU. */
 public static final String SKU = "sku";
 
 /** The Constant TYPE. */
 public static final String TYPE = "type";
 
 /** The Constant SUBTYPE. */
 public static final String SUBTYPE = "subcategory";
 
 /** The Constant TITLE. */
 public static final String TITLE = "title";
 
 /** The Constant GTIN14. */
 public static final String GTIN14 = "gtin14";
 
 /** The Constant NAME. */
 public static final String NAME = "name";
 
 /** The Constant PRODUCT_ID. */
 public static final String PRODUCT_ID = "productID";
 
 /** The Constant CATEGORY. */
 public static final String CATEGORY = "category";
 
 /** The Constant PRODUCTION_DATE. */
 public static final String PRODUCTION_DATE = "productionDate";
 
 /** The Constant RELEASE_DATE. */
 public static final String RELEASE_DATE = "releaseDate";
 
 /** The Constant DESCRIPTION. */
 public static final String DESCRIPTION = "description";
 
 /** The Constant IMAGES. */
 public static final String IMAGES = "imageIds";
 
 /** The Constant IMAGES_ALT_TEXT. */
 public static final String IMAGES_ALT_TEXT = "altTextImages";
 
 /** The Constant SIZES. */
 public static final String SIZES = "size";
 
 /** The Constant SIZES. */
 public static final String ASSETS = "assets";
 
 /** The Constant RESOURCE_TYPE_PATH. */
 public static final String RESOURCE_TYPE_PATH = "/apps/unilever-iea/components/product";
 
 /** The Constant RESOURCE_TYPE_PATH. */
 public static final String SLING_RESOURCE = JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY;
 
 /** The Constant DISCLAIMER_COL_NAME. */
 public static final String DISCLAIMER_COL_NAME = "disclaimer";
 
 /** The Constant SIZE_COL_NAME. */
 public static final String SIZE_COL_NAME = "sizes";
 
 /** The Constant PRICE_COL_NAME. */
 public static final String PRICE_COL_NAME = "price";
 
 /** The Constant INGREDIENTS_COL_NAME. */
 public static final String INGREDIENTS_COL_NAME = "ingredients";
 
 /** The Constant TAGS_COL_NAME. */
 public static final String TAGS_COL_NAME = "tags";
 
 /** The Constant FIRST_CUSTOM_PROP_COL_NAME. */
 public static final String FIRST_CUSTOM_PROP_COL_NAME = "additional_property";
 
 /** The Constant ADD_OPERATION. */
 public static final String ADD_OPERATION = "new";
 
 /** The Constant DELETE_OPERATION. */
 public static final String DELETE_OPERATION = "delete";
 
 /** The Constant UPDATE_OPERATION. */
 public static final String UPDATE_OPERATION = "update";
 
 /** The Constant PRODUCT_TYPE. */
 public static final String PRODUCT_TYPE = "product";
 
 /** The Constant VARIATION_TYPE. */
 public static final String VARIATION_TYPE = "variation";
 
 /** The Constant SIZE_TYPE. */
 public static final String SIZE_TYPE = "size";
 
 /** The Constant DTO_NAMESPACE. */
 public static final String DTO_NAMESPACE = "com.unilever.platform.foundation.commerce.dto.";
 
 /** The Constant ADDITIONAL_METHOD_NAME. */
 public static final String ADDITIONAL_METHOD_NAME = "setAdditionalProperty";
 
 /** The Constant VARIANT. */
 public static final String VARIANT = "variant";
 
 /** The Constant THOUNSAND. */
 public static final int THOUNSAND = 1000;
 
 /** The Constant SIXTY. */
 public static final int SIXTY = 60;
 
 /** The Constant HUNDRED_TWENTY. */
 public static final int HUNDRED_TWENTY = 120;
 
 /** The Constant PRODUCT_VARIANT_AXES. */
 public static final String PRODUCT_VARIANT_AXES = "cq:productVariantAxes";
 
 /** The Constant NI_UNSTRUCTURED. */
 public static final String NI_UNSTRUCTURED = JcrConstants.NT_UNSTRUCTURED;
 
 /** The Constant UNIQUE_ID. */
 public static final String UNIQUE_ID = "uniqueID";
 
 /**
  * Instantiates a new UNI commerce constants.
  */
 private UNICommerceConstants() {
  LOG.debug("Calling private constructor for UTILITY class");
 }
}
