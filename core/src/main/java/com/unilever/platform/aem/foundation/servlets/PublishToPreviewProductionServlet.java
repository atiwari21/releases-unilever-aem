/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.integration.RatingHelper;

/**
 * This class sets the lock and unlock to product catalogs based on catalog selection handled for single and multiple selection catalogs.
 */
@SlingServlet(paths = { "/bin/publish/preview/production" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.PublishToPreviewProductionServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the preview agent to be enabled ", propertyPrivate = false) })
public class PublishToPreviewProductionServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -78270800297708351L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PublishToPreviewProductionServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  Resource previewResource = (resourceResolver != null) ? resourceResolver.getResource("/etc/replication/agents.author/Publish-0101/jcr:content")
    : null;
  setPreviewAgentEnabled(resourceResolver, previewResource, true);
  Resource productionResource = (resourceResolver != null) ? resourceResolver.getResource("/etc/replication/agents.author/Publish-0202/jcr:content")
    : null;
  setPreviewAgentEnabled(resourceResolver, productionResource, true);
  if (resourceResolver != null) {
   resourceResolver.close();
  }
 }
 
 private void setPreviewAgentEnabled(ResourceResolver resourceResolver, Resource resource, boolean isEnabled) {
  Node previewResourceNode = (resource != null) ? resource.adaptTo(Node.class) : null;
  if (previewResourceNode != null) {
   try {
    previewResourceNode.setProperty("enabled", isEnabled);
    resourceResolver.commit();
   } catch (RepositoryException e) {
    LOGGER.error("RepositoryException:", e);
   } catch (PersistenceException e) {
    LOGGER.error("PersistenceException:", e);
   }
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
}
