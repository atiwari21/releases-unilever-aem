/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class IgnoreRule.
 */
public class IgnoreRule extends ConfigurationSelectorRule {
 
 /**
  * Instantiates a new ignore rule.
  */
 private IgnoreRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the ignore rule
  */
 public static IgnoreRule create() {
  return new IgnoreRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the rule
  */
 public static IgnoreRule createFrom(JSONObject configuration) {
  return (IgnoreRule) create().withSelector(configuration.getString("selector"));
 }
 
 /**
  * Apply.
  *
  * @param transformer
  *         the transformer
  * @param container
  *         the container
  * @param node
  *         the node
  * @return the container
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  return container;
 }
 
 /**
  * Gets the context class.
  *
  * @return the context class
  */
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticleElement.getClassName() };
 }
 
}
