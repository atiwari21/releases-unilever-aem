/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;

/**
 * The Class UNIProductHelper.
 */
public class UNIProductHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UNIProductHelper.class);
 
 /**
  * Instantiates a new UNI product helper.
  */
 private UNIProductHelper() {
  
 }
 
 /**
  * Gets the node from path.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param path
  *         the path
  * @param nodeName
  *         the node name
  * @return the node from path
  */
 public static Node getNodeFromPath(ResourceResolver resourceResolver, String path, String nodeName) {
  LOGGER.debug("Inside getProductNodePath of ProductHelperExt");
  Node resultNode = null;
  Session session = null;
  try {
   session = resourceResolver != null ? resourceResolver.adaptTo(Session.class) : null;
   // Set the query & Obtain the query manager for the session ...
   QueryManager queryManager = session != null ? session.getWorkspace().getQueryManager() : null;
   // Setup the query to get all tags of a parent tag at provided path
   String sqlStatement = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([" + path + "]) and NAME() = '" + nodeName + "'";
   Query query = queryManager.createQuery(sqlStatement, "JCR-SQL2");
   // Execute the query and get the results
   
   QueryResult results = query.execute();
   // Iterate over the nodes in the results
   NodeIterator itr = results != null ? results.getNodes() : null;
   if (null != itr && itr.getSize() == 1) {
    resultNode = itr.nextNode();
   }
   
  } catch (Exception e) {
   LOGGER.error("Error in getProductNodePath of ProductHelperExt in finding path of {} ", nodeName, e);
  }
  return resultNode;
 }
 
 /**
  * Find product.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param productId
  *         the product id
  * @param productPath
  *         the product path
  * @return the UNI product
  */
 public static UNIProduct findProduct(ResourceResolver resourceResolver, String productId, String productPath) {
  LOGGER.debug("Inside findProduct of ProductHelperExt");
  UNIProduct currentProduct = null;
  try {
   
   if (!StringUtils.isBlank(productPath)) {
    
    Node productNode = getNodeFromPath(resourceResolver, productPath, productId);
    LOGGER.debug("productNode in findProduct: {}", productNode.toString());
    String productFinalPath = productNode != null ? productNode.getPath() : null;
    LOGGER.debug("productFinalPath in findProduct: {}", productFinalPath);
    currentProduct = getProductByPath(resourceResolver, productFinalPath);
   }
   
  } catch (Exception e) {
   LOGGER.error("Error in findProduct for {} ", productId, e);
  }
  return currentProduct;
 }
 
 /**
  * Gets the product by path.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param productFinalPath
  *         the product final path
  * @return the product by path
  */
 public static UNIProduct getProductByPath(ResourceResolver resourceResolver, String productFinalPath) {
  Resource productResource = resourceResolver != null ? resourceResolver.getResource(productFinalPath) : null;
  Product product = productResource != null ? productResource.adaptTo(Product.class) : null;
  LOGGER.debug("product in getProductByPath : {}", product);
  UNIProduct currentProduct = null;
  if (null != product && product instanceof UNIProduct) {
   currentProduct = (UNIProduct) product;
  }
  return currentProduct;
 }
 
}
