/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Document;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * Class List that represents a simple HTML list
 * 
 * Example Unordered:
 * <ul>
 *     <li>Dog</li>
 *     <li>Cat</li>
 *     <li>Fox</li>
 * </ul>
 * 
 * Example Ordered:
 * <ol>
 *     <li>Groceries</li>
 *     <li>School</li>
 *     <li>Sleep</li>
 * </ol>.
 */
/**
 * The Class ListElement.
 */
public class ListElement extends InstantArticleElement implements Container {
 
 /** The list item list. */
 private List<ListItem> listItemList = new ArrayList<ListItem>();
 
 /**
  * Factory method for an Ordered list.
  *
  * @return ListElement the new instance List as an ordered list
  */
 public static ListElement createOrdered() {
  
  return ListElement.create().enableOrdered();
 }
 
 /**
  * Factory method for an Ordered list.
  *
  * @return ListElement the new instance List as an ordered list
  */
 public static ListElement createUnOrdered() {
  
  return ListElement.create().disableOrdered();
 }
 
 /**
  * Factory method for an Ordered list.
  *
  * @return ListElement the new instance List as an ordered list
  */
 public ListElement enableOrdered() {
  this.isOrdered = true;
  return this;
 }
 
 /**
  * Makes the list become unordered.
  *
  * @return $this
  */
 public ListElement disableOrdered() {
  this.isOrdered = false;
  return this;
 }
 
 /**
  * Adds the item.
  *
  * @param listItem
  *         the list item
  * @return the list element
  */
 public ListElement addItem(String listItem) {
  if (StringUtils.isNotBlank(listItem)) {
   ListItem item = (ListItem) ListItem.create().appendText(listItem);
   this.listItemList.add(item);
  }
  
  return this;
 }
 
 /**
  * Adds the item.
  *
  * @param listItem
  *         the list item
  * @return the list element
  */
 public ListElement addItem(ListItem listItem) {
  
  this.listItemList.add(listItem);
  
  return this;
 }
 
 /**
  * With items.
  *
  * @param listItems
  *         the list items
  * @return the list element
  */
 public ListElement withItems(List<ListItem> listItems) {
  this.listItemList.addAll(listItems);
  return this;
 }
 
 /**
  * With items.
  *
  * @param listItems
  *         the list items
  * @return the list element
  */
 public ListElement withItems(String[] listItems) {
  for (String item : listItems) {
   addItem(item);
  }
  return this;
 }
 
 /**
  * Gets the items.
  *
  * @return the items
  */
 public List<ListItem> getItems() {
  return this.listItemList;
 }
 
 /**
  * Checks if is ordered.
  *
  * @return true, if is ordered
  */
 public boolean isOrdered() {
  return this.isOrdered;
 }
 
 /** The cover. */
 private boolean isOrdered;
 
 /**
  * Creates the.
  *
  * @return the list element
  */
 public static ListElement create() {
  return new ListElement();
 }
 
 /**
  * Instantiates a new list element.
  */
 private ListElement() {
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  List<Object> children = new ArrayList<Object>();
  if (!this.listItemList.isEmpty()) {
   children.addAll(listItemList);
  }
  return children;
 }
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement()
  */
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement(org.jsoup.nodes.Document)
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  org.jsoup.nodes.Element element;
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  
  if (this.isOrdered) {
   element = document.createElement("ol");
  } else {
   element = document.createElement("ul");
  }
  if (!this.listItemList.isEmpty()) {
   for (ListItem item : this.listItemList) {
    if (item.toDOMElement() != null) {
     element.append(item.toDOMElement().outerHtml());
    }
   }
  }
  return element;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#isValid()
  */
 @Override
 public boolean isValid() {
  boolean flag = false;
  if (!this.listItemList.isEmpty()) {
   for (ListItem item : this.listItemList) {
    if (item.isValid()) {
     flag = true;
     break;
    }
   }
  }
  return flag;
 }
}
