/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.jcr.resource.JcrPropertyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;

/**
 * The Class UnileverSitemap.
 */
public class UnileverSitemap {
 
 /** The Constant SECTIONS. */
 public static final String NO_INDEX = "noindex";
 
 /** The links. */
 private List<Link> links = new<Link> LinkedList();
 
 /**
  * Identifier to hold value of logger.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(UnileverSitemap.class);
 
 /**
  * Instantiates a new unilever sitemap.
  *
  * @param rootPage
  *         the root page
  */
 public UnileverSitemap(Page rootPage) {
  buildLinkAndChildren(rootPage, 0);
 }
 
 /**
  * This method build the links from the page underneth the govn page.
  *
  * It ignores the pages with property indexingEnabled = false
  *
  * @param page
  *         the page
  * @param level
  *         the level
  */
 protected void buildLinkAndChildren(Page page, int level) {
  if (page != null) {
   String title = page.getTitle();
   if (title == null) {
    title = page.getName();
   }
   if (!isNoIndex(page)) {
    LOGGER.debug("Index this page :" + page.getPath());
    this.links.add(new Link(page.getPath(), title, level));
   }
   
   Iterator children = page.listChildren();
   
   while (children.hasNext()) {
    Page child = (Page) children.next();
    buildLinkAndChildren(child, level + 1);
   }
   
  }
 }
 
 /**
  * outputs the links as HTML.
  *
  * @param w
  *         the w
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 public void draw(Writer w) throws IOException {
  PrintWriter out = new PrintWriter(w);
  
  int previousLevel = -1;
  
  for (Link aLink : this.links) {
   if (aLink.getLevel() > previousLevel) {
    out.print("<div class=\"linkcontainer\">");
   } else if (aLink.getLevel() < previousLevel) {
    for (int i = aLink.getLevel(); i < previousLevel; ++i) {
     out.print("</div>");
    }
   }
   out.printf("<div class=\"link\"><a href=\"%s.html\">%s</a></div>",
     new Object[] { StringEscapeUtils.escapeHtml4(aLink.getPath()), StringEscapeUtils.escapeHtml4(aLink.getTitle()) });
   
   previousLevel = aLink.getLevel();
  }
  
  for (int i = -1; i < previousLevel; ++i) {
   out.print("</div>");
  }
 }
 
 /**
  * whether indexing is required in sitemap.
  *
  * @param page
  *         Page object.
  * @return true/false.
  */
 protected boolean isNoIndex(Page page) {
  boolean noIndex = false;
  
  Resource resource = page.getContentResource();
  
  if (null != resource) {
   
   Node currentNode = resource.adaptTo(Node.class);
   try {
    if (null != currentNode && currentNode.hasProperty(NO_INDEX)) {
     noIndex = Boolean.valueOf(new JcrPropertyMap(currentNode).get(NO_INDEX, StringUtils.EMPTY));
     
    }
   } catch (RepositoryException e) {
    LOGGER.error("could not find property isIndexingEnabled for page :" + page.getPath(), e);
   }
  }
  return noIndex;
 }
 
 /**
  * Gets the links.
  *
  * @return the links
  */
 public List<Link> getLinks() {
  return this.links;
 }
 
 /**
  * The Class Link.
  */
 public class Link {
  
  /** The path. */
  private String path;
  
  /** The title. */
  private String title;
  
  /** The level. */
  private int level;
  
  /**
   * Instantiates a new link.
   *
   * @param paramString1
   *         the param string1
   * @param paramString2
   *         the param string2
   * @param paramInt
   *         the param int
   */
  public Link(String paramString1, String paramString2, int paramInt) {
   this.path = paramString1;
   this.title = paramString2;
   this.level = paramInt;
  }
  
  /**
   * Gets the path.
   *
   * @return the path
   */
  public String getPath() {
   return this.path;
  }
  
  /**
   * Gets the level.
   *
   * @return the level
   */
  public int getLevel() {
   return this.level;
  }
  
  /**
   * Gets the title.
   *
   * @return the title
   */
  public String getTitle() {
   return this.title;
  }
 }
}
