/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

/**
 * The Class AEMConfigurationService.
 */
@Component(name = "com.unilever.platform.aem.foundation.core.service.AEMConfigurationService", label = "AEM Server | Search Configuration Service", description = "A service for configuring Solr", immediate = true, metatype = true)
@Service(AEMConfigurationService.class)
@Properties({
  @Property(name = SolrAEMConfigurationServiceAdminConstants.AUTHOR_AEM_DETAILS, value = { "author", "http://localhost:4502", "admin",
    "admin" }, label = "AEM Author server details", description = "Solr Author Server Details"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.PUBLISH_AEM_DETAILS, value = { "publish", "http://localhost:4503", "admin",
    "admin" }, label = "AEM Publish server details", description = "Solr Publish Server Details"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.IS_INDEXING_ENABLED, value = "true", label = "Enabled", description = "Enable the default Solr Index Listener"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.INDEXING_PATH, value = { "/content/unilever-iea",
    "/content/dove" }, label = "Observed Repository Paths", description = "The paths on which this listener is active"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.BREADCRUMB_START_LEVEL, value = {
    "4" }, label = "Breadcrumb initial Level", description = "Start level to show breadcrumb on search results"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.DO_NOT_INDEX_SOLR_LIST, value = { "/content/dove/en_gb/home",
    "/content/dove/en_gb/home/500", "/content/dove/en_gb/home/404",
    "/content/dove/en_gb/home/search" }, label = "Solr Do not index pages", description = "Solr Do not index pages list"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.AEM_PUBLISH_DOMAIN_NAME, value = "http://localhost:4503", label = "Unilever AEM publisher domain", description = "Add the Unilever publisher url"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.COMMIT_WITHIN, value = "3000", label = "commit within", description = "define commit within duration in ms, when 0, and softCoomit is false instant commit "
    + "option gets enabled(default functionality)"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.SOFT_COMMIT, boolValue = false, label = "soft commit", description = "softCommit, works only when commitWithin parameter is set 0"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.OBSERVATION_ENABLED, boolValue = true, label = "Observation Enabled", description = "the paths configured under observed.paths work only when this is true, on bulk update it is "
    + "advisable to make it false (performance parameter)"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.RESTRICTED_CONTENT_TYPE, value = {
    "recipe" }, label = "Content type to restrict", description = "Add the content types to restrict indexing", unbounded = PropertyUnbounded.ARRAY) })
/**
 * AEMConfigurationService provides a services for setting and getting AEM configuration information.
 */
public class AEMConfigurationService {
 
 /** The collection name. */
 private String collectionName = "";
 
 /** The indexing status. */
 private boolean isIndexingEnabled;
 
 /** The indexing path. */
 private String[] indexingPath;
 
 /** The author aem server array. */
 private String[] authorAEMServerArray;
 
 /** The publish aem server array. */
 private String[] publishAEMServerArray;
 
 /** The breadcrumb level. */
 private int breadcrumbLevel;
 /** The do not index pages. */
 private String[] doNotIndexPages;
 
 private String publisherdomain = "";
 /** The array of restricted content types. */
 private String[] restrcitedContentTypesArray;
 
 /** The Constant THREE_THOUSAND. */
 public static final int THREE_THOUSAND = 3000;
 
 private int commitWithinMS;
 
 private boolean softCommit = false;
 
 private boolean observationEnabled = true;
 
 /**
  * Activate.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  
  isIndexingEnabled = Boolean.valueOf(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.IS_INDEXING_ENABLED).toString());
  indexingPath = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.INDEXING_PATH));
  authorAEMServerArray = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.AUTHOR_AEM_DETAILS));
  publishAEMServerArray = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.PUBLISH_AEM_DETAILS));
  breadcrumbLevel = Integer.parseInt(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.BREADCRUMB_START_LEVEL).toString());
  doNotIndexPages = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.DO_NOT_INDEX_SOLR_LIST));
  publisherdomain = ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.AEM_PUBLISH_DOMAIN_NAME).toString();
  restrcitedContentTypesArray = PropertiesUtil
    .toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.RESTRICTED_CONTENT_TYPE));
  commitWithinMS = PropertiesUtil.toInteger(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.COMMIT_WITHIN), THREE_THOUSAND);
  softCommit = PropertiesUtil.toBoolean(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.SOFT_COMMIT), false);
  observationEnabled = PropertiesUtil.toBoolean(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.OBSERVATION_ENABLED), true);
 }
 
 public String getPublisherdomain() {
  return publisherdomain;
 }
 
 public String[] getDoNotIndexPages() {
  return doNotIndexPages;
 }
 
 /**
  * Checks if is Indexing enabled.
  *
  * @return true, if is Indexing enabled
  */
 public boolean isIndexingStatusEnabled() {
  return isIndexingEnabled;
 }
 
 /**
  * Gets the collection name.
  *
  * @return the collectionName
  */
 public String getCollectionName() {
  return collectionName;
 }
 
 /**
  * Gets the indexing path.
  *
  * @return the indexingPath
  */
 public String[] getIndexingPath() {
  return indexingPath;
 }
 
 /**
  * Gets the author aem server array.
  *
  * @return the authorAEMServerArray
  */
 public String[] getAuthorAEMServerArray() {
  return authorAEMServerArray;
 }
 
 /**
  * Gets the publish aem server array.
  *
  * @return the publishAEMServerArray
  */
 public String[] getPublishAEMServerArray() {
  return publishAEMServerArray;
 }
 
 /**
  * Gets the breadcrumb level.
  *
  * @return the breadcrumbLevel
  */
 public int getBreadcrumbLevel() {
  return breadcrumbLevel;
 }
 
 public String[] getRestrcitedContentTypesArray() {
  return restrcitedContentTypesArray;
 }
 
 public int getCommitWithinMS() {
  return commitWithinMS;
 }
 
 public boolean isSoftCommit() {
  return softCommit;
 }
 
 public boolean isObservationEnabled() {
  return observationEnabled;
 }
 
}
