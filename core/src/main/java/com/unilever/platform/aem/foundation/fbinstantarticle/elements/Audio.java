/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;

/**
 * The Class Time.
 */

/**
 * An audio within for the article. Also consider to use one of the other media types for an article:
 * <ul>
 * <li>Image</li>
 * <li>Video</li>
 * <li>SlideShow</li>
 * <li>Map</li>
 * </ul>
 * .
 *
 * Example: <audio title="audio title"> <source src="http://foo.com/mp3"> </audio>
 *
 */

public class Audio extends InstantArticleElement {
 /**
  * The audio title
  */
 private String title;
 /**
  * The string url for the audio file
  */
 private String url;
 /**
  * Can be set with: empty ("") (Default), "muted" or "autoplay"
  */
 private String playback;
 /**
  * boolean stores the usage or not of autoplay for audio
  */
 private boolean autoplay;
 /**
  * boolean stores status of muted for this audio
  */
 private boolean muted;
 
 private static Audio instance = new Audio();
 
 private Audio() {
  
 }
 
 public static Audio create() {
  
  return instance;
 }
 
 /**
  * Sets the URL for the audio. It is REQUIRED.
  *
  * @param string
  *         url The url of image. Ie: http://domain.com/audiofile.mp3
  *
  * @return this
  */
 public Audio withURL(String url) {
  this.url = url;
  return this;
 }
 
 /**
  * The audio title.
  *
  * @param string
  *         title the audio title that will be shown
  *
  * @return this
  */
 public Audio withTitle(String title) {
  this.title = title;
  return this;
 }
 
 /**
  * It will make audio start automatically.
  *
  * @return this
  */
 public Audio enableAutoplay() {
  this.autoplay = true;
  return this;
 }
 
 /**
  * It will make audio Not start automatically.
  *
  * @return this
  */
 public Audio disableAutoplay() {
  this.autoplay = false;
  return this;
 }
 
 /**
  * It will make audio be muted initially.
  *
  * @return this
  */
 public Audio enableMuted() {
  this.muted = true;
  return this;
 }
 
 /**
  * It will make audio laud.
  *
  * @return $this
  */
 public Audio disableMuted() {
  this.muted = false;
  return this;
 }
 
 /**
  * Gets the audio title
  *
  * @return string Audio title
  */
 public String getTitle() {
  return this.title;
 }
 
 /**
  * Gets the url for the audio
  *
  * @return string Audio url
  */
 public String getUrl() {
  return this.url;
 }
 
 /**
  * Gets the playback definition
  *
  * @return string playback definition
  */
 public String getPlayback() {
  return this.playback;
 }
 
 /**
  * To dom element.
  *
  * @param document
  *         the document
  * @return the org.jsoup.nodes. element
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  org.jsoup.nodes.Element audioElement = document.createElement("audio");
  
  if (StringUtils.isNotBlank(this.title)) {
   audioElement.attr("title", this.title);
  }
  if (this.autoplay) {
   audioElement.attr("autoplay", String.valueOf(this.autoplay));
  }
  if (this.muted) {
   audioElement.attr("muted", String.valueOf(this.muted));
  }
  
  // Audio URL markup. REQUIRED
  if (StringUtils.isNotBlank(this.url)) {
   org.jsoup.nodes.Element source_element = document.createElement("source");
   source_element.attr("src", this.url);
   audioElement.append(source_element.html());
  }
  
  return audioElement;
 }
 
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 public boolean isValid() {
  return StringUtils.isNotBlank(this.url) ? true : false;
 }
 
}
