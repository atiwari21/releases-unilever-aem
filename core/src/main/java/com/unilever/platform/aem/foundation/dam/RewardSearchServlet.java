/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.helper.RewardHelper;

@SlingServlet(paths = { "/bin/public/cms/rewards" }, methods = { HttpConstants.METHOD_GET }, label = "Unilever Reward Search Servlet")
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.dam.RewardSearchServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever Reward Search Servlet", propertyPrivate = false) })
public class RewardSearchServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RewardSearchServlet.class);
 
 private static final String PAGE_NUM = "pn";
 
 private static final String ITEMS_PER_PAGE = "ipp";
 
 /** The Constant DEFAULT_CONTENT_PATH. */
 private static final String DEFAULT_CONTENT_PATH = "/etc/commerce/products/unilever";
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 @Reference
 private ConfigurationAdmin configAdmin;
 
 /** The Constant REWARD. */
 public static final String REWARD = "rewardConfig";
 
 /** The Constant LOCATION. */
 private static final String LOCATION = "location";
 
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  ResourceResolver resourceResolver = request.getResourceResolver();
  Session session = resourceResolver.adaptTo(Session.class);
  int startIndex = 0;
  RequestParameterMap requestParametrs = request.getRequestParameterMap();
  int pageNum = Integer.parseInt(requestParametrs.containsKey(PAGE_NUM) ? requestParametrs.getValue(PAGE_NUM).toString() : "0");
  int itemsPerPage = Integer.parseInt(requestParametrs.containsKey(ITEMS_PER_PAGE) ? requestParametrs.getValue(ITEMS_PER_PAGE).toString() : "9");
  String pagePath = requestParametrs.containsKey("pp") ? requestParametrs.getValue("pp").toString() : StringUtils.EMPTY;
  
  if (pageNum > 0) {
   startIndex = (pageNum - 1) * itemsPerPage;
  }
  
  Page page = resourceResolver.getResource(pagePath) != null ? resourceResolver.getResource(pagePath).adaptTo(Page.class) : null;
  
  String rewardLocation = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, REWARD, LOCATION);
  if (StringUtils.isBlank(rewardLocation)) {
   rewardLocation = DEFAULT_CONTENT_PATH;
  }
  
  Map<String, Object> rewardRedemptionMap = new LinkedHashMap<String, Object>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  List<Map<String, Object>> rewardList = RewardHelper.getRewardList(rewardLocation, resourceResolver, session, page, request, startIndex,
    itemsPerPage, rewardRedemptionMap);
  
  rewardRedemptionMap.put("rewardList", rewardList.toArray());
  
  Gson gson = new GsonBuilder().serializeNulls().create();
  String jsonResponseString = gson.toJson(rewardRedemptionMap, new TypeToken<Map<String, Object>>() {
  }.getType());
  
  JSONObject obj1 = null;
  try {
   obj1 = new JSONObject(jsonResponseString);
  } catch (JSONException e1) {
   // TODO Auto-generated catch block
   e1.printStackTrace();
  }
  
  JSONObject jsonObj = new JSONObject();
  try {
   
   jsonObj.put("code", 200);
   jsonObj.put("status", "Success");
   jsonObj.put("errors", "");
   jsonObj.put("responseData", obj1);
  } catch (JSONException e) {
   LOGGER.error("Error in creating json", e);
  }
  response.getWriter().write(jsonObj.toString());
 }
}
