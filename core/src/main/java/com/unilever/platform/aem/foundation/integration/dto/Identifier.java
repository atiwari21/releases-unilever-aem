/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * The Class Identifier.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "value" })
public class Identifier {
 
 /** The value. */
 @XmlValue
 protected String value;
 
 /** The type. */
 @XmlAttribute(name = "Type", required = true)
 protected String type;
 
 /**
  * Gets the value.
  *
  * @return the value
  */
 public String getValue() {
  return value;
 }
 
 /**
  * Sets the value.
  *
  * @param value
  *         the new value
  */
 public void setValue(String value) {
  this.value = value;
 }
 
 /**
  * Gets the type.
  *
  * @return the type
  */
 public String getType() {
  return type;
 }
 
 /**
  * Sets the type.
  *
  * @param value
  *         the new type
  */
 public void setType(String value) {
  this.type = value;
 }
 
}
