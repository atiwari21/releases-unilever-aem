/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Caption;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.H1;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class H1Rule.
 */
public class H1Rule extends ConfigurationSelectorRule {
 
 /**
  * Instantiates a new h1 rule.
  */
 private H1Rule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the h1 rule
  */
 public static H1Rule create() {
  return new H1Rule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the rule
  */
 public static Rule createFrom(JSONObject configuration) {
  H1Rule h1Rule = new H1Rule();
  h1Rule.withSelector(configuration.getString("selector"));
  h1Rule.withProperties(getCaptionProperties(), configuration);
  return h1Rule;
 }
 
 /**
  * Gets the caption properties.
  *
  * @return the caption properties
  */
 static List<String> getCaptionProperties() {
  List<String> properties = new ArrayList<String>();
  properties.add(Caption.POSITION_BELOW);
  properties.add(Caption.POSITION_CENTER);
  properties.add(Caption.POSITION_ABOVE);
  properties.add(Caption.ALIGN_LEFT);
  properties.add(Caption.ALIGN_CENTER);
  properties.add(Caption.ALIGN_RIGHT);
  properties.add(Caption.SIZE_MEDIUM);
  properties.add(Caption.SIZE_LARGE);
  properties.add(Caption.SIZE_XLARGE);
  return properties;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 public String[] getContextClass() {
  return new String[] { Header.getClassName(), Caption.getClassName(), InstantArticle.getClassName() };
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Element)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  H1 h1 = H1.create();
  Container tempContainer=container;
  if (container instanceof Header) {
   tempContainer = ((Header) tempContainer).withTitle(h1);
  } else if (container instanceof Caption) {
   tempContainer = ((Caption) tempContainer).withTitle(h1);
  } else {
   tempContainer = transformer.getInstantArticle().addChild(h1);
  }
  
  if (this.getProperty(Caption.POSITION_BELOW, node) != null) {
   h1.withPosition(Caption.POSITION_BELOW);
  }
  if (this.getProperty(Caption.POSITION_CENTER, node) != null) {
   h1.withPosition(Caption.POSITION_CENTER);
  }
  if (this.getProperty(Caption.POSITION_ABOVE, node) != null) {
   h1.withPosition(Caption.POSITION_ABOVE);
  }
  if (this.getProperty(Caption.ALIGN_LEFT, node) != null) {
   h1.withPosition(Caption.ALIGN_LEFT);
  }
  if (this.getProperty(Caption.ALIGN_CENTER, node) != null) {
   h1.withPosition(Caption.ALIGN_CENTER);
  }
  if (this.getProperty(Caption.ALIGN_RIGHT, node) != null) {
   h1.withPosition(Caption.ALIGN_RIGHT);
  }
  transformer.transform(h1, node);
  return tempContainer;
 }
 
}
