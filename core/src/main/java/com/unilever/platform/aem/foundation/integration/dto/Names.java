/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Names.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "name" })
public class Names {
 
 /** The name. */
 @XmlElement(name = "Name")
 private List<Name> name;
 
 /**
  * Gets the name.
  *
  * @return the name
  */
 public List<Name> getName() {
  if (name == null) {
   name = new ArrayList<Name>();
  }
  return this.name;
 }
 
 /**
  * Sets the name.
  *
  * @param name
  *         the new name
  */
 public void setName(List<Name> name) {
  this.name = name;
 }
 
}
