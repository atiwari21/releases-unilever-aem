/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceConstants;
import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.AbstractJcrCommerceService;
import com.adobe.cq.commerce.common.ServiceContext;
import com.unilever.platform.aem.foundation.commerce.api.UNICommerceService;
import com.unilever.platform.aem.foundation.commerce.api.UNICommerceSession;

/**
 * The Class UNICommerceServiceImpl to implement commerce service.
 */
public class UNIRewardsCommerceServiceImpl extends AbstractJcrCommerceService implements UNICommerceService {
 
 /** The Constant LOG. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UNIRewardsCommerceServiceImpl.class);
 
 /**
  * The resource.
  */
 private final Resource resource;
 
 /**
  * The resolver.
  */
 private final ResourceResolver resolver;
 
 /**
  * Instantiates a new UNI commerce service impl.
  *
  * @param serviceContext
  *         the service context
  * @param resource
  *         the res
  */
 public UNIRewardsCommerceServiceImpl(ServiceContext serviceContext, Resource resource) {
  super(serviceContext);
  this.resource = resource;
  this.resolver = resource.getResourceResolver();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#login(org.apache.sling.api. SlingHttpServletRequest, org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 public UNICommerceSession login(SlingHttpServletRequest request, SlingHttpServletResponse response) throws CommerceException {
  return new UNICommerceSessionImpl(this, request, response, this.resource);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#isAvailable(java.lang.String)
  */
 @Override
 public boolean isAvailable(String serviceType) {
  return "commerce-service".equals(serviceType);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getProduct(java.lang.String)
  */
 @Override
 public Product getProduct(final String productPath) {
  Product product = null;
  StringBuilder path = new StringBuilder();
  
  path.append(productPath);
  LOGGER.debug("product path is : " + productPath);
  
  Resource productResouce = this.resolver.getResource(path.toString());
  if (productResouce != null && UNIProductImpl.isAProductOrVariant(productResouce)) {
   return new UNIProductImpl(productResouce);
  }
  return product;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getCountries()
  */
 @Override
 public List<String> getCountries() throws CommerceException {
  List<String> countries = new ArrayList<String>();
  countries.add("*");
  return countries;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getCreditCardTypes()
  */
 @Override
 public List<String> getCreditCardTypes() throws CommerceException {
  List<String> ccTypes = new ArrayList<String>();
  ccTypes.add("*");
  return ccTypes;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceService#getOrderPredicates()
  */
 @Override
 public List<String> getOrderPredicates() throws CommerceException {
  List<String> predicates = new ArrayList<String>();
  predicates.add(CommerceConstants.OPEN_ORDERS_PREDICATE);
  return predicates;
 }
}
