/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.helper.ContentMigrationHelper;

/**
 * The Class UpdateContentServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "updatecontent" }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UpdateContentServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the content's properties ", propertyPrivate = false) })
public class UpdateContentServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 782708002977083726L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UpdateContentServlet.class);
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "text/html";
 
 /** The Constant RELEASE_NUMBER. */
 private static final String UPDATE_TYPE = "updateType";
 
 /** The Constant JSON. */
 private static final String JSON = "json";
 
 /** The Constant SITE_PATH. */
 private static final String SITE_PATH = com.day.cq.wcm.api.NameConstants.PN_SITE_PATH;
 
 /** The Constant CONTENT_UPDATE_CONFIG. */
 private static final String CONTENT_UPDATE_CONFIG = "contentUpdateConfig";
 
 /** The Constant TEMPLATE_NAME. */
 private static final String TEMPLATE_NAME = "templateName";
 
 /** The Constant PROPERTY_NAMES. */
 private static final String PROPERTY_NAMES = "propertyNames";
 
 private static final String PAGE_PRPERTIES = "pagePrperties";
 
 /** The Constant "</tr>". */
 private static final String TR = "</tr>";
 
 /** The Constant HTML_FORM. */
 private static final String HTML_FORM = "<!DOCTYPE html>" + "<html>" + "<head>" + "<meta charset=\"ISO-8859-1\">"
   + "<title>Content Migration</title>" + "</head>" + "<body>" + "<form action=\"\" method=\"get\">" + "<table>" + "<tr>"
   + "<td><label>Update Type</label></td>" + "<td><input type=\"text\" name=\"updateType\"></td>" + TR + "<tr>" + "<td><label>Site Path</label></td>"
   + "<td><input type=\"text\" name=\"sitePath\"></td>" + TR + "<tr>" + "<td><label>Config JSON</label></td>"
   + "<td><textarea rows=\"10\" cols=\"100\" name=\"json\"></textarea></td>" + TR + "<tr>" + "<td><input type=\"submit\"></td>" + TR + "</table>"
   + "</form>" + "</body>" + "</html>";
 
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String configJSON = request.getParameter(JSON);
  LOGGER.info("configJSON in UpdateContentServlet {} ", configJSON);
  response.setContentType(CONTENT_TYPE);
  PrintWriter out = response.getWriter();
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  LOGGER.info("sitePath in UpdateContentServlet {} ", request.getParameter(SITE_PATH));
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = request.getParameter(SITE_PATH) != null ? pageManager.getPage(request.getParameter(SITE_PATH)) : null;
  if (configJSON == null) {
   out.append(HTML_FORM);
  } else if (page != null) {
   String updateType = request.getParameter(UPDATE_TYPE) != null ? request.getParameter(UPDATE_TYPE) : StringUtils.EMPTY;
   if (updateType.equals(PAGE_PRPERTIES)) {
    try {
     StringBuilder successMessage = new StringBuilder("Below properties updated for pages <br/><br/>");
     Map<Page, JSONArray> pageMap = new LinkedHashMap<Page, JSONArray>();
     loadPagesMap(pageManager, page, configJSON, pageMap);
     if (ContentMigrationHelper.updatePageProperties(pageMap, successMessage)) {
      out.write(successMessage.toString());
      LOGGER.info(successMessage.toString());
      resourceResolver.commit();
     }
    } catch (Exception e) {
     out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
     LOGGER.error("Error in updating the content", e);
     resourceResolver.revert();
    }
   }
   
  } else {
   out.write("Please provide the valid site path");
  }
  out.flush();
  out.close();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
 
 /**
  * Load pages map.
  *
  * @param pageManager
  *         the page manager
  * @param page
  *         the page
  * @param configJSON
  *         the config json
  * @param pageMap
  */
 private void loadPagesMap(PageManager pageManager, Page page, String configJSON, Map<Page, JSONArray> pageMap) {
  LOGGER.debug("inside loadPagesMap of UpdateContentServlet");
  JSONObject configJSONObj = new JSONObject(configJSON);
  JSONArray arr = configJSONObj.getJSONArray(CONTENT_UPDATE_CONFIG);
  for (int i = 0; arr != null && i < arr.length(); ++i) {
   JSONObject obj = arr.getJSONObject(i);
   String templateNameStr = obj.getString(TEMPLATE_NAME) != null ? obj.getString(TEMPLATE_NAME) : StringUtils.EMPTY;
   LOGGER.debug("templateName from configJSON : " + templateNameStr);
   Iterator<Page> itr = page.listChildren();
   while (itr.hasNext()) {
    Page childPage = itr.next();
    String templatePath = childPage.getProperties().get(NameConstants.PN_TEMPLATE, StringUtils.EMPTY);
    LOGGER.debug("templatePath of current iterating page : " + templatePath);
    if (templatePath.endsWith(templateNameStr)) {
     JSONArray propArr = obj.getJSONArray(PROPERTY_NAMES);
     pageMap.put(childPage, propArr);
     
    }
    loadPagesMap(pageManager, childPage, configJSON, pageMap);
   }
  }
 }
 
}
