/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;

/**
 * The Class CreateSchemaServlet
 *
 */
@SlingServlet(label = "Unilever create pim schema", paths = { "/bin/createproductschemaconfigurations" }, methods = { HttpConstants.METHOD_POST })
public class CreateSchemaServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 782708002977083726L;
 
 /** The Constant STORE_PATH. */
 private static final String STORE_PATH = "storePath";
 
 private static final String MARKETS = "markets";
 
 private static final String DEFAULT_CONTENT_PATH = "/content/pimschema";
 
 private static final String DEFAULT_SCHEMA_PATH = "/etc/commerce/products";
 
 private static final String SCHEMA = "/schema";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CreateSchemaServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  
  ResourceResolver resourseResolver = request.getResource().getResourceResolver();
  PageManager pm = resourseResolver.adaptTo(PageManager.class);
  
  String destinationPath = request.getParameter(STORE_PATH).toString();
  String originalDestinationPath = request.getParameter(STORE_PATH).toString();
  String selectedSchemaConfigurationPagePath = request.getParameter(MARKETS).toString();
  
  Page selectedSchemaConfigurationPage = pm.getPage(selectedSchemaConfigurationPagePath);
  Page existingSchemaPage = pm.getPage(destinationPath + SCHEMA);
  Session session = resourseResolver.adaptTo(Session.class);
  try {
   if (existingSchemaPage != null) {
    pm.delete(existingSchemaPage.adaptTo(Resource.class), false, true);
   }
   
   Node productFolderNode = resourseResolver.getResource(destinationPath).adaptTo(Node.class);
   
   destinationPath = destinationPath.replace(DEFAULT_SCHEMA_PATH, DEFAULT_CONTENT_PATH);
   if (resourseResolver.getResource(destinationPath) == null) {
    JcrUtil.createPath(destinationPath, true, JcrResourceConstants.NT_SLING_FOLDER, JcrResourceConstants.NT_SLING_FOLDER, session, true);
   }
   
   if (productFolderNode != null) {
    productFolderNode.setProperty("schemaPath", destinationPath + SCHEMA);
    session.save();
   }
   
   Page schemaPage = pm.copy(selectedSchemaConfigurationPage, destinationPath + SCHEMA, null, false, false, true);
   Node pageProperties = schemaPage.getContentResource().adaptTo(Node.class);
   
   Node liveSyncConfigNode = schemaPage.getContentResource("cq:LiveSyncConfig").adaptTo(Node.class);
   liveSyncConfigNode.setProperty("cq:master", selectedSchemaConfigurationPage.getPath());
   pageProperties.setProperty("parentProductSchemaPath", selectedSchemaConfigurationPage.getPath());
   
  } catch (WCMException e) {
   LOGGER.error("Error while creating schema configuration page at destination location", e);
  } catch (RepositoryException re) {
   LOGGER.error("Error while getting resource node", re);
  }
  try {
   session.save();
  } catch (RepositoryException se) {
   LOGGER.error("Session timed out", se);
  }
  response.sendRedirect("/aem/products.html" + originalDestinationPath);
 }
 
}
