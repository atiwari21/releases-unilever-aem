/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.commons.ImageResource;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.search.QueryBuilder;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.impl.SearchServiceImpl;

/**
 * Utility Helper class for handlebar script helpers.
 * 
 * @author nbhart
 * 
 */
public final class RatingHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RatingHelper.class);
 
 /** The Constant BV_IMAGE_DEFAULT_SIZE. */
 private static final String IMAGE_DEFAULT_SIZE = "220x220";
 
 /** The Constant BV_IMAGE_SIZE. */
 private static final String IMAGE_SIZE = "imageSize";
 
 private static final String FEED_CATEGORY = "productFeedConfiguration";
 
 private static final String PROPERTY_NAME = "propName";
 
 private static final String PROPERTY_VALUE = "propValue";
 
 private static final String IS_ENCODING_REQUIRED = "isEncodingRequired";
 
 /**
  * private constructor.
  */
 private RatingHelper() {
 }
 
 /**
  * Gets the product page urls.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configurationService
  * @return the product page urls
  */
 public static String getPageFullUrls(Page page, String domainUrl, ConfigurationService configurationService) {
  String isEncodingRequired = getEncodingRequired(page, configurationService);
  return domainUrl + getEncodedOrSamePath(UrlResolverUtility.getResolvedPagePath(configurationService, page) + ".html", isEncodingRequired);
 }
 
 /**
  * Save feedto file system.
  *
  * @param feed
  *         the feed
  * @return the string
  */
 public static String saveFeedtoFileSystem(ProductFeeds feed) {
  ObjectMapper mapper = new ObjectMapper();
  String jsonInString = "";
  try {
   jsonInString = mapper.writeValueAsString(feed);
  } catch (JsonGenerationException e) {
   LOGGER.error("JsonGenerationException in feed feeds generation ", e);
  } catch (JsonMappingException e) {
   LOGGER.error("Error in mapping json ", e);
  } catch (IOException e) {
   LOGGER.error("IO exception in json feeds generation ", e);
  }
  return jsonInString;
  
 }
 
 /**
  * Gets the identifier.
  *
  * @param currentProduct
  *         the current product
  * @return the identifier
  */
 public static Identifier getIdentifier(UNIProduct currentProduct) {
  Identifier identifier = new Identifier();
  identifier.setIdentifierType(currentProduct.getIdentifierType());
  identifier.setIdentifierValue(currentProduct.getIdentifierValue());
  return identifier;
 }
 
 /**
  * Gets the product category.
  *
  * @param page
  *         the page
  * @return the product category
  */
 public static String getProductCategoryName(Page page) {
  
  return page.getParent().getTitle();
 }
 
 /**
  * Gets the category feed.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param resourceResolver
  *         the resource resolver
  * @return the category feed
  */
 public static CategoryType getCategoryFeed(Page page, String domainUrl, ConfigurationService configurationService) {
  CategoryType category = new CategoryType();
  category.setCategoryId(getCategoryId(configurationService, page));
  category.setParentCategoryId(getParentCategoryId(configurationService, page));
  category.setFaimlyId(getProductFamilyPrefix(configurationService, page) + category.getCategoryId());
  category.setName(page.getTitle());
  category.setCategoryPageUrl(RatingHelper.getPageFullUrls(page, domainUrl, configurationService));
  category.setImageUrl(getCategoryImage(page, domainUrl, configurationService));
  return category;
 }
 
 /**
  * Gets the category id.
  *
  * @param page
  *         the page
  * @param configurationService
  *         the configurationService
  * @return the category id
  */
 public static String getCategoryId(ConfigurationService configurationService, Page page) {
  final Resource jcrResource = page.getContentResource();
  TagManager tagManager = jcrResource.getResourceResolver().adaptTo(TagManager.class);
  Tag[] tags = tagManager.getTags(jcrResource);
  String parentId = getParentIdForCategory(configurationService, page, tags);
  String[] categoryNamespace = getCategoryNamespace(configurationService, page);
  String categoryId = null;
  Map<String, Tag> namespaceTags = new HashMap<String, Tag>();
  for (String categoryName : categoryNamespace) {
   Tag namespaceTag = getMatchingTag(tags, StringUtils.trim(categoryName));
   if (namespaceTag != null) {
    namespaceTags.put(categoryName, namespaceTag);
   }
  }
  if (ArrayUtils.isNotEmpty(categoryNamespace) && categoryNamespace.length == namespaceTags.size()) {
   categoryId = StringUtils.EMPTY;
   for (String categoryName : categoryNamespace) {
    Tag tag = namespaceTags.get(categoryName);
    categoryId = categoryId + tag.getLocalTagID().toString().replace("/", "").replace("-", "");
   }
  }
  if (StringUtils.isNotBlank(categoryId) && StringUtils.isNotBlank(parentId)) {
   categoryId = parentId + categoryId;
  }
  return categoryId;
 }
 
 /**
  * Gets the category image.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param configurationService
  *         the configuration service
  * @return the category image
  */
 public static String getCategoryImage(Page page, String domainUrl, ConfigurationService configurationService) {
  String isEncodingRequired = getEncodingRequired(page, configurationService);
  String categoryTeaserImage = StringUtils.EMPTY;
  String imagepath = StringUtils.EMPTY;
  String widgetImageSize = IMAGE_DEFAULT_SIZE;
  try {
   widgetImageSize = configurationService.getConfigValue(page, FEED_CATEGORY, IMAGE_SIZE);
   if (StringUtils.isBlank(widgetImageSize)) {
    widgetImageSize = IMAGE_DEFAULT_SIZE;
   }
  } catch (ConfigurationNotFoundException e) {
   widgetImageSize = IMAGE_DEFAULT_SIZE;
   LOGGER.error("BV Image size configuration not found", e);
  }
  ValueMap properties = page.getProperties();
  categoryTeaserImage = properties.get("teaserImage") != null ? properties.get("teaserImage").toString() : StringUtils.EMPTY;
  
  if (!categoryTeaserImage.isEmpty() && StringUtils.contains(categoryTeaserImage, ".")) {
   String imagepathTemp = categoryTeaserImage + ".ulenscale." + widgetImageSize + categoryTeaserImage.substring(categoryTeaserImage.indexOf("."));
   imagepath = domainUrl + getEncodedOrSamePath(imagepathTemp, isEncodingRequired);
  }
  
  return imagepath;
 }
 
 /**
  * Gets the category data for product.
  *
  * @param page
  *         the page
  * @param configurationService
  *         the configurationService
  * @return the categorydata
  */
 public static List<CategoryDetail> getCategoryDataForProduct(ConfigurationService configurationService, Page page) {
  List<CategoryDetail> categoryList = new ArrayList<CategoryDetail>();
  CategoryDetail mainCategory = new CategoryDetail();
  
  String familyPrefix = getProductFamilyPrefix(configurationService, page);
  mainCategory.setCategoryId(getCategoryId(configurationService, page));
  mainCategory.setFamilyId(familyPrefix + mainCategory.getCategoryId());
  if (StringUtils.isNotBlank(mainCategory.getCategoryId())) {
   categoryList.add(mainCategory);
  }
  final Resource jcrResource = page.getContentResource();
  
  TagManager tagManager = jcrResource.getResourceResolver().adaptTo(TagManager.class);
  Tag[] tags = tagManager.getTags(jcrResource);
  categoryList.addAll(getParentCategoriesFromProduct(configurationService, page, tags));
  
  return categoryList;
  
 }
 
 /**
  * Gets the product image urls.
  *
  * @param page
  *         the page
  * @param currentProduct
  *         the current product
  * @param domainUrl
  *         the domain url
  * @param configurationService
  *         the configuration service
  * @return the product image urls
  */
 public static String getProductImageUrls(Page page, UNIProduct currentProduct, String domainUrl, ConfigurationService configurationService) {
  String isEncodingRequired = getEncodingRequired(page, configurationService);
  String imageurls = "";
  if ((currentProduct != null) && (!currentProduct.getImages().isEmpty())) {
   ImageResource imgResource = currentProduct.getImages().get(0);
   String widgetImageSize = IMAGE_DEFAULT_SIZE;
   try {
    widgetImageSize = configurationService.getConfigValue(page, FEED_CATEGORY, IMAGE_SIZE);
    if (StringUtils.isBlank(widgetImageSize)) {
     widgetImageSize = IMAGE_DEFAULT_SIZE;
    }
   } catch (ConfigurationNotFoundException e) {
    widgetImageSize = IMAGE_DEFAULT_SIZE;
    LOGGER.error("BV Image size configuration not found", e);
   }
   String imageurlsTemp = imgResource.getHref() + ".ulenscale." + widgetImageSize + imgResource.getExtension();
   imageurls = domainUrl + getEncodedOrSamePath(imageurlsTemp, isEncodingRequired);
  }
  
  return imageurls;
 }
 
 /**
  * Gets the categories feed.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @param domainUrl
  *         the domain url
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param builder
  *         the builder
  * @return the categories feed
  */
 public static CategoriesType getCategoriesFeed(Session session, String path, String domainUrl, ResourceResolver resourceResolver,
   ConfigurationService configurationService, QueryBuilder builder) {
  
  CategoriesType categories = new CategoriesType();
  String propName = StringUtils.EMPTY;
  String propValue = StringUtils.EMPTY;
  
  try {
   Map<String, String> configurations = configurationService.getCategoryConfiguration(resourceResolver.getResource(path).adaptTo(Page.class),
     FEED_CATEGORY);
   propName = configurations.get(PROPERTY_NAME);
   propValue = configurations.get(PROPERTY_VALUE);
  } catch (ConfigurationNotFoundException cnfe) {
   LOGGER.error("Configuration not found for {}", FEED_CATEGORY, cnfe);
  }
  List<Page> categoryPageList = SearchServiceImpl.getPages(session, path, propName, propValue, builder);
  List<String> parentCategoryList = new ArrayList<String>();
  for (Page page : categoryPageList) {
   Page targetPage = page;
   while (StringUtils.isNotBlank(getCategoryId(configurationService, targetPage)) && parentCategoryList.indexOf(targetPage.getPath()) == -1) {
    categories.getCategory().add(RatingHelper.getCategoryFeed(targetPage, domainUrl, configurationService));
    parentCategoryList.add(targetPage.getPath());
    targetPage = targetPage.getParent();
   }
  }
  
  return categories;
 }
 
 /**
  * Gets the product feed.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param resourceResolver
  *         the resource resolver
  * @return the product feed
  */
 public static ProductType getProductFeed(Page page, String domainUrl, ConfigurationService configurationService) {
  ProductType product = null;
  try {
   Product prd = CommerceHelper.findCurrentProduct(page);
   if (prd instanceof UNIProduct) {
    product = new ProductType();
    UNIProduct currentProduct = (UNIProduct) (prd);
    
    product.setProductId(currentProduct.getSKU());
    product.setCategoryDetail(getCategoryDataForProduct(configurationService, page));
    product.setName(getProductName(configurationService, currentProduct, page));
    product.setImageUrl(getProductImageUrls(page, currentProduct, domainUrl, configurationService));
    product.setDescription(getProductDescription(configurationService, currentProduct, page));
    product.setProductPageUrl(getPageFullUrls(page, domainUrl, configurationService));
    Identifier identifier = getIdentifier(currentProduct);
    product.setIdentifier(identifier);
    product.setCategoryId(getCategoryId(configurationService, page));
    product.setProductVariants(getProductVariantsList(currentProduct));
    
    ReplicationStatus status = page.adaptTo(ReplicationStatus.class);
    if (status != null) {
     if (status.isActivated()) {
      product.setActive(true);
     } else if (status.isDeactivated()) {
      product.setActive(false);
     } else {
      product.setActive(false);
     }
    }
   }
  } catch (Exception e) {
   LOGGER.error("error while getting product from page : " + page.getPath(), e);
  }
  return product;
 }
 
 /**
  * Gets the parent category id.
  *
  * @param page
  *         the page
  * @param configurationService
  *         the configurationService
  * @return the parent category id
  */
 public static String getParentCategoryId(ConfigurationService configurationService, Page page) {
  String categoryId = StringUtils.EMPTY;
  if (page != null) {
   categoryId = getCategoryId(configurationService, page.getParent());
  }
  return categoryId;
 }
 
 /**
  * @param configurationService
  * @param page
  *         the page
  * @return
  */
 public static String getProductFamilyPrefix(ConfigurationService configurationService, Page page) {
  String prefix = StringUtils.EMPTY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config.get("productFamilyPrefix") != null) {
    prefix = (String) config.get("productFamilyPrefix");
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for {} ", FEED_CATEGORY, e);
  }
  return prefix;
 }
 
 /**
  * @param configurationService
  * @return
  */
 private static String[] getCategoryNamespace(ConfigurationService configurationService, Page page) {
  String[] namespace = ArrayUtils.EMPTY_STRING_ARRAY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config.get("categoryNamespace") != null) {
    String categoryNamespace = (String) config.get("categoryNamespace");
    if (StringUtils.isNotBlank(categoryNamespace)) {
     namespace = StringUtils.split(categoryNamespace, ",");
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for {}.", FEED_CATEGORY, e);
  }
  return namespace;
 }
 
 /**
  * @param product
  * @return
  */
 private static List<ProductVariantType> getProductVariantsList(UNIProduct product) {
  List<ProductVariantType> variantList = null;
  try {
   Iterator<Product> productVariants = product.getVariants();
   if (productVariants != null) {
    variantList = getVariantsList(productVariants);
   } else {
    variantList = new ArrayList<ProductVariantType>();
   }
  } catch (CommerceException ce) {
   LOGGER.error("Exception while getting the variants", ce);
  }
  
  return variantList;
 }
 
 /**
  * Returns variants list.
  * 
  * @param variantList
  * @param productVariants
  */
 private static List<ProductVariantType> getVariantsList(Iterator<Product> productVariants) {
  List<ProductVariantType> variantList = new ArrayList<ProductVariantType>();
  while (productVariants.hasNext()) {
   ProductVariantType productVariantType = getProductVariant((UNIProduct) productVariants.next());
   if (productVariantType != null) {
    variantList.add(productVariantType);
   }
  }
  return variantList;
 }
 
 /**
  * @param product
  * @return
  */
 private static ProductVariantType getProductVariant(UNIProduct product) {
  ProductVariantType variant = null;
  if (product != null && product.getSKU() != null) {
   variant = new ProductVariantType();
   variant.setName(product.getName());
   variant.setVariantId(product.getSKU());
   variant.setVariantSize(product.getSize());
   Identifier identifier = getIdentifier(product);
   variant.setIdentifier(identifier);
  }
  return variant;
 }
 
 /**
  * Finds the parent and super parent categories for a product based upon the tags associated with the product.
  * 
  * @param configurationService
  * @param page
  * @param tags
  * @return list of parent and super parent categories
  */
 public static List<CategoryDetail> getParentCategoriesFromProduct(ConfigurationService configurationService, Page page, Tag[] tags) {
  
  List<CategoryDetail> categoryList = new ArrayList<CategoryDetail>();
  Map<String, Tag> namespaceTags = new HashMap<String, Tag>();
  String[] categoryNamespace = getCategoryNamespace(configurationService, page);
  String parentId = getParentIdForCategory(configurationService, page, tags);
  String familyPrefix = getProductFamilyPrefix(configurationService, page);
  for (String categoryName : categoryNamespace) {
   Tag namespaceTag = getMatchingParentTag(tags, StringUtils.trim(categoryName));
   if (namespaceTag != null) {
    namespaceTags.put(categoryName, namespaceTag);
   }
  }
  if (ArrayUtils.isNotEmpty(categoryNamespace)) {
   while (categoryNamespace.length == namespaceTags.size()) {
    String categoryId = StringUtils.EMPTY;
    for (String categoryName : categoryNamespace) {
     Tag tag = namespaceTags.get(categoryName);
     categoryId = categoryId + tag.getLocalTagID().toString().replace("/", "").replace("-", "");
     tag = tag.getParent();
     if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), categoryName) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), categoryName)) {
      namespaceTags.replace(categoryName, tag);
     } else {
      namespaceTags.remove(categoryName);
     }
    }
    if (StringUtils.isNotBlank(categoryId)) {
     CategoryDetail secondaryCategory = new CategoryDetail();
     secondaryCategory.setCategoryId(parentId + categoryId);
     secondaryCategory.setFamilyId(familyPrefix + parentId + categoryId);
     if (StringUtils.isNotBlank(secondaryCategory.getCategoryId())) {
      categoryList.add(secondaryCategory);
     }
    }
   }
  }
  
  return categoryList;
 }
 
 /**
  * Get parent category namespace
  * 
  * @param configurationService
  * @param page
  * @return namespace
  */
 private static String getParentCategoryNamespace(ConfigurationService configurationService, Page page) {
  String namespace = StringUtils.EMPTY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config.get("parentCategoryNamespace") != null) {
    namespace = (String) config.get("parentCategoryNamespace");
    
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for {}", FEED_CATEGORY, e);
  }
  return namespace;
 }
 
 /**
  * Get parent category id.
  * 
  * @param configurationService
  * @param page
  * @param tags
  * @return parent category id.
  */
 private static String getParentIdForCategory(ConfigurationService configurationService, Page page, Tag[] tags) {
  String parentCategory = getParentCategoryNamespace(configurationService, page);
  String categoryId = StringUtils.EMPTY;
  if (StringUtils.isNotBlank(parentCategory)) {
   for (Tag t : tags) {
    if (StringUtils.startsWithIgnoreCase(t.getLocalTagID(), parentCategory) && !StringUtils.equalsIgnoreCase(t.getLocalTagID(), parentCategory)) {
     categoryId = t.getLocalTagID().toString().replace("/", "").replace("-", "");
     break;
    }
   }
  }
  return categoryId;
 }
 
 /**
  * Returns matching parent tag from passed in namespace
  * 
  * @param tags
  * @param namespace
  * @return parent tag of the matching tag
  */
 private static Tag getMatchingParentTag(Tag[] tags, String namespace) {
  Tag matchingTag = null;
  for (Tag tag : tags) {
   if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), namespace) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), namespace)) {
    tag = tag.getParent();
    if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), namespace) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), namespace)) {
     matchingTag = tag;
     break;
    }
   }
  }
  return matchingTag;
 }
 
 /**
  * Returns matching tag for passed in namespace
  * 
  * @param tags
  * @param namespace
  * @return matching tag
  */
 private static Tag getMatchingTag(Tag[] tags, String namespace) {
  Tag matchingTag = null;
  for (Tag tag : tags) {
   if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), namespace) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), namespace)) {
    matchingTag = tag;
    break;
   }
  }
  return matchingTag;
 }
 
 /**
  * This method returns product name. It picks name from the property that has been set in config, if no proprty set then directly returns name of
  * current product.
  * 
  * @param configurationService
  * @param currentProduct
  * @param page
  * @return
  */
 private static String getProductName(ConfigurationService configurationService, UNIProduct currentProduct, Page page) {
  String name = StringUtils.EMPTY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config != null) {
    name = (String) config.get("productNameProperty");
   }
   if (StringUtils.isBlank(name)) {
    name = currentProduct.getName();
   } else {
    name = currentProduct.getProperty(name, String.class);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for Product name in ", FEED_CATEGORY, e);
  }
  return name;
 }
 
 /**
  * This method returns product description. It picks description from the property that has been set in config, if no proprty set then directly
  * returns description of current product.
  * 
  * @param configurationService
  * @param currentProduct
  * @param page
  * @return
  */
 private static String getProductDescription(ConfigurationService configurationService, UNIProduct currentProduct, Page page) {
  String description = StringUtils.EMPTY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config != null) {
    description = (String) config.get("productDescriptionProperty");
   }
   if (StringUtils.isBlank(description)) {
    description = currentProduct.getDescription();
   } else {
    description = currentProduct.getProperty(description, String.class);
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for Product description in ", FEED_CATEGORY, e);
  }
  return description;
 }
 
 /**
  * Gets the encodedPath.
  *
  * @param path
  *         the path
  * @return the encodedPath
  */
 private static String getEncodedOrSamePath(String path, String isEncodingRequired) {
  
  String encodedPath = StringUtils.EMPTY;
  if (("true").equalsIgnoreCase(isEncodingRequired)) {
   String[] pathArray = path.split("/");
   StringBuilder stringBuilder = new StringBuilder();
   for (int i = 0; i < pathArray.length; i++) {
    try {
     String segment = java.net.URLEncoder.encode(pathArray[i], "UTF-8");
     if (pathArray.length - 1 == i) {
      stringBuilder.append(segment);
     } else {
      stringBuilder.append(segment + "/");
     }
    } catch (UnsupportedEncodingException e) {
     LOGGER.error("Error occured while encoding", e);
    }
   }
   encodedPath = stringBuilder.toString();
  } else {
   encodedPath = path;
  }
  
  return encodedPath;
 }
 
 /**
  * Gets the encodingRequired.
  *
  * @param configurationService
  *         the configurationService
  * @return the encodingRequired
  */
 private static String getEncodingRequired(Page page, ConfigurationService configurationService) {
  
  String encodingRequired = StringUtils.EMPTY;
  try {
   encodingRequired = configurationService.getConfigValue(page, FEED_CATEGORY, IS_ENCODING_REQUIRED);
  } catch (ConfigurationNotFoundException e1) {
   LOGGER.error("isEncodingRequired key not found in the category productFeedConfiguration", e1);
  }
  return encodingRequired;
 }
}
