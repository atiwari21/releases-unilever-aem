/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class DAMAssetSearchServlet.
 */
@SlingServlet(paths = { "/bin/public/cms/assetsearch" },label = "Unilever DAM Asset Servlet to Fetch Assets", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = { HttpConstants.METHOD_GET }, selectors = { "damasset" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.dam.DAMAssetSearchServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever DAM Asset Servlet to Fetch Assets", propertyPrivate = false),

})
public class DAMAssetSearchServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(DAMAssetSearchServlet.class);
 
 /** The Constant HASHTAG_PARAM. */
 private static final String HASHTAG_PARAM = "h";
 
 /** The Constant CHANNEL_PARAM. */
 private static final String CHANNEL_PARAM = "chnl";
 
 /** The Constant START_INDEX. */
 private static final String START_INDEX = "si";
 
 /** The Constant RESULT_NO. */
 private static final String RESULT_NO = "rsno";
 
 /** The Constant SEARCH_PATH. */
 private static final String SEARCH_PATH = "sp";
 
 /** The Constant BRAND_NAME. */
 private static final String BRAND_NAME = "br";
 
 /** The Constant LOCALE_NAME. */
 private static final String LOCALE_NAME = "lo";
 
 /** The Constant DEFAULT_CONTENT_PATH. */
 private static final String DEFAULT_CONTENT_PATH = "/content/dam/unilever";
 
 /** The Constant GROUP_1. */
 private static final String GROUP_1 = "group.1_group.";
 
 /** The Constant GROUP_2. */
 private static final String GROUP_2 = "group.2_group.";
 
 private static final String METADATA_PATH = "@jcr:content/metadata/";
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 @Reference
 private ConfigurationAdmin configAdmin;
 
 private long totalSize = 0;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  List<Asset> assets = new ArrayList<Asset>();
  
  String path = request.getRequestPathInfo().getResourcePath();
  ResourceResolver resourceResolver = request.getResourceResolver();
  Page page = resourceResolver.getResource(path).adaptTo(Page.class);
  
  RequestParameterMap requestParametrs = request.getRequestParameterMap();
  String[] hashTags = getRequestParamsArray(requestParametrs, HASHTAG_PARAM).toArray(new String[0]);
  String[] channels = getRequestParamsArray(requestParametrs, CHANNEL_PARAM).toArray(new String[0]);
  int startIndex = Integer.parseInt(requestParametrs.containsKey(START_INDEX) ? requestParametrs.getValue(START_INDEX).toString() : "0");
  int resultNumber = Integer.parseInt(requestParametrs.containsKey(RESULT_NO) ? requestParametrs.getValue(RESULT_NO).toString() : "-1");
  String searchPath = requestParametrs.containsKey(SEARCH_PATH) ? requestParametrs.getValue(SEARCH_PATH).toString() : StringUtils.EMPTY;
  String brandName = requestParametrs.containsKey(BRAND_NAME) ? requestParametrs.getValue(BRAND_NAME).toString() : StringUtils.EMPTY;
  String localeName = requestParametrs.containsKey(LOCALE_NAME) ? requestParametrs.getValue(LOCALE_NAME).toString() : StringUtils.EMPTY;
  
  if (StringUtils.isBlank(searchPath)) {
   searchPath = DEFAULT_CONTENT_PATH;
  }
  
  if (!StringUtils.isBlank(brandName)) {
   searchPath = DEFAULT_CONTENT_PATH + "/" + brandName;
  }
  
  if (!StringUtils.isBlank(localeName)) {
   searchPath = searchPath + "/" + localeName;
  }
  
  if (startIndex > 0) {
   startIndex = startIndex - 1;
  } else if (startIndex < 0) {
   startIndex = 0;
  }
  
  assets = getAssetsByHashTag(hashTags, channels, searchPath, startIndex, resultNumber, request.getResourceResolver());
  JSONObject jsonObj = new JSONObject();
  try {
   JSONArray jsonArray = getJSONResponseFromAssets(assets, page, resourceResolver, request);
   jsonObj.put("totalResults", totalSize);
   jsonObj.put("results", jsonArray);
  } catch (JSONException e) {
   LOGGER.error("Error in creating json", e);
  }
  
  response.getWriter().write(jsonObj.toString());
 }
 
 /**
  * Gets the request params array.
  *
  * @param requestParamMap
  *         the request param map
  * @param key
  *         the key
  * @return the request params array
  */
 private List<String> getRequestParamsArray(RequestParameterMap requestParamMap, String key) {
  
  List<String> valueList = new ArrayList<String>();
  if (requestParamMap != null && requestParamMap.containsKey(key)) {
   for (RequestParameter rsParamMap : requestParamMap.get(key)) {
    valueList.add(rsParamMap.getString());
   }
  }
  return valueList;
 }
 
 /**
  * Gets the assets by hash tag.
  *
  * @param hashTags
  *         the hash tags
  * @param channels
  *         the channels
  * @param searchPath
  *         the search path
  * @param startIndex
  *         the start index
  * @param noOfRecords
  *         the no of records
  * @param resolver
  *         the resolver
  * @return the assets by hash tag
  */
 private List<Asset> getAssetsByHashTag(String[] hashTags, String[] channels, String searchPath, int startIndex, int noOfRecords,
   ResourceResolver resolver) {
  
  List<Asset> assetList = new ArrayList<Asset>();
  Map<String, String> map = getQueryStringMap(hashTags, channels, searchPath);
  
  Query query = builder.createQuery(PredicateGroup.create(map), resolver.adaptTo(Session.class));
  
  query.setStart(startIndex);
  query.setHitsPerPage(noOfRecords);
  
  SearchResult result = query.getResult();
  totalSize = result.getTotalMatches();
  
  // iterating over the results
  for (Hit hit : result.getHits()) {
   Asset asset = getAssetFromSearchHit(hit, resolver);
   if (asset != null) {
    assetList.add(asset);
   }
  }
  return assetList;
 }
 
 /**
  * Gets the query string map.
  *
  * @param hashTags
  *         the hash tags
  * @param channels
  *         the channels
  * @param searchPath
  *         the search path
  * @return the query string map
  */
 private Map<String, String> getQueryStringMap(String[] hashTags, String[] channels, String searchPath) {
  
  int i = 0;
  int j = 0;
  Map<String, String> map = new HashMap<String, String>();
  String[] channelsProperties = ArrayUtils.EMPTY_STRING_ARRAY;
  String[] hashTagProperties = ArrayUtils.EMPTY_STRING_ARRAY;
  
  Object channelsPropertiesObj = GlobalConfigurationUtility.getConfig(configAdmin, "channelPropNames", this.getClass().getName());
  Object hashTagPropertiesObj = GlobalConfigurationUtility.getConfig(configAdmin, "hashTagPropNames", this.getClass().getName());
  Object sortPropertiesObject = GlobalConfigurationUtility.getConfig(configAdmin, "sortPropName", this.getClass().getName());
  
  if (channelsPropertiesObj != null) {
   channelsProperties = (String[]) channelsPropertiesObj;
  }
  
  if (hashTagPropertiesObj != null) {
   hashTagProperties = (String[]) hashTagPropertiesObj;
  }
  
  map.put("path", searchPath);
  map.put("type", DamConstants.NT_DAM_ASSET);
  // combine this group with OR
  map.put("group.1_group.p.or", "true");
  // combine this group with OR
  map.put("group.2_group.p.or", "true");
  
  for (String hashTag : hashTags) {
   i++;
   hashTag = "#" + hashTag;
   for (String prop : hashTagProperties) {
    prop = getFormattedPropertyName(prop);
    
    map.put(GROUP_1 + String.valueOf(i) + "_property", METADATA_PATH + prop);
    map.put(GROUP_1 + String.valueOf(i) + "_property.value", "%" + hashTag + "%");
    map.put(GROUP_1 + String.valueOf(i) + "_property.operation", "like");
    
    i++;
   }
   
  }
  
  for (String channel : channels) {
   j++;
   for (String prop : channelsProperties) {
    prop = getFormattedPropertyName(prop);
    
    map.put(GROUP_2 + String.valueOf(j) + "_property", METADATA_PATH + prop);
    map.put(GROUP_2 + String.valueOf(j) + "_property.value", "%" + channel + "%");
    map.put(GROUP_2 + String.valueOf(j) + "_property.operation", "like");
    
    j++;
   }
  }
  
  if (sortPropertiesObject != null) {
   String sortProp = (String) sortPropertiesObject;
   sortProp = getFormattedPropertyName(sortProp);
   
   map.put("orderby", METADATA_PATH + sortProp);
   map.put("orderby.sort", "desc");
  }
  
  map.put("p.offset", "0");
  map.put("p.limit", "-1");
  
  return map;
 }
 
 private String getFormattedPropertyName(String prop) {
  return prop.trim().replaceAll("\\s", "_x0020_");
 }
 
 /**
  * Gets the JSON response from assets.
  *
  * @param assets
  *         the assets
  * @param page
  *         the page
  * @param resourceResolver
  *         the resource resolver
  * @param request
  *         the request
  * @return the JSON response from assets
  * @throws JSONException
  *          the JSON exception
  */
 private JSONArray getJSONResponseFromAssets(List<Asset> assets, Page page, ResourceResolver resourceResolver, SlingHttpServletRequest request)
   throws JSONException {
  JSONArray jsonArr = new JSONArray();
  if (assets != null && !assets.isEmpty()) {
   for (Asset asset : assets) {
    jsonArr.put(getJSONObjectFromAssetMap(DAMAssetUtility.getImageMetadataMapFromAsset(asset, page, resourceResolver, request)));
   }
  }
  return jsonArr;
 }
 
 /**
  * Gets the JSON object from asset map.
  *
  * @param assetMap
  *         the asset map
  * @return the JSON object from asset map
  * @throws JSONException
  *          the JSON exception
  */
 private JSONObject getJSONObjectFromAssetMap(Map<String, Object> assetMap) throws JSONException {
  JSONObject jsonObj = new JSONObject();
  if (assetMap != null && !assetMap.isEmpty()) {
   for (String key : assetMap.keySet()) {
    if (assetMap.get(key) instanceof Map<?, ?>) {
     JSONObject imageObject = getJSONObjectFromAssetMap((Map<String, Object>) assetMap.get(key));
     jsonObj.put("image", imageObject);
    } else {
     jsonObj.put(key, assetMap.get(key));
    }
   }
  }
  return jsonObj;
 }
 
 /**
  * Gets the asset from search hit.
  *
  * @param hit
  *         the hit
  * @param resolver
  *         the resolver
  * @return the asset from search hit
  */
 private Asset getAssetFromSearchHit(Hit hit, ResourceResolver resolver) {
  Asset asset = null;
  String path = StringUtils.EMPTY;
  try {
   path = hit.getPath();
   if (StringUtils.isNotBlank(path)) {
    asset = resolver.getResource(path).adaptTo(Asset.class);
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in getting asset from search result : ", e);
  }
  return asset;
 }
}
