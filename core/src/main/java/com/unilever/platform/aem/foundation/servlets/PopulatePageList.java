/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.jose4j.json.internal.json_simple.JSONArray;
import org.jose4j.json.internal.json_simple.JSONObject;
import org.jose4j.json.internal.json_simple.parser.JSONParser;
import org.jose4j.json.internal.json_simple.parser.ParseException;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class PopulatePageList.
 */
@SlingServlet(paths = { "/bin/content/projects/tasks/pageList" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.PopulatePageList", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "persist the pages Data when task is updated", propertyPrivate = false) })
public class PopulatePageList extends SlingAllMethodsServlet {
 /**
     * 
     */
 private static final long serialVersionUID = 1L;
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PopulatePageList.class);
 /** The Constant ONE. */
 private static final int ONE = 1;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  ResourceResolver resourceResolver = null;
  StringBuilder requestObjectBuilder = new StringBuilder();
  String line = null;
  try {
   BufferedReader reader = request.getReader();
   while ((line = reader.readLine()) != null) {
    requestObjectBuilder.append(line);
   }
  } catch (IOException e) {
   LOGGER.error("Error while reading request", e.getMessage() + ", Exception Trace:" + e);
  }
  try {
   resourceResolver = request.getResource().getResourceResolver();
  } catch (Exception e) {
   LOGGER.error("Error while getting resourceResolver", e.getMessage() + ", Exception Trace:" + e);
  }
  String requestObjectString = requestObjectBuilder.toString();
  if (StringUtils.isNotBlank(requestObjectString)) {
   requestObjectString = getValueWithoutQuote(requestObjectString, requestObjectString.length(), ONE, ONE);
   String newrequestObjectString = "{" + requestObjectString + "}";
   JSONParser parser = new JSONParser();
   try {
    JSONObject json = (JSONObject) parser.parse(newrequestObjectString);
    JSONArray pageList = (JSONArray) json.get("pageList");
    String taskPath = ((Object) json.get("taskPath")).toString();
    
    Resource currentResource = resourceResolver.getResource(taskPath);
    setConfiguration(currentResource, "pageList", pageList);
   } catch (ParseException | RepositoryException e) {
    LOGGER.error("Error while reading request", e.getMessage() + ", Exception Trace:" + e);
   }
  }
 }
 
 private void setConfiguration(Resource currentResource, String pageListProperty, JSONArray pageList) throws RepositoryException {
  Node catalogNode = (currentResource != null) ? currentResource.adaptTo(Node.class) : null;
  if (catalogNode != null) {
   String pageListArray = "";
   for (int i = 0; i < pageList.size(); i++) {
    String page = (String) pageList.get(i);
    if (("").equals(pageListArray)) {
     pageListArray = page;
    } else {
     pageListArray = pageListArray + "," + page;
    }
   }
   catalogNode.setProperty(pageListProperty, pageListArray);
   catalogNode.save();
  }
  
 }
 
 /**
  * Gets the value without quote.
  * 
  * @param value
  *         the value
  * @param length
  *         the length
  * @param withOutQuoteIndexFromStart
  *         the start index
  * @param withOutQuoteIndexFromLast
  *         the end index
  * @return the value without quote
  */
 private String getValueWithoutQuote(String value, int length, int withOutQuoteIndexFromStart, int withOutQuoteIndexFromLast) {
  String updatedValue = StringUtils.EMPTY;
  if (length > TWO) {
   updatedValue = value.substring(withOutQuoteIndexFromStart, length - withOutQuoteIndexFromLast);
  }
  return updatedValue;
 }
}
