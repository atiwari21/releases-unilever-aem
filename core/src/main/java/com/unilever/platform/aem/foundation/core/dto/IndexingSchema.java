/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

/**
 * The Class IndexingSchema.
 */
public class IndexingSchema {
 
 /** The Constant BODYCONTENT. */
 public static final String BODYCONTENT = "BodyContent";
 
 /** The Constant DESCRIPTION. */
 public static final String DESCRIPTION = "Description";
 
 /** The Constant ID. */
 public static final String ID = "id";
 
 /** The Constant PUBLISH_DATE. */
 public static final String PUBLISH_DATE = "LaunchDate";
 
 /** The Constant TITLE. */
 public static final String TITLE = "Title";
 
 /** The Constant PAGEURL. */
 public static final String PAGEURL = "PageUrl";
 
 /** The Constant IMAGEURL. */
 public static final String IMAGEURL = "ImageUrl";
 
 /** The Constant CONTENTTYPE. */
 public static final String CONTENTTYPE = "ContentType";
 
 /** The Constant SUBCONTENTTYPE. */
 public static final String SUBCONTENTTYPE = "SubContentType";
 
 /** The Constant BREADCRUMB. */
 public static final String BREADCRUMB = "BreadCrumb";
 
 /** The Constant PRODUCTID. */
 public static final String PRODUCTID = "ProductSKU";
 
 /** The Constant BRANDNAME. */
 public static final String BRANDNAME = "BrandName";
 
 /** The Constant BRANDLOCALE. */
 public static final String BRANDLOCALE = "BrandLocale";
 
 /** The Constant LOCALE. */
 public static final String LOCALE = "Locale";
 
 /** The Constant TAGS. */
 public static final String TAGS = "Tags";
 
 /** The Constant IDENTIFIERTYPE. */
 public static final String IDENTIFIERTYPE = "ProductIdentifierValue";
 
 /** The Constant IDENTIFIERVALUE. */
 public static final String IDENTIFIERVALUE = "ProductIdentifierType";
 
 /** The Constant SHORTIDENTIFIERVALUE. */
 public static final String SHORTIDENTIFIERVALUE = "ProductShortIdentifierValue";
 
 /** The Constant IMAGENAME. */
 public static final String IMAGENAME = "ImageName";
 
 /** The Constant IMAGEEXTENSION. */
 public static final String IMAGEEXTENSION = "ImageExtension";
 
 /** The Constant ISNOTADAPTIVEIMAGE. */
 public static final String ISNOTADAPTIVEIMAGE = "isNotAdaptiveImage";
 
 /** The Constant IMAGETITLE. */
 public static final String IMAGETITLE = "ImageTitle";
 
 /** The Constant IMAGEPATH. */
 public static final String IMAGEPATH = "ImagePath";
 
 /** The Constant IMAGEALTTEXT. */
 public static final String IMAGEALTTEXT = "ImageAltText";
 
 public static final String TAGTITLE = "TagTitle";
 
 /** The Constant TEASER_COPY. */
 public static final String TEASER_COPY = "TeaserCopy";

 public static final String RELATED_VIDEOS = "isRelatedVideo";
 
 /**
  * Instantiates a new indexing schema.
  */
 private IndexingSchema() {
  
 }
 
}
