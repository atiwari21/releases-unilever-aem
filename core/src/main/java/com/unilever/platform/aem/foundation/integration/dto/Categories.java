/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Categories.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "category" })
public class Categories {
 
 /** The category. */
 @XmlElement(name = "Category")
 private List<Category> category;
 
 /**
  * Gets the category.
  *
  * @return the category
  */
 public List<Category> getCategory() {
  if (category == null) {
   category = new ArrayList<Category>();
  }
  return this.category;
 }
 
 /**
  * Sets the category.
  *
  * @param category
  *         the new category
  */
 public void setCategory(List<Category> category) {
  this.category = category;
 }
 
}
