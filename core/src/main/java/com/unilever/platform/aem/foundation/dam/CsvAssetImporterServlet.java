/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.util.Text;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.mime.MimeTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.text.csv.Csv;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class CsvAssetImporterServlet.
 */
@SlingServlet(label = "Unilver DAM - CSV to Asset Servlet", methods = { org.apache.sling.api.servlets.HttpConstants.METHOD_POST }, resourceTypes = {
  CommonConstants.DEFAULT_SERVLET_NAME }, selectors = { "damimport" }, extensions = { "json" })
public class CsvAssetImporterServlet extends SlingAllMethodsServlet {
 
 private static final String GLOBAL3 = "global";
 
 private static final String FILE_NAME = "File Name";
 
 private static final String GLOBAL2 = "Global";
 
 /** The Constant log. */
 private static final Logger LOGGER = LoggerFactory.getLogger(CsvAssetImporterServlet.class);
 
 /** The Constant TERMINATED. */
 public static final String TERMINATED = "_LINE_TERMINATED";
 
 /** The mime type service. */
 @Reference
 private MimeTypeService mimeTypeService;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  final JSONObject jsonResponse = new JSONObject();
  final Parameters params = new Parameters(request);
  
  if (params.getFile() != null) {
   
   final long start = System.currentTimeMillis();
   final Iterator<String[]> rows = this.getRowsFromCsv(params);
   
   try {
    // First row is property names
    
    // Get the required properties for this tool (source and dest)
    processColumn(request, response, jsonResponse, params, start, rows);
   } catch (RepositoryException e) {
    LOGGER.error("Could not save Assets to JCR", e);
    this.addMessage(jsonResponse, "Could not save assets. " + e.getMessage());
    response.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
   } catch (Exception e) {
    LOGGER.error("Could not process CSV import", e);
    this.addMessage(jsonResponse, "Could not process CSV import. " + e.getMessage());
    response.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
   }
  } else {
   LOGGER.error("Could not find CSV file in request.");
   this.addMessage(jsonResponse, "CSV file is missing");
   response.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
  }
  
  response.getWriter().print(jsonResponse.toString());
 }
 
 /**
  * Process column.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @param jsonResponse
  *         the json response
  * @param params
  *         the params
  * @param start
  *         the start
  * @param rows
  *         the rows
  * @throws CsvAssetImportException
  *          the csv asset import exception
  * @throws RepositoryException
  *          the repository exception
  * @throws PersistenceException
  *          the persistence exception
  * @throws InterruptedException
  *          the interrupted exception
  */
 private void processColumn(final SlingHttpServletRequest request, final SlingHttpServletResponse response, final JSONObject jsonResponse,
   final Parameters params, final long start, final Iterator<String[]> rows)
   throws CsvAssetImportException, RepositoryException, PersistenceException, InterruptedException {
  final String[] requiredProperties = new String[] { params.getRelSrcPathProperty()
    // ,params.getAbsTargetPathProperty()
  };
  
  // Get the columns from the first row of the CSV
  final Map<String, Column> columns = Column.getColumns(rows.next(), params.getMultiDelimiter(), params.getIgnoreProperties(), requiredProperties);
  
  // Process Asset row entries
  final List<String> result = new ArrayList<String>();
  final List<String> batch = new ArrayList<String>();
  final List<String> failures = new ArrayList<String>();
  final List<String> ignored = new ArrayList<String>();
  int failuresCount = 0;
  
  while (rows.hasNext()) {
   final String[] row = rows.next();
   
   LOGGER.debug("Processing row {}", Arrays.asList(row));
   
   try {
    String global = row[columns.get("Created For").getIndex()];
    String deleted = row[columns.get("Request to Delete").getIndex()];
    if (!"Deleted".equalsIgnoreCase(deleted)) {
     handleGlobal(request, params, columns, batch, row, global);
    } else {
     ignored.add("File Name :" + row[columns.get(FILE_NAME).getIndex()]);
    }
    
   } catch (FileNotFoundException e) {
    failuresCount++;
    failures.add("File Name ;" + row[columns.get(FILE_NAME).getIndex()] + " Error :" + e.getLocalizedMessage());
    LOGGER.error("Could not find file for row ", Arrays.asList(row), e);
   } catch (CsvAssetImportException e) {
    failuresCount++;
    failures.add("File Name ;" + row[columns.get(FILE_NAME).getIndex()] + " Error :" + e.getLocalizedMessage());
    LOGGER.error("Could not import the row due to ", e.getMessage(), e);
   }
   
   LOGGER.debug("Processed row {}", Arrays.asList(row));
   
   if (batch.size() % params.getBatchSize() == 0) {
    this.save(request.getResourceResolver(), params.getBatchSize());
    result.addAll(batch);
    batch.clear();
    
    // Throttle saves
    if (params.getThrottle() > 0) {
     LOGGER.info("Throttling CSV Asset Importer batch processing for {} ms", params.getThrottle());
     Thread.sleep(params.getThrottle());
    }
   }
  }
  
  // Final save to catch any non-modulo stragglers; will only invoke persist if there are changes
  this.save(request.getResourceResolver(), params.getBatchSize());
  result.addAll(batch);
  
  LOGGER.info("Imported as TOTAL of [ {} ] assets in {} ms", result.size(), System.currentTimeMillis() - start);
  
  try {
   jsonResponse.put("assets", result);
   jsonResponse.put("ignoredDeleted", ignored);
   jsonResponse.put("failuresCount", failuresCount);
   jsonResponse.put("failures", failures);
   
  } catch (JSONException e) {
   LOGGER.error("Could not serialized Excel Importer results into JSON", e);
   this.addMessage(jsonResponse, "Could not serialized Excel Importer results into JSON");
   response.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
  }
 }
 
 /**
  * @param request
  * @param params
  * @param columns
  * @param batch
  * @param row
  * @param global
  * @throws FileNotFoundException
  * @throws RepositoryException
  * @throws PersistenceException
  * @throws CsvAssetImportException
  */
 private void handleGlobal(final SlingHttpServletRequest request, final Parameters params, final Map<String, Column> columns,
   final List<String> batch, final String[] row, String global)
   throws FileNotFoundException, RepositoryException, PersistenceException, CsvAssetImportException {
  if (global.contains(GLOBAL2)) {
   batch.add(this.importAsset(request.getResourceResolver(), params, columns, row, GLOBAL3));
  } else {
   String[] countries = (String[]) columns.get("Created For Country(ies)").getMultiData(row[columns.get("Created For Country(ies)").getIndex()]);
   
   for (String country : countries) {
    batch.add(this.importAsset(request.getResourceResolver(), params, columns, row, country));
   }
  }
 }
 
 /**
  * Interrogates the Request parameters and returns a prepared and parsed set of rows from the CSV file.
  *
  * @param params
  *         the Request parameters
  * @return The rows from the uploaded CSV file
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private Iterator<String[]> getRowsFromCsv(final Parameters params) throws IOException {
  final Csv csv = new Csv();
  InputStream is = params.getFile();
  
  if (params.getDelimiter() != null) {
   LOGGER.debug("Setting Field Delimiter to [ {} ]", params.getDelimiter());
   csv.setFieldDelimiter(params.getDelimiter());
  }
  
  if (params.getSeparator() != null) {
   LOGGER.debug("Setting Field Separator to [ {} ]", params.getSeparator());
   csv.setFieldSeparatorRead(params.getSeparator());
  }
  
  // Hack to prevent empty-value ending lines from breaking
  is = this.terminateLines(is, params.getSeparator() != null ? params.getSeparator() : csv.getFieldSeparatorRead(), params.getCharset());
  
  return csv.read(is, params.getCharset());
 }
 
 /**
  * Import the row of data into the DAM.
  *
  * @param resourceResolver
  *         the resource
  * @param params
  *         the CSV Asset Importer params
  * @param columns
  *         the Columns of the CSV
  * @param row
  *         the row data
  * @param country
  *         the country
  * @return the path of the imported Asset
  * @throws FileNotFoundException
  *          the file not found exception
  * @throws RepositoryException
  *          the repository exception
  * @throws PersistenceException
  *          the persistence exception
  * @throws CsvAssetImportException
  *          the csv asset import exception
  */
 private String importAsset(final ResourceResolver resourceResolver, final Parameters params, final Map<String, Column> columns, final String[] row,
   String country) throws FileNotFoundException, RepositoryException, PersistenceException, CsvAssetImportException {
  
  try {
   // Get, create or move the asset in the JCR
   final Asset asset = this.getOrCreateAsset(resourceResolver, params, columns, row, country);
   LOGGER.debug("Imported asset: {}", asset.getPath());
   // Add/Update/Delete the asset's properties in the JCR
   this.updateProperties(columns, row, params.getIgnoreProperties(), asset, country);
   LOGGER.debug("Updated properties on asset: {}", asset.getPath());
   
   // Return the asset path
   return asset.getPath();
  } catch (Exception e) {
   throw new CsvAssetImportException("Could not import row", e);
  }
 }
 
 /**
  * Updates the Metadata Properties of the Asset.
  *
  * @param columns
  *         the Columns of the CSV
  * @param row
  *         the row data
  * @param ignoreProperties
  *         Properties which to ignore when persisting property values to the Asset
  * @param asset
  *         the Asset to persist the data to
  * @param country
  *         the country
  */
 private void updateProperties(final Map<String, Column> columns, final String[] row, final String[] ignoreProperties, final Asset asset,
   final String country) {
  final ModifiableValueMap properties = this.getMetadataProperties(asset);
  
  // Copy properties
  for (final Map.Entry<String, Column> entry : columns.entrySet()) {
   if (ArrayUtils.contains(ignoreProperties, entry.getKey()) || StringUtils.isBlank(entry.getKey())) {
    continue;
   }
   
   final Column column = entry.getValue();
   final String valueStr = row[column.getIndex()];
   
   if (StringUtils.isNotBlank(valueStr)) {
    if (column.isMulti()) {
     properties.put(entry.getKey(), column.getMultiData(valueStr));
     LOGGER.debug("Setting multi property [ {} ~> {} ]", entry.getKey(), Arrays.asList(column.getMultiData(valueStr)));
    } else {
     properties.put(entry.getKey(), column.getData(valueStr));
     LOGGER.debug("Setting property [ {} ~> {} ]", entry.getKey(), column.getData(valueStr));
    }
   } else {
    if (properties.containsKey(entry.getKey())) {
     properties.remove(entry.getKey());
     LOGGER.debug("Removing property [ {} ]", entry.getKey());
    }
   }
  }
  properties.put("damCreatedCountry", country);
 }
 
 /**
  * Gets an existing Asset to update or creates a new Asset.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param params
  *         the CSV Asset Importer params
  * @param columns
  *         the Columns of the CSV
  * @param row
  *         a row in the CSV
  * @param country
  *         the country
  * @return the Asset
  * @throws FileNotFoundException
  *          the file not found exception
  * @throws RepositoryException
  *          the repository exception
  * @throws CsvAssetImportException
  *          the csv asset import exception
  */
 private Asset getOrCreateAsset(final ResourceResolver resourceResolver, final Parameters params, final Map<String, Column> columns,
   final String[] row, String country) throws FileNotFoundException, RepositoryException, CsvAssetImportException {
  
  final AssetManager assetManager = resourceResolver.adaptTo(AssetManager.class);
  
  String uniqueId = null;
  if (StringUtils.isNotBlank(params.getUniqueProperty())) {
   uniqueId = row[columns.get(params.getUniqueProperty()).getIndex()];
  }
  
  final String srcPath = params.getFileLocation() + "/" + row[columns.get(params.getRelSrcPathProperty()).getIndex()];
  final String mimeType = this.getMimeType(params, columns, row);
  final String absTargetPath = getAbsPath(columns, row, country);
  
  Asset asset = null;
  
  if (StringUtils.isNotBlank(params.getUniqueProperty()) && StringUtils.isNotBlank(uniqueId)) {
   // Check for existing Assets
   asset = this.findExistingAsset(resourceResolver, absTargetPath, params.getUniqueProperty(), uniqueId, country);
  }
  
  final FileInputStream fileInputStream = new FileInputStream(srcPath);
  
  boolean createAsset = false;
  
  createAsset = processAsset(resourceResolver, params, columns, row, absTargetPath, asset, createAsset);
  
  if (createAsset) {
   // Create new Asset
   try {
    asset = assetManager.createAsset(absTargetPath, fileInputStream, mimeType, true);
   } catch (Exception e) {
    LOGGER.error("Could not create Asset at [ {} ]", absTargetPath);
    throw new CsvAssetImportException("Could not create Asset at [ " + absTargetPath + " ]", e);
   }
   LOGGER.info("Created new asset [ {} ]", asset.getPath());
  }
  
  return asset;
 }
 
 /**
  * @param resourceResolver
  * @param params
  * @param columns
  * @param row
  * @param absTargetPath
  * @param asset
  * @param createAsset
  * @return
  *
  */
 private boolean processAsset(final ResourceResolver resourceResolver, final Parameters params, final Map<String, Column> columns, final String[] row,
   final String absTargetPath, Asset asset1, boolean createAssetParam) throws FileNotFoundException, RepositoryException, CsvAssetImportException {
  Asset asset = asset1;
  boolean createAsset = createAssetParam;
  if (asset != null) {
   if (!params.isFullImport() && !StringUtils.equals(asset.getPath(), absTargetPath)) {
    // Moving the existing asset
    final Session session = resourceResolver.adaptTo(Session.class);
    
    if (!session.nodeExists(absTargetPath)) {
     JcrUtils.getOrCreateByPath(StringUtils.substringBeforeLast(absTargetPath, "/"),
       org.apache.sling.jcr.resource.JcrResourceConstants.NT_SLING_ORDERED_FOLDER, session);
    }
    
    session.move(asset.getPath(), absTargetPath);
    LOGGER.info("Moved asset from [ {} ~> {}]", asset.getPath(), absTargetPath);
    Asset newAsset = DamUtil.resolveToAsset(resourceResolver.getResource(absTargetPath));
    asset = newAsset;
   } else if (params.isFullImport()) {
    // Remove existing asset so it can be recreated
    asset.adaptTo(Resource.class).adaptTo(Node.class).remove();
    createAsset = true;
    LOGGER.info("Removed existing asset so it can be re-created");
   }
  } else {
   LOGGER.info("Existing asset could not be found at [ {} ]", absTargetPath);
   createAsset = true;
  }
  if (asset != null && asset.adaptTo(Resource.class).adaptTo(Node.class).getProperty("jcr:content/metadata/Last Modified Date").getDate()
    .compareTo((Calendar) columns.get("Last Modified Date").getData(row[columns.get("Last Modified Date").getIndex()])) != 0) {
   // Remove existing asset so it can be recreated
   asset.adaptTo(Resource.class).adaptTo(Node.class).remove();
   createAsset = true;
   LOGGER.info("Removed existing asset so it can be re-created");
  }
  return createAsset;
 }
 
 /**
  * @param columns
  * @param row
  * @param country
  * @return
  * @throws CsvAssetImportException
  */
 private String getAbsPath(final Map<String, Column> columns, final String[] row, String country) throws CsvAssetImportException {
  final String absTargetPath = "/content/dam/unilever" + getPathValue(row[columns.get("Brand Position").getIndex()]) + getPathValue(country)
    + getPathValue(row[columns.get("Asset Language").getIndex()]) + getPathValue(row[columns.get("Asset Type").getIndex()])
    + getPathValue(row[columns.get("Asset Sub-Type").getIndex()]) + getPathValue(row[columns.get("Category").getIndex()])
    + getPathValue(row[columns.get("Market").getIndex()]) + getPathValue(row[columns.get("Product Name").getIndex()]) + "/"
    + row[columns.get(FILE_NAME).getIndex()]
  
  ;
  
  if (StringUtils.endsWith(absTargetPath, "/")) {
   throw new CsvAssetImportException("Absolute path [ " + absTargetPath + " ] is to a folder, not a file. Skipping");
  }
  return absTargetPath;
 }
 
 /**
  * Gets the mimeType of the asset based on the filename of how it will be stored in the AEM DAM. The destination filename is used since source files
  * can be very messy (and may not even have extensions depending on export options)
  *
  * @param params
  *         the CSV Asset Importer params
  * @param columns
  *         the Columns of the CSV
  * @param row
  *         a row in the CSV
  * @return The mimeType or blank if one cannot be derived
  */
 private String getMimeType(final Parameters params, final Map<String, Column> columns, final String[] row) {
  String mimeType = "";
  final Column mimeTypeCol = columns.get(params.getMimeTypeProperty());
  
  if (mimeTypeCol != null) {
   mimeType = row[columns.get(params.getMimeTypeProperty()).getIndex()];
  }
  
  if (StringUtils.isNotBlank(mimeType)) {
   // Get the tar
   final Column destColumn = columns.get(params.getAbsTargetPathProperty());
   final String fileName = Text.getName(row[destColumn.getIndex()]);
   mimeType = mimeTypeService.getMimeType(fileName);
  }
  
  return mimeType;
 }
 
 /**
  * Search for an existing asset based on the uniqueId property and value.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param absTargetPath
  *         the absolute target path of the asset
  * @param uniquePropertyName
  *         the rel property to search for on the dam:AssetContent node
  * @param uniqueId
  *         the unique value
  * @param country
  *         the country
  * @return the Asset if one can be found, else null
  */
 private Asset findExistingAsset(final ResourceResolver resourceResolver, final String absTargetPath, final String uniquePropertyName,
   final String uniqueId, String country) {
  
  // First try the very fast check against the absolute target path for the Asset.
  // If this a repeat processing there is a good change this is where the Asset lives.
  
  final String absMetadataPath = absTargetPath + "/" + JcrConstants.JCR_CONTENT + "/" + DamConstants.METADATA_FOLDER;
  
  final Resource resource = resourceResolver.getResource(absMetadataPath);
  
  if (resource != null) {
   final ValueMap properties = resource.adaptTo(ValueMap.class);
   final String val = properties.get(uniquePropertyName, String.class);
   if (StringUtils.equals(val, uniqueId)) {
    LOGGER.debug("Found  Asset at [ {} ] with matching unique property value of [ {} ]", resource.getPath(), uniqueId);
    // Good news! Found the Asset at the absolute target path
    return DamUtil.resolveToAsset(resource);
   }
  }
  if (!country.contains(GLOBAL3)) {
   deleteGlobalifnotGlobal(uniquePropertyName, uniqueId, resourceResolver);
  }
  
  String query = getQuery(uniquePropertyName, uniqueId, country);
  
  return getAssetByQuery(resourceResolver, query);
 }
 
 /**
  * @param resourceResolver
  * @param query
  * @return
  */
 private Asset getAssetByQuery(final ResourceResolver resourceResolver, String query) {
  final Iterator<Resource> resourceIterator = resourceResolver.findResources(query, "JCR-SQL2");
  Asset asset = null;
  
  if (resourceIterator.hasNext()) {
   asset = DamUtil.resolveToAsset(resourceIterator.next());
  }
  while (resourceIterator.hasNext()) {
   Asset otherAsset = DamUtil.resolveToAsset(resourceIterator.next());
   try {
    if (otherAsset != null && otherAsset.adaptTo(Resource.class).adaptTo(Node.class) != null) {
     otherAsset.adaptTo(Resource.class).adaptTo(Node.class).remove();
    }
   } catch (Exception e) {
    LOGGER.error("Error while deleting existing duplicate asset {}", otherAsset.getPath(), e);
   }
  }
  return asset;
 }
 
 /**
  * @param uniquePropertyName
  * @param uniqueId
  * @param country
  * @return
  */
 private String getQuery(final String uniquePropertyName, final String uniqueId, String country) {
  // Could not find the resource by guessing it exists in the absolute target path, so resort of a query.
  // In AEM6 this property should have a supporting index so it is very fast.
  
  String query = "SELECT * FROM [dam:AssetContent] WHERE [" + DamConstants.METADATA_FOLDER + "/" + uniquePropertyName + "] = '" + uniqueId + "'";
  if (!country.contains(GLOBAL3)) {
   query = query + " and [metadata/damCreatedCountry] = '" + country + "'";
  }
  return query;
 }
 
 /**
  * Delete globalifnot global.
  *
  * @param uniquePropertyName
  *         the unique property name
  * @param uniqueId
  *         the unique id
  * @param country
  *         the country
  * @param resourceResolver
  *         the resource resolver
  */
 private void deleteGlobalifnotGlobal(String uniquePropertyName, String uniqueId, ResourceResolver resourceResolver) {
  String query = "SELECT * FROM [dam:AssetContent] WHERE [" + DamConstants.METADATA_FOLDER + "/" + uniquePropertyName + "] = '" + uniqueId + "'";
  query = query + " and [metadata/damCreatedCountry] = 'global'";
  final Iterator<Resource> resourceIterator = resourceResolver.findResources(query, "JCR-SQL2");
  
  while (resourceIterator.hasNext()) {
   Asset otherAsset = DamUtil.resolveToAsset(resourceIterator.next());
   try {
    if (otherAsset != null && otherAsset.adaptTo(Resource.class).adaptTo(Node.class) != null) {
     otherAsset.adaptTo(Resource.class).adaptTo(Node.class).remove();
    }
   } catch (Exception e) {
    LOGGER.error("Error while deleting existing duplicate asset {}", otherAsset.getPath(), e);
   }
  }
  
 }
 
 /**
  * Adds a populated terminating field to the ends of CSV entries. If the last entry in a CSV row is empty, the CSV library has difficulty
  * understanding that is the end of the row.
  *
  * @param is
  *         the CSV file as an inputstream
  * @param separator
  *         The field separator
  * @param charset
  *         The charset
  * @return An inputstream that is the same as is, but each line has a populated line termination entry
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private InputStream terminateLines(final InputStream is, final char separator, final String charset) throws IOException {
  
  final ByteArrayOutputStream baos = new ByteArrayOutputStream();
  final PrintStream printStream = new PrintStream(baos);
  
  final LineIterator lineIterator = IOUtils.lineIterator(is, charset);
  
  while (lineIterator.hasNext()) {
   String line = StringUtils.stripToNull(lineIterator.next());
   
   if (line != null) {
    line += separator + TERMINATED;
    printStream.println(line);
   }
  }
  
  return new ByteArrayInputStream(baos.toByteArray());
 }
 
 /**
  * Helper for saving changes to the JCR; contains timing logging.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param size
  *         the number of changes to save
  * @throws PersistenceException
  *          the persistence exception
  */
 private void save(final ResourceResolver resourceResolver, final int size) throws PersistenceException {
  if (resourceResolver.hasChanges()) {
   final long start = System.currentTimeMillis();
   resourceResolver.commit();
   LOGGER.info("Imported a BATCH of [ {} ] assets in {} ms", size, System.currentTimeMillis() - start);
  } else {
   LOGGER.debug("Nothing to save");
  }
 }
 
 /**
  * Gets the ModifiableValueMap for the Asset's metadata node.
  *
  * @param asset
  *         the asset to get the properties for
  * @return the ModifiableValueMap for the Asset's metadata node
  */
 private ModifiableValueMap getMetadataProperties(final Asset asset) {
  Resource assetResource = asset.adaptTo(Resource.class);
  Resource metadataResource = assetResource.getChild(JcrConstants.JCR_CONTENT + "/" + DamConstants.METADATA_FOLDER);
  return metadataResource.adaptTo(ModifiableValueMap.class);
 }
 
 /**
  * Helper method; adds a message to the JSON Response object.
  *
  * @param jsonObject
  *         the JSON object to add the message to
  * @param message
  *         the message to add.
  */
 private void addMessage(final JSONObject jsonObject, final String message) {
  try {
   jsonObject.put("message", message);
  } catch (JSONException e) {
   LOGGER.error("Could not formulate JSON Response", e);
  }
 }
 
 /**
  * Gets the path value.
  *
  * @param path
  *         the path
  * @return the path value
  */
 private String getPathValue(String path) {
  return (StringUtils.isEmpty(path) || "null".equalsIgnoreCase(path)) ? "" : "/" + JcrUtil.createValidName(path);
  
 }
}
