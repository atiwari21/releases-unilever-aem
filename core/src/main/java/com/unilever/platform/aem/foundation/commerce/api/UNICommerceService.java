/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.Product;

/**
 * The Interface UNICommerceService to implement commerce service.
 */
public interface UNICommerceService extends CommerceService {
 
 /**
  * Gets the product using product path /etc/commerce/products/unilever/dove/45/56/94/455694 ).
  *
  * @param productId
  *         the product path
  * @return the product
  */
 @Override
 public Product getProduct(String productId);
 
 /**
  * This method returns the UNI specific commerce session. Which will give the flexibility in concrete implementation of actual business requirements.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @return UNICommerceSession that extends the CommerceSession.
  * @throws CommerceException
  *          the commerce exception
  */
 @Override
 public UNICommerceSession login(SlingHttpServletRequest request, SlingHttpServletResponse response) throws CommerceException;
}
