/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.helper;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.mailer.MailService;
import com.day.cq.mailer.MailingException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.apache.sling.api.resource.LoginException;

/**
 * The Class MailHelper.
 */
public class MailHelper {
 
 /**
  * Initiates Mail Helper
  */
 private MailHelper() {
  
 }
 
 /**
  * Send mail.
  *
  * @param adminSession
  *         the admin session
  * @param mailer
  *         the mailer
  * @param sendTo
  *         the send to
  * @param parameters
  *         the parameters
  * @throws RepositoryException
  *          the repository exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * @throws LoginException
  *          the login exception
  * @throws EmailException
  *          the email exception
  * @throws MailingException
  *          the mailing exception
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 public static void sendMail(Session adminSession, MailService mailer, Map<String, String> sendTo, Map<String, String> parameters)
   throws RepositoryException, IOException, LoginException, EmailException, MailingException {
  String templateRootPath = (String) parameters.get("templateRootPath");
  
  Node node = adminSession.getNode(templateRootPath);
  InputStream is = node.getNode(JcrConstants.JCR_CONTENT).getProperty(JcrConstants.JCR_DATA).getBinary().getStream();
  
  Properties properties = new Properties();
  properties.load(is);
  
  Email email = null;
  
  email = new SimpleEmail();
  email.setCharset("UTF-8");
  
  Map headerMap = new HashMap();
  headerMap.put("Content-Type", "text/plain; charset=utf-8");
  
  email.setHeaders(headerMap);
  email.setSubject(getSubject(properties, parameters, ""));
  
  for (Map.Entry entry : sendTo.entrySet()) {
   email.addTo((String) entry.getValue());
   email.setMsg(getMessage(properties, parameters, (String) entry.getKey()));
   if (mailer != null) {
    mailer.send(email);
   }
   
   email.getToAddresses().clear();
  }
 }
 
 /**
  * Gets the subject.
  *
  * @param properties
  *         the properties
  * @param parameters
  *         the parameters
  * @param invitee
  *         the invitee
  * @return the subject
  * @throws RepositoryException
  *          the repository exception
  */
 private static String getSubject(Properties properties, Map<String, String> parameters, String invitee) throws RepositoryException {
  Properties values = getValues(parameters, invitee);
  return StrSubstitutor.replace(properties.get("subject"), values);
 }
 
 /**
  * Gets the message.
  *
  * @param properties
  *         the properties
  * @param parameters
  *         the parameters
  * @param invitee
  *         the invitee
  * @return the message
  * @throws RepositoryException
  *          the repository exception
  */
 private static String getMessage(Properties properties, Map<String, String> parameters, String invitee) throws RepositoryException {
  Properties values = getValues(parameters, invitee);
  StringBuilder msg = new StringBuilder();
  msg.append(StrSubstitutor.replace(properties.get("header"), values));
  msg.append(StrSubstitutor.replace(properties.get("message"), values));
  msg.append(StrSubstitutor.replace(properties.get("footer"), values));
  return msg.toString();
 }
 
 /**
  * Gets the values.
  *
  * @param parameters
  *         the parameters
  * @param invitee
  *         the invitee
  * @return the values
  * @throws RepositoryException
  *          the repository exception
  */
 @SuppressWarnings("rawtypes")
 private static Properties getValues(Map<String, String> parameters, String invitee) throws RepositoryException {
  Properties values = new Properties();
  for (Map.Entry entry : parameters.entrySet()) {
   values.setProperty((String) entry.getKey(), (String) entry.getValue());
  }
  values.setProperty("inviteeFirstName", invitee);
  values.setProperty("time", new Date().toString());
  return values;
 }
}
