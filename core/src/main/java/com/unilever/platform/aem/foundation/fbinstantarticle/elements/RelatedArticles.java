/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * The Class RelatedArticles.
 */
public class RelatedArticles extends InstantArticleElement implements Container {
 
 /** The items. */
 private List<RelatedItem> items = new LinkedList<RelatedItem>();
 
 /** The title. */
 private String title;
 
 /**
  * Gets the items.
  *
  * @return the items
  */
 public List<RelatedItem> getItems() {
  return items;
 }
 
 /**
  * Gets the title.
  *
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Instantiates a new related articles.
  */
 private RelatedArticles() {
 }
 
 /**
  * Creates the.
  *
  * @return the related articles
  */
 public static RelatedArticles create() {
  return new RelatedArticles();
 }
 
 /**
  * Adds the related.
  *
  * @param item
  *         the item
  * @return the related articles
  */
 public RelatedArticles addRelated(RelatedItem item) {
  this.items.add(item);
  return this;
 }
 
 /**
  * With title.
  *
  * @param title
  *         the title
  * @return the related articles
  */
 public RelatedArticles withTitle(String title) {
  this.title = title;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  List<Object> chidren = new ArrayList<Object>();
  for (RelatedItem item : items) {
   chidren.add(item);
  }
  return chidren;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#isValid()
  */
 public boolean isValid() {
  if (CollectionUtils.isEmpty(items)) {
   return false;
  }
  for (RelatedItem item : items) {
   if (!item.isValid()) {
    return false;
   }
  }
  
  return true;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement()
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  return toDOMElement(document, true);
 }
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }

 public Element toDOMElement(Document document, boolean isTitle) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element element = document.createElement("ul");
  element.attr("class", "op-related-articles");
  if (isTitle && StringUtils.isNotBlank(this.title)) {
   element.attr("title", this.title);
  }
  if(CollectionUtils.isNotEmpty(items) && items.size()>4){
   items=items.subList(0, 3);
  }
  for (RelatedItem item : items) {
   element.appendChild(item.toDOMElement(document));
  }
  
  return element;
 }
}
