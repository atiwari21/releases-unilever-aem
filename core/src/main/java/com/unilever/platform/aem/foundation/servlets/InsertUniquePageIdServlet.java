/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class InsertUniquePageIdServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "insertuniquepageid" }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.InsertUniquePageIdServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Insertes the uniquePageId property to the page node ", propertyPrivate = false) })
public class InsertUniquePageIdServlet extends SlingAllMethodsServlet {
 
 /**
     * 
     */
 private static final long serialVersionUID = -8592000388395734161L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(InsertUniquePageIdServlet.class);
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "text/html";
 
 /** The Constant JSON. */
 private static final String JSON = "json";
 
 /** The Constant SITE_PATH. */
 private static final String SITE_PATH = com.day.cq.wcm.api.NameConstants.PN_SITE_PATH;
 
 /** The Constant UPDATE_TYPE. */
 private static final String UPDATE_TYPE = "updateType";
 
 /** The Constant PAGE_PATH_LIST. */
 private static final String PAGE_PATH_LIST = "pagePathList";
 
 /** The Constant CHECKBOX_SPECIFIC_PAGES. */
 private static final String CHECKBOX_SPECIFIC_PAGES = "specificCheckbox";
 
 /** The Constant CHECKBOX_SPECIFIC_VALUE. */
 private static final String CHECKBOX_SPECIFIC_VALUE = "specificPage";
 
 /** The Constant HTML_FORM. */
 private static final String HTML_FORM = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>Update Properties"
   + "</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">       <table>         <tr>"
   + "                <td><label></label></td>                <td><input type=\"hidden\" name=\"updateType\" "
   + "value=\"dryRun\" id=\"updateType\"></td>         </tr>           <tr>                <td><label>Site Path*</label>"
   + "</td>               <td><input type=\"text\" name=\"sitePath\"></td>            </tr>           <tr> "
   + "               <td><input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\"></td>                <td>"
   + "<input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>           </tr>       </table>    </form>"
   + "</body><script>  function dryRun(){      var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }"
   + "   function execute(){     var updateTypeEle=document.getElementById(\"updateType\");      updateTypeEle.value=\"execute\";"
   + "        var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }</script></html>";
 
 /** The Constant HTML_FORM_NEW. */
 private static final String HTML_FORM_NEW = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\">"
   + "<title>Update Properties</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">"
   + "       <table>         <tr>                <td><label></label></td>                <td>"
   + "<input type=\"hidden\" name=\"updateType\" value=\"dryRun\" id=\"updateType\"></td>         </tr>           <tr>"
   + "                <td><label>Site Path*</label></td>               <td><input type=\"text\" name=\"sitePath\"></td>"
   + "            </tr>           <tr>                <td><input type=\"checkbox\" name=\"specificCheckbox\" value=\"specificPage\">"
   + "Update specific pages from Dry Run list? </td>            </tr>           <tr>                <td><label>"
   + "Provide pages from list obtained after Dry Run in Comma separated format</label></td>                <td>"
   + "<textarea rows=\"10\" cols=\"100\" name=\"pagePathList\"></textarea></td>           </tr>           <tr>"
   + "                <td><input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\"></td>                <td>"
   + "<input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>           </tr>       </table>    </form></body>"
   + "<script>  function dryRun(){      var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }   "
   + "function execute(){     var updateTypeEle=document.getElementById(\"updateType\");      updateTypeEle.value=\"execute\";"
   + "        var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }</script></html>";
 
 /** The count. */
 static int count = 1;
 
 /** The Constant UNIQUE_PAGE_ID. */
 public static final String UNIQUE_PAGE_ID = "uniquePageId";
 
 /** The Constant UNIQUE_PAGE_ID_VALUE. */
 public static final String UNIQUE_PAGE_ID_VALUE = "uniquePageId property's value";
 
 /** The Constant MAX. */
 private static final int MAX = 999;
 
 /** The Constant MIN. */
 private static final int MIN = 100;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant Node Path. */
 private static final String NODE_PATH = "Node Path";
 
 /** The Constant Page Path. */
 private static final String PAGE_PATH = "Page Path";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String configJSON = request.getParameter(JSON);
  LOGGER.info("configJSON in InsertUniquePageIdServlet {} ", configJSON);
  response.setContentType(CONTENT_TYPE);
  PrintWriter out = response.getWriter();
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  LOGGER.info("sitePath in InsertUniquePageIdServlet {} ", request.getParameter(SITE_PATH));
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = request.getParameter(SITE_PATH) != null ? pageManager.getPage(request.getParameter(SITE_PATH)) : null;
  String specificPage = request.getParameter(CHECKBOX_SPECIFIC_PAGES) != null ? request.getParameter(CHECKBOX_SPECIFIC_PAGES) : StringUtils.EMPTY;
  if (page == null) {
   out.write("<b>" + "Please provide the valid site path" + "</b>");
   out.append(HTML_FORM);
  } else if (page != null) {
   try {
    String updateType = request.getParameter(UPDATE_TYPE) != null ? request.getParameter(UPDATE_TYPE) : StringUtils.EMPTY;
    Map<String, String> map = new LinkedHashMap<String, String>();
    StringBuilder successMessage = new StringBuilder("");
    setNestedChildResourceList(page, map);
    if ("dryRun".equals(updateType)) {
     dryRunUpdate(out, map);
    } else if (specificPage.equals(CHECKBOX_SPECIFIC_VALUE)) {
     specificPagesUpdate(resourceResolver, out, pageManager, successMessage, request);
    } else if (updatePropertyNames(map, resourceResolver, successMessage)) {
     out.write("Below Nodes are updated Successfully" + "<br/><br/>");
     out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>"
       + "<th>" + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessage.toString() + "</table>");
     LOGGER.info(successMessage.toString());
     resourceResolver.commit();
    }
   } catch (Exception e) {
    out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
    LOGGER.error("Error in updating the content", e);
    resourceResolver.revert();
   }
   
  }
  out.flush();
  out.close();
 }
 
 /**
  * Dry run update.
  *
  * @param out
  *         the out
  * @param map
  *         the map
  */
 private void dryRunUpdate(PrintWriter out, Map<String, String> map) {
  out.append(HTML_FORM_NEW);
  int dryRunCount = 1;
  StringBuilder successMessageDryRun = new StringBuilder("");
  Iterator<Entry<String, String>> mapIterator = map.entrySet().iterator();
  while (mapIterator.hasNext()) {
   String nodePath = mapIterator.next().getKey();
   String[] parts = nodePath.split("/jcr:content/", TWO);
   String pagPath = parts[0];
   successMessageDryRun.append("<tr>" + "<td>" + dryRunCount + ". " + "</td>" + "<td>" + pagPath + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
   dryRunCount++;
  }
  out.write("<h2 style=" + "'color:red;'" + ">" + "Before clicking Execute Button, Please take the backup of all the page that you want to update"
    + "</h2>" + "<br/>");
  out.write("The below pages do not have " + "<b>" + UNIQUE_PAGE_ID + "</b>" + " and " + "<b>" + UNIQUE_PAGE_ID_VALUE + "</b>" + "<br/><br/>");
  out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>" + "<th>"
    + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessageDryRun.toString() + "</table>");
  LOGGER.info(successMessageDryRun.toString());
  
 }
 
 /**
  * Specific pages update.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param out
  *         the out
  * @param pageManager
  *         the page manager
  * @param successMessage
  *         the success message
  * @param request
  *         the request
  */
 private void specificPagesUpdate(ResourceResolver resourceResolver, PrintWriter out, PageManager pageManager, StringBuilder successMessage,
   SlingHttpServletRequest request) {
  
  String pagesToBeUpdatedList = request.getParameter(PAGE_PATH_LIST) != null ? request.getParameter(PAGE_PATH_LIST) : StringUtils.EMPTY;
  pagesToBeUpdatedList = pagesToBeUpdatedList.replaceAll("\\s+", "");
  if (!pagesToBeUpdatedList.isEmpty()) {
   String[] pagesToBeUpdatedArr = pagesToBeUpdatedList.split(",");
   for (int i = 0; i < pagesToBeUpdatedArr.length; i++) {
    Page newPage = pageManager.getPage(pagesToBeUpdatedArr[i]);
    if (newPage != null) {
     try {
      Map<String, String> mapNew = new LinkedHashMap<String, String>();
      setNestedChildResourceList(newPage, mapNew);
      updatePropertyNames(mapNew, resourceResolver, successMessage);
     } catch (Exception e) {
      out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
      LOGGER.error("Error in updating the content", e);
      resourceResolver.revert();
     }
    } else {
     out.write("Please provide the valid site path");
     out.append(HTML_FORM_NEW);
    }
   }
   out.write("Below Nodes are updated Successfully" + "<br/><br/>");
   out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>"
     + "<th>" + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessage.toString() + "</table>");
   LOGGER.info(successMessage.toString());
   try {
    resourceResolver.commit();
   } catch (PersistenceException e) {
    LOGGER.error("PersistenceException in specificPagesUpdate method:", e);
   }
  } else {
   out.write("<b>" + "Please provide the page path from Dry Run List if specific pages need to be updated" + "</b>");
   out.append(HTML_FORM_NEW);
  }
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
 
 /**
  * set resource list and resource map.
  *
  * @param res
  *         the res
  * @param map
  *         the list
  */
 private void setNestedChildResourceList(Page parentPage, Map<String, String> map) {
  LOGGER.debug("inside setNestedChildResourceList of InsertUniquePageIdServlet");
  if (null != parentPage) {
   if (!parentPage.getProperties().containsKey(UNIQUE_PAGE_ID)) {
    map.put(parentPage.getPath(), parentPage.getContentResource().toString());
   } else if (StringUtils.isEmpty(parentPage.getProperties().get(UNIQUE_PAGE_ID).toString())) {
    map.put(parentPage.getPath(), parentPage.getContentResource().toString());
   }
   Iterator<Page> itr = parentPage.listChildren();
   while (itr.hasNext()) {
    Page childPage = itr.next();
    if (!childPage.getProperties().containsKey(UNIQUE_PAGE_ID)) {
     map.put(childPage.getPath(), childPage.getContentResource().toString());
    } else if (StringUtils.isEmpty(childPage.getProperties().get(UNIQUE_PAGE_ID).toString())) {
     map.put(childPage.getPath(), childPage.getContentResource().toString());
    }
    setNestedChildResourceList(childPage, map);
   }
  }
 }
 
 /**
  * Update page properties.
  *
  * @param map
  *         the map
  * @param resourceResolver
  *         the resource resolver
  * @param successMessage
  *         the success message
  * @return true, if successful
  * @throws RepositoryException
  *          the repository exception
  */
 public static boolean updatePropertyNames(Map<String, String> map, ResourceResolver resourceResolver, StringBuilder successMessage)
   throws RepositoryException {
  LOGGER.debug("inside updatePagePropertyNames of InsertUniquePageIdServlet");
  Iterator<String> itr = map.keySet().iterator();
  while (itr.hasNext()) {
   String nodePath = itr.next();
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page page = pageManager.getContainingPage(nodePath);
   if (page != null) {
    Resource resource = page.getContentResource();
    Node node = resource.adaptTo(Node.class);
    if (!page.getProperties().containsKey(UNIQUE_PAGE_ID)) {
     node.setProperty(UNIQUE_PAGE_ID, getUniquePageId(page.getPath().hashCode()));
     successMessage.append("<tr>" + "<td>" + count + ". " + "</td>" + "<td>" + nodePath + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
     count++;
    } else if (StringUtils.isEmpty(page.getProperties().get(UNIQUE_PAGE_ID).toString())) {
     node.setProperty(UNIQUE_PAGE_ID, getUniquePageId(page.getPath().hashCode()));
     successMessage.append("<tr>" + "<td>" + count + ". " + "</td>" + "<td>" + nodePath + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
     count++;
    }
   }
  }
  return true;
 }
 
 /**
  * Gets the uniqueValue string.
  *
  * @param hashCode
  *         the hashCode
  * @return the uniqueValue
  */
 private static String getUniquePageId(int hashCode) {
  String uniqueValue = StringUtils.EMPTY;
  SecureRandom rand = new SecureRandom();
  int randomNum = MIN + rand.nextInt((MAX - MIN) + 1);
  uniqueValue = String.valueOf(Math.abs(hashCode)) + randomNum;
  return uniqueValue;
 }
 
}
