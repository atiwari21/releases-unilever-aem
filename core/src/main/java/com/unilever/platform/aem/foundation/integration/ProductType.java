/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.List;

/**
 * The Class ProductType.
 */
public class ProductType {
 
 /** The external id. */
 protected String productId;
 
 private boolean active;
 
 /** The name. */
 protected String name;
 
 /** The description. */
 protected String description;
 
 /** The category detail. */
 protected List<CategoryDetail> categoryDetail;
 
 /** The product page url. */
 protected String productPageUrl;
 
 /** The image url. */
 protected String imageUrl;
 
 /** The identifier. */
 protected Identifier identifier;
 /** The categoryId. */
 protected String categoryId;
 /** The productVariants. */
 protected List<ProductVariantType> productVariants;
 
 /**
  * Gets the value of the externalId property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getProductId() {
  return productId;
 }
 
 /**
  * Sets the value of the externalId property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setProductId(String value) {
  this.productId = value;
 }
 
 /**
  * Gets the value of the name property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getName() {
  return name;
 }
 
 /**
  * Sets the value of the name property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setName(String value) {
  this.name = value;
 }
 
 /**
  * Gets the value of the description property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getDescription() {
  return description;
 }
 
 /**
  * Sets the value of the description property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setDescription(String value) {
  this.description = value;
 }
 
 /**
  * Gets the value of the productPageUrl property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getProductPageUrl() {
  return productPageUrl;
 }
 
 /**
  * Sets the value of the productPageUrl property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setProductPageUrl(String value) {
  this.productPageUrl = value;
 }
 
 /**
  * Gets the value of the imageUrl property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getImageUrl() {
  return imageUrl;
 }
 
 /**
  * Sets the value of the imageUrl property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setImageUrl(String value) {
  this.imageUrl = value;
 }
 
 /**
  * Gets the category detail.
  *
  * @return the category detail
  */
 public List<CategoryDetail> getCategoryDetail() {
  return categoryDetail;
 }
 
 /**
  * Sets the category detail.
  *
  * @param categoryDetail
  *         the new category detail
  */
 public void setCategoryDetail(List<CategoryDetail> categoryDetail) {
  this.categoryDetail = categoryDetail;
 }
 
 /**
  * Gets the identifier.
  *
  * @return the identifier
  */
 public Identifier getIdentifier() {
  return identifier;
 }
 
 /**
  * Sets the identifier.
  *
  * @param identifier
  *         the new identifier
  */
 public void setIdentifier(Identifier identifier) {
  this.identifier = identifier;
 }
 
 /**
  * @return the categoryId
  */
 public String getCategoryId() {
  return categoryId;
 }
 
 /**
  * @param categoryId
  *         the categoryId to set
  */
 public void setCategoryId(String categoryId) {
  this.categoryId = categoryId;
 }
 
 /**
  * @return the productVariants
  */
 public List<ProductVariantType> getProductVariants() {
  return productVariants;
 }
 
 /**
  * @param productVariants
  *         the productVariants to set
  */
 public void setProductVariants(List<ProductVariantType> productVariants) {
  this.productVariants = productVariants;
 }
 
 public boolean isActive() {
  return active;
 }
 
 public void setActive(boolean active) {
  this.active = active;
 }
 
}
