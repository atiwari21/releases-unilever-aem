/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.jose4j.json.internal.json_simple.JSONArray;
import org.jose4j.json.internal.json_simple.JSONObject;
import org.jose4j.json.internal.json_simple.parser.JSONParser;
import org.jose4j.json.internal.json_simple.parser.ParseException;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class BrandConfigurationData.
 */
@SlingServlet(paths = { "/bin/content/projects/unilever" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.BrandConfigurationData", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the preview agent to be enabled ", propertyPrivate = false) })
public class BrandConfigurationData extends SlingAllMethodsServlet {
 
 /**
     * 
     */
 private static final long serialVersionUID = 1L;
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(BrandConfigurationData.class);
 
 /** The Constant ONE. */
 private static final int ONE = 1;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  
  StringBuilder requestObjectBuilder = new StringBuilder();
  String line = null;
  try {
   BufferedReader reader = request.getReader();
   while ((line = reader.readLine()) != null) {
    requestObjectBuilder.append(line);
   }
  } catch (Exception e) {
   LOGGER.error("Error while reading request", e);
  }
  
  String requestObjectString = requestObjectBuilder.toString();
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  if (StringUtils.isNotBlank(requestObjectString)) {
   requestObjectString = getValueWithoutQuote(requestObjectString, requestObjectString.length(), ONE, ONE);
   String newrequestObjectString = "{" + requestObjectString + "}";
   JSONParser parser = new JSONParser();
   try {
    JSONObject json = (JSONObject) parser.parse(newrequestObjectString);
    JSONArray records = (JSONArray) json.get("record");
    Object jsonName = (Object) json.get("jsonName");
    Object projectTitle = (Object) json.get("projectTitle");
    String newJsonName = StringUtils.EMPTY;
    String title = StringUtils.EMPTY;
    String configurationName = StringUtils.EMPTY;
    if (jsonName != null) {
     newJsonName = jsonName.toString();
    }
    if (projectTitle != null) {
     title = projectTitle.toString();
    }
    if (newJsonName != null) {
     configurationName = newJsonName.substring(TWO);
    }
    
    String folderName = getDateFolder();
    Resource currentResource = resourceResolver.getResource("/content/projects/" + folderName + "/" + title + "/jcr:content");
    if (currentResource == null) {
     currentResource = resourceResolver.getResource("/content/projects/" + folderName + "/" + title.replaceAll("-", "") + "/jcr:content");
    }
    
    setConfiguration(currentResource, configurationName, records);
    
   } catch (ParseException | RepositoryException e) {
    LOGGER.error("Error while setting brandConfiguration node", e);
   }
   
  }
 }
 
 private String getDateFolder() {
  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
  Date date = new Date();
  String systemDate = dateFormat.format(date);
  String[] dateSplit = systemDate.split("/");
  return dateSplit[0] + dateSplit[1] + dateSplit[TWO];
  
 }
 
 private void setConfiguration(Resource currentResource, String configurationName, JSONArray records) throws RepositoryException {
  
  Node catalogNode = (currentResource != null) ? currentResource.adaptTo(Node.class) : null;
  if (catalogNode != null) {
   String[] recordsArray = new String[records.size()];
   for (int i = 0; i < records.size(); i++) {
    JSONObject record = (JSONObject) records.get(i);
    String recordString = record.toString();
    recordsArray[i] = recordString;
   }
   catalogNode.setProperty(configurationName, recordsArray);
   catalogNode.save();
  }
 }
 
 /**
  * Gets the value without quote.
  * 
  * @param value
  *         the value
  * @param length
  *         the length
  * @param startIndex
  *         the start index
  * @param endIndex
  *         the end index
  * @return the value without quote
  */
 private String getValueWithoutQuote(String value, int length, int withOutQuoteIndexFromStart, int withOutQuoteIndexFromLast) {
  String updatedValue = StringUtils.EMPTY;
  if (length > TWO) {
   updatedValue = value.substring(withOutQuoteIndexFromStart, length - withOutQuoteIndexFromLast);
  }
  return updatedValue;
 }
 
}
