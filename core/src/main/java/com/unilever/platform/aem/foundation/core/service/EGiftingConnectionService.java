/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

/**
 * The Interface EGiftingConnectionService.
 */
public interface EGiftingConnectionService {
 
 /**
  * Gets the post entity url.
  *
  * @return the post entity url
  */
 public String getPostEntityURL();
 
 /**
  * Gets the client token url.
  *
  * @return the client token url
  */
 public String getClientTokenURL();
 
 /**
  * Gets the generate pdfurl.
  *
  * @return the generate pdfurl
  */
 public String getGeneratePDFURL();
 
 /**
  * Gets the timeout time.
  *
  * @return the timeout time
  */
 public int getTimeoutTime();
}
