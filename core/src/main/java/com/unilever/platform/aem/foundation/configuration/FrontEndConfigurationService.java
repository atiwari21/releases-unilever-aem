/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.util.Map;

import org.osgi.service.component.ComponentContext;

import com.day.cq.wcm.api.Page;
import com.google.common.cache.LoadingCache;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Interface FrontEndConfigurationService.
 */
public interface FrontEndConfigurationService {
 
 /**
  * Activates
  *
  * @param ctx
  *         the component context
  */
 public void activate(ComponentContext ctx);
  
 /**
  * Gets Cache Object.
  */
 public LoadingCache<String, Map<String, String>> getConfigCache();
 
 /**
  * Invalidate cache.
  */
 public void inValidateCache();
 
 /**
  * Invalidate cache.
  *
  * @param key the key
  */
 public void inValidateCache(String key);
 
}
