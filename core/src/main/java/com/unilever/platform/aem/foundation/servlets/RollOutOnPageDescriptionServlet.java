/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * This class sets the targer page decsription to page description of blueprint page in case of roll out.
 */
@SlingServlet(paths = { "/bin/pageProperties/description" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.RollOutOnPageDescriptionServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "trigger rollout of target page descrition with blueprint page description", propertyPrivate = false) })
public class RollOutOnPageDescriptionServlet extends SlingAllMethodsServlet {
 
 private static final String CQ_PROPERTY_INHERITANCE_CANCELLED = "cq:propertyInheritanceCancelled";
 
 private static final String ROLLOUT_PAGE_PATHS = "rolloutPagePaths";
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -3297291563570366315L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RollOutOnPageDescriptionServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String rolloutPagePathParam = request.getParameter(ROLLOUT_PAGE_PATHS);
  String[] rollOutPagePaths = StringUtils.isNotBlank(rolloutPagePathParam) ? StringUtils.split(rolloutPagePathParam, CommonConstants.COMMA)
    : new String[] {};
  ResourceResolver resourceResolver = request.getResourceResolver();
  if (rollOutPagePaths.length > CommonConstants.ONE) {
   String blueprintPath = rollOutPagePaths[CommonConstants.ZERO];
   LOGGER.info("blueprint path is" + blueprintPath);
   String targetPath = rollOutPagePaths[CommonConstants.ONE];
   LOGGER.info("target path is" + targetPath);
   Resource targetPathresource = (resourceResolver != null)
     ? resourceResolver.getResource(targetPath + CommonConstants.SLASH_CHAR_LITERAL + JcrConstants.JCR_CONTENT) : null;
   PageManager pageManager = (resourceResolver != null) ? resourceResolver.adaptTo(PageManager.class) : null;
   Page bluePrintPage = (pageManager != null && StringUtils.isNotBlank(blueprintPath)) ? pageManager.getContainingPage(blueprintPath) : null;
   String blueprintPageDescription = (bluePrintPage != null) ? bluePrintPage.getDescription() : null;
   LOGGER.debug("blueprint page description is " + blueprintPageDescription);
   Node targetPathNode = (targetPathresource != null) ? targetPathresource.adaptTo(Node.class) : null;
   setTargetPageDescription(resourceResolver, targetPathNode, blueprintPageDescription);
  }
  if (resourceResolver != null) {
   resourceResolver.close();
  }
 }
 
 /**
  * sets target page description in case of roll out
  * 
  * @param resourceResolver
  * @param targetPathresource
  * @param blueprintPageDescription
  */
 private void setTargetPageDescription(ResourceResolver resourceResolver, Node targetPathNode, String blueprintPageDescription) {
  if (StringUtils.isNotBlank(blueprintPageDescription) && targetPathNode != null) {
   try {
    boolean rolloutAllowed = true;
    if (targetPathNode.hasProperty(CQ_PROPERTY_INHERITANCE_CANCELLED)) {
     javax.jcr.Property inheritanceCancelledProperty = targetPathNode.getProperty(CQ_PROPERTY_INHERITANCE_CANCELLED);
     Value[] propertiesCancelledInheritance = inheritanceCancelledProperty.getValues();
     rolloutAllowed = isRollOutChangesApplicable(rolloutAllowed, propertiesCancelledInheritance);
    }
    setTargetPageDescription(resourceResolver, blueprintPageDescription, targetPathNode, rolloutAllowed);
   } catch (RepositoryException e) {
    LOGGER.error("Repository exception at" + targetPathNode, e);
   }
  }
 }
 
 /**
  * @param resourceResolver
  * @param blueprintPageDescription
  * @param targetPathNode
  * @param rolloutAllowed
  * @throws RepositoryException
  * @throws PersistenceException
  */
 private void setTargetPageDescription(ResourceResolver resourceResolver, String blueprintPageDescription, Node targetPathNode,
   boolean rolloutAllowed) {
  if (rolloutAllowed) {
   try {
    targetPathNode.setProperty(JcrConstants.JCR_DESCRIPTION, blueprintPageDescription);
    resourceResolver.commit();
   } catch (RepositoryException e) {
    LOGGER.error("Repository exception for resolver" + resourceResolver, e);
   } catch (PersistenceException e) {
    LOGGER.error("Persistence exception at" + targetPathNode, e);
   }
  }
 }
 
 /**
  * returns true in case of roll out is enabled for target page description
  * 
  * @param rolloutAllowed
  * @param propertiesCancelledInheritance
  * @return
  */
 private boolean isRollOutChangesApplicable(boolean rolloutAllowed, Value[] propertiesCancelledInheritance) {
  for (Value property : propertiesCancelledInheritance) {
   try {
    if (StringUtils.equals(property.getString(), JcrConstants.JCR_DESCRIPTION)) {
     rolloutAllowed = false;
     break;
    }
   } catch (RepositoryException e) {
    LOGGER.error("Repository exception at proeprty" + property, e);
   }
  }
  return rolloutAllowed;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
 }
}
