/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Audible;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Audio;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class AnchorRule.
 */
public class AudioRule extends ConfigurationSelectorRule {
 
 private static final String PROPERTY_AUDIO_URL = "audio.url";
 private static final String PROPERTY_AUDIO_TITLE = "audio.title";
 private static final String PROPERTY_AUDIO_AUTOPLAY = "audio.autoplay";
 private static final String PROPERTY_AUDIO_MUTED = "audio.muted";
 
 /** The instance. */
 private static AudioRule instance = new AudioRule();
 
 /**
  * Instantiates a new anchor rule.
  */
 private AudioRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor rule
  */
 public static AudioRule create() {
  return instance;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Audio audio = Audio.create();
  String url = (String) this.getProperty(PROPERTY_AUDIO_URL, node);
  String title = (String) this.getProperty(PROPERTY_AUDIO_TITLE, node);
  String autoplay = (String) this.getProperty(PROPERTY_AUDIO_AUTOPLAY, node);
  String muted = (String) this.getProperty(PROPERTY_AUDIO_MUTED, node);
  if (StringUtils.isNotBlank(url)) {
   audio.withURL(url);
   ((Audible) container).withAudio(audio);
  }
  if (StringUtils.isNotBlank(title)) {
   audio.withTitle(title);
  }
  if (StringUtils.isEmpty(autoplay) || autoplay.equalsIgnoreCase("true") || autoplay.equalsIgnoreCase("autoplay")) {
   audio.enableAutoplay();
  }
  if (StringUtils.isEmpty(muted) || muted.equalsIgnoreCase("true") || muted.equalsIgnoreCase("autoplay")) {
   audio.enableMuted();
  }
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Audible.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the audio rule
  */
 public static AudioRule createFrom(JSONObject configuration) {
  AudioRule audioRule = create();
  audioRule = (AudioRule) audioRule.withSelector(configuration.getString("selector"));
  JSONObject anchorConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_AUDIO_URL);
  properties.add(PROPERTY_AUDIO_TITLE);
  properties.add(PROPERTY_AUDIO_AUTOPLAY);
  properties.add(PROPERTY_AUDIO_MUTED);
  audioRule.withProperties(properties, anchorConfiguration);
  return audioRule;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
