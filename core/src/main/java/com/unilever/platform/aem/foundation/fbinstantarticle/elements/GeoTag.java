/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;

/**
 * Class Map
 * This element Class holds map content for the articles.
 *
 * Example:
 *  <figure class="op-map">
 *    <script type="application/json" class="op-geotag">
 *      {
 *          "type": "Feature",
 *          "geometry": {
 *               "type": "Point",
 *               "coordinates": [23.166667, 89.216667]
 *          },
 *          "properties": {
 *               "title": "Jessore, Bangladesh",
 *               "radius": 750000,
 *               "pivot": true,
 *               "style": "satellite",
 *           }
 *       }
 *    </script>
 *  </figure>
 *
 */
/**
 * The Class Time.
 */
public class GeoTag extends InstantArticleElement {
 
 /** The instance. */
 private static GeoTag instance = new GeoTag();
 
 /** The script. */
 private String script;
 
 /**
  * Instantiates a new geo tag.
  */
 private GeoTag() {
  
 }
 
 /**
  * Sets the geotag on the image.
  *
  * @param script
  *         the script
  * @return $this
  * @see {link:http://geojson.org/}
  */
 public GeoTag withScript(String script) {
  this.script = script;
  return this;
 }
 
 /**
  * Gets the script.
  *
  * @return string Geotag json content unescaped
  */
 public String getScript() {
  return script;
 }
 
 /**
  * Creates the.
  *
  * @return the geo tag
  */
 public static GeoTag create() {
  return instance;
 }
 
 /**
  * To dom element.
  *
  * @param document
  *         the document
  * @return the org.jsoup.nodes. element
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  org.jsoup.nodes.Element scriptElement = document.createElement("script");
  scriptElement.attr("type", "application/json");
  scriptElement.attr("class", "op-geotag");
  if (StringUtils.isNotBlank(this.script)) {
   String cdataString = "<!--//<![CDATA[ " + this.script + " //]]>-->";
   scriptElement.append(cdataString);
  }
  return scriptElement;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement()
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#isValid()
  */
 public boolean isValid() {
  return StringUtils.isNotBlank(this.script) ? true : false;
 }
 
}
