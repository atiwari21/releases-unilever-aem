/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Italic;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Paragraph;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

public class ItalicRule extends ConfigurationSelectorRule {
 
 private ItalicRule() {
  
 }
 
 public static ItalicRule create() {
  return new ItalicRule();
 }
 
 public static Rule createFrom(JSONObject configuration) {
  ItalicRule italicRule = create();
  italicRule.withSelector(configuration.getString("selector"));
  return italicRule;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Italic italic = Italic.create();
  if (container instanceof Paragraph) {
   ((Paragraph) container).addChild(italic);
  } else {
   transformer.getInstantArticle().addChild(italic);
  }
  transformer.transform(italic, node);
  return container;
 }
 
 @Override
 public String[] getContextClass() {
  return new String[] { TextContainer.getClassName(),InstantArticle.getClassName() };
 }
 
}
