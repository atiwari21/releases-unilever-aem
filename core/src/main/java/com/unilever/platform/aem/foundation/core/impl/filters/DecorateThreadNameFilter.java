/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.impl.filters;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DecorateThreadNameFilter * This method decorates the thread name with following contextual information *
 * <li>Old Thread name: to ensure that we are not losing any original context with thread names;</li>
 * <li>Time when the request was intercepted;</li>
 * <li>The RequestURI that proviced information on what RestFUL endpoint was accessed as part of this request;</li>
 * <li>A Token that was received in the header. This token is encrypted and does not exposes any confidential information. Also, this token provides
 * context which helps during debugging;</li>
 * <li>The Payload from the token. This information will be very helpful when we have to debug for issues that may be happening with a call request as
 * this holds all the information sent from the called.</li>
 * </ul>
 * 
 * This filter will also reset the ThreadName back to it's original name once the processing is complete.
 */

@Component(immediate = true, description = "Filter to decorate the thread name with additional contextual information", label = "Unilever Decorate Thread Name Filter", enabled = true)
@Service(Filter.class)
@Properties({ @Property(name = EngineConstants.SLING_FILTER_SCOPE, value = { EngineConstants.FILTER_SCOPE_REQUEST }),
  @Property(name = Constants.SERVICE_VENDOR, value = "unilever"), @Property(name = "filter.order", intValue = -601) })
public class DecorateThreadNameFilter implements Filter {
 
 /**
  * Logger for the MDCUpdateFilter.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(DecorateThreadNameFilter.class);
 
 /**
  * Destroy.
  *
  * @see javax.servlet.Filter#destroy()
  */
 @Override
 public void destroy() {
  // Not using this method in this class.
 }
 
 /**
  *
  *
  * @param request
  *         - http servlet request object.
  * @param response
  *         - http servlet response object.
  * @param filterChain
  *         - filter chain object to achieve chaining.
  * @throws IOException
  *          - In case of any IO exception.
  * @throws ServletException
  *          - In case of any system error.
  * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
  */
 @Override
 public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
   throws IOException, ServletException {
  
  HttpServletRequest httpRequest = (HttpServletRequest) request;
  
  final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  
  Thread thread = Thread.currentThread();
  String threadOriginalName = thread.getName();
  
  String uri = httpRequest.getRequestURI();
  String time = dateFormat.format(new Date());
  String token = httpRequest.getHeader("authorization");
  
  try {
   thread.setName(String.format("%s StartTime \"%s\" RequestURI \"%s\" Token \"%s\"", threadOriginalName, time, uri, token));
  } catch (Exception ex) {
   LOGGER.error("Failed to set the thread name.", ex);
   // this is an internal filter and an error here should not impact
   // the request processing, hence eat the exception
  }
  
  try {
   filterChain.doFilter(request, response);
  } finally {
   try {
    thread.setName(threadOriginalName);
   } catch (Exception ex) {
    LOGGER.error("Failed to reset the thread name.", ex);
    // this is an internal filter and an error here should not
    // impact the request processing, hence eat the exception
   }
  }
  
 }
 
 /**
  * Inits the.
  *
  * @param arg0
  *         filterConfig.
  * @throws ServletException
  *          servletException
  * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
  */
 @Override
 public void init(FilterConfig arg0) throws ServletException {
  // Not using this method in this class.
  
 }
}
