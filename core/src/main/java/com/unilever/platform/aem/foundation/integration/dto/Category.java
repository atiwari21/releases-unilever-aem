/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Category.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "sourceId", "parentSourceId", "names", "pageUrls" })
public class Category {
 
 /** The source id. */
 @XmlElement(name = "SourceId", required = true)
 private String sourceId;
 
 /** The parent source id. */
 @XmlElement(name = "ParentSourceId")
 protected String parentSourceId;
 
 /** The names. */
 @XmlElement(name = "Names", required = true)
 protected Names names;
 
 /** The page urls. */
 @XmlElement(name = "PageUrls", required = true)
 protected PageUrls pageUrls;
 
 /**
  * Gets the parent source id.
  *
  * @return the parent source id
  */
 public String getParentSourceId() {
  return parentSourceId;
 }
 
 /**
  * Sets the parent source id.
  *
  * @param value
  *         the new parent source id
  */
 public void setParentSourceId(String value) {
  this.parentSourceId = value;
 }
 
 /**
  * Gets the names.
  *
  * @return the names
  */
 public Names getNames() {
  return names;
 }
 
 /**
  * Sets the names.
  *
  * @param value
  *         the new names
  */
 public void setNames(Names value) {
  this.names = value;
 }
 
 /**
  * Gets the page urls.
  *
  * @return the page urls
  */
 public PageUrls getPageUrls() {
  return pageUrls;
 }
 
 /**
  * Sets the page urls.
  *
  * @param value
  *         the new page urls
  */
 public void setPageUrls(PageUrls value) {
  this.pageUrls = value;
 }
 
 /**
  * Gets the source id.
  *
  * @return the source id
  */
 public String getSourceId() {
  return sourceId;
 }
 
 /**
  * Sets the source id.
  *
  * @param sourceId
  *         the new source id
  */
 public void setSourceId(String sourceId) {
  this.sourceId = sourceId;
 }
 
}
