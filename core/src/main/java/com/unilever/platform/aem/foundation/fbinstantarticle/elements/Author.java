/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;

/**
 * The Class Author.
 */
public class Author extends InstantArticleElement {
 
 /**
  * The Author name
  */
 private String name;
 /**
  * The string url for the Author
  */
 private String url;
 /**
  * The string description.
  */
 private String description;
 
 private static Author instance = new Author();
 
 private Author() {
  
 }
 
 public static Author create() {
  
  return instance;
 }
 
 public Author withURL(String url) {
  this.url = url;
  return this;
 }
 
 public Author withDescription(String description) {
  this.description = description;
  return this;
 }
 
 /**
  * The Author name.
  *
  * @param string
  *         title the audio title that will be shown
  *
  * @return this
  */
 public Author withName(String name) {
  this.name = name;
  return this;
 }
 
 public String getName() {
  return this.name;
 }
 
 public String getDescription() {
  return this.description;
 }
 
 /**
  * Gets the url for the audio
  *
  * @return string Audio url
  */
 public String getUrl() {
  return this.url;
 }
 
 /**
  * To dom element.
  *
  * @param document
  *         the document
  * @return the org.jsoup.nodes. element
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  org.jsoup.nodes.Element addressElement = document.createElement("address");
  org.jsoup.nodes.Element anchorElement = document.createElement("a");
  
  if (StringUtils.isNotBlank(this.name)) {
   anchorElement.attr("title", this.name);
  }
  if (StringUtils.isNotBlank(this.url)) {
   anchorElement.attr("src", this.url);
  }
  anchorElement.appendText(this.name);
  addressElement.appendChild(anchorElement);
  return addressElement;
 }
 
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 public boolean isValid() {
  return StringUtils.isNotBlank(this.name) ? true : false;
 }
 
}
