/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

/**
 * The Class ConfigurationNotFoundException.
 */
public class ConfigurationNotFoundException extends Exception {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -1658927835162116097L;
 
 /** The message. */
 private final String message;
 
 /**
  * Instantiates a new configuration not found exception.
  *
  * @param message
  *         the message
  */
 public ConfigurationNotFoundException(String message) {
  super(message);
  this.message = message;
 }
 
 /**
  * Instantiates a new configuration not found exception.
  *
  * @param cause
  *         the cause
  */
 public ConfigurationNotFoundException(Throwable cause) {
  super(cause);
  this.message = cause.getMessage();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see java.lang.Throwable#toString()
  */
 @Override
 public String toString() {
  return message;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see java.lang.Throwable#getMessage()
  */
 @Override
 public String getMessage() {
  return message;
 }
}
