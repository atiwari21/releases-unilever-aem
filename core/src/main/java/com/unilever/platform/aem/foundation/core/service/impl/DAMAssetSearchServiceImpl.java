/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;
import com.unilever.platform.aem.foundation.core.service.DAMAssetSearchService;

/**
 * The Class DAMAssetSearchServiceImpl.
 */
@Component(label = "DAM Asset Search Service", immediate = true, metatype = true)
@Service(value = { DAMAssetSearchService.class })
public class DAMAssetSearchServiceImpl implements DAMAssetSearchService {
 
 /** The query. */
 private static String query = "SELECT * FROM [dam:Asset] As asset WHERE ISDESCENDANTNODE(asset, '{0}') AND [jcr:content/metadata/recordID] IN ({1})";
 
 /** The Constant JCR_SQL2. */
 private static final String JCR_SQL2 = "JCR-SQL2";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.DAMAssetSearchService#getAssetByRecordId(java.lang.String, java.lang.String,
  * org.apache.sling.api.resource.ResourceResolver)
  */
 @Override
 public Asset getAssetByRecordId(String recordId, String searchPath, ResourceResolver resolver) {
  Asset asset = null;
  
  final Iterator<Resource> resourceIterator = resolver.findResources(getRefinedQuery(searchPath, recordId), JCR_SQL2);
  
  while (resourceIterator.hasNext()) {
   Resource res = resourceIterator.next();
   asset = DamUtil.resolveToAsset((Resource) res);
   
   break;
  }
  
  return asset;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.DAMAssetSearchService#getAssetsByRecordIds(java.lang.String[], java.lang.String,
  * org.apache.sling.api.resource.ResourceResolver)
  */
 @Override
 public List<Asset> getAssetsByRecordIds(String[] recordId, String searchPath, ResourceResolver resolver) {
  
  List<Asset> assetList = new ArrayList<Asset>();
  String ids = StringUtils.EMPTY;
  for (String img : recordId) {
   ids = ids + "'" + img.trim() + "',";
  }
  
  if (StringUtils.isNotBlank(ids)) {
   
   final Iterator<Resource> resourceIterator = resolver.findResources(getRefinedQuery(searchPath, ids.substring(0, ids.length() - 1)), JCR_SQL2);
   
   while (resourceIterator.hasNext()) {
    Resource res = resourceIterator.next();
    Asset asset = DamUtil.resolveToAsset((Resource) res);
    if (asset != null) {
     assetList.add(asset);
    }
   }
  }
  return assetList;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.DAMAssetSearchService#getAssetsByHashTag(java.lang.String, java.lang.String[],
  * java.lang.String, int, int, org.apache.sling.api.resource.ResourceResolver)
  */
 @Override
 public List<Asset> getAssetsByHashTag(String hashTag, String[] channelList, String searchPath, int startIndex, int noOfRecords,
   ResourceResolver resolver) {
  List<Asset> assetList = new ArrayList<Asset>();
  return assetList;
 }
 
 /**
  * Gets the refined query.
  *
  * @param searchPath
  *         the search path
  * @param ids
  *         the ids
  * @return the refined query
  */
 private String getRefinedQuery(String searchPath, String ids) {
  return MessageFormat.format(query, searchPath.trim(), ids.trim());
 }
}
