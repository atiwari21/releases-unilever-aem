/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * The Class GenericHelper.
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class SubtractHelper implements HandlebarsHelperService<Object> {
 
 /**
  * Logger Instance.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(SubtractHelper.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new SubtractHelper();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "subtract";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#setCache(com.sapient.platform.iea.aem.scripting.
  * handlebars.api.HandlebarsTemplateCache)
  */
 @Override
 public void setCache(HandlebarsTemplateCache templateCache) {
  LOGGER.info("Cache is not implemented for renderTemplate handler.");
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.github.jknack.handlebars.Helper#apply(java.lang.Object, com.github.jknack.handlebars.Options)
  */
 @SuppressWarnings("unchecked")
 @Override
 public CharSequence apply(Object objValue, Options options) throws IOException {
  
  CharSequence chars = null;
  LOGGER.debug("objValue = ", objValue.toString());
  LOGGER.debug("subtraction = ", options.params[0].toString());
  try {
   float value = Float.parseFloat(objValue.toString());
   float subtraction = Float.parseFloat(options.params[0].toString());
   
   float result = value - subtraction;
   
   chars = String.valueOf(result);
   
  } catch (Exception e) {
   LOGGER.error("Error occurred in subtract Helper.", e);
  }
  return chars;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getName()
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getHelper()
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
}
