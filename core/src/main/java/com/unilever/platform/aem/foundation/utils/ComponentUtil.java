/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.BadLocationException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.engine.SlingRequestProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;

/**
 * The Class ComponentUtil.
 */
public class ComponentUtil {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ComponentUtil.class);
 
 /**
  * Instantiates a new component util.
  */
 private ComponentUtil() {
  
 }
 
 /**
  * @param resource
  * @param propertyName
  * @return
  */
 public static List<Map<String, String>> getNestedMultiFieldProperties(Resource resource, String propertyName) {
  
  List<Map<String, String>> list = new ArrayList<Map<String, String>>();
  
  if (resource != null && propertyName != null) {
   Node currentNode = resource.adaptTo(Node.class);
   try {
    Property property = null;
    
    if (currentNode.hasProperty(propertyName)) {
     property = currentNode.getProperty(propertyName);
    }
    
    if (property != null) {
     Value[] values = getValueArray(property);
     
     for (Value val : values) {
      JSONObject rootObject = new JSONObject(val.getString());
      Map<String, String> topMap = toMap(rootObject);
      if (topMap != null) {
       list.add(topMap);
      }
     }
    }
   } catch (PathNotFoundException e) {
    LOGGER.error("path not found" + e.getMessage());
    LOGGER.debug("path not found", e);
   } catch (RepositoryException e) {
    LOGGER.error("could not connect to repository" + e.getMessage());
    LOGGER.debug("could not connect to repository", e);
   } catch (JSONException e) {
    LOGGER.error("Error while fetching JSON Object" + e.getMessage());
    LOGGER.debug("Error while fetching JSON Object", e);
   }
  }
  return list;
 }
 
 /**
  * Gets the value array.
  * 
  * @param property
  *         the property
  * @return the value array
  * @throws RepositoryException
  *          the repository exception
  */
 private static Value[] getValueArray(Property property) throws RepositoryException {
  Value[] values = null;
  if (property.isMultiple()) {
   values = property.getValues();
  } else {
   values = new Value[1];
   values[0] = property.getValue();
  }
  
  return values;
 }
 
 /**
  * To map.
  * 
  * @param object
  *         the object
  * @return the map
  * @throws JSONException
  *          the JSON exception
  */
 public static Map<String, String> toMap(JSONObject object) throws JSONException {
  Map<String, String> map = new HashMap<String, String>();
  
  Iterator<String> keysItr = object.keys();
  while (keysItr.hasNext()) {
   String key = keysItr.next();
   Object value = object.get(key);
   map.put(key, value.toString());
  }
  return map;
 }
 
 /**
  * Gets the nested multi field properties.
  * 
  * @param resource
  *         the resource
  * @param propertyName
  *         the property name
  * @return the nested multi field properties
  */
 public static List<Map<String, String>> getDialogProperties(Resource resource, String propertyName) {
  
  List<Map<String, String>> list = new ArrayList<Map<String, String>>();
  
  if (resource != null && propertyName != null) {
   Node currentNode = resource.adaptTo(Node.class);
   try {
    Property property = null;
    
    if (currentNode.hasProperty(propertyName)) {
     property = currentNode.getProperty(propertyName);
    }
    
    if (property != null) {
     Value[] values = getValueArray(property);
     
     for (Value val : values) {
      JSONObject rootObject = new JSONObject(val.getString());
      Map<String, String> topMap = toMap(rootObject);
      if (topMap != null) {
       list.add(topMap);
      }
     }
    }
   } catch (PathNotFoundException e) {
    LOGGER.error("path not found" + e.getMessage());
    LOGGER.debug("path not found", e);
   } catch (RepositoryException e) {
    LOGGER.error("could not connect to repository" + e.getMessage());
    LOGGER.debug("could not connect to repository", e);
   } catch (JSONException e) {
    LOGGER.error("Error while fetching JSON Object" + e.getMessage());
    LOGGER.debug("Error while fetching JSON Object", e);
   }
  }
  return list;
 }
 
 /**
  * 
  * @param properties
  * @param propertyKey
  * @return
  */
 public static String[] getPropertyValueArray(Map<String, Object> properties, String propertyKey) {
  LOGGER.debug("Inside getPropertyValueArray");
  Object propertyValObj = properties.get(propertyKey);
  String[] propertyValArr = new String[] {};
  if (propertyValObj != null) {
   if (propertyValObj instanceof String[]) {
    propertyValArr = (String[]) propertyValObj;
   } else if (StringUtils.isNotBlank((String) propertyValObj)) {
    propertyValArr = new String[] { propertyValObj.toString() };
   }
  }
  return propertyValArr;
 }
 
 /**
  * Gets the word count.
  *
  * @param url
  *         the url
  * @param resolver
  *         the resolver
  * @param attrMap
  *         {@link Map}
  * @param requestResponseFactory
  * @param requestProcessor
  * @return the word count
  * @throws BadLocationException
  *          the bad location exception
  */
 public static Reader getReaderFromURL(String url, ResourceResolver resolver, RequestResponseFactory requestResponseFactory,
   SlingRequestProcessor requestProcessor, Map<String, String> attrMap, String method) {
  
  HttpServletRequest httpServletRequest = requestResponseFactory.createRequest(method, url);
  httpServletRequest.setAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME, WCMMode.DISABLED);
  if (attrMap != null) {
   Iterator<String> itr = attrMap.keySet().iterator();
   while (itr.hasNext()) {
    String attrName = itr.next();
    String attrValue = attrMap.get(attrName);
    if (StringUtils.isNotBlank(attrValue)) {
     httpServletRequest.setAttribute(attrName, attrValue);
    }
   }
  }
  ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
  HttpServletResponse httpServletResponse = requestResponseFactory.createResponse(arrayOutputStream);
  try {
   requestProcessor.processRequest(httpServletRequest, httpServletResponse, resolver);
   InputStream isFromFirstData = new ByteArrayInputStream(arrayOutputStream.toByteArray());
   return new BufferedReader(new InputStreamReader(isFromFirstData));
   
  } catch (ServletException e) {
   LOGGER.debug("Servlet Exception:", e);
   LOGGER.error("Servlet Exception:", e.getMessage());
  } catch (IOException e) {
   LOGGER.debug("IO Exception", e);
   LOGGER.error("IO Exception", e.getMessage());
  }
  return null;
 }
 
 
}
