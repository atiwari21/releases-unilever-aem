/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import java.util.Date;
import java.util.List;

import com.day.cq.tagging.Tag;

/**
 * The Class IndexDTO.
 */
public class IndexDTO {
 
 /** The page url. */
 private String pageUrl;
 
 /** The body content. */
 private String bodyContent;
 
 /** The content type. */
 private String contentType;
 
 /** The sub content type. */
 private String subContentType;
 
 /** The title. */
 private String title;
 
 /** The description. */
 private String description;
 
 /** The publish date. */
 private Date publishDate;
 
 /** The Image url. */
 private String imageUrl;
 
 /** The productid. */
 private String productid;
 
 /** The brandname. */
 private String brandname;
 
 /** The brandlocale. */
 private String brandlocale;
 
 /** The locale. */
 private String locale;
 
 /** The id. */
 private String id;
 
 /** The indentifier type. */
 private String indentifierType;
 
 /** The identifier value. */
 private String identifierValue;
 
 /** The short identifier value. */
 private String shortIdentifierValue;
 
 /** The breadcrumb. */
 private List<SearchBreadcrumbDTO> breadcrumb;
 
 /** The Image name. */
 private String imageName;
 
 /** The Image extension. */
 private String imageExtension;
 
 /** The Is not adaptive image. */
 private String isNotAdaptiveImage;
 
 /** The Image title. */
 private String imageTitle;
 
 /** The Image path. */
 private String imagePath;
 
 /** The Image alt text. */
 private String imageAltText;
 
 /** The tags. */
 private Tag[] tags;
 
 /** The Long teaser Copy. */
 private String teaserCopy;
 
 private boolean isRelatedVideo;
 
 /**
  * Instantiates a new index dto.
  */
 public IndexDTO() {
  super();
 }
 
 /**
  * Gets the image name.
  *
  * @return the image name
  */
 public String getImageName() {
  return imageName;
 }
 
 /**
  * Sets the image name.
  *
  * @param imageName
  *         the new image name
  */
 public void setImageName(String imgName) {
  imageName = imgName;
 }
 
 /**
  * Gets the image extension.
  *
  * @return the image extension
  */
 public String getImageExtension() {
  return imageExtension;
 }
 
 /**
  * Sets the image extension.
  *
  * @param imageExtension
  *         the new image extension
  */
 public void setImageExtension(String imageExtn) {
  imageExtension = imageExtn;
 }
 
 /**
  * Gets the checks if is not adaptive image.
  *
  * @return the checks if is not adaptive image
  */
 public String getIsNotAdaptiveImage() {
  return isNotAdaptiveImage;
 }
 
 /**
  * Sets the checks if is not adaptive image.
  *
  * @param isNotAdaptiveImage
  *         the new checks if is not adaptive image
  */
 public void setIsNotAdaptiveImage(String isNotAdaptiveImg) {
  isNotAdaptiveImage = isNotAdaptiveImg;
 }
 
 /**
  * Gets the image title.
  *
  * @return the image title
  */
 public String getImageTitle() {
  return imageTitle;
 }
 
 /**
  * Sets the image title.
  *
  * @param imageTitle
  *         the new image title
  */
 public void setImageTitle(String imgTitle) {
  imageTitle = imgTitle;
 }
 
 /**
  * Gets the image path.
  *
  * @return the image path
  */
 public String getImagePath() {
  return imagePath;
 }
 
 /**
  * Sets the image path.
  *
  * @param imagePath
  *         the new image path
  */
 public void setImagePath(String imgPath) {
  imagePath = imgPath;
 }
 
 /**
  * Gets the image alt text.
  *
  * @return the image alt text
  */
 public String getImageAltText() {
  return imageAltText;
 }
 
 /**
  * Sets the image alt text.
  *
  * @param imageAltText
  *         the new image alt text
  */
 public void setImageAltText(String altText) {
  imageAltText = altText;
 }
 
 /**
  * Gets the breadcrumb.
  *
  * @return the breadcrumb
  */
 public List<SearchBreadcrumbDTO> getBreadcrumb() {
  return breadcrumb;
 }
 
 /**
  * Sets the breadcrumb.
  *
  * @param breadcrumb
  *         the new breadcrumb
  */
 public void setBreadcrumb(List<SearchBreadcrumbDTO> breadcrumb) {
  this.breadcrumb = breadcrumb;
 }
 
 /**
  * Gets the indentifier type.
  *
  * @return the indentifier type
  */
 public String getIndentifierType() {
  return indentifierType;
 }
 
 /**
  * Sets the indentifier type.
  *
  * @param indentifierType
  *         the new indentifier type
  */
 public void setIndentifierType(String indentifierType) {
  this.indentifierType = indentifierType;
 }
 
 /**
  * Gets the identifier value.
  *
  * @return the identifier value
  */
 public String getIdentifierValue() {
  return identifierValue;
 }
 
 /**
  * Sets the identifier value.
  *
  * @param identifierValue
  *         the new identifier value
  */
 public void setIdentifierValue(String identifierValue) {
  this.identifierValue = identifierValue;
 }
 
 /**
  * Gets the tags.
  *
  * @return the tags
  */
 public Tag[] getTags() {
  return tags;
 }
 
 /**
  * Sets the tags.
  *
  * @param tags
  *         the new tags
  */
 public void setTags(Tag[] tags) {
  this.tags = tags;
 }
 
 /**
  * Gets the id.
  *
  * @return the id
  */
 public String getId() {
  return id;
 }
 
 /**
  * Sets the id.
  *
  * @param id
  *         the new id
  */
 public void setId(String id) {
  this.id = id;
 }
 
 /**
  * Gets the description.
  *
  * @return the description
  */
 public String getDescription() {
  return description;
 }
 
 /**
  * Sets the description.
  *
  * @param description
  *         the new description
  */
 public void setDescription(String description) {
  this.description = description;
 }
 
 /**
  * Gets the publish date.
  *
  * @return the publish date
  */
 public Date getPublishDate() {
  return publishDate;
 }
 
 /**
  * Sets the publish date.
  *
  * @param date
  *         the new publish date
  */
 public void setPublishDate(Date date) {
  this.publishDate = date;
 }
 
 /**
  * Gets the image url.
  *
  * @return the image url
  */
 public String getImageUrl() {
  return imageUrl;
 }
 
 /**
  * Sets the image url.
  *
  * @param imageUrl
  *         the new image url
  */
 public void setImageUrl(String imageUrl) {
  this.imageUrl = imageUrl;
 }
 
 /**
  * Gets the productid.
  *
  * @return the productid
  */
 public String getProductid() {
  return productid;
 }
 
 /**
  * Sets the productid.
  *
  * @param productid
  *         the new productid
  */
 public void setProductid(String productid) {
  this.productid = productid;
 }
 
 /**
  * Gets the brandname.
  *
  * @return the brandname
  */
 public String getBrandname() {
  return brandname;
 }
 
 /**
  * Sets the brandname.
  *
  * @param brandname
  *         the new brandname
  */
 public void setBrandname(String brandname) {
  this.brandname = brandname;
 }
 
 /**
  * Gets the locale.
  *
  * @return the locale
  */
 public String getLocale() {
  return locale;
 }
 
 /**
  * Sets the locale.
  *
  * @param locale
  *         the new locale
  */
 public void setLocale(String locale) {
  this.locale = locale;
 }
 
 /**
  * Gets the title.
  *
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  *
  * @param title
  *         the new title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the page url.
  *
  * @return the page url
  */
 public String getPageUrl() {
  return pageUrl;
 }
 
 /**
  * Sets the page url.
  *
  * @param pageUrl
  *         the new page url
  */
 public void setPageUrl(String pageUrl) {
  this.pageUrl = pageUrl;
 }
 
 /**
  * Gets the body content.
  *
  * @return the body content
  */
 public String getBodyContent() {
  return bodyContent;
 }
 
 /**
  * Sets the body content.
  *
  * @param bodyContent
  *         the new body content
  */
 public void setBodyContent(String bodyContent) {
  this.bodyContent = bodyContent;
 }
 
 /**
  * Gets the content type.
  *
  * @return the content type
  */
 public String getContentType() {
  return contentType;
 }
 
 /**
  * Sets the content type.
  *
  * @param contentType
  *         the new content type
  */
 public void setContentType(String contentType) {
  this.contentType = contentType;
 }
 
 /**
  * Gets the brand Locale type.
  *
  * @return the brandlocale type
  */
 public String getBrandlocale() {
  return brandlocale;
 }
 
 /**
  * Sets the content type.
  *
  * @param brandlocale
  *         the locale of brand
  */
 public void setBrandlocale(String brandlocale) {
  this.brandlocale = brandlocale;
 }
 
 /**
  * Get the long teaser Copy.
  *
  */
 public String getTeaserCopy() {
  return teaserCopy;
 }
 
 /**
  * Sets the long teaser Copy.
  *
  * @param teaserCopy
  *         the long teaser Copy
  */
 public void setTeaserCopy(String teaserCopy) {
  this.teaserCopy = teaserCopy;
 }
 
 /**
  * Gets the sub content type.
  * 
  * @return the subContentType
  */
 public String getSubContentType() {
  return subContentType;
 }
 
 /**
  * Sets the sub content type.
  * 
  * @param subContentType
  *         the subContentType to set
  */
 public void setSubContentType(String subContentType) {
  this.subContentType = subContentType;
 }
 
 /**
  * Gets short identifier value.
  * 
  * @return the shortIdentifierValue
  */
 public String getShortIdentifierValue() {
  return shortIdentifierValue;
 }
 
 /**
  * Sets the short identifier value.
  * 
  * @param shortIdentifierValue
  *         the shortIdentifierValue to set
  */
 public void setShortIdentifierValue(String shortIdentifierValue) {
  this.shortIdentifierValue = shortIdentifierValue;
 }

 /**
  * @return the isRelatedVideo
  */
 public boolean isRelatedVideo() {
  return isRelatedVideo;
 }

 /**
  * @param isRelatedVideo the isRelatedVideo to set
  */
 public void setRelatedVideo(boolean isRelatedVideo) {
  this.isRelatedVideo = isRelatedVideo;
 }
 
}
