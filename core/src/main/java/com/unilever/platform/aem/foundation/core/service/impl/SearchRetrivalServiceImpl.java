/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse.Suggestion;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.dto.AdaptiveImageDTO;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;
import com.unilever.platform.aem.foundation.core.dto.IndexingSchema;
import com.unilever.platform.aem.foundation.core.service.SearchRetrivalService;
import com.unilever.platform.aem.foundation.core.service.SolrConfigurationService;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.aem.foundation.utils.ComponentUtil;

/**
 * This is SearchRetrieval Service.
 */
@Component(label = "Unilever Search Retrival Service", immediate = true, metatype = true)
@Service(value = { SearchRetrivalService.class })
public class SearchRetrivalServiceImpl implements SearchRetrivalService {
 
 private static final String SEARCH_INPUT_PATH = "sip";
 
 private static final String SUB_BRAND_NAME = "sbn";
 
 /** The Constant IPP. */
 private static final String IPP = "ipp";
 
 /** The solr config. */
 @Reference
 private SolrConfigurationService solrConfig;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 private ResourceResolverFactory factory;
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(SearchRetrivalService.class);
 
 /** The Constant REQ_PARAM_TYPE. */
 public static final String SEARCH_TERM = "q";
 
 /** The Constant FILTER. */
 public static final String FILTER = "fq";
 
 /** The Constant HIGHLIGHT. */
 public static final String HIGHLIGHT = "highlight";
 
 /** The Constant HIGHLIGHTTERM. */
 public static final String HIGHLIGHTTERM = "highlightFilter";
 
 /** The Constant HIGHLIGHTTGAGPRE. */
 public static final String HIGHLIGHTTGAGPRE = "highlightPrefix";
 
 /** The Constant HIGHLIGHTTGAGPOST. */
 public static final String HIGHLIGHTTGAGPOST = "highlightPostfix";
 
 /** The Constant RESULTPERPAGE. */
 public static final String RESULTPERPAGE = "resultPerPage";
 
 /** The Constant HANDLER. */
 public static final String HANDLER = "handler";
 
 /** The Constant LOCALE. */
 public static final String LOCALE = "Locale";
 
 /** The Constant BRANDNAME. */
 public static final String BRANDNAME = "BrandName";
 
 /** The Constant START. */
 public static final String START = "PageNum";
 
 /** The Constant SEARCH_HANDLER. */
 public static final String SEARCH_HANDLER = "/select";
 
 /** The Constant SUGGESTION_EXIST. */
 public static final String SUGGESTION_EXIST = "suggestionExist";
 
 /** The Constant SUGGESTION_QUERY. */
 public static final String SUGGESTION_QUERY = "suggestionQuery";
 
 /** The Constant UTF-8. */
 public static final String UTF_8 = "UTF-8";
 
 /** The Constant TWO. */
 private static final int NUMERIC_TWO = 2;
 
 /** The Constant NUMERIC_THREE. */
 private static final int NUMERIC_THREE = 3;
 
 /** The Constant NUMERIC_FOUR. */
 private static final int NUMERIC_FOUR = 4;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.SearchRetrivalService# getSearchResult(org.apache.sling.api.SlingHttpServletRequest)
  */
 @Override
 public Map<String, Object> getSearchResult(SlingHttpServletRequest request) {
  return getSearchResult(request, false);
 }
 
 /**
  * Gets the result json.
  *
  * @param queryResponse
  *         the query response
  * @param searchResultsMap
  *         the search results map
  * @param response
  *         the response
  * @param isJson
  *         the is json
  * @return the result json
  */
 private Map<String, Object> getResultJson(final QueryResponse queryResponse, Map<String, Object> searchResultsMap, SolrDocumentList response,
   Boolean isJson, boolean suggestionExist) {
  List<Map<String, Object>> finalResult = new ArrayList<Map<String, Object>>();
  for (SolrDocument result : response) {
   finalResult.add(getSolrDocumentMap(result, queryResponse, isJson));
  }
  if (suggestionExist) {
   searchResultsMap.put(SearchConstants.TOTALRESULTS, 0);
  } else {
   searchResultsMap.put(SearchConstants.TOTALRESULTS, queryResponse.getResults().getNumFound());
  }
  searchResultsMap.put(SearchConstants.RESPONSE, finalResult);
  searchResultsMap.put(SearchConstants.FACET, getFacetsArray(queryResponse));
  searchResultsMap.put(SearchConstants.LABEL, "new");
  return searchResultsMap;
 }
 
 /**
  * Gets the solr document map.
  *
  * @param result
  *         the result
  * @param queryResponse
  *         the query response
  * @param isJson
  *         the is json
  * @return the solr document map
  */
 private Map<String, Object> getSolrDocumentMap(SolrDocument result, final QueryResponse queryResponse, Boolean isJson) {
  
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  List<JSONObject> list = new ArrayList<JSONObject>();
  List<Map<String, Object>> jsonObjMapList = new ArrayList<Map<String, Object>>();
  
  try {
   Collection<Object> c = result.getFieldValues(IndexingSchema.BREADCRUMB);
   
   Iterator itr = result.keySet().iterator();
   while (itr.hasNext()) {
    String key = itr.next().toString();
    Object value = result.get(key);
    map.put(key, value);
   }
   if (null != c && !c.isEmpty()) {
    Iterator<Object> itr1 = c.iterator();
    while (itr1.hasNext()) {
     String str = (String) itr1.next();
     str = str.replace("\\", "");
     
     if (isJson) {
      Map<String, Object> jsonObjectMap = new ObjectMapper().readValue(str, HashMap.class);
      jsonObjMapList.add(jsonObjectMap);
     } else {
      JSONObject o = new JSONObject(str);
      list.add(o);
     }
    }
    if (isJson) {
     map.put(IndexingSchema.BREADCRUMB, jsonObjMapList);
    } else {
     map.put(IndexingSchema.BREADCRUMB, list);
    }
    
   }
   if (null != queryResponse.getHighlighting().get(map.get(IndexingSchema.ID))) {
    map.put(SearchConstants.HIGHLIGHTING, queryResponse.getHighlighting().get(map.get(IndexingSchema.ID)));
   }
   if (StringUtils.isNotEmpty((String) map.get(IndexingSchema.IMAGEURL))) {
    setImageMap(map);
   }
   map.remove("SpellCorrect");
   map.remove("SearchSuggestions");
   
  } catch (Exception e) {
   LOG.error("Exception in getting Solr Document Map. " + ExceptionUtils.getStackTrace(e));
  }
  
  return map;
 }
 
 /**
  * Gets the query response.
  *
  * @param requestedfields
  *         the requestedfields
  * @param client
  *         the client
  * @param query
  *         the query
  * @param collection
  *         the collection
  * @param sort
  *         the sort
  * @return the query response
  * @throws SolrServerException
  *          the solr server exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 private QueryResponse getQueryResponse(final Map<String, String> requestedfields, SolrClient client, final SolrQuery query, String collection,
   boolean sort) throws SolrServerException, IOException, ConfigurationNotFoundException {
  LOG.debug("Inside query building and getting response method");
  query.setRequestHandler(SEARCH_HANDLER);
  Map<String, String> searchConfig = new HashMap<String, String>();
  BrandMarketBean marketBean = new BrandMarketBean(requestedfields.get(BRANDNAME), requestedfields.get(LOCALE));
  searchConfig = configurationService.getCategoryConfiguration(marketBean, "searchConfiguration");
  if (sort) {
   query.addSort("LaunchDate", SolrQuery.ORDER.desc);
  }
  if ((SearchConstants.SEARCH_TRUE).equals(searchConfig.get(HIGHLIGHT))) {
   query.setHighlight(Boolean.valueOf(searchConfig.get(HIGHLIGHT)));
   query.addHighlightField(searchConfig.get(HIGHLIGHTTERM));
   query.setHighlightSimplePre(searchConfig.get(HIGHLIGHTTGAGPRE));
   query.setHighlightSimplePost(searchConfig.get(HIGHLIGHTTGAGPOST));
   query.setHighlightSnippets(NUMERIC_TWO);
   query.setFacet(true);
   query.addFacetField("ContentType");
   int resultperpage = requestedfields.containsKey("ipp") ? Integer.parseInt(requestedfields.get("ipp"))
     : Integer.parseInt(searchConfig.get(RESULTPERPAGE));
   // code changed for pagination
   int pageNumTemp = Integer.parseInt(requestedfields.get(START));
   if (pageNumTemp > 0) {
    int pageNum = pageNumTemp - 1;
    int offset = resultperpage * pageNum;
    query.setStart(offset);
   } else {
    query.setStart(0);
   }
   
   query.setRows(resultperpage);
  }
  query.addFilterQuery(SearchConstants.LOCALECOLON + requestedfields.get(LOCALE));
  query.addFilterQuery(SearchConstants.BRANDNAMECOLON + requestedfields.get(BRANDNAME));
  QueryResponse queryResponse = null;
  if (!collection.isEmpty()) {
   queryResponse = client.query(collection, query);
  }
  
  return queryResponse;
 }
 
 /**
  * Gets the collection.
  *
  * @param collectionsBrandsArray
  *         the collections brands array
  * @param requestedfields
  *         the requestedfields
  * @return the collection
  */
 private String getCollection(String[] collectionsBrandsArray, Map<String, String> requestedfields) {
  String collection = "";
  String defaultCollection = "collection1";
  for (String collectionName : collectionsBrandsArray) {
   StringTokenizer str1 = new StringTokenizer(collectionName, ":");
   while (str1.hasMoreTokens()) {
    String collectionBrand = str1.nextToken();
    String collectionLocale = str1.nextToken();
    collection = str1.nextToken();
    if (requestedfields.get(LOCALE).equalsIgnoreCase(collectionLocale) && requestedfields.get(BRANDNAME).equalsIgnoreCase(collectionBrand)) {
     return collection;
    }
   }
  }
  return defaultCollection;
 }
 
 /**
  * Obtains the sling:resourceType from the request.
  *
  * @param request
  *         the request
  * @return the sling:resourceType on success and an empty string otherwise.
  */
 protected Map<String, String> getSlingResourceType(SlingHttpServletRequest request) {
  final Map requestMap = request.getParameterMap();
  Map<String, String> requestedData = new HashMap<String, String>();
  requestedData.put(SEARCH_TERM, requestMap.containsKey(SEARCH_TERM) ? request.getParameter(SEARCH_TERM) : "");
  requestedData.put(FILTER, requestMap.containsKey(FILTER) ? request.getParameter(FILTER) : "");
  requestedData.put(LOCALE, requestMap.containsKey(LOCALE) ? request.getParameter(LOCALE) : "");
  requestedData.put(BRANDNAME, requestMap.containsKey(BRANDNAME) ? request.getParameter(BRANDNAME) : "");
  requestedData.put(START, requestMap.containsKey(START) ? request.getParameter(START) : "0");
  requestedData.put(SEARCH_INPUT_PATH, requestMap.containsKey(SEARCH_INPUT_PATH) ? request.getParameter(SEARCH_INPUT_PATH) : "");
  requestedData.put(SUB_BRAND_NAME, requestMap.containsKey(SUB_BRAND_NAME) ? request.getParameter(SUB_BRAND_NAME) : "");
  if (requestMap.containsKey(IPP)) {
   requestedData.put(IPP, request.getParameter(IPP));
  }
  return requestedData;
  
 }
 
 /**
  * Checks if is filter.
  *
  * @param requestedfields
  *         the requestedfields
  * @param request
  *         the request
  * @return true, if is filter
  */
 private boolean isFilter(final Map<String, String> requestedfields, SlingHttpServletRequest request) {
  String requestPath = request.getRequestPathInfo().getResourcePath();
  boolean isFilter = false;
  if (!requestedfields.get(FILTER).isEmpty()) {
   isFilter = true;
  } else if (!(requestPath.isEmpty()) && AdaptiveUtil.isAdaptive(request)) {
   String[] pagePathArr = requestPath.split(CommonConstants.SLASH_CHAR_LITERAL);
   requestedfields.put("Locale", pagePathArr.length > NUMERIC_THREE ? pagePathArr[NUMERIC_THREE] : "global");
   requestedfields.put("BrandName", pagePathArr[NUMERIC_TWO]);
   isFilter = false;
  }
  return isFilter;
 }
 
 /**
  * Sets the image map.
  *
  * @param map
  *         the map
  */
 private void setImageMap(Map<String, Object> map) {
  AdaptiveImageDTO image = new AdaptiveImageDTO();
  image.setPath((String) map.get(IndexingSchema.IMAGEURL));
  image.setFileName("");
  image.setExtension((String) map.get(IndexingSchema.IMAGEEXTENSION));
  image.setIsNotAdaptiveImage((String) map.get(IndexingSchema.ISNOTADAPTIVEIMAGE));
  image.setTitle((String) map.get(IndexingSchema.IMAGETITLE));
  image.setUrl((String) map.get(IndexingSchema.IMAGEPATH));
  image.setAltImage((String) map.get(IndexingSchema.IMAGEALTTEXT));
  map.put("image", image);
  map.remove(IndexingSchema.IMAGEURL);
  map.remove(IndexingSchema.IMAGENAME);
  map.remove(IndexingSchema.IMAGEEXTENSION);
  map.remove(IndexingSchema.ISNOTADAPTIVEIMAGE);
  map.remove(IndexingSchema.IMAGETITLE);
  map.remove(IndexingSchema.IMAGEPATH);
  map.remove(IndexingSchema.IMAGEALTTEXT);
 }
 
 /**
  * Gets the suggestions result.
  *
  * @param requestedfields
  *         the requestedfields
  * @param collection
  *         the collection
  * @param client
  *         the client
  * @param query
  *         the query
  * @param suggestions
  *         the suggestions
  * @param isJson
  *         the is json
  * @return the suggestions result
  * @throws UnsupportedEncodingException
  *          the unsupported encoding exception
  * @throws SolrServerException
  *          the solr server exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 private QueryResponse getSuggestionsResult(final Map<String, String> requestedfields, String collection, SolrClient client, final SolrQuery query,
   List<String> suggestions, Boolean isJson) throws UnsupportedEncodingException, SolrServerException, IOException, ConfigurationNotFoundException {
  final String sugg = new String(suggestions.get(0).getBytes(UTF_8), UTF_8);
  query.setQuery(sugg);
  return getQueryResponse(requestedfields, client, query, collection, false);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.SearchRetrivalService# getSearchResult(org.apache.sling.api.SlingHttpServletRequest,
  * java.lang.Boolean)
  */
 @Override
 public Map<String, Object> getSearchResult(SlingHttpServletRequest request, Boolean isJson) {
  String[] solrPublishDetails = solrConfig.getSolrPublishServerArray();
  String[] collectionsBrandsArray = solrConfig.getSolrCollectionsArray();
  final Map<String, String> requestedfields = getSlingResourceType(request);
  String id = request.getParameterMap().containsKey("SU") ? request.getParameter("SU") : StringUtils.EMPTY;
  String collection = "";
  collection = getCollection(collectionsBrandsArray, requestedfields);
  Map<String, Object> searchResultsMap = new HashMap<String, Object>();
  
  SolrClient client = new HttpSolrClient(solrPublishDetails[1]);
  // Build query.
  final SolrQuery query = new SolrQuery();
  try {
   
   final String param = new String(requestedfields.get(SEARCH_TERM).getBytes(SearchConstants.ISO_8859_1), SearchConstants.UTF_8);
   query.setQuery(ClientUtils.escapeQueryChars(param));
   LOG.debug("Final Search query term is---" + param);
   if (isFilter(requestedfields, request)) {
    query.addFilterQuery(getRequestParamsArray(request.getRequestParameterMap(), FILTER));
   }
   if (StringUtils.isNotEmpty(requestedfields.get(SEARCH_INPUT_PATH)) && StringUtils.isNotEmpty(requestedfields.get(SUB_BRAND_NAME))) {
    String[] subBrandFilters = getTagsRelation(requestedfields.get(SEARCH_INPUT_PATH), requestedfields.get(SUB_BRAND_NAME));
    if (ArrayUtils.isNotEmpty(subBrandFilters)) {
     query.addFilterQuery(subBrandFilters);
    }
    
   }
   if (StringUtils.isNotEmpty(id)) {
    query.addFilterQuery("id:" + "*" + id + "*");
   }
   if (StringUtils.isNotEmpty(param)) {
    final QueryResponse queryResponse = getQueryResponse(requestedfields, client, query, collection, false);
    List<String> suggestions = new ArrayList<String>();
    if (null != queryResponse && queryResponse.getResults().getNumFound() > 0) {
     LOG.debug("Found total {} results", queryResponse.getResults().getNumFound());
     SolrDocumentList results = queryResponse.getResults();
     searchResultsMap = getResultJson(queryResponse, searchResultsMap, results, isJson, false);
     searchResultsMap.put(SUGGESTION_EXIST, false);
     searchResultsMap.put(SUGGESTION_QUERY, requestedfields.get(SEARCH_TERM));
    } else {
     SpellCheckResponse spellCheckResponse = queryResponse.getSpellCheckResponse();
     boolean noSuggest = false;
     if (spellCheckResponse != null && !spellCheckResponse.isCorrectlySpelled()) {
      for (Suggestion suggestion : queryResponse.getSpellCheckResponse().getSuggestions()) {
       suggestions.addAll(suggestion.getAlternatives());
       LOG.debug(
         "original token: " + suggestion.getToken() + " - alternatives: " + suggestion.getAlternatives() + "suggestion 1 is :" + suggestions.get(0));
      }
      
      if (!suggestions.isEmpty()) {
       QueryResponse querySuggestion = getSuggestionsResult(requestedfields, collection, client, query, suggestions, isJson);
       if (null != querySuggestion && querySuggestion.getResults().getNumFound() > 0) {
        searchResultsMap.put(SearchConstants.SUGGESTIONS, suggestions.get(0));
        searchResultsMap.put(SUGGESTION_EXIST, true);
        searchResultsMap = getResultJson(querySuggestion, searchResultsMap, querySuggestion.getResults(), isJson, true);
        searchResultsMap.put(SUGGESTION_QUERY, suggestions.get(0));
        
       } else {
        noSuggest = true;
       }
      } else {
       noSuggest = true;
      }
     } else {
      noSuggest = true;
     }
     if (noSuggest) {
      LOG.debug("There is no suggested term of provided search field");
      Map<String, String> searchNoResult = new HashMap<String, String>();
      BrandMarketBean marketBean = new BrandMarketBean(requestedfields.get(BRANDNAME), requestedfields.get(LOCALE));
      searchNoResult = configurationService.getCategoryConfiguration(marketBean, "searchConfiguration");
      String[] filters = getRequestParamsArray(request.getRequestParameterMap(), FILTER);
      boolean noResult = true;
      for (String filter : filters) {
       if (StringUtils.contains(filter, "Tags")) {
        noResult = false;
       }
      }
      String privillegedData = StringUtils.EMPTY;
      if (noResult) {
       privillegedData = searchNoResult.containsKey("privillegedData") ? searchNoResult.get("privillegedData") : StringUtils.EMPTY;
      }
      String[] privillegedDatas = privillegedData.split(",");
      QueryResponse querySuggestion = null;
      int i = 0;
      for (String data : privillegedDatas) {
       i++;
       if (StringUtils.isNotEmpty(data)) {
        String[] datas = data.split("&");
        String contentType = datas[0];
        String tags = StringUtils.EMPTY;
        if (datas.length == NUMERIC_TWO) {
         tags = datas[1];
        }
        querySuggestion = getPrivillegedResults(query, collection, requestedfields, client, contentType, tags);
        if (null != querySuggestion) {
         if (querySuggestion.getResults().getNumFound() > 0) {
          LOG.debug("Found total {} new  {}", querySuggestion.getResults().getNumFound(), contentType);
          searchResultsMap = getResultJson(querySuggestion, searchResultsMap, querySuggestion.getResults(), isJson, true);
          searchResultsMap.put(SearchConstants.NORESULT, true);
          searchResultsMap.put(SUGGESTION_EXIST, true);
          searchResultsMap.put(SUGGESTION_QUERY, requestedfields.get(SEARCH_TERM));
          break;
         } else if (i < privillegedDatas.length) {
          continue;
         } else {
          querySuggestion = null;
         }
        }
       }
      }
      if (querySuggestion == null && noResult) {
       querySuggestion = getPrivillegedResults(query, collection, requestedfields, client, "Product", "dove/content/featured");
       if (null != querySuggestion && querySuggestion.getResults().getNumFound() > 0) {
        LOG.debug("Found total {} new  {}", querySuggestion.getResults().getNumFound(), "Product");
        searchResultsMap = getResultJson(querySuggestion, searchResultsMap, querySuggestion.getResults(), isJson, true);
        searchResultsMap.put(SearchConstants.NORESULT, true);
        searchResultsMap.put(SUGGESTION_EXIST, true);
        searchResultsMap.put(SUGGESTION_QUERY, requestedfields.get(SEARCH_TERM));
       } else {
        LOG.debug("There is no featured product/article page available.");
        searchResultsMap.put(SearchConstants.TOTALRESULTS, querySuggestion.getResults().getNumFound());
        searchResultsMap.put(SearchConstants.RESPONSE, "");
        searchResultsMap.put(SearchConstants.NORESULT, true);
        searchResultsMap.put(SUGGESTION_EXIST, false);
        searchResultsMap.put(SUGGESTION_QUERY, requestedfields.get(SEARCH_TERM));
        searchResultsMap.put(SearchConstants.FACET, getFacetsArray(querySuggestion));
       }
      } else if (!noResult) {
       LOG.debug("There is no featured product/article page available ");
       searchResultsMap.put(SearchConstants.TOTALRESULTS, queryResponse.getResults().getNumFound());
       searchResultsMap.put(SearchConstants.RESPONSE, "");
       searchResultsMap.put(SearchConstants.NORESULT, true);
       searchResultsMap.put(SUGGESTION_EXIST, false);
       searchResultsMap.put(SUGGESTION_QUERY, requestedfields.get(SEARCH_TERM));
       searchResultsMap.put(SearchConstants.FACET, getFacetsArray(queryResponse));
      }
     }
    }
   } else {
    LOG.debug("There is no featured product/article page available");
    searchResultsMap.put(SearchConstants.TOTALRESULTS, 0);
    searchResultsMap.put(SearchConstants.RESPONSE, "");
    searchResultsMap.put(SearchConstants.NORESULT, true);
    searchResultsMap.put(SUGGESTION_EXIST, false);
    searchResultsMap.put(SUGGESTION_QUERY, requestedfields.get(SEARCH_TERM));
    searchResultsMap.put(SearchConstants.FACET, new JSONObject());
   }
  } catch (IOException e) {
   LOG.error("Exception in getting suggestions from solr server .............." + ExceptionUtils.getStackTrace(e));
  } catch (SolrServerException e) {
   LOG.error("Exception in accessing  solr server .............." + ExceptionUtils.getStackTrace(e));
  } catch (ConfigurationNotFoundException e) {
   LOG.error("Exception in READING CONFIGURATIONS  .............." + ExceptionUtils.getStackTrace(e));
  } finally {
   try {
    client.close();
   } catch (IOException e) {
    LOG.error("Exception in closing client .............." + ExceptionUtils.getStackTrace(e));
   }
  }
  return searchResultsMap;
 }
 
 /**
  * Gets the request params array.
  * 
  * @param requestParamMap
  *         the request param map
  * @param key
  *         the key
  * @return the request params array
  */
 private String[] getRequestParamsArray(RequestParameterMap requestParamMap, String key) {
  
  Set<String> valueSet = new HashSet<String>();
  if (requestParamMap != null && requestParamMap.containsKey(key)) {
   for (RequestParameter rsParamMap : requestParamMap.get(key)) {
    if (StringUtils.equals(key, "fq")) {
     String param = rsParamMap.getString();
     if (StringUtils.contains(param, "Tags") || StringUtils.contains(param, "ContentType")) {
      String[] paramsArray = param.split(":");
      String paramKey = paramsArray[0];
      String paramValue = paramsArray[1];
      String pattern = "\"%s\"";
      String newParamKey = StringUtils.EMPTY;
      if (StringUtils.contains(paramValue, " OR ")) {
       for (String paramValuePart : paramValue.split("OR")) {
        paramValuePart = StringUtils.trim(paramValuePart);
        if (paramValuePart.startsWith("(")) {
         paramValuePart = StringUtils.substring(paramValuePart, 1, paramValuePart.length());
        }
        if (paramValuePart.endsWith(")")) {
         paramValuePart = StringUtils.substring(paramValuePart, 0, paramValuePart.length() - 1);
        }
        newParamKey += String.format(pattern, paramValuePart) + " OR ";
       }
       newParamKey = StringUtils.substring(newParamKey, 0, newParamKey.length() - NUMERIC_FOUR);
       String queryFormat = "(%s)";
       newParamKey = String.format(queryFormat, newParamKey);
       valueSet.add(paramKey + ":" + newParamKey);
      } else {
       paramValue = String.format(pattern, paramValue);
       valueSet.add(paramKey + ":" + paramValue);
      }
     }
    } else {
     valueSet.add(rsParamMap.getString());
    }
   }
  }
  return valueSet.toArray(new String[valueSet.size()]);
 }
 
 /**
  * Gets the facets array.
  *
  * @param queryResponse
  *         the query response
  * @return the facets array
  */
 public JSONObject getFacetsArray(QueryResponse queryResponse) {
  List<FacetField> facets = queryResponse.getFacetFields();
  JSONObject jsonObject = new JSONObject();
  List<Map<String, Object>> facetsList = new ArrayList<Map<String, Object>>();
  if (null != facets) {
   Iterator<FacetField> itrFacet = facets.iterator();
   while (itrFacet.hasNext()) {
    FacetField str = itrFacet.next();
    jsonObject.put(str.getName(), str.getValues() + "");
    for (Count value : str.getValues()) {
     Map<String, Object> facetObj = new HashMap<String, Object>();
     long count = value.getCount();
     String name = value.getName();
     facetObj.put("contentType", name);
     facetObj.put("totalResults", Long.toString(count));
     facetsList.add(facetObj);
    }
   }
  }
  Map<String, Object>[] facetsArray = (Map<String, Object>[]) new Map[facetsList.size()];
  jsonObject.put("contentTypes", facetsList.toArray(facetsArray));
  return jsonObject;
  
 }
 
 /**
  * Convert json array to list of string map.
  *
  * @param jsonArrayString
  *         the json array string
  * @return the list
  */
 public static List<Map<String, String>> convertJsonArrayToListOfStringMap(String jsonArrayString) {
  
  List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
  try {
   JSONArray jsonObject = new JSONArray(jsonArrayString);
   if (jsonObject != null) {
    for (int i = 0; i < jsonObject.length(); i++) {
     Object value = jsonObject.get(i);
     if (value instanceof org.apache.sling.commons.json.JSONObject) {
      Map<String, String> nestedMap = ComponentUtil.toMap((org.apache.sling.commons.json.JSONObject) value);
      if (nestedMap != null) {
       mapList.add(nestedMap);
      }
     }
    }
   }
  } catch (JSONException e) {
   LOG.error("Error while fetching JSON Object", e);
  }
  return mapList;
 }
 
 /**
  * Gets the privilleged results.
  *
  * @param query
  *         the query
  * @param collection
  *         the collection
  * @param requestedfields
  *         the requestedfields
  * @param client
  *         the client
  * @param contentType
  *         the content type
  * @param tags
  *         the tags
  * @return the privilleged results
  * @throws SolrServerException
  *          the solr server exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 private QueryResponse getPrivillegedResults(SolrQuery query, String collection, final Map<String, String> requestedfields, SolrClient client,
   String contentType, String tags) throws SolrServerException, IOException, ConfigurationNotFoundException {
  query = new SolrQuery();
  query.setQuery("*:*");
  query.addFilterQuery("ContentType:" + contentType);
  requestedfields.remove(FILTER);
  if (StringUtils.isNotEmpty(tags)) {
   query.setFilterQueries("Tags:" + tags);
  }
  return getQueryResponse(requestedfields, client, query, collection, true);
 }
 
 @Override
 public String[] getTagsRelation(String path, String subBrandName) {
  List<String> tagsQuery = new ArrayList<String>();
  ResourceResolver resolver = null;
  try {
   resolver = getResourceResolverByService("ieaadmin");
  } catch (LoginException e) {
   LOG.error("unable to get resource resolver", e);
  }
  List<Map<String, String>> resourceMap = ComponentUtil.getDialogProperties(resolver.resolve(path), "subBrands");
  for (Map<String, String> resourceData : resourceMap) {
   String subBrand = MapUtils.getString(resourceData, "subBrandName");
   if (StringUtils.equals(subBrandName, subBrand)) {
    String filterDisplayFormat = MapUtils.getString(resourceData, "tagsRelation");
    String inclusionTag = getTagString(MapUtils.getString(resourceData, "inclusionTags", StringUtils.EMPTY), resolver.adaptTo(TagManager.class),
      resolver, false, false, filterDisplayFormat, false);
    String exclusionTag = getTagString(MapUtils.getString(resourceData, "exclusionTags", StringUtils.EMPTY), resolver.adaptTo(TagManager.class),
      resolver, true, false, filterDisplayFormat, false);
    String matchingTag = getTagString(MapUtils.getString(resourceData, "matchingTag", StringUtils.EMPTY), resolver.adaptTo(TagManager.class),
      resolver, false, true, filterDisplayFormat, false);
    if (StringUtils.isNotEmpty(inclusionTag)) {
     tagsQuery.add(inclusionTag);
    }
    if (StringUtils.isNotEmpty(matchingTag)) {
     tagsQuery.add(matchingTag);
    }
    if (StringUtils.isNotEmpty(exclusionTag)) {
     tagsQuery.add(exclusionTag);
    }
   }
  }
  if (tagsQuery.isEmpty()) {
   return ArrayUtils.EMPTY_STRING_ARRAY;
  } else {
   String[] tagQueryArray = new String[tagsQuery.size()];
   return tagsQuery.toArray(tagQueryArray);
  }
 }
 
 private String getTagString(String tagID, TagManager tagManager, ResourceResolver resolver, boolean negate, boolean matching, String tagRelation,
   boolean children) {
  String fullTagsRel = StringUtils.EMPTY;
  String tagQueryFormat = "Tags:(%s) %s";
  String tagFormat = "\"%s\"";
  if (matching) {
   tagFormat = "*%s*";
  }
  if (negate) {
   tagQueryFormat = StringUtils.concatenate(new String[] { "-", tagQueryFormat });
  }
  
  for (String tag : tagID.split(",")) {
   String tagsRel = StringUtils.EMPTY;
   Tag tagSource = tagManager.resolve(tag);
   if (tagSource != null) {
    tagsRel += String.format(tagFormat, tagSource.getLocalTagID()) + " OR ";
    Iterator<Tag> tagChild = tagSource.listAllSubTags();
    while (children && tagChild.hasNext()) {
     tagsRel += String.format(tagFormat, tagChild.next().getLocalTagID()) + " OR ";
    }
   }
   if (StringUtils.isNotEmpty(tagsRel)) {
    if (!matching) {
     tagRelation = "AND";
    }
    tagsRel = String.format(tagQueryFormat, StringUtils.substring(tagsRel, 0, tagsRel.length() - NUMERIC_FOUR), tagRelation);
    fullTagsRel += " " + tagsRel;
   }
  }
  if (StringUtils.isNotEmpty(fullTagsRel)) {
   fullTagsRel = StringUtils.substring(fullTagsRel, 0, fullTagsRel.length() - tagRelation.length());
  }
  return fullTagsRel;
 }
 
 private ResourceResolver getResourceResolverByService(String serviceName) throws LoginException {
  Map<String, Object> resolverParamMap = new HashMap<String, Object>();
  
  resolverParamMap.put(ResourceResolverFactory.SUBSERVICE, serviceName);
  return factory.getServiceResourceResolver(resolverParamMap);
 }
 
}
