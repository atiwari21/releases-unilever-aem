/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.constants;

/**
 * The Class CommonConstants.
 */
public final class CommonConstants {
 
 /** The Constant ANALYTICS. */
 public static final String ANALYTICS = "analytics";
 
 /**
  * the constant for ",".
  */
 public static final String COMMA = ",";
 
 /**
  * the constant for "x".
  */
 public static final String CROSS = "x";
 /** The Constant TWENTY. */
 public static final int TWENTY = 20;
 
 /** The Constant FIFTY. */
 public static final int FIFTY = 50;
 
 /**
  * Constant for sling's default servlet.
  */
 public static final String DEFAULT_SERVLET_NAME = "sling/servlet/default";
 
 /** The Constant DOT_CHAR_LITERAL. */
 public static final String DOT_CHAR_LITERAL = ".";
 
 /**
  * Constant for 5.
  */
 public static final int FIVE = 5;
 
 /**
  * Constant for 4.
  */
 public static final int FOUR = 4;
 
 /** The Constant JCR_CONSTANTS. */
 public static final String JCR_CONSTANTS = "/jcr:content";
 
 /** The Constant NONPROD_DOMAIN. */
 public static final String NONPROD_DOMAIN = "nonprod_domain";
 
 /**
  * Constant for 1.
  */
 public static final int ONE = 1;
 
 /** The Constant PROD_DOMAIN. */
 public static final String PROD_DOMAIN = "prod_domain";
 
 /** The Constant READSERVICE. */
 public static final String READSERVICE = "readService";
 
 /**
  * Constant for 6.
  */
 public static final int SEVEN = 7;
 /**
  * Constant for 8.
  */
 public static final int EIGHT = 8;
 /**
  * Constant for 6.
  */
 public static final int SIX = 6;
 
 /** The Constant SLASH_CHAR. */
 public static final String SLASH_CHAR_LITERAL = "/";
 
 /**
  * Constant for 3.
  */
 public static final int THREE = 3;
 
 /**
  * Constant for 2.
  */
 public static final int TWO = 2;
 
 /** The Constant UNDERSCORE_CHAR. */
 public static final String UNDERSCORE_CHAR = "_";
 
 /**
  * Constant for 0.
  */
 public static final int ZERO = 0;
 
 /** Constant for jpeg */
 public static final String JPEG = "jpeg";
 
 /**
  * /** Constant for jpg
  */
 public static final String JPG = "jpg";
 
 /** Constant for jpeg */
 public static final String GIF = "gif";
 /** Constant for jpeg */
 public static final String PNG = "png";
 
 public static final String FORWARD_SLASH = "/";
 public static final String FULL_STOP = ".";
 
 /** The Constant ARTICLE_TEASER_TITLE. */
 public static final String ARTICLE_TEASER_TITLE = "teaserTitle";
 
 /** The Constant ARTICLE_TEASER_IMAGE. */
 public static final String ARTICLE_TEASER_IMAGE = "teaserImage";
 
 /** The Constant ARTICLE_TEASER_COPY. */
 public static final String ARTICLE_TEASER_COPY = "teaserCopy";
 
 /** The Constant EXTERNALIZER_DOMAIN_KEY. */
 public static final String EXTERNALIZER_DOMAIN_KEY = "externalizer_domain_key";
 
 /** The Constant BRAND. */
 public static final String BRAND = "brand";
 
 /** The Constant NAME. */
 public static final String NAME = "name";
 
 /** The Constant PAGE_EXTENSION. */
 public static final String PAGE_EXTENSION = ".html";
 
 /**
  * Identifier for holding the default service ranking value used in default view helper implementations.
  */
 public static final int DEFAULT_SERVCE_RANKING = 5000;
 
 /**
  * Instantiates a new common constants.
  */
 private CommonConstants() {
  
 }
}
