/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.GeoTag;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Image;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class AnchorRule.
 */
public class GeoTagRule extends ConfigurationSelectorRule {
 
 private static final String PROPERTY_MAP_GEOTAG = "map.geotag";
 
 /** The instance. */
 private static GeoTagRule instance = new GeoTagRule();
 
 /**
  * Instantiates a new anchor rule.
  */
 private GeoTagRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor rule
  */
 public static GeoTagRule create() {
  return instance;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  GeoTag geoTag = GeoTag.create();
  String script = (String) this.getProperty(PROPERTY_MAP_GEOTAG, node);
  if (StringUtils.isNotBlank(script)) {
   geoTag.withScript(script);
   
  }
  if (container instanceof Image) {
   ((Image) container).withGeoTag(geoTag);
  }
  
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Image.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the GeoTag rule
  */
 public static GeoTagRule createFrom(JSONObject configuration) {
  GeoTagRule geoTagRule = create();
  geoTagRule = (GeoTagRule) geoTagRule.withSelector(configuration.getString("selector"));
  JSONObject geoTagConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_MAP_GEOTAG);
  geoTagRule.withProperties(properties, geoTagConfiguration);
  return geoTagRule;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
