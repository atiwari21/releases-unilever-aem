/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The Class Pullquote.
 */

public class Pullquote extends TextContainer {
 
 /** The text. */
 private Cite attribution;
 
 /**
  * Instantiates a new blockquote.
  */
 private Pullquote() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static Pullquote create() {
  Pullquote h1 = new Pullquote();
  return h1;
 }
 
 /**
  * Gets the text.
  *
  * @return the text
  */
 public Cite getAttribution() {
  return attribution;
 }
 
 /**
  * Sets the unescaped text within the blockquote.
  *
  * @param text
  *         the text
  * @return this
  */
 public Pullquote withAttribution(String attribution) {
  this.attribution = (Cite) Cite.create().appendText(attribution);
  return this;
 }
 
 public Pullquote withAttribution(Cite attribution) {
     this.attribution = attribution;
     return this;
    }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement()
  */
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element asideElement = document.createElement("aside");
  asideElement.append(textToDOMDocumentFragment(document));
  return asideElement;
  
 }
 
}