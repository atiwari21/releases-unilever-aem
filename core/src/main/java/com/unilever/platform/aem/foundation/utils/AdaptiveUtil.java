/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.commons.lang.StringUtils;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.day.cq.commons.Externalizer;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class AdaptiveUtil is used for detecting whether the page selector has selector feature phone or not
 * 
 */
public class AdaptiveUtil {
 /**
  * Constant for identifier label feature phone
  */
 private static final String FEATUREPHONE = "adaptiveFeature";
 /**
  * Constant for setting default feature name
  */
 private static final String DEFAULT_FEATURE = "featurephone";
 /**
  * Constant for getting adaptive tenant for feature phone
  */
 private static final String ADAPTIVE_TENANT = "adaptiveTenant";
 /**
  * Constant for setting default adaptive tenant name
  */
 private static final String DEFAULT_TENANT = "feature-phone";
 /**
  * Constant for getting template original path
  */
 private static final String TEMPLATE_VIEW = "templateView";
 /**
  * Constant for character /
  */
 private static final String SLASH = "/";
 
 private static final Logger LOGGER = LoggerFactory.getLogger(AdaptiveUtil.class);
 
 /** The Constant EXTERNALIZER_DOMAIN_KEY. */
 private static final String EXTERNALIZER_DOMAIN_KEY = "externalizer_domain_key";
 
 /** The Constant BRAND. */
 private static final String BRAND = "brand";
 
 /**
  * constructor
  */
 private AdaptiveUtil() {
  
 }
 
 /**
  * isAdaptive method used to specify whether the given requested url has featurephone selector to identify the device is feature phone or not default
  * is false
  * 
  * @param request
  * @return
  */
 public static boolean isAdaptive(SlingHttpServletRequest request) {
  boolean isAdaptive = false;
  String[] pageSelectors = request.getRequestPathInfo().getSelectors();
  Resource resource = request.getResource();
  InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
  String adaptiveTenantName = valueMap.getInherited(FEATUREPHONE, DEFAULT_FEATURE);
  for (String selector : pageSelectors) {
   if (selector.equals(adaptiveTenantName)) {
    isAdaptive = true;
   }
  }
  return isAdaptive;
  
 }
 
 /**
  * getAdaptiveName method used to get the device name if it is a feature phone
  * 
  * @param request
  * @return
  */
 public static String getAdaptiveName(SlingHttpServletRequest request) {
  String adaptiveName = null;
  String[] pageSelectors = request.getRequestPathInfo().getSelectors();
  Resource resource = request.getResource();
  InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(resource);
  String adaptiveTenantName = valueMap.getInherited(FEATUREPHONE, DEFAULT_FEATURE);
  for (String selector : pageSelectors) {
   if (selector.equals(adaptiveTenantName)) {
    adaptiveName = adaptiveTenantName;
   }
  }
  return adaptiveName;
  
 }
 
 /**
  * This method used to get feature phone tenant name
  * 
  * @param request
  * @return
  */
 public static String getAdaptiveTenantName(SlingHttpServletRequest request) {
  Resource resource = request.getResource();
  PageManager pageManager = request.getResourceResolver().adaptTo(PageManager.class);
  InheritanceValueMap valueMap = new HierarchyNodeInheritanceValueMap(pageManager.getContainingPage(resource).getContentResource());
  return valueMap.getInherited(ADAPTIVE_TENANT, DEFAULT_TENANT);
 }
 
 /**
  * Returns the full url of page. Domain path will be fetched from externalizer service and relative path will be added to it.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param pagePathParam
  *         the page path param
  * @return the full url
  */
 public static String getFullURL(ResourceResolver resourceResolver, String pagePathParam) {
  LOGGER.debug("Inside getFullURL of ComponentUtil");
  String pagePath = pagePathParam;
  ConfigurationService configurationService = resourceResolver.adaptTo(ConfigurationService.class);
  String fullPath = StringUtils.EMPTY;
  
  if (StringUtils.startsWith(pagePath, SLASH)) {
   Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
   if (externalizer == null) {
    LOGGER.error("Missing Externalizer service");
    return pagePath;
   } else {
    String path = pagePath;
    if (StringUtils.contains(pagePath, CommonConstants.DOT_CHAR_LITERAL)) {
     path = path.substring(0, path.lastIndexOf(CommonConstants.DOT_CHAR_LITERAL));
    }
    Resource resource = resourceResolver.getResource(path);
    if (resource != null) {
     pagePath = resourceResolver.map(pagePath);
     PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
     Page page = pageManager.getContainingPage(resource);
     
     fullPath = setFullUrlPath(resourceResolver, pagePathParam, pagePath, configurationService, externalizer, page);
     if (!pagePath.contains(CommonConstants.DOT_CHAR_LITERAL)) {
      pagePath = pagePath + CommonConstants.PAGE_EXTENSION;
     }
     LOGGER.debug("Relative Path = " + pagePath);
     
    }
   }
  }
  if (!pagePath.contains(CommonConstants.DOT_CHAR_LITERAL)) {
   pagePath = pagePath + CommonConstants.PAGE_EXTENSION;
  }
  return StringUtils.isBlank(fullPath) ? pagePath : fullPath;
 }
 
 /**
  * This method used to get the feature phone template path
  * 
  * @param request
  * @return
  */
 public static String getAdaptiveTemplate(SlingHttpServletRequest request) {
  String templateView = request.getAttribute(TEMPLATE_VIEW).toString();
  int slashPosition = templateView.indexOf(SLASH);
  slashPosition = templateView.indexOf(SLASH, slashPosition + 1);
  int fourthSlaSh = templateView.indexOf(SLASH, slashPosition + 1);
  String finalTemplatePath = templateView.substring(templateView.indexOf(SLASH, fourthSlaSh + 1));
  String etcui = templateView.substring(templateView.indexOf(SLASH), fourthSlaSh + 1);
  String adaptiveTenantName = getAdaptiveTenantName(request);
  return etcui + adaptiveTenantName + finalTemplatePath;
 }
 
 private static String setFullUrlPath(ResourceResolver resourceResolver, String pagePathParam, String pagePath,
   ConfigurationService configurationService, Externalizer externalizer, Page page) {
  String externalizerKey = StringUtils.EMPTY;
  String fullPath = StringUtils.EMPTY;
  try {
   externalizerKey = configurationService.getConfigValue(page, BRAND, EXTERNALIZER_DOMAIN_KEY);
  } catch (ConfigurationNotFoundException ex) {
   LOGGER.error("Error generating the full url for pagePath = " + pagePathParam, ex);
  }
  if (!StringUtils.isBlank(externalizerKey)) {
   try {
    fullPath = externalizer.externalLink(resourceResolver, externalizerKey, pagePath);
   } catch (Exception e) {
    LOGGER.error("Exception in getting the externalizer url ");
    LOGGER.error(ExceptionUtils.getStackTrace(e));
   }
  }
  return fullPath;
 }
 
}
