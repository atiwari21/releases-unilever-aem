/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Class GlobalConfigurationUtility.
 */
public final class GlobalConfigurationUtility {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(GlobalConfigurationUtility.class);
 
 /**
  * Instantiates a new global configuration utility.
  */
 private GlobalConfigurationUtility() {
  LOGGER.info("Global Configuration Utility Constructor Called.");
 }
 
 /**
  * Gets the value from configuration.
  * 
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @param category
  *         the category
  * @param key
  *         the key
  * @return the value from configuration
  */
 public static String getValueFromConfiguration(ConfigurationService configurationService, Page page, String category, String key) {
  
  String value = StringUtils.EMPTY;
  try {
   value = configurationService.getConfigValue(page, category, key);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Not found for Category: " + category + " and key: " + key, e);
  }
  return value;
 }
 
 /**
  * Gets the value from configuration.
  * 
  * @param categoryName
  *         the category name
  * @param configurationService
  *         the configuration Service
  * @param currentPage
  *         the current page
  * @param key
  *         the key
  * @return the value from configuration
  */
 public static int getIntegerValueFromConfiguration(String categoryName, ConfigurationService configurationService, Page currentPage, String key) {
  
  int number = 0;
  String numberString = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, currentPage, categoryName, key);
  if (StringUtils.isNotBlank(numberString)) {
   try {
    number = Integer.parseInt(numberString);
   } catch (NumberFormatException e) {
    LOGGER.error("Error in integer conversion. key : " + key + "for category : " + categoryName + ". value = " + numberString);
   }
  }
  return number;
 }
 
 /**
  * Gets the map from configuration.
  * 
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @param category
  *         the category
  * @return the map from configuration
  */
 public static Map<String, String> getMapFromConfiguration(ConfigurationService configurationService, Page page, String category) {
  
  Map<String, String> map = new HashMap<String, String>();
  try {
   map = configurationService.getCategoryConfiguration(page, category);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Map not found for Category: " + category, e);
  }
  return map;
 }
 
 /**
  * Returns true only if locale matches with argument country code.
  *
  * @param page
  * @param configurationService
  *         the configuration service
  * @param category
  *         the category
  * @param key
  *         the key
  * @return boolean
  */
 public static boolean isJiaThisLocaleEnabled(Page page, ConfigurationService configurationService, String category, String key) {
  boolean localeFlag = false;
  BrandMarketBean brandMarketBean = new BrandMarketBean(page);
  String currentLocale = brandMarketBean.getMarket();
  String jiaThisLocales = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, category, key);
  if (StringUtils.isNotBlank(jiaThisLocales)) {
   jiaThisLocales = StringUtils.replace(jiaThisLocales, " ", "");
   String[] locale = jiaThisLocales.split(CommonConstants.COMMA);
   int i = 0;
   for (i = 0; i < locale.length; i++) {
    if (StringUtils.equals(currentLocale, locale[i])) {
     localeFlag = true;
     break;
    }
   }
  }
  return localeFlag;
 }
 
 /**
  * Gets the overridden config map.
  * 
  * @param configCat
  *         the config cat
  * @param page
  *         the page
  * @param valueMap
  *         the value map
  * @return the overridden config map
  */
 public static Map<String, Object> getOverriddenConfigMap(String configCat, Page page, Map<String, Object> valueMap) {
  ConfigurationService configurationService = page.adaptTo(ConfigurationService.class);
  try {
   Map<String, String> configMap = configurationService.getCategoryConfiguration(page, configCat);
   return getOverriddenConfigMap(configMap, valueMap);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("error getting config for {}", configCat, e);
   return null;
  }
 }
 
 /**
  * Gets the overridden config map.
  * 
  * @param configMap
  *         the config map
  * @param valueMap
  *         the value map
  * @return the overridden config map
  */
 public static Map<String, Object> getOverriddenConfigMap(Map<String, String> configMap, Map<String, Object> valueMap) {
  Map<String, Object> finalConfigMap = new HashMap<String, Object>();
  Iterator<String> configItr = configMap.keySet().iterator();
  while (configItr.hasNext()) {
   String key = configItr.next();
   String configValue = configMap.get(key) != null ? configMap.get(key) : "";
   Object propValue = valueMap.get(key) != null ? valueMap.get(key) : null;
   String[] valArr = StringUtils.split(configValue, ",");
   if (propValue == null) {
    if (valArr.length > 1) {
     finalConfigMap.put(key, valArr);
    } else {
     finalConfigMap.put(key, configValue);
    }
   } else {
    finalConfigMap.put(key, propValue);
   }
  }
  return finalConfigMap;
 }
 
 /**
  * Gets the overridden config val.
  * 
  * @param page
  *         the page
  * @param category
  *         the category
  * @param key
  *         the key
  * @param valueMap
  *         the value map
  * @return the overridden config val
  */
 public static String getOverriddenConfigVal(Page page, String category, String key, Map<String, Object> valueMap) {
  if (valueMap.get(key) == null) {
   String value = "";
   ConfigurationService configurationService = page.adaptTo(ConfigurationService.class);
   try {
    value = configurationService.getConfigValue(page, category, key);
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error(ExceptionUtils.getStackTrace(e));
   }
   return value;
  } else {
   return valueMap.get(key).toString();
  }
 }
 
 /**
  * Gets the config.
  *
  * @param configAdmin
  *         the config admin
  * @param key
  *         the key
  * @param pid
  *         the pid
  * @return the config
  */
 public static Object getConfig(ConfigurationAdmin configAdmin, String key, String pid) {
  try {
   if (configAdmin != null) {
    Configuration conf = configAdmin.getConfiguration(pid);
    if (conf != null && conf.getProperties() != null && conf.getProperties().get(key) != null) {
     return conf.getProperties().get(key);
    }
   }
  } catch (IOException e) {
   LOGGER.error("Error while accessing configuration :", e);
  }
  return null;
 }
}
