/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * The Class Interactive.
 *
 * @author pdwiv5
 */
public class Interactive extends ElementWithHTML implements Container {
 
 /** The Constant NO_MARGIN. */
 public static final String NO_MARGIN = "no-margin";
 
 /** The Constant COLUMN_WIDTH. */
 public static final String COLUMN_WIDTH = "column-width";
 
 /** The caption. */
 private Caption caption;
 
 /** The width. */
 private int width;
 
 /** The height. */
 private int height;
 
 /** The source. */
 private String source;
 
 /** The margin. */
 private String margin;
 
 /**
  * Instantiates a new interactive.
  */
 private Interactive() {
  
 }
 
 /**
  * Gets the caption.
  *
  * @return the caption
  */
 public Caption getCaption() {
  return caption;
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static Interactive create() {
  return new Interactive();
 }
 
 /**
  * With caption.
  *
  * @param caption
  *         the caption
  * @return the interactive
  */
 public Interactive withCaption(Caption caption) {
  this.caption = caption;
  return this;
 }
 
 /**
  * Gets the width.
  *
  * @return the width
  */
 public int getWidth() {
  return width;
 }
 
 /**
  * With width.
  *
  * @param width
  *         the width
  * @return the interactive
  */
 public Interactive withWidth(int width) {
  this.width = width;
  return this;
 }
 
 /**
  * Gets the height.
  *
  * @return the height
  */
 public int getHeight() {
  return height;
 }
 
 /**
  * With height.
  *
  * @param height
  *         the height
  * @return the interactive
  */
 public Interactive withHeight(int height) {
  this.height = height;
  return this;
 }
 
 /**
  * Gets the source.
  *
  * @return the source
  */
 public String getSource() {
  return source;
 }
 
 /**
  * With source.
  *
  * @param source
  *         the source
  * @return the interactive
  */
 public Interactive withSource(String source) {
  this.source = source;
  return this;
 }
 
 /**
  * Gets the margin.
  *
  * @return the margin
  */
 public String getMargin() {
  return margin;
 }
 
 /**
  * With margin.
  *
  * @param margin
  *         the margin
  * @return the interactive
  */
 public Interactive withMargin(String margin) {
  this.margin = margin;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  return Collections.<Object> emptyList();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement()
  */
 @Override
 public Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public Element toDOMElement(Document documentOrg) {
  Document document = documentOrg;
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element figure = document.createElement("figure");
  org.jsoup.nodes.Element iframe = document.createElement("iframe");
  figure.appendChild(iframe);
  figure.addClass("op-interactive");
  if (this.caption != null) {
   figure.appendChild(this.caption.toDOMElement(document));
  }
  if (StringUtils.isNotBlank(this.source)) {
   iframe.attr("src", this.source);
  }
  if (StringUtils.isNotBlank(this.margin)) {
   iframe.attr("class", this.margin);
  }
  return figure;
 }
 
}
