/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Caption;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.H3;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

public class H3Rule extends ConfigurationSelectorRule {
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 public String[] getContextClass() {
  return new String[] { Header.getClassName(), Caption.getClassName(), InstantArticle.getClassName() };
 }
 
 public static H3Rule create() {
  return new H3Rule();
 }
 
 public static Rule createFrom(JSONObject configuration) {
  H3Rule h3Rule = H3Rule.create();
  h3Rule.withSelector(configuration.getString("selector"));
  h3Rule.withProperties(getCaptionProperties(), configuration);
  return h3Rule;
 }
 
 static List<String> getCaptionProperties() {
  List<String> properties = new ArrayList<String>();
  properties.add(Caption.POSITION_BELOW);
  properties.add(Caption.POSITION_CENTER);
  properties.add(Caption.POSITION_ABOVE);
  properties.add(Caption.ALIGN_LEFT);
  properties.add(Caption.ALIGN_CENTER);
  properties.add(Caption.ALIGN_RIGHT);
  return properties;
 }
 
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  H3 h3 = H3.create();
  Container tempContainer= container;
  if (container instanceof Header) {
   tempContainer = ((Header) tempContainer).withSubTitle(h3);
  }else  {
   tempContainer = transformer.getInstantArticle().addChild(h3);
  }
  
  if (this.getProperty(Caption.POSITION_BELOW, node) != null) {
   h3.withPosition(Caption.POSITION_BELOW);
  }
  if (this.getProperty(Caption.POSITION_CENTER, node) != null) {
   h3.withPosition(Caption.POSITION_CENTER);
  }
  if (this.getProperty(Caption.POSITION_ABOVE, node) != null) {
   h3.withPosition(Caption.POSITION_ABOVE);
  }
  if (this.getProperty(Caption.ALIGN_LEFT, node) != null) {
   h3.withPosition(Caption.ALIGN_LEFT);
  }
  if (this.getProperty(Caption.ALIGN_CENTER, node) != null) {
   h3.withPosition(Caption.ALIGN_CENTER);
  }
  if (this.getProperty(Caption.ALIGN_RIGHT, node) != null) {
   h3.withPosition(Caption.ALIGN_RIGHT);
  }
  transformer.transform(h3, node);
  return container;
 }
 
}
