/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * The Class Slideshow.
 */
public class Slideshow extends Audible implements Container {
 
 /** Caption The caption for the Slideshow. */
 private Caption caption;
 
 /** List<Image>the images hosted on web that will be shown on the slideshow. */
 private List<Image> articleImages = new ArrayList<Image>();
 
 /**
  * Instantiates a new slideshow.
  */
 private Slideshow() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the slideshow
  */
 public static Slideshow create() {
  return new Slideshow();
 }
 
 /**
  * Gets the caption.
  *
  * @return the caption
  */
 public Caption getCaption() {
  return caption;
 }
 
 /**
  * With caption.
  *
  * @param caption
  *         the caption
  * @return the slideshow
  */
 public Slideshow withCaption(Caption caption) {
  this.caption = caption;
  return this;
 }
 
 /**
  * Gets the images.
  *
  * @return the images
  */
 public List<Image> getImages() {
  return articleImages;
 }
 
 /**
  * With images.
  *
  * @param articleImages
  *         the article_images
  * @return the slideshow
  */
 public Slideshow withImages(List<Image> articleImages) {
  this.articleImages = articleImages;
  return this;
 }
 
 /**
  * Adds the image.
  *
  * @param image
  *         the image
  * @return the slideshow
  */
 public Slideshow addImage(Image image) {
  this.articleImages.add(image);
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  List<Object> children = new ArrayList<Object>();
  if (!this.articleImages.isEmpty()) {
   for (Image image : this.articleImages) {
    children.add(image);
   }
  }
  if (this.caption != null) {
   children.add(this.caption);
  }
  return children;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.elements.Audible#withAudio(com.unilever.platform.aem.foundation.fbinstantarticle.elements.
  * Audio)
  */
 @Override
 public Audible withAudio(Audio audio) {
  return null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement()
  */
 @Override
 public Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public Element toDOMElement(Document document1) {
  Document document = document1;
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element slideShowContainer = document.createElement("figure");
  slideShowContainer.addClass("op-slideshow");
  if (!this.articleImages.isEmpty()) {
   for (Image image : this.articleImages) {
    org.jsoup.nodes.Element slideshowImage = image.toDOMElement(document);
    slideShowContainer.appendChild(slideshowImage);
   }
  }
  
  return slideShowContainer;
 }
 
}
