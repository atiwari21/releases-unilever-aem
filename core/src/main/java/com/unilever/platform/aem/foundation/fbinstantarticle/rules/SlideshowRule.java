/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Slideshow;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

public class SlideshowRule extends ConfigurationSelectorRule {
 
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Slideshow sildeshow = Slideshow.create();
  ((InstantArticle) container).addChild(sildeshow);
  transformer.transform(sildeshow, node);
  return container;
 }
 
 /**
  * Creates the.
  *
  * @return the Slideshow rule
  */
 public static SlideshowRule create() {
  return new SlideshowRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the Slideshow rule
  */
 public static SlideshowRule createFrom(JSONObject configuration) {
  SlideshowRule slideshowRule = create();
  slideshowRule = (SlideshowRule) slideshowRule.withSelector(configuration.getString("selector"));
  return slideshowRule;
 }
 
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName() };
 }
 
}
