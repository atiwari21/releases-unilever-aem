/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.product.importers;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.pim.api.ProductImporter;

/**
 * An abstract base class used for writing product importers.
 */
@Component(componentAbstract = true, metatype = true)
@Service
public abstract class UNIAbstractProductImporter extends UNIAbstractImporter implements ProductImporter {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(UNIAbstractProductImporter.class);
 
 /**
  * The location where imported product stores are created.
  */
 protected String basePath = "/etc/commerce/products";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.pim.api.ProductImporter#importProducts(org.apache .sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 public void importProducts(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  
  if (!validateInput(request, response)) {
   return;
  }
  
  ResourceResolver resourceResolver = request.getResourceResolver();
  String storeName = request.getParameter("storeName");
  String storePath = request.getParameter("storePath");
  String provider = request.getParameter("provider");
  initTicker(request.getParameter("tickertoken"));
  
  Boolean incrementalImport = false;
  if (request.getParameter("incrementalImport") != null) {
   incrementalImport = true;
  }
  
  LOG.debug("Calling do import");
  
  run(resourceResolver, storePath != null ? storePath : basePath, storeName, incrementalImport, provider, request, response);
 }
 
 /**
  * Validate any input required to run the importer. Implementation to be supplied by concrete classes.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 protected abstract boolean validateInput(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException;
 
}
