/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * This class sets replicated date to preview and production in touch ui list view.
 */
@SlingServlet(paths = { "/bin/addPreviewProductionReplication" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.AddCustomColumnsToListView", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the last replication to preview and production in touch ui list view ", propertyPrivate = false) })
public class AddCustomColumnsToListView extends SlingAllMethodsServlet {
 
 private static final String CLOSE_BRACKET = ")";
 
 private static final String OPEN_BRACKET = "(";
 
 private static final String BY = "By";
 
 private static final String EMPTY_STRING = StringUtils.EMPTY;
 
 private static final String LAST_REPLICATED_TO_PREVIEW = "lastReplicatedToPreview";
 
 private static final String REPLICATION_TO_PRODUCTION = "replicationToProduction";
 
 private static final String REPLICATION_TO_PREVIEW = "replicationToPreview";
 
 private static final String LAST_REPLICATED_TO_PRODUCTION = "lastReplicatedToProduction";
 
 /** The Constant PATHS. */
 private static final String PATH = "path";
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 6359259544089117959L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AddCustomColumnsToListView.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  String pagePath = request.getParameter(PATH);
  Resource resource = StringUtils.isNotBlank(pagePath) ? resourceResolver.getResource(pagePath + CommonConstants.JCR_CONSTANTS) : null;
  LOGGER.debug("page path", pagePath);
  try {
   if (resource != null) {
    Node pageNode = resource.adaptTo(Node.class);
    
    writer.object();
    if (pageNode.hasProperty(LAST_REPLICATED_TO_PREVIEW)) {
     setLastReplicatedTpPreviewProduction(writer, pageNode, LAST_REPLICATED_TO_PRODUCTION, REPLICATION_TO_PRODUCTION);
     setLastReplicatedTpPreviewProduction(writer, pageNode, LAST_REPLICATED_TO_PREVIEW, REPLICATION_TO_PREVIEW);
    } else {
     writer.key(REPLICATION_TO_PREVIEW).value(EMPTY_STRING);
     writer.key(REPLICATION_TO_PRODUCTION).value(EMPTY_STRING);
    }
    writer.endObject();
    
   } else {
    writer.key(REPLICATION_TO_PREVIEW).value(EMPTY_STRING);
    writer.key(REPLICATION_TO_PRODUCTION).value(EMPTY_STRING);
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error while getting resource node ", e);
  } catch (JSONException e) {
   LOGGER.error("Error while parsing JSON object", e);
  }
  
 }
 
 /**
  * sets replication action date of preview and production with user id's
  * 
  * @param writer
  * @param pageNode
  * @param key
  */
 private void setLastReplicatedTpPreviewProduction(JSONWriter writer, Node pageNode, String key, String replicateKey) {
  try {
   String tempFormat = "dd-MMM-YYYY HH:mm";
   String dateFormatter = "yyyy-MM-dd'T'HH:mm:ss";
   SimpleDateFormat dateFormat = new SimpleDateFormat(tempFormat);
   SimpleDateFormat needDateFormat = new SimpleDateFormat(dateFormatter);
   String dateInString = pageNode.getProperty(key).getValue().getString();
   Date dateObject = StringUtils.isNotBlank(dateInString) ? needDateFormat.parse(dateInString) : null;
   String dateWithUserId = (dateObject != null)
     ? dateFormat.format(dateObject) + OPEN_BRACKET + pageNode.getProperty(key + BY).getValue().getString() + CLOSE_BRACKET : StringUtils.EMPTY;
   writer.key(replicateKey).value(dateWithUserId);
  } catch (ParseException e) {
   LOGGER.warn("Error while getting resource node.", e);
  } catch (RepositoryException e) {
   LOGGER.error("Error while getting resource node", e);
  } catch (JSONException e) {
   LOGGER.error("Error while parsing JSON object", e);
  }
 }
}
