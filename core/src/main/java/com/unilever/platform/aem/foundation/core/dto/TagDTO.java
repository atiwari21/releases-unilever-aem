/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

/**
 * The Class TagDTO.
 */
public class TagDTO {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The tag name. */
 private String tagName;
 
 /** The tag title. */
 private String tagTitle;
 
 /** The tag path. */
 private String tagPath;
 
 /**
  * Gets the tag name.
  *
  * @return the tag name
  */
 public String getTagName() {
  return this.tagName;
 }
 
 /**
  * Sets the tag name.
  *
  * @param tagName
  *         the new tag name
  */
 public void setTagName(String tagName) {
  this.tagName = tagName;
 }
 
 /**
  * Gets the tag title.
  *
  * @return the tag title
  */
 public String getTagTitle() {
  return this.tagTitle;
 }
 
 /**
  * Sets the tag title.
  *
  * @param tagTitle
  *         the new tag title
  */
 public void setTagTitle(String tagTitle) {
  this.tagTitle = tagTitle;
 }
 
 /**
  * Gets the tag path.
  *
  * @return the tag path
  */
 public String getTagPath() {
  return this.tagPath;
 }
 
 /**
  * Sets the tag path.
  *
  * @param tagPath
  *         the new tag path
  */
 public void setTagPath(String tagPath) {
  this.tagPath = tagPath;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see java.lang.Object#toString()
  */
 @Override
 public String toString() {
  return "TagDTO [tagName=" + tagName + ", tagTitle=" + tagTitle + ", tagPath=" + tagPath + "]";
 }
}
