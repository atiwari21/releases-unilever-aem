/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.product.importers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceConstants;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.launcher.ConfigEntry;
import com.adobe.granite.workflow.launcher.WorkflowLauncher;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrObservationThrottle;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.wcm.api.NameConstants;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;

/**
 * An abstract base class used for writing commerce importers.
 */
@Component(componentAbstract = true, metatype = true)
@Service
public abstract class UNIAbstractImporter {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(UNIAbstractImporter.class);
 
 /** The event admin. */
 @Reference
 EventAdmin eventAdmin = null;
 
 /**
  * The (approximate) number of nodes to create in a single batch before saving the session.
  *
  * Configurable via the "Save Batch Size" property of the concrete OSGi component. Defaults to 1,000.
  */
 private int saveBatchSize;
 
 /** The Constant DEFAULT_SAVE_BATCH_SIZE. */
 private static final int DEFAULT_SAVE_BATCH_SIZE = 1000;
 
 /** The Constant SAVE_BATCH_SIZE_PROP_NAME. */
 @Property(label = "Save Batch Size", description = "Approximate number of nodes to batch between session saves", intValue = DEFAULT_SAVE_BATCH_SIZE)
 public static final String SAVE_BATCH_SIZE_PROP_NAME = "cq.commerce.importer.savebatchsize";
 
 /**
  * The (approximate) number of nodes to create before pausing to allow the observation manager to catch up.
  *
  * Configurable via the "Throttle Batch Size" property of the concrete OSGi component. Defaults to 50,000.
  */
 private int throttleBatchSize;
 
 /** The Constant DEFAULT_THROTTLE_BATCH_SIZE. */
 private static final int DEFAULT_THROTTLE_BATCH_SIZE = 50000;
 
 /** The Constant THROTTLE_BATCH_SIZE_PROP_NAME. */
 @Property(label = "Throttle Batch Size", description = "Approximate number of nodes between pauses for observation manager", intValue = DEFAULT_THROTTLE_BATCH_SIZE)
 public static final String THROTTLE_BATCH_SIZE_PROP_NAME = "cq.commerce.importer.throttlebatchsize";
 
 /**
  * The (approximate) maximum number of paths included in each PRODUCT_PAGE_ADDED, PRODUCT_PAGE_MODIFIED or PRODUCT_PAGE_DELETED event.
  */
 private int eventBatchSize = UNICommerceConstants.THOUNSAND;
 
 /**
  * The number of messages allowed in the response. Large imports generate too many messages for the browser to render in reasonable time, but
  * individual messages can be useful when testing on smaller imports.
  *
  * Configurable via the "Message Cap" property of the concrete OSGi component. Defaults to 1,000. Set to 0 to return only summary messages.
  */
 private int messageCap;
 
 /** The Constant DEFAULT_MESSAGE_CAP. */
 private static final int DEFAULT_MESSAGE_CAP = 1000;
 
 /** The Constant MESSAGE_CAP_PROP_NAME. */
 @Property(label = "Message Cap", description = "Maximum number of messages to return in response", intValue = DEFAULT_MESSAGE_CAP)
 public static final String MESSAGE_CAP_PROP_NAME = "cq.commerce.importer.messagecap";
 
 /** The save batch count. */
 private int saveBatchCount = 0;
 
 /** The throttle batch count. */
 private int throttleBatchCount = 0;
 
 /** The throttle. */
 private JcrObservationThrottle throttle = null;
 
 /** The disabled workflows. */
 private Set<String> disabledWorkflows = new HashSet<String>();
 
 /** The event queues. */
 private Map<String, Set<String>> eventQueues = new HashMap<String, Set<String>>();
 
 /** The messages. */
 private List<String> messages;
 
 /** The error count. */
 private int errorCount;
 
 /** The ticker token. */
 private String tickerToken = null;
 
 /** The ticker message. */
 private String tickerMessage;
 
 /** The ticker complete. */
 private boolean tickerComplete;
 
 /**
  * Activate.
  *
  * @param ctx
  *         the ctx
  * @throws Exception
  *          the exception
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  saveBatchSize = PropertiesUtil.toInteger(ctx.getProperties().get(SAVE_BATCH_SIZE_PROP_NAME), DEFAULT_SAVE_BATCH_SIZE);
  throttleBatchSize = PropertiesUtil.toInteger(ctx.getProperties().get(THROTTLE_BATCH_SIZE_PROP_NAME), DEFAULT_THROTTLE_BATCH_SIZE);
  messageCap = PropertiesUtil.toInteger(ctx.getProperties().get(MESSAGE_CAP_PROP_NAME), DEFAULT_MESSAGE_CAP);
 }
 
 /**
  * Execute the main body of the importer. This method sets up the store itself, and brackets the concrete implementation's doImport method with
  * performance-enhancing facilities.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param basePath
  *         the base path
  * @param storeName
  *         the store name
  * @param incrementalImport
  *         Indicates import should be applied to existing store as a delta
  * @param provider
  *         the commerceProvider to be used for the store
  * @param request
  *         the request
  * @param response
  *         the response
  */
 protected void run(ResourceResolver resourceResolver, String basePath, String storeName, boolean incrementalImport, String provider,
   SlingHttpServletRequest request, SlingHttpServletResponse response) {
  
  messages = new ArrayList<String>();
  errorCount = 0;
  incrementalImport = true;
  
  String filePath = request.getParameter(UNICommerceConstants.CSVPATH);
  String extension = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());
  
  if (StringUtils.isNotBlank(extension) && ("csv").equalsIgnoreCase(extension)) {
   incrementalImport = false;
  }
  
  try {
   Node rootNode = setupStore(resourceResolver, basePath, storeName, !incrementalImport, provider);
   
   disableWorkflows(resourceResolver);
   openThrottle(rootNode);
   
   doImport(resourceResolver, rootNode, incrementalImport, request, response);
  } catch (Exception e) {
   LOG.error("Error while running import", e);
  } finally {
   tickerComplete = true;
   checkpoint(resourceResolver.adaptTo(Session.class), true);
   closeThrottle();
   reenableWorkflows(resourceResolver);
  }
 }
 
 /**
  * Initializes the store, creating it if necessary, and deleting any existing content if not.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param basePath
  *         the base path
  * @param storeName
  *         the store name
  * @param clear
  *         true if any existing products should be cleared from the store
  * @param provider
  *         the provider
  * @return the node
  */
 protected Node setupStore(ResourceResolver resourceResolver, String basePath, String storeName, boolean clear, String provider) {
  Session session = resourceResolver.adaptTo(Session.class);
  
  String storePath = basePath;
  if (StringUtils.isNotEmpty(storeName)) {
   storePath += "/" + mangleName(storeName);
  }
  
  Resource rootResource = resourceResolver.getResource(storePath);
  Node rootNode = null;
  
  try {
   if (rootResource != null) {
    rootNode = rootResource.adaptTo(Node.class);
    
    if (clear && rootNode.hasNodes()) {
     NodeIterator it = rootNode.getNodes();
     while (it.hasNext()) {
      Node n = it.nextNode();
      if (!NameConstants.NT_PAGE.equals(n.getProperty(JcrConstants.JCR_PRIMARYTYPE).getString())) {
       n.remove();
      }
     }
    }
   } else {
    rootNode = JcrUtil.createPath(storePath, false, JcrResourceConstants.NT_SLING_FOLDER, JcrResourceConstants.NT_SLING_FOLDER, session, false);
    if (StringUtils.isNotEmpty(storeName)) {
     // In many cases mangleName() will have done serious damage
     // to the given name (particularly if
     // it was multi-byte); the least we can do is set the title
     // to the correct name:
     rootNode.setProperty(JcrConstants.JCR_TITLE, storeName);
    }
   }
   rootNode.setProperty(CommerceConstants.PN_COMMERCE_PROVIDER, provider);
   session.save();
  } catch (Exception e) {
   LOG.error("Failed to initialize store: ", e);
  }
  
  return rootNode;
 }
 
 /**
  * Run the actual import. Implementation to be supplied by concrete classes.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storeRoot
  *         the store root
  * @param incrementalImport
  *         Indicates import should be applied to existing store as a delta
  * @param request
  *         the request
  * @param response
  *         the response
  * @throws RepositoryException
  *          the repository exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 protected abstract void doImport(ResourceResolver resourceResolver, Node storeRoot, boolean incrementalImport, SlingHttpServletRequest request,
   SlingHttpServletResponse response) throws RepositoryException, IOException;
 
 //
 // Performance considerations for large imports.
 // 1) only save session every SAVE_BATCH_SIZE nodes
 // 2) pause to allow observation manager to catch up every
 // THROTTLE_BATCH_SIZE nodes
 //
 
 /**
  * Open throttle.
  *
  * @param storeRoot
  *         the store root
  * @throws RepositoryException
  *          the repository exception
  */
 protected void openThrottle(Node storeRoot) throws RepositoryException {
  throttle = new JcrObservationThrottle(JcrUtil.createUniqueNode(storeRoot, "temp", JcrConstants.NT_UNSTRUCTURED, storeRoot.getSession()));
  throttle.open();
 }
 
 /**
  * Close throttle.
  */
 protected void closeThrottle() {
  if (throttle != null) {
   throttle.close();
  }
 }
 
 /**
  * Should be called after each node creation. The flush parameter can be used to force session saving and event dispatch (for instance at the end of
  * an import), otherwise saves and event dispatch are batched.
  *
  * @param session
  *         the session
  * @param flush
  *         the flush
  */
 protected void checkpoint(Session session, boolean flush) {
  saveBatchCount++;
  throttleBatchCount++;
  
  if (saveBatchCount > saveBatchSize || flush) {
   setCheckpoint(session);
  }
  
  for (String eventName : eventQueues.keySet()) {
   setEvent(eventName, flush);
  }
  
 }
 
 /**
  * Sets the event.
  *
  * @param eventName
  *         the event name
  * @param flush
  *         the flush
  */
 private void setEvent(String eventName, boolean flush) {
  Set<String> paths = eventQueues.get(eventName);
  if (paths.size() > eventBatchSize || (flush && !(paths.isEmpty()))) {
   if (eventAdmin != null) {
    Map<String, Object> eventProperties = new HashMap<String, Object>();
    eventProperties.put("paths", paths.toArray(new String[paths.size()]));
    eventAdmin.postEvent(new Event(eventName, eventProperties));
   }
   paths.clear();
  }
 }
 
 /**
  * Sets the checkpoint.
  *
  * @param session
  *         the new checkpoint
  */
 private void setCheckpoint(Session session) {
  if (StringUtils.isNotEmpty(tickerToken)) {
   try {
    Node node = JcrUtils.getOrCreateByPath("/tmp/commerce/tickers/import_" + tickerToken, JcrConstants.NT_UNSTRUCTURED, session);
    node.setProperty("message", tickerMessage);
    node.setProperty("errorCount", errorCount);
    node.setProperty("complete", tickerComplete);
   } catch (Exception e) {
    LOG.error("ERROR updating ticker", e);
   }
  }
  
  try {
   session.save();
   saveBatchCount = 0;
  } catch (Exception e) {
   logMessage("ERROR saving session", false);
   errorCount += saveBatchCount;
   LOG.error("ERROR saving session", e);
  }
  
  if (throttleBatchCount > throttleBatchSize) {
   try {
    throttle.waitForEvents();
    throttleBatchCount = 0;
   } catch (RepositoryException e) {
    // keep calm and carry on...
    LOG.error("Error during checkpoint : ", e);
   }
  }
 }
 
 //
 // Importers may also choose to disable some workflows while importing,
 // either for performance
 // reasons or for process reasons. Simply override the predicate and return
 // true for those
 // workflows that should be disabled.
 //
 
 /**
  * Disable workflow predicate.
  *
  * @param workflowConfigEntry
  *         the workflow config entry
  * @return true, if successful
  */
 protected boolean disableWorkflowPredicate(ConfigEntry workflowConfigEntry) {
  LOG.debug("Config entry is  : " + workflowConfigEntry.toString());
  return false;
 }
 
 /**
  * Disable workflows.
  *
  * @param resourceResolver
  *         the resource resolver
  */
 protected void disableWorkflows(ResourceResolver resourceResolver) {
  try {
   WorkflowLauncher launcher = resourceResolver.adaptTo(WorkflowLauncher.class);
   Iterator<ConfigEntry> entries = launcher.getConfigEntries();
   while (entries.hasNext()) {
    ConfigEntry entry = entries.next();
    if (entry.isEnabled() && disableWorkflowPredicate(entry)) {
     entry.setEnabled(false);
     launcher.editConfigEntry(entry.getId(), entry);
     disabledWorkflows.add(entry.getId());
    }
   }
  } catch (WorkflowException e) {
   LOG.error("Error while disabling workflows", e);
  }
 }
 
 /**
  * Reenable workflows.
  *
  * @param resourceResolver
  *         the resource resolver
  */
 protected void reenableWorkflows(ResourceResolver resourceResolver) {
  try {
   WorkflowLauncher launcher = resourceResolver.adaptTo(WorkflowLauncher.class);
   Iterator<ConfigEntry> entries = launcher.getConfigEntries();
   while (entries.hasNext()) {
    ConfigEntry entry = entries.next();
    if (disabledWorkflows.contains(entry.getId())) {
     entry.setEnabled(true);
     launcher.editConfigEntry(entry.getId(), entry);
    }
   }
  } catch (WorkflowException e) {
   LOG.error("Error while re-enabling workflows", e);
  }
 }
 
 //
 // Some utility routines.
 //
 
 /**
  * Mangle name.
  *
  * @param name
  *         the name
  * @return the string
  */
 protected static String mangleName(String name) {
  return StringUtils.isEmpty(name) ? "" : JcrUtil.createValidName(name.trim().replace(" ", "-"));
 }
 
 /**
  * Log message.
  *
  * @param message
  *         the message
  * @param isError
  *         the is error
  */
 protected void logMessage(String message, boolean isError) {
  if (messages.size() < messageCap) {
   messages.add(message);
  }
  if (isError) {
   errorCount++;
  }
 }
 
 protected void initTicker(String tickerToken) {
  this.tickerToken = tickerToken;
  tickerMessage = "";
  tickerComplete = false;
 }
 
 /**
  * Update ticker.
  *
  * @param tickerMessage
  *         the ticker message
  */
 protected void updateTicker(String tickerMessage) {
  this.tickerMessage = tickerMessage;
 }
 
 /**
  * Respond with messages.
  *
  * @param response
  *         the response
  * @param summary
  *         the summary
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 protected void respondWithMessages(SlingHttpServletResponse response, String summary, List<String> messagesList) throws IOException {
  
  response.setContentType("text/html");
  response.setCharacterEncoding("UTF-8");
  
  PrintWriter pw = response.getWriter();
  pw.println("<html><body>");
  pw.println("<pre>");
  pw.println(summary);
  pw.println("");
  if (messagesList != null) {
   for (String msg : messagesList) {
    pw.println(msg);
   }
  }
  
  pw.println("</pre>");
  pw.println("</body></html>");
  pw.flush();
 }
}
