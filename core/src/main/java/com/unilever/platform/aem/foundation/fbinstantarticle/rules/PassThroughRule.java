/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

public class PassThroughRule extends ConfigurationSelectorRule {
 
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  return transformer.transform(container, node);
 }
 
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticleElement.getClassName() };
 }
 
 public static PassThroughRule create() {
  return new PassThroughRule();
 }
 
 public static PassThroughRule createFrom(JSONObject configuration) {
  return (PassThroughRule) create().withSelector(configuration.getString("selector"));
 }
 
}
