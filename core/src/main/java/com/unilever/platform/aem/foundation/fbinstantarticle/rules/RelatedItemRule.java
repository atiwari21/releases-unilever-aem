/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.RelatedArticles;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.RelatedItem;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class RelatedItemRule.
 */
public class RelatedItemRule extends ConfigurationSelectorRule {
 
 /** The Constant PROPERTY_SPONSORED. */
 public static final String PROPERTY_SPONSORED = "related.sponsored";
 
 /** The Constant PROPERTY_URL. */
 public static final String PROPERTY_URL = "related.url";
 
 /**
  * Instantiates a new related item rule.
  */
 private RelatedItemRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the related item rule
  */
 public static RelatedItemRule create() {
  return new RelatedItemRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the related item rule
  */
 public static RelatedItemRule createFrom(JSONObject configuration) {
  RelatedItemRule relatedItemRule = create();
  relatedItemRule.withSelector(configuration.getString("selector"));
  List<String> props = new LinkedList<String>();
  props.add(PROPERTY_SPONSORED);
  props.add(PROPERTY_URL);
  relatedItemRule.withProperties(props, configuration);
  return relatedItemRule;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer , com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  RelatedItem relatedItem = RelatedItem.create();
  RelatedArticles relatedArticles = (RelatedArticles) container;
  
  String url = (String) this.getProperty(PROPERTY_URL, node);
  if (StringUtils.isNotBlank(url)) {
   relatedItem.withURL(url);
  }
  if ((Boolean) this.getProperty(PROPERTY_SPONSORED, node)) {
   relatedItem.enableSponsored();
  }
  relatedArticles.addRelated(relatedItem);
  return relatedArticles;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { RelatedArticles.getClassName() };
 }
 
}
