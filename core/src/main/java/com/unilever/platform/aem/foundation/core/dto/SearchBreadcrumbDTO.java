/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

/**
 * The Class SearchBreadcrumbDTO.
 */
public class SearchBreadcrumbDTO {
 
 /** The page url. */
 private String pageUrl;
 
 /** The page name. */
 private String pageName;
 
 /**
  * Gets the page url.
  *
  * @return the page url
  */
 public String getPageUrl() {
  return pageUrl;
 }
 
 /**
  * Sets the page url.
  *
  * @param pageUrl
  *         the new page url
  */
 public void setPageUrl(String pageUrl) {
  this.pageUrl = pageUrl;
 }
 
 /**
  * Gets the page name.
  *
  * @return the page name
  */
 public String getPageName() {
  return pageName;
 }
 
 /**
  * Sets the page name.
  *
  * @param pageName
  *         the new page name
  */
 public void setPageName(String pageName) {
  this.pageName = pageName;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see java.lang.Object#toString()
  */
 @Override
 public String toString() {
  return "{\"pageUrl\":\"" + pageUrl + "\",\"pageName\":\"" + pageName + "\"}";
 }
 
}
