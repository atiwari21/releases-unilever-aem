/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.TextNode;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * The Class Footer.
 */
public class Footer extends InstantArticleElement implements Container {
 
 /** The copyright. */
 private Small copyright;
 
 private RelatedArticles relatedArticles;
 
 /**
  * Instantiates a new footer.
  */
 private Footer() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the footer
  */
 public static Footer create() {
  return new Footer();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement()
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element footer = document.createElement("footer");
  if (copyright != null) {
   footer.appendChild(this.copyright.toDOMElement(document));
   
  } else {
   footer.appendChild(new TextNode("", document.baseUri()));
  }
  if (this.relatedArticles != null && this.relatedArticles.isValid()) {
   footer.appendChild(this.relatedArticles.toDOMElement(document,false));
  }
  return footer;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  return new ArrayList<Object>();
 }
 
 public RelatedArticles getRelatedArticles() {
  return relatedArticles;
 }
 
 /**
  * Gets the copyright.
  *
  * @return the copyright
  */
 public Object getCopyright() {
  return copyright;
 }
 
 /**
  * With copyright.
  *
  * @param copyright
  *         the copyright
  * @return the footer
  */
 public Footer withCopyright(Object copyright) {
  if (copyright instanceof String) {
   this.copyright = (Small) (Small.create()).appendText(copyright);
  } else if (copyright instanceof Small) {
   this.copyright = (Small) copyright;
  }
  return this;
  
 }
 
 public Footer withRelatedArticles(RelatedArticles relatedArticles) {
  if (relatedArticles.isValid()) {
   this.relatedArticles = relatedArticles;
  }
  return this;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#isValid()
  */
 public boolean isValid() {
  return this.copyright != null && this.copyright.isValid() ? true : false;
 }
}
