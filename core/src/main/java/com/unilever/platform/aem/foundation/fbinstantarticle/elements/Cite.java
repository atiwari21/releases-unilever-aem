/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The Class Cite.
 */
public class Cite extends TextContainer {
 
 /**
  * The text alignment.
  *
  * @var string text align. Values: "op-left"|"op-center"|"op-right"
  */
 private String textAlignment;
 
 /**
  * The text alignment.
  *
  * @var string text align. Values: "op-left"|"op-center"|"op-right"
  */
 private String verticalAlignment;
 
 /**
  * The position.
  *
  * @var string text position. Values: "op-vertical-below"|"op-vertical-above"|"op-vertical-center"
  */
 private String position;
 
 private Cite() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static Cite create() {
  return new Cite();
 }
 
 /**
  * With text alignment.
  *
  * @param textAlignment
  *         the text alignment
  * @return the element
  */
 public InstantArticleElement withTextAlignment(String textAlignment) {
  this.textAlignment = textAlignment;
  return this;
  
 }
 
 /**
  * With verticle alignment.
  *
  * @param verticleAlignment
  *         the text alignment
  * @return the element
  */
 public InstantArticleElement withVerticalAlignment(String textAlignment) {
  this.textAlignment = textAlignment;
  return this;
  
 }
 
 /**
  * With position.
  *
  * @param position
  *         the position
  * @return the element
  */
 public InstantArticleElement withPosition(String position) {
  this.position = position;
  return this;
  
 }
 
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element cite = document.createElement("cite");
  List<String> classesList = new ArrayList<String>();
  if (StringUtils.isNotBlank(this.position)) {
   classesList.add(this.position);
  }
  if (StringUtils.isNotBlank(this.textAlignment)) {
   classesList.add(this.textAlignment);
  }
  if (StringUtils.isNotBlank(this.verticalAlignment)) {
   classesList.add(this.verticalAlignment);
  }
  if (CollectionUtils.isNotEmpty(classesList)) {
   Iterator<String> itr = classesList.iterator();
   String classs = "";
   while (itr.hasNext()) {
    classs = classs + itr.next() + " ";
   }
   cite.attr("class", StringUtils.join(classesList.toArray(), " "));
  }
  
  cite.text(textToDOMDocumentFragment(document));
  return cite;
 }
 
}