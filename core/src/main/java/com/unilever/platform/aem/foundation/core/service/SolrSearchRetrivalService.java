/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SolrSearchRetrivalService.
 */
@SlingServlet(label = "Unilver Search - Solr Result Retrival Service", methods = { org.apache.sling.api.servlets.HttpConstants.METHOD_GET }, paths = {
  SolrSearchRetrivalService.SERVLET_PATH })
public class SolrSearchRetrivalService extends SlingAllMethodsServlet {
 
 private static final long serialVersionUID = -6520445102937607753L;
 
 @Reference
 SearchRetrivalService searchRetrivalService;
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(SolrSearchRetrivalService.class);
 
 protected static final String SERVLET_PATH = "/bin/search/retrivalhandler";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  LOG.debug("inside search result retrival service");
  response.setContentType("application/json");
  response.setCharacterEncoding("utf-8");
  
  JSONObject obj = new JSONObject(searchRetrivalService.getSearchResult(request));
  response.getWriter().write(obj.toString());
 }
 
 /**
  * Gets the servlet path.
  *
  * @return the servlet path
  */
 public static String getServletPath() {
  
  return SERVLET_PATH;
 }
 
}
