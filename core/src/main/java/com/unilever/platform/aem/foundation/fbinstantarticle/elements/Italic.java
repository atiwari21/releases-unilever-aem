/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Class Italic.
 */
public class Italic extends FormattedText {
 
 private Italic() {
  
 }
 
 public static Italic create() {
  return new Italic();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement()
  */
 public Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!this.isValid()) {
   return null;
  }
  Element italic = document.createElement("i");
  italic.append(textToDOMDocumentFragment(document));
  return italic;
 }
}
