package com.unilever.platform.aem.foundation.utils;

import java.security.Key;

import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.servlets.Context;
import com.unilever.platform.aem.foundation.servlets.Site;

public final class TokenUtility {
 
 private static final Logger LOGGER = LoggerFactory.getLogger(TokenUtility.class);
 
 private static final int TWO = 2;
 
 private static final int TEN = 10;
 
 private static final String ERROR_WHILE_GENERATING_DCU_TOKEN = "error while generating DCU token: ";
 
 private static final String HMAC_SHA256 = "HmacSHA256";
 
 public static String gettoken(Page page, String category, ConfigurationService configurationService) {
  
  String securityKey = StringUtils.EMPTY;
  String apiKey = StringUtils.EMPTY;
  String brand = StringUtils.EMPTY;
  String entity = StringUtils.EMPTY;
  String locale = StringUtils.EMPTY;
  
  try {
   securityKey = configurationService.getConfigValue(page, category, "se_key");
   apiKey = configurationService.getConfigValue(page, category, "token");
   brand = configurationService.getConfigValue(page, category, "brand");
   entity = configurationService.getConfigValue(page, category, "entity");
   locale = configurationService.getConfigValue(page, category, "locale");
   
  } catch (ConfigurationNotFoundException e1) {
   LOGGER.error(ERROR_WHILE_GENERATING_DCU_TOKEN, e1);
  }
  
  return getJWTToken(securityKey, apiKey, brand, entity, locale);
 }
 
 public static String getJWTToken(String securityKey, String apiKey, String brand, String entity, String locale) {
  Key secretKey = new SecretKeySpec(securityKey.getBytes(), HMAC_SHA256);
  
  // Create the Claims, which will be the content of the JWT
  JwtClaims claims = new JwtClaims();
  
  // who creates the token and signs it
  claims.setIssuer(apiKey);
  // time when the token will expire (10 minutes from now)
  claims.setExpirationTimeMinutesInTheFuture(TEN);
  // a unique identifier for the token
  claims.setGeneratedJwtId();
  // when the token was issued/created (now)
  claims.setIssuedAtToNow();
  // time before which the token is not yet valid (2 minutes ago)
  claims.setNotBeforeMinutesInThePast(TWO);
  claims.setClaim("context", "{\"site\": {\"locale\": \"" + locale + "\",\"entity\": \"" + entity + "\",\"brand\": \"" + brand + "\"}}");
  Context context = new Context();
  Site site = new Site();
  site.setLocale(locale);
  site.setEntity(entity);
  site.setBrand(brand);
  context.setSite(site);
  
  // A JWT is a JWS and/or a JWE with JSON claims as the payload.
  // In this example it is a JWS so we create a JsonWebSignature object.
  JsonWebSignature jws = new JsonWebSignature();
  jws.setPayload(claims.toJson());
  jws.setKey(secretKey);
  // Set the signature algorithm on the JWT/JWS that will integrity protect the claims
  jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
  
  // Sign the JWS and produce the compact serialization or the complete JWT/JWS
  // representation, which is a string consisting of three dot ('.') separated
  // base64url-encoded parts in the form Header.Payload.Signature
  // If you wanted to encrypt it, you can simply set this jwt as the payload
  // of a JsonWebEncryption object and set the cty (Content Type) header to "jwt".
  String jwt = "";
  try {
   jwt = jws.getCompactSerialization();
  } catch (JoseException e) {
   LOGGER.error(ERROR_WHILE_GENERATING_DCU_TOKEN, e);
   
  }
  
  // Now you can do something with the JWT. Like send it to some other party
  // over the clouds and through the interwebs.
  LOGGER.debug("JWT: " + jwt);
  return jwt;
 }
}
