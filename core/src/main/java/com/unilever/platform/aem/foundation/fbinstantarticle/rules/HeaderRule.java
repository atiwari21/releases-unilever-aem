/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class HeaderTitleRule.
 */
public class HeaderRule extends ConfigurationSelectorRule {
 
 /**
  * Instantiates a new header title rule.
  */
 private HeaderRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the header title rule
  */
 public static HeaderRule create() {
  return new HeaderRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the header title rule
  */
 public static HeaderRule createFrom(JSONObject configuration) {
  return (HeaderRule) create().withSelector(configuration.getString("selector"));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer , com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Header header = Header.create();
  ((InstantArticle) container).addHeader(header);
  
  transformer.transform(header, node);
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName() };
 }
}