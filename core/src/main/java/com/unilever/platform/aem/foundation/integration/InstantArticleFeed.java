/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.jcr.Session;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.engine.SlingRequestProcessor;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SearchService;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Author;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Caption;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Footer;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Image;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;
import com.unilever.platform.aem.foundation.utils.ComponentUtil;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * The Class InstantAricleFeed.
 */
public class InstantArticleFeed extends AbstractFeed {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(InstantArticleFeed.class);
 
 /** The Constant DATE_FORMAT. */
 private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
 
 /** The sdf. */
 SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
 
 /** The current page. */
 Page currentPage;
 
 /** The resource resolver. */
 ResourceResolver resourceResolver;
 
 /** The page manager. */
 PageManager pageManager;
 
 /** The request. */
 SlingHttpServletRequest request;
 
 /** The response. */
 SlingHttpServletResponse response;
 
 /** The transformer. */
 Transformer transformer;
 
 /** The transformer. */
 Transformer footerTransformer;
 
 /** Boolean Include Teaser Image in Header. */
 boolean includeTeaserImageInHeader;
 
 /** The configuration service. */
 ConfigurationService configurationService;
 
 /** The request response factory. */
 RequestResponseFactory requestResponseFactory;
 
 /** The request processor. */
 SlingRequestProcessor requestProcessor;
 
 /** The search service. */
 SearchService searchService;
 
 /** The config map. */
 Map<String, String> configMap;
 
 /** The excluded component list. */
 List<String> excludedComponentList;
 
 /**
  * Instantiates a new instant article feed.
  *
  * @param req
  *         the req
  * @param resp
  *         the resp
  * @param requestResponseFactory
  *         the request response factory
  * @param requestProcessor
  *         the request processor
  */
 public InstantArticleFeed(SlingHttpServletRequest req, SlingHttpServletResponse resp, RequestResponseFactory requestResponseFactory,
   SlingRequestProcessor requestProcessor) {
  super(req);
  sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
  request = req;
  response = resp;
  this.resourceResolver = req.getResourceResolver();
  this.pageManager = resourceResolver.adaptTo(PageManager.class);
  this.requestResponseFactory = requestResponseFactory;
  
  this.requestProcessor = requestProcessor;
  
  this.currentPage = pageManager.getContainingPage(req.getResource());
  this.searchService = currentPage.adaptTo(SearchService.class);
  configurationService = currentPage.adaptTo(ConfigurationService.class);
  
  try {
   configMap = configurationService.getCategoryConfiguration(currentPage, "instantarticle");
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("IO Exception ", e.getMessage());
   LOGGER.debug("IO Exception ", e);
  }
  String configJSON = MapUtils.getString(configMap, "configurationJSON", "");
  String footerRule = MapUtils.getString(configMap, "footerRule", "");
  this.includeTeaserImageInHeader = MapUtils.getBoolean(configMap, "includeTeaserImageInHeader", false);
  JSONObject configuration = null;
  JSONObject footerConfiguration = null;
  try {
   configuration = new JSONObject(configJSON);
   footerConfiguration = new JSONObject(footerRule);
  } catch (JSONException jsonException) {
   LOGGER.error("json is not well formed ", jsonException.getMessage());
   LOGGER.debug("json is not well formed ", jsonException);
  }
  if (configuration != null) {
   this.transformer = new Transformer();
   this.transformer.loadRules(configuration);
  }
  
  if (footerConfiguration != null) {
   this.footerTransformer = new Transformer();
   this.footerTransformer.loadRules(footerConfiguration);
  }
  
  String excluded = MapUtils.getString(configMap, "excludedComponent", "");
  excludedComponentList = new ArrayList<String>();
  if (StringUtils.isNotBlank(excluded)) {
   excludedComponentList = Arrays.asList(excluded.split(","));
  }
 }
 
 /**
  * Prints the XMl header for RSS feed.
  *
  * @param properties
  *         the properties
  */
 @Override
 public void printHeader(ValueMap properties) {
  initXml();
  Document doc = getDoc();
  rootElement = doc.createElement("rss");
  rootElement.setAttribute("version", "2.0");
  rootElement.setAttribute("xmlns:content", "http://purl.org/rss/1.0/modules/content/");
  doc.appendChild(rootElement);
  Element channel = doc.createElement("channel");
  rootElement.appendChild(channel);
  String title = MapUtils.getString(configMap, "feedTitle", "");
  String desc = MapUtils.getString(configMap, "feedDesc", "");
  
  addTextNode(channel, "title", title);
  addTextNode(channel, "description", desc);
  addTextNode(channel, "link", getHtmlLink(currentPage));
  addTextNode(channel, "language", currentPage.getLanguage(false).toLanguageTag());
  addTextNode(channel, "lastBuildDate", sdf.format(new Date()));
 }
 
 /**
  * Prints the XMl Child Elements for RSS feed.
  *
  * @param page
  *         the page
  */
 @Override
 public void printEntry(Page page) {
  ValueMap properties = page.getProperties();
  Object dateObj = MapUtils.getObject(properties, UnileverConstants.PUBLISH_DATE) != null ? MapUtils.getObject(properties,
    UnileverConstants.PUBLISH_DATE) : MapUtils.getObject(properties, NameConstants.PN_PAGE_LAST_MOD);
  
  if (dateObj == null) {
   return;
  }
  Document doc = getDoc();
  Element item = doc.createElement("item");
  rootElement.appendChild(item);
  String url = getHtmlLink(page);
  
  Date pubDateTime = null;
  if (dateObj instanceof GregorianCalendar) {
   pubDateTime = dateObj != null ? ((GregorianCalendar) dateObj).getTime() : null;
  }
  String title = MapUtils.getString(properties, UnileverConstants.TEASER_TITLE, page.getTitle());
  addTextNode(item, "title", title);
  addTextNode(item, "link", url);
  addTextNode(item, "guid", url);
  if (pubDateTime != null) {
   addTextNode(item, "pubDate", sdf.format(pubDateTime));
  } else {
   addTextNode(item, "pubDate", "");
  }
  
  addTextNode(item, "author", MapUtils.getString(properties, "authorName", StringUtils.EMPTY));
  addTextNode(item, "description", MapUtils.getString(properties, UnileverConstants.TEASER_COPY, StringUtils.EMPTY));
  Element content = doc.createElement("content:encoded");
  item.appendChild(content);
  StringBuilder data = new StringBuilder();
  Map<String, String> attrMap = new HashMap<String, String>();
  try (BufferedReader br = (BufferedReader) ComponentUtil.getReaderFromURL(page.getPath() + ".html", resourceResolver, requestResponseFactory,
    requestProcessor, attrMap, "GET")) {
   String line;
   while ((line = br.readLine()) != null) {
    data.append(line);
   }
  } catch (IOException e) {
   LOGGER.error("IOException", e.getMessage());
   LOGGER.debug("IOException", e);
  }
  String dataStr = data.toString().replaceAll("\n", " ");
  String html = getInstantArticleHTML(dataStr, this.excludedComponentList, pubDateTime, url, properties);
  content.appendChild(doc.createCDATASection(html));
 }
 
 /**
  * Gets the html link.
  *
  * @param page
  *         the page
  * @return the html link
  */
 public String getHtmlLink(Page page) {
  return getUrlPrefix() + resourceResolver.map(page.getPath()) + SUFFIX_HTML;
 }
 
 /**
  * Gets the resultfrom query.
  *
  * @param session
  *         the session
  * @param map
  *         the map
  * @param builder
  *         the builder
  * @return the resultfrom query
  */
 private List<Page> getResultfromQuery(Session session, Map<String, String> map, QueryBuilder builder) {
  com.day.cq.search.Query query = null;
  SearchResult result = null;
  List<Page> pageList = new ArrayList<Page>();
  query = builder.createQuery(PredicateGroup.create(map), session);
  query.setHitsPerPage(0);
  if (query != null) {
   result = query.getResult();
   Iterator<Resource> rsIterator = result.getResources();
   while (rsIterator.hasNext()) {
    Page page = pageManager.getContainingPage(rsIterator.next());
    if (page != null) {
     pageList.add(page);
    }
   }
  }
  return pageList;
 }
 
 /**
  * Gets the pages.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @param propertyName
  *         the property name
  * @param valueList
  *         the value list
  * @param type
  *         the type
  * @return the pages
  */
 private List<Page> getPages(Session session, String path, String propertyName, List<String> valueList, String type) {
  Map<String, String> map = new HashMap<String, String>();
  List<Page> pageList = new ArrayList<Page>();
  map.put("path", path);
  map.put("type", type);
  if (CollectionUtils.isNotEmpty(valueList)) {
   map.put("property", propertyName);
   for (int i = 1; i < valueList.size() + 1; ++i) {
    map.put("property." + i + "_value", valueList.get(i - 1));
   }
   pageList = getResultfromQuery(session, map, resourceResolver.adaptTo(QueryBuilder.class));
  }
  
  return pageList;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.integration.AbstractFeed#printChildEntries()
  */
 public void printChildEntries() {
  if (currentPage != null) {
   List<Resource> children = new ArrayList<Resource>();
   String contentTypes = MapUtils.getString(configMap, "contentTypes", "");
   List<String> contentTypeList = Arrays.asList(contentTypes.split(","));
   
   List<Page> pageList = getPages(resourceResolver.adaptTo(Session.class), currentPage.getPath(), UnileverConstants.CONTENT_TYPE, contentTypeList,
     "cq:PageContent");
   int limit = MapUtils.getIntValue(configMap, "limit", 50);
   List<Page> pageSubList = pageList;
   if (limit > 0 && pageList.size() >= limit) {
    pageSubList = pageList.subList(0, limit - 1);
   }
   Iterator<Page> itr = pageSubList.iterator();
   while (itr.hasNext()) {
    Page page = itr.next();
    children.add(page.getContentResource());
   }
   printEntries(children.iterator());
  }
 }
 
 /**
  * Gets the instant article html.
  *
  * @param data
  *         the data
  * @param excludeList
  *         the exclude list
  * @param pubDate
  *         the pub date
  * @param url
  *         the url
  * @param properties
  *         the properties
  * @return the instant article html
  */
 public String getInstantArticleHTML(String data, List<String> excludeList, Date pubDate, String url, ValueMap properties) {
  org.jsoup.nodes.Document doc = Jsoup.parse(data).normalise();
  for (String exclude : excludeList) {
   doc.select("[data-role=" + exclude + "]").remove();
  }
  InstantArticle a = InstantArticle.create();
  a.withCanonicalURL(url);
  if (this.transformer != null) {
   transformer.transform(a, doc.body().parent());
  }
  
  Object dateObj = MapUtils.getObject(properties, NameConstants.PN_PAGE_LAST_MOD);
  Date modifiedDateTime = dateObj != null ? ((GregorianCalendar) dateObj).getTime() : null;
  
  Image headerImage = Image.create();
  if (this.includeTeaserImageInHeader) {
   String teaserImage = (String) MapUtils.getString(properties, "teaserImage", "");
   if (StringUtils.isNotBlank(teaserImage)) {
    String teaserAlt = (String) MapUtils.getString(properties, "teaserAltText", "");
    Caption teaserCaption = Caption.create().withTitle(teaserAlt);
    headerImage.withUrl(teaserImage);
    headerImage.withCaption(teaserCaption);
   }
  }
  
  String teaserCopy = (String) MapUtils.getString(properties, "teaserCopy", StringUtils.EMPTY);
  String authorName = MapUtils.getString(properties, "authorName", StringUtils.EMPTY);
  Author author = Author.create().withName(authorName);
  
  Header header = a.getHeader().withPublishTime(pubDate);
  header.withAuthor(author);
  header = header.withModifyTime(modifiedDateTime);
  header.withCover(headerImage);
  header.withSubTitle(teaserCopy);
  a.withHeader(header);
  a.withStyle(configMap.getOrDefault("style", ""));
  setFooter(data, a);
  Footer footer = a.getFooter();
  if (footer != null && footer.isValid() && a.getRelatedArticles() != null && a.getRelatedArticles().isValid()) {
   footer.withRelatedArticles(a.getRelatedArticles());
  }
  a.withFooter(footer);
  return a.render();
  
 }
 
 
 /**
  * Sets the footer.
  *
  * @param data
  *         the data
  * @param a
  *         the a
  */
 void setFooter(String data, InstantArticle a) {
  org.jsoup.nodes.Document doc = Jsoup.parse(data).normalise();
  String footerComponentName = configMap.getOrDefault("footerComponentName", "");
  if (StringUtils.isNotBlank(footerComponentName)) {
   String[] footers = footerComponentName.split(",");
   for (String footer : footers) {
    Elements footerElements = doc.select("[data-role=" + footer + "]");
    if (CollectionUtils.isNotEmpty(footerElements)) {
     org.jsoup.nodes.Element footerElement = footerElements.get(0);
     footerTransformer.transform(a, footerElement);
     break;
    }
   }
   
  }
  
 }
 
}
