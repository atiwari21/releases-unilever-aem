/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.helper;

import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.PageManager;

/**
 * The Class PageComparator.
 */
public class PageComparator implements Comparator<Object> {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PageComparator.class);
 
 /** The tags list. */
 private List<String> tagsList;
 
 /** The promotion tag ids. */
 private String[] promotionTagIds;
 
 /** The sort by date. */
 private boolean sortByDate;
 
 /** The page manager. */
 private PageManager pageManager;
 
 /**
  * Instantiates a new page comparator.
  *
  * @param tagsList
  *         the tags list
  * @param promotionTagIds
  *         the promotion tag ids
  * @param sortByDate
  *         the sort by date
  * @param pageManager
  *         the page manager
  */
 public PageComparator(List<String> tagsList, String[] promotionTagIds, boolean sortByDate, PageManager pageManager) {
  super();
  this.tagsList = tagsList;
  this.promotionTagIds = promotionTagIds;
  this.sortByDate = sortByDate;
  this.pageManager = pageManager;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
  */
 @Override
 public int compare(Object obj1, Object obj2) {
  LOGGER.debug("obj1 : {}", obj1);
  LOGGER.debug("obj2 : {}", obj2);
  PageSortingDTO dataDTO1 = new PageSortingDTO(this.pageManager.getPage(obj1.toString()), tagsList, promotionTagIds);
  PageSortingDTO dataDTO2 = new PageSortingDTO(this.pageManager.getPage(obj2.toString()), tagsList, promotionTagIds);
  
  return getResultForPromotion(dataDTO1, dataDTO2);
 }
 
 /**
  * Gets the result for promotion.
  *
  * @param dataDTO1
  *         the data dt o1
  * @param dataDTO2
  *         the data dt o2
  * @return the result for promotion
  */
 private int getResultForPromotion(PageSortingDTO dataDTO1, PageSortingDTO dataDTO2) {
  int promotionTagCount1 = dataDTO1.getPromotionTagCount();
  int promotionTagCount2 = dataDTO2.getPromotionTagCount();
  int result = promotionTagCount2 - promotionTagCount1;
  if (result == 0) {
   if (this.sortByDate) {
    result = dataDTO2.getDate().compareTo(dataDTO1.getDate());
    if (result == 0) {
     result = dataDTO2.getCount() - dataDTO1.getCount();
    }
   } else {
    result = dataDTO2.getCount() - dataDTO1.getCount();
    if (result == 0) {
     result = dataDTO2.getDate().compareTo(dataDTO1.getDate());
    }
   }
  }
  return result;
 }
}
