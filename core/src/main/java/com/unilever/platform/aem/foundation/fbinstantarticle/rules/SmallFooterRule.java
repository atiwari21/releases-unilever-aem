/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Footer;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Small;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class SmallFooterRule.
 */
public class SmallFooterRule extends ConfigurationSelectorRule {
 
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Small small = Small.create();
  Footer footer = Footer.create();
  if (container instanceof InstantArticle) {
   footer = ((InstantArticle) container).getFooter();
  } else if (container instanceof Footer) {
   footer = (Footer) container;
  }
  return footer.withCopyright(transformer.transform(small, node));
 }
 
 @Override
 public String[] getContextClass() {
  return new String[] { Footer.getClassName() };
 }
 
 public static SmallFooterRule create() {
  return new SmallFooterRule();
 }
 
 public static Rule createFrom(JSONObject configuration) {
  SmallFooterRule smallFooterRule = create();
  smallFooterRule.withSelector(configuration.getString("selector"));
  return smallFooterRule;
 }
 
}
