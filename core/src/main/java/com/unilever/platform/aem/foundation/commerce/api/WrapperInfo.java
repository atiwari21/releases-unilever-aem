/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api;

import com.day.cq.commons.ImageResource;

/**
 * The Class WrapperInfo.
 */
public class WrapperInfo {
 
 /** The text. */
 private String text;
 
 /** The id. */
 private String id;
 
 /** The wrapper image. */
 private ImageResource wrapperImageResource;
 
 /** The image alt text. */
 private String imageAltText;
 
 /** The wrapper image. */
 private String wrapperImage;
 
 /**
  * Gets the text.
  *
  * @return the text
  */
 public String getText() {
  return text;
 }
 
 /**
  * Gets the id.
  *
  * @return the id
  */
 public String getId() {
  return id;
 }
 
 /**
  * Sets the text.
  *
  * @param text
  *         the new text
  */
 public void setText(String text) {
  this.text = text;
 }
 
 /**
  * Sets the id.
  *
  * @param id
  *         the new id
  */
 public void setId(String id) {
  this.id = id;
 }
 
 /**
  * Gets the image alt text.
  *
  * @return the image alt text
  */
 public String getImageAltText() {
  return imageAltText;
 }
 
 /**
  * Sets the image alt text.
  *
  * @param imageAltText
  *         the new image alt text
  */
 public void setImageAltText(String imageAltText) {
  this.imageAltText = imageAltText;
 }
 
 /**
  * Gets the wrapper image resource.
  *
  * @return the wrapper image resource
  */
 public ImageResource getWrapperImageResource() {
  return wrapperImageResource;
 }
 
 /**
  * Sets the wrapper image resource.
  *
  * @param wrapperImageResource
  *         the new wrapper image resource
  */
 public void setWrapperImageResource(ImageResource wrapperImageResource) {
  this.wrapperImageResource = wrapperImageResource;
 }
 
 /**
  * Gets the wrapper image.
  *
  * @return the wrapper image
  */
 public String getWrapperImage() {
  return wrapperImage;
 }
 
 /**
  * Sets the wrapper image.
  *
  * @param wrapperImage
  *         the new wrapper image
  */
 public void setWrapperImage(String wrapperImage) {
  this.wrapperImage = wrapperImage;
 }
}
