/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.solr.common.SolrInputDocument;
import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.commons.ImageResource;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.constants.SearchConstants;

/**
 * A factory for creating IndexingContent objects.
 */
public class IndexingContentFactory {
 
 /** The Constant FOUR. */
 private static final int FOUR = 4;
 
 /** The Constant THREE. */
 private static final int THREE = 3;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant SLASH_HOME. */
 private static final String SLASH_HOME = "/home/";
 
 /** The Constant HOME_HTML. */
 private static final String HOME_HTML = "/home.html";
 
 /** The Constant TEASER_COPY. */
 private static final String TEASER_COPY = "teaserCopy";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(IndexingContentFactory.class);
 
 /** The Constant homePageName. */
 private static final String HOME_PAGE_NAME = "homePageName";
 
 private static final String VIDEO_ID = "videoId";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * Instantiates a new indexing content factory.
  */
 private IndexingContentFactory() {
  
 }
 
 /**
  * Adapt from resource.
  *
  * @param resource
  *         the resource
  * @param hostUrl
  *         the host url
  * @param breadcrumbLevel
  *         the breadcrumb level
  * @param indexDoc
  *         the index doc
  * @param searchurlMappings
  *         the searchurl mappings
  * @return the indexing content factory
  * @throws LoginException
  *          the login exception
  */
 
 public static IndexDTO adaptFromResource(Resource resource, String[] hostUrl, int breadcrumbLevel, IndexDTO indexDoc,
   Map<String, String> searchurlMappings) throws LoginException {
  LOGGER.debug("inside adaptFromResource to fetch data from page jcr property");
  final String id = resource.getPath();
  final String pageurl = resource.getPath();
  
  int brandindex = org.apache.commons.lang3.StringUtils.ordinalIndexOf(pageurl, "/", TWO);
  int beginindex = org.apache.commons.lang3.StringUtils.ordinalIndexOf(pageurl, "/", THREE);
  int endIndex = org.apache.commons.lang3.StringUtils.ordinalIndexOf(pageurl, "/", FOUR);
  
  String locale = "";
  if (endIndex > 0) {
   locale = pageurl.substring(beginindex + 1, endIndex);
  } else {
   locale = pageurl.substring(beginindex + 1, pageurl.length());
  }
  
  final String brand = pageurl.substring(brandindex + 1, beginindex);
  final String brandLocale = brand + "_" + locale;
  final Resource jcrResource = resource.getChild(JcrConstants.JCR_CONTENT);
  final ValueMap valueMap = jcrResource.adaptTo(ValueMap.class);
  String title = valueMap.get(JcrConstants.JCR_TITLE, "");
  String teaserCopy = valueMap.get(TEASER_COPY, "");
  String contentType = valueMap.get(SearchConstants.CONTENTTYPE, "");
  PageManager pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
  String templateName = valueMap.get(org.apache.sling.jcr.resource.JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, "");
  LOGGER.debug("Content type is {}", contentType);
  if (contentType.equals(ContentType.PRODUCT.getValue())) {
   setProductValues(indexDoc, pageurl, pageManager);
  } else if (contentType.equals(ContentType.ARTICLE.getValue())) {
   setArticleValues(indexDoc, valueMap);
  } else if (SearchConstants.RANGELANDINGTEMPLATE.equalsIgnoreCase(templateName)) {
   LOGGER.debug("Inside range landing data set");
   indexDoc.setDescription(valueMap.get(SearchConstants.RANGESHORTCOPY, ""));
   if (null != valueMap.get(SearchConstants.RANGEIMAGE)) {
    indexDoc.setImageUrl(valueMap.get(SearchConstants.RANGEIMAGE, ""));
    indexDoc.setImageAltText(valueMap.get(SearchConstants.RANGENAME, ""));
    setImageValues(indexDoc, indexDoc.getImageAltText());
   }
  } else {
   setOtherContentType(indexDoc, jcrResource, valueMap);
  }
  indexDoc.setPageUrl(getFullURL(pageurl, searchurlMappings, resource.getResourceResolver()));
  List<SearchBreadcrumbDTO> navigationList = getBreadCrumbHierarchy(pageurl, pageManager, breadcrumbLevel, searchurlMappings,
    resource.getResourceResolver());
  String bodyContent = getDatafromHttpClient(hostUrl[1] + pageurl + SearchConstants.CONTENTHTML, hostUrl);
  TagManager tagManager = resource.getResourceResolver().adaptTo(TagManager.class);
  Tag[] tags = tagManager.getTags(jcrResource);
  LOGGER.debug("Locale is" + locale);
  indexDoc.setTitle(title);
  
  indexDoc.setBodyContent(bodyContent);
  indexDoc.setBrandname(brand);
  indexDoc.setBrandlocale(brandLocale);
  indexDoc.setContentType(valueMap.get(SearchConstants.CONTENTTYPE, ""));
  indexDoc.setSubContentType(valueMap.get(SearchConstants.SUBCONTENTTYPE, StringUtils.EMPTY));
  indexDoc.setLocale(locale);
  indexDoc.setId(id);
  indexDoc.setTags(tags);
  indexDoc.setBreadcrumb(navigationList);
  indexDoc.setTeaserCopy(teaserCopy);
  LOGGER.debug("Exiting Indexcontentfactory class adaptFromResource() method");
  
  return indexDoc;
 }
 
 /**
  * Sets the other content type.
  *
  * @param indexDoc
  *         the index doc
  * @param jcrResource
  *         the jcr resource
  * @param valueMap
  *         the value map
  */
 private static void setOtherContentType(IndexDTO indexDoc, final Resource jcrResource, final ValueMap valueMap) {
  LOGGER.debug("Inside non-product/article data set");
  indexDoc.setDescription(valueMap.get(JcrConstants.JCR_DESCRIPTION, ""));
  final Resource heroResource = jcrResource.getChild(SearchConstants.HERO);
  if (heroResource != null) {
   final ValueMap heroValueMap = heroResource.adaptTo(ValueMap.class);
   String heroImage = heroValueMap.get(SearchConstants.IMAGEURL, "");
   if (heroImage != null) {
    LOGGER.debug("inside hero image property get section");
    indexDoc.setImageUrl(heroImage);
    indexDoc.setImageAltText(heroValueMap.get(SearchConstants.ALTIMAGE, ""));
    setImageValues(indexDoc, indexDoc.getImageAltText());
   }
  } else {
   final Resource imageResource = jcrResource.getChild(SearchConstants.IMAGE);
   if (null != imageResource && null != imageResource.getValueMap()) {
    LOGGER.debug("inside image node property get section");
    indexDoc.setImageUrl(imageResource.getValueMap().get(com.day.cq.commons.DownloadResource.PN_REFERENCE, ""));
    indexDoc.setImageAltText("");
    setImageValues(indexDoc, indexDoc.getImageAltText());
   }
  }
 }
 
 /**
  * Sets the article values.
  *
  * @param indexDoc
  *         the index doc
  * @param valueMap
  *         the value map
  */
 private static void setArticleValues(IndexDTO indexDoc, final ValueMap valueMap) {
  LOGGER.debug("Inside Article data set method");
  if (valueMap.get(CommonConstants.ARTICLE_TEASER_IMAGE) != null) {
   indexDoc.setDescription(valueMap.get(CommonConstants.ARTICLE_TEASER_COPY, ""));
   indexDoc.setImageUrl(valueMap.get(CommonConstants.ARTICLE_TEASER_IMAGE, ""));
   indexDoc.setImageAltText(valueMap.get(CommonConstants.ARTICLE_TEASER_TITLE, ""));
   setImageValues(indexDoc, indexDoc.getImageAltText());
   
  }
  if (valueMap.get(SearchConstants.PUBLISHDATE) != null) {
   indexDoc.setPublishDate(((GregorianCalendar) valueMap.get(SearchConstants.PUBLISHDATE)).getTime());
  }
 }
 
 /**
  * Sets the product values.
  *
  * @param indexDoc
  *         the index doc
  * @param pageurl
  *         the pageurl
  * @param pageManager
  *         the page manager
  */
 private static void setProductValues(IndexDTO indexDoc, final String pageurl, PageManager pageManager) {
  LOGGER.debug("Inside Product Value set method");
  Page productpage = pageManager.getContainingPage(pageurl);
  Product product = CommerceHelper.findCurrentProduct(productpage);
  UNIProduct currentProduct = null;
  if (product instanceof UNIProduct) {
   currentProduct = (UNIProduct) product;
  }
  if (currentProduct != null) {
   if (null != currentProduct.getImages() && !currentProduct.getImages().isEmpty()) {
    indexDoc.setImageUrl(currentProduct.getImages().get(0).getFileReference());
    indexDoc.setImageAltText(currentProduct.getImages().get(0).getAlt());
    setImageValues(indexDoc, indexDoc.getImageAltText());
   }
   String videoId = StringUtils.EMPTY;
   boolean hasVideos=false;
   if(product!=null){
    List<ImageResource> images = product.getImages();
    for(ImageResource imageResource: images){
     if (null != imageResource.adaptTo(ValueMap.class).get(VIDEO_ID)) {
      videoId = imageResource.adaptTo(ValueMap.class).get(VIDEO_ID).toString();
      if (StringUtils.isNotBlank(videoId)) {
       hasVideos = true;
       break;
      }
     }
    }
   }
   indexDoc.setRelatedVideo(hasVideos);
   indexDoc.setProductid(currentProduct.getSKU());
   indexDoc.setDescription(currentProduct.getShortPageDescription());
   indexDoc.setPublishDate(currentProduct.getProductionDate());
   indexDoc.setIndentifierType(currentProduct.getIdentifierType());
   indexDoc.setIdentifierValue(currentProduct.getIdentifierValue());
   indexDoc.setShortIdentifierValue(currentProduct.getShortIdentifierValue());
   LOGGER.debug("inside product page indexing with product SKU" + currentProduct.getSKU());
  }
 }
 
 /**
  * Sets the image values.
  *
  * @param indexDoc
  *         the index doc
  * @param title
  *         the title
  */
 private static void setImageValues(IndexDTO indexDoc, String title) {
  indexDoc.setImageName(StringUtils.isBlank(indexDoc.getImageUrl()) ? StringUtils.EMPTY : fetchImageFileName(indexDoc.getImageUrl()));
  indexDoc.setIsNotAdaptiveImage("false");
  indexDoc.setImageExtension(StringUtils.isBlank(indexDoc.getImageUrl()) ? StringUtils.EMPTY
    : getExtensionFromMimeType(indexDoc.getImageUrl().substring(indexDoc.getImageUrl().lastIndexOf('.') + 1, indexDoc.getImageUrl().length())));
  indexDoc.setImageTitle(StringUtils.isBlank(title) ? indexDoc.getImageName() : title);
  indexDoc.setImagePath(indexDoc.getImageUrl());
 }
 
 /**
  * This method is used to fetch the image name.
  *
  * @param fileReference
  *         the image path
  * @return the file name
  */
 public static String fetchImageFileName(String fileReference) {
  String fileName = StringUtils.EMPTY;
  if (StringUtils.isNotEmpty(fileReference) && StringUtils.contains(fileReference, CommonConstants.FORWARD_SLASH)
    && StringUtils.contains(fileReference, CommonConstants.FULL_STOP)) {
   fileName = fileReference.substring(fileReference.lastIndexOf(CommonConstants.FORWARD_SLASH) + 1,
     fileReference.lastIndexOf(CommonConstants.FULL_STOP));
  }
  return getEncodedPath(fileName);
 }
 
 /**
  * Gets the encoded path.
  *
  * @param path
  *         the path
  * @return the encoded path
  */
 public static String getEncodedPath(String path) {
  String fileReference = path;
  try {
   if (StringUtils.isNotBlank(path)) {
    fileReference = URLEncoder.encode(path, "UTF-8");
   }
  } catch (UnsupportedEncodingException uee) {
   LOGGER.error("UnsupportedEncodingException while encoding the file reference: ", uee);
  }
  
  return fileReference;
 }
 
 /**
  * This method is used to return the valid extension corresponding to the mime type.
  *
  * @param mimeType
  *         This is the mimeType String.
  * @return The valid extension.
  */
 public static String getExtensionFromMimeType(String mimeType) {
  return ImageDTO.getExtensionFromMimeType(mimeType);
 }
 
 /**
  * Returns the full url of page. Domain path will be fetched from externalizer service and relative path will be added to it.
  *
  * @param pagePathParam
  *         the page path param
  * @param searchurlMappings
  *         the searchurl mappings
  * @param resourceResolver
  * @return the full url
  */
 public static String getFullURL(String pagePathParam, Map<String, String> searchurlMappings, ResourceResolver resourceResolver) {
  LOGGER.debug("Inside getFullURL of ComponentUtil");
  String fullPath = StringUtils.EMPTY;
  String mappedPath = StringUtils.EMPTY;
  Resource res = resourceResolver.getResource(pagePathParam);
  if (res != null) {
   String resolutionPathInfo = res.getResourceMetadata().getResolutionPathInfo();
   LOGGER.debug("map: Path maps to resource {} with path info {}", res, resolutionPathInfo);
   List<String> names = new LinkedList<String>();
   while (res != null) {
    String alias = getProperty(res, "sling:alias");
    if (alias == null) {
     alias = ResourceUtil.getName(res.getPath());
    }
    if ((alias != null) && (alias.length() > 0) && (!(alias.endsWith(":")))) {
     names.add(alias);
    }
    res = resourceResolver.getResource(ResourceUtil.getParent(res.getPath()));
   }
   StringBuilder buf = new StringBuilder();
   if (names.isEmpty()) {
    buf.append('/');
   } else {
    while (!(names.isEmpty())) {
     buf.append('/');
     buf.append((String) names.remove(names.size() - 1));
    }
   }
   if (resolutionPathInfo != null) {
    buf.append(resolutionPathInfo);
   }
   mappedPath = buf.toString();
   LOGGER.debug("map: Alias mapping resolves to path {}", mappedPath);
  }
  
  String pagePath = StringUtils.isBlank(mappedPath) ? pagePathParam : mappedPath;
  if (StringUtils.startsWith(pagePath, CommonConstants.FORWARD_SLASH)) {
   
   String path = pagePath;
   if (StringUtils.contains(pagePath, CommonConstants.FULL_STOP)) {
    path = path.substring(0, path.lastIndexOf(CommonConstants.FULL_STOP));
   }
   pagePath = getPagePath(pagePath, searchurlMappings);
   LOGGER.debug("Relative Path = " + pagePath);
   
  }
  if (!pagePath.contains(CommonConstants.FULL_STOP)) {
   pagePath = pagePath + CommonConstants.PAGE_EXTENSION;
  }
  return StringUtils.isBlank(fullPath) ? pagePath : fullPath;
 }
 
 /**
  *
  * @param res
  * @param propName
  * @return
  */
 public static String getProperty(Resource res, String propName) {
  return (String) getProperty(res, propName, String.class);
 }
 
 /**
  *
  * @param res
  * @param propName
  * @param type
  * @return
  */
 @SuppressWarnings("unchecked")
 public static <Type> Type getProperty(Resource res, String propName, Class<Type> type) {
  ValueMap props = (ValueMap) res.adaptTo(ValueMap.class);
  if (props != null) {
   Object prop = props.get(propName, type);
   if (prop != null) {
    LOGGER.debug("getProperty: Resource {} has property {}={}", new Object[] { res, propName, prop });
    return (Type) prop;
   }
   prop = props.get("jcr:content/" + propName, type);
   return (Type) prop;
  }
  
  return null;
 }
 
 /**
  * Gets the page path.
  *
  * @param pagePathParam
  *         the page path param
  * @param searchurlMappings
  *         the searchurl mappings
  * @return the page path
  */
 private static String getPagePath(String pagePathParam, Map<String, String> searchurlMappings) {
  String pagePath = pagePathParam;
  String homePages = StringUtils.EMPTY;
  if (searchurlMappings != null) {
   if (searchurlMappings.containsKey(HOME_PAGE_NAME)) {
    homePages = MapUtils.getString(searchurlMappings, HOME_PAGE_NAME);
    searchurlMappings.remove(HOME_PAGE_NAME);
   }
   for (Map.Entry<String, String> entry : searchurlMappings.entrySet()) {
    if (pagePath.contains(SLASH_HOME)) {
     pagePath = pagePath.replace(entry.getKey(), entry.getValue());
    } else if (pagePath.contains(HOME_HTML)) {
     pagePath = pagePath.replace(entry.getKey(), entry.getValue());
    } else {
     for (String homePage : homePages.split(",")) {
      String pageFormat = "/" + homePage + "/";
      String pageHtmlFormat = "/" + homePage + ".html";
      String key = entry.getKey();
      if (pagePath.contains(pageFormat) && StringUtils.contains(key, pageFormat)) {
       pagePath = pagePath.replace(key, entry.getValue());
      } else if (pagePath.contains(pageHtmlFormat) && StringUtils.contains(key, pageFormat)) {
       pagePath = pagePath.replace(key, entry.getValue());
      }
      
     }
    }
   }
  }
  
  if (!pagePath.contains(CommonConstants.PAGE_EXTENSION)) {
   pagePath = pagePath + CommonConstants.PAGE_EXTENSION;
  }
  return pagePath;
 }
 
 /**
  * Gets the bread crumb hierarchy.
  *
  * @param pageurl
  *         the pageurl
  * @param pageManager
  *         the page manager
  * @param breadcrumbLevel
  *         the breadcrumb level
  * @param searchurlMappings
  *         the searchurl mappings
  * @return the bread crumb hierarchy
  */
 private static List<SearchBreadcrumbDTO> getBreadCrumbHierarchy(final String pageurl, PageManager pageManager, int breadcrumbLevel,
   Map<String, String> searchurlMappings, ResourceResolver resourceResolver) {
  LOGGER.debug("entering Indexcontentfactory class getBreadCrumbHierarchy() method");
  List<SearchBreadcrumbDTO> navigationList = new ArrayList<SearchBreadcrumbDTO>();
  Page currentPagePath = pageManager.getContainingPage(pageurl);
  if (null != currentPagePath) {
   int currentLevelX = currentPagePath.getDepth();
   int parentlevel = breadcrumbLevel;
   int currentLevel = currentLevelX - 1;
   SearchBreadcrumbDTO primaryNavigationMap;
   while (parentlevel <= currentLevel - 1) {
    Page pageobj;
    String pageURL;
    String pageName;
    
    pageobj = currentPagePath.getAbsoluteParent(parentlevel);
    if (pageobj != null && !pageobj.isHideInNav()) {
     primaryNavigationMap = new SearchBreadcrumbDTO();
     pageURL = getFullURL(pageobj.getPath(), searchurlMappings, resourceResolver);
     pageName = pageNameSelection(pageobj);
     primaryNavigationMap.setPageName(pageName);
     addPageUrl(parentlevel, currentLevel, pageURL, primaryNavigationMap);
     LOGGER.debug("Map for breadcrumbs list " + primaryNavigationMap);
     navigationList.add(primaryNavigationMap);
    }
    parentlevel++;
   }
   LOGGER.debug("NavigationList is" + navigationList);
   LOGGER.debug("exiting Indexcontentfactory class getBreadCrumbHierarchy() method");
  }
  return navigationList;
 }
 
 /**
  * Page name selection.
  *
  * @param pageobj
  *         the pageobj
  * @return the string
  */
 private static String pageNameSelection(final Page pageobj) {
  LOGGER.debug("entering Indexcontentfactory class pageNameSelection() method");
  String pageName;
  if (pageobj.getNavigationTitle() != null) {
   pageName = pageobj.getNavigationTitle();
  } else {
   pageName = pageobj.getTitle();
  }
  LOGGER.debug("exiting Indexcontentfactory class pageNameSelection() method");
  return pageName;
 }
 
 /**
  * Adds the page url.
  *
  * @param parentlevel
  *         the parentlevel
  * @param currentLevel
  *         the current level
  * @param pageURL
  *         the page url
  * @param primaryNavigationMap
  *         the primary navigation map
  */
 private static void addPageUrl(final int parentlevel, final int currentLevel, final String pageURL, final SearchBreadcrumbDTO primaryNavigationMap) {
  if (parentlevel < currentLevel) {
   primaryNavigationMap.setPageUrl(pageURL);
  } else {
   primaryNavigationMap.setPageUrl("");
  }
 }
 
 /**
  * Gets the solr doc.
  *
  * @param res
  *         the res
  * @param hostUrl
  *         the host url
  * @param breadcrumbLevel
  *         the breadcrumb level
  * @param searchurlMappings
  *         the searchurl mappings
  * @return the solr doc
  */
 public static SolrInputDocument getSolrDoc(Resource res, String[] hostUrl, int breadcrumbLevel, Map<String, String> searchurlMappings) {
  IndexDTO indexDoc = new IndexDTO();
  SolrInputDocument doc = new SolrInputDocument();
  try {
   adaptFromResource(res, hostUrl, breadcrumbLevel, indexDoc, searchurlMappings);
   LOGGER.info("page to be index is--------------" + indexDoc.getPageUrl());
   doc.addField(IndexingSchema.ID, indexDoc.getId());
   doc.addField(IndexingSchema.TITLE, indexDoc.getTitle());
   doc.addField(IndexingSchema.DESCRIPTION, indexDoc.getDescription());
   LOGGER.debug("Page desciption content is--- " + indexDoc.getDescription());
   doc.addField(IndexingSchema.BODYCONTENT, indexDoc.getBodyContent());
   doc.addField(IndexingSchema.PAGEURL, indexDoc.getPageUrl());
   doc.addField(IndexingSchema.CONTENTTYPE, indexDoc.getContentType());
   doc.addField(IndexingSchema.SUBCONTENTTYPE, indexDoc.getSubContentType());
   doc.addField(IndexingSchema.LOCALE, indexDoc.getLocale());
   doc.addField(IndexingSchema.BRANDNAME, indexDoc.getBrandname());
   doc.addField(IndexingSchema.BRANDLOCALE, indexDoc.getBrandlocale());
   doc.addField(IndexingSchema.IMAGEURL, indexDoc.getImageUrl());
   doc.addField(IndexingSchema.IMAGEALTTEXT, indexDoc.getImageAltText());
   doc.addField(IndexingSchema.IMAGEEXTENSION, indexDoc.getImageExtension());
   doc.addField(IndexingSchema.IMAGENAME, indexDoc.getImageName());
   doc.addField(IndexingSchema.IMAGEPATH, indexDoc.getImagePath());
   doc.addField(IndexingSchema.IMAGETITLE, indexDoc.getImageTitle());
   doc.addField(IndexingSchema.ISNOTADAPTIVEIMAGE, indexDoc.getIsNotAdaptiveImage());
   doc.addField(IndexingSchema.TEASER_COPY, indexDoc.getTeaserCopy());
   ResourceResolver resolver = res.getResourceResolver();
   String[] tagsArray = ArrayUtils.EMPTY_STRING_ARRAY;
   if (resolver != null) {
    PageManager manager = resolver.adaptTo(PageManager.class);
    Page page = manager.getContainingPage(res);
    String tags = resolver.adaptTo(ConfigurationService.class).getConfigValue(page, "indexingConfig", "tags");
    tagsArray = tags.split(",");
   }
   TagManager tagManager = resolver.adaptTo(TagManager.class);
   for (String perTag : tagsArray) {
    Tag perTagRes = tagManager.resolve(perTag);
    if (perTagRes != null) {
     Iterator<Tag> perTagChild = perTagRes.listAllSubTags();
     while (perTagChild.hasNext()) {
      tagsArray = ArrayUtils.add(tagsArray, perTagChild.next().getLocalTagID());
     }
    }
   }
   
   for (Tag tag : indexDoc.getTags()) {
    String tagLocalID = tag.getLocalTagID();
    if (ArrayUtils.isNotEmpty(tagsArray) && ArrayUtils.contains(tagsArray, tagLocalID)) {
     doc.addField(IndexingSchema.TAGTITLE, tag.getTitle(new Locale(indexDoc.getLocale())));
    }
    doc.addField(IndexingSchema.TAGS, tag.getLocalTagID());
   }
   doc.addField(IndexingSchema.BREADCRUMB, indexDoc.getBreadcrumb());
   if (null != indexDoc.getPublishDate()) {
    doc.addField(IndexingSchema.PUBLISH_DATE, indexDoc.getPublishDate());
   }
   if (ContentType.PRODUCT.getValue().equalsIgnoreCase(indexDoc.getContentType())) {
    doc.addField(IndexingSchema.PRODUCTID, indexDoc.getProductid());
    doc.addField(IndexingSchema.IDENTIFIERTYPE, indexDoc.getIndentifierType());
    doc.addField(IndexingSchema.IDENTIFIERVALUE, indexDoc.getIdentifierValue());
    doc.addField(IndexingSchema.SHORTIDENTIFIERVALUE, indexDoc.getShortIdentifierValue());
    doc.addField(IndexingSchema.RELATED_VIDEOS, indexDoc.isRelatedVideo());
   }
   LOGGER.debug("Indexed doc json is -- " + doc);
  } catch (LoginException e) {
   LOGGER.error("Exception in adding values in solr doc.............." + ExceptionUtils.getStackTrace(e));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("cannot find configuration indexingConfig", e);
  }
  return doc;
 }
 
 /**
  * Gets the datafrom http client.
  *
  * @param url
  *         the url
  * @param hostDetails
  *         the host details
  * @return the datafrom http client
  */
 private static String getDatafromHttpClient(String url, String[] hostDetails) {
  String textResult = "";
  try {
   
   CredentialsProvider credsProvider = new BasicCredentialsProvider();
   credsProvider.setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
     new UsernamePasswordCredentials(hostDetails[TWO], hostDetails[THREE]));
   
   CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
   HttpGet request = new HttpGet(url);
   
   request.addHeader("content-type", "application/json");
   
   CloseableHttpResponse response = httpclient.execute(request);
   
   LOGGER.info("Indexed doc status is" + response.getStatusLine().getStatusCode() + url);
   
   StringBuilder result = new StringBuilder();
   if (response.getEntity() != null) { // success
    HttpGet getReq = new HttpGet(url + SearchConstants.WCMMODE);
    getReq.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;");
    response = httpclient.execute(getReq);
    if (response.getEntity() != null) {
     BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
     
     String line = "";
     while ((line = rd.readLine()) != null) {
      result.append(line);
     }
     rd.close();
    }
   } else {
    LOGGER.debug("GET request not worked as response entity was null in Indexdto---" + response.getEntity());
   }
   String updatedResult;
   updatedResult = result.toString().replaceAll("(?s)<!--excludesearch-->.*?<!--endexcludesearch-->", "");
   updatedResult = updatedResult.replaceAll("<a.*?</a>", "");
   updatedResult = updatedResult.replaceAll("</h1>", " </h1>");
   HtmlToPlainText formatter = new HtmlToPlainText();
   Document doc = Jsoup.parse(updatedResult.toString());
   textResult = formatter.getPlainText(doc).trim().replaceAll("\n\n+", " ").replaceAll("\\s+", " ").replaceAll("\\xA0", " ").replaceAll("\\*+", " ")
     .trim();
   
   return textResult;
  } catch (MalformedURLException e) {
   LOGGER.error("Exception in getting result from httpclient MalformedURLException.............." + ExceptionUtils.getStackTrace(e));
   
  } catch (IOException e) {
   LOGGER.error("Exception in getting result from httpclient.............." + ExceptionUtils.getStackTrace(e));
   
  }
  return textResult;
  
 }
 
}
