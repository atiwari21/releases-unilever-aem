/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;

/**
 * The Interface SearchRetrivalService.
 */
public interface SearchRetrivalService {
 
 /**
  * Gets the search result.
  *
  * @param request
  *         the request
  * @return the search result
  */
 Map<String, Object> getSearchResult(SlingHttpServletRequest request);
 
 /**
  * Gets the search result.
  *
  * @param request
  *         the request
  * @param isJson
  *         the isJson
  * @return the search result
  */
 Map<String, Object> getSearchResult(SlingHttpServletRequest request, Boolean isJson);
 
 /**
  *
  * @param path
  * @param subBrandName
  * @return
  */
 public String[] getTagsRelation(String path, String subBrandName);
}
