/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import java.util.Date;

/**
 * The Class ArticleSchema.
 */
public class ArticleSchema extends IndexDTO {
 
 /** The publish date. */
 private Date publishDate;
 
 /**
  * Instantiates a new article schema.
  */
 public ArticleSchema() {
  super();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.dto.IndexDTO#getPublishDate()
  */
 public Date getPublishDate() {
  return publishDate;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.dto.IndexDTO#setPublishDate(java.util.Date)
  */
 public void setPublishDate(Date date) {
  this.publishDate = date;
 }
 
}
