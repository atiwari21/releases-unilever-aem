/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.InstantArticleInterface;

/**
 * The Class InstantArticle.
 */
public class InstantArticle extends InstantArticleElement
  implements Container, InstantArticleInterface {
 
 /** The children. */
 private List<InstantArticleElement> children = new ArrayList<InstantArticleElement>();
 
 /** The Constant CURRENT_VERSION. */
 public static final String CURRENT_VERSION = "1.5.4";
 
 /** The meta properties. */
 private Map<String, Object> metaProperties = new HashMap<String, Object>();
 
 /** The canonical url. */
 private String canonicalURL;
 
 /** The markup version. */
 private String markupVersion = "v1.0";
 
 /** The is automatic ad placed. */
 private boolean isAutomaticAdPlaced = true;
 
 /** The charset. */
 private String charset = "utf-8";
 
 /** The style. */
 private String style;
 
 /** The header. */
 private Header header;
 
 /** The footer. */
 private Footer footer;
 
 /** The related articles. */
 private RelatedArticles relatedArticles;
 
 /** The is rtl enabled. */
 private boolean isRTLEnabled = false;
 
 /**
  * Instantiates a new instant article.
  */
 private InstantArticle() {
  
  this.header = Header.create();
  this.addMetaProperty("op:generator", "facebook-instant-articles-sdk");
  this.addMetaProperty("op:generator:version", CURRENT_VERSION);
 }
 
 /**
  * Creates the.
  *
  * @return the instant article
  */
 public static InstantArticle create() {
  return new InstantArticle();
 }
 
 /**
  * Adds new child elements to this InstantArticle.
  *
  * @param header the header
  * @return $this
  */
 public InstantArticle addHeader(Header header) {
  this.header = header;
  return this;
 }
 
 /**
  * Adds the footer.
  *
  * @param footer the footer
  * @return the instant article
  */
 public InstantArticle addFooter(Footer footer) {
  this.footer = footer;
  return this;
 }
 
 /**
  * Adds the meta property.
  *
  * @param propertName
  *         the propert name
  * @param propertyContent
  *         the property content
  * @return the instant article
  */
 public InstantArticle addMetaProperty(String propertName, String propertyContent) {
  this.metaProperties.put(propertName, propertyContent);
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#render()
  */
 @Override
 public String render() {
  return render("<!doctype html>", false);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#render(java.lang.String, boolean)
  */
 public String render(String docType, boolean format) {
  return super.render(docType, format);
 }
 
 /**
  * With canonical url.
  *
  * @param url the url
  * @return the instant article
  */
 public InstantArticle withCanonicalURL(String url) {
  this.canonicalURL = url;
  return this;
 }
 
 /**
  * With header.
  *
  * @param header the header
  * @return the instant article
  */
 public InstantArticle withHeader(Header header) {
  this.header = header;
  return this;
 }
 
 /**
  * With footer.
  *
  * @param footer the footer
  * @return the instant article
  */
 public InstantArticle withFooter(Footer footer) {
  this.footer = footer;
  return this;
 }
 
 /**
  * With related articles.
  *
  * @param relatedArticles the related articles
  * @return the instant article
  */
 public InstantArticle withRelatedArticles(RelatedArticles relatedArticles) {
  this.relatedArticles = relatedArticles;
  return this;
 }
 
 /**
  * To dom element.
  *
  * @param document
  *         the document
  * @return the element
  */
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  Element html = document.createElement("html");
  if (this.isRTLEnabled) {
   html.attr("dir", "rtl");
  }
  Element head = document.createElement("head");
  
  Element link = document.createElement("link");
  link.attr("rel", "canonical");
  link.attr("href", this.canonicalURL);
  head.appendChild(link);
  
  Element charset = document.createElement("meta");
  charset.attr("charset", this.charset);
  head.appendChild(charset);
  
  this.addMetaProperty("op:markup_version", this.markupVersion);
  
  if (this.header != null && CollectionUtils.isNotEmpty(this.header.getAds())) {
   this.addMetaProperty("fb:use_automatic_ad_placement", "" + (this.isAutomaticAdPlaced ? true : false));
  }
  
  if (this.style != null) {
   this.addMetaProperty("fb:article_style", this.style);
  }
  
  Iterator<String> itr = this.metaProperties.keySet().iterator();
  while (itr.hasNext()) {
   String key = itr.next();
   head.appendChild(this.createMetaElement(document, key, this.metaProperties.get(key).toString()));
  }
  html.appendChild(head);
  
  Element body = document.createElement("body");
  Element article = document.createElement("article");
  if (this.header != null && this.header.isValid()) {
   article.appendChild(this.header.toDOMElement());
  }
  if (CollectionUtils.isNotEmpty(this.children)) {
   Iterator<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> childItr = this.children.iterator();
   while (childItr.hasNext()) {
    com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement child = childItr.next();
    if (child instanceof TextContainer) {
     TextContainer textContainer = (TextContainer) child;
     if (CollectionUtils.isEmpty(textContainer.getTextChildren()) && CollectionUtils.isEmpty(textContainer.getContainerChildren())) {
      continue;
     } else if (textContainer.getTextChildren().size() == 1) {
      Object content = textContainer.getTextChildren().get(0);
      if (content instanceof String && content.toString().equals("")) {
       continue;
      }
     }
    }
    Element domElement = child.toDOMElement();
    if (domElement != null) {
     article.appendChild(domElement);
    }
   }
   
  } else {
   article.appendChild(new TextNode("", document.baseUri()));
  }
  if (this.relatedArticles != null && this.relatedArticles.isValid()) {
   article.appendChild(this.relatedArticles.toDOMElement());
  }
  
  if (this.footer != null && this.footer.isValid()) {
   article.appendChild(this.footer.toDOMElement(document));
  }
  
  body.appendChild(article);
  html.appendChild(body);
  
  return html;
  
 }
 
 /**
  * With style.
  *
  * @param style the style
  * @return the instant article
  */
 public InstantArticle withStyle(String style) {
  this.style = style;
  return this;
 }
 
 /**
  * Creates the meta element.
  *
  * @param document
  *         the document
  * @param propertyName
  *         the property name
  * @param propertyContent
  *         the property content
  * @return the element
  */
 private Element createMetaElement(Document document, String propertyName, String propertyContent) {
  Element meta = document.createElement("meta");
  meta.attr("property", propertyName);
  meta.attr("content", propertyContent);
  return meta;
 }
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }
 
 /**
  * Adds new child elements to this InstantArticle.
  *
  * @param child
  *         the child
  * @return $this
  */
 public InstantArticle addChild(com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement child) {
  children.add(child);
  return this;
 }
 
 /**
  * Gets the children.
  *
  * @return the children
  */
 public List<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> getChildren() {
  return children;
 }
 
 /**
  * Gets the canonical url.
  *
  * @return the canonical url
  */
 public String getCanonicalURL() {
  return canonicalURL;
 }
 
 /**
  * Checks if is automatic ad placed.
  *
  * @return true, if is automatic ad placed
  */
 public boolean isAutomaticAdPlaced() {
  return isAutomaticAdPlaced;
 }
 
 /**
  * Gets the charset.
  *
  * @return the charset
  */
 public String getCharset() {
  return charset;
 }
 
 /**
  * Gets the style.
  *
  * @return the style
  */
 public String getStyle() {
  return style;
 }
 
 /**
  * Gets the header.
  *
  * @return the header
  */
 public Header getHeader() {
  return header;
 }
 
 /**
  * Gets the footer.
  *
  * @return the footer
  */
 public Footer getFooter() {
  return footer;
 }
 
 /**
  * Checks if is RTL enabled.
  *
  * @return true, if is RTL enabled
  */
 public boolean isRTLEnabled() {
  return isRTLEnabled;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement()
  */
 @Override
 public Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#isValid()
  */
 public boolean isValid() {
  boolean headerValid = false;
  if (this.getHeader() != null) {
   headerValid = this.getHeader().isValid();
  }
  
  List<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> items = this.getChildren();
  
  boolean oneItemValid = false;
  if (CollectionUtils.isNotEmpty(items)) {
   Iterator<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> itr = items.iterator();
   while (itr.hasNext()) {
    com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement item = itr.next();
    if (item.isValid()) {
     oneItemValid = true;
     break;
    }
   }
  }
  boolean footerValid = false;
  if (this.getFooter() != null) {
   footerValid = this.getFooter().isValid();
   
  }
  return StringUtils.isNotBlank(this.canonicalURL) && headerValid && oneItemValid && footerValid;
  
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  return new ArrayList<Object>();
 }
 
 /**
  * Gets the related articles.
  *
  * @return the related articles
  */
 public RelatedArticles getRelatedArticles() {
  return this.relatedArticles;
 }
 
}
