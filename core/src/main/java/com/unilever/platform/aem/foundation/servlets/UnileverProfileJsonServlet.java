/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpStatus;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class sets profile details who is logged in.
 */
@SlingServlet(paths = { "/bin/personalisation/profileDetails" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UnileverProfileJsonServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "gets the profile details using cookies", propertyPrivate = false) })
public class UnileverProfileJsonServlet extends SlingAllMethodsServlet {
 
 private static final String COOKIE = "Cookie";
 private static final String UTF_8 = "utf-8";
 private static final String STRING_INSIDE_CLOSEBRACKET = ")";
 private static final String JSONP_STORE_JSONP_CALLBACKS_UNILEVERPROFILESTORE = "CQ_Analytics.JSONPStore.JSONPCallbacks.unileverprofilestore(";
 private static final String MD5TOKEN = "MD5token";
 private static final String JWTTOKEN = "Jwttoken";
 private static final String CONTENT_TYPE = "Content-Type";
 private static final String APPLICATION_JSON = "application/json";
 private static final String ACCEPT = "Accept";
 private static final String GET = "GET";
 private static final String REQUEST_URL = "requestUrl";
 /**
  * constant servlet id
  */
 private static final long serialVersionUID = 909094036852872658L;
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UnileverProfileJsonServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  doGet(request, response);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  String output = "";
  String requestUrl = request.getParameter(REQUEST_URL);
  LOGGER.info("request url is" + requestUrl);
  if (StringUtils.isNotBlank(requestUrl)) {
   try {
    URL url = new URL(requestUrl);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod(GET);
    conn.addRequestProperty(ACCEPT, APPLICATION_JSON);
    conn.addRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
    Cookie jwtToken = request.getCookie(JWTTOKEN);
    Cookie md5Token = request.getCookie(MD5TOKEN);
    setRequiredHeaders(jwtToken, conn);
    setRequiredHeaders(md5Token, conn);
    LOGGER.debug("request status is " + conn.getResponseCode() + "with message " + conn.getResponseMessage());
    if (conn.getResponseCode() == HttpStatus.SC_OK) {
     BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
     while ((output = br.readLine()) != null) {
      JSONObject myObject = new JSONObject(output);
      String message = JSONP_STORE_JSONP_CALLBACKS_UNILEVERPROFILESTORE + myObject.toString() + STRING_INSIDE_CLOSEBRACKET;
      response.setContentType(APPLICATION_JSON);
      response.setCharacterEncoding(UTF_8);
      response.getWriter().write(message);
     }
     conn.disconnect();
    }
   } catch (MalformedURLException e) {
    LOGGER.error("URL malformed exception", e);
   } catch (IOException e) {
    LOGGER.error("IO exception", e);
   }
  }
 }
 
 /**
  * @param cookie
  * @param connection
  */
 private void setRequiredHeaders(Cookie cookie, HttpURLConnection connection) {
  String cookieString = StringUtils.EMPTY;
  if (cookie != null) {
   cookieString = requiredHeaderString(cookie, cookieString);
  }
  if (StringUtils.isNotBlank(cookieString)) {
   connection.addRequestProperty(COOKIE, cookieString);
  }
 }
 
 /**
  * @param cookie
  * @param cookieString
  * @param value
  * @param originalFormat
  * @param requiredFormat
  * @return
  */
 private String requiredHeaderString(Cookie cookie, String cookieString) {
  String localCookieString = cookieString;
  if (StringUtils.isNotBlank(cookie.getValue())) {
   String httpConsider = StringUtils.equalsIgnoreCase(cookie.getName(), MD5TOKEN) ? "; HttpOnly" : "";
   String isSecure = cookie.getSecure() ? "; Secure" : "";
   String cookieName = StringUtils.equalsIgnoreCase(cookie.getName(), MD5TOKEN) ? MD5TOKEN : JWTTOKEN;
   String cookiePath = StringUtils.isNotBlank(cookie.getPath()) ? "; Path=" + cookie.getPath() : "";
   localCookieString = cookieName + "=" + cookie.getValue() + cookiePath + isSecure + httpConsider;
   LOGGER.info("request header info of " + cookie.getName() + " is " + localCookieString);
  }
  return localCookieString;
 }
}
