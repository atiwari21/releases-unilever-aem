/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Class Anchor.
 */
public class Anchor extends FormattedText {
 
 /** The href. */
 private String href;
 
 /** The rel. */
 private String rel;
 
 /**
  * Instantiates a new anchor.
  */
 private Anchor() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor
  */
 public static Anchor create() {
  return new Anchor();
 }
 
 /**
  * With href.
  *
  * @param href
  *         the href
  * @return the anchor
  */
 public Anchor withHref(String href) {
  this.href = href;
  return this;
 }
 
 /**
  * With rel.
  *
  * @param rel
  *         the rel
  * @return the anchor
  */
 public Anchor withRel(String rel) {
  this.rel = rel;
  return this;
 }
 
 /**
  * Gets the href.
  *
  * @return the href
  */
 public String getHref() {
  return href;
 }
 
 /**
  * Gets the rel.
  *
  * @return the rel
  */
 public String getRel() {
  return rel;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement()
  */
 public Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!this.isValid()) {
   return null;
  }
  Element anchor = document.createElement("a");
  if (this.href != null) {
   anchor.attr("href", this.href);
  }
  if (this.rel != null) {
   anchor.attr("rel", this.rel);
  }
  
  anchor.text(textToDOMDocumentFragment(document));
  return anchor;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#isValid()
  */
 public boolean isValid() {
  if (StringUtils.isNotBlank(this.href)) {
   if (this.href.endsWith(".html")) {
    return true;
   }
   try {
    new URL(this.href);
   } catch (MalformedURLException e) {
    return false;
   }
   return true;
   
  }
  return false;
 }
}
