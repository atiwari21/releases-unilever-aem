/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.scheduler.Job;
import org.apache.sling.commons.scheduler.JobContext;
import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.adobe.granite.security.user.UserPropertiesService;
import com.adobe.granite.taskmanagement.Task;
import com.adobe.granite.taskmanagement.TaskAction;
import com.adobe.granite.taskmanagement.TaskManager;
import com.adobe.granite.taskmanagement.TaskManagerException;
import com.adobe.granite.taskmanagement.TaskManagerFactory;
import com.day.cq.commons.Externalizer;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.mailer.MailService;
import com.day.cq.mailer.MailingException;
import com.day.cq.replication.AccessDeniedException;
import com.day.cq.replication.PathNotFoundException;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.security.User;
import com.day.crx.JcrConstants;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.helper.MailHelper;

/**
 * The Class AssetExpiryNotificationScheduler.
 */
@SuppressWarnings("deprecation")
@Component(metatype = true, label = "unilever DAM Expiry Notification", description = "Unilever DAM Expiry Notification")
public class AssetExpiryNotificationScheduler implements Job {
 
 /** The Constant PROPERTY_VALUE. */
 private static final String PROPERTY_VALUE = "1_property.value";
 
 /** The Constant PROPERTY. */
 private static final String PROPERTY = "1_property";
 
 /** The Constant DAM_ASSET. */
 private static final String DAM_ASSET = DamConstants.NT_DAM_ASSET;
 
 /** The Constant CONTENT_DAM. */
 private static final String CONTENT_DAM = "/content/dam";
 
 /** The Constant DATERANGE_UPPER_BOUND. */
 private static final String DATERANGE_UPPER_BOUND = "1_daterange.upperBound";
 
 /** The Constant DATERANGE_LOWER_BOUND. */
 private static final String DATERANGE_LOWER_BOUND = "1_daterange.lowerBound";
 
 /** The Constant DATERANGE_PROPERTY. */
 private static final String DATERANGE_PROPERTY = "1_daterange.property";
 
 /** The Constant TYPE. */
 private static final String TYPE = "type";
 
 /** The Constant PATH. */
 private static final String PATH = "path";
 
 /** The Constant JCR_CONTENT_METADATA. */
 private static final String JCR_CONTENT_METADATA = "jcr:content/metadata";
 
 /** The Constant JCR_CONTENT_METADATA_PRISM_EXPIRATION_DATE. */
 private static final String JCR_CONTENT_METADATA_PRISM_EXPIRATION_DATE = "jcr:content/metadata/prism:expirationDate";
 
 /** The Constant log. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AssetExpiryNotificationScheduler.class);
 
 /** The Constant ELEVEN. */
 private static final int ELEVEN = 11;
 
 /** The Constant TWELVE. */
 private static final int TWELVE = 12;
 
 /** The Constant THIRTEEN. */
 private static final int THIRTEEN = 13;
 
 /** The Constant FOURTEEN. */
 private static final int FOURTEEN = 14;
 
 /** The Constant UNILEVER TEAM. */
 private static final String UNILEVER_TEAM = "UNILEVER TEAM";
 
 /** The scheduler. */
 @Reference
 private Scheduler scheduler;
 
 /** The resolver factory. */
 @Reference
 private ResourceResolverFactory resolverFactory;
 
 /** The query builder. */
 @Reference
 private QueryBuilder queryBuilder;
 
 /** The mailer. */
 @Reference(cardinality = ReferenceCardinality.OPTIONAL_UNARY, policy = ReferencePolicy.DYNAMIC)
 private MailService mailer;
 
 /** The externalizer service. */
 @Reference
 Externalizer externalizerService;
 
 /** The user properties service. */
 @Reference
 UserPropertiesService userPropertiesService;
 
 /** The replicator. */
 @Reference
 private Replicator replicator;
 
 /** The Constant SCHEDULE_TIME_BASED. */
 @org.apache.felix.scr.annotations.Property(boolValue = {
   true }, label = "Time based Scheduler", description = "Whether to schedule a time based schedular")
 public static final String SCHEDULE_TIME_BASED = "cq.dam.expiry.notification.scheduler.istimebased";
 
 /** The Constant SCHEDULER_TIMEBASED_RULE. */
 @org.apache.felix.scr.annotations.Property(value = {
   "0 0 0/24 * * ?" }, label = "Time Based Scheduler Rule", description = "Regular expression for time based Scheduler. Eg: '0 0 0 * * ?'. "
     + "The example expression triggers the Job @ 00 hrs. This expression get picked if Time Based Scheduler is true")
 public static final String SCHEDULER_TIMEBASED_RULE = "cq.dam.expiry.notification.scheduler.timebased.rule";
 
 /** The Constant SCHEDULER_PERIOD. */
 @org.apache.felix.scr.annotations.Property(longValue = {
   86400L }, label = "Preiodic Scheduler", description = "Time in seconds for periodic scheduler. This expression get picked if Time Based Scheduler is set false")
 public static final String SCHEDULER_PERIOD = "cq.dam.expiry.notification.scheduler.period.rule";
 
 /** The Constant CONFIG_SEND_EMAIL. */
 @org.apache.felix.scr.annotations.Property(boolValue = {
   false }, name = "send_email", label = "send email", description = "Send emails on asset expiry")
 public static final String CONFIG_SEND_EMAIL = "send_email";
 
 /** The Constant PRIOR_NOTIFICATION_SECONDS. */
 @org.apache.felix.scr.annotations.Property(longValue = {
   604800L }, name = "prior_notification_seconds", label = "Prior notification in seconds", description = "Number of seconds before which a notification should be sent before an asset expires")
 public static final String PRIOR_NOTIFICATION_SECONDS = "prior_notification_seconds";
 
 /** The Constant PROTOCOL. */
 @org.apache.felix.scr.annotations.Property(value = { "http" }, label = "Protocol", description = "Protocol")
 public static final String PROTOCOL = "cq.dam.expiry.notification.url.protocol";
 
 /** The Constant TASK_TYPE. */
 public static final String TASK_TYPE = "Notification";
 
 /** The send email. */
 private boolean sendEmail;
 
 /** The protocol. */
 private String protocol;
 
 /** The last run. */
 private Calendar lastRun;
 
 /** The this run. */
 private Calendar thisRun;
 
 /** The prior notification in seconds. */
 private long priorNotificationInSeconds;
 
 /**
  * Instantiates a new asset expiry notification scheduler.
  */
 public AssetExpiryNotificationScheduler() {
  this.externalizerService = null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.commons.scheduler.Job#execute(org.apache.sling.commons. scheduler.JobContext)
  */
 /**
  * @param arg0
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 public void execute(JobContext arg0) {
  thisRun = Calendar.getInstance();
  try {
   ResourceResolver resolver = this.resolverFactory.getAdministrativeResourceResolver(null);
   Collection<Hit> expiredAssets = getExpiredAssetHits(resolver);
   Collection toExpireAssets = getToexpireAssetHits(resolver);
   Collection<Asset> expireRefererAssets = propagateExpiry(resolver, expiredAssets);
   dePropagateExpiry(resolver);
   
   Notification notification = new Notification(resolver, expiredAssets, toExpireAssets, expireRefererAssets);
   notification.sendPriorInboxNotification();
   notification.sendExpiryInBoxNotification();
   notification.sendRefExpiryInBoxNotification();
   if (this.sendEmail) {
    notification.sendPriorEmailNotification();
    notification.sendExpiryEmailNotification();
    notification.sendRefExpiryEmailNotification();
   }
   
   PublishOrUnPublishAssets publishOrUnPublishAssets = new PublishOrUnPublishAssets(resolver, "Deactivate");
   
   Collection assetPaths = new ArrayList();
   for (Hit hit : expiredAssets) {
    assetPaths.add(hit.getPath());
   }
   for (Asset each : expireRefererAssets) {
    assetPaths.add(each.getPath());
   }
   publishOrUnPublishAssets.action(assetPaths);
  } catch (Exception e) {
   LOGGER.error("Error in execute.", e);
  }
  lastRun = thisRun;
 }
 
 /**
  * Propagate expiry.
  *
  * @param resolver
  *         the resolver
  * @param hits
  *         the hits
  * @return the collection
  * @throws RepositoryException
  *          the repository exception
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 private Collection<Asset> propagateExpiry(ResourceResolver resolver, Collection<Hit> hits) throws RepositoryException {
  Session session = (Session) resolver.adaptTo(Session.class);
  Collection<Asset> expiredSinceLastRun = getExpiredAssets(resolver, hits);
  Collection expireRefererAssets = new ArrayList();
  for (Asset each : expiredSinceLastRun) {
   String path = each.getPath();
   Collection<Asset> refererAssets = DamUtil.getRefererAssets(resolver, each.getPath());
   Resource res = resolver.getResource(path);
   if (DamUtil.isSubAsset(res)) {
    Asset asset = DamUtil.getParentAsset(res);
    if (null != asset) {
     refererAssets.add(asset);
    }
   }
   expireRefererAssets.addAll(refererAssets);
   for (Asset refAsset : refererAssets) {
    Node refNode = (Node) resolver.getResource(refAsset.getPath()).adaptTo(Node.class);
    refNode.getNode(JCR_CONTENT_METADATA).setProperty("refExpired", true);
   }
  }
  session.save();
  return expireRefererAssets;
 }
 
 /**
  * Gets the expired assets.
  *
  * @param resolver
  *         the resolver
  * @param hits
  *         the hits
  * @return the expired assets
  * @throws RepositoryException
  *          the repository exception
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 private Collection<Asset> getExpiredAssets(ResourceResolver resolver, Collection<Hit> hits) throws RepositoryException {
  List expiredSinceLastRun = new ArrayList();
  if (!hits.isEmpty()) {
   for (Hit hit : hits) {
    Resource res = resolver.getResource(hit.getPath());
    Asset asset = null;
    if (null != res) {
     asset = (Asset) res.adaptTo(Asset.class);
     if (null != asset) {
      expiredSinceLastRun.add(asset);
     }
    }
   }
  }
  return expiredSinceLastRun;
 }
 
 /**
  * De propagate expiry.
  *
  * @param resolver
  *         the resolver
  * @throws RepositoryException
  *          the repository exception
  */
 private void dePropagateExpiry(ResourceResolver resolver) throws RepositoryException {
  Calendar now = Calendar.getInstance();
  Collection<Asset> expiredRefAssets = getExpiredRefAssets(resolver);
  for (Asset each : expiredRefAssets) {
   Collection<Asset> subAssets = DamUtil.getReferencedSubAssets(resolver.getResource(each.getPath()));
   subAssets.addAll(DamUtil.getSubAssets(resolver.getResource(each.getPath())));
   boolean toChangeStatus = true;
   for (Asset each1 : subAssets) {
    Node node = (Node) resolver.getResource(each1.getPath()).adaptTo(Node.class);
    if (!node.hasProperty(JCR_CONTENT_METADATA_PRISM_EXPIRATION_DATE)) {
     Calendar expiryTime = node.getProperty(JCR_CONTENT_METADATA_PRISM_EXPIRATION_DATE).getDate();
     if (expiryTime.before(now)) {
      toChangeStatus = false;
      break;
     }
    }
   }
   if (toChangeStatus) {
    Node eachNode = (Node) resolver.getResource(each.getPath()).adaptTo(Node.class);
    eachNode.getNode(JCR_CONTENT_METADATA).setProperty("refExpired", false);
   }
  }
  ((Session) resolver.adaptTo(Session.class)).save();
 }
 
 /**
  * Gets the expired ref assets.
  *
  * @param resolver
  *         the resolver
  * @return the expired ref assets
  * @throws RepositoryException
  *          the repository exception
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 private Collection<Asset> getExpiredRefAssets(ResourceResolver resolver) throws RepositoryException {
  Collection<Hit> hits = getExpiredRefAssetHits(resolver);
  List expiredRefAssets = new ArrayList();
  if (!hits.isEmpty()) {
   for (Hit hit : hits) {
    Resource res = resolver.getResource(hit.getPath());
    Asset asset = null;
    if (null != res) {
     asset = (Asset) res.adaptTo(Asset.class);
     if (null != asset) {
      expiredRefAssets.add(asset);
     }
    }
   }
  }
  return expiredRefAssets;
 }
 
 /**
  * Gets the expired asset hits.
  *
  * @param resolver
  *         the resolver
  * @return the expired asset hits
  */
 @SuppressWarnings({ "unchecked", "rawtypes" })
 private Collection<Hit> getExpiredAssetHits(ResourceResolver resolver) {
  Map map = new HashMap();
  map.put(PATH, CONTENT_DAM);
  map.put(TYPE, DAM_ASSET);
  map.put(DATERANGE_PROPERTY, JCR_CONTENT_METADATA_PRISM_EXPIRATION_DATE);
  map.put(DATERANGE_LOWER_BOUND, "" + lastRun.getTimeInMillis());
  map.put(DATERANGE_UPPER_BOUND, "" + thisRun.getTimeInMillis());
  
  Query query = this.queryBuilder.createQuery(PredicateGroup.create(map), (Session) resolver.adaptTo(Session.class));
  SearchResult results = query.getResult();
  return results.getHits();
 }
 
 /**
  * Gets the toexpire asset hits.
  *
  * @param resolver
  *         the resolver
  * @return the toexpire asset hits
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 private Collection<Hit> getToexpireAssetHits(ResourceResolver resolver) {
  long priorNotificationInMillis = this.priorNotificationInSeconds * 1000L;
  long thisRunTime = thisRun.getTimeInMillis();
  long lastRunTime = lastRun.getTimeInMillis();
  long startTime = thisRunTime;
  long endTime = thisRunTime + priorNotificationInMillis;
  
  if (priorNotificationInMillis > thisRunTime - lastRunTime) {
   startTime = priorNotificationInMillis + lastRunTime;
  }
  
  Map map = new HashMap();
  map.put(PATH, CONTENT_DAM);
  map.put(TYPE, DAM_ASSET);
  map.put(DATERANGE_PROPERTY, JCR_CONTENT_METADATA_PRISM_EXPIRATION_DATE);
  map.put(DATERANGE_LOWER_BOUND, "" + startTime);
  map.put(DATERANGE_UPPER_BOUND, "" + endTime);
  
  Query query = this.queryBuilder.createQuery(PredicateGroup.create(map), (Session) resolver.adaptTo(Session.class));
  SearchResult results = query.getResult();
  return results.getHits();
 }
 
 /**
  * Gets the expired ref asset hits.
  *
  * @param resolver
  *         the resolver
  * @return the expired ref asset hits
  */
 @SuppressWarnings({ "rawtypes", "unchecked" })
 private Collection<Hit> getExpiredRefAssetHits(ResourceResolver resolver) {
  Map map = new HashMap();
  map.put(PATH, CONTENT_DAM);
  map.put(TYPE, DAM_ASSET);
  map.put(PROPERTY, "jcr:content/metadata/refExpired");
  map.put(PROPERTY_VALUE, "true");
  
  Query query = this.queryBuilder.createQuery(PredicateGroup.create(map), (Session) resolver.adaptTo(Session.class));
  SearchResult results = query.getResult();
  return results.getHits();
 }
 
 /**
  * Activate.
  *
  * @param componentContext
  *         the component context
  * @throws Exception
  *          the exception
  */
 protected void activate(ComponentContext componentContext) throws Exception {
  try {
   lastRun = Calendar.getInstance();
   lastRun.set(ELEVEN, 0);
   lastRun.set(TWELVE, 0);
   lastRun.set(THIRTEEN, 0);
   lastRun.set(FOURTEEN, 0);
   boolean isTimeBased = ((Boolean) componentContext.getProperties().get("cq.dam.expiry.notification.scheduler.istimebased")).booleanValue();
   
   if (isTimeBased) {
    String expression = (String) componentContext.getProperties().get("cq.dam.expiry.notification.scheduler.timebased.rule");
    this.scheduler.addJob(AssetExpiryNotificationScheduler.class.getName(), this, null, expression, false);
   } else {
    long periodinsecs = ((Long) componentContext.getProperties().get("cq.dam.expiry.notification.scheduler.period.rule")).longValue();
    this.scheduler.addPeriodicJob(AssetExpiryNotificationScheduler.class.getName(), this, null, periodinsecs, false);
   }
   this.sendEmail = ((Boolean) componentContext.getProperties().get("send_email")).booleanValue();
   this.priorNotificationInSeconds = ((Long) componentContext.getProperties().get("prior_notification_seconds")).longValue();
   this.protocol = ((String) componentContext.getProperties().get("cq.dam.expiry.notification.url.protocol"));
  } catch (Exception e) {
   LOGGER.error("Error in activate.", e);
   throw e;
  }
 }
 
 /**
  * Deactivate.
  *
  * @param componentContext
  *         the component context
  */
 protected void deactivate(ComponentContext componentContext) {
  LOGGER.debug("Deactivating the expiry notification scheduler");
  LOGGER.debug("ComponentContext " + componentContext.toString());
  this.scheduler.removeJob(AssetExpiryNotificationScheduler.class.getName());
 }
 
 /**
  * Bind scheduler.
  *
  * @param paramScheduler
  *         the param scheduler
  */
 protected void bindScheduler(Scheduler paramScheduler) {
  this.scheduler = paramScheduler;
 }
 
 /**
  * Unbind scheduler.
  *
  * @param paramScheduler
  *         the param scheduler
  */
 protected void unbindScheduler(Scheduler paramScheduler) {
  if (this.scheduler == paramScheduler) {
   this.scheduler = null;
  }
 }
 
 /**
  * Bind resolver factory.
  *
  * @param paramResourceResolverFactory
  *         the param resource resolver factory
  */
 protected void bindResolverFactory(ResourceResolverFactory paramResourceResolverFactory) {
  this.resolverFactory = paramResourceResolverFactory;
 }
 
 /**
  * Unbind resolver factory.
  *
  * @param paramResourceResolverFactory
  *         the param resource resolver factory
  */
 protected void unbindResolverFactory(ResourceResolverFactory paramResourceResolverFactory) {
  if (this.resolverFactory == paramResourceResolverFactory) {
   this.resolverFactory = null;
  }
 }
 
 /**
  * Bind query builder.
  *
  * @param paramQueryBuilder
  *         the param query builder
  */
 protected void bindQueryBuilder(QueryBuilder paramQueryBuilder) {
  this.queryBuilder = paramQueryBuilder;
 }
 
 /**
  * Unbind query builder.
  *
  * @param paramQueryBuilder
  *         the param query builder
  */
 protected void unbindQueryBuilder(QueryBuilder paramQueryBuilder) {
  if (this.queryBuilder == paramQueryBuilder) {
   this.queryBuilder = null;
  }
 }
 
 /**
  * Bind mailer.
  *
  * @param paramMailService
  *         the param mail service
  */
 protected void bindMailer(MailService paramMailService) {
  this.mailer = paramMailService;
 }
 
 /**
  * Unbind mailer.
  *
  * @param paramMailService
  *         the param mail service
  */
 protected void unbindMailer(MailService paramMailService) {
  if (this.mailer == paramMailService) {
   this.mailer = null;
  }
 }
 
 /**
  * Bind externalizer service.
  *
  * @param paramExternalizer
  *         the param externalizer
  */
 protected void bindExternalizerService(Externalizer paramExternalizer) {
  this.externalizerService = paramExternalizer;
 }
 
 /**
  * Unbind externalizer service.
  *
  * @param paramExternalizer
  *         the param externalizer
  */
 protected void unbindExternalizerService(Externalizer paramExternalizer) {
  if (this.externalizerService == paramExternalizer) {
   this.externalizerService = null;
  }
 }
 
 /**
  * Bind user properties service.
  *
  * @param paramUserPropertiesService
  *         the param user properties service
  */
 protected void bindUserPropertiesService(UserPropertiesService paramUserPropertiesService) {
  this.userPropertiesService = paramUserPropertiesService;
 }
 
 /**
  * Unbind user properties service.
  *
  * @param paramUserPropertiesService
  *         the param user properties service
  */
 protected void unbindUserPropertiesService(UserPropertiesService paramUserPropertiesService) {
  if (this.userPropertiesService == paramUserPropertiesService) {
   this.userPropertiesService = null;
  }
 }
 
 /**
  * Bind replicator.
  *
  * @param paramReplicator
  *         the param replicator
  */
 protected void bindReplicator(Replicator paramReplicator) {
  this.replicator = paramReplicator;
 }
 
 /**
  * Unbind replicator.
  *
  * @param paramReplicator
  *         the param replicator
  */
 protected void unbindReplicator(Replicator paramReplicator) {
  if (this.replicator == paramReplicator) {
   this.replicator = null;
  }
 }
 
 /**
  * The Class Notification.
  */
 private class Notification {
  
  /** The Constant REP_DENY_ACE. */
  private static final String REP_DENY_ACE = "rep:DenyACE";
  
  /** The Constant REP_PRINCIPAL_NAME. */
  private static final String REP_PRINCIPAL_NAME = "rep:principalName";
  
  /** The Constant REP_POLICY. */
  private static final String REP_POLICY = "/rep:policy";
  
  /** The Constant ASSET_REFERENCE_EMAIL_TEMPLATE_PATH. */
  private static final String ASSET_REFERENCE_EMAIL_TEMPLATE_PATH = "/etc/notification/email/default/com.adobe.cq.dam.asset.refexpiry/en.txt";
  
  /** The Constant ASSET_EXPIRY_EMAIL_TEMPLATE_PATH. */
  private static final String ASSET_EXPIRY_EMAIL_TEMPLATE_PATH = "/etc/notification/email/default/com.adobe.cq.dam.asset.expiry/en.txt";
  
  /** The Constant REMOVE. */
  private static final String REMOVE = "Remove";
  
  /** The Constant NOTIFICATION. */
  private static final String NOTIFICATION_VALUE = "Notification";
  
  /** The Constant EMAIL. */
  private static final String EMAIL = "email";
  
  /** The Constant PROFILE. */
  private static final String PROFILE = "profile";
  
  /** The Constant ASSETLINK. */
  private static final String ASSETLINK = "assetlink";
  
  /** The Constant ASSETS_HTML_CONTENT_DAM. */
  private static final String ASSETS_HTML_CONTENT_DAM = "/assets.html/content/dam";
  
  /** The Constant HOST_USER_FULL_NAME. */
  private static final String HOST_USER_FULL_NAME = "hostUserFullName";
  
  /** The Constant ASSETPATHS. */
  private static final String ASSETPATHS = "assetpaths";
  
  /** The Constant ASSET_PRIOR_EMAIL_TEMPLATE_PATH. */
  private static final String ASSET_PRIOR_EMAIL_TEMPLATE_PATH = "/etc/notification/email/default/com.adobe.cq.dam.asset.expiry.prior/en.txt";
  
  /** The Constant TEMPLATE_ROOT_PATH. */
  private static final String TEMPLATE_ROOT_PATH = "templateRootPath";
  
  /** The resolver. */
  ResourceResolver resolver;
  
  /** The expired assets with user. */
  Map<String, Set<String>> expiredAssetsWithUser;
  
  /** The to expire assets with user. */
  Map<String, Set<String>> toExpireAssetsWithUser;
  
  /** The expired ref assets with user. */
  Map<String, Set<String>> expiredRefAssetsWithUser;
  
  /** The upm. */
  UserPropertiesManager upm;
  
  /**
   * Instantiates a new notification.
   *
   * @param resolver
   *         the resolver
   * @param expiredAssets
   *         the expired assets
   * @param toExpireAssets
   *         the to expire assets
   * @param expiredRefAssets
   *         the expired ref assets
   * @throws RepositoryException
   *          the repository exception
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public Notification(ResourceResolver resolver, Collection<Hit> expiredAssets, Collection<Hit> toExpireAssets, Collection<Asset> expiredRefAssets)
    throws RepositoryException {
   this.resolver = resolver;
   this.upm = AssetExpiryNotificationScheduler.this.userPropertiesService.createUserPropertiesManager((Session) resolver.adaptTo(Session.class),
     resolver);
   
   this.expiredAssetsWithUser = new HashMap();
   this.expiredRefAssetsWithUser = new HashMap();
   this.toExpireAssetsWithUser = new HashMap();
   Resource expiredRefAssetsResource = null;
   setExpiredAssets(resolver, expiredAssets, this.expiredAssetsWithUser);
   setExpiredAssets(resolver, toExpireAssets, this.toExpireAssetsWithUser);
   for (Asset each : expiredRefAssets) {
    expiredRefAssetsResource = null;
    setExpiredAssetsWithUserId(resolver, this.expiredRefAssetsWithUser, each.getPath(), expiredRefAssetsResource);
   }
  }
  
  /**
   * Sets the expired assets.
   *
   * @param resolver
   *         the resolver
   * @param expiredAndToExpiredAssets
   *         the expired and to expired assets
   * @param requiredExpiryAssetsWithUser
   *         the required expiry assets with user
   * @param resource
   *         the resource
   * @throws RepositoryException
   *          the repository exception
   * @throws UnsupportedRepositoryOperationException
   *          the unsupported repository operation exception
   * @throws PathNotFoundException
   *          the path not found exception
   * @throws AccessDeniedException
   *          the access denied exception
   */
  private void setExpiredAssets(ResourceResolver resolver, Collection<Hit> expiredAndToExpiredAssets,
    Map<String, Set<String>> requiredExpiryAssetsWithUser)
    throws RepositoryException, UnsupportedRepositoryOperationException, javax.jcr.PathNotFoundException, javax.jcr.AccessDeniedException {
   for (Hit each : expiredAndToExpiredAssets) {
    setExpiredAssetsWithUserId(resolver, requiredExpiryAssetsWithUser, each.getPath(), null);
   }
  }
  
  /**
   * Sets the expired assets with user id.
   *
   * @param resolver
   *         the resolver
   * @param requiredExpiryAssetsWithUser
   *         the required expiry assets with user
   * @param assetPath
   *         the asset path
   * @param resource
   *         the resource
   * @throws RepositoryException
   *          the repository exception
   * @throws UnsupportedRepositoryOperationException
   *          the unsupported repository operation exception
   * @throws PathNotFoundException
   *          the path not found exception
   * @throws AccessDeniedException
   *          the access denied exception
   */
  private void setExpiredAssetsWithUserId(ResourceResolver resolver, Map<String, Set<String>> requiredExpiryAssetsWithUser, String assetPath,
    Resource resource)
    throws RepositoryException, UnsupportedRepositoryOperationException, javax.jcr.PathNotFoundException, javax.jcr.AccessDeniedException {
   Resource res = resolver.getResource(assetPath);
   Asset asset = null;
   if (null != res) {
    asset = (Asset) res.adaptTo(Asset.class);
    if (null != asset) {
     setUsersToExpiryNotification(assetPath, requiredExpiryAssetsWithUser, resource);
    }
   }
  }
  
  /**
   * Sets the users to expiry notification.
   *
   * @param assetPath
   *         the asset path
   * @param requiredExpiryAssetsWithUser
   *         the required expiry assets with user
   * @param resourceBundle
   *         the resource bundle
   * @throws UnsupportedRepositoryOperationException
   *          the unsupported repository operation exception
   * @throws RepositoryException
   *          the repository exception
   * @throws PathNotFoundException
   *          the path not found exception
   * @throws AccessDeniedException
   *          the access denied exception
   */
  @SuppressWarnings({ "unchecked", "rawtypes" })
  private void setUsersToExpiryNotification(String assetPath, Map<String, Set<String>> requiredExpiryAssetsWithUser, Resource resourceBundle)
    throws UnsupportedRepositoryOperationException, RepositoryException, javax.jcr.PathNotFoundException, javax.jcr.AccessDeniedException {
   boolean userHasAccess = true;
   String originalAssetPath = assetPath;
   String localAssetPath = assetPath;
   Resource localResourceBundle = resourceBundle;
   com.day.cq.security.UserManager userManagerd = this.resolver.adaptTo(com.day.cq.security.UserManager.class);
   Iterator<User> userList = userManagerd.getUsers();
   while (userList.hasNext()) {
    User user = userList.next();
    String userId = user.getID();
    if (StringUtils.isNotBlank(userId)) {
     Resource resource = this.resolver.getResource(localAssetPath + REP_POLICY);
     String lastParentAssetPath = localAssetPath;
     localAssetPath = lastParentAssetPath;
     int assetHeirarchy = StringUtils.split(lastParentAssetPath, CommonConstants.SLASH_CHAR_LITERAL).length;
     if (localResourceBundle != null) {
      NodeIterator iterator = localResourceBundle.adaptTo(Node.class).getNodes();
      if (iterator.hasNext()) {
       userHasAccess = isUserHasAccess(userId, iterator);
      }
     } else {
      if (resource == null) {
       for (int i = 0; i < assetHeirarchy; i++) {
        int lastSecondIndex = lastParentAssetPath.lastIndexOf(CommonConstants.SLASH_CHAR_LITERAL);
        lastParentAssetPath = StringUtils.substring(lastParentAssetPath, 0, lastSecondIndex);
        localAssetPath = (StringUtils.startsWith(lastParentAssetPath, CONTENT_DAM)) ? lastParentAssetPath : localAssetPath;
        if (!localAssetPath.equals(originalAssetPath)) {
         resource = this.resolver.getResource(localAssetPath + REP_POLICY);
         if (resource != null) {
          NodeIterator iterator = resource.adaptTo(Node.class).getNodes();
          if (iterator.hasNext()) {
           break;
          }
         }
        }
       }
      }
      if (resource != null) {
       NodeIterator iterator = resource.adaptTo(Node.class).getNodes();
       if (iterator.hasNext()) {
        userHasAccess = isUserHasAccess(userId, iterator);
       } else {
        for (int i = 0; i < StringUtils.split(resource.getPath().replace(REP_POLICY, ""), CommonConstants.SLASH_CHAR_LITERAL).length; i++) {
         int lastSecondIndex = resource.getPath().replace(REP_POLICY, "").lastIndexOf(CommonConstants.SLASH_CHAR_LITERAL);
         lastParentAssetPath = StringUtils.substring(lastParentAssetPath, 0, lastSecondIndex);
         localAssetPath = (StringUtils.startsWith(lastParentAssetPath, CONTENT_DAM)) ? lastParentAssetPath : localAssetPath;
         if (!localAssetPath.equals(resource.getPath().replace(REP_POLICY, ""))) {
          resource = this.resolver.getResource(localAssetPath + REP_POLICY);
          if (resource != null) {
           iterator = resource.adaptTo(Node.class).getNodes();
           if (iterator.hasNext()) {
            break;
           }
          }
         }
        }
       }
      } else {
       lastParentAssetPath = originalAssetPath;
       localAssetPath = lastParentAssetPath;
       assetHeirarchy = StringUtils.split(lastParentAssetPath, CommonConstants.SLASH_CHAR_LITERAL).length;
       for (int i = 0; i < assetHeirarchy; i++) {
        int lastSecondIndex = lastParentAssetPath.lastIndexOf(CommonConstants.SLASH_CHAR_LITERAL);
        lastParentAssetPath = StringUtils.substring(lastParentAssetPath, 0, lastSecondIndex);
        localAssetPath = (StringUtils.startsWith(lastParentAssetPath, CONTENT_DAM)) ? lastParentAssetPath : localAssetPath;
        if (!localAssetPath.equals(originalAssetPath)) {
         resource = this.resolver.getResource(localAssetPath + REP_POLICY);
         if (resource != null) {
          NodeIterator iterator = resource.adaptTo(Node.class).getNodes();
          if (iterator.hasNext()) {
           break;
          }
         }
        }
       }
      }
      if (resource != null) {
       NodeIterator iterator = resource.adaptTo(Node.class).getNodes();
       if (iterator.hasNext()) {
        userHasAccess = isUserHasAccess(userId, iterator);
       }
       localResourceBundle = resource;
      }
     }
     if (requiredExpiryAssetsWithUser.containsKey(userId) && userHasAccess) {
      ((Set) requiredExpiryAssetsWithUser.get(userId)).add(originalAssetPath);
     } else if (userHasAccess) {
      Set set = new HashSet();
      set.add(originalAssetPath);
      requiredExpiryAssetsWithUser.put(userId, set);
     }
    }
   }
  }
  
  /**
   * Checks if is user has access.
   *
   * @param userId
   *         the user id
   * @param iterator
   *         the iterator
   * @return true, if is user has access
   * @throws ValueFormatException
   *          the value format exception
   * @throws RepositoryException
   *          the repository exception
   * @throws PathNotFoundException
   *          the path not found exception
   */
  private boolean isUserHasAccess(String userId, NodeIterator iterator)
    throws ValueFormatException, RepositoryException, javax.jcr.PathNotFoundException {
   boolean userHasAccess;
   Node node = iterator.nextNode();
   String primaryType = node.getProperty(JcrConstants.JCR_PRIMARYTYPE).getString();
   if (REP_DENY_ACE.equals(primaryType) && userId.equals(node.getProperty(REP_PRINCIPAL_NAME).getString())) {
    userHasAccess = false;
   } else {
    userHasAccess = true;
   }
   return userHasAccess;
  }
  
  /**
   * Send prior inbox notification.
   *
   * @throws TaskManagerException
   *          the task manager exception
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public void sendPriorInboxNotification() throws TaskManagerException {
   if ((null == this.toExpireAssetsWithUser) || (this.toExpireAssetsWithUser.isEmpty())) {
    return;
   }
   Iterator entries = this.toExpireAssetsWithUser.entrySet().iterator();
   TaskManager tm = (TaskManager) this.resolver.adaptTo(TaskManager.class);
   TaskManagerFactory tmf = tm.getTaskManagerFactory();
   while (entries.hasNext()) {
    StringBuilder description = new StringBuilder("Following Assets are about to expired:\n");
    Map.Entry entry = (Map.Entry) entries.next();
    Set<String> asstePaths = (Set) entry.getValue();
    for (String each : asstePaths) {
     description.append("\n" + each);
    }
    Task newTask = tmf.newTask(NOTIFICATION_VALUE);
    newTask.setName("ASSETS ABOUT TO EXPIRE");
    TaskAction newTaskAction = tmf.newTaskAction(REMOVE);
    newTask.setActions(Collections.singletonList(newTaskAction));
    newTask.setContentPath(CONTENT_DAM);
    newTask.setCurrentAssignee((String) entry.getKey());
    newTask.setDescription(description.toString());
    tm.createTask(newTask);
   }
  }
  
  /**
   * Send prior email notification.
   *
   * @throws RepositoryException
   *          the repository exception
   * @throws MailingException
   *          the mailing exception
   * @throws IOException
   *          Signals that an I/O exception has occurred.
   * @throws LoginException
   *          the login exception
   * @throws EmailException
   *          the email exception
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  private void sendPriorEmailNotification() throws RepositoryException, MailingException, IOException, LoginException, EmailException {
   if ((null == this.toExpireAssetsWithUser) || (this.toExpireAssetsWithUser.isEmpty())) {
    return;
   }
   Iterator entries = this.toExpireAssetsWithUser.entrySet().iterator();
   
   while (entries.hasNext()) {
    Map parameters = new HashMap();
    Map.Entry entry = (Map.Entry) entries.next();
    Set<String> asstePaths = (Set) entry.getValue();
    parameters.put(TEMPLATE_ROOT_PATH, ASSET_PRIOR_EMAIL_TEMPLATE_PATH);
    
    StringBuilder allAssetPaths = new StringBuilder("");
    for (String each : asstePaths) {
     allAssetPaths.append("\n" + each);
    }
    parameters.put(ASSETPATHS, allAssetPaths.toString());
    parameters.put(HOST_USER_FULL_NAME, UNILEVER_TEAM);
    
    String assetsLink = AssetExpiryNotificationScheduler.this.externalizerService.absoluteLink(this.resolver,
      AssetExpiryNotificationScheduler.this.protocol, ASSETS_HTML_CONTENT_DAM);
    parameters.put(ASSETLINK, assetsLink);
    
    UserManager um = ((JackrabbitSession) this.resolver.adaptTo(Session.class)).getUserManager();
    Authorizable user = um.getAuthorizable((String) entry.getKey());
    UserProperties up = this.upm.getUserProperties(user, PROFILE);
    
    String email = up == null ? "" : up.getProperty(EMAIL);
    
    if ((AssetExpiryNotificationScheduler.this.sendEmail) && (email != null) && (!"".equals(email))) {
     MailHelper.sendMail((Session) this.resolver.adaptTo(Session.class), AssetExpiryNotificationScheduler.this.mailer,
       Collections.singletonMap(up.getDisplayName(), email), parameters);
    }
   }
  }
  
  /**
   * Send expiry in box notification.
   *
   * @throws TaskManagerException
   *          the task manager exception
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public void sendExpiryInBoxNotification() throws TaskManagerException {
   if ((null == this.expiredAssetsWithUser) || (this.expiredAssetsWithUser.isEmpty())) {
    return;
   }
   Iterator entries = this.expiredAssetsWithUser.entrySet().iterator();
   
   TaskManager tm = (TaskManager) this.resolver.adaptTo(TaskManager.class);
   TaskManagerFactory tmf = tm.getTaskManagerFactory();
   while (entries.hasNext()) {
    StringBuilder description = new StringBuilder("Following assets have expired:\n");
    Map.Entry entry = (Map.Entry) entries.next();
    Set<String> asstePaths = (Set) entry.getValue();
    for (String each : asstePaths) {
     description.append("\n" + each);
    }
    Task newTask = tmf.newTask(NOTIFICATION_VALUE);
    newTask.setName("ASSETS EXPIRED");
    TaskAction newTaskAction = tmf.newTaskAction(REMOVE);
    newTask.setActions(Collections.singletonList(newTaskAction));
    newTask.setContentPath(CONTENT_DAM);
    newTask.setCurrentAssignee((String) entry.getKey());
    newTask.setDescription(description.toString());
    tm.createTask(newTask);
   }
  }
  
  /**
   * Send ref expiry in box notification.
   *
   * @throws TaskManagerException
   *          the task manager exception
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public void sendRefExpiryInBoxNotification() throws TaskManagerException {
   if ((null == this.expiredRefAssetsWithUser) || (this.expiredRefAssetsWithUser.isEmpty())) {
    return;
   }
   Iterator entries = this.expiredRefAssetsWithUser.entrySet().iterator();
   
   TaskManager tm = (TaskManager) this.resolver.adaptTo(TaskManager.class);
   TaskManagerFactory tmf = tm.getTaskManagerFactory();
   while (entries.hasNext()) {
    StringBuilder description = new StringBuilder("Following Assets have expired subassets:\n");
    Map.Entry entry = (Map.Entry) entries.next();
    Set<String> asstePaths = (Set) entry.getValue();
    for (String each : asstePaths) {
     description.append("\n" + each);
    }
    Task newTask = tmf.newTask(NOTIFICATION_VALUE);
    newTask.setName("SUBASSETS EXPIRED");
    TaskAction newTaskAction = tmf.newTaskAction(REMOVE);
    newTask.setActions(Collections.singletonList(newTaskAction));
    newTask.setContentPath(CONTENT_DAM);
    newTask.setCurrentAssignee((String) entry.getKey());
    newTask.setDescription(description.toString());
    tm.createTask(newTask);
   }
  }
  
  /**
   * Send expiry email notification.
   *
   * @throws RepositoryException
   *          the repository exception
   * @throws MailingException
   *          the mailing exception
   * @throws IOException
   *          Signals that an I/O exception has occurred.
   * @throws LoginException
   *          the login exception
   * @throws EmailException
   *          the email exception
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  private void sendExpiryEmailNotification() throws RepositoryException, MailingException, IOException, LoginException, EmailException {
   if ((null == this.expiredAssetsWithUser) || (this.expiredAssetsWithUser.isEmpty())) {
    return;
   }
   Iterator entries = this.expiredAssetsWithUser.entrySet().iterator();
   
   while (entries.hasNext()) {
    Map parameters = new HashMap();
    Map.Entry entry = (Map.Entry) entries.next();
    Set<String> asstePaths = (Set) entry.getValue();
    parameters.put(TEMPLATE_ROOT_PATH, ASSET_EXPIRY_EMAIL_TEMPLATE_PATH);
    
    StringBuilder allAssetPaths = new StringBuilder("");
    for (String each : asstePaths) {
     allAssetPaths.append("\n" + each);
    }
    parameters.put(ASSETPATHS, allAssetPaths.toString());
    parameters.put(HOST_USER_FULL_NAME, UNILEVER_TEAM);
    
    String assetsLink = AssetExpiryNotificationScheduler.this.externalizerService.absoluteLink(this.resolver,
      AssetExpiryNotificationScheduler.this.protocol, ASSETS_HTML_CONTENT_DAM);
    parameters.put(ASSETLINK, assetsLink);
    
    UserManager um = ((JackrabbitSession) this.resolver.adaptTo(Session.class)).getUserManager();
    Authorizable user = um.getAuthorizable((String) entry.getKey());
    UserProperties up = this.upm.getUserProperties(user, PROFILE);
    
    String email = up == null ? "" : up.getProperty(EMAIL);
    
    if ((AssetExpiryNotificationScheduler.this.sendEmail) && (email != null) && (!"".equals(email))) {
     MailHelper.sendMail((Session) this.resolver.adaptTo(Session.class), AssetExpiryNotificationScheduler.this.mailer,
       Collections.singletonMap(up.getDisplayName(), email), parameters);
    }
   }
  }
  
  /**
   * Send ref expiry email notification.
   *
   * @throws RepositoryException
   *          the repository exception
   * @throws MailingException
   *          the mailing exception
   * @throws IOException
   *          Signals that an I/O exception has occurred.
   * @throws LoginException
   *          the login exception
   * @throws EmailException
   *          the email exception
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  private void sendRefExpiryEmailNotification() throws RepositoryException, MailingException, IOException, LoginException, EmailException {
   if ((null == this.expiredRefAssetsWithUser) || (this.expiredRefAssetsWithUser.isEmpty())) {
    return;
   }
   Iterator entries = this.expiredRefAssetsWithUser.entrySet().iterator();
   
   while (entries.hasNext()) {
    Map parameters = new HashMap();
    Map.Entry entry = (Map.Entry) entries.next();
    Set<String> asstePaths = (Set) entry.getValue();
    parameters.put(TEMPLATE_ROOT_PATH, ASSET_REFERENCE_EMAIL_TEMPLATE_PATH);
    
    StringBuilder allAssetPaths = new StringBuilder("");
    for (String each : asstePaths) {
     allAssetPaths.append("\n" + each);
    }
    parameters.put(ASSETPATHS, allAssetPaths.toString());
    parameters.put(HOST_USER_FULL_NAME, UNILEVER_TEAM);
    
    String assetsLink = AssetExpiryNotificationScheduler.this.externalizerService.absoluteLink(this.resolver,
      AssetExpiryNotificationScheduler.this.protocol, ASSETS_HTML_CONTENT_DAM);
    parameters.put(ASSETLINK, assetsLink);
    
    UserManager um = ((JackrabbitSession) this.resolver.adaptTo(Session.class)).getUserManager();
    Authorizable user = um.getAuthorizable((String) entry.getKey());
    UserProperties up = this.upm.getUserProperties(user, PROFILE);
    
    String email = up == null ? "" : up.getProperty(EMAIL);
    
    if ((AssetExpiryNotificationScheduler.this.sendEmail) && (email != null) && (!"".equals(email))) {
     MailHelper.sendMail((Session) this.resolver.adaptTo(Session.class), AssetExpiryNotificationScheduler.this.mailer,
       Collections.singletonMap(up.getDisplayName(), email), parameters);
    }
   }
  }
 }
 
 /**
  * The Class PublishOrUnPublishAssets.
  */
 private class PublishOrUnPublishAssets {
  
  /** The resolver. */
  ResourceResolver resolver;
  
  /** The action. */
  ReplicationActionType action;
  
  /**
   * Instantiates a new publish or un publish assets.
   *
   * @param resolver
   *         the resolver
   * @param action
   *         the action
   */
  public PublishOrUnPublishAssets(ResourceResolver resolver, String action) {
   this.resolver = resolver;
   this.action = ReplicationActionType.fromName(action);
   
   if ((null == this.resolver) || (null == this.action)) {
    throw new RuntimeException("Cannot read the action type or resource resolver");
   }
  }
  
  /**
   * Action.
   *
   * @param assets
   *         the assets
   */
  public void action(Collection<String> assets) {
   Session session = (Session) this.resolver.adaptTo(Session.class);
   if (null == AssetExpiryNotificationScheduler.this.replicator) {
    AssetExpiryNotificationScheduler.LOGGER.error("Replicator service is not available");
    return;
   }
   if ((null == assets) || assets.isEmpty()) {
    AssetExpiryNotificationScheduler.LOGGER.info("No assets passed to deactivate.");
    return;
   }
   for (String eachPath : assets) {
    try {
     AssetExpiryNotificationScheduler.this.replicator.replicate(session, this.action, eachPath);
    } catch (PathNotFoundException pnfe) {
     AssetExpiryNotificationScheduler.LOGGER.error("Path not found for replication: " + eachPath + ", Exception Trace:", pnfe);
    } catch (AccessDeniedException ade) {
     AssetExpiryNotificationScheduler.LOGGER
       .debug(this.resolver.getUserID() + " is not allowed to replicate " + "this resource " + eachPath + ". Issuing request for 'replication.", ade);
    } catch (ReplicationException e) {
     AssetExpiryNotificationScheduler.LOGGER.error("Error during replication: " + e.getMessage(), e);
    }
   }
  }
 }
}
