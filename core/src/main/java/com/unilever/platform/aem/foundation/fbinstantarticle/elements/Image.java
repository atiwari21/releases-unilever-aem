/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * The Class Image.
 */
public class Image extends Audible implements Container {
 
 /** The caption for Image. */
 private Caption caption;
 
 /** The URL. */
 private String url;
 
 /** The Lazy Load URL. */
 private String lazyLoadURL;
 
 /** Tells if like is enabled. Default: false. */
 private boolean isLikeEnabled = false;
 
 /** Tells if comments is enabled. Default: false. */
 private boolean isCommentsEnabled = false;
 
 /**
  * string The picture size for the video.
  */
 private String presentation;
 
 /** GeoTag The Map object. */
 private GeoTag geoTag;
 
 /** Audio The audio file for this Image. */
 private Audio audio;
 
 /**
  * Creates the.
  *
  * @return the image
  */
 public static Image create() {
  return new Image();
 }
 
 /**
  * Instantiates a new image.
  */
 private Image() {
  
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  return new ArrayList<Object>();
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement()
  */
 @Override
 public Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  org.jsoup.nodes.Element element = document.createElement("figure");
  
  if (this.isLikeEnabled || this.isCommentsEnabled) {
   if (this.isLikeEnabled && this.isCommentsEnabled) {
    element.attr("data-feedback", "fb:likes,fb:comments");
    element.attr("data-feedback", "fb:likes");
   } else {
    element.attr("data-feedback", "fb:comments");
   }
  }
  
  if (StringUtils.isNotBlank(this.presentation)) {
   element.attr("data-mode", this.presentation);
  }
  
  if (StringUtils.isNotBlank(this.url)) {
   org.jsoup.nodes.Element imageElement = document.createElement("img");
   imageElement.attr("src", this.url);
   element.append(imageElement.outerHtml());
  } else if (StringUtils.isNotBlank(this.lazyLoadURL)) {
   org.jsoup.nodes.Element imageElement = document.createElement("img");
   imageElement.attr("src", this.lazyLoadURL);
   element.append(imageElement.outerHtml());
  }
  
  if (this.caption != null) {
   element.append(this.caption.toDOMElement().outerHtml());
  }
  if (this.geoTag != null) {
   element.append(this.geoTag.toDOMElement().outerHtml());
  }
  if (this.audio != null) {
   element.append(this.audio.toDOMElement().outerHtml());
  }
  
  return element;
 }
 
 /**
  * Gets the caption.
  *
  * @return the caption
  */
 public Caption getCaption() {
  return caption;
 }
 
 /**
  * With caption.
  *
  * @param caption the caption
  * @return the image
  */
 public Image withCaption(Caption caption) {
  this.caption = caption;
  return this;
 }
 
 /**
  * Gets the url.
  *
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * With url.
  *
  * @param url the url
  * @return the image
  */
 public Image withUrl(String url) {
  this.url = StringUtils.substringBefore(url, ".ulenscale");
  return this;
 }
 
 /**
  * Gets the lazy load url.
  *
  * @return the lazy load url
  */
 public String getLazyLoadUrl() {
  return lazyLoadURL;
 }
 
 /**
  * With lazy load url.
  *
  * @param lazyLoadUrl the lazy load url
  * @return the image
  */
 public Image withLazyLoadUrl(String lazyLoadUrl) {
  this.lazyLoadURL = StringUtils.substringBefore(lazyLoadUrl, ".ulenscale");
  return this;
 }
 
 /**
  * Makes like enabled for this image.
  *
  * @return the image
  */
 public Image enableLike() {
  this.isLikeEnabled = true;
  return this;
 }
 
 /**
  * Makes like disabled for this image.
  *
  * @return the image
  */
 public Image disableLike() {
  this.isLikeEnabled = false;
  return this;
 }
 
 /**
  * Makes comments enabled for this image.
  *
  * @return the image
  */
 public Image enableComments() {
  this.isCommentsEnabled = true;
  return this;
 }
 
 /**
  * Makes comments disabled for this image.
  *
  * @return the image
  */
 public Image disableComments() {
  this.isCommentsEnabled = false;
  return this;
 }
 
 /**
  * Gets the presentation.
  *
  * @return the presentation
  */
 public String getPresentation() {
  return presentation;
 }
 
 /**
  * With presentation.
  *
  * @param presentation the presentation
  * @return the image
  */
 public Image withPresentation(String presentation) {
  this.presentation = presentation;
  return this;
 }
 
 /**
  * Gets the geo tag.
  *
  * @return the geo tag
  */
 public GeoTag getGeoTag() {
  return geoTag;
 }
 
 /**
  * With geo tag.
  *
  * @param geoTag the geo tag
  * @return the image
  */
 public Image withGeoTag(GeoTag geoTag) {
  this.geoTag = geoTag;
  return this;
 }
 
 /**
  * With geo tag.
  *
  * @param geoTag the geo tag
  * @return the image
  */
 public Image withGeoTag(String geoTag) {
  this.geoTag = GeoTag.create().withScript(geoTag);
  return this;
 }
 
 /**
  * Gets the audio.
  *
  * @return the audio
  */
 public Audio getAudio() {
  return audio;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Audible#withAudio(com.unilever.platform.aem.foundation.fbinstantarticle.elements.Audio)
  */
 public Image withAudio(Audio audio) {
  this.audio = audio;
  return this;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#isValid()
  */
 public boolean isValid() {
  return StringUtils.isNotBlank(this.url) || StringUtils.isNotBlank(this.lazyLoadURL) ? true : false;
 }
 
}
