/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The Class H2.
 */
public class H2 extends TextContainer {
 
 /**
  * The text alignment.
  *
  * @var string text align. Values: "op-left"|"op-center"|"op-right"
  */
 private String textAlignment;
 
 /**
  * The position.
  *
  * @var string text position. Values: "op-vertical-below"|"op-vertical-above"|"op-vertical-center"
  */
 private String position;
 
 /**
  * With text alignment.
  *
  * @param textAlignment
  *         the text alignment
  * @return the element
  */
 public InstantArticleElement withTextAlignment(String textAlignment) {
  this.textAlignment = textAlignment;
  return this;
  
 }
 
 /**
  * With position.
  *
  * @param position
  *         the position
  * @return the element
  */
 public InstantArticleElement withPosition(String position) {
  this.position = position;
  return this;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement()
  */
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element h2 = document.createElement("h2");
  List<String> classesList = new ArrayList<String>();
  if (StringUtils.isNotBlank(this.position)) {
   classesList.add(this.position);
  }
  if (StringUtils.isNotBlank(this.textAlignment)) {
   classesList.add(this.textAlignment);
  }
  if (CollectionUtils.isNotEmpty(classesList)) {
   Iterator<String> itr = classesList.iterator();
   String classs = "";
   while (itr.hasNext()) {
    classs = classs + itr.next() + " ";
   }
   h2.attr("class", StringUtils.join(classesList.toArray(), " "));
  }
  
  h2.text(textToDOMDocumentFragment(document));
  return h2;
 }
 
 /**
  * Creates the.
  *
  * @return the h2
  */
 public static H2 create() {
  return new H2();
 }
 
 /**
  * Gets the text alignment.
  *
  * @return the text alignment
  */
 public String getTextAlignment() {
  return textAlignment;
 }
 
 /**
  * Gets the position.
  *
  * @return the position
  */
 public String getPosition() {
  return position;
 }
 
}
