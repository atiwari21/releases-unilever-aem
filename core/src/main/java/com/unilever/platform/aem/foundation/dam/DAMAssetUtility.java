/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.core.dto.AssociatedAssetDTO;
import com.unilever.platform.aem.foundation.core.dto.ImageDTO;

/**
 * The Class DAMAssetUtility.
 */
public final class DAMAssetUtility {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(DAMAssetUtility.class);
 
 /** Constant for Copy of Featured Instagram. */
 private static final String COPY = "copy";
 
 /** Constant for author of Featured Instagram. */
 private static final String AUTHOR_NAME = "authorName";
 
 /** Constant for approved Date of Featured Instagram. */
 private static final String APPROVED_DATE = "approvedDate";
 
 /** Constant for image Featured Instagram. */
 private static final String IMAGE = "image";
 
 /** The Constant SOCIAL_CHANNEL_NAME. */
 private static final String SOCIAL_CHANNEL_NAME = "socialChannelName";
 
 /** The Constant POST_URL. */
 private static final String POST_URL = "postUrl";
 
 private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
 
 /**
  * Instantiates a new DAM asset utility.
  */
 private DAMAssetUtility() {
  LOGGER.info("DAM Asset Utility Constructor Called.");
 }
 
 /**
  * Gets the image metadata map from asset.
  *
  * @param asset
  *         the asset
  * @param page
  *         the page
  * @param resourceResolver
  *         the resource resolver
  * @param slingRequest
  *         the sling request
  * @return the image metadata map from asset
  */
 public static Map<String, Object> getImageMetadataMapFromAsset(Asset asset, Page page, ResourceResolver resourceResolver,
   SlingHttpServletRequest slingRequest) {
  Map<String, Object> imgMetaMap = new LinkedHashMap<String, Object>();
  
  if (asset != null) {
   AssociatedAssetDTO rlAsset = getAssetData(asset, resourceResolver);
   imgMetaMap.put(COPY, rlAsset.getTweetCopy());
   imgMetaMap.put(AUTHOR_NAME, rlAsset.getAuthorName());
   imgMetaMap.put(APPROVED_DATE, rlAsset.getApprovedDate());
   imgMetaMap.put(SOCIAL_CHANNEL_NAME, rlAsset.getSocialChannelName());
   imgMetaMap.put(POST_URL, rlAsset.getPostUrl());
   imgMetaMap.put(IMAGE, getImageMap(asset.getPath(), page, slingRequest));
  }
  return imgMetaMap;
 }
 
 /**
  * Gets the asset data.
  *
  * @param asset
  *         the asset
  * @param resourceResolver
  *         the resource resolver
  * @return the asset data
  */
 public static AssociatedAssetDTO getAssetData(Asset asset, ResourceResolver resourceResolver) {
  AssociatedAssetDTO relatedAsset = new AssociatedAssetDTO();
  if (asset != null) {
   Resource resource = resourceResolver.getResource(asset.getPath() + "/jcr:content/metadata");
   relatedAsset.setTweetCopy(getValueFromMetadata("longDescription", resource));
   relatedAsset.setApprovedDate(getValueFromMetadata("latestEffectiveDate", resource));
   relatedAsset.setAuthorName(getValueFromMetadata("originatorEmail", resource));
   relatedAsset.setSocialChannelName(getValueFromMetadata("originatorName", resource));
   relatedAsset.setPostUrl(getValueFromMetadata("wWWInitialAddress", resource));
  }
  return relatedAsset;
 }
 
 /**
  * Gets the image map.
  *
  * @param imagePath
  *         the image path
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the image map
  */
 public static Map<String, Object> getImageMap(String imagePath, Page page, SlingHttpServletRequest slingRequest) {
  ImageDTO image = new ImageDTO(imagePath, "", "", page, slingRequest);
  return image.convertToMap();
 }
 
 /**
  * Gets the value from metadata.
  *
  * @param property
  *         the property
  * @param resource
  *         the resource
  * @return the value from metadata
  */
 private static String getValueFromMetadata(String property, Resource resource) {
  String value = StringUtils.EMPTY;
  if (resource != null && resource.getValueMap().containsKey(property)) {
   Object val = resource.getValueMap().get(property);
   if (val instanceof GregorianCalendar) {
    GregorianCalendar startDate = (GregorianCalendar) val;
    value = getEffectiveDateFromCalendar(startDate);
   } else {
    value = val.toString();
   }
  }
  return value;
 }
 
 /**
  * Gets effective date from calendar
  * 
  * @param date
  *         the date
  * @return the date
  */
 public static String getEffectiveDateFromCalendar(GregorianCalendar date) {
  String value = StringUtils.EMPTY;
  if (date != null) {
   value = DATE_FORMAT.format(date.getTime());
  }
  return value;
 }
 
 /**
  * Gets the assets map.
  *
  * @param assets
  *         the assets
  * @return the assets map
  */
 public static Map<String, Object> getAssetsMap(List<Asset> assets) {
  
  List<Map<String, Object>> assetMap = new ArrayList<Map<String, Object>>();
  Map<String, Object> map = new LinkedHashMap<>();
  
  for (Asset asset : assets) {
   assetMap.add(getAssetMap(asset));
  }
  map.put("assets", assetMap);
  return map;
 }
 
 /**
  * Gets the asset map.
  *
  * @param asset
  *         the asset
  * @return the asset map
  */
 public static Map<String, Object> getAssetMap(Asset asset) {
  
  Map<String, Object> assetMap = new LinkedHashMap<String, Object>();
  
  if (asset != null) {
   assetMap.put("path", asset.getPath());
   assetMap.put("tweetcopy", "tweet copy");
   assetMap.put("authorname", "author name");
   assetMap.put("approvedate", "author date");
  }
  
  return assetMap;
 }
}
