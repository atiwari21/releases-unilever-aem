/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Value;
import javax.servlet.ServletException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.AgentIdFilter;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.utils.PreviewProductionAgentsUtility;

/**
 * This class sets the lock and unlock to product catalogs based on catalog selection handled for single and multiple selection catalogs.
 */
@SlingServlet(paths = { "/bin/unpublish/preview" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UnPublishFromPreviewServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the preview agent to be enabled ", propertyPrivate = false) })
public class UnPublishFromPreviewServlet extends SlingAllMethodsServlet {
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant ZERO. */
 private static final int ZERO = 0;
 
 /** The Constant ONE. */
 private static final int ONE = 1;
 
 /** The Constant PATHS. */
 private static final String PATHS = "paths";
 
 /** The Constant LOCK. */
 private static final String PREVIEW = "binunpublishpreview";
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -782708002977083761L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PublishToPreviewServlet.class);
 @Reference
 ConfigurationService configurationService;
 @Reference
 GlobalConfiguration globalConfiguration;
 @Reference
 Replicator replicator;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  StringBuilder requestObjectBuilder = new StringBuilder();
  String line = null;
  try {
   BufferedReader reader = request.getReader();
   while ((line = reader.readLine()) != null) {
    requestObjectBuilder.append(line);
   }
  } catch (Exception e) {
   LOGGER.error("Error while reading request", e);
  }
  String requestObjectString = requestObjectBuilder.toString();
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  if (StringUtils.isNotBlank(requestObjectString)) {
   requestObjectString = getValueWithoutQuote(requestObjectString, requestObjectString.length(), ONE, ONE);
   String[] keyValuePairs = requestObjectString.split(PREVIEW);
   Map<String, Object> map = getMapWithKeyValue(keyValuePairs);
   String productCatalogPaths = MapUtils.getString(map, PATHS);
   String productCatalogPathsWithoutArray = getValueWithoutQuote(productCatalogPaths, productCatalogPaths.length(), ONE, ONE);
   String[] splitPathsByComma = productCatalogPathsWithoutArray.split(CommonConstants.COMMA);
   setReplicatedToPreviewData(splitPathsByComma, resourceResolver);
  }
  if (resourceResolver != null) {
   resourceResolver.commit();
  }
 }
 
 /**
  * Sets the post status data.
  * 
  * @param writer
  *         the writer
  * @param splitPathsByComma
  *         the split paths by comma
  * @param lockProperty
  *         the lock property
  * @param resourceResolver
  *         the resource resolver
  */
 private void setReplicatedToPreviewData(String[] splitPathsByComma, ResourceResolver resourceResolver) {
  String[] previewAgents = PreviewProductionAgentsUtility.getPreviewAgents(globalConfiguration);
  if (previewAgents.length > 0) {
   ReplicationOptions replicationOptions = new ReplicationOptions();
   AgentIdFilter agentIdFilter = new AgentIdFilter(previewAgents);
   replicationOptions.setFilter(agentIdFilter);
   for (String pagePath : splitPathsByComma) {
    String pagePathWithoutQuote = getValueWithoutQuote(pagePath, pagePath.length(), ONE, ONE);
    LOGGER.debug("page path", pagePathWithoutQuote);
    Resource resource = resourceResolver.getResource(pagePathWithoutQuote + "/jcr:content");
    Node catalogNode = (resource != null) ? resource.adaptTo(Node.class) : null;
    try {
     if (catalogNode != null) {
      replicator.replicate(catalogNode.getSession(), ReplicationActionType.DEACTIVATE, pagePathWithoutQuote, replicationOptions);
      catalogNode.setProperty("lastReplicatedToPreview", (Value) null);
      catalogNode.setProperty("lastReplicatedToPreviewBy", (Value) null);
      resourceResolver.commit();
     }
    } catch (Exception e) {
     LOGGER.error("Error while getting resource node", e);
    }
   }
  }
 }
 
 /**
  * Gets the value without quote.
  * 
  * @param value
  *         the value
  * @param length
  *         the length
  * @param startIndex
  *         the start index
  * @param endIndex
  *         the end index
  * @return the value without quote
  */
 private String getValueWithoutQuote(String value, int length, int withOutQuoteIndexFromStart, int withOutQuoteIndexFromLast) {
  String updatedValue = StringUtils.EMPTY;
  if (length > TWO) {
   updatedValue = value.substring(withOutQuoteIndexFromStart, length - withOutQuoteIndexFromLast);
  }
  return updatedValue;
 }
 
 /**
  * Gets the map with key value.
  * 
  * @param keyValuePairs
  *         the key value pairs
  * @return the map with key value
  */
 private Map<String, Object> getMapWithKeyValue(String[] keyValuePairs) {
  int pathCount = ZERO;
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  for (String pair : keyValuePairs) {
   String[] entry = pair.split(":");
   String key = entry[ZERO].trim().length() > ONE ? entry[ZERO].trim() : PREVIEW;
   key = (pathCount == ZERO) ? getValueWithoutQuote(key, key.length(), ONE, ONE) : key;
   String keyVal = entry[ONE].trim().split(CommonConstants.COMMA).length > ONE
     ? getValueWithoutQuote(entry[ONE].trim(), entry[ONE].trim().length(), ZERO, TWO) : entry[ONE].trim();
   map.put(key, keyVal);
   pathCount++;
  }
  return map;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
}
