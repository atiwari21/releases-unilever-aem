/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Descriptions.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "description" })
public class Descriptions {
 
 /** The description. */
 @XmlElement(name = "Description")
 private List<Description> description;
 
 /**
  * Gets the description.
  *
  * @return the description
  */
 public List<Description> getDescription() {
  if (description == null) {
   description = new ArrayList<Description>();
  }
  return this.description;
 }
 
 /**
  * Sets the description.
  *
  * @param description
  *         the new description
  */
 public void setDescription(List<Description> description) {
  this.description = description;
 }
 
}
