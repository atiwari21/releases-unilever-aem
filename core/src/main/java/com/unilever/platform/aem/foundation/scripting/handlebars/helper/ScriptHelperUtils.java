/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Options;

/**
 * Utility Helper class for handlebar script helpers.
 * 
 * @author nbhart
 * 
 */
public final class ScriptHelperUtils {
 
 /**
  * , Logger Instance.
  */
 private static final Logger LOG = LoggerFactory.getLogger(ScriptHelperUtils.class);
 
 /**
  * private constructor.
  */
 private ScriptHelperUtils() {
 }
 
 /**
  * Adds the context parameters to the context data. These arguments can be accessed via "args" key.
  * 
  * @param dataMap
  * @param options
  * @return
  */
 public static Map<String, Object> addParamsToContextMap(final Map<String, Object> dataMap, final Options options) {
  int paramCount = 1;
  Map<String, Object> contextData = dataMap;
  List<Object> list = new ArrayList<Object>();
  try {
   while (options.params.length > paramCount) {
    if (options.params[paramCount] instanceof Map) {
     Map<String, Object> map = new HashMap<String, Object>();
     map.putAll((Map<? extends String, ? extends Object>) options.params[paramCount]);
     
     if (map != null) {
      list.add(map);
     }
    } else {
     list.add(options.params[paramCount]);
    }
    paramCount++;
   }
   contextData.put("args", list.toArray());
  } catch (Exception e) {
   LOG.error("Error Encountered while add params to context map" + ExceptionUtils.getFullStackTrace(e));
  }
  return contextData;
 }
}
