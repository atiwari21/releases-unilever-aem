/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.tags;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class holds methods which provides the release information. This is typically meant to be used in JSP via tag library.
 * 
 * @author nbhart
 *
 */
public final class ReleaseDetails {
 
 /** The Constant RELEASE_PROPERTIES_FILE. */
 private static final String RELEASE_PROPERTIES_FILE = "releasedetails.properties";
 /**
  * Holds the instance of {@link Logger} for logging information.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(ReleaseDetails.class);
 
 /**
  * Instantiates a new release details.
  */
 private ReleaseDetails() {
  
 }
 
 /**
  * Gets the release message.
  *
  * @return the release message
  */
 public static String getReleaseMessage() {
  String environment = StringUtils.EMPTY;
  Properties prop = new Properties();
  try (InputStream input = ReleaseDetails.class.getClassLoader().getResourceAsStream(RELEASE_PROPERTIES_FILE)) {
   prop.load(input);
   environment = prop.getProperty("versiondetails");
  } catch (IOException e) {
   LOGGER.error("Error ingetting Release version", e.getMessage());
   LOGGER.debug("Error ingetting Release version", e);
  }
  return environment;
 }
}
