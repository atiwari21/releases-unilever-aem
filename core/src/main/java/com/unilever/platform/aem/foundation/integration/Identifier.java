/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import org.apache.commons.lang.StringUtils;

/**
 * The Class Identifier.
 */
public class Identifier {
 
 /** The identifier type. */
 protected String identifierType = StringUtils.EMPTY;
 
 /** The identifiervalue. */
 protected String identifierValue = StringUtils.EMPTY;
 
 /**
  * Gets the identifier type.
  *
  * @return the identifier type
  */
 public String getIdentifierType() {
  return identifierType;
 }
 
 /**
  * Sets the identifier type.
  *
  * @param identifierType
  *         the new identifier type
  */
 public void setIdentifierType(String identifierType) {
  if (StringUtils.isNotBlank(identifierType)) {
   this.identifierType = identifierType;
  }
 }
 
 /**
  * Gets the identifiervalue.
  *
  * @return the identifiervalue
  */
 public String getIdentifierValue() {
  return identifierValue;
 }
 
 /**
  * Sets the identifierValue.
  *
  * @param identifiervalue
  *         the new identifierValue
  */
 public void setIdentifierValue(String identifierValue) {
  if (StringUtils.isNotBlank(identifierValue)) {
   this.identifierValue = identifierValue;
  }
 }
 
}
