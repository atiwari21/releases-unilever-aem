/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The Class Blockquote.
 */
/**
 * Each blockquote of article should be an instance of this class.
 *
 * Example: <blockquote> This is the first blockquote of body text. </blockquote>
 *
 * or
 *
 * <blockquote> This is the <i>second</i> blockquote of <b>body text</b>. </blockquote>
 *
 * @see {link:https://developers.intern.facebook.com/docs/instant-articles/reference/blockquote}
 */
public class Blockquote extends TextContainer {
 
 /** The text. */
 private String text;
 
 /**
  * Instantiates a new blockquote.
  */
 private Blockquote() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static Blockquote create() {
  Blockquote h1 = new Blockquote();
  return h1;
 }
 
 /**
  * Gets the text.
  *
  * @return the text
  */
 public String getText() {
  return text;
 }
 
 /**
  * Sets the unescaped text within the blockquote.
  *
  * @param text
  *         the text
  * @return this
  */
 public Blockquote withText(String text) {
  this.text = text;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement()
  */
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element blockquote = document.createElement("blockquote");
  blockquote.append(textToDOMDocumentFragment(document));
  return blockquote;
  
 }
 
}