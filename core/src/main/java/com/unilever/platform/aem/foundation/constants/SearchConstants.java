/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.constants;

import com.day.cq.commons.jcr.JcrConstants;

/**
 * The Class SearchConstants.
 */
public final class SearchConstants {
 
 /** The Constant SEARCH_TITLE. */
 public static final String SEARCH_TITLE = JcrConstants.JCR_TITLE;
 
 /** The Constant TAGID_OR. */
 public static final String TAGID_OR = "tagid.or";
 
 /** The Constant SEARCH_RELATED. */
 public static final String SEARCH_RELATED = "related:";
 
 /** The Constant SLASH_CONTENT. */
 public static final String SLASH_CONTENT = "/content";
 
 /** The Constant OFFSET. */
 public static final String OFFSET = "p.offset";
 
 /** The Constant LIMIT. */
 public static final String LIMIT = "p.limit";
 
 /** The Constant SEARCH_FULLTEXT. */
 public static final String SEARCH_FULLTEXT = "_fulltext";
 
 /** The Constant SEARCH_SIMILAR. */
 public static final String SEARCH_SIMILAR = "similar";
 
 /** The Constant SEARCH_GROUP. */
 public static final String SEARCH_GROUP = "group.";
 
 /** The Constant SEARCH_GROUP_P_OR. */
 public static final String SEARCH_GROUP_P_OR = "group.p.or";
 
 /** The Constant SEARCH_FULLTEXT_RELPATH. */
 public static final String SEARCH_FULLTEXT_RELPATH = "_fulltext.relPath";
 
 /** The Constant SEARCH_TYPE. */
 public static final String SEARCH_TYPE = "type";
 
 /** The Constant SEARCH_TRUE. */
 public static final String SEARCH_TRUE = "true";
 
 /** The Constant SEARCH_OR. */
 public static final String SEARCH_OR = "or";
 
 /** The Constant SEARCH_AND. */
 public static final String SEARCH_AND = "and";
 
 /** The Constant SEARCH_PATH. */
 public static final String SEARCH_PATH = "path";
 
 /** The Constant TAG_ID. */
 public static final String TAG_ID = "tagid";
 
 /** The Constant TAG_ID_PROPERTY. */
 public static final String TAG_ID_PROPERTY = "tagid.property";
 
 /** The Constant SEARCH_STRING_DOT. */
 public static final String SEARCH_STRING_DOT = ".";
 
 /** The Constant SEARCH_PROPERTY. */
 public static final String SEARCH_PROPERTY = "property";
 
 /** The Constant SEARCH_UNDERSCORE_PROPERTY. */
 public static final String SEARCH_UNDERSCORE_PROPERTY = "_property";
 
 /** The Constant SEARCH_RELPATH. */
 public static final String SEARCH_RELPATH = "relPath";
 
 /** The Constant SEARCH_VALUE_FULLTEXT. */
 public static final String SEARCH_VALUE_FULLTEXT = "fulltext";
 
 /** The Constant SEARCH_XPATH. */
 public static final String SEARCH_XPATH = "xpath";
 
 /** The Constant SEARCH_KEYWORD. */
 public static final String SEARCH_KEYWORD = "q";
 
 /** The Constant SEARCH_TAG. */
 public static final String SEARCH_TAG = "tag";
 
 /** The Constant SEARCH_FROM. */
 public static final String SEARCH_FROM = "from";
 
 /** The Constant SEARCH_TO. */
 public static final String SEARCH_TO = "to";
 
 /** The Constant SEARCH_START. */
 public static final String SEARCH_START = "start";
 
 /** The Constant SEARCH_STRING_FORWARD_SLASH. */
 public static final String SEARCH_STRING_FORWARD_SLASH = "/";
 
 /** The Constant SEARCH_STRING_AT_THE_RATE. */
 public static final String SEARCH_STRING_AT_THE_RATE = "@";
 
 /** The Constant SEARCH_QUESTIONMARK. */
 public static final String SEARCH_QUESTIONMARK = "?";
 
 /** The Constant SEARCH_AMPERSAND. */
 public static final String SEARCH_AMPERSAND = "&";
 
 /** The Constant SEARCH_EQUAL. */
 public static final String SEARCH_EQUAL = "=";
 
 /** The Constant SEARCH_TAGS. */
 public static final String SEARCH_TAGS = "tags";
 
 /** The Constant SEARCH_VALUE. */
 public static final String SEARCH_VALUE = "value";
 
 /** The Constant SEARCH_LASTMOD. */
 public static final String SEARCH_LASTMOD = "lastModified";
 
 /** The Constant SEARCH_DATERANGE. */
 public static final String SEARCH_DATERANGE = "daterange";
 
 /** The Constant SEARCH_TAGPATH. */
 public static final String SEARCH_TAGPATH = "jcr:content/cq:tags";
 
 /** The Constant SEARCH_ORDERBY. */
 public static final String SEARCH_ORDERBY = "orderby";
 
 /** The Constant SEARCH_SORT. */
 public static final String SEARCH_SORT = "sort";
 
 /** The Constant SEARCH_LANGUAGE. */
 public static final String SEARCH_LANGUAGE = "language";
 
 /** The Constant SEARCH_LANGUAGES. */
 public static final String SEARCH_LANGUAGES = "languages";
 
 /** The Constant SEARCH_OPERATION. */
 public static final String SEARCH_OPERATION = "operation";
 
 /** The Constant SEARCH_OPERATION_LIKE. */
 public static final String SEARCH_OPERATION_LIKE = "like";
 
 /** The Constant SEARCH_OPERATION_EXISTS. */
 public static final String SEARCH_OPERATION_EXISTS = "exists";
 
 /** The Constant SEARCH_OPERATION_CONTAINS. */
 public static final String SEARCH_OPERATION_CONTAINS = "contains";
 
 /** The Constant SEARCH_EXT. */
 public static final String SEARCH_EXT = ".html";
 
 /** The Constant SEARCH_PROPERTY_47. */
 public static final int SEARCH_PROPERTY_47 = 47;
 
 /** The Constant SORTPROPERTY_SCORE. */
 public static final String SORTPROPERTY_SCORE = "@jcr:score";
 
 /** The Constant SORTPROPERTY_CREATED. */
 public static final String SORTPROPERTY_CREATED = "@jcr:created";
 
 /** The Constant FEATURED_YES. */
 public static final String FEATURED_YES = "yes";
 
 /** The Constant SORTPROPERTY_RELEVANCE. */
 public static final String SORTPROPERTY_RELEVANCE = "relevance";
 
 /** The Constant SEARCH_START_0. */
 public static final long SEARCH_START_0 = 0;
 
 /** The Constant SEARCH_RESULTS_PER_PAGE. */
 public static final long SEARCH_RESULTS_PER_PAGE = 10;
 
 public static final String SLING_SERVLET_RESOURCETYPE = "sling.servlet.resourceTypes";
 
 public static final String SLING_SERVLET_SELECTOR = "sling.servlet.selectors";
 
 public static final String SLING_SERVLET_EXTENSION = "sling.servlet.extensions";
 
 public static final String SLING_SERVLET_RESOURCETYPE_VALUE = "sling/servlet/default";
 /** The Constant DOWNLOAD_SEARCH_RESULTS_PER_PAGE. */
 public static final long DOWNLOAD_SEARCH_RESULTS_PER_PAGE = 20;
 
 /** The Constant SEARCH_TITLEPATH. */
 public static final String SEARCH_TITLEPATH = "jcr:content/*/*/sling:resourceType";
 
 /** The Constant SEARCH_RESTYPE. */
 public static final String SEARCH_RESTYPE = "help/components/title";
 
 /** The Constant PROPERTY_RESULTS_PER_PAGE. */
 public static final String PROPERTY_RESULTS_PER_PAGE = "./resultsPerPage";
 
 /** The Constant SEARCH_PERCENTSYMBOL. */
 public static final String SEARCH_PERCENTSYMBOL = "%";
 
 /** The Constant SEARCH_UNDERSCORE. */
 public static final String SEARCH_UNDERSCORE = "_";
 
 /** The Constant SEARCH_PROPERTIES. */
 public static final String SEARCH_PROPERTIES = "properties";
 
 /** The Constant TAG_PATH_CONTENT. */
 public static final String TAG_PATH_CONTENT = "jcr:content/cq:tags";
 
 /** The Constant SORT_CRITERIA_NODE_PROPERTY_CONTENT_TITLE. */
 public static final String SORT_CRITERIA_NODE_PROPERTY_CONTENT_TITLE = "@jcr:content/jcr:title";
 
 /** The Constant NODE_PROPERTY_CONTENT_TITLE. */
 public static final String NODE_PROPERTY_CONTENT_TITLE = "jcr:content/jcr:title";
 
 /** The Constant SEARCH_REQUEST_PARAMETER_KEYWORD. */
 public static final String SEARCH_REQUEST_PARAMETER_KEYWORD = "keyword";
 
 /** The Constant PREDICTIVE_SEARCH_MAX_NUMBER_OF_SUGGESTIONS. */
 public static final int PREDICTIVE_SEARCH_MAX_NUMBER_OF_SUGGESTIONS = 5;
 
 /** The Constant SEARCH_REGEX_ASTERIX. */
 public static final String SEARCH_REGEX_ASTERIX = "*";
 
 /** The Constant EXTERNAL_LINK_URL. */
 public static final String EXTERNAL_LINK_URL = "urlLink";
 
 /** The Constant TAGS. */
 public static final String TAGS = "tags";
 
 /** The Constant TAG. */
 public static final String TAG = "tag";
 
 /** The Constant PARENT_TAG. */
 public static final String PARENT_TAG = "parentTag";
 
 /** The Constant PARENT_TAGS. */
 public static final String PARENT_TAGS = "parentTags";
 
 /** The Constant PERCENTAGE. */
 public static final String PERCENTAGE = "%";
 
 /** The Constant FILTER_FLAG_PRESENT. */
 public static final String FILTER_FLAG_PRESENT = "true";
 
 /** The Constant PUBLISHDATE. */
 public static final String PUBLISHDATE = "publishDate";
 /** The Constant RANGELANDINGTEMPLATE. */
 public static final String RANGELANDINGTEMPLATE = "dove/pagecomponents/productRangeLandingPage";
 
 /** The Constant CONTENTTYPE. */
 public static final String CONTENTTYPE = "contentType";
 /** The Constant RANGESHORTCOPY. */
 public static final String RANGESHORTCOPY = "rangeShortCopy";
 /** The Constant RANGEIMAGE. */
 public static final String RANGEIMAGE = "rangeImage";
 /** The Constant RANGENAME. */
 public static final String RANGENAME = "rangeName";
 /** The Constant CONTENTHTML. */
 public static final String CONTENTHTML = ".content.html";
 /** The Constant WCMMODE. */
 public static final String WCMMODE = "?wcmmode=disabled";
 /** The Constant IMAGEURL. */
 public static final String IMAGEURL = "imageUrl";
 /** The Constant HERO. */
 public static final String HERO = "hero";
 /** The Constant ALTIMAGE. */
 public static final String ALTIMAGE = "altImage";
 /** The Constant IMAGE. */
 public static final String IMAGE = "image";
 /** The Constant LOCALECOLON. */
 public static final String LOCALECOLON = "Locale:";
 /** The Constant BRANDNAMECOLON. */
 public static final String BRANDNAMECOLON = "BrandName:";
 /** The Constant TOTALRESULTS. */
 public static final String TOTALRESULTS = "totalResults";
 /** The Constant RESPONSE. */
 public static final String RESPONSE = "response";
 /** The Constant NORESULT. */
 public static final String NORESULT = "noresult";
 /** The Constant SUGGESTIONS. */
 public static final String SUGGESTIONS = "suggestions";
 /** The Constant FACET. */
 public static final String FACET = "Facet";
 /** The Constant LABEL. */
 public static final String LABEL = "label";
 /** The Constant HIGHLIGHTING. */
 public static final String HIGHLIGHTING = "Highlighting";
 /** The Constant SUBCONTENTTYPE. */
 public static final String SUBCONTENTTYPE = "subContentType";
 
 public static final String ISO_8859_1 = "iso-8859-1";
 
 /** The Constant UTF-8. */
 public static final String UTF_8 = "UTF-8";
 
 /**
  * Instantiates a new search constants.
  */
 private SearchConstants() {
 }
}
