/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api;

import com.adobe.cq.commerce.api.CommerceSession;

/**
 * Brand specific contract for commerce implementation. Right now we don't have any e-commerce implementation that why we leave this as empty
 *
 */
public interface UNICommerceSession extends CommerceSession {
 
}
