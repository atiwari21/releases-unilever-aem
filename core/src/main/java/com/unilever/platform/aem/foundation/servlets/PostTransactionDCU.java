/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.helper.URLConnectionHelper;
import com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService;

/**
 * The Class PostTransactionDCU.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { PostTransactionDCU.SELECTOR }, methods = {
  HttpConstants.METHOD_POST })
@Properties(value = { @Property(name = Constants.SERVICE_DESCRIPTION, value = "Util to post transaction to DCS", propertyPrivate = false),
  @Property(name = "sling.auth.requirements", value = "/system/sling/login")

})
public class PostTransactionDCU extends SlingAllMethodsServlet {
 
 /**
  * serial version number.
  */
 private static final long serialVersionUID = 1L;
 
 /**
  * Constant for "ulecscale" selector.
  */
 public static final String SELECTOR = "dcstransactionpost";
 
 /**
  * ConfigurationService service reference.
  */
 @Reference
 ConfigurationService configurationService;
 @Reference
 EGiftingConnectionService eGiftingConnectionService;
 @Reference
 HttpClientBuilderFactory factory;
 /**
  * Identifier to hold value of logger.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(PostTransactionDCU.class);
 
 private static final String ERROR_JSON_STR = "{\"errors\":[{\"error\":\"Error in connection\"}]} ";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  ServletOutputStream sout = slingResponse.getOutputStream();
  boolean flag = false;
  try {
   String url = eGiftingConnectionService.getPostEntityURL();
   LOGGER.debug("EGifting Transaction Service Post entity URL: {}", url);
   int timeout = eGiftingConnectionService.getTimeoutTime();
   HttpResponse response = URLConnectionHelper.getResponseFromPayloadPost(slingRequest, url, factory, timeout);
   if (response != null) {
    int responseCode = response.getStatusLine().getStatusCode();
    LOGGER.debug("Response Code : " + responseCode);
    
    HttpEntity responseEntity = response.getEntity();
    InputStream is = null;
    if (responseEntity != null) {
     is = responseEntity.getContent();
     LOGGER.debug("Response data : " + responseEntity.getContent());
    }
    
    slingResponse.setStatus(responseCode);
    slingResponse.setContentType("application/json");
    BufferedReader in = new BufferedReader(new InputStreamReader(is));
    String inputLine;
    while ((inputLine = in.readLine()) != null) {
     LOGGER.debug("Response line : " + inputLine);
     sout.write(inputLine.getBytes());
    }
    LOGGER.debug("Response outstream : " + sout.toString());
    in.close();
   } else {
    LOGGER.debug("Response got is null ");
    flag = true;
   }
  } catch (Exception e) {
   flag = true;
   LOGGER.error("Error in post transaction servlet ", e);
  } finally {
   if (flag) {
    sout.write(ERROR_JSON_STR.getBytes());
   }
   sout.flush();
  }
  
 }
}
