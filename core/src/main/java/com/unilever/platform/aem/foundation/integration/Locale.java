/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

/**
 * The Class Locale.
 */
public class Locale {
 
 /** The localename. */
 protected String localeName;
 
 /** The brand. */
 protected BrandType brand;
 
 /** The categories. */
 protected CategoriesType categories;
 
 /** The products. */
 protected ProductsType products;
 
 /**
  * Gets the localename.
  *
  * @return the localename
  */
 public String getLocaleName() {
  return localeName;
 }
 
 /**
  * Sets the localename.
  *
  * @param localename
  *         the new localename
  */
 public void setLocalename(String localeName) {
  this.localeName = localeName;
 }
 
 /**
  * Gets the value of the categories property.
  * 
  * @return possible object is {@link CategoriesType }
  * 
  */
 public CategoriesType getCategories() {
  return categories;
 }
 
 /**
  * Sets the value of the categories property.
  * 
  * @param value
  *         allowed object is {@link CategoriesType }
  * 
  */
 public void setCategories(CategoriesType value) {
  this.categories = value;
 }
 
 /**
  * Gets the value of the products property.
  * 
  * @return possible object is {@link ProductsType }
  * 
  */
 public ProductsType getProducts() {
  return products;
 }
 
 /**
  * Sets the value of the products property.
  * 
  * @param value
  *         allowed object is {@link ProductsType }
  * 
  */
 public void setProducts(ProductsType value) {
  this.products = value;
 }
 
 /**
  * Sets the brand.
  *
  * @param brand
  *         the new brand
  */
 public void setBrand(BrandType brand) {
  this.brand = brand;
 }
 
 /**
  * Gets the brand.
  *
  * @return the brand
  */
 public BrandType getBrand() {
  return brand;
 }
}
