/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Cite;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Pullquote;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

public class PullquoteCiteRule extends ConfigurationSelectorRule {

 /**
  * Instantiates a new pull Quote rule.
  */
 private PullquoteCiteRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor rule
  */
 public static PullquoteCiteRule create() {
  return new PullquoteCiteRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Cite cite = Cite.create();
  ((Pullquote) container).withAttribution(cite);
  transformer.transform(cite, node);
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Pullquote.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the audio rule
  */
 public static PullquoteCiteRule createFrom(JSONObject configuration) {
  PullquoteCiteRule pullquoteRule = create();
  pullquoteRule = (PullquoteCiteRule) pullquoteRule.withSelector(configuration.getString("selector"));
  return pullquoteRule;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
