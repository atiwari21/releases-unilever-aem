/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The Class Small.
 */
public class Small extends TextContainer {
 
 private Small() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static Small create() {
  return new Small();
 }
 
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element small = document.createElement("small");
  
  small.text(textToDOMDocumentFragment(document));
  return small;
 }
 
}
