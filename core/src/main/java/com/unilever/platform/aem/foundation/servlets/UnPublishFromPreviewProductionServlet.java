/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class sets the lock and unlock to product catalogs based on catalog selection handled for single and multiple selection catalogs.
 */
@SlingServlet(paths = { "/bin/unpublish/preview/production" }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UnPublishFromPreviewProductionServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the preview agent to be enabled ", propertyPrivate = false) })
public class UnPublishFromPreviewProductionServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -782708002977083743L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UnPublishFromPreviewProductionServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  Resource previewResource = (resourceResolver != null) ? resourceResolver.getResource("/etc/replication/agents.author/Publish-0101/jcr:content")
    : null;
  setPreviewAgentEnabled(previewResource, true);
  Resource productionResource = (resourceResolver != null) ? resourceResolver.getResource("/etc/replication/agents.author/Publish-0202/jcr:content")
    : null;
  setPreviewAgentEnabled(productionResource, true);
 }
 
 private void setPreviewAgentEnabled(Resource resource, boolean isEnabled) {
  Node previewResourceNode = (resource != null) ? resource.adaptTo(Node.class) : null;
  if (previewResourceNode != null) {
   try {
    previewResourceNode.setProperty("enabled", isEnabled);
    previewResourceNode.save();
   } catch (RepositoryException e) {
    LOGGER.error("Exception in setPreviewAgentEnabled:", e);
   }
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
}
