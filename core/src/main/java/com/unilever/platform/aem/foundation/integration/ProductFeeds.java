/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.List;

/**
 * The Class ProductFeeds.
 */
public class ProductFeeds {
 
 /** The locales. */
 protected List<Locale> locales;
 
 /** The status. */
 protected String status;
 
 /** The message. */
 protected String message;
 
 /**
  * Gets the locales.
  *
  * @return the locales
  */
 public List<Locale> getLocales() {
  return locales;
 }
 
 /**
  * Sets the locales.
  *
  * @param locales
  *         the new locales
  */
 public void setLocales(List<Locale> locales) {
  this.locales = locales;
 }
 
 /**
  * Gets the status.
  *
  * @return the status
  */
 public String getStatus() {
  return status;
 }
 
 /**
  * Sets the status.
  *
  * @param status
  *         the new status
  */
 public void setStatus(String status) {
  this.status = status;
 }
 
 /**
  * Gets the message.
  *
  * @return the message
  */
 public String getMessage() {
  return message;
 }
 
 /**
  * Sets the message.
  *
  * @param message
  *         the new message
  */
 public void setMessage(String message) {
  this.message = message;
 }
 
}
