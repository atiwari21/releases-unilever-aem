/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Feed.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "categories", "products" })
@XmlRootElement(name = "Feed")
public class Feed {
 
 /** The categories. */
 @XmlElement(name = "Categories", required = true)
 protected Categories categories;
 
 /** The products. */
 @XmlElement(name = "Products", required = true)
 protected Products products;
 
 /** The brand. */
 @XmlAttribute(name = "Brand", required = true)
 protected String brand;
 
 /** The source brand id. */
 @XmlAttribute(name = "SourceBrandId")
 protected String sourceBrandId;
 
 /** The incremental. */
 @XmlAttribute(name = "incremental")
 protected String incremental;
 
 /**
  * Gets the categories.
  *
  * @return the categories
  */
 public Categories getCategories() {
  return categories;
 }
 
 /**
  * Sets the categories.
  *
  * @param value
  *         the new categories
  */
 public void setCategories(Categories value) {
  this.categories = value;
 }
 
 /**
  * Gets the products.
  *
  * @return the products
  */
 public Products getProducts() {
  return products;
 }
 
 /**
  * Sets the products.
  *
  * @param value
  *         the new products
  */
 public void setProducts(Products value) {
  this.products = value;
 }
 
 /**
  * Gets the brand.
  *
  * @return the brand
  */
 public String getBrand() {
  return brand;
 }
 
 /**
  * Sets the brand.
  *
  * @param value
  *         the new brand
  */
 public void setBrand(String value) {
  this.brand = value;
 }
 
 /**
  * Gets the source brand id.
  *
  * @return the source brand id
  */
 public String getSourceBrandId() {
  return sourceBrandId;
 }
 
 /**
  * Sets the source brand id.
  *
  * @param value
  *         the new source brand id
  */
 public void setSourceBrandId(String value) {
  this.sourceBrandId = value;
 }
 
 /**
  * Gets the incremental.
  *
  * @return the incremental
  */
 public String getIncremental() {
  return incremental;
 }
 
 /**
  * Sets the incremental.
  *
  * @param value
  *         the new incremental
  */
 public void setIncremental(String value) {
  this.incremental = value;
 }
 
}
