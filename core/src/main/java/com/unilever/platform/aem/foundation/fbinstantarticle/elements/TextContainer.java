/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jsoup.nodes.Document;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * The Class TextContainer.
 */
public class TextContainer extends InstantArticleElement implements Container {
 
 /** The text children. */
 protected List<Object> textChildren;
 
 TextContainer() {
  textChildren = new LinkedList<Object>();
 }
 
 /**
  * Adds content to the formatted text.
  *
  * @param child
  *         the child
  * @return $this
  * @throws Exception
  */
 public TextContainer appendText(Object child) {
  this.textChildren.add(child);
  return this;
 }
 
 /**
  * Gets the text children.
  *
  * @return the text children
  */
 public List<Object> getTextChildren() {
  return textChildren;
 }
 
 /**
  * Text to dom document fragment.
  */
 public void textToDOMDocumentFragment() {
  textToDOMDocumentFragment(null);
 }
 
 /**
  * Text to dom document fragment.
  *
  * @param document
  *         the document
  * @return the document
  */
 public String textToDOMDocumentFragment(Document document) {
  StringBuilder docFragment = new StringBuilder();
  Iterator<Object> itr = textChildren.iterator();
  while (itr.hasNext()) {
   Object obj = itr.next();
   docFragment.append(obj);
  }
  return docFragment.toString();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  List<Object> children = new ArrayList<Object>();
  Iterator<Object> itr = children.iterator();
  while (itr.hasNext()) {
   Object obj = itr.next();
   if (obj instanceof TextContainer) {
    children.add(obj);
   }
  }
  return children;
 }
 
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 @Override
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  return null;
 }
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }
 
 @Override
 public boolean isValid() {
  return CollectionUtils.isEmpty(this.textChildren) ? false : true;
 }
}