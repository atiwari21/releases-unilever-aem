/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.jsoup.nodes.Document;

/**
 * The Class Time.
 */
public class Time extends InstantArticleElement {
 public static final String PUBLISHED = "op-published";
 
 public static final String MODIFIED = "op-modified";
 
 public static final String PRINT_DATE_FORMET = "EEE, d MMM yyyy HH:mm:ss Z";
 
 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
 SimpleDateFormat printDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
 
 private Date date;
 
 private String type;
 
 private Time() {
  
 }
 
 public static Time create(String type) {
  Time instance = new Time();
  instance.dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
  return instance.withType(type);
 }
 
 public Time withType(String type) {
  this.type = type;
  return this;
 }
 
 public Time withTime(Date date) {
  this.date = date;
  return this;
 }
 
 public Date getDate() {
  return date;
 }
 
 public String getType() {
  return type;
 }
 
 /**
  * To dom element.
  *
  * @param document
  *         the document
  * @return the org.jsoup.nodes. element
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  org.jsoup.nodes.Element timeElement = document.createElement("time");
  timeElement.attr("class", type);
  timeElement.attr("dateTime", dateFormat.format(date));
  timeElement.text(printDateFormat.format(date));
  return timeElement;
 }
 
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 public boolean isValid() {
  return this.date != null ? true : false;
 }
 
}
