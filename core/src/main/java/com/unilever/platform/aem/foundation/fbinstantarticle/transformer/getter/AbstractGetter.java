/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

/**
 * The Class AbstractGetter.
 */
public abstract class AbstractGetter {
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the element getter
  */
 abstract public AbstractGetter createFrom(JSONObject configuration);
 
 /**
  * Gets the.
  *
  * @param node
  *         the node
  * @return the element
  */
 abstract public Object get(Node node);
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public String getClassName() {
  return this.getClass().getName();
 }
 
}
