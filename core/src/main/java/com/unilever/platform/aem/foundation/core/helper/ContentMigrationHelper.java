/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.helper;

import java.util.Iterator;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.json.JSONArray;
import org.json.JSONObject;

import com.day.cq.wcm.api.Page;

/**
 * The Class ContentMigrationHelper.
 */
public class ContentMigrationHelper {
 /** The Constant OLD. */
 public static final String OLD = "old";
 
 /** The Constant NEW. */
 public static final String NEW = "new";
 
 /**
  * Instantiates a new content migration helper.
  */
 private ContentMigrationHelper() {
  
 }
 
 /**
  * Update page properties.
  * 
  * @param pageMap
  * @param successMessage
  * 
  * @return true, if successful
  * @throws RepositoryException
  *          the repository exception
  */
 public static boolean updatePageProperties(Map<Page, JSONArray> pageMap, StringBuilder successMessage) throws RepositoryException {
  Iterator<Page> itr = pageMap.keySet().iterator();
  int count = 0;
  while (itr.hasNext()) {
   count++;
   Page pageKey = itr.next();
   JSONArray propArray = pageMap.get(pageKey) != null ? pageMap.get(pageKey) : new JSONArray();
   Resource contentResource = pageKey.getContentResource();
   if (contentResource != null) {
    Node node = contentResource.adaptTo(Node.class);
    PropertyIterator propertyIterator = node.getProperties();
    successMessage.append("<br/>" + count + ") Page Path : " + pageKey.getPath() + "<br/><br/>");
    updateProperties(node, propertyIterator, propArray, successMessage);
   }
  }
  return true;
 }
 
 /**
  * Update properties.
  *
  * @param node
  *         the node
  * @param propertyIterator
  *         the property iterator
  * @param successMessage
  *         the successMessage
  * @param propArray
  *         the prop array
  * @throws RepositoryException
  *          the repository exception
  */
 public static void updateProperties(Node node, PropertyIterator propertyIterator, JSONArray propArray, StringBuilder successMessage)
   throws RepositoryException {
  
  boolean flag = false;
  while (propertyIterator.hasNext()) {
   javax.jcr.Property prop = propertyIterator.nextProperty();
   String propName = prop.getName();
   for (int i = 0; i < propArray.length(); ++i) {
    JSONObject propJSONObj = propArray.getJSONObject(i);
    String oldProp = propJSONObj.getString(OLD) != null ? propJSONObj.getString(OLD) : StringUtils.EMPTY;
    String newProp = propJSONObj.getString(NEW) != null ? propJSONObj.getString(NEW) : StringUtils.EMPTY;
    if (oldProp.equals(propName)) {
     flag = true;
     successMessage.append(oldProp + " -->  " + newProp + "<br/>");
     String val = prop.getValue().getString();
     node.setProperty(newProp, val);
     node.setProperty(oldProp, (Value) null);
    }
   }
  }
  if (!flag) {
   successMessage.append("No property updated<br/>");
  }
 }
}
