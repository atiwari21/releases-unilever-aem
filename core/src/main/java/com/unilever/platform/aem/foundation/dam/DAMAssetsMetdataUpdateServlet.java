/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class DAMMetadataServlet.
 */
@SlingServlet(label = "Unilever DAM Metadata Update Proprties Servlet", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = {
  HttpConstants.METHOD_GET }, selectors = { "updatedammetaprops" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.dam.DAMAssetsMetdataUpdateServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever DAM Metadata Update Proprties Servlet", propertyPrivate = false),

})
public class DAMAssetsMetdataUpdateServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(DAMAssetsMetdataUpdateServlet.class);
 
 /** The Constant SPACE. */
 private static final String SPACE = " ";
 
 /** The Constant DEFAULT_CONTENT_PATH. */
 private static final String DEFAULT_CONTENT_PATH = "/content/dam/unilever";
 
 /** The Constant DATE_FORMAT. */
 private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  ResourceResolver resourceResolver = request.getResourceResolver();
  Session session = resourceResolver.adaptTo(Session.class);
  
  RequestParameterMap requestParametrs = request.getRequestParameterMap();
  
  String searchPath = requestParametrs.containsKey("sp") ? requestParametrs.getValue("sp").toString() : StringUtils.EMPTY;
  
  if (StringUtils.isBlank(searchPath)) {
   searchPath = DEFAULT_CONTENT_PATH;
  }
  
  Query query = builder.createQuery(PredicateGroup.create(getQueryMap(searchPath)), resourceResolver.adaptTo(Session.class));
  SearchResult result = query.getResult();
  
  int counterAssets = 0;
  // iterating over the results
  for (Hit hit : result.getHits()) {
   Asset asset = getAssetFromSearchHit(hit, resourceResolver);
   if (asset != null) {
    counterAssets = updateAsset(asset, session, counterAssets, resourceResolver, response);
   }
  }
  response.getWriter().write("Total no of assets updated : " + String.valueOf(counterAssets));
 }
 
 /**
  * Checks if is exclude prop update.
  *
  * @param key
  *         the key
  * @return true, if is exclude prop update
  */
 private boolean isExcludePropUpdate(String key) {
  String[] nameSpacesProperies = { "dam:", "dc:", "exif:", "jcr:", "photoshop:", "tiff:", "xmp:", "crs:", "psAux:", "xmpMM:", "xmpRights:" };
  
  for (String nameSpace : nameSpacesProperies) {
   if (key.startsWith(nameSpace)) {
    return true;
   }
  }
  return false;
 }
 
 /**
  * Convert string to gregorian calendar date.
  *
  * @param dateInput
  *         the date input
  * @return the gregorian calendar
  */
 private GregorianCalendar convertStringToGregorianCalendarDate(String dateInput) {
  
  if (StringUtils.isNotBlank(dateInput)) {
   GregorianCalendar calendar = new GregorianCalendar();
   try {
    Date date = DATE_FORMAT.parse(dateInput.trim());
    calendar.setTime(date);
   } catch (Exception e) {
    LOGGER.error("Unable to covert string in date : ", e);
   }
   return calendar;
  }
  return null;
 }
 
 /**
  * Gets the updated key.
  *
  * @param key
  *         the key
  * @return the updated key
  */
 private String getUpdatedKey(String key) {
  String updatedKey = key.trim().replaceAll("\\s", "");
  updatedKey = updatedKey.substring(0, 1).toLowerCase() + updatedKey.substring(1);
  
  return updatedKey;
 }
 
 /**
  * Gets the query map.
  *
  * @param path
  *         the path
  * @return the query map
  */
 private Map<String, String> getQueryMap(String path) {
  Map<String, String> map = new HashMap<String, String>();
  
  map.put("path", path);
  map.put("type", DamConstants.NT_DAM_ASSET);
  
  map.put("p.offset", "0");
  map.put("p.limit", "-1");
  
  return map;
 }
 
 /**
  * Update property.
  * 
  * @param valueNode
  *         the value node
  * @param key
  *         the key
  * @param value
  *         the value
  * @param oldKey
  *         the old key
  */
 private void updateProperty(Node valueNode, String key, Object value, String oldKey, ValueMap valueMap) {
  try {
   if (value instanceof InputStream) {
    valueNode.setProperty(key, (InputStream) value);
    cleanProperties(valueNode, oldKey);
   }
   if (value instanceof String[]) {
    valueNode.setProperty(key, (String[]) value);
    cleanProperties(valueNode, oldKey);
   }
   if (value instanceof Long) {
    valueNode.setProperty(key, (Long) value);
    cleanProperties(valueNode, oldKey);
   }
   if (value instanceof Double) {
    valueNode.setProperty(key, (Double) value);
    cleanProperties(valueNode, oldKey);
   }
   if (value instanceof Boolean) {
    valueNode.setProperty(key, (Boolean) value);
    cleanProperties(valueNode, oldKey);
   }
   if (value instanceof BigDecimal) {
    valueNode.setProperty(key, (BigDecimal) value);
    cleanProperties(valueNode, oldKey);
   }
   if (value instanceof GregorianCalendar) {
    valueNode.setProperty(key, (GregorianCalendar) value);
    cleanProperties(valueNode, oldKey);
   }
   if (value instanceof String) {
    if ("latestEffectiveDate".equals(key.trim()) || "assetImportDate".equals(key.trim()) || "shipToTradeDate".equals(key.trim())
      || "earliestExpiryDate".equals(key.trim())) {
     convertStringPropToDate("latestEffectiveDate", valueMap, key, valueNode, oldKey);
     convertStringPropToDate("assetImportDate", valueMap, key, valueNode, oldKey);
     convertStringPropToDate("shipToTradeDate", valueMap, key, valueNode, oldKey);
     convertStringPropToDate("earliestExpiryDate", valueMap, key, valueNode, oldKey);
    } else {
     cleanProperties(valueNode, oldKey);
     valueNode.setProperty(key, (String) value);
    }
   }
  } catch (Exception e) {
   LOGGER.error("Unable to update property : " + key, e);
  }
 }
 
 /**
  * Convert string prop to date.
  *
  * @param prop
  *         the prop
  * @param valueMap
  *         the value map
  * @param key
  *         the key
  * @param valueNode
  *         the value node
  */
 private void convertStringPropToDate(String prop, ValueMap valueMap, String key, Node valueNode, String oldKey) {
  try {
   if (prop.equals(key.trim()) && ((valueMap.get(key)) instanceof String)) {
    GregorianCalendar cal = convertStringToGregorianCalendarDate(valueMap.get(key).toString());
    if (cal != null) {
     cleanProperties(valueNode, oldKey);
     valueNode.setProperty(key, cal);
    }
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in converting string property to date", e);
  }
 }
 
 /**
  * Convert string prop to long.
  *
  * @param prop
  *         the prop
  * @param valueMap
  *         the value map
  * @param key
  *         the key
  * @param valueNode
  *         the value node
  */
 private void convertStringPropToLong(String prop, ValueMap valueMap, String key, Node valueNode, String oldKey) {
  try {
   if (prop.equals(key.trim()) && ((valueMap.get(key)) instanceof String)) {
    Long id = Long.parseLong(valueMap.get(key).toString());
    if (id != null) {
     cleanProperties(valueNode, oldKey);
     valueNode.setProperty(key, id);
    }
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in converting string property to date", e);
  }
 }
 
 /**
  * Update asset.
  *
  * @param asset
  *         the asset
  * @param session
  *         the session
  * @param counterAssets
  *         the counter assets
  * @param resourceResolver
  *         the resource resolver
  * @param response
  *         the response
  * @return the int
  */
 private int updateAsset(Asset asset, Session session, int counterAssets, ResourceResolver resourceResolver, SlingHttpServletResponse response) {
  
  Resource resource = resourceResolver.getResource(asset.getPath() + "/jcr:content/metadata");
  if (resource != null) {
   
   ValueMap valueMap = resource.getValueMap();
   Node valueNode = resource.adaptTo(Node.class);
   
   try {
    for (String key : valueMap.keySet()) {
     if (!isExcludePropUpdate(key) && (key.contains(SPACE) || StringUtils.isAllUpperCase(key.substring(0, 1)))) {
      Object value = valueMap.get(key);
      String updatedKey = getUpdatedKey(key);
      
      updateProperty(valueNode, updatedKey, value, key, valueMap);
      response.getWriter().write("Updated Property : Old name -> " + key + " New name -> " + updatedKey + " Value = " + value.toString()
        + " For Asset " + asset.getName() + ".\n");
      
     }
    }
    session.save();
    counterAssets++;
   } catch (RepositoryException e) {
    LOGGER.error("Unable to update asset : " + e);
   } catch (IOException e) {
    LOGGER.error("Unable to write response : " + e);
   }
  }
  
  return counterAssets;
 }
 
 /**
  * Gets the asset from search hit.
  * 
  * @param h
  *         the hit
  * @param resourceResolver
  *         the resolver
  * @return the asset from search hit
  */
 private Asset getAssetFromSearchHit(Hit h, ResourceResolver resourceResolver) {
  String assetPath = StringUtils.EMPTY;
  Asset asset = null;
  try {
   assetPath = h.getPath();
   if (StringUtils.isNotBlank(assetPath)) {
    asset = resourceResolver.getResource(assetPath).adaptTo(Asset.class);
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in getting asset from search result : ", e);
  }
  return asset;
 }
 
 /**
  * Clean properties.
  * 
  * @param node
  *         the node
  * @param propName
  *         the prop name
  * @throws RepositoryException
  *          the repository exception
  */
 private void cleanProperties(Node node, String propName) throws RepositoryException {
  PropertyIterator existingProps = null;
  for (existingProps = node.getProperties(propName); existingProps.hasNext();) {
   javax.jcr.Property prop = (javax.jcr.Property) existingProps.next();
   prop.remove();
  }
 }
}
