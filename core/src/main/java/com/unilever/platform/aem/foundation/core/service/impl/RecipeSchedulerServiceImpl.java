/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.foundation.constants.UnileverConstants;

/**
 * The Class RecipeSchedulerServiceImpl.
 */
@Component(label = "Unilever Recipe Scheduler Service", immediate = true, metatype = true)
@Service(value = Runnable.class)
public class RecipeSchedulerServiceImpl implements Runnable {
 
 private static final String CQ_MASTER = "cq:master";
 
 private static final String ENHANCED_TEMPLATE = "enhanced_template";
 
 private static final String BASIC_TEMPLATE = "basic_template";
 
 private static final String CQ_PAGE_CONTENT = "cq:PageContent";
 
 private static final String BASE_TEMPLATE = "/apps/unilever-iea/templates/unileverBaseTemplate";
 
 private static final String BLUPRINT_CREATED_LOGGER = "bluprint created is";
 
 private static final String BLUEPRINT_TEMPLATE = "/libs/wcm/msm/templates/blueprint";
 
 private static final String ETC_BLUEPRINTS = "/etc/blueprints/";
 
 private static final String CQ_LIVE_SYNC_CONFIG = "cq:LiveSyncConfig";
 
 private static final String FOUNDATION_COMPONENTS_PARSYS = "foundation/components/parsys";
 
 private static final String CQ_LIVE_SYNC_CANCELLED = "cq:LiveSyncCancelled";
 
 private static final String CQ_LIVE_RELATIONSHIP = "cq:LiveRelationship";
 
 private static final String METADATA_PATH = "metadataPath";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant THREE. */
 private static final int THREE = 3;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RecipeSchedulerServiceImpl.class);
 
 /** The Constant DEFAULT_SERVICE_ENABLED. */
 private static final boolean DEFAULT_SERVICE_ENABLED = false;
 
 /** The Constant DEFAULT_SCHEDULER_EXPRESSION. */
 private static final String DEFAULT_SCHEDULER_EXPRESSION = "0 0 0/8 * * ?";
 
 /** The Constant DEFAULT_RECIPE_ID_PROP. */
 private static final String DEFAULT_RECIPE_ID_PROP = "recipeId";
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant SERVICE_ENABLED. */
 @Property(label = "Enabled", description = "Enable/Disable the Scheduled Service", boolValue = true)
 private static final String SERVICE_ENABLED = "service.enabled";
 
 /** The Constant SCHEDULER_EXPRESSION. */
 @Property(label = "Cron expression for recipe scheduleder", value = DEFAULT_SCHEDULER_EXPRESSION)
 private static final String SCHEDULER_EXPRESSION = "scheduler.expression";
 
 /** The Constant BRAND_LOCALES_PATH. */
 @Property(unbounded = PropertyUnbounded.ARRAY, label = "Path for brand locale combination")
 private static final String BRAND_LOCALES_PATH = "brand.locale";
 
 /** The Constant SCHEDULER_CONCURRENT. */
 @Property(label = "Allow concurrent executions", boolValue = false)
 private static final String SCHEDULER_CONCURRENT = "scheduler.concurrent";
 
 /** The Constant AUTO_ACTIVATE. */
 @Property(boolValue = true, label = "Auto Activate pages on scheduler run?", description = "check to auto publish RDP pages after any modifications in scheduler run")
 private static final String AUTO_ACTIVATE = "autoActivate";
 
 /** The enabled. */
 private boolean enabled = true;
 
 /** The brand locales path. */
 private String[] brandLocalesPath = ArrayUtils.EMPTY_STRING_ARRAY;
 
 /** The autoActivate. */
 private boolean autoActivate = true;
 
 private static final String RECIPE_CONFIG = "recipeConfig";
 
 private static final String URL = "url";
 
 private static final String RECIPE_CONFIG_RESOURCETYPE = "unilever-aem/components/content/recipeConfig";
 
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 
 @Reference
 private Replicator replicator;
 
 private static final String DEFAULT_SERVICE_NAME = "ieaadmin";
 
 /**
  * Activate.
  *
  * @param componentContext
  *         the component context
  * @throws Exception
  *          the exception
  */
 @SuppressWarnings("unchecked")
 @Activate
 protected void activate(final ComponentContext componentContext) throws Exception {
  final Map<String, String> properties = (Map<String, String>) componentContext.getProperties();
  if (properties != null) {
   this.enabled = PropertiesUtil.toBoolean(properties.get(SERVICE_ENABLED), DEFAULT_SERVICE_ENABLED);
   this.brandLocalesPath = PropertiesUtil.toStringArray(properties.get(BRAND_LOCALES_PATH), ArrayUtils.EMPTY_STRING_ARRAY);
   this.autoActivate = PropertiesUtil.toBoolean(properties.get(AUTO_ACTIVATE), true);
  }
 }
 
 public ResourceResolver getResourceResolverByService(String serviceName) throws LoginException {
  Map<String, Object> resolverParamMap = new HashMap<String, Object>();
  
  resolverParamMap.put(ResourceResolverFactory.SUBSERVICE, serviceName);
  return resourceResolverFactory.getServiceResourceResolver(resolverParamMap);
 }
 
 /**
  * Run.
  */
 /*
  * (non-Javadoc)
  * 
  * @see java.lang.Runnable#run()
  */
 @Override
 public void run() {
  LOGGER.debug("Initiating Recipe Scheduler Service ---> ");
  if (enabled) {
   ResourceResolver resolver = null;
   try {
    resolver = getResourceResolverByService(DEFAULT_SERVICE_NAME);
    LOGGER.debug("Brand Locale Path ---> " + brandLocalesPath.toString());
    for (String path : brandLocalesPath) {
     LOGGER.debug("path ---> " + path);
     processRecipeInformation(path, resolver);
    }
    
   } catch (LoginException e) {
    LOGGER.error("error while fetching resource resolver. ", e);
   } finally {
    if (resolver != null) {
     try {
      resolver.commit();
     } catch (PersistenceException e) {
      LOGGER.error("some changes not saved", e);
     }
     LOGGER.debug("closing resource resolver.");
     resolver.close();
    }
   }
  }
 }
 
 /**
  * Process recipe information.
  *
  * @param brandLocalePath
  *         the brand locale path
  * @param resolver
  *         the resolver
  */
 private void processRecipeInformation(String brandLocalePath, ResourceResolver resolver) {
  
  LOGGER.debug("starting process recipe information ---> ");
  
  String brandName = StringUtils.EMPTY;
  String localeName = StringUtils.EMPTY;
  String recipesPath = StringUtils.EMPTY;
  
  Page homePage = null;
  
  PageManager pageMgr = resolver.adaptTo(PageManager.class);
  StringBuilder debugStmt = new StringBuilder();
  if (StringUtils.isNotBlank(brandLocalePath)) {
   brandName = getBrandName(brandLocalePath);
   localeName = getLocaleName(brandLocalePath);
   
  }
  debugStmt.append("brandName ---> " + brandName + "localeName ---> " + localeName);
  
  homePage = pageMgr.getPage(brandLocalePath);
  if (homePage != null) {
   debugStmt.append("homePage not null.");
   try {
    
    recipesPath = configurationService.getConfigValue(homePage, RECIPE_CONFIG, METADATA_PATH);
    debugStmt.append("recipesPath ---> " + recipesPath);
    
    String recipeConfigPagePath = configurationService.getConfigValue(homePage, RECIPE_CONFIG, URL);
    debugStmt.append("recipeconfigPagePath ---> " + recipeConfigPagePath);
    LOGGER.debug(debugStmt.toString());
    getRecipeConfigs(recipeConfigPagePath, recipesPath, resolver, brandLocalePath);
    
   } catch (ConfigurationNotFoundException e) {
    LOGGER.error("error while fetching cofigurations. ", e);
   }
  }
 }
 
 private void getRecipeConfigs(String recipePagePath, String recipesPath, ResourceResolver resolver, String brandLocalePath)
   throws ConfigurationNotFoundException {
  
  if (StringUtils.isNotBlank(recipePagePath)) {
   Session session = resolver.adaptTo(Session.class);
   PageManager pageManager = resolver.adaptTo(PageManager.class);
   Page recipeConfigpage = pageManager.getPage(recipePagePath);
   if (recipeConfigpage != null) {
    Iterable<Resource> child = recipeConfigpage.getContentResource().getChild("par").getChildren();
    
    for (Resource res : child) {
     operatePerConfig(res, recipesPath, resolver, session, brandLocalePath);
    }
   }
  }
 }
 
 /**
  * Gets the brand name.
  *
  * @param path
  *         the path
  * @return the brand name
  */
 private static String getBrandName(String path) {
  if (StringUtils.isNotBlank(path)) {
   String[] pathArray = path.split("/");
   if (pathArray.length >= TWO) {
    return pathArray[TWO];
   }
  }
  return StringUtils.EMPTY;
 }
 
 /**
  * Gets the locale name.
  *
  * @param path
  *         the path
  * @return the locale name
  */
 private static String getLocaleName(String path) {
  if (StringUtils.isNotBlank(path)) {
   String[] pathArray = path.split("/");
   if (pathArray.length >= THREE) {
    return pathArray[THREE];
   }
  }
  return StringUtils.EMPTY;
 }
 
 /**
  * Gets the recipes.
  *
  * @param recipeStubPath
  *         the recipe stub path
  * @param associatedRecipeTags
  *         the associated recipe tags
  * @param session
  *         the session
  * @return the recipes
  */
 private Iterator<Resource> getRecipesByTags(String recipeStubPath, String[] associatedRecipeTags, Session session) {
  
  LOGGER.debug("inside search map.");
  
  builder = getServiceReference(QueryBuilder.class);
  Map<String, String> map = new HashMap<String, String>();
  
  if (associatedRecipeTags != null && builder != null) {
   map.put("path", recipeStubPath);
   map.put("p.limit", "-1");
   
   map.put("type", JcrConstants.NT_UNSTRUCTURED);
   
   map.put("property", TagConstants.PN_TAGS);
   map.put("property.or", "true");
   
   int i = 0;
   
   for (String tag : associatedRecipeTags) {
    i++;
    String index = String.valueOf(i);
    map.put("property." + index + "_value", tag);
   }
   
   return getResultfromQuery(session, map);
  }
  return null;
 }
 
 /**
  * Gets the resultfrom query.
  *
  * @param session
  *         the session
  * @param map
  *         the map
  * @return the resultfrom query
  */
 @SuppressWarnings("unchecked")
 private Iterator<Resource> getResultfromQuery(Session session, Map<String, String> map) {
  
  LOGGER.debug("get result query");
  
  Query query = builder.createQuery(PredicateGroup.create(map), session);
  SearchResult result = null;
  Iterator<Resource> resources = IteratorUtils.emptyIterator();
  
  query.setHitsPerPage(0);
  if (query != null) {
   result = query.getResult();
  }
  
  if (result != null) {
   LOGGER.debug("result not null.");
   LOGGER.debug("result count --->" + result.getTotalMatches());
   resources = result.getResources();
  }
  
  return resources;
 }
 
 /**
  * Gets the service reference.
  *
  * @param <T>
  *         the generic type
  * @param serviceClass
  *         the service class
  * @return the service reference
  */
 @SuppressWarnings("unchecked")
 private static <T> T getServiceReference(final Class<T> serviceClass) {
  T serviceRef;
  final BundleContext bundleContext = FrameworkUtil.getBundle(serviceClass).getBundleContext();
  final ServiceReference osgiRef = bundleContext.getServiceReference(serviceClass.getName());
  serviceRef = (T) bundleContext.getService(osgiRef);
  return serviceRef;
 }
 
 /**
  * Creates the pages.
  *
  * @param recipe
  *         the recipe
  * @param resourceResolver
  *         the resource resolver
  * @param pagePath
  *         the page path
  * @param templatePath
  *         the template path
  */
 private void createRecipePages(Resource recipe, ResourceResolver resourceResolver, String pagePath, String templatePath, String enhancedTemplatePath,
   String aliasRegex, String brandLocalePath, String recipePath) {
  
  LOGGER.debug("Inside create pages method. ");
  
  Node recipeNode = recipe.adaptTo(Node.class);
  PageManager pageMgr = resourceResolver.adaptTo(PageManager.class);
  Session session = resourceResolver.adaptTo(Session.class);
  
  LOGGER.debug("recipe node path ---> " + recipe.getPath());
  try {
   String recipeId = getValueFromNode(recipeNode, DEFAULT_RECIPE_ID_PROP);
   String title = getValueFromNode(recipeNode, JcrConstants.JCR_TITLE);
   String description = getValueFromNode(recipeNode, JcrConstants.JCR_DESCRIPTION);
   Boolean isDeleted = Boolean.parseBoolean(getValueFromNode(recipeNode, "isDeleted"));
   String localAliasRegex = aliasRegex;
   localAliasRegex = StringUtils.isEmpty(localAliasRegex) ? "\\s+" : localAliasRegex;
   String aliasName = title.replaceAll(localAliasRegex, "-").toLowerCase();
   String debugStmt = "recipeId ---> " + recipeId + "title ---> " + title + "description ---> " + description;
   LOGGER.debug(debugStmt);
   
   Page pageTOSetUp = pageMgr.getPage(pagePath + "/" + recipeId);
   
   Page templatePage = pageMgr.getPage(templatePath);
   Page enhancedTemplatePage = pageMgr.getPage(enhancedTemplatePath);
   
   String templatePagePath = BASE_TEMPLATE;
   templatePagePath = createBluePrint(templatePath, pageMgr, brandLocalePath, resourceResolver, templatePagePath);
   templatePagePath = createBluePrint(enhancedTemplatePath, pageMgr, brandLocalePath, resourceResolver, templatePagePath);
   if (!isDeleted) {
    boolean isNew = false;
    String[] tagsArray = getRecipeAssociateTags(recipeNode, TagConstants.PN_TAGS);
    Page templateToUse = getTemplate(configurationService, pageMgr.getPage(brandLocalePath), resourceResolver, templatePage, enhancedTemplatePage,
      tagsArray);
    if (pageTOSetUp == null) {
     LOGGER.debug("page is null.");
     isNew = true;
    } else {
     isNew = false;
    }
    boolean changed = false;
    boolean diff = false;
    if (!isNew) {
     diff = isTemplateChanged(pageTOSetUp, templateToUse.getPath(), resourceResolver);
     if (!diff) {
      changed = setupPageContent(pageTOSetUp, resourceResolver, recipeNode, aliasName, isNew, templateToUse, tagsArray, changed);
      setUpLiveSyncNode(resourceResolver, pageTOSetUp, templateToUse.getPath());
     }
    }
    if (diff || isNew) {
     pageTOSetUp = pageMgr.create(pagePath, recipeId, templateToUse.getTemplate().getPath(), title, true);
     changed = setupPageContent(pageTOSetUp, resourceResolver, recipeNode, aliasName, isNew, templateToUse, tagsArray, changed);
     setUpLiveSyncNode(resourceResolver, pageTOSetUp, templateToUse.getPath());
    }
    setUpPage(pageTOSetUp, templateToUse, session, pageMgr, resourceResolver, isNew, changed);
   } else {
    if (pageTOSetUp != null) {
     replicateOrdeletePage(pageTOSetUp, session, pageMgr, recipePath, recipeId, recipeNode);
    }
   }
   
  } catch (WCMException e) {
   LOGGER.error("error while creating page - ", e);
  } catch (RepositoryException e) {
   LOGGER.error("error while logging in repository --> ", e);
  } catch (ReplicationException e) {
   LOGGER.error("Error while replication", e);
  } catch (PersistenceException e) {
   LOGGER.error("error while saving site source property", e);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("unable to find config", e);
  } finally {
   try {
    resourceResolver.commit();
   } catch (PersistenceException e) {
    LOGGER.error("error while saving properties" + e.getMessage());
    LOGGER.debug("error while saving properties", e);
   }
  }
  
 }
 
 /**
  * Gets the value from node.
  *
  * @param node
  *         the node
  * @param propertyName
  *         the property name
  * @return the value from node
  */
 private String getValueFromNode(Node node, String propertyName) {
  String value = StringUtils.EMPTY;
  LOGGER.debug("node ---> " + node);
  try {
   if (node != null && node.hasProperty(propertyName)) {
    LOGGER.debug("node not null ---> ");
    value = node.getProperty(propertyName).getValue().toString();
    LOGGER.debug("value ---> " + value);
   }
  } catch (RepositoryException e) {
   LOGGER.error("error while getting value from node for property - " + propertyName, e);
  }
  return value;
 }
 
 /**
  * Gets the recipe associate tags.
  *
  * @param node
  *         the node
  * @param propertyName
  *         the property name
  * @return the recipe associate tags
  */
 private String[] getRecipeAssociateTags(Node node, String propertyName) {
  
  try {
   if (node != null && node.hasProperty(propertyName)) {
    List<String> tags = new ArrayList<String>();
    
    Value[] tagsValueArray = node.getProperty(propertyName).getValues();
    for (Value tag : tagsValueArray) {
     tags.add(tag.getString());
    }
    String[] tagsArray = new String[tags.size()];
    tagsArray = tags.toArray(tagsArray);
    
    return tagsArray;
   }
  } catch (RepositoryException e) {
   LOGGER.error("error while getting value from node for property - " + propertyName, e);
  }
  return ArrayUtils.EMPTY_STRING_ARRAY;
 }
 
 /**
  * 
  * @param childNodes
  * @param pageMgr
  * @param jcrNode
  * @param resourceResolver
  * @param isNew
  * @throws WCMException
  * @throws ConfigurationNotFoundException
  * @throws RepositoryException
  * @throws PersistenceException
  */
 private void copyProperties(Iterator<Resource> childNodes, PageManager pageMgr, Node jcrNode, ResourceResolver resourceResolver, Boolean isNew)
   throws RepositoryException, ConfigurationNotFoundException, WCMException, PersistenceException {
  while (childNodes.hasNext()) {
   Resource resource = childNodes.next();
   String resourcePath = resource.getPath();
   String resourcePagePath = pageMgr.getContainingPage(resource).getPath();
   String jcrPath = pageMgr.getContainingPage(resourceResolver.getResource(jcrNode.getPath())).getPath();
   resourcePath = resourcePath.substring(resourcePagePath.length() + 1, resourcePath.length());
   String[] mixinType = ArrayUtils.EMPTY_STRING_ARRAY;
   String jcrResourcePath = jcrPath + "/" + resourcePath;
   Resource jcrResource = resourceResolver.getResource(jcrResourcePath);
   if (jcrResource != null) {
    ValueMap map = jcrResource.adaptTo(ValueMap.class);
    mixinType = (String[]) map.get(JcrConstants.JCR_MIXINTYPES);
   }
   if (isNew) {
    Node targetNode = JcrUtil.copy(resource.adaptTo(Node.class),
      getResourcePath(resourceResolver.getResource(ResourceUtil.getParent(resource.getPath(), 1)), pageMgr, resourceResolver, jcrNode),
      resource.getName());
    if (targetNode != null) {
     setLiveSyncOnNewNode(resourceResolver.getResource(targetNode.getPath()), resourceResolver);
     LOGGER.debug("Live Sync Property set on new node" + targetNode.getPath());
    }
   } else {
    createNodeAndCopyProps(resource, pageMgr, resourceResolver, jcrNode, mixinType, isNew);
   }
  }
 }
 
 private void setUpPage(Page pageToSetUp, Page templatePage, Session session, PageManager pageMgr, ResourceResolver resourceResolver, boolean isNew,
   boolean isChanged) {
  boolean changed = isChanged;
  Node jcrNode = pageToSetUp.getContentResource().adaptTo(Node.class);
  Page localTemplatePage = templatePage;
  try {
   if (localTemplatePage != null) {
    Iterator<Resource> childNodes = localTemplatePage.getContentResource().listChildren();
    copyProperties(childNodes, pageMgr, jcrNode, resourceResolver, isNew);
   }
   
   if (!changed && !isNew) {
    ValueMap templateMap = localTemplatePage.getContentResource().adaptTo(ValueMap.class);
    ValueMap jcrMap = pageToSetUp.getContentResource().adaptTo(ValueMap.class);
    Date lastModifiedDate = templateMap.get(NameConstants.PN_PAGE_LAST_MOD, Date.class);
    Date lastReplicatedDate = jcrMap.get(NameConstants.PN_PAGE_LAST_MOD, Date.class);
    int changedStamp = (lastModifiedDate != null && lastReplicatedDate != null) ? lastModifiedDate.compareTo(lastReplicatedDate) : 0;
    if (changedStamp > 0) {
     changed = true;
    }
   }
   if (!isNew) {
    removeExtraComponents(localTemplatePage.getContentResource(), pageToSetUp.getContentResource(), resourceResolver);
    if (copyTemplateNodeProperties(localTemplatePage, pageToSetUp)) {
     changed = true;
    }
   }
   if (changed || isNew) {
    pageMgr.touch(pageToSetUp.adaptTo(Node.class), true, Calendar.getInstance(), false);
    if (autoActivate) {
     replicator.replicate(session, ReplicationActionType.ACTIVATE, pageToSetUp.getPath());
     ReplicationStatus replicationStatus = pageToSetUp.adaptTo(ReplicationStatus.class);
     LOGGER.debug("replication status of " + pageToSetUp.getPath() + " is activated:" + replicationStatus.isActivated());
    }
   }
   session.save();
   LOGGER.debug("page saved.");
  } catch (ItemNotFoundException e) {
   LOGGER.error("page exception is", e);
  } catch (ValueFormatException e) {
   LOGGER.error("value format exception is", e);
  } catch (VersionException e) {
   LOGGER.error("version exception is", e);
  } catch (LockException e) {
   LOGGER.error("Lock exception is", e);
  } catch (ConstraintViolationException e) {
   LOGGER.error("Constraint voilation exception is", e);
  } catch (RepositoryException e) {
   LOGGER.error("repository exception is", e);
  } catch (PersistenceException e) {
   LOGGER.error("persistence error while removing extra compoent", e);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("error reading excludedComponents config", e);
  } catch (WCMException e) {
   LOGGER.error("error while touching page {}", pageToSetUp.getPath(), e);
  } catch (ReplicationException e) {
   LOGGER.error("unable to replicate page " + pageToSetUp.getPath(), e);
  }
 }
 
 /**
  * 
  * @param resourceResolver
  * @param templatePage
  * @param masterPath
  * @param isNew
  */
 public void setUpLiveSyncNode(ResourceResolver resourceResolver, Page templatePage, String masterPath) {
  try {
   if (resourceResolver.getResource(templatePage.getContentResource().getPath() + UnileverConstants.FORWARD_SLASH + CQ_LIVE_SYNC_CONFIG) == null) {
    Node liveSyncNode = JcrUtil.createUniqueNode(templatePage.getContentResource().adaptTo(Node.class), CQ_LIVE_SYNC_CONFIG, "cq:LiveCopy",
      resourceResolver.adaptTo(Session.class));
    liveSyncNode.setProperty("cq:excludedPaths", new String[0]);
    liveSyncNode.setProperty("cq:isDeep", true);
    liveSyncNode.setProperty(CQ_MASTER, masterPath);
    liveSyncNode.setProperty("cq:rolloutConfigs", new String[0]);
    resourceResolver.commit();
   }
  } catch (ValueFormatException e) {
   LOGGER.error("ValueFormatException exception is", e);
  } catch (VersionException e) {
   LOGGER.error("VersionException exception is", e);
  } catch (LockException e) {
   LOGGER.error("LockException exception is", e);
  } catch (ConstraintViolationException e) {
   LOGGER.error("ConstraintViolationException exception is", e);
  } catch (RepositoryException e) {
   LOGGER.error("RepositoryException exception is", e);
  } catch (PersistenceException e) {
   LOGGER.error("PersistenceException unable to persist value", e);
  }
 }
 
 /**
  * 
  * @param resource
  * @param pageMgr
  * @param resourceResolver
  * @param jcrNode
  * @return
  * @throws RepositoryException
  */
 public Node getResourcePath(Resource resource, PageManager pageMgr, ResourceResolver resourceResolver, Node jcrNode) throws RepositoryException {
  String resourcePath = resource.getPath();
  String resourcePagePath = pageMgr.getContainingPage(resource).getPath();
  String jcrPath = pageMgr.getContainingPage(resourceResolver.getResource(jcrNode.getPath())).getPath();
  resourcePath = resourcePath.substring(resourcePagePath.length() + 1, resourcePath.length());
  Resource currResource = resourceResolver.getResource(jcrPath + CommonConstants.FORWARD_SLASH + resourcePath);
  if (currResource != null) {
   return resourceResolver.getResource(jcrPath + CommonConstants.FORWARD_SLASH + resourcePath).adaptTo(Node.class);
  }
  return null;
  
 }
 
 /**
  * 
  * @param source
  * @param target
  * @param resourceResolver
  * @return
  * @throws PersistenceException
  * @throws ConfigurationNotFoundException
  * @throws WCMException
  */
 private Resource copyNodeProperties(Resource source, Resource target, ResourceResolver resourceResolver)
   throws PersistenceException, RepositoryException, ConfigurationNotFoundException, WCMException {
  Node targetNode = target.adaptTo(Node.class);
  PropertyIterator targetNodeProperties = targetNode.getProperties();
  ModifiableValueMap targetValueMap = target.adaptTo(ModifiableValueMap.class);
  ModifiableValueMap sourceValueMap = source.adaptTo(ModifiableValueMap.class);
  PageManager pagemanager = resourceResolver.adaptTo(PageManager.class);
  Page page = pagemanager.getContainingPage(target);
  while (targetNodeProperties.hasNext()) {
   String propertyNameKey = targetNodeProperties.nextProperty().getName();
   String restrictedPropety = configurationService.getConfigValue(page, RECIPE_CONFIG, "restrictedProperties");
   String[] restrictedProperties = restrictedPropety.split(",");
   if (!ArrayUtils.contains(restrictedProperties, propertyNameKey)) {
    targetNode.setProperty(propertyNameKey, (Value) null);
   }
  }
  targetValueMap.putAll(sourceValueMap);
  if (!StringUtils.equals(target.getResourceType(), FOUNDATION_COMPONENTS_PARSYS)) {
   targetValueMap.put(JcrConstants.JCR_MIXINTYPES, CQ_LIVE_RELATIONSHIP);
  }
  resourceResolver.commit();
  return target;
 }
 
 /**
  * Set Live Sync on New Node
  * 
  * @param resource
  *         the resource
  * @param resourceResolver
  *         the resourceResolver
  */
 public void setLiveSyncOnNewNode(Resource resource, ResourceResolver resourceResolver) {
  ModifiableValueMap valueMap = resource.adaptTo(ModifiableValueMap.class);
  String[] mixinType = (String[]) valueMap.get(JcrConstants.JCR_MIXINTYPES);
  if (ArrayUtils.isEmpty(mixinType)) {
   valueMap.put(JcrConstants.JCR_MIXINTYPES, CQ_LIVE_RELATIONSHIP);
   try {
    resourceResolver.commit();
   } catch (PersistenceException e) {
    LOGGER.debug("NO PROBLEM IF THIS PROPERTY DOESN'T GETS SET.. This exception is meant to be caught locally..!!");
    LOGGER.error("Problem addding mixin type to newly created node {}", resource.getPath(), e);
   }
  }
  if (resource.hasChildren()) {
   // Recursively call this method for every child of node.
   Iterator<Resource> children = resource.listChildren();
   while (children.hasNext()) {
    setLiveSyncOnNewNode(children.next(), resourceResolver);
   }
  }
  
 }
 
 /**
  * Remove the extra components
  * 
  * @param detailTemplateContentResource
  *         the detailTemplateContentResource
  * @param targetPageContentResource
  *         the targetPageContentResource
  * @param resolver
  *         the resolver
  * @return the page
  * @throws PersistenceException
  * @throws ConfigurationNotFoundException
  */
 private Page removeExtraComponents(Resource detailTemplateContentResource, Resource targetPageContentResource, ResourceResolver resolver)
   throws PersistenceException, ConfigurationNotFoundException {
  Iterator<Resource> children = targetPageContentResource.listChildren();
  PageManager manager = resolver.adaptTo(PageManager.class);
  String templatePage = manager.getContainingPage(detailTemplateContentResource).getPath();
  String targetPage = manager.getContainingPage(targetPageContentResource).getPath();
  while (children.hasNext()) {
   Resource child = children.next();
   String childPath = child.getPath();
   String resourceRelativePath = childPath.substring(targetPage.length() + 1, childPath.length());
   LOGGER.debug("resource relative path is " + resourceRelativePath);
   String resourceType = resolver.resolve(templatePage + CommonConstants.FORWARD_SLASH + resourceRelativePath).getResourceType();
   if (resourceType == Resource.RESOURCE_TYPE_NON_EXISTING) {
    ValueMap childMap = child.getValueMap();
    String[] mixinType = (String[]) childMap.get(JcrConstants.JCR_MIXINTYPES);
    if (!(ArrayUtils.isEmpty(mixinType) || ArrayUtils.contains(mixinType, "cq:LiveSyncCancelled"))
      && (!"cq:LiveSyncConfig".equals(child.getName()))) {
     resolver.delete(child);
     resolver.commit();
    }
   }
   if (resolver.getResource(childPath) != null && child.hasChildren()) {
    removeExtraComponents(detailTemplateContentResource, child, resolver);
   }
  }
  return manager.getContainingPage(targetPageContentResource);
  
 }
 
 /**
  * copies property from node to recipe detail page.
  * 
  * @param p
  * @param jcrNode
  * @param node
  * @param aliasName
  * @param isNew
  * @return
  * @throws ConfigurationNotFoundException
  * @throws ValueFormatException
  * @throws VersionException
  * @throws LockException
  * @throws ConstraintViolationException
  * @throws RepositoryException
  */
 private boolean rollOutNodeProperties(Page p, Resource jcrNode, Resource node, String aliasName, boolean isNew, boolean isChanged)
   throws ConfigurationNotFoundException, RepositoryException {
  boolean changed = isChanged;
  String rolloutProperties = configurationService.getConfigValue(p, RECIPE_CONFIG, "rolloutProperties");
  String noInheritedConfig = configurationService.getConfigValue(p, RECIPE_CONFIG, "noInheritedProperties");
  String[] noInheritedProperties = noInheritedConfig.split(",");
  ValueMap recipeMap = node.adaptTo(ValueMap.class);
  ModifiableValueMap pageMap = jcrNode.adaptTo(ModifiableValueMap.class);
  String[] rolloutArray = rolloutProperties.split(",");
  for (String rollOutProperty : rolloutArray) {
   String[] propertyMap = rollOutProperty.split("-");
   String sourcePropertyName = propertyMap[0];
   String targetPropertyName = sourcePropertyName;
   if (propertyMap.length == TWO) {
    targetPropertyName = propertyMap[1];
   }
   String sourcePropertyVal = StringUtils.EMPTY;
   String targetValue = StringUtils.EMPTY;
   if (recipeMap.containsKey(sourcePropertyName)) {
    sourcePropertyVal = (String) recipeMap.get(sourcePropertyName);
   }
   if (pageMap.containsKey(targetPropertyName)) {
    targetValue = (String) pageMap.get(targetPropertyName);
    if (StringUtils.isEmpty(sourcePropertyVal)) {
     JcrUtil.setProperty(jcrNode.adaptTo(Node.class), targetPropertyName, null);
    }
   }
   if (StringUtils.equals(targetPropertyName, "sling:alias")) {
    sourcePropertyVal = aliasName;
   }
   if (!StringUtils.equals(sourcePropertyVal, targetValue)) {
    if (!isNew) {
     if (!ArrayUtils.contains(noInheritedProperties, targetPropertyName)) {
      String[] values = ArrayUtils.EMPTY_STRING_ARRAY;
      if (pageMap.containsKey("cq:propertyInheritanceCancelled")) {
       values = (String[]) pageMap.get("cq:propertyInheritanceCancelled");
      }
      if (!ArrayUtils.contains(values, targetPropertyName)) {
       pageMap.put(targetPropertyName, sourcePropertyVal);
       changed = true;
      }
     }
    } else {
     pageMap.put(targetPropertyName, sourcePropertyVal);
    }
   }
  }
  
  return changed;
 }
 
 /**
  * 
  * @param templatePage
  * @param p
  * @return
  * @throws ConfigurationNotFoundException
  * @throws PathNotFoundException
  * @throws ValueFormatException
  * @throws VersionException
  * @throws LockException
  * @throws ConstraintViolationException
  * @throws RepositoryException
  */
 private boolean copyTemplateNodeProperties(Page templatePage, Page p) throws ConfigurationNotFoundException, RepositoryException {
  boolean changed = false;
  Node templatePageNode = templatePage.getContentResource().adaptTo(Node.class);
  Node pageNode = p.getContentResource().adaptTo(Node.class);
  String pagePropertiesNames = configurationService.getConfigValue(p, RECIPE_CONFIG, "pagePropertiesNames");
  if (StringUtils.isNotBlank(pagePropertiesNames)) {
   for (String property : pagePropertiesNames.split(",")) {
    setTemplateProperties(templatePageNode, property, pageNode);
   }
  }
  if (pageNode.isModified()) {
   changed = true;
  }
  return changed;
 }
 
 private String createBluePrint(String templatePath, PageManager pageMgr, String brandLocalePath, ResourceResolver resourceResolver,
   String templatePagePathParam) throws WCMException, PersistenceException {
  String templatePagePath = templatePagePathParam;
  Page templatePage = pageMgr.getPage(templatePath);
  if (templatePage != null) {
   templatePagePath = templatePage.getTemplate().getPath();
   String templatePageName = getBrandName(brandLocalePath) + getLocaleName(brandLocalePath) + templatePage.getName();
   String blueprintPath = ETC_BLUEPRINTS;
   if (resourceResolver.getResource(blueprintPath + templatePageName) == null) {
    Page bluprint;
    bluprint = pageMgr.create(blueprintPath, templatePageName, BLUEPRINT_TEMPLATE, templatePageName, true);
    ModifiableValueMap valueMap = bluprint.getContentResource().adaptTo(ModifiableValueMap.class);
    valueMap.put(NameConstants.PN_SITE_PATH, templatePage.getPath());
    resourceResolver.commit();
    LOGGER.debug(BLUPRINT_CREATED_LOGGER + bluprint.getPath());
   }
   
  }
  return templatePagePath;
 }
 
 private Page getTemplate(ConfigurationService configurationService, Page marketPage, ResourceResolver resourceResolver, Page templatePage,
   Page enhancedTemplatePage, String[] tagsArray) throws ConfigurationNotFoundException {
  Page localTemplatePage = templatePage;
  String basicTemplates = configurationService.getConfigValue(marketPage, RECIPE_CONFIG, BASIC_TEMPLATE);
  String enhancedTemplates = configurationService.getConfigValue(marketPage, RECIPE_CONFIG, ENHANCED_TEMPLATE);
  TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
  Tag templatetag = tagManager.resolve(basicTemplates);
  if (templatetag != null) {
   basicTemplates = templatetag.getTagID();
  }
  templatetag = tagManager.resolve(enhancedTemplates);
  if (templatetag != null) {
   enhancedTemplates = templatetag.getTagID();
  }
  if (StringUtils.isNotEmpty(enhancedTemplates) && ArrayUtils.contains(tagsArray, enhancedTemplates) && enhancedTemplatePage != null) {
   localTemplatePage = enhancedTemplatePage;
  }
  return localTemplatePage;
 }
 
 private boolean setupPageContent(Page pageToSetUp, ResourceResolver resourceResolver, Node recipeNode, String aliasName, boolean isNew,
   Page localTemplatePage, String[] tagsArray, boolean isChanged) throws ConfigurationNotFoundException, RepositoryException {
  boolean changed = isChanged;
  Node jcrNode = null;
  Node pageNode = pageToSetUp.adaptTo(Node.class);
  if (pageToSetUp.hasContent()) {
   jcrNode = pageToSetUp.getContentResource().adaptTo(Node.class);
  } else {
   jcrNode = pageNode.addNode(JcrConstants.JCR_CONTENT, CQ_PAGE_CONTENT);
   changed = true;
  }
  changed = rollOutNodeProperties(pageToSetUp, resourceResolver.getResource(jcrNode.getPath()), resourceResolver.getResource(recipeNode.getPath()),
    aliasName, isNew, changed);
  jcrNode.addMixin("cq:LiveSync");
  String[] pageTagsArray = getRecipeAssociateTags(jcrNode, TagConstants.PN_TAGS);
  
  Set<String> tagsList = new HashSet<String>();
  for (String tag : tagsArray) {
   tagsList.add(tag);
  }
  for (String tag : pageTagsArray) {
   tagsList.add(tag);
  }
  if (!tagsList.isEmpty()) {
   String[] tags = new String[tagsList.size()];
   int i = 0;
   for (String t : tagsList) {
    tags[i] = t;
    i++;
   }
   jcrNode.setProperty(TagConstants.PN_TAGS, tags);
  }
  if (!changed) {
   if (ArrayUtils.isEmpty(pageTagsArray)) {
    if (ArrayUtils.isNotEmpty(tagsArray)) {
     changed = true;
    }
   } else {
    Set<String> pageTagList = new HashSet<String>(Arrays.asList(pageTagsArray));
    tagsList.removeAll(pageTagList);
    if (!pageTagList.containsAll(tagsList)) {
     changed = true;
    }
   }
  }
  try {
   resourceResolver.commit();
  } catch (PersistenceException e) {
   LOGGER.error("unable to persist value" + e.getMessage());
   LOGGER.debug("unable to persist value", e);
  }
  return changed;
 }
 
 private boolean isTemplateChanged(Page templatePage, String masterPath, ResourceResolver resourceResolver)
   throws RepositoryException, PersistenceException, WCMException {
  boolean diff = false;
  if (templatePage != null) {
   Resource contentResource = templatePage.getContentResource();
   String liveSyncNodePath = contentResource.getPath() + UnileverConstants.FORWARD_SLASH + CQ_LIVE_SYNC_CONFIG;
   Resource cqLiveSyncResource = resourceResolver.getResource(liveSyncNodePath);
   ModifiableValueMap cqLiveSyncMap = cqLiveSyncResource.adaptTo(ModifiableValueMap.class);
   String cqMaster = (String) cqLiveSyncMap.get(CQ_MASTER);
   if (StringUtils.isEmpty(cqMaster) || !StringUtils.equals(cqMaster, masterPath)) {
    cqLiveSyncMap.put(CQ_MASTER, masterPath);
    if (contentResource != null) {
     diff = true;
     resourceResolver.adaptTo(PageManager.class).delete(templatePage, false, true);
    }
    resourceResolver.commit();
   }
  }
  return diff;
 }
 
 /**
  * 
  * @param res
  * @param recipesPath
  * @param resolver
  * @param session
  * @param brandLocalePath
  */
 public void operatePerConfig(Resource res, String recipesPath, ResourceResolver resolver, Session session, String brandLocalePath) {
  if (RECIPE_CONFIG_RESOURCETYPE.equals(res.getResourceType())) {
   ValueMap properties = res.getValueMap();
   
   String recipeLandingPagePath = MapUtils.getString(properties, "recipeLandingPagePath", StringUtils.EMPTY);
   StringBuilder debugStmt = new StringBuilder("recipeLandingPagePath ---> " + recipeLandingPagePath);
   
   String recipeDetailTemplatePagePath = MapUtils.getString(properties, "basicRecipeDetailTemplatePagePath", StringUtils.EMPTY);
   debugStmt.append("recipeDetailTemplatePagePath ---> " + recipeDetailTemplatePagePath);
   
   String enhancedRecipeDetailTemplatePagePath = MapUtils.getString(properties, "enhancedRecipeDetailTemplatePagePath", StringUtils.EMPTY);
   debugStmt.append("enhancedRecipeDetailTemplatePagePath ---> " + enhancedRecipeDetailTemplatePagePath);
   
   String aliasRegex = MapUtils.getString(properties, "aliasRegex", StringUtils.EMPTY);
   debugStmt.append("alias Regex ---> " + aliasRegex);
   
   String[] tagsArray = getRecipeAssociateTags(res.adaptTo(Node.class), TagConstants.PN_TAGS);
   if (StringUtils.isNotBlank(recipeLandingPagePath)
     && (StringUtils.isNotBlank(recipeDetailTemplatePagePath) || StringUtils.isNotEmpty(enhancedRecipeDetailTemplatePagePath))) {
    
    Iterator<Resource> associatedRecipes = getRecipesByTags(recipesPath, tagsArray, session);
    debugStmt.append("associatedRecipes ---> " + associatedRecipes);
    LOGGER.debug(debugStmt.toString());
    while (associatedRecipes.hasNext()) {
     Resource recipe = associatedRecipes.next();
     LOGGER.debug("recipe path ---> " + recipe.getPath());
     createRecipePages(recipe, resolver, recipeLandingPagePath, recipeDetailTemplatePagePath, enhancedRecipeDetailTemplatePagePath, aliasRegex,
       brandLocalePath, recipesPath);
    }
   }
  }
 }
 
 private void setTemplateProperties(Node templatePageNode, String property, Node pageNode) throws RepositoryException {
  if (templatePageNode.hasProperty(property)) {
   javax.jcr.Property propertyjcr = templatePageNode.getProperty(property);
   if (!propertyjcr.getDefinition().isProtected()) {
    if (templatePageNode.getProperty(property).isMultiple()) {
     pageNode.setProperty(property, propertyjcr.getValues());
    } else {
     pageNode.setProperty(property, propertyjcr.getValue());
    }
   }
  } else if (pageNode.hasProperty(property)) {
   pageNode.getProperty(property).remove();
  }
  
 }
 
 private void replicateOrdeletePage(Page pageTOSetUp, Session session, PageManager pageMgr, String recipePath, String recipeId, Node recipeNode)
   throws ReplicationException, WCMException, RepositoryException {
  
  ReplicationStatus replicationStatus = pageTOSetUp.adaptTo(ReplicationStatus.class);
  if (StringUtils.isNotBlank(recipeId)) {
   if (replicationStatus.isActivated()) {
    LOGGER.debug("page is in published state", pageTOSetUp.getPath());
    replicator.replicate(session, ReplicationActionType.DEACTIVATE, pageTOSetUp.getPath());
    ModifiableValueMap map = pageTOSetUp.getContentResource().adaptTo(ModifiableValueMap.class);
    map.put("isRecipeDeleted", true);
    session.save();
    LOGGER.debug("page deactivated{}", pageTOSetUp.getPath());
   }
   if (replicationStatus.isDeactivated() && !replicationStatus.isPending()) {
    pageMgr.delete(pageTOSetUp, false);
    if (pageMgr.getPage(recipePath + "/" + recipeId) == null) {
     recipeNode.remove();
    }
    session.save();
    LOGGER.error("Page deleted {} and recipe deleted{}", pageTOSetUp.getPath(), recipeNode);
   }
  }
  
 }
 
 private void createNodeAndCopyProps(Resource resource, PageManager pageMgr, ResourceResolver resourceResolver, Node jcrNode, String[] mixinTypeParam,
   boolean isNew) throws RepositoryException, ConfigurationNotFoundException, WCMException, PersistenceException {
  String[] mixinType = mixinTypeParam;
  Node jcrResourceNode = getResourcePath(resource, pageMgr, resourceResolver, jcrNode);
  if (jcrResourceNode == null) {
   LOGGER.debug("no matching resource path in current page" + resource.getPath());
   LOGGER.debug("jcr node path is" + jcrNode.getPath());
   ValueMap jcrResourceValueMap = resource.getValueMap();
   String jcrResourcePrimaryType = jcrResourceValueMap.containsKey(JcrConstants.JCR_PRIMARYTYPE)
     ? (String) jcrResourceValueMap.get(JcrConstants.JCR_PRIMARYTYPE) : JcrConstants.NT_UNSTRUCTURED;
   jcrResourceNode = JcrUtil.createUniqueNode(
     getResourcePath(resourceResolver.getResource(ResourceUtil.getParent(resource.getPath(), 1)), pageMgr, resourceResolver, jcrNode),
     resource.getName(), jcrResourcePrimaryType, resourceResolver.adaptTo(Session.class));
  }
  mixinType = (String[]) resourceResolver.getResource(jcrResourceNode.getPath()).getValueMap().get(JcrConstants.JCR_MIXINTYPES);
  if (ArrayUtils.isEmpty(mixinType)) {
   ModifiableValueMap valueMap = resourceResolver.getResource(jcrResourceNode.getPath()).adaptTo(ModifiableValueMap.class);
   valueMap.put(JcrConstants.JCR_MIXINTYPES, CQ_LIVE_RELATIONSHIP);
   resourceResolver.commit();
  }
  if (!ArrayUtils.contains(mixinType, CQ_LIVE_SYNC_CANCELLED)) {
   copyNodeProperties(resource, resourceResolver.getResource(jcrResourceNode.getPath()), resourceResolver);
  }
  if (resource.hasChildren()) {
   copyProperties(resource.listChildren(), pageMgr, jcrNode, resourceResolver, isNew);
  }
  
 }
}
