/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

/**
 * The Class ResourceResolverUtility.
 */
@Service
public class ResourceResolverUtility {
 
 /** The resource resolver factory. */
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 
 /** The Constant DEFAULT_SERVICE_NAME. */
 private static final String DEFAULT_SERVICE_NAME = "ieaAdminService";
 
 /**
  * Gets the resource resolver by service.
  *
  * @return the resource resolver by service
  * @throws LoginException
  *          the login exception
  */
 public ResourceResolver getResourceResolverByService() throws LoginException {
  return getResourceResolverByService(DEFAULT_SERVICE_NAME);
 }
 
 /**
  * Gets the resource resolver by service.
  *
  * @param serviceName
  *         the service name
  * @return the resource resolver by service
  * @throws LoginException
  *          the login exception
  */
 public ResourceResolver getResourceResolverByService(String serviceName) throws LoginException {
  Map<String, Object> resolverParamMap = new HashMap<String, Object>();
  
  resolverParamMap.put(ResourceResolverFactory.SUBSERVICE, serviceName);
  return resourceResolverFactory.getServiceResourceResolver(resolverParamMap);
 }
}
