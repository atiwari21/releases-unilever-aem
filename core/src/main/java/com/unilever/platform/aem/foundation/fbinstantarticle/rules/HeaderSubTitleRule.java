/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.H2;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class HeaderSubTitleRule.
 */
public class HeaderSubTitleRule extends ConfigurationSelectorRule {
 
 /**
  * Creates the.
  *
  * @return the header sub title rule
  */
 public static HeaderSubTitleRule create() {
  return new HeaderSubTitleRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the header title rule
  */
 public static HeaderSubTitleRule createFrom(JSONObject configuration) {
  return (HeaderSubTitleRule) create().withSelector(configuration.getString("selector"));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Header header = Header.create();
  if (container instanceof InstantArticle) {
   header = ((InstantArticle) container).getHeader();
  }
  H2 h2 = H2.create();
  h2.appendText(((Element) node).html());
  
  return header.withSubTitle(transformer.transform(h2, node));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Header.getClassName() };
 }
 
}
