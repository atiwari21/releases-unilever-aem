/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.TextNode;

/**
 * Class Element. This class is the meta each tag element that contains rendering code for the tags.
 *
 */
abstract public class InstantArticleElement {
 
 /**
  * To dom element.
  *
  * @return the org.jsoup.nodes. element
  */
 public abstract org.jsoup.nodes.Element toDOMElement();
 
 /**
  * To dom element.
  *
  * @param document
  *         the document
  * @return the org.jsoup.nodes. element
  */
 public abstract org.jsoup.nodes.Element toDOMElement(Document document);
 
 /**
  * Checks if is valid.
  *
  * @return true, if is valid
  */
 public boolean isValid() {
  return true;
 }
 
 /**
  * Empty element.
  *
  * @param document
  *         the document
  * @return the org.jsoup.nodes. element
  */
 protected org.jsoup.nodes.Element emptyElement(Document document) {
  return document.appendChild(new TextNode("", document.baseUri()));
 }
 
 /**
  * Render.
  *
  * @param docType
  *         the doc type
  * @param format
  *         the format
  * @return the string
  */
 public String render(String docType, boolean format) {
  Document document = new Document("");
  org.jsoup.nodes.Element element = this.toDOMElement(document);
  String html = element.outerHtml();
  return html;
 }
 
 /**
  * Render.
  *
  * @return the string
  */
 public String render() {
  return render("", false);
 }
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }
}
