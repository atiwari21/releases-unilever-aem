/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Class List that represents a simple HTML list
 *
 * Example: <li>Dog</li>
 */
public class ListItem extends TextContainer {
 /** The children. */
 private List<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> children = null;
 
 private ListItem() {
  children = new ArrayList<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement>();
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static ListItem create() {
  return new ListItem();
 }
 
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element listItem = document.createElement("li");
  if (CollectionUtils.isNotEmpty(this.children)) {
   Iterator<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> childItr = this.children.iterator();
   while (childItr.hasNext()) {
    com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement child = childItr.next();
    if (child instanceof TextContainer) {
     TextContainer textContainer = (TextContainer) child;
     if (CollectionUtils.isEmpty(textContainer.getTextChildren())) {
      continue;
     } else if (textContainer.getTextChildren().size() == 1) {
      Object content = textContainer.getTextChildren().get(0);
      if (content instanceof String && content.toString().equals("")) {
       continue;
      }
     }
    }
    Element domElement = child.toDOMElement();
    if (domElement != null) {
     listItem.appendChild(domElement);
    }
    
   }
  }
  if (CollectionUtils.isNotEmpty(this.textChildren)) {
   Iterator<Object> itr = this.textChildren.iterator();
   while (itr.hasNext()) {
    listItem.appendText("" + itr.next());
   }
  }
  
  return listItem;
 }
 
 /**
  * Adds new child elements to this InstantArticle.
  *
  * @param child
  *         the child
  * @return $this
  */
 public ListItem addChild(com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement child) {
  children.add(child);
  return this;
 }
 
 @Override
 public boolean isValid() {
  if (CollectionUtils.isNotEmpty(children)) {
   for (InstantArticleElement child : children) {
    if (!child.isValid()) {
     return false;
    }
   }
  } else if (CollectionUtils.isNotEmpty(textChildren)) {
   return true;
  }
  return false;
 }
 
}