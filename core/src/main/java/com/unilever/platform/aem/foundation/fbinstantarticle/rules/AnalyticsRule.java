/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Analytics;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class AnalyticsRule.
 */
public class AnalyticsRule extends ConfigurationSelectorRule {
 
 /** The Constant PROPERTY_TRACKER_URL. */
 public static final String PROPERTY_TRACKER_URL = "analytics.url";
 
 /** The Constant PROPERTY_TRACKER_EMBED_URL. */
 public static final String PROPERTY_TRACKER_EMBED_URL = "analytics.embed";
 
 /**
  * Instantiates a new analytics rule.
  */
 private AnalyticsRule() {
 }
 
 /**
  * Creates the.
  *
  * @return the analytics rule
  */
 public static AnalyticsRule create() {
  return new AnalyticsRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration the configuration
  * @return the analytics rule
  */
 public static AnalyticsRule createFrom(JSONObject configuration) {
  AnalyticsRule analyticsRule=create();
  analyticsRule.withSelector(configuration.getString("selector"));
  List<String> properties=new ArrayList<String>();
  properties.add(PROPERTY_TRACKER_EMBED_URL);
  properties.add(PROPERTY_TRACKER_URL);
  analyticsRule.withProperties(properties, configuration);
  return analyticsRule;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Analytics analytics=Analytics.create();
  String url=(String)this.getProperty(PROPERTY_TRACKER_URL, node);
  if(StringUtils.isNotBlank(url)){
   analytics.withSource(url);
  }
  String embed_code=(String)this.getProperty(PROPERTY_TRACKER_EMBED_URL, node);
  if(StringUtils.isNotBlank(embed_code)){
   analytics.withHTML(embed_code);
  }
  
  if(StringUtils.isNotBlank(url)|| StringUtils.isNotBlank(embed_code)){
   transformer.getInstantArticle().addChild(analytics);
  }
  return container;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName() };
 }
}
