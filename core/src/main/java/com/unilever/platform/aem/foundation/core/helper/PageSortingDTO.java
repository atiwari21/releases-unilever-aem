/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;

/**
 * The Class PageSortingDTO.
 */
public class PageSortingDTO {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PageSortingDTO.class);
 
 /** The Constant DEFAULT_DATE_STRING. */
 private static final String DEFAULT_DATE_STRING = "1900/01/01";
 
 /** The Constant DATE_FORMAT. */
 private static final String DATE_FORMAT = "yyyy/MM/dd";
 
 /** The default date. */
 private static Date defaultDate;
 static {
  DateFormat df = new SimpleDateFormat(DATE_FORMAT);
  try {
   defaultDate = df.parse(DEFAULT_DATE_STRING);
  } catch (ParseException e) {
   LOGGER.warn("Date ParseException", e);
  }
 }
 
 /** The count. */
 private int count;
 
 /** The date. */
 private Date date;
 
 /** The promotion tag count. */
 private int promotionTagCount;
 
 /**
  * Instantiates a new page sorting dto.
  *
  * @param page
  *         the page
  * @param tagsList
  *         the tags list
  * @param promotionTagIds
  *         the promotion tag ids
  */
 public PageSortingDTO(Page page, List<String> tagsList, String[] promotionTagIds) {
  setDataObj(page, tagsList, promotionTagIds);
 }
 
 /**
  * Gets the count.
  *
  * @return the count
  */
 public int getCount() {
  return count;
 }
 
 /**
  * Sets the count.
  *
  * @param count
  *         the new count
  */
 public void setCount(int count) {
  this.count = count;
 }
 
 /**
  * Gets the date.
  *
  * @return the date
  */
 public Date getDate() {
  return date;
 }
 
 /**
  * Sets the date.
  *
  * @param date
  *         the new date
  */
 public void setDate(Date date) {
  this.date = date;
 }
 
 /**
  * Gets the promotion tag count.
  *
  * @return the promotion tag count
  */
 public int getPromotionTagCount() {
  return promotionTagCount;
 }
 
 /**
  * Sets the promotion tag count.
  *
  * @param promotionTagCount
  *         the new promotion tag count
  */
 public void setPromotionTagCount(int promotionTagCount) {
  this.promotionTagCount = promotionTagCount;
 }
 
 /**
  * Sets the data obj.
  *
  * @param page
  *         the page
  * @param tagsList
  *         the tags list
  * @param promotionTagIds
  *         the promotion tag ids
  */
 public void setDataObj(Page page, List<String> tagsList, String[] promotionTagIds) {
  try {
   Tag[] tagArr = page.getTags();
   for (Tag tag : tagArr) {
    String tagId = tag.getTagID();
    if (CollectionUtils.isNotEmpty(tagsList) && isMatchingTags(tagsList, tagId)) {
     this.count++;
    }
    if (promotionTagIds != null && promotionTagIds.length > 0 && isMatchingTags(Arrays.asList(promotionTagIds), tagId)) {
     this.promotionTagCount++;
    }
   }
  } catch (IllegalStateException e) {
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  }
  
  Product product = CommerceHelper.findCurrentProduct(page);
  
  Date publishDate = defaultDate;
  if(product instanceof UNIProduct){
   publishDate = setProductLaunchDate(page, (UNIProduct)product);
  }
  this.setDate(publishDate);
 }
 
 /**
  * Sets the product launch date.
  *
  * @param page
  *         the page
  * @param uniProduct
  *         the uni product
  * @return the date
  */
 private Date setProductLaunchDate(Page page, UNIProduct uniProduct) {
  Date publishDate;
  if (uniProduct != null) {
   publishDate = uniProduct.getProductionDate() != null ? uniProduct.getProductionDate() : defaultDate;
  } else {
   publishDate = page.getProperties().get("publishDate") != null ? page.getProperties().get("publishDate", Date.class) : defaultDate;
   publishDate = publishDate == null ? defaultDate : publishDate;
  }
  return publishDate;
 }
 
 /**
  * Checks if is matching tags.
  *
  * @param tagsList
  *         the tags list
  * @param tagId
  *         the tag id
  * @return true, if is matching tags
  */
 private boolean isMatchingTags(List<String> tagsList, String tagId) {
  boolean tagFlag = false;
  for (String individualTag : tagsList) {
   if (tagId.startsWith(individualTag)) {
    tagFlag = true;
    break;
   }
  }
  
  return tagFlag;
 }
 
}
