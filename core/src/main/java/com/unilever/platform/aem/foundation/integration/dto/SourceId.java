/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * The Class SourceId.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "value" })
public class SourceId {
 
 /** The value. */
 @XmlValue
 protected String value;
 
 /** The active. */
 @XmlAttribute(name = "Active")
 protected String active;
 
 /**
  * Gets the value.
  *
  * @return the value
  */
 public String getValue() {
  return value;
 }
 
 /**
  * Sets the value.
  *
  * @param value
  *         the new value
  */
 public void setValue(String value) {
  this.value = value;
 }
 
 /**
  * Gets the active.
  *
  * @return the active
  */
 public String getActive() {
  return active;
 }
 
 /**
  * Sets the active.
  *
  * @param value
  *         the new active
  */
 public void setActive(String value) {
  this.active = value;
 }
 
}
