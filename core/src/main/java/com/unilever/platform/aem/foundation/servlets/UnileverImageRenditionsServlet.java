/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.servlets;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.IOException;

import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.ImageHelper;
import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.commons.AbstractImageServlet;
import com.day.cq.wcm.foundation.Image;
import com.day.image.Layer;
import com.sapient.platform.iea.aem.core.tenant.Tenant;
import com.sapient.platform.iea.aem.core.tenant.service.TenantConfigurationService;
import com.sapient.platform.iea.core.exception.SystemException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Class UnileverImageRenditionsServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { UnileverImageRenditionsServlet.SELECTOR }, methods = {
  HttpConstants.METHOD_GET })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UnileverImageRenditionsServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Render images on-the-fly for variety of sizes", propertyPrivate = false) })
public class UnileverImageRenditionsServlet extends AbstractImageServlet {
 
 /**
  * Variable for Logger.
  */
 private static final Logger LOG = LoggerFactory.getLogger(UnileverImageRenditionsServlet.class);
 /**
  * serial version number.
  */
 private static final long serialVersionUID = 1L;
 
 /**
  * Constant for "ulecscale" selector.
  */
 public static final String SELECTOR = "ulenscale";
 
 /**
  * Constant for global config image rendition size.
  */
 public static final String GLOBAL_CONFIG_RENDITION_IMAGE = "imageRenditionSize";
 
 /**
  * Constant for global config image rendition size key.
  */
 public static final String VALID_IMAGE_SIZE = "validSizes";
 
 /**
  * Constant for brand.
  */
 public static final String BRAND = "brandName";
 
 /**
  * Constant for market .
  */
 public static final String MARKET = "brandLocale";
 
 /**
  * ConfigurationService service reference.
  */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * Variable to hold TenantConfigurationService object.
  */
 @Reference
 private TenantConfigurationService tenantConfigurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.day.cq.wcm.commons.AbstractImageServlet#createLayer(com.day.cq.wcm.commons.AbstractImageServlet.ImageContext)
  */
 @Override
 protected Layer createLayer(ImageContext imageContext) throws RepositoryException, IOException {
  
  Tenant tenant = tenantConfigurationService.getTenant(imageContext.request);
  
  Layer computedLayer = null;
  String[] selectors = imageContext.request.getRequestPathInfo().getSelectors();
  String size = null;
  int width = 0;
  int height = 0;
  int indexOfUlEnscale = ArrayUtils.indexOf(selectors, SELECTOR);
  Image image = new Image(imageContext.resource);
  String requestPath = imageContext.request.getResource().getPath();
  Layer originalLayer = getOriginalLayer(image, imageContext, requestPath);
  
  if (indexOfUlEnscale > -1 && selectors.length > indexOfUlEnscale + CommonConstants.ONE) {
   size = selectors[indexOfUlEnscale + CommonConstants.ONE];
  }
  if (isValidSize(size, tenant) && StringUtils.contains(size, CommonConstants.CROSS)) {
   width = Integer.valueOf(StringUtils.split(size, CommonConstants.CROSS)[CommonConstants.ZERO]);
   height = Integer.valueOf(StringUtils.split(size, CommonConstants.CROSS)[CommonConstants.ONE]);
  } else {
   LOG.debug("The size {} is not valid or configured at the site level configurations hence blank image is being rendered.", size);
   return null;
  }
  computedLayer = computeAndRenderImage(originalLayer, width, height);
  return computedLayer;
 }
 
 /**
  * Gets the original layer.
  *
  * @param image
  *         {@link Image}.
  * @param imageContext
  *         {@link ImageContext}.
  * @param requestPath
  *         {@link String}
  * @return {@link Layer}
  * @throws RepositoryException
  *          {@link RepositoryException}
  * @throws IOException
  *          {@link IOException}
  */
 protected Layer getOriginalLayer(final Image image, final ImageContext imageContext, final String requestPath)
   throws RepositoryException, IOException {
  
  String imagePath = getImagePath(image, imageContext, requestPath);
  return ImageHelper.createLayer(imageContext.node.getSession(), imagePath);
  
 }
 
 /**
  * It is used to return the original image path.
  *
  * @param image
  *         The image object.
  * @param imageContext
  *         The image context.
  * @param requestPath
  *         The request path.
  * @return The original image rendition path.
  * @throws RepositoryException
  *          The repository exception.
  */
 private String getImagePath(final Image image, final ImageContext imageContext, final String requestPath) throws RepositoryException {
  String imagePath = null;
  
  if (!image.hasContent() && StringUtils.isNotEmpty(requestPath)) {
   if (checkIfAsset(requestPath, imageContext)) {
    imagePath = requestPath;
   } else {
    throw new SystemException(
      "The resource having path as " + requestPath + " in URI cannot be adapted to asset. " + "Check the resource type and try again.");
   }
  }
  
  return checkImagePath(imagePath, image, imageContext);
 }
 
 /**
  * Check if asset.
  *
  * @param requestPath
  *         the request path
  * @param imageContext
  *         the image context
  * @return true, if successful
  */
 private boolean checkIfAsset(final String requestPath, final ImageContext imageContext) {
  Resource resource = null;
  Asset asset = null;
  
  resource = imageContext.request.getResourceResolver().getResource(requestPath);
  asset = resource.adaptTo(Asset.class);
  if (asset != null) {
   return true;
  }
  return false;
 }
 
 /**
  * This method checks if the passed path is empty or not. If its empty then the path is obtained using getHref() else original image path is returned
  * by adapting the obtained resource to asset.
  *
  * @param imagePath
  *         The image path.
  * @param image
  *         The image object.
  * @param imageContext
  *         The image context.
  * @return The original image rendition path.
  * @throws RepositoryException
  *          The repository exception.
  */
 private String checkImagePath(final String imagePath, final Image image, final ImageContext imageContext) throws RepositoryException {
  String path = imagePath;
  Resource resource = null;
  Asset asset = null;
  
  if (StringUtils.isEmpty(path)) {
   
   if (StringUtils.isNotEmpty(image.getHref()) && image.getHref().startsWith("/content/dam")) {
    resource = imageContext.request.getResourceResolver().getResource(image.getHref());
    asset = resource.adaptTo(Asset.class);
    path = asset.getOriginal().getPath();
   } else {
    path = image.getData().getPath();
   }
   
  } else {
   resource = imageContext.request.getResourceResolver().getResource(path);
   asset = resource.adaptTo(Asset.class);
   path = asset.getOriginal().getPath();
  }
  return path;
  
 }
 
 /**
  * Scales the given image to the dimensions specified by newWidth and newHeight. The scaling algorithm will always scale the image in such a way that
  * no white space will visible around the image. This means that in any case where the dimensions are not the exact aspect ratio of the image, some
  * cropping will occur. Once the image has been cropped it will be adjusted so the center of the cropped dimension is still centered.
  *
  * @param originalLayer
  *         the original layer
  * @param newWidth
  *         new width
  * @param newHeight
  *         specify 0 to scale based on width and keep the current aspect ratio
  * @return scaled (and/or cropped) image layer
  */
 private Layer computeAndRenderImage(final Layer originalLayer, final int newWidth, final int newHeight) {
  int scaleHeight = newHeight;
  int currentWidth = originalLayer.getWidth();
  int currentHeight = originalLayer.getHeight();
  Dimension newSize;
  
  // Try resizing the width first and test if the height is > newHeight
  // We do not want any whitespace on the generated image. Trimming is
  // fine.
  double widthRatio = (double) newWidth / currentWidth;
  double heightRatio = (double) scaleHeight / currentHeight;
  
  // Scale height proportionally to the width if set to 0
  if (scaleHeight == 0) {
   scaleHeight = (int) (currentHeight * widthRatio);
  }
  
  int potentialScaledHeight = (int) (currentHeight * widthRatio);
  if (potentialScaledHeight >= scaleHeight) {
   newSize = new Dimension(newWidth, potentialScaledHeight);
  } else {
   newSize = new Dimension((int) (currentWidth * heightRatio), scaleHeight);
  }
  
  return renderScaledImageOnLayer(originalLayer, newSize, newWidth, scaleHeight);
 }
 
 /**
  * Crop the original image to new size.
  *
  * @param originalLayer
  *         the original layer
  * @param scaledSize
  *         the scaled size
  * @param newWidth
  *         the new width
  * @param newHeight
  *         the new height
  * @return the layer
  */
 private Layer renderScaledImageOnLayer(final Layer originalLayer, final Dimension scaledSize, final int newWidth, final int newHeight) {
  originalLayer.resize(scaledSize.width, scaledSize.height);
  
  int shiftX = 0;
  int shiftY = 0;
  // One of the dimensions will be equal to the target. We need to center
  // the other axis.
  if (scaledSize.width != newWidth) {
   shiftX = (Math.abs(scaledSize.width - newWidth) / CommonConstants.TWO);
  } else {
   shiftY = (Math.abs(scaledSize.height - newHeight) / CommonConstants.TWO);
  }
  
  Rectangle newDimensions = new Rectangle();
  newDimensions.setBounds(shiftX, shiftY, newWidth, newHeight);
  originalLayer.crop(newDimensions);
  
  return originalLayer;
 }
 
 /**
  * Checks if is valid size.
  *
  * @param size
  *         the size
  * @return true, if is valid size
  */
 private boolean isValidSize(String size, Tenant tenant) {
  boolean isImageSizeValid = true;
  
  String validSizes = StringUtils.EMPTY;
  try {
   
   BrandMarketBean brandMarketBean = new BrandMarketBean(getProperty(tenant, BRAND), getProperty(tenant, MARKET));
   validSizes = configurationService.getConfigValue(brandMarketBean, GLOBAL_CONFIG_RENDITION_IMAGE, VALID_IMAGE_SIZE);
  } catch (ConfigurationNotFoundException e) {
   LOG.equals("configuration not found for image sizes" + e);
  }
  
  if (StringUtils.isNotBlank(validSizes) && StringUtils.isNotBlank(size)) {
   validSizes = StringUtils.replace(validSizes, " ", "");
   String[] availableImageSizes = validSizes.split(CommonConstants.COMMA);
   isImageSizeValid = ArrayUtils.indexOf(availableImageSizes, size) > -1;
   LOG.debug("Is image size {} available in configurations? {}.", size, isImageSizeValid);
  } else {
   isImageSizeValid = false;
  }
  return isImageSizeValid;
 }
 
 /**
  * Returns the property value configured in the Tenant.
  * 
  * @param tenant
  * @param propertyName
  * @return property value
  */
 private String getProperty(Tenant tenant, String propertyName) {
  String property = StringUtils.EMPTY;
  if (tenant != null) {
   Object propertyObject = tenant.getTenantConfigMap().get(propertyName);
   if (propertyObject != null) {
    property = propertyObject.toString();
   }
  }
  return property;
 }
 
 /*
  * Overriding doGet of AbstractImageServlet (non-Javadoc)
  * 
  * @see com.day.cq.wcm.commons.AbstractImageServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String imageQuality = StringUtils.EMPTY;
  ImageContext context = null;
  Layer layer = null;
  try {
   if (checkModifiedSince(request, response)) {
    return;
   }
   String type = getImageType(request.getRequestPathInfo().getExtension());
   if (type == null) {
    response.sendError(HttpStatus.SC_NOT_FOUND, "Image type not supported");
    return;
   }
   context = new ImageContext(request, type);
   layer = createLayer(context);
   if (layer != null) {
    applyDiff(layer, context);
   } else {
    response.sendError(HttpStatus.SC_NOT_FOUND, "Invalid image size!");
    return;
   }
   Tenant tenant = tenantConfigurationService.getTenant(request);
   BrandMarketBean brandMarketBean = new BrandMarketBean(getProperty(tenant, BRAND), getProperty(tenant, MARKET));
   imageQuality = configurationService.getConfigValue(brandMarketBean, GLOBAL_CONFIG_RENDITION_IMAGE, "imageQuality");
  } catch (ConfigurationNotFoundException e) {
   LOG.error("configuration not found for image sizes" + e);
  } catch (RepositoryException e) {
   LOG.error("Repository exception found for layer", e);
  }
  try {
   double imagePixelQuality = StringUtils.isNotBlank(imageQuality) ? Double.parseDouble(imageQuality) : 1.0D;
   writeLayer(request, response, context, layer, imagePixelQuality);
  } catch (RepositoryException e) {
   LOG.error("Repository exception found ", e);
  }
 }
 
}
