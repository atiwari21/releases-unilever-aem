/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * The Class GenericHelper.
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class FeaturePhoneImg implements HandlebarsHelperService<Object> {
 
 /**
  * Logger Instance.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(FeaturePhoneImg.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new FeaturePhoneImg();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "featurePhoneImg";
 
 /**
  * The integer one.
  */
 private static final int ONE = 1;
 
 /**
  * The integer two.
  */
 private static final int TWO = 2;
 
 /**
  * The integer three.
  */
 private static final int THREE = 3;
 
 /**
  * The integer four.
  */
 private static final int FOUR = 4;
 
 /**
  * The integer five.
  */
 private static final int FIVE = 5;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.github.jknack.handlebars.Helper#apply(java.lang.Object, com.github.jknack.handlebars.Options)
  */
 @SuppressWarnings("unchecked")
 @Override
 public CharSequence apply(Object path, Options options) throws IOException {
  
  CharSequence chars = null;
  String imageURL = null;
  String dimensions = null;
  String ext = null;
  String alt = null;
  String className = null;
  String title = null;
  String altText = StringUtils.EMPTY;
  String titlePath = StringUtils.EMPTY;
  String imagePath = StringUtils.EMPTY;
  String classPath = StringUtils.EMPTY;
  
  try {
   if (path != null) {
    LOGGER.debug("path = ", path.toString());
    imageURL = path.toString();
    
    Object[] paramMap = options.params;
    if (paramMap != null) {
     
     int size = paramMap.length;
     
     if (size > ONE) {
      dimensions = paramMap[ONE] == null ? null : paramMap[ONE].toString();
     }
     if (size > ONE) {
      ext = paramMap[TWO] == null ? null : paramMap[TWO].toString();
     }
     if (size > THREE) {
      alt = paramMap[THREE] == null ? null : paramMap[THREE].toString();
      altText = " alt = '" + alt + "'";
     }
     if (size > FOUR) {
      title = paramMap[FOUR] == null ? null : paramMap[FOUR].toString();
      titlePath = " title = '" + title + "'";
     }
     if (size > FIVE) {
      className = paramMap[FIVE] == null ? null : paramMap[FIVE].toString();
      classPath = " class = '" + className + "'";
     }
     
     if (imageURL != null) {
      if ((dimensions != null) && (ext != null)) {
       dimensions = dimensions.replace(".", "x");
       imagePath = imageURL + ".ulenscale." + dimensions + "." + ext;
      } else {
       imagePath = imageURL;
      }
     }
    }
   }
   
   chars = "<img src='" + imagePath + "' " + classPath + altText + titlePath + ">";
   
  } catch (Exception e) {
   LOGGER.error("Error occurred in featurePhoneImg Helper.", e);
  }
  
  return chars;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#setCache(com.sapient.platform.iea.aem.scripting.
  * handlebars.api.HandlebarsTemplateCache)
  */
 @Override
 public void setCache(HandlebarsTemplateCache templateCache) {
  LOGGER.info("Cache is not implemented for renderTemplate handler.");
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getName()
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getHelper()
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
}
