/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 * The Class StringGetter.
 */
public class StringGetter extends ChildrenGetter {
 
 /** The attribute. */
 protected String attribute;
 
 /** The prefix. */
 protected String prefix;
 
 /** The suffix. */
 protected String suffix;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.ElementGetter#createFrom(org.json.JSONObject)
  */
 public ElementGetter createFrom(JSONObject configuration) {
  if (configuration.has("selector")) {
   this.withSelector(configuration.getString("selector"));
  }
  if (configuration.has("attribute")) {
   this.withAttribute(configuration.getString("attribute"));
  }
  if (configuration.has("prefix")) {
   this.withPrefix(configuration.getString("prefix"));
  }
  if (configuration.has("suffix")) {
   this.withSuffix(configuration.getString("suffix"));
  }
  return this;
 }
 
 /**
  * With suffix.
  *
  * @param suffix
  *         the suffix
  * @return the element getter
  */
 public ElementGetter withSuffix(String suffix) {
  this.suffix = suffix;
  return this;
 }
 
 /**
  * With prefix.
  *
  * @param prefix
  *         the prefix
  * @return the element getter
  */
 public ElementGetter withPrefix(String prefix) {
  this.prefix = prefix;
  return this;
 }
 
 /**
  * With attribute.
  *
  * @param attribute
  *         the attribute
  * @return the element getter
  */
 public ElementGetter withAttribute(String attribute) {
  this.attribute = attribute;
  return this;
 }
 
 /**
  * Gets the String.
  *
  * @param node
  *         the node
  * @return the 1
  */
 public Object get(Node node) {
  String result = "";
  Elements elements = this.findAll(node, this.selector);
  if (CollectionUtils.isNotEmpty(elements)) {
   Element element = elements.get(0);
   if (this.attribute != null) {
    result = element.attr(this.attribute);
   } else {
    result = element.text();
   }
   if (StringUtils.isNotBlank(this.prefix)) {
    result = this.prefix;
   }
   if (StringUtils.isNotBlank(this.suffix)) {
    result = this.suffix;
   }
   return result;
  }
  return null;
  
 }
}
