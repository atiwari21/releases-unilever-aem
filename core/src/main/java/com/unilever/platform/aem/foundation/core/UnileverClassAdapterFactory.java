/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.adapter.AdapterFactory;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;
import com.unilever.platform.aem.foundation.core.service.AnalyticsService;

/**
 * A factory for creating UnileverClassAdapter objects.
 */
@Component(metatype = false)
@Service(value = AdapterFactory.class)
public class UnileverClassAdapterFactory implements AdapterFactory {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UnileverClassAdapterFactory.class);
 
 /** The Constant CONFIGURATION_SERVICE_CLASS. */
 private static final Class<ConfigurationService> CONFIGURATION_SERVICE_CLASS = ConfigurationService.class;
 
 /** The Constant GLOBAL_CONFIGURATION_SERVICE_CLASS. */
 private static final Class<GlobalConfiguration> GLOBAL_CONFIGURATION_SERVICE_CLASS = GlobalConfiguration.class;
 
 /** The Constant ANALYTIC_SERVICE_CLASS. */
 private static final Class<AnalyticsService> ANALYTIC_SERVICE_CLASS = AnalyticsService.class;
 
 /** The resource resolver factory. */
 @Reference
 ResourceResolverFactory resourceResolverFactory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The analytics service. */
 @Reference
 AnalyticsService analyticsService;
 
 @Reference
 GlobalConfiguration globalConfiguration;
 
 /** The Constant ADAPTER_CLASSES. */
 @Property(name = SlingConstants.PROPERTY_ADAPTER_CLASSES)
 private static final String[] ADAPTER_CLASSES = { CONFIGURATION_SERVICE_CLASS.getName(), ANALYTIC_SERVICE_CLASS.getName(),
   GLOBAL_CONFIGURATION_SERVICE_CLASS.getName() };
 
 /** The Constant ADAPTABLE_CLASSES. */
 @Property(name = SlingConstants.PROPERTY_ADAPTABLE_CLASSES)
 private static final String[] ADAPTABLE_CLASSES = { ResourceResolver.class.getName(), Page.class.getName() };
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.adapter.AdapterFactory#getAdapter(java.lang.Object, java.lang.Class)
  */
 public <T> T getAdapter(Object adaptable, Class<T> t) {
  if (adaptable instanceof ResourceResolver) {
   return getAdapterFromResourceResolver((ResourceResolver) adaptable, t);
  }
  if (adaptable instanceof Page) {
   return getAdapterFromPage((Page) adaptable, t);
  }
  return null;
 }
 
 /**
  * Gets the adapter classes.
  *
  * @return the adapterClasses
  */
 public static String[] getAdapterClasses() {
  return ADAPTER_CLASSES;
 }
 
 /**
  * Gets the adaptable classes.
  *
  * @return the adaptableClasses
  */
 public static String[] getAdaptableClasses() {
  return ADAPTABLE_CLASSES;
 }
 
 /**
  * Adapter for resource.
  *
  * @param <AdapterType>
  *         the generic type
  * @param resourceResolver
  *         the resource resolver
  * @param type
  *         the type
  * @return the adapter
  */
 @SuppressWarnings("unchecked")
 private <T> T getAdapterFromResourceResolver(ResourceResolver resourceResolver, Class<T> t) {
  T obj = null;
  if (null == resourceResolver) {
   return null;
  }
  try {
   if (t == CONFIGURATION_SERVICE_CLASS) {
    obj = (T) configurationService;
   }
   if (t == ANALYTIC_SERVICE_CLASS) {
    obj = (T) analyticsService;
   }
   if (t == GLOBAL_CONFIGURATION_SERVICE_CLASS) {
    obj = (T) globalConfiguration;
   }
  } catch (Exception e) {
   LOGGER.error("Unable to adapt resourceResolver {}", resourceResolver, e);
  }
  return obj;
 }
 
 /**
  * Gets the adapter.
  *
  * @param <AdapterType>
  *         the generic type
  * @param page
  *         the page
  * @param type
  *         the type
  * @return the adapter
  */
 @SuppressWarnings("unchecked")
 private <T> T getAdapterFromPage(Page page, Class<T> t) {
  T obj = null;
  if (null == page) {
   return null;
  }
  try {
   if (t == CONFIGURATION_SERVICE_CLASS) {
    obj = (T) configurationService;
   }
   if (t == ANALYTIC_SERVICE_CLASS) {
    obj = (T) analyticsService;
   }
   if (t == GLOBAL_CONFIGURATION_SERVICE_CLASS) {
    obj = (T) globalConfiguration;
   }
  } catch (Exception e) {
   LOGGER.error("Unable to adapt page {}", page.getName(), e);
  }
  return obj;
 }
}
