/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.variants.PageVariantsProvider;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.utils.LanguageSelectorUtility;

@SlingServlet(resourceTypes = { SearchConstants.SLING_SERVLET_RESOURCETYPE_VALUE }, methods = { HttpConstants.METHOD_GET }, selectors = {
  "recipefeed" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.integration.BVFeedProviderService", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "BV Feed provider service call for unilever", propertyPrivate = false),

})
/**
 * The Class RecipeFeedProviderServlet.
 */
public class RecipeFeedProviderServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RecipeFeedProviderServlet.class);
 
 /** The Constant BRAND_NAME_KEY. */
 private static final String BRAND_NAME_KEY = "brandName";
 
 /** The Constant BRAND_NAME_KEY. */
 private static final String BRAND_ID_KEY = "brandId";
 
 /** The Constant LOCALE_NAME_KEY. */
 private static final String LOCALE_NAME_KEY = "localeName";
 
 /** The Constant FEED_DOMAIN_URL. */
 private static final String FEED_DOMAIN_URL = "feedDomainUrl";
 
 /** The Constant BV_CATEGORY_KEY. */
 private static final String BV_CATEGORY_KEY = "bazaarvoice";
 
 /** The locale. */
 private static String locale = "";
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The page variant. */
 @Reference
 PageVariantsProvider pageVariant;
 
 /**
  * Do get.
  *
  * @param slingRequest
  *         the sling request
  * @param slingResponse
  *         the sling response
  */
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) {
  
  Session session = null;
  String path = null;
  /** The resource resolver. */
  ResourceResolver resourceResolver;
  
  String jsonString = "";
  List<Map<String, String>> languagesList = new ArrayList<Map<String, String>>();
  
  try {
   session = slingRequest.getResourceResolver().adaptTo(Session.class);
   resourceResolver = slingRequest.getResourceResolver();
   path = slingRequest.getRequestPathInfo().getResourcePath();
   ProductFeeds feed = new ProductFeeds();
   Resource res = resourceResolver.getResource(path);
   List<Locale> lList = new ArrayList<Locale>();
   
   if (null != res) {
    Page page = res.adaptTo(Page.class);
    languagesList = LanguageSelectorUtility.getLanguageMap(page, slingRequest, resourceResolver, configurationService, -1, pageVariant, true);
    
    getFeedsData(session, resourceResolver, languagesList, feed, lList);
    
   } else if (res == null) {
    
    feed.setStatus("fail");
    feed.setMessage("Resource is null");
   }
   
   jsonString = RecipeRatingHelper.saveFeedtoFileSystem(feed);
   slingResponse.setContentType("application/json");
   slingResponse.setCharacterEncoding("utf-8");
   
   slingResponse.getWriter().write(jsonString);
  } catch (IOException e) {
   LOGGER.error("IOexception in generating JSON", e);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error while fetching configuration for bazaarvoice .", e);
  }
  
 }
 
 /**
  * Gets the feeds data.
  *
  * @param session
  *         the session
  * @param resourceResolver
  *         the resource resolver
  * @param languagesList
  *         the languages list
  * @param feed
  *         the feed
  * @param lList
  *         the l list
  * @return the feeds data
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 private void getFeedsData(Session session, ResourceResolver resourceResolver, List<Map<String, String>> languagesList, ProductFeeds feed,
   List<Locale> lList) throws ConfigurationNotFoundException {
  for (Map<String, String> map : languagesList) {
   for (Map.Entry<String, String> entry : map.entrySet()) {
    String key = entry.getKey();
    String value = entry.getValue();
    getLocalefeeds(session, resourceResolver, lList, key, value, feed);
   }
   feed.setLocales(lList);
   if (!lList.isEmpty() || null != lList) {
    feed.setStatus("success");
    feed.setMessage("Feeds generated successfully");
   }
  }
 }
 
 /**
  * Gets the localefeeds.
  *
  * @param session
  *         the session
  * @param resourceResolver
  *         the resource resolver
  * @param lList
  *         the l list
  * @param key
  *         the key
  * @param value
  *         the value
  * @param feed
  *         the feed
  * @return the localefeeds
  */
 private void getLocalefeeds(Session session, ResourceResolver resourceResolver, List<Locale> lList, String key, String value, ProductFeeds feed) {
  String domainUrl;
  String brandName;
  Locale localeData;
  
  try {
   if ("link".equals(key) && null != value) {
    Page page1 = resourceResolver.getResource(value).adaptTo(Page.class);
    // provide service provider name
    String serviceProvidername = configurationService.getConfigValue(page1, "ratingAndReviews", "serviceProviderName");
    Map<String, String> serviceProviderConfig = configurationService.getCategoryConfiguration(page1, serviceProvidername);
    if (serviceProviderConfig != null) {
     domainUrl = serviceProviderConfig.get(FEED_DOMAIN_URL);
     brandName = serviceProviderConfig.get(BRAND_NAME_KEY);
     locale = serviceProviderConfig.get(LOCALE_NAME_KEY);
     String brandId = serviceProviderConfig.get(BRAND_ID_KEY);
     localeData = getLocalesFeed(brandName, brandId, locale, session, value, domainUrl, resourceResolver);
     lList.add(localeData);
     
    } else {
     feed.setMessage("Configuration not available for" + BV_CATEGORY_KEY);
     feed.setStatus("fail");
     
    }
    
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Error while fetching configuration for bazaarvoice .", e);
  }
  
 }
 
 /**
  * Gets the category feed.
  *
  * @param brandName
  *         the brand name
  * @param brandId
  *         the brand id
  * @param locale
  *         the locale
  * @param session
  *         the session
  * @param path
  *         the path
  * @param domainUrl
  *         the domain url
  * @param resourceResolver
  *         the resource resolver
  * @return the category feed
  */
 private Locale getLocalesFeed(String brandName, String brandId, String locale, Session session, String path, String domainUrl,
   ResourceResolver resourceResolver) {
  
  List<Page> productPageList = new ArrayList<Page>();
  Locale localefeed = new Locale();
  localefeed.setLocalename(locale);
  BrandType bt = new BrandType();
  bt.setName(brandName);
  if (StringUtils.isNotBlank(brandId)) {
   bt.setBrandId(brandId);
  } else {
   bt.setBrandId(brandName);
  }
  localefeed.setBrand(bt);
  localefeed.setCategories(RecipeRatingHelper.getCategoriesFeed(path, domainUrl, resourceResolver, configurationService, productPageList));
  localefeed.setProducts(getRecipesFeed(domainUrl, resourceResolver, productPageList));
  
  return localefeed;
 }
 
 /**
  * Gets the Recipes feed.
  * 
  * @param domainUrl
  *         the domain url
  * @param resourceResolver
  *         the resource resolver
  * @param productPageList
  *         the product page list
  * @return the products feed
  */
 private ProductsType getRecipesFeed(String domainUrl, ResourceResolver resourceResolver, List<Page> productPageList) {
  ProductsType products = new ProductsType();
  
  for (Page page : productPageList) {
   if (page != null) {
    ProductType productType = RecipeRatingHelper.getRecipeFeed(page, domainUrl, configurationService, resourceResolver);
    if (productType != null) {
     products.getProduct().add(productType);
    }
   }
  }
  
  return products;
 }
 
}
