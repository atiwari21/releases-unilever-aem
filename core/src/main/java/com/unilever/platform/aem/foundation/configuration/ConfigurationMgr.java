/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class ConfigurationMgr.
 */
public class ConfigurationMgr {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationMgr.class);
 
 /** The resource resolver. */
 private ResourceResolver resourceResolver;
 
 /**
  * Instantiates a new configuration mgr.
  *
  * @param resourceResolver
  *         the resource resolver
  */
 ConfigurationMgr(ResourceResolver resourceResolver) {
  LOGGER.info("Inside ConfigurationMgr Constructor");
  this.resourceResolver = resourceResolver;
 }
 
 /**
  * Load configuration. The method is responsible for creating the configuration keys from site hierarchy's nested configPages and loading these
  * configuration keys and values to a map
  * 
  * @param sitesPaths
  *         the sites paths
  * @return the map
  */
 public Map<String, String> loadConfiguration(String[] sitesPaths) {
  LOGGER.info("Inside loadConfiguration method");
  Map<String, String> finalMap = new TreeMap<String, String>();
  try {
   LOGGER.debug("Adding nested configuration of parent config pages");
   for (String sitePath : sitesPaths) {
    Resource siteResource = resourceResolver.getResource(sitePath);
    
    if (siteResource != null) {
     Iterator<Resource> marketPageItr = siteResource.getChildren().iterator();
     setFinalMap(finalMap, marketPageItr);
    }
   }
   
  } catch (Exception ex) {
   LOGGER.error("Error in loading the configuration map ", ex);
  }
  return finalMap;
 }

/**sets final map with all key value pairs
 * @param finalMap
 * @param marketPageItr
 */
 private void setFinalMap(Map<String, String> finalMap, Iterator<Resource> marketPageItr) {
  while (marketPageItr.hasNext()) {
   Resource chidlResource = marketPageItr.next();
   Page childConfigPage = ConfigurationHelper.getConfigPage(chidlResource);
   if (childConfigPage != null) {
    finalMap.putAll(addKeysToMap(childConfigPage));
      }
     }
}
 
 /**
  * Adds the keys to map. The method is responsible for iterating through the categories and keys saved in the properties of siteConfig node of the
  * page and adding the keys+values to a map
  *
  * @param page
  *         the page
  * @return the map
  */
 private Map<String, String> addKeysToMap(Page page) {
  Map<String, String> map = new TreeMap<String, String>();
  try {
   LOGGER.debug("Inside addKeysToMap");
   String pagePath = page.getPath();
   pagePath = pagePath.split(ConfigurationConstants.CONFIG_KEY_SPLITTER)[1];
   String[] pagePathArr = pagePath.split(CommonConstants.SLASH_CHAR_LITERAL);
   String tempKey = StringUtils.EMPTY;
   for (int i = 0; i < pagePathArr.length - 1; ++i) {
    String token = pagePathArr[i];
    tempKey = tempKey + token + CommonConstants.DOT_CHAR_LITERAL;
   }
   Iterator<Resource> itr = page.getContentResource("par") != null ? page.getContentResource("par").getChildren().iterator() : null;
   while (itr != null && itr.hasNext()) {
    Resource siteConfigComponentRes = itr.next();
    ValueMap properties = siteConfigComponentRes.getValueMap();
    String category = properties.get("category") != null ? properties.get("category").toString() : null;
    String[] configuration = ConfigurationHelper.getPropertyValueArray(properties, "configuration");
    setKeyValueMap(map, tempKey, category, configuration);
   }
  } catch (Exception e) {
   LOGGER.error("Error in adding keys to map ", e);
  }
  return map;
  
 }

/**sets map with category key value
 * @param map
 * @param tempKey
 * @param category
 * @param configuration
 */
 private void setKeyValueMap(Map<String, String> map, String tempKey, String category, String[] configuration) {
  if (category != null && configuration != null) {
   for (int i = 0; i < configuration.length; ++i) {
    JSONObject obj = new JSONObject(configuration[i]);
    String key = obj.getString(ConfigurationConstants.KEY_PROPERTY_NAME);
    String finalKey = tempKey + category + CommonConstants.DOT_CHAR_LITERAL + key;
    String configKeyType = StringUtils.contains(obj.toString(), "categoryType")?obj.getString("categoryType"):"";
    String value = "RICHTEXT".equals(configKeyType)?obj.getString("text"):obj.getString(ConfigurationConstants.VALUE_PROPERTY_NAME);
    map.put(finalKey, value);
     }
     
    }
}
}
