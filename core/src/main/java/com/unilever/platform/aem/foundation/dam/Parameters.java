/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.dam;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameters class to handle request parameter .
 */
public class Parameters {
 
 /** The Constant DEFAULT_CHARSET. */
 private static final String DEFAULT_CHARSET = "UTF-8";
 
 /** The Constant DEFAULT_THROTTLE. */
 private static final long DEFAULT_THROTTLE = 0L;
 
 /** The Constant DEFAULT_BATCH_SIZE. */
 private static final int DEFAULT_BATCH_SIZE = 1000;
 
 /** The unique property. */
 private String uniqueProperty;
 
 /** The rel src path property. */
 private String relSrcPathProperty;
 
 /** The abs target path property. */
 private String absTargetPathProperty;
 
 /** The mime type property. */
 private String mimeTypeProperty;
 
 /** The multi delimiter. */
 private String multiDelimiter;
 
 /** The file location. */
 private String fileLocation;
 
 /** The ignore properties. */
 private String[] ignoreProperties;
 
 /** The full import. */
 private boolean fullImport;
 
 /** The separator. */
 private Character separator;
 
 /** The delimiter. */
 private Character delimiter = null;
 
 /** The charset. */
 private String charset = DEFAULT_CHARSET;
 
 /** The file. */
 private InputStream file;
 
 /** The throttle. */
 private Long throttle = DEFAULT_THROTTLE;
 
 /** The batch size. */
 private int batchSize = DEFAULT_BATCH_SIZE;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(Parameters.class);
 
 /**
  * Default constructor .
  *
  * @param request
  *         the request
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 public Parameters(SlingHttpServletRequest request) throws IOException {
  
  final RequestParameter charsetParam = request.getRequestParameter("charset");
  final RequestParameter delimiterParam = request.getRequestParameter("delimiter");
  final RequestParameter fileParam = request.getRequestParameter("file");
  final RequestParameter multiDelimiterParam = request.getRequestParameter("multiDelimiter");
  final RequestParameter separatorParam = request.getRequestParameter("separator");
  final RequestParameter fileLocationParam = request.getRequestParameter("fileLocation");
  final RequestParameter fullImportParam = request.getRequestParameter("fullImport");
  final RequestParameter mimeTypePropertyParam = request.getRequestParameter("mimeTypeProperty");
  final RequestParameter absTargetPathPropertyParam = request.getRequestParameter("absTargetPathProperty");
  final RequestParameter relSrcPathPropertyParam = request.getRequestParameter("relSrcPathProperty");
  final RequestParameter uniquePropertyParam = request.getRequestParameter("uniqueProperty");
  final RequestParameter ignorePropertiesParam = request.getRequestParameter("ignoreProperties");
  final RequestParameter throttleParam = request.getRequestParameter("throttle");
  final RequestParameter batchSizeParam = request.getRequestParameter("batchSize");
  
  this.charset = DEFAULT_CHARSET;
  if (charsetParam != null) {
   this.charset = StringUtils.defaultIfEmpty(charsetParam.toString(), DEFAULT_CHARSET);
  }
  
  this.delimiter = null;
  if (delimiterParam != null && StringUtils.isNotBlank(delimiterParam.toString())) {
   this.delimiter = delimiterParam.toString().charAt(0);
  }
  
  this.separator = null;
  if (separatorParam != null && StringUtils.isNotBlank(separatorParam.toString())) {
   this.separator = separatorParam.toString().charAt(0);
  }
  
  this.multiDelimiter = "|";
  if (multiDelimiterParam != null && StringUtils.isNotBlank(multiDelimiterParam.toString())) {
   this.multiDelimiter = multiDelimiterParam.toString();
  }
  this.fullImport = true;
  if (fullImportParam != null && StringUtils.isNotBlank(fullImportParam.toString())) {
   this.fullImport = StringUtils.equalsIgnoreCase(fullImportParam.toString(), "true");
  }
  
  this.fileLocation = "/dev/null";
  if (fileLocationParam != null && StringUtils.isNotBlank(fileLocationParam.toString())) {
   this.fileLocation = fileLocationParam.toString();
  }
  
  this.mimeTypeProperty = null;
  if (mimeTypePropertyParam != null && StringUtils.isNotBlank(mimeTypePropertyParam.toString())) {
   mimeTypeProperty = StringUtils.stripToNull(mimeTypePropertyParam.toString());
  }
  
  this.absTargetPathProperty = null;
  if (absTargetPathPropertyParam != null && StringUtils.isNotBlank(absTargetPathPropertyParam.toString())) {
   this.absTargetPathProperty = StringUtils.stripToNull(absTargetPathPropertyParam.toString());
  }
  
  this.relSrcPathProperty = null;
  if (relSrcPathPropertyParam != null && StringUtils.isNotBlank(relSrcPathPropertyParam.toString())) {
   this.relSrcPathProperty = StringUtils.stripToNull(relSrcPathPropertyParam.toString());
  }
  
  this.uniqueProperty = null;
  if (uniquePropertyParam != null && StringUtils.isNotBlank(uniquePropertyParam.toString())) {
   this.uniqueProperty = StringUtils.stripToNull(uniquePropertyParam.toString());
  }
  
  this.ignoreProperties = new String[] { CsvAssetImporterServlet.TERMINATED };
  if (ignorePropertiesParam != null && StringUtils.isNotBlank(ignorePropertiesParam.toString())) {
   final String[] tmp = StringUtils.split(StringUtils.stripToNull(ignorePropertiesParam.toString()), ",");
   final List<String> list = new ArrayList<String>();
   
   for (final String t : tmp) {
    String val = StringUtils.stripToNull(t);
    if (val != null) {
     list.add(val);
    }
   }
   
   list.add(CsvAssetImporterServlet.TERMINATED);
   this.ignoreProperties = list.toArray(new String[] {});
  }
  
  if (fileParam != null && fileParam.getInputStream() != null) {
   this.file = fileParam.getInputStream();
  }
  
  this.throttle = DEFAULT_THROTTLE;
  if (throttleParam != null && StringUtils.isNotBlank(throttleParam.toString())) {
   try {
    this.throttle = Long.valueOf(throttleParam.toString());
    if (this.throttle < 0) {
     this.throttle = DEFAULT_THROTTLE;
    }
   } catch (Exception e) {
    LOGGER.debug("Throttle value not found using default value", e);
    this.throttle = DEFAULT_THROTTLE;
   }
  }
  
  batchSize = DEFAULT_BATCH_SIZE;
  if (batchSizeParam != null && StringUtils.isNotBlank(batchSizeParam.toString())) {
   try {
    this.batchSize = Integer.valueOf(batchSizeParam.toString());
    if (this.batchSize < 1) {
     this.batchSize = DEFAULT_BATCH_SIZE;
    }
   } catch (Exception e) {
    LOGGER.debug("Default Batch value not found using default value", e);
    this.batchSize = DEFAULT_BATCH_SIZE;
   }
  }
 }
 
 /**
  * Gets the unique property.
  *
  * @return the unique property
  */
 public final String getUniqueProperty() {
  return this.uniqueProperty;
 }
 
 /**
  * Gets the rel src path property.
  *
  * @return the rel src path property
  */
 public final String getRelSrcPathProperty() {
  return relSrcPathProperty;
 }
 
 /**
  * Gets the abs target path property.
  *
  * @return the abs target path property
  */
 public final String getAbsTargetPathProperty() {
  return absTargetPathProperty;
 }
 
 /**
  * Gets the mime type property.
  *
  * @return the mime type property
  */
 public final String getMimeTypeProperty() {
  return mimeTypeProperty;
 }
 
 /**
  * Gets the file location.
  *
  * @return the file location
  */
 public final String getFileLocation() {
  return fileLocation;
 }
 
 /**
  * Checks if is full import.
  *
  * @return true, if is full import
  */
 public final boolean isFullImport() {
  return fullImport;
 }
 
 /**
  * Gets the separator.
  *
  * @return the separator
  */
 public final Character getSeparator() {
  return separator;
 }
 
 /**
  * Gets the delimiter.
  *
  * @return the delimiter
  */
 public final Character getDelimiter() {
  return delimiter;
 }
 
 /**
  * Gets the charset.
  *
  * @return the charset
  */
 public final String getCharset() {
  return charset;
 }
 
 /**
  * Gets the file.
  *
  * @return the file
  */
 public final InputStream getFile() {
  return file;
 }
 
 /**
  * Gets the batch size.
  *
  * @return the batch size
  */
 public final int getBatchSize() {
  return batchSize;
 }
 
 /**
  * Gets the multi delimiter.
  *
  * @return the multi delimiter
  */
 public final String getMultiDelimiter() {
  return multiDelimiter;
 }
 
 /**
  * Gets the ignore properties.
  *
  * @return the ignore properties
  */
 public final String[] getIgnoreProperties() {
  return ignoreProperties;
 }
 
 /**
  * Gets the throttle.
  *
  * @return the throttle
  */
 public final long getThrottle() {
  return throttle;
 }
}
