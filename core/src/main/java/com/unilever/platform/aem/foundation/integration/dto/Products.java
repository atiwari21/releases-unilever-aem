/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Products.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "product" })
public class Products {
 
 /** The product. */
 @XmlElement(name = "Product")
 private List<Product> product;
 
 /**
  * Gets the product.
  *
  * @return the product
  */
 public List<Product> getProduct() {
  if (product == null) {
   product = new ArrayList<Product>();
  }
  return this.product;
 }
 
 /**
  * Sets the product.
  *
  * @param product
  *         the new product
  */
 public void setProduct(List<Product> product) {
  this.product = product;
 }
 
}
