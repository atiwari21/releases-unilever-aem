/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 * The Class NextSiblingGetter.
 */
public class NextSiblingGetter extends StringGetter {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.StringGetter#createFrom(org.json.JSONObject)
  */
 public ElementGetter createFrom(JSONObject configuration) {
  if (configuration.getString("selector") != null) {
   return this.withSelector(configuration.getString("selector"));
  }
  if (configuration.getString("attribute") != null) {
   return this.withAttribute(configuration.getString("attribute"));
  }
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.StringGetter#get(org.jsoup.nodes.Node)
  */
 public Object get(Node node) {
  String result = null;
  Elements elements = this.findAll(node, this.selector);
  if (CollectionUtils.isNotEmpty(elements)) {
   Element element = elements.get(0);
   Element siblingNode = (Element) element.nextSibling();
   if (siblingNode != null) {
    if (this.attribute != null) {
     result = siblingNode.attr(this.attribute);
    }
    result = siblingNode.text();
   }
  }
  return result;
  
 }
}
