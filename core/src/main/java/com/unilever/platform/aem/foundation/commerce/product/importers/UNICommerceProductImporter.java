/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.product.importers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONArray;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.cq.commerce.pim.api.ProductImporter;
import com.adobe.granite.workflow.launcher.ConfigEntry;
import com.day.cq.i18n.I18n;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.foundation.commerce.dto.ErrorReport;
import com.unilever.platform.foundation.commerce.dto.ResultReport;
import com.unilever.platform.foundation.commerce.ingestion.ExtractorFactory;
import com.unilever.platform.foundation.commerce.ingestion.IExtractor;
import com.unilever.platform.foundation.commerce.ingestion.ILoader;
import com.unilever.platform.foundation.commerce.ingestion.ITransformer;

/**
 * The Class UNICommerceProductImporter. This will be the entry point for each import performed for products aem. Commerce provider - unilever will be
 * used for this class.
 */
@Component(metatype = true, label = "Unilever Product Importer", description = "CSV-based product importer for Unilever")
@Service
@Properties(value = { @Property(name = Constants.SERVICE_DESCRIPTION, value = "CSV-based product importer for Unilever"),
  @Property(name = "commerceProvider", value = "unilever", propertyPrivate = true) })
public class UNICommerceProductImporter extends UNIAbstractProductImporter implements ProductImporter {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(UNICommerceProductImporter.class);
 
 /**
  * The jsonarray which will be returned by extractor after processing csv. Same array will be used by transformer and loader for validation and
  * saving the products in repository respectively.
  */
 protected JSONArray jsonarray;
 
 protected Boolean lockReleased = false;
 
 /** The Constant lockOwner. */
 private static final String LOCK_OWNER = "lockOwner";
 
 /** The Constant locked. */
 private static final String LOCKED = "locked";
 
 /** The extractor. Reference for the IExtractor class. */
 @Reference
 IExtractor extractor;
 
 /** The transformer. Reference for the ITransformer class. */
 @Reference
 ITransformer transformer;
 
 /** The loader. Reference for the ILoader class. */
 @Reference
 ILoader loader;
 
 /**
  * Disable workflow predicate.
  *
  * @param workflowConfigEntry
  *         the workflow config entry
  * @return true, if successful
  */
 @Override
 protected boolean disableWorkflowPredicate(ConfigEntry workflowConfigEntry) {
  return workflowConfigEntry.getGlob().startsWith(UNICommerceConstants.PRODUCTSROOT);
 }
 
 /**
  * Convert any input required to run the importer in to json array. It also ensures that provider, store name, store path should be available before
  * conversion.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 @Override
 protected boolean validateInput(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
  I18n i18n = new I18n(request);
  String provider = StringUtils.EMPTY;
  
  String storePath = StringUtils.EMPTY;
  String storeName = StringUtils.EMPTY;
  String lockOwner = StringUtils.EMPTY;
  String currentUser = StringUtils.EMPTY;
  Boolean locked = false;
  
  try {
   
   provider = request.getParameter(UNICommerceConstants.PROVIDER);
   storePath = request.getParameter(UNICommerceConstants.STOREPATH);
   storeName = request.getParameter(UNICommerceConstants.STORENAME);
   
   LOG.debug("provider = ", provider);
   LOG.debug("storePath = ", storePath);
   LOG.debug("storeName = ", storeName);
   
   Resource resource = request.getResourceResolver().getResource(storePath);
   
   if (null != resource) {
    
    if (resource.getValueMap().containsKey(LOCKED)) {
     locked = (Boolean) resource.getValueMap().get(LOCKED);
    }
    Node currentNode = resource.adaptTo(Node.class);
    if (resource.getValueMap().containsKey(LOCK_OWNER)) {
     lockOwner = resource.getValueMap().get(LOCK_OWNER).toString();
     currentUser = currentNode.getSession().getUserID();
    }
    if (!locked) {
     // checking the provider
     // checking store path and store name
     // convert input in to json array
     if ((!checkProvider(provider, response)) || (!checkStorePathStoreName(storePath, storeName, response)) || (!setJSONData(request, response))) {
      return false;
     }
    } else if (lockOwner.equals(currentUser)) {
     
     if ((!checkProvider(provider, response)) || (!checkStorePathStoreName(storePath, storeName, response)) || (!setJSONData(request, response))) {
      return false;
     } else {
      if (currentNode.getProperty(LOCKED) != null && currentNode.getProperty(LOCK_OWNER) != null) {
       currentNode.getProperty(LOCKED).remove();
       currentNode.getProperty(LOCK_OWNER).remove();
       currentNode.getSession().save();
       lockReleased = true;
      }
     }
    } else {
     respondWithMessages(response, i18n.get("productImport.lockedMessage") + lockOwner, null);
     return false;
    }
    
   } else {
    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, i18n.get("productImport.invalidImportPath"));
    return false;
   }
  } catch (Exception e) {
   LOG.error("Invalid product csv.", e);
   response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, i18n.get("productImport.invalidProductCsv"));
   
   return false;
  }
  
  return true;
 }
 
 /**
  * saves the products using json array in repository after successful validation against json schema.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storeRoot
  *         the store root
  * @param incrementalImport
  *         Indicates import should be applied to existing store as a delta
  * @param request
  *         the request
  * @param response
  *         the response
  * @throws RepositoryException
  *          the repository exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 @Override
 protected void doImport(ResourceResolver resourceResolver, Node storeRoot, boolean incrementalImport, SlingHttpServletRequest request,
   SlingHttpServletResponse response) throws RepositoryException, IOException {
  
  String storePath = storeRoot.getPath();
  String filePath = request.getParameter(UNICommerceConstants.CSVPATH);
  String extension = filePath != null ? filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length()) : StringUtils.EMPTY;
  boolean oldIngestion = false;
  
  if (jsonarray != null) {
   // Validating json against schema
   List<ErrorReport> errorReport = transformer.validateProducts(jsonarray, extension, request, storePath).getErrorReport();
   LOG.debug("Report after validation json array = ", errorReport);
   
   if (!errorReport.isEmpty()) {
    // print error messages in case of errors in report
    for (ErrorReport r : errorReport) {
     String indexNumber = String.valueOf(r.getIndex());
     String errorMessage = "Error : At line no. " + indexNumber + " " + r.getErrorMessage();
     respondWithMessages(response, errorMessage, null);
    }
   } else {
    
    if (StringUtils.isNotBlank(extension) && ("csv").equalsIgnoreCase(extension)) {
     oldIngestion = true;
    }
    
    // saving products in repository using json array
    ResultReport report = loader.saveProducts(resourceResolver, storePath, jsonarray, oldIngestion);
    if (report != null) {
     if (lockReleased) {
      respondWithMessages(response, I18n.get(request, "productImport.lockReleased"), null);
     }
     respondWithMessages(response, report.getSummary(), report.getMessageList());
    }
   }
  }
 }
 
 /**
  * Method for checking the provider. If provider not present validation will fail.
  *
  * @param provider
  *         the provider
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private boolean checkProvider(String provider, SlingHttpServletResponse response) throws IOException {
  if (StringUtils.isEmpty(provider)) {
   response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No commerce provider specified.");
   return false;
  }
  return true;
 }
 
 /**
  * Method for checking store path & store name. Both are essential to pass the validation phase.
  *
  * @param storePath
  *         the store path
  * @param storeName
  *         the store name
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private boolean checkStorePathStoreName(String storePath, String storeName, SlingHttpServletResponse response) throws IOException {
  if (StringUtils.isEmpty(storePath) && StringUtils.isEmpty(storeName)) {
   response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Destination not specified.");
   return false;
  }
  return true;
 }
 
 /**
  * Method for setting the json array obtained from the extractor. This also checks whether provided CSV is empty or not.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * @throws RepositoryException
  *          the repository exception
  */
 private boolean setJSONData(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException, RepositoryException {
  
  ResourceResolver resourceResolver = null;
  Resource fileResource = null;
  InputStream is = null;
  Node source = null;
  
  ExtractorFactory extrFactory = new ExtractorFactory();
  
  String filePath = StringUtils.EMPTY;
  String dataPropPath = JcrConstants.JCR_CONTENT + "/" + JcrConstants.JCR_DATA;
  
  filePath = request.getParameter(UNICommerceConstants.CSVPATH);
  String extension = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());
  
  resourceResolver = request.getResourceResolver();
  fileResource = resourceResolver.getResource(filePath);
  
  if (fileResource == null) {
   respondWithMessages(response, "Error : Provided file does not exist.", null);
   return false;
  }
  
  extractor = extrFactory.getExtractor(extension);
  
  if (extractor != null) {
   extractor.setErrorReport();
   source = fileResource.adaptTo(Node.class);
   is = source.getProperty(dataPropPath).getBinary().getStream();
   
   // checking data is present in CSV or not
   jsonarray = extractor.extractData(is, request);
   LOG.debug("json array returned from the extractor = ", jsonarray);
   if (jsonarray == null) {
    respondWithMessages(response, "Error : Provided file contains headers only .", null);
    return false;
   }
   
  } else {
   respondWithMessages(response, "Error : Provided file is invalid.", null);
   return false;
  }
  
  List<ErrorReport> errorReport = extractor.getErrorReport().getErrorReport();
  if (!errorReport.isEmpty()) {
   for (ErrorReport r : errorReport) {
    respondWithMessages(response, "Error : " + r.getErrorMessage(), null);
   }
   return false;
  }
  
  return true;
 }
}
