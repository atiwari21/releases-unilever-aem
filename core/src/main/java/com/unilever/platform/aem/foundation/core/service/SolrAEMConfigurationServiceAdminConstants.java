/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

/**
 * SolrConfigurationServiceAdminConstants is responsible for defining the configurable properties for the {@link SolrConfigurationService}.
 *
 */
public class SolrAEMConfigurationServiceAdminConstants {
 
 /** Proxy URL. */
 public static final String PROXY_URL = "solr.proxy.url";
 /** Allowed request handlers. */
 public static final String ALLOWED_REQUEST_HANDLERS = "solr.proxy.allowed.handlers";
 
 /** Author server details. */
 public static final String AUTHOR_SOLR_DETAILS = "author.solr.details";
 
 /** Solr publish server details. */
 public static final String PUBLISH_SOLR_DETAILS = "publish.solr.details";
 
 /** AEM Author server details. */
 public static final String AUTHOR_AEM_DETAILS = "author.aem.details";
 
 /** AEM publish server details. */
 public static final String PUBLISH_AEM_DETAILS = "publish.aem.details";
 
 /** AEM publish server details. */
 public static final String IS_INDEXING_ENABLED = "indexing.status.enabled";
 
 /** AEM publish server details. */
 public static final String INDEXING_PATH = "indexing.path";
 /** Solr core collection name. */
 public static final String COLLECTIONS = "solr.collections";
 
 /** The Constant BREADCRUMB_START_LEVEL. */
 public static final String BREADCRUMB_START_LEVEL = "breadcrumb.start.level";
 
 /** The Constant DO_NOT_INDEX_SOLR_LIST. */
 public static final String DO_NOT_INDEX_SOLR_LIST = "solr.donot.index.pages";
 
 /** The Constant AEM_PUBLISH_DOMAIN_NAME. */
 public static final String AEM_PUBLISH_DOMAIN_NAME = "aem.publish.domain.name";
 
 /** The Constant RESTRICTED_CONTENT_TYPE. */
 public static final String RESTRICTED_CONTENT_TYPE = "restricted.contentType";
 
 /** The Constant COMMIT_WITHIN. */
 public static final String COMMIT_WITHIN = "commitWithinMS";
 
 /** The Constant INSTANT_COMMIT */
 public static final String SOFT_COMMIT = "softCommit";
 
 /** The Constant OBSERVATION_ENABLED */
 public static final String OBSERVATION_ENABLED = "observationEnabled";
 
 /**
  * Instantiates a new solr aem configuration service admin constants.
  */
 private SolrAEMConfigurationServiceAdminConstants() {
  
 }
 
}
