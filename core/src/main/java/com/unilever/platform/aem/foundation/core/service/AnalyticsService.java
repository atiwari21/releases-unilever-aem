/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.util.Map;

import javax.jcr.Node;

import com.day.cq.wcm.api.Page;

/**
 * The Interface SearchService.
 *
 */
public interface AnalyticsService {
 
 /**
  * This method retrieve the Cloud services configurations map for the site.
  *
  * @param currentPage
  *         the current page
  * @param currentNode
  *         the current node
  * @return Analytics JS path
  */
 public Map<String, String> getAnalyticsConfiguration(Page currentPage, Node currentNode);
 
 /**
  * Gets the analytics basic properties.
  *
  * @param currentPage
  *         the current page
  * @return the analytics basic properties
  */
 public Map<String, String> getAnalyticsBasicProperties(Page currentPage);
 
 /**
  * Gets the analytics values.
  *
  * @param currentPage
  *         the current page
  * @param category
  *         the category
  * @return the analytics values
  */
 public Map<String, String> getAnalyticsValues(Page currentPage, String category);
}
