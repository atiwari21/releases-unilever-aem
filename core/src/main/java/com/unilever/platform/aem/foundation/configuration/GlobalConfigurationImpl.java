/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Class GlobalConfigurationImpl.
 *
 */
@Component(immediate = true, metatype = false)
@Service(value = GlobalConfiguration.class)
public class GlobalConfigurationImpl implements GlobalConfiguration {
 
 /** The Constant OSGI_CONFIG_FETCH_ERROR. */
 private static final String OSGI_CONFIG_FETCH_ERROR = "Error occured in fetching osgi config";
 
 /** The Constant COMMON_GLOBAL_CONFIG_PID. */
 private static final String COMMON_GLOBAL_CONFIG_PID = "com.unilever.platform.aem.foundation.configuration.CommonGlobalConfigurationImpl";
 
 /** The config admin. */
 @Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY, policy = ReferencePolicy.STATIC)
 private ConfigurationAdmin configAdmin;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationServiceImpl.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getPageConfigurations(com.unilever.platform.aem.foundation.core.dto.
  * BrandMarketBean)
  */
 @Override
 public Map<String, Map<String, String>> getPageConfigurations(BrandMarketBean brandMarketBean) {
  return null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getCategoryConfiguration(com.unilever.platform.aem.foundation.core.dto.
  * BrandMarketBean, java.lang.String)
  */
 @Override
 public Map<String, String> getCategoryConfiguration(BrandMarketBean brandMarketBean, String category) {
  return null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getConfigValue(com.unilever.platform.aem.foundation.core.dto.
  * BrandMarketBean, java.lang.String, java.lang.String)
  */
 @Override
 public String getConfigValue(BrandMarketBean brandMarketBean, String category, String key) {
  return null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getConfigValue(java.lang.String)
  */
 @Override
 public String getConfigValue(String key) {
  try {
   Configuration conf = configAdmin.getConfiguration(this.getClass().getName());
   if (conf != null && null != conf.getProperties() && null != conf.getProperties().get(key)) {
    return conf.getProperties().get(key).toString();
   }
  } catch (Exception e) {
   LOGGER.error(OSGI_CONFIG_FETCH_ERROR, e);
  }
  return "";
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getConfigValueArray(java.lang.String)
  */
 @Override
 public String[] getConfigValueArray(String key) {
  try {
   Configuration conf = configAdmin.getConfiguration(this.getClass().getName());
   if (conf != null && null != conf.getProperties() && null != conf.getProperties().get(key)) {
    return (String[]) conf.getProperties().get(key);
   }
  } catch (Exception e) {
   LOGGER.error("Error occured in fetching osgi config array", e);
  }
  return ArrayUtils.EMPTY_STRING_ARRAY;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getCommonGlobalConfigValueArray(java.lang.String)
  */
 @Override
 public String[] getCommonGlobalConfigValueArray(String key) {
  try {
   Configuration conf = configAdmin.getConfiguration(COMMON_GLOBAL_CONFIG_PID);
   if (conf != null && null != conf.getProperties() && null != conf.getProperties().get(key)) {
    return (String[]) conf.getProperties().get(key);
   }
  } catch (Exception e) {
   LOGGER.error("Error occured in fetching osgi config array", e);
  }
  return ArrayUtils.EMPTY_STRING_ARRAY;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getConfigMap()
  */
 @SuppressWarnings({ "rawtypes" })
 @Override
 public Dictionary getConfigMap() {
  Configuration conf = null;
  try {
   conf = configAdmin.getConfiguration(this.getClass().getName());
   
  } catch (IOException e) {
   LOGGER.error(OSGI_CONFIG_FETCH_ERROR, e);
  }
  
  return conf.getProperties();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#valueOf(java.util.Dictionary)
  */
 @Override
 public <K, V> Map<K, V> valueOf(Dictionary<K, V> dictionary) {
  if (dictionary == null) {
   return null;
  }
  Map<K, V> map = new HashMap<K, V>(dictionary.size());
  Enumeration<K> keys = dictionary.keys();
  while (keys.hasMoreElements()) {
   K key = keys.nextElement();
   map.put(key, dictionary.get(key));
  }
  return map;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getCommonGlobalConfigMap()
  */
 @SuppressWarnings({ "rawtypes" })
 @Override
 public Dictionary getCommonGlobalConfigMap() {
  Configuration conf = null;
  try {
   conf = configAdmin.getConfiguration(COMMON_GLOBAL_CONFIG_PID);
   
  } catch (IOException e) {
   LOGGER.error(OSGI_CONFIG_FETCH_ERROR, e);
  }
  
  return conf.getProperties();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.GlobalConfiguration#getCommonGlobalConfigValue(java.lang.String)
  */
 @Override
 public String getCommonGlobalConfigValue(String key) {
  try {
   Configuration conf = configAdmin.getConfiguration(COMMON_GLOBAL_CONFIG_PID);
   if (conf != null && null != conf.getProperties() && null != conf.getProperties().get(key)) {
    return conf.getProperties().get(key).toString();
   }
  } catch (Exception e) {
   LOGGER.error(OSGI_CONFIG_FETCH_ERROR, e);
  }
  return "";
 }
 
}
