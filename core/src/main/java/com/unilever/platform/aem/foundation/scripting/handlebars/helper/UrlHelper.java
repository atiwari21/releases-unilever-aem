/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * The Class UrlHelper.
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class UrlHelper implements HandlebarsHelperService<Object> {
 /**
  * Logger Instance.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(UrlHelper.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new UrlHelper();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "urlHelper";
 
 /**
  * The constant slash.
  */
 private static final String SLASH = "/";
 
 /**
  * The constant .html.
  */
 private static final String DOT_HTML = ".html";
 
 /**
  * The constant .html.
  */
 private static final String HTTP = "http";
 
 /**
  * The constant .html.
  */
 private static final String COLON_SLASH_SLASH = "://";
 
 /**
  * The constant .html.
  */
 private static final String SLASH_CONTENT_SLASH = "/content/";
 
 /**
  * The integer zero.
  */
 private static final int ZERO = 0;
 
 /**
  * The integer two.
  */
 private static final int TWO = 2;
 
 /**
  * The integer four.
  */
 private static final int FOUR = 4;
 
 /**
  * The integer five.
  */
 private static final int FIVE = 5;
 
 /**
  * The integer seven.
  */
 private static final int SEVEN = 7;
 
 @Override
 public CharSequence apply(Object path, Options options) throws IOException {
  CharSequence chars = null;
  String prototypeURL = null;
  String[] splitByHtml = null;
  if (path != null) {
   LOGGER.debug("path = ", path.toString());
   prototypeURL = path.toString();
   String[] splitBySlash = prototypeURL.split(SLASH);
   if (splitBySlash.length > FOUR && (prototypeURL.contains(HTTP) && prototypeURL.contains(COLON_SLASH_SLASH))) {
    if (prototypeURL.contains(SLASH_CONTENT_SLASH)) {
     splitByHtml = splitBySlash[SEVEN].split(DOT_HTML);
     chars = splitByHtml[ZERO];
    } else {
     splitByHtml = splitBySlash[FOUR].split(DOT_HTML);
     chars = splitByHtml[ZERO];
    }
   } else if (splitBySlash.length > TWO && !(prototypeURL.contains(HTTP) && prototypeURL.contains(COLON_SLASH_SLASH))) {
    if (prototypeURL.contains(SLASH_CONTENT_SLASH)) {
     splitByHtml = splitBySlash[FIVE].split(DOT_HTML);
     chars = splitByHtml[ZERO];
    } else {
     splitByHtml = splitBySlash[TWO].split(DOT_HTML);
     chars = splitByHtml[ZERO];
    }
    
   }
  }
  return chars;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#setCache(com.sapient.platform.iea.aem.scripting.
  * handlebars.api.HandlebarsTemplateCache)
  */
 @Override
 public void setCache(HandlebarsTemplateCache templateCache) {
  LOGGER.info("Cache is not implemented for renderTemplate handler.");
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getName()
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getHelper()
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
}
