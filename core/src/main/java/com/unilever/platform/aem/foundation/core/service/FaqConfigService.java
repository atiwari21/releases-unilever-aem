/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

/**
 * The Interface FaqConfigService
 * 
 * @author asri54
 *
 */
public interface FaqConfigService {
 
 /**
  * Gets the Web Service URL
  * 
  * @return the faq wen service URL
  */
 public String getWebServiceUrl();
 
}
