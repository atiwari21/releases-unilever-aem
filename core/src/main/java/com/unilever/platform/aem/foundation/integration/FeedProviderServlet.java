/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.engine.SlingRequestProcessor;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.unilever.platform.aem.foundation.constants.SearchConstants;

/**
 * The FeedProvider Servlet
 */
@SlingServlet(resourceTypes = { SearchConstants.SLING_SERVLET_RESOURCETYPE_VALUE }, methods = { HttpConstants.METHOD_GET }, selectors = {
  "feeds" }, extensions = { "xml", "json" })
public class FeedProviderServlet extends SlingSafeMethodsServlet {
 
 private static final long serialVersionUID = 4129546353450166501L;
 
 private static final Logger LOGGER = LoggerFactory.getLogger(FeedProviderServlet.class);
 
 private static final String XML_TEXT = "xml";
 
 @Reference
 RequestResponseFactory requestResponseFactory;
 @Reference
 SlingRequestProcessor requestProcessor;
 
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
  LOGGER.info("Feed Provider Servlet");
  String requestPath = request.getRequestPathInfo().getResourcePath();
  String contentPath = requestPath + "/" + JcrConstants.JCR_CONTENT;
  String extension = request.getRequestPathInfo().getExtension();
  ResourceResolver resourceResolver = request.getResourceResolver();
  Resource resource = resourceResolver.getResource(contentPath);
  ValueMap properties = null;
  if (resource != null) {
   properties = resource.adaptTo(ValueMap.class);
  }
  
  String feedOutput = null;
  Feed feed = null;
  
  try {
   feed = getFeed(request, response);
   feed.printHeader(properties);
   feed.printChildEntries();
   TransformerFactory transformerFactory = TransformerFactory.newInstance();
   DOMSource source = new DOMSource(feed.getDoc());
   Transformer transformer = transformerFactory.newTransformer();
   StringWriter writer = new StringWriter();
   transformer.transform(source, new StreamResult(writer));
   feedOutput = writer.getBuffer().toString();
  } catch (TransformerConfigurationException e) {
   LOGGER.error("TransformerConfigurationException {}", e);
  } catch (TransformerException e) {
   LOGGER.error("TransformerException {}", e);
  }
  
  if (XML_TEXT.equals(extension)) {
   response.setContentType(feed.getContentType());
   response.setCharacterEncoding(feed.getCharacterEncoding());
   response.getWriter().write(feedOutput);
  } else {
   JSONObject jsonFeed = XML.toJSONObject(feedOutput);
   response.getWriter().write(jsonFeed.toString());
  }
  
 }
 
 /**
  * @param req
  *         the request
  * @param resp
  *         the response
  * @return the feed
  */
 protected Feed getFeed(SlingHttpServletRequest req, SlingHttpServletResponse resp) {
  String[] sels = req.getRequestPathInfo().getSelectors();
  Feed feed = new AtomFeed(req);
  if (sels.length > 1) {
   String selVal = sels[1];
   switch (selVal) {
    case "rss":
     feed = new RssFeed(req);
     break;
    case "instantfeed":
     feed = new InstantArticleFeed(req, resp, requestResponseFactory, requestProcessor);
     break;
    default:
     break;
   }
  }
  return feed;
 }
}
