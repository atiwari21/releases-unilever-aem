/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

/**
 * Class SearchParam
 */
public class SearchParam {
 
 /** The offset. */
 private int offset;
 /** The limit. */
 private int limit;
 /** The sortOrder. */
 private String sortOrder;
 
 /**
  * Public Constructor
  * 
  * @param offset
  *         the offset
  * @param limit
  *         the limit
  * @param sortOrder
  *         the sortOrder
  */
 public SearchParam(int offset, int limit, String sortOrder) {
  super();
  this.offset = offset;
  this.limit = limit;
  this.sortOrder = sortOrder;
 }
 
 /**
  * @return the offset
  */
 public int getOffset() {
  return offset;
 }
 
 /**
  * @param offset
  *         the offset to set
  */
 public void setOffset(int offset) {
  this.offset = offset;
 }
 
 /**
  * @return the limit
  */
 public int getLimit() {
  return limit;
 }
 
 /**
  * @param limit
  *         the limit to set
  */
 public void setLimit(int limit) {
  this.limit = limit;
 }
 
 /**
  * @return the sortOrder
  */
 public String getSortOrder() {
  return sortOrder;
 }
 
 /**
  * @param sortOrder
  *         the sortOrder to set
  */
 public void setSortOrder(String sortOrder) {
  this.sortOrder = sortOrder;
 }
 
}
