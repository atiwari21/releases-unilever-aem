/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.H1;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class HeaderTitleRule.
 */
public class HeaderTitleRule extends ConfigurationSelectorRule {
 
 /**
  * Instantiates a new header title rule.
  */
 private HeaderTitleRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the header title rule
  */
 public static HeaderTitleRule create() {
  return new HeaderTitleRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the header title rule
  */
 public static HeaderTitleRule createFrom(JSONObject configuration) {
  return (HeaderTitleRule) create().withSelector(configuration.getString("selector"));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(java.lang.Object)
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer , com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Header header = Header.create();
  if (container instanceof InstantArticle) {
   header = ((InstantArticle) container).getHeader();
  } else if (container instanceof Header) {
   header = ((Header) container);
  }
  H1 h1 = H1.create();  
  return header.withTitle(transformer.transform(h1, node));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Header.getClassName() };
 }
 
}
