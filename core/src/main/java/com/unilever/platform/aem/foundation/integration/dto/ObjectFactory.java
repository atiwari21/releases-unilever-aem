/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * com.unilever.platform.aem.foundation.integration.dto package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML
 * content can consist of schema derived interfaces and classes representing the binding of schema type definitions, element declarations and model
 * groups. Factory methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
 
 /**
  * Instantiates a new object factory.
  */
 public ObjectFactory() {
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the feed
  */
 public Feed createFeed() {
  return new Feed();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the products
  */
 public Products createFeedProducts() {
  return new Products();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the product
  */
 public Product createFeedProductsProduct() {
  return new Product();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the image urls
  */
 public ImageUrls createFeedProductsProductImageUrls() {
  return new ImageUrls();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the page urls
  */
 public PageUrls createFeedPageUrls() {
  return new PageUrls();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the descriptions
  */
 public Descriptions createFeedProductsProductDescriptions() {
  return new Descriptions();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the names
  */
 public Names createFeedNames() {
  return new Names();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the identifiers
  */
 public Identifiers createFeedProductsProductIdentifiers() {
  return new Identifiers();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the categories
  */
 public Categories createFeedCategories() {
  return new Categories();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the category
  */
 public Category createFeedCategoriesCategory() {
  return new Category();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the source id
  */
 public SourceId createFeedProductsProductSourceId() {
  return new SourceId();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the product category
  */
 public ProductCategory createFeedProductsProductCategory() {
  return new ProductCategory();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the description
  */
 public Description createFeedProductsProductDescriptionsDescription() {
  return new Description();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the name
  */
 public Name createFeedName() {
  return new Name();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the identifier
  */
 public Identifier createFeedProductsProductIdentifiersIdentifier() {
  return new Identifier();
 }
 
 /**
  * Creates a new Object object.
  *
  * @return the url
  */
 public Url createFeedUrl() {
  return new Url();
 }
}
