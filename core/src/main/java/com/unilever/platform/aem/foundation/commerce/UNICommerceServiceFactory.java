/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.References;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.settings.SlingSettingsService;

import com.adobe.cq.commerce.api.CommerceService;
import com.adobe.cq.commerce.api.CommerceServiceFactory;
import com.adobe.cq.commerce.common.AbstractJcrCommerceServiceFactory;
import com.adobe.cq.commerce.common.CommerceSearchProviderManager;
import com.adobe.granite.security.user.UserPropertiesService;
import com.day.cq.wcm.api.LanguageManager;
import com.unilever.platform.aem.foundation.commerce.api.impl.UNICommerceServiceImpl;

/**
 * A factory for creating UnileverCommerceService objects.
 */
@Component
@Service
@Properties({
  @Property(name = org.osgi.framework.Constants.SERVICE_DESCRIPTION, value = { "Factory for reference implementation Unilever commerce services" }),
  @Property(name = "commerceProvider", value = { "unilever" }, propertyPrivate = true) })
@References({
  @Reference(name = "languageManager", referenceInterface = LanguageManager.class, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, policy = ReferencePolicy.STATIC, bind = "bindLanguageManager", unbind = "unbindLanguageManager"),
  @Reference(name = "userPropertiesService", referenceInterface = UserPropertiesService.class, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, policy = ReferencePolicy.STATIC, bind = "bindUserPropertiesService", unbind = "unbindUserPropertiesService"),
  @Reference(name = "slingSettingsService", referenceInterface = SlingSettingsService.class, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, policy = ReferencePolicy.STATIC, bind = "bindSlingSettingsService", unbind = "unbindSlingSettingsService"),
  @Reference(name = "slingRepository", referenceInterface = SlingRepository.class, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, policy = ReferencePolicy.STATIC, bind = "bindSlingRepository", unbind = "unbindSlingRepository"),
  @Reference(name = "searchProviderManager", referenceInterface = CommerceSearchProviderManager.class, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, policy = ReferencePolicy.STATIC, bind = "bindSearchProviderManager", unbind = "unbindSearchProviderManager") })
/**
 * UNI implementation of commerce services factory class
 *
 */
public class UNICommerceServiceFactory extends AbstractJcrCommerceServiceFactory implements CommerceServiceFactory {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.CommerceServiceFactory#getCommerceService(org .apache.sling.api.resource.Resource)
  */
 @Override
 public CommerceService getCommerceService(Resource resource) {
  return new UNICommerceServiceImpl(getServiceContext(), resource);
 }
}
