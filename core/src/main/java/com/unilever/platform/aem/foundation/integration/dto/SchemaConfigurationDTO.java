/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import org.apache.commons.lang.StringUtils;

/**
 * The Class SchemaConfigurationDTO.
 */
public class SchemaConfigurationDTO {
 
 /** The pim field description. */
 String pimFieldDescription;
 
 /** The field name. */
 String fieldName;
 
 /** The description. */
 String description;
 
 /** The column. */
 String column;
 
 /** The is applicable. */
 String isApplicable;
 
 /** The mandatory. */
 String mandatory;
 
 /** The mandatory default value. */
 String mandatoryDefaultValue;
 
 /** The mandatory variant. */
 String mandatoryVariant;
 
 /** The unique values. */
 String uniqueValues;
 
 /** The data must contain. */
 String dataMustContain;
 
 /** The min length. */
 String minLength;
 
 /** The max length. */
 String maxLength;
 
 /** The read only. */
 String readOnly;
 
 /** The regex. */
 String regex;
 
 /**
  * Gets the pim field description.
  *
  * @return the pim field description
  */
 public String getPimFieldDescription() {
  return pimFieldDescription;
 }
 
 /**
  * Sets the pim field description.
  *
  * @param pimFieldDescription
  *         the new pim field description
  */
 public void setPimFieldDescription(String pimFieldDescription) {
  this.pimFieldDescription = pimFieldDescription;
 }
 
 /**
  * Gets the field name.
  *
  * @return the field name
  */
 public String getFieldName() {
  return fieldName;
 }
 
 /**
  * Sets the field name.
  *
  * @param fieldName
  *         the new field name
  */
 public void setFieldName(String fieldName) {
  this.fieldName = fieldName;
 }
 
 /**
  * Gets the description.
  *
  * @return the description
  */
 public String getDescription() {
  return description;
 }
 
 /**
  * Sets the description.
  *
  * @param description
  *         the new description
  */
 public void setDescription(String description) {
  this.description = description;
 }
 
 /**
  * Gets the column.
  *
  * @return the column
  */
 public String getColumn() {
  return column;
 }
 
 /**
  * Sets the column.
  *
  * @param column
  *         the new column
  */
 public void setColumn(String column) {
  this.column = column;
 }
 
 /**
  * Gets the checks if is applicable.
  *
  * @return the checks if is applicable
  */
 public String getIsApplicable() {
  return isApplicable;
 }
 
 /**
  * Sets the checks if is applicable.
  *
  * @param isApplicable
  *         the new checks if is applicable
  */
 public void setIsApplicable(String isApplicable) {
  this.isApplicable = isTrue(isApplicable);
 }
 
 /**
  * Gets the mandatory.
  *
  * @return the mandatory
  */
 public String getMandatory() {
  return mandatory;
 }
 
 /**
  * Sets the mandatory.
  *
  * @param mandatory
  *         the new mandatory
  */
 public void setMandatory(String mandatory) {
  this.mandatory = isTrue(mandatory);
 }
 
 /**
  * Gets the mandatory default value.
  *
  * @return the mandatory default value
  */
 public String getMandatoryDefaultValue() {
  return mandatoryDefaultValue;
 }
 
 /**
  * Sets the mandatory default value.
  *
  * @param mandatoryDefaultValue
  *         the new mandatory default value
  */
 public void setMandatoryDefaultValue(String mandatoryDefaultValue) {
  this.mandatoryDefaultValue = mandatoryDefaultValue;
 }
 
 /**
  * Gets the mandatory variant.
  *
  * @return the mandatory variant
  */
 public String getMandatoryVariant() {
  return mandatoryVariant;
 }
 
 /**
  * Sets the mandatory variant.
  *
  * @param mandatoryVariant
  *         the new mandatory variant
  */
 public void setMandatoryVariant(String mandatoryVariant) {
  this.mandatoryVariant = isTrue(mandatoryVariant);
 }
 
 /**
  * Gets the unique values.
  *
  * @return the unique values
  */
 public String getUniqueValues() {
  return uniqueValues;
 }
 
 /**
  * Sets the unique values.
  *
  * @param uniqueValues
  *         the new unique values
  */
 public void setUniqueValues(String uniqueValues) {
  this.uniqueValues = isTrue(uniqueValues);
 }
 
 /**
  * Gets the data must contain.
  *
  * @return the data must contain
  */
 public String getDataMustContain() {
  return dataMustContain;
 }
 
 /**
  * Sets the data must contain.
  *
  * @param dataMustContain
  *         the new data must contain
  */
 public void setDataMustContain(String dataMustContain) {
  this.dataMustContain = dataMustContain;
 }
 
 /**
  * Gets the min length.
  *
  * @return the min length
  */
 public String getMinLength() {
  return minLength;
 }
 
 /**
  * Sets the min length.
  *
  * @param minLength
  *         the new min length
  */
 public void setMinLength(String minLength) {
  this.minLength = (!"0".equals(minLength)) ? minLength : StringUtils.EMPTY;
 }
 
 /**
  * Gets the max length.
  *
  * @return the max length
  */
 public String getMaxLength() {
  return maxLength;
 }
 
 /**
  * Sets the max length.
  *
  * @param maxLength
  *         the new max length
  */
 public void setMaxLength(String maxLength) {
  this.maxLength = (!"0".equals(maxLength)) ? maxLength : StringUtils.EMPTY;
 }
 
 /**
  * Gets the read only.
  *
  * @return the read only
  */
 public String getReadOnly() {
  return readOnly;
 }
 
 /**
  * Sets the read only.
  *
  * @param readOnly
  *         the new read only
  */
 public void setReadOnly(String readOnly) {
  this.readOnly = isTrue(readOnly);
 }
 
 /**
  * Gets the regex.
  *
  * @return the regex
  */
 public String getRegex() {
  return regex;
 }
 
 /**
  * Sets the regex.
  *
  * @param regex
  *         the new regex
  */
 public void setRegex(String regex) {
  this.regex = regex;
 }
 
 /**
  * Checks if is true.
  *
  * @param str
  *         the str
  * @return the string
  */
 private String isTrue(String str) {
  if ("true".equalsIgnoreCase(str)) {
   return "Y";
  } else {
   return "N";
  }
 }
}
