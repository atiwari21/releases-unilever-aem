/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AssignAssetPathServlet
 *
 */
@SlingServlet(label = "Unilever assign base asset path", paths = { "/bin/assignbaseassetpath" }, methods = { HttpConstants.METHOD_POST })
public class AssignAssetPathServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 782708002977083730L;
 
 /** The Constant STORE_PATH. */
 private static final String STORE_PATH = "storePath";
 
 /** The Constant GLOBAL_ASSET_BASE_PATH. */
 private static final String GLOBAL_ASSET_BASE_PATH = "globalAssetBasePath";
 
 /** The Constant LOCAL_ASSET_BASE_PATH. */
 private static final String LOCAL_ASSET_BASE_PATH = "localAssetBasePath";
 
 private static final String IMAGE_FORMAT = "imageFormat";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AssignAssetPathServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  
  ResourceResolver resourseResolver = request.getResource().getResourceResolver();
  
  String currentPagPath = request.getParameter(STORE_PATH).toString();
  String globalAssetBasePath = request.getParameter(GLOBAL_ASSET_BASE_PATH).toString();
  String localAssetBasePath = request.getParameter(LOCAL_ASSET_BASE_PATH).toString();
  String imageFormat = request.getParameter(IMAGE_FORMAT).toString();
  Session session = resourseResolver.adaptTo(Session.class);
  
  try {
   Node currentPageProperties = session.getNode(currentPagPath);
   currentPageProperties.setProperty("globalAssetBasePath", globalAssetBasePath);
   currentPageProperties.setProperty("localAssetBasePath", localAssetBasePath);
   currentPageProperties.setProperty("imageFormat", imageFormat);
   session.save();
  } catch (RepositoryException re) {
   LOGGER.error("Error while setting property on folder", re);
  }
  response.sendRedirect("/aem/products.html" + currentPagPath);
 }
 
}
