/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api;

/**
 * The Class RecipientsInfo.
 */
public class RecipientsInfo {
 
 /** The type. */
 private String type;
 
 /** The character count. */
 private long characterCount;
 
 /**
  * Gets the type.
  *
  * @return the type
  */
 public String getType() {
  return type;
 }
 
 /**
  * Gets the character count.
  *
  * @return the character count
  */
 public long getCharacterCount() {
  return characterCount;
 }
 
 /**
  * Sets the type.
  *
  * @param type
  *         the new type
  */
 public void setType(String type) {
  this.type = type;
 }
 
 /**
  * Sets the character count.
  *
  * @param characterCount
  *         the new character count
  */
 public void setCharacterCount(long characterCount) {
  this.characterCount = characterCount;
 }
 
}
