/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.product.importers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.pim.api.ProductImporter;
import com.adobe.granite.workflow.launcher.ConfigEntry;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.helper.URLConnectionHelper;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;
import com.unilever.platform.aem.foundation.utils.TokenUtility;
import com.unilever.platform.foundation.commerce.dto.ErrorReport;
import com.unilever.platform.foundation.commerce.dto.ResultReport;
import com.unilever.platform.foundation.commerce.ingestion.IExtractor;
import com.unilever.platform.foundation.commerce.ingestion.ILoader;
import com.unilever.platform.foundation.commerce.ingestion.ITransformer;
import com.unilever.platform.foundation.commerce.ingestion.product.RewardsExcelExtractor;

@Component(metatype = true, label = "Unilever Rewards Importer", description = "Reward importer for Unilever D2 Platform")
@Service
@Properties(value = { @Property(name = Constants.SERVICE_DESCRIPTION, value = "Reward importer for Unilever D2 Platform"),
  @Property(name = "commerceProvider", value = "rewards", propertyPrivate = true) })
public class UNIRewardsCommerceProductImporter extends UNIAbstractProductImporter implements ProductImporter {
 
 private static final String CONFIG_PAGE_PATH = "configPagePath";
 
 private static final Logger LOGGER = LoggerFactory.getLogger(UNIRewardsCommerceProductImporter.class);
 
 protected JSONArray jsonarray;
 
 protected Boolean lockReleased = false;
 
 private static final String LOCK_OWNER = "lockOwner";
 
 private static final String LOCKED = "locked";
 
 @Reference
 ConfigurationService configurationService;
 
 /** The extractor. Reference for the IExtractor class. */
 @Reference
 IExtractor extractor;
 
 /** The transformer. Reference for the ITransformer class. */
 @Reference
 ITransformer transformer;
 
 /** The loader. Reference for the ILoader class. */
 @Reference
 ILoader loader;
 
 @Reference
 HttpClientBuilderFactory factory;
 
 /**
  * Disable workflow predicate.
  *
  * @param workflowConfigEntry
  *         the workflow config entry
  * @return true, if successful
  */
 @Override
 protected boolean disableWorkflowPredicate(ConfigEntry workflowConfigEntry) {
  return workflowConfigEntry.getGlob().startsWith(UNICommerceConstants.PRODUCTSROOT);
 }
 
 /**
  * Convert any input required to run the importer in to json array. It also ensures that provider, store name, store path should be available before
  * conversion.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 @Override
 protected boolean validateInput(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
  I18n i18n = new I18n(request);
  String provider = StringUtils.EMPTY;
  
  String storePath = StringUtils.EMPTY;
  String storeName = StringUtils.EMPTY;
  String lockOwner = StringUtils.EMPTY;
  String currentUser = StringUtils.EMPTY;
  Boolean locked = false;
  
  try {
   
   provider = request.getParameter(UNICommerceConstants.PROVIDER);
   storePath = request.getParameter(UNICommerceConstants.STOREPATH);
   storeName = request.getParameter(UNICommerceConstants.STORENAME);
   
   LOGGER.debug("provider = ", provider);
   LOGGER.debug("storePath = ", storePath);
   LOGGER.debug("storeName = ", storeName);
   
   Resource resource = request.getResourceResolver().getResource(storePath);
   
   if (null != resource) {
    
    if (resource.getValueMap().containsKey(LOCKED)) {
     locked = (Boolean) resource.getValueMap().get(LOCKED);
    }
    Node currentNode = resource.adaptTo(Node.class);
    if (resource.getValueMap().containsKey(LOCK_OWNER)) {
     lockOwner = resource.getValueMap().get(LOCK_OWNER).toString();
     currentUser = currentNode.getSession().getUserID();
    }
    currentNode.setProperty("schemaPath", "/content/unilever/globalRewardSchema");
    currentNode.getSession().save();
    if (!locked) {
     // checking the provider
     // checking store path and store name
     // convert input in to json array
     if ((!checkProvider(provider, response)) || (!checkStorePathStoreName(storePath, storeName, response)) || (!setJSONData(request, response))) {
      return false;
     }
    } else if (lockOwner.equals(currentUser)) {
     
     if ((!checkProvider(provider, response)) || (!checkStorePathStoreName(storePath, storeName, response)) || (!setJSONData(request, response))) {
      return false;
     } else {
      if (currentNode.getProperty(LOCKED) != null && currentNode.getProperty(LOCK_OWNER) != null) {
       currentNode.getProperty(LOCKED).remove();
       currentNode.getProperty(LOCK_OWNER).remove();
       currentNode.getSession().save();
       lockReleased = true;
      }
     }
    } else {
     respondWithMessages(response, i18n.get("productImport.lockedMessage") + lockOwner, null);
     return false;
    }
    
   } else {
    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, i18n.get("productImport.invalidImportPath"));
    return false;
   }
  } catch (Exception e) {
   LOGGER.error("Invalid product csv.", e);
   response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, i18n.get("productImport.invalidProductCsv"));
   
   return false;
  }
  
  return true;
 }
 
 private Map<String, String> getRewardConfigMap(ResourceResolver resourceResolver, String storePath) {
  Map<String, String> rewardConfigMap = new HashMap<String, String>();
  String configPagePath = StringUtils.EMPTY;
  Resource folderResource = resourceResolver.getResource(storePath);
  PageManager pgMgr = resourceResolver.adaptTo(PageManager.class);
  if (null != folderResource && folderResource.getValueMap().containsKey(CONFIG_PAGE_PATH) && pgMgr != null) {
   configPagePath = folderResource.getValueMap().get(CONFIG_PAGE_PATH, String.class);
   Page configPage = pgMgr.getPage(configPagePath);
   if (configPage != null && StringUtils.isNotBlank(configPagePath)) {
    rewardConfigMap = GlobalConfigurationUtility.getMapFromConfiguration(configurationService, configPage, "rewardConfig");
   }
  } else {
   rewardConfigMap = null;
  }
  return rewardConfigMap;
 }
 
 /**
  * saves the products using json array in repository after successful validation against json schema.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param storeRoot
  *         the store root
  * @param incrementalImport
  *         Indicates import should be applied to existing store as a delta
  * @param request
  *         the request
  * @param response
  *         the response
  * @throws RepositoryException
  *          the repository exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 @Override
 protected void doImport(ResourceResolver resourceResolver, Node storeRoot, boolean incrementalImport, SlingHttpServletRequest request,
   SlingHttpServletResponse response) throws RepositoryException, IOException {
  
  String storePath = storeRoot.getPath();
  String filePath = request.getParameter(UNICommerceConstants.CSVPATH);
  String extension = filePath != null ? filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length()) : StringUtils.EMPTY;
  Map<String, String> rewardConfigMap = getRewardConfigMap(resourceResolver, storePath);
  
  if (jsonarray != null && rewardConfigMap != null) {
   // Validating json against schema
   List<ErrorReport> errorReport = transformer.validateProducts(jsonarray, extension, request, storePath).getErrorReport();
   LOGGER.debug("Report after validation json array = ", errorReport);
   
   if (!errorReport.isEmpty()) {
    // print error messages in case of errors in report
    for (ErrorReport r : errorReport) {
     String indexNumber = String.valueOf(r.getIndex());
     String errorMessage = "Error : At line no. " + indexNumber + " " + r.getErrorMessage();
     respondWithMessages(response, errorMessage, null);
    }
   } else {
    String url = rewardConfigMap.containsKey("ingestionUrl") ? rewardConfigMap.get("ingestionUrl").toString() : StringUtils.EMPTY;
    String securityKey = rewardConfigMap.containsKey("se_key") ? rewardConfigMap.get("se_key").toString() : StringUtils.EMPTY;
    String apiToken = rewardConfigMap.containsKey("token") ? rewardConfigMap.get("token").toString() : StringUtils.EMPTY;
    String brand = rewardConfigMap.containsKey("brand") ? rewardConfigMap.get("brand").toString() : StringUtils.EMPTY;
    String entity = rewardConfigMap.containsKey("entity") ? rewardConfigMap.get("entity").toString() : StringUtils.EMPTY;
    String locale = rewardConfigMap.containsKey("locale") ? rewardConfigMap.get("locale").toString() : StringUtils.EMPTY;
    int timeout = rewardConfigMap.containsKey("timeout") ? Integer.parseInt(rewardConfigMap.get("timeout").toString()) : 5000;
    
    String token = TokenUtility.getJWTToken(securityKey, apiToken, brand, entity, locale);
    LOGGER.info("JWT Token for reward helper : " + token);
    
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("locale", locale);
    jsonObj.put("brand", brand);
    jsonObj.put("products", jsonarray);
    
    HttpResponse rewardResponse = URLConnectionHelper.getResponseFromPostRequestToURLIncludingAuthToken(url, jsonObj.toString(), factory, timeout,
      token);
    if (rewardResponse != null) {
     int responseCode = rewardResponse.getStatusLine().getStatusCode();
     LOGGER.info("response code from TS layer: " + responseCode);
     HttpEntity responseEntity = rewardResponse.getEntity();
     LOGGER.info("response entity from TS layer: " + responseEntity.toString());
     InputStream is = null;
     if (responseEntity != null) {
      is = responseEntity.getContent();
      LOGGER.info("Response data : " + responseEntity.getContent());
     }
     
     BufferedReader in = new BufferedReader(new InputStreamReader(is));
     String inputLine;
     StringBuilder response1 = new StringBuilder();
     
     while ((inputLine = in.readLine()) != null) {
      response1.append(inputLine);
      LOGGER.info("Response data : " + inputLine.getBytes());
      LOGGER.info("Response data aggregated : " + response1.toString());
     }
     in.close();
     
     JSONObject responseJson = new JSONObject(response1.toString());
     int code = responseJson.has("code") ? responseJson.getInt("code") : 500;
     String statusMessage = responseJson.has("status") ? responseJson.getString("status") : "Invalid response code retuned from TS Layer";
     if (responseCode == 200 && code == 200) {
      // saving products in repository using json array
      ResultReport report = loader.saveProducts(resourceResolver, storePath, jsonarray, false);
      if (report != null) {
       if (lockReleased) {
        respondWithMessages(response, I18n.get(request, "productImport.lockReleased"), null);
       }
       respondWithMessages(response, report.getSummary(), report.getMessageList());
      }
     } else {
      respondWithMessages(response, statusMessage, null);
     }
    }else{
     respondWithMessages(response, "Error : Some error occured while connecting with Transaction Layer.", null);
    }
   }
  } else {
   respondWithMessages(response, "Error : Home page path not assigned.", null);
  }
 }
 
 /**
  * Method for checking the provider. If provider not present validation will fail.
  *
  * @param provider
  *         the provider
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private boolean checkProvider(String provider, SlingHttpServletResponse response) throws IOException {
  if (StringUtils.isEmpty(provider)) {
   response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No commerce provider specified.");
   return false;
  }
  return true;
 }
 
 /**
  * Method for checking store path & store name. Both are essential to pass the validation phase.
  *
  * @param storePath
  *         the store path
  * @param storeName
  *         the store name
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private boolean checkStorePathStoreName(String storePath, String storeName, SlingHttpServletResponse response) throws IOException {
  if (StringUtils.isEmpty(storePath) && StringUtils.isEmpty(storeName)) {
   response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Destination not specified.");
   return false;
  }
  return true;
 }
 
 /**
  * Method for setting the json array obtained from the extractor. This also checks whether provided CSV is empty or not.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  * @throws RepositoryException
  *          the repository exception
  */
 private boolean setJSONData(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException, RepositoryException {
  
  ResourceResolver resourceResolver = null;
  Resource fileResource = null;
  InputStream is = null;
  Node source = null;
  
  String filePath = StringUtils.EMPTY;
  String dataPropPath = JcrConstants.JCR_CONTENT + "/" + JcrConstants.JCR_DATA;
  
  filePath = request.getParameter(UNICommerceConstants.CSVPATH);
  
  resourceResolver = request.getResourceResolver();
  fileResource = resourceResolver.getResource(filePath);
  
  if (fileResource == null) {
   respondWithMessages(response, "Error : Provided file does not exist.", null);
   return false;
  }
  
  extractor = PIMInjestionUtility.getServiceReference(RewardsExcelExtractor.class);
  
  if (extractor != null) {
   extractor.setErrorReport();
   source = fileResource.adaptTo(Node.class);
   is = source.getProperty(dataPropPath).getBinary().getStream();
   
   // checking data is present in CSV or not
   jsonarray = extractor.extractData(is, request);
   LOGGER.debug("json array returned from the extractor = ", jsonarray);
   if (jsonarray == null) {
    respondWithMessages(response, "Error : Provided file contains headers only .", null);
    return false;
   }
   
  } else {
   respondWithMessages(response, "Error : Provided file is invalid.", null);
   return false;
  }
  
  List<ErrorReport> errorReport = extractor.getErrorReport().getErrorReport();
  if (!errorReport.isEmpty()) {
   for (ErrorReport r : errorReport) {
    respondWithMessages(response, "Error : " + r.getErrorMessage(), null);
   }
   return false;
  }
  
  return true;
 }
 
}
