/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Bold;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Paragraph;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

public class BoldRule extends ConfigurationSelectorRule {
 
 /**
  * Instantiates a new anchor rule.
  */
 private BoldRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor rule
  */
 public static BoldRule create() {
  return new BoldRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Bold bold = Bold.create();
  //
  if (container instanceof Paragraph) {
   ((Paragraph) container).addChild(bold);
  } else if (container instanceof InstantArticle) {
   ((InstantArticle) container).addChild(bold);
  } else {
   bold.appendText(((Element) node).html());
  }
  transformer.transform(bold, node);
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { TextContainer.getClassName(), InstantArticle.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the bold rule
  */
 public static BoldRule createFrom(JSONObject configuration) {
  BoldRule boldRule = create();
  boldRule = (BoldRule) boldRule.withSelector(configuration.getString("selector"));
  return boldRule;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
