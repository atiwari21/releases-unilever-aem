/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.json.JSONObject;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.commerce.UNICommerceConstants;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.integration.dto.SchemaConfigurationDTO;

/**
 * The Class PIMInjestionUtility.
 */
public final class PIMInjestionUtility {
 
 /** The Constant PIM_SHEET_PATH. */
 private static final String PIM_SHEET_PATH = "/content/dam/UnileverPIM/PIM_Ingestion_Sheet.xlsm";
 
 private static final String RIM_SHEET_PATH = "/content/dam/UnileverRIM/Reward_Ingestion_Sheet.xlsx";
 
 /** The Constant PIM_PRODUCT_DATA_SHEET_NAME. */
 public static final String PIM_PRODUCT_DATA_SHEET_NAME = "Product Data";
 
 public static final String RIM_REWARDS_DATA_SHEET_NAME = "Reward Data";
 
 /** The Constant PIM__MASTERCONFIGURATION_SHEET_NAME. */
 public static final String PIM_MASTERCONFIGURATION_SHEET_NAME = "Master configuration";
 
 /** The Constant SCHEMA_JCR_CONTENT_PAR. */
 public static final String SCHEMA_JCR_CONTENT_PAR = "/schema/jcr:content/par";
 
 /** The Constant TWENTY_SIX. */
 private static final int TWENTY_SIX = 26;
 
 /** The builder. */
 @Reference
 private static QueryBuilder builder;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PIMInjestionUtility.class);
 
 /**
  * Initiates PIMInjestionUtility.
  */
 private PIMInjestionUtility() {
  
 }
 
 /**
  * Gets the PIM schema configuration.
  *
  * @param schemaResource
  *         the schema resource
  * @param pimFieldMap
  *         the pim field map
  * @return the PIM schema configuration
  * @throws RepositoryException
  *          the repository exception
  */
 public static void getPIMSchemaConfiguration(Resource schemaResource, Map<String, SchemaConfigurationDTO> pimFieldMap) throws RepositoryException {
  
  Node schemaNode = schemaResource.adaptTo(Node.class);
  NodeIterator schemaNodeIterator = schemaNode.getNodes();
  
  while (schemaNodeIterator.hasNext()) {
   Node schemaConfigurationNode = schemaNodeIterator.nextNode();
   if (isPIMSchemaConfigurationNode(schemaConfigurationNode)) {
    LOGGER.info("isPIMSchemaConfigurationNode true-->");
    String pimFieldName = getAttribute(schemaConfigurationNode, "pimField");
    LOGGER.info("pimFieldName-->" + pimFieldName);
    if (StringUtils.isNotBlank(pimFieldName)) {
     SchemaConfigurationDTO attributes = new SchemaConfigurationDTO();
     
     attributes.setPimFieldDescription(getAttribute(schemaConfigurationNode, "pimFieldDescription"));
     attributes.setFieldName(pimFieldName);
     attributes.setDescription(getAttribute(schemaConfigurationNode, "description"));
     attributes.setColumn(getAttribute(schemaConfigurationNode, "column"));
     attributes.setIsApplicable(getAttribute(schemaConfigurationNode, "dataApplicableBrand"));
     attributes.setMandatory(getAttribute(schemaConfigurationNode, "mandatory"));
     attributes.setMandatoryDefaultValue(getAttribute(schemaConfigurationNode, "mandatoryDefaultValue"));
     attributes.setMandatoryVariant(getAttribute(schemaConfigurationNode, "mandatoryVariant"));
     attributes.setUniqueValues(getAttribute(schemaConfigurationNode, "uniqueValues"));
     attributes.setDataMustContain(getAttribute(schemaConfigurationNode, "dataMustContain"));
     attributes.setMinLength(getAttribute(schemaConfigurationNode, "minLength"));
     attributes.setMaxLength(getAttribute(schemaConfigurationNode, "maxLength"));
     attributes.setReadOnly(getAttribute(schemaConfigurationNode, "readOnly"));
     attributes.setRegex(getAttribute(schemaConfigurationNode, "regex"));
     
     pimFieldMap.put(pimFieldName, attributes);
     LOGGER.info("pimFieldName-->" + attributes);
    }
   }
  }
  
  LOGGER.info("pimFieldName maps-->" + pimFieldMap);
 }
 
 /**
  * Checks if is PIM schema configuration node.
  *
  * @param schemaConfiguration
  *         the schema configuration
  * @return true, if is PIM schema configuration node
  * @throws RepositoryException
  *          the repository exception
  */
 private static boolean isPIMSchemaConfigurationNode(Node schemaConfiguration) throws RepositoryException {
  return schemaConfiguration.hasProperty("componentName")
    && (("schemaConfiguration").equals(schemaConfiguration.getProperty("componentName").getValue().getString()));
 }
 
 /**
  * Gets the attribute.
  *
  * @param schemaConfigurationNode
  *         the schema configuration node
  * @param key
  *         the key
  * @return the attribute
  */
 private static String getAttribute(Node schemaConfigurationNode, String key) {
  String value = StringUtils.EMPTY;
  try {
   value = (schemaConfigurationNode.hasProperty(key)) ? schemaConfigurationNode.getProperty(key).getString() : value;
  } catch (RepositoryException e) {
   LOGGER.error("Repository exception found", e);
  }
  return value;
 }
 
 /**
  * Gets the products.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @return the products
  */
 public static List<UNIProduct> getProducts(Session session, String path) {
  
  Map<String, String> map = new HashMap<String, String>();
  List<UNIProduct> productList = new ArrayList<UNIProduct>();
  Iterator<Resource> resourceList = null;
  
  map.put("path", path);
  map.put("type", JcrConstants.NT_UNSTRUCTURED);
  map.put("property", "cq:commerceType");
  map.put("property.value", "product");
  
  builder = getServiceReference(QueryBuilder.class);
  if (builder != null) {
   resourceList = getResultfromQuery(session, map);
  }
  
  while (resourceList.hasNext()) {
   Product product = resourceList.next().adaptTo(Product.class);
   UNIProduct uniProduct = getUniProduct(product);
   
   if (uniProduct != null) {
    productList.add(uniProduct);
   }
  }
  
  return productList;
 }
 
 /**
  * Gets the resultfrom query.
  *
  * @param session
  *         the session
  * @param map
  *         the map
  * @return the resultfrom query
  */
 private static Iterator<Resource> getResultfromQuery(Session session, Map<String, String> map) {
  
  Query query = null;
  SearchResult result = null;
  Iterator<Resource> rsIterator = null;
  query = builder.createQuery(PredicateGroup.create(map), session);
  query.setHitsPerPage(0);
  if (query != null) {
   result = query.getResult();
   rsIterator = result.getResources();
  }
  return rsIterator;
 }
 
 /**
  * Gets the service reference.
  *
  * @param <T>
  *         the generic type
  * @param serviceClass
  *         the service class
  * @return the service reference
  */
 @SuppressWarnings("unchecked")
 public static <T> T getServiceReference(final Class<T> serviceClass) {
  T serviceRef;
  final BundleContext bundleContext = FrameworkUtil.getBundle(serviceClass).getBundleContext();
  final ServiceReference osgiRef = bundleContext.getServiceReference(serviceClass.getName());
  serviceRef = (T) bundleContext.getService(osgiRef);
  return serviceRef;
 }
 
 /**
  * Gets the uni product.
  *
  * @param product
  *         the product
  * @return the uni product
  */
 public static UNIProduct getUniProduct(Product product) {
  UNIProduct uniProduct = null;
  if (product instanceof UNIProduct) {
   uniProduct = (UNIProduct) product;
  }
  return uniProduct;
 }
 
 /**
  * Gets the input stream from resource.
  *
  * @param resolver
  *         the resolver
  * @param response
  *         the response
  * @return the input stream from resource
  */
 public static InputStream getInputStreamFromResource(ResourceResolver resolver, SlingHttpServletResponse response, String commerceProvider) {
  
  InputStream is = null;
  Resource resource = null;
  if ("rewards".equals(commerceProvider)) {
   resource = resolver.getResource(RIM_SHEET_PATH);
  } else {
   resource = resolver.getResource(PIM_SHEET_PATH);
  }
  
  if (resource != null) {
   Asset asset = resource.adaptTo(Asset.class);
   if (asset != null) {
    is = asset.getOriginal().getStream();
    LOGGER.info("content length : ".concat(String.valueOf(asset.getOriginal().getSize())));
    response.setHeader("Content-Type", "application/vnd.ms-excel.sheet.macroenabled.12");
    response.setHeader("Content-Disposition", "inline; filename=\"" + asset.getName() + "\"");
   }
  }
  return is;
 }
 
 /**
  * Title to number.
  *
  * @param s
  *         the s
  * @return the int
  */
 public static int titleToNumber(String s) {
  if (s == null || s.length() == 0) {
   throw new IllegalArgumentException("Input is not valid!");
  }
  
  int result = 0;
  int i = s.length() - 1;
  int t = 0;
  while (i >= 0) {
   char curr = s.charAt(i);
   result = result + (int) Math.pow(TWENTY_SIX, t) * (curr - 'A' + 1);
   t++;
   i--;
  }
  
  return result;
 }
 
 /**
  * Gets the import location.
  *
  * @param request
  *         the request
  * @return the import location
  */
 public static String getImportLocation(SlingHttpServletRequest request) {
  String importLocation = StringUtils.EMPTY;
  if (request != null && request.getParameter(UNICommerceConstants.STOREPATH) != null) {
   importLocation = request.getParameter(UNICommerceConstants.STOREPATH).toString();
  }
  return importLocation;
 }
 
 /**
  * Checks if is not expired.
  *
  * @param res
  *         the res
  * @return true, if is not expired
  */
 public static boolean isNotExpired(Resource res) {
  boolean status = true;
  ValueMap valueMap = res.getChild("metadata").adaptTo(ValueMap.class);
  if (valueMap != null) {
   GregorianCalendar assetExpiryDate = (GregorianCalendar) MapUtils.getObject(valueMap, "prism:expirationDate");
   if (assetExpiryDate != null && assetExpiryDate.before(Calendar.getInstance())) {
    status = false;
   }
  }
  return status;
 }
 
 /**
  * Gets the.
  *
  * @param obj
  *         the obj
  * @param key
  *         the key
  * @return the string
  */
 public static String get(JSONObject obj, String key) {
  String value = null;
  if ((obj != null) && obj.has(key)) {
   value = obj.getString(key);
  }
  return value;
 }
 
 /**
  * Gets the image resources by id.
  *
  * @param imageID
  *         the image id
  * @param path
  *         the path
  * @param resourceResolver
  *         the resource resolver
  * @param imageType
  *         the image type
  * @return the image resources by id
  */
 public static List getImageResourcesByID(String imageID, String path, ResourceResolver resourceResolver, String imageType) {
  
  List list = new LinkedList();
  imageType = imageType.toUpperCase();
  String query = "SELECT * FROM [dam:AssetContent] WHERE ISDESCENDANTNODE([" + path + "]) and [metadata/recordID] IN (" + imageID.trim() + ")";
  if (StringUtils.isNotBlank(imageType)) {
   query = "SELECT * FROM [dam:AssetContent] WHERE ISDESCENDANTNODE([" + path + "]) and [metadata/recordID] IN (" + imageID.trim()
     + ") and [metadata/dam:Fileformat] IN ('" + imageType + "')";
  }
  
  Iterator<Resource> resourceIterator = resourceResolver.findResources(query, "JCR-SQL2");
  
  while (resourceIterator.hasNext()) {
   Resource res = resourceIterator.next();
   if (isNotExpired(res)) {
    list.add(res);
   }
  }
  
  return list;
 }
 
 /**
  * Gets the asset location path.
  *
  * @param storePath
  *         the store path
  * @param resourceResolver
  *         the resource resolver
  * @param propertyName
  *         the property name
  * @return the asset location path
  */
 public static String getAssetLocationPath(String storePath, ResourceResolver resourceResolver, String propertyName) {
  String assetLocationPath = StringUtils.EMPTY;
  Resource folderResource = resourceResolver.getResource(storePath);
  if (folderResource != null) {
   Node folderNode = folderResource.adaptTo(Node.class);
   try {
    if (folderNode.hasProperty(propertyName)) {
     assetLocationPath = folderNode.getProperty(propertyName).getValue().getString();
    }
   } catch (RepositoryException e) {
    LOGGER.error("Error while fetching asset location properties : " + propertyName, e);
   }
  }
  return assetLocationPath;
 }
 
 /**
  * Gets the image id from decimal value.
  *
  * @param imageId
  *         the image id
  * @return the image id from decimal value
  */
 public static String getImageIdFromDecimalValue(String imageId) {
  float f = Float.parseFloat(imageId);
  return String.valueOf((int) f);
 }
 
 /**
  * Gets the schema pages from search hit.
  *
  * @param h
  *         the h
  * @param resourceResolver
  *         the resource resolver
  * @return the schema pages from search hit
  */
 public static Page getSchemaPagesFromSearchHit(Hit h, ResourceResolver resourceResolver) {
  String pagePath = StringUtils.EMPTY;
  Page page = null;
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  try {
   pagePath = h.getPath();
   if (StringUtils.isNotBlank(pagePath) && (pageManager != null)) {
    page = pageManager.getContainingPage(resourceResolver.getResource(pagePath));
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in getting page from search result : ", e);
  }
  return page;
 }
 
 /**
  * Gets the PIM schema query map.
  *
  * @param path
  *         the path
  * @return the PIM schema query map
  */
 public static Map<String, String> getPIMSchemaQueryMap(String path) {
  Map<String, String> map = new HashMap<String, String>();
  
  map.put("path", path);
  map.put("type", "cq:PageContent");
  map.put("1_property", JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY);
  map.put("1_property.value", "unilever-aem/components/page/schemaConfigurationPage");
  
  map.put("p.offset", "0");
  map.put("p.limit", "-1");
  
  return map;
 }
 
 /**
  * Gets the extractor column mapping.
  *
  * @return the extractor column mapping
  */
 public static String[] getExtractorDefaultColumnMapping() {
  String[] extractorColumnMappings = { "SkuParent=EANparent", "Type=content type", "Action=status", "UniqueID=productID", "ProductID=EAN", "Sku=EAN",
    "Name=productName", "Category=category", "Title=shortTitle", "Description=shortProductDescription", "ProductionDate=productLaunchDate",
    "CqTags=productTags", "Ingredients=ingredients", "ImageIds=productImage", "AltTextImages=altTextImages", "Size=productSize", "Price=productPrice",
    "SmartProductId=smartProductID", "ProductSize=imageType", "RecipientsInfo=recipientsInfo", "WrapperInfo=WrapperInfo",
    "CustomizeProductPrice=CustomizeProductPrice", "CustomizeDescription=CustomizeDescription", "WrappingPrice=wrappingPrice",
    "CustomizeImageIds=CustomizeImageId", "CustomizeImageAltText=CustomizeImageAltText", "CustomizeGiftWrapDescription=CustomizeGiftWrapDescription",
    "NutritionalFacts=NutritionFacts", "ChildProducts=ChildProducts", "Allergy=Allergy", "ShortIdentifierValue=shortIdentifierValue",
    "LabelInsightId=labelInsightId", "IlsProductName=ilsProductName", "AemPageName=aemPageName", "Lastmodifiedtime=lastmodifiedtime",
    "ImageFormat=imageFormat" };
  return extractorColumnMappings;
 }
 
 public static String[] getRewardDefaultColumnMapping() {
  String[] rewardColumnMappings = { "Action=status", "Sku=rewardID", "SkuParent=rewardParentID", "Name=rewardName", "ImageIds=rewardImage",
    "AltTextImages=rewardAltTextImages", "ImageFormat=imageFormat", "RewardVariant=rewardVariant", "Title=rewardTitle",
    "Description=rewardDescription", "Price=rewardPointPrice", "RewardAvailableFrom=rewardStartDate", "RewardAvailableTo=rewardEndDate",
    "Quantity=rewardStock", "CqTags=rewardTags", "Lastmodifiedtime=lastmodifiedtime", "AboutThisRewardDescription=aboutThisRewardDescription" };
  return rewardColumnMappings;
 }
 
 /**
  * Gets the scaffolding column mapping.
  *
  * @return the scaffolding column mapping
  */
 public static String[] getScaffoldingColumnMapping() {
  String[] scaffoldingColumnMappings = { "content type=type", "status=action", "EAN=sku", "EANparent=skuParent", "productName=name",
    "productImage=imageIds", "productSize=size", "shortTitle=title", "shortProductDescription=description", "productTags=cqTags",
    "smartProductID=smartProductId", "imageType=productSize", "CustomizeImageId=customizeImageIds", "recipientsInfo=recipientsInfo",
    "WrapperInfo=wrapperInfo", "NutritionFacts=nutritionalFacts", "productID=uniqueID", "productPrice=price", "productLaunchDate=productionDate",
    "CustomizeProductPrice=customizeProductPrice", "CustomizeDescription=customizeDescription", "CustomizeImageAltText=customizeImageAltText",
    "CustomizeGiftWrapDescription=customizeGiftWrapDescription", "ChildProducts=childProducts", "Allergy=allergy",
    "jcr:lastModified=jcr:lastModified" };
  return scaffoldingColumnMappings;
 }
 
 /**
  * Method exists.
  *
  * @param obj
  *         the obj
  * @param methodName
  *         the method name
  * @param className
  *         the class name
  * @return true, if successful
  */
 public static boolean methodExists(Object obj, String methodName, Class className) {
  boolean hasMethod = false;
  Method method = null;
  try {
   method = obj.getClass().getMethod(methodName, className);
   if (method != null) {
    hasMethod = true;
   }
  } catch (NoSuchMethodException e) {
   hasMethod = false;
   LOGGER.debug("Method not exist", e);
  }
  return hasMethod;
 }
 
 /**
  * Sets the additional key name.
  *
  * @param obj
  *         the obj
  * @param colName
  *         the col name
  * @param colValue
  *         the col value
  * @return the object
  * @throws NoSuchMethodException
  *          the no such method exception
  * @throws InvocationTargetException
  *          the invocation target exception
  * @throws IllegalAccessException
  *          the illegal access exception
  */
 public static Object setAdditionalKeyName(Object obj, String colName, String colValue)
   throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
  
  if (colValue != null) {
   Method method = obj.getClass().getMethod(UNICommerceConstants.ADDITIONAL_METHOD_NAME, String.class, Object.class);
   method.invoke(obj, colName, colValue);
  }
  
  return obj;
 }
 
 /**
  * Gets the schema resource.
  *
  * @param path
  *         the path
  * @param rs
  *         the rs
  * @return the schema resource
  */
 public static Resource getSchemaResource(String path, ResourceResolver rs) {
  Resource sprs = null;
  try {
   if (rs != null && StringUtils.isNotBlank(path)) {
    String schemaPath = StringUtils.EMPTY;
    Resource folderResource = rs.getResource(path);
    if (folderResource != null) {
     Node folderNode = folderResource.adaptTo(Node.class);
     if (folderNode.hasProperty("schemaPath")) {
      schemaPath = folderNode.getProperty("schemaPath").getString();
     } else {
      schemaPath = path + "/schema";
     }
    }
    
    sprs = rs.getResource(schemaPath);
   }
  } catch (RepositoryException e) {
   LOGGER.error("Unable to get schema resource ", e);
  }
  
  return sprs;
 }
 
 public static String getCommerceProvider(String path, ResourceResolver rs) {
  String commerceProvider = StringUtils.EMPTY;
  try {
   if (rs != null && StringUtils.isNotBlank(path)) {
    Resource folderResource = rs.getResource(path);
    if (folderResource != null) {
     Node folderNode = folderResource.adaptTo(Node.class);
     if (folderNode.hasProperty("cq:commerceProvider")) {
      commerceProvider = folderNode.getProperty("cq:commerceProvider").getString();
     }
    }
   }
  } catch (RepositoryException e) {
   LOGGER.error("Unable to get schema resource ", e);
  }
  
  return commerceProvider;
 }
 
 /**
  * Gets the resultfrom query.
  *
  * @param session
  *         the session
  * @param map
  *         the map
  * @return the resultfrom query
  */
 public static List<Page> getResultPagesfromQuery(Session session, Map<String, String> map) {
  
  Query query = null;
  SearchResult result = null;
  List<Page> pageList = new ArrayList<Page>();
  
  query = builder.createQuery(PredicateGroup.create(map), session);
  query.setHitsPerPage(0);
  if (query != null) {
   result = query.getResult();
   
   Iterator<Resource> rsIterator = result.getResources();
   while (rsIterator.hasNext()) {
    Page page = rsIterator.next().adaptTo(Page.class);
    if (page != null) {
     pageList.add(page);
    }
   }
  }
  return pageList;
 }
 
 /**
  * Gets the pages.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @param commerceTypePath
  *         the commerce type path
  * @param pageType
  *         the page type
  * @return the pages
  */
 public static List<Page> getPages(Session session, String path, String commerceTypePath, String pageType) {
  
  Map<String, String> map = new HashMap<String, String>();
  
  List<Page> pageList = new ArrayList<Page>();
  
  map.put("path", path);
  map.put("type", NameConstants.NT_PAGE);
  map.put("property", commerceTypePath);
  map.put("property.value", pageType);
  
  builder = PIMInjestionUtility.getServiceReference(QueryBuilder.class);
  if (builder != null) {
   pageList = PIMInjestionUtility.getResultPagesfromQuery(session, map);
  }
  return pageList;
 }
}
