/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.format.DateTimeFormat;

/**
 * The Class Column.
 *
 * @param <T>
 *         the generic type
 */
public final class Column<T> {
 
 /** The Constant MULTI. */
 private static final String MULTI = "multi";
 
 /** The mapping. */
 private static Map<String, String> mapping = new HashMap<String, String>();
 
 /** The Constant ISO_DATE_PATTERN. */
 private static final Pattern ISO_DATE_PATTERN = Pattern.compile("^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$");
 
 /** The multi delimiter. */
 private String multiDelimiter = "|";
 
 /** The raw. */
 private String raw;
 
 /** The property name. */
 private String propertyName;
 
 /** The multi. */
 private boolean multi = false;
 
 /** The data type. */
 private Class dataType = String.class;
 
 /** The index. */
 private int index = 0;
 
 /** The ignore. */
 private boolean ignore = false;
 
 /** The Constant TWO */
 private static final int NUMERIC_TWO = 2;
 
 /**
  * Instantiates a new column.
  *
  * @param oldRaw
  *         the old raw
  * @param index
  *         the index
  */
 public Column(String oldRaw, final int index) {
  String rawInput = oldRaw;
  mapping.put("Short Description - Title", com.day.cq.dam.api.DamConstants.DC_TITLE);
  mapping.put("Long Description", com.day.cq.dam.api.DamConstants.DC_DESCRIPTION);
  mapping.put("Last Modified Date", "Last Modified Date {{ calendar }}");
  mapping.put("Date Created", "Date Created {{ calendar }}");
  mapping.put("Earliest Expiry Date", "Earliest Expiry Date {{ calendar }}");
  mapping.put("Asset Import Date", "Asset Import Date {{ calendar }}");
  mapping.put("Latest Effective Date", "Latest Effective Date {{ calendar }}");
  mapping.put("Created For Country(ies)", "Created For Country(ies) {{String : multi }}");
  mapping.put("Other Keywords", "Other Keywords {{String : multi }}");
  
  this.index = index;
  this.raw = StringUtils.trim(rawInput);
  
  rawInput = mapping.get(this.raw) != null ? mapping.get(this.raw) : rawInput;
  this.raw = StringUtils.trim(rawInput);
  
  String paramsStr = StringUtils.substringBetween(rawInput, "{{", "}}");
  String[] params = StringUtils.split(paramsStr, ":");
  
  if (StringUtils.isBlank(paramsStr)) {
   this.propertyName = this.getRaw();
  } else {
   this.propertyName = StringUtils.trim(StringUtils.substringBefore(this.getRaw(), "{{"));
   
   if (params.length == NUMERIC_TWO) {
    this.dataType = nameToClass(StringUtils.stripToEmpty(params[0]));
    this.multi = StringUtils.equalsIgnoreCase(StringUtils.stripToEmpty(params[1]), MULTI);
   }
   
   if (params.length == 1) {
    if (StringUtils.equalsIgnoreCase(MULTI, StringUtils.stripToEmpty(params[0]))) {
     this.multi = true;
    } else {
     this.dataType = nameToClass(StringUtils.stripToEmpty(params[0]));
    }
   }
  }
 }
 
 /**
  * Gets the data.
  *
  * @param data
  *         the data
  * @return the data
  */
 public T getData(String data) {
  return (T) toObjectType(data, this.getDataType());
 }
 
 /**
  * Gets the multi data.
  *
  * @param data
  *         the data
  * @return the multi data
  */
 public T[] getMultiData(String data) {
  final String[] vals = StringUtils.split(data, this.multiDelimiter);
  
  final List<T> list = new ArrayList<T>();
  
  for (String val : vals) {
   T obj = (T) this.toObjectType(val, this.getDataType());
   list.add(obj);
  }
  
  return list.toArray((T[]) Array.newInstance(this.getDataType(), 0));
 }
 
 /**
  * To object type.
  *
  * @param <T>
  *         the generic type
  * @param oldData
  *         the old data
  * @param klass
  *         the klass
  * @return the t
  */
 private <T> T toObjectType(String oldData, Class<T> klass) {
  String data = oldData;
  
  data = StringUtils.trim(data);
  
  if (Double.class.equals(klass)) {
   try {
    return klass.cast(Double.parseDouble(data));
   } catch (NumberFormatException ex) {
    return null;
   }
  } else if (Long.class.equals(klass)) {
   try {
    return klass.cast(Long.parseLong(data));
   } catch (NumberFormatException ex) {
    return null;
   }
  } else if (Integer.class.equals(klass)) {
   try {
    return klass.cast(Long.parseLong(data));
   } catch (NumberFormatException ex) {
    return null;
   }
  } else if (StringUtils.equalsIgnoreCase("true", data)) {
   return klass.cast(Boolean.TRUE);
  } else if (StringUtils.equalsIgnoreCase("false", data)) {
   return klass.cast(Boolean.FALSE);
  } else if ((Date.class.equals(Date.class) || Calendar.class.equals(Calendar.class)) && ISO_DATE_PATTERN.matcher(data).matches()) {
   
   return klass.cast(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(data).toCalendar(Locale.US));
  } else {
   return klass.cast(data);
  }
 }
 
 /**
  * Name to class.
  *
  * @param name
  *         the name
  * @return the class
  */
 private Class nameToClass(String name) {
  Class nameToClass = String.class;
  if (StringUtils.equalsIgnoreCase(name, "date") || StringUtils.equalsIgnoreCase(name, "calendar")) {
   nameToClass = Calendar.class;
  } else if (StringUtils.equalsIgnoreCase(name, "double")) {
   nameToClass = Double.class;
  } else if (StringUtils.equalsIgnoreCase(name, "long") || StringUtils.equalsIgnoreCase(name, "int")
    || StringUtils.equalsIgnoreCase(name, "integer")) {
   nameToClass = Long.class;
  } else if (StringUtils.equalsIgnoreCase(name, "boolean")) {
   nameToClass = Boolean.class;
  }
  return nameToClass;
 }
 
 /**
  * Gets the raw.
  *
  * @return the raw
  */
 public String getRaw() {
  return raw;
 }
 
 /**
  * Gets the property name.
  *
  * @return the property name
  */
 public String getPropertyName() {
  return propertyName;
 }
 
 /**
  * Checks if is multi.
  *
  * @return true, if is multi
  */
 public boolean isMulti() {
  return multi;
 }
 
 /**
  * Gets the data type.
  *
  * @return the data type
  */
 public Class getDataType() {
  return dataType;
 }
 
 /**
  * Gets the index.
  *
  * @return the index
  */
 public int getIndex() {
  return index;
 }
 
 /**
  * Checks if is ignore.
  *
  * @return true, if is ignore
  */
 public boolean isIgnore() {
  return ignore;
 }
 
 /**
  * Sets the ignore.
  *
  * @param ignore
  *         the new ignore
  */
 public void setIgnore(final boolean ignore) {
  this.ignore = ignore;
 }
 
 /**
  * Sets the multi delimiter.
  *
  * @param multiDelimiter
  *         the new multi delimiter
  */
 public void setMultiDelimiter(final String multiDelimiter) {
  this.multiDelimiter = multiDelimiter;
 }
 
 /**
  * Gets the columns.
  *
  * @param row
  *         the row
  * @param multiDelimiter
  *         the multi delimiter
  * @param ignoreProperties
  *         the ignore properties
  * @param requiredProperties
  *         the required properties
  * @return the columns
  * @throws CsvAssetImportException
  *          the csv asset import exception
  */
 public static Map<String, Column> getColumns(final String[] row, final String multiDelimiter, final String[] ignoreProperties,
   final String[] requiredProperties) throws CsvAssetImportException {
  final Map<String, Column> map = new HashMap<String, Column>();
  
  for (int i = 0; i < row.length; i++) {
   final Column col = new Column(row[i], i);
   
   col.setIgnore(ArrayUtils.contains(ignoreProperties, col.getPropertyName()));
   col.setMultiDelimiter(multiDelimiter);
   
   map.put(col.getPropertyName(), col);
  }
  
  final List<String> missingRequiredProperties = hasRequiredFields(map.values(), requiredProperties);
  
  if (!missingRequiredProperties.isEmpty()) {
   throw new CsvAssetImportException("Could not find required columns in CSV: " + StringUtils.join(missingRequiredProperties, ", "));
  }
  
  return map;
 }
 
 /**
  * Checks for required fields.
  *
  * @param columns
  *         the columns
  * @param requiredPropertyNames
  *         the required property names
  * @return the list
  */
 private static List<String> hasRequiredFields(final Collection<Column> columns, final String... requiredPropertyNames) {
  final List<String> missing = new ArrayList<String>();
  
  for (final String propertyName : requiredPropertyNames) {
   boolean found = false;
   
   for (final Column column : columns) {
    if (StringUtils.equals(propertyName, column.getPropertyName())) {
     found = true;
     break;
    }
   }
   
   if (!found) {
    missing.add(propertyName);
   }
  }
  
  return missing;
 }
}
