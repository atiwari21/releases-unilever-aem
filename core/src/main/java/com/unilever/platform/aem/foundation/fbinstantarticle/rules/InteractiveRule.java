/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;
import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Interactive;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Paragraph;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class AnchorRule.
 */
public class InteractiveRule extends ConfigurationSelectorRule {
 
 public static final String PROPERTY_IFRAME = "interactive.iframe";
 
 public static final String PROPERTY_URL = "interactive.url";
 
 public static final String PROPERTY_WIDTH_NO_MARGIN = Interactive.NO_MARGIN;
 
 public static final String PROPERTY_WIDTH_COLUMN_WIDTH = Interactive.COLUMN_WIDTH;
 
 public static final String PROPERTY_HEIGHT = "interactive.height";
 public static final String PROPERTY_WIDTH = "interactive.width";
 
 /**
  * Instantiates a new anchor rule.
  */
 private InteractiveRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor rule
  */
 public static InteractiveRule create() {
  return new InteractiveRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Interactive interactive = Interactive.create();
  String url = this.getProperty(PROPERTY_URL, node) != null ? this.getProperty(PROPERTY_URL, node).toString() : null;
  String iframe = this.getProperty(PROPERTY_IFRAME, node) != null ? this.getProperty(PROPERTY_IFRAME, node).toString() : null;
  String columnWidth = this.getProperty(PROPERTY_WIDTH_COLUMN_WIDTH, node) != null ? this.getProperty(PROPERTY_WIDTH_COLUMN_WIDTH, node).toString()
    : null;
  String width = this.getProperty(PROPERTY_WIDTH, node) != null ? this.getProperty(PROPERTY_WIDTH, node).toString() : null;
  String height = this.getProperty(PROPERTY_HEIGHT, node) != null ? this.getProperty(PROPERTY_HEIGHT, node).toString() : null;
  
  if (StringUtils.isNotBlank(iframe)) {
   interactive.withHTML(iframe);
  }
  if (StringUtils.isNotBlank(url)) {
   interactive.withSource(url);
  }
  
  if (StringUtils.isNotBlank(iframe) || StringUtils.isNotBlank(url)) {
   transformer.getInstantArticle().addChild(interactive);
   if (!(container instanceof InstantArticle)) {
    transformer.getInstantArticle().addChild((com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement) container);
   }
  }
  
  transformer.transform(interactive, node);
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Paragraph.getClassName(), InstantArticle.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the anchor rule
  */
 public static InteractiveRule createFrom(JSONObject configuration) {
  InteractiveRule interactiveRule = create();
  interactiveRule = (InteractiveRule) interactiveRule.withSelector(configuration.getString("selector"));
  JSONObject anchorConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_IFRAME);
  properties.add(PROPERTY_HEIGHT);
  properties.add(PROPERTY_URL);
  properties.add(PROPERTY_WIDTH);
  properties.add(PROPERTY_WIDTH_COLUMN_WIDTH);
  properties.add(PROPERTY_WIDTH_NO_MARGIN);
  interactiveRule.withProperties(properties, anchorConfiguration);
  return interactiveRule;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
