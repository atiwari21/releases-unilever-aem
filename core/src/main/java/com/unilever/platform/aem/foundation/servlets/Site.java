/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import org.apache.commons.lang.StringUtils;

/**
 * This is use to hold site data available in 'context' claim of JWT token.
 *
 * @author Saba Parveen
 * 
 */
public class Site {
 
 private String locale;
 
 private String brand;
 
 private String entity;
 
 private String publication;
 
 /**
  *
  * @return locale
  */
 public String getLocale() {
  return locale;
 }
 
 /**
  *
  * @param locale
  */
 public void setLocale(String locale) {
  this.locale = locale;
 }
 
 /**
  *
  * @return Brand
  */
 public String getBrand() {
  return brand;
 }
 
 /**
  *
  * @param brand
  */
 public void setBrand(String brand) {
  this.brand = brand;
 }
 
 /**
  *
  * @return Entity
  */
 public String getEntity() {
  return entity;
 }
 
 /**
  *
  * @param entity
  */
 public void setEntity(String entity) {
  this.entity = entity;
 }
 
 /**
  *
  * @return Publication
  */
 public String getPublication() {
  return publication;
 }
 
 /**
  *
  * @param publication
  */
 public void setPublication(String publication) {
  this.publication = publication;
 }
 
 @Override
 public String toString() {
  StringBuilder br = new StringBuilder();
  br.append("{ \"locale\" : \"");
  br.append(this.locale);
  br.append("\", \"entity\" : \"");
  br.append(this.entity);
  br.append("\", \"brand\" : \"");
  br.append(this.brand);
  
  if (StringUtils.isNotEmpty(this.publication)) {
   br.append("\", \"publication\" : \"");
   br.append(this.publication);
  }
  br.append("\" }");
  return br.toString();
 }
}
