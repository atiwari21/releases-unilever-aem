/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api.impl;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.common.AbstractJcrCommerceService;
import com.adobe.cq.commerce.common.AbstractJcrCommerceSession;
import com.unilever.platform.aem.foundation.commerce.api.UNICommerceSession;

/**
 * The Class UNICommerceSessionImpl.
 */
public class UNICommerceSessionImpl extends AbstractJcrCommerceSession implements UNICommerceSession {
 
 /**
  * Logger for this class.
  */
 private static final Logger LOG = LoggerFactory.getLogger(UNICommerceSessionImpl.class);
 
 /**
  * Instantiates a new uni commerce session impl.
  *
  * @param commerceService
  *         the commerce service
  * @param request
  *         the request
  * @param response
  *         the response
  * @param resource
  *         the resource
  * @throws CommerceException
  *          the commerce exception
  */
 public UNICommerceSessionImpl(AbstractJcrCommerceService commerceService, SlingHttpServletRequest request, SlingHttpServletResponse response,
   Resource resource) throws CommerceException {
  super(commerceService, request, response, resource);
  LOG.debug("Initiating the UNI Commerce Session");
 }
 
}
