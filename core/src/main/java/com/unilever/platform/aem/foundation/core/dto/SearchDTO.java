/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import java.util.Date;

import javax.jcr.Value;

/**
 * The Class SearchDTO.
 */
public class SearchDTO {
 
 /** The result node tags. */
 private Value[] resultNodeTags;
 
 /** The result node path. */
 private String resultNodePath;
 
 /** The replication time. */
 private Date replicationTime;
 
 /**
  * Gets the result node tags.
  *
  * @return the result node tags
  */
 public Value[] getResultNodeTags() {
  return resultNodeTags;
 }
 
 /**
  * Sets the result node tags.
  *
  * @param resultNodeTags
  *         the new result node tags
  */
 public void setResultNodeTags(Value[] resultNodeTags) {
  this.resultNodeTags = resultNodeTags;
 }
 
 /**
  * Gets the result node path.
  *
  * @return the result node path
  */
 public String getResultNodePath() {
  return resultNodePath;
 }
 
 /**
  * Sets the result node path.
  *
  * @param resultNodePath
  *         the new result node path
  */
 public void setResultNodePath(String resultNodePath) {
  this.resultNodePath = resultNodePath;
 }
 
 /**
  * Gets the replication time.
  *
  * @return the replication time
  */
 public Date getReplicationTime() {
  return replicationTime;
 }
 
 /**
  * Sets the replication time.
  *
  * @param replicationTime
  *         the new replication time
  */
 public void setReplicationTime(Date replicationTime) {
  this.replicationTime = replicationTime;
 }
}
