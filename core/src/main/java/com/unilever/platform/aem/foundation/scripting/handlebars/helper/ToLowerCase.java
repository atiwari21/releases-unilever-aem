/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * The Class GenericHelper.
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class ToLowerCase implements HandlebarsHelperService<Object> {
 
 /**
  * Logger Instance.
  */
 private static final Logger LOG = LoggerFactory.getLogger(ToLowerCase.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new ToLowerCase();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "toLowerCase";
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getName()
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getHelper()
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.github.jknack.handlebars.Helper#apply(java.lang.Object, com.github.jknack.handlebars.Options)
  */
 @Override
 public CharSequence apply(Object path, Options options) throws IOException {
  
  CharSequence chars = null;
  if (path != null) {
   LOG.debug("path = ", path.toString());
  } else {
   LOG.debug("path is null");
  }
  try {
   String finalPath = (String) path;
   
   chars = StringUtils.lowerCase(finalPath);
  } catch (Exception e) {
   LOG.error("Error occurred in ToLowerCase Helper.", e);
  }
  return chars;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#setCache(com.sapient.platform.iea.aem.scripting.
  * handlebars.api.HandlebarsTemplateCache)
  */
 @Override
 public void setCache(HandlebarsTemplateCache templateCache) {
  LOG.info("Cache is not implemented for renderTemplate handler.");
 }
}
