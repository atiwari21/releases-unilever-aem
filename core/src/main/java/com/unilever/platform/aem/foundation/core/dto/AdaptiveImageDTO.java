/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

/**
 * The Class SearchBreadcrumbDTO.
 */
public class AdaptiveImageDTO {
 
 /** The path. */
 private String path;
 
 /** The fileName. */
 private String fileName;
 
 /** The extension. */
 private String extension;
 
 /** The is not adaptive image. */
 private String isNotAdaptiveImage;
 
 /** The title. */
 private String title;
 
 /** The url. */
 private String url;
 
 /** The alt image. */
 private String altImage;
 
 /**
  * Gets the path.
  *
  * @return the path
  */
 public String getPath() {
  return path;
 }
 
 /**
  * Sets the path.
  *
  * @param path
  *         the new path
  */
 public void setPath(String path) {
  this.path = path;
 }
 
 /**
  * Gets the file name.
  *
  * @return the file name
  */
 public String getFileName() {
  return fileName;
 }
 
 /**
  * Sets the file name.
  *
  * @param fileName
  *         the new file name
  */
 public void setFileName(String fileName) {
  this.fileName = fileName;
 }
 
 /**
  * Gets the extension.
  *
  * @return the extension
  */
 public String getExtension() {
  return extension;
 }
 
 /**
  * Sets the extension.
  *
  * @param extension
  *         the new extension
  */
 public void setExtension(String extension) {
  this.extension = extension;
 }
 
 /**
  * Gets the checks if is not adaptive image.
  *
  * @return the checks if is not adaptive image
  */
 public String getIsNotAdaptiveImage() {
  return isNotAdaptiveImage;
 }
 
 /**
  * Sets the checks if is not adaptive image.
  *
  * @param isNotAdaptiveImage
  *         the new checks if is not adaptive image
  */
 public void setIsNotAdaptiveImage(String isNotAdaptiveImage) {
  this.isNotAdaptiveImage = isNotAdaptiveImage;
 }
 
 /**
  * Gets the title.
  *
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  *
  * @param title
  *         the new title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the url.
  *
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Sets the url.
  *
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
 /**
  * Gets the alt image.
  *
  * @return the alt image
  */
 public String getAltImage() {
  return altImage;
 }
 
 /**
  * Sets the alt image.
  *
  * @param altImage
  *         the new alt image
  */
 public void setAltImage(String altImage) {
  this.altImage = altImage;
 }
 
}
