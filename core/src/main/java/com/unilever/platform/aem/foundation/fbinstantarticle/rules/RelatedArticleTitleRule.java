/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.RelatedArticles;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

public class RelatedArticleTitleRule extends ConfigurationSelectorRule {
 
 /**
  * Instantiates a new header title rule.
  */
 private RelatedArticleTitleRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the header title rule
  */
 public static RelatedArticleTitleRule create() {
  return new RelatedArticleTitleRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the header title rule
  */
 public static RelatedArticleTitleRule createFrom(JSONObject configuration) {
  return (RelatedArticleTitleRule) create().withSelector(configuration.getString("selector"));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(java.lang.Object)
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer , com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  RelatedArticles relatedArticles = RelatedArticles.create();
  if (container instanceof RelatedArticles) {
   relatedArticles = ((RelatedArticles) container);
  }
  String title = "";
  if (node instanceof Element) {
   title = ((Element) node).html();
  } else {
   title = ((TextNode) node).text();
  }
  
  return relatedArticles.withTitle(title);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { RelatedArticles.getClassName() };
 }
 
}
