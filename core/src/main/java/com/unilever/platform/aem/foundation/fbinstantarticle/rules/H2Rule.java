/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Caption;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.H2;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class H2Rule.
 */
public class H2Rule extends ConfigurationSelectorRule {
 
 @Override
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 public String[] getContextClass() {
  return new String[] { Header.getClassName(), Caption.getClassName(), InstantArticle.getClassName() };
 }
 
 /**
  * Creates the.
  *
  * @return the h2 rule
  */
 public static H2Rule create() {
  return new H2Rule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration the configuration
  * @return the rule
  */
 public static Rule createFrom(JSONObject configuration) {
  H2Rule h2Rule = H2Rule.create();
  h2Rule.withSelector(configuration.getString("selector"));
  h2Rule.withProperties(getCaptionProperties(), configuration);
  return h2Rule;
 }
 
 /**
  * Gets the caption properties.
  *
  * @return the caption properties
  */
 static List<String> getCaptionProperties() {
  List<String> properties = new ArrayList<String>();
  properties.add(Caption.POSITION_BELOW);
  properties.add(Caption.POSITION_CENTER);
  properties.add(Caption.POSITION_ABOVE);
  properties.add(Caption.ALIGN_LEFT);
  properties.add(Caption.ALIGN_CENTER);
  properties.add(Caption.ALIGN_RIGHT);
  return properties;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  H2 h2 = H2.create();
  Container tempContainer=container;
  if (tempContainer instanceof Header) {
   tempContainer = ((Header) tempContainer).withSubTitle(h2);
  } else if (tempContainer instanceof Caption) {
   tempContainer = ((Caption) tempContainer).withSubTitle(h2);
  } {
   tempContainer = transformer.getInstantArticle().addChild(h2);
  }
  
  if (this.getProperty(Caption.POSITION_BELOW, node) != null) {
   h2.withPosition(Caption.POSITION_BELOW);
  }
  if (this.getProperty(Caption.POSITION_CENTER, node) != null) {
   h2.withPosition(Caption.POSITION_CENTER);
  }
  if (this.getProperty(Caption.POSITION_ABOVE, node) != null) {
   h2.withPosition(Caption.POSITION_ABOVE);
  }
  if (this.getProperty(Caption.ALIGN_LEFT, node) != null) {
   h2.withPosition(Caption.ALIGN_LEFT);
  }
  if (this.getProperty(Caption.ALIGN_CENTER, node) != null) {
   h2.withPosition(Caption.ALIGN_CENTER);
  }
  if (this.getProperty(Caption.ALIGN_RIGHT, node) != null) {
   h2.withPosition(Caption.ALIGN_RIGHT);
  }
  transformer.transform(h2, node);
  return tempContainer;
 }
 
}
