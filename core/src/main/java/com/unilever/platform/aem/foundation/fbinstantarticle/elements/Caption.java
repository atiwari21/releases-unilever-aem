/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Class Caption.
 */
public class Caption extends FormattedText {
 
 /** The Constant SIZE_MEDIUM. */
 public static final String SIZE_MEDIUM = "op-medium";
 
 /** The Constant SIZE_LARGE. */
 public static final String SIZE_LARGE = "op-large";
 
 /** The Constant SIZE_XLARGE. */
 public static final String SIZE_XLARGE = "op-extra-large";
 
 /** The Constant ALIGN_LEFT. */
 // Text alignment (horizontal)
 public static final String ALIGN_LEFT = "op-left";
 
 /** The Constant ALIGN_CENTER. */
 public static final String ALIGN_CENTER = "op-center";
 
 /** The Constant ALIGN_RIGHT. */
 public static final String ALIGN_RIGHT = "op-right";
 
 /** The Constant POSITION_BELOW. */
 // Vertical position of the block
 public static final String POSITION_BELOW = "op-vertical-below";
 
 /** The Constant POSITION_ABOVE. */
 public static final String POSITION_ABOVE = "op-vertical-above";
 
 /** The Constant POSITION_CENTER. */
 public static final String POSITION_CENTER = "op-vertical-center";
 
 /** The Constant VERTICAL_TOP. */
 // Vertical alignment of the block
 public static final String VERTICAL_TOP = "op-vertical-top";
 
 /** The Constant VERTICAL_BOTTOM. */
 public static final String VERTICAL_BOTTOM = "op-vertical-bottom";
 
 /** The Constant VERTICAL_CENTER. */
 public static final String VERTICAL_CENTER = "op-vertical-center";
 
 /**
  * Creates the.
  *
  * @return the caption
  */
 public static Caption create() {
  return new Caption();
 }
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }
 
 /**
  * H1 The caption title. REQUIRED
  */
 private H1 title;
 
 /**
  * The sub title.
  *
  * @var H2 The caption subtitle. optional
  */
 private H2 subTitle;
 
 /**
  * The font size.
  *
  * @var string text Size. Values: "op-medium"|"op-large"|"op-extra-large"
  */
 private String fontSize;
 
 /**
  * The text alignment.
  *
  * @var string text align. Values: "op-left"|"op-center"|"op-right"
  */
 private String textAlignment;
 
 /**
  * The vertical alignment.
  *
  * @var string vertical align. Values: "op-vertical-top"|"op-vertical-bottom"|"op-vertical-center"
  */
 private String verticalAlignment;
 
 /**
  * The position.
  *
  * @var string text position. Values: "op-vertical-below"|"op-vertical-above"|"op-vertical-center"
  */
 private String position;
 
 /**
  * Gets the title.
  *
  * @return the title
  */
 public H1 getTitle() {
  return title;
 }
 
 /**
  * With title.
  *
  * @param title the title
  * @return the caption
  */
 public Caption withTitle(Object title) {
  if (title instanceof H1) {
   this.title = (H1) title;
  } else {
   this.title = (H1) H1.create().appendText(title);
  }
  return this;
 }
 
 /**
  * Gets the sub title.
  *
  * @return the sub title
  */
 public H2 getSubTitle() {
  return subTitle;
 }
 
 /**
  * With sub title.
  *
  * @param subTitle the sub title
  * @return the caption
  */
 public Caption withSubTitle(H2 subTitle) {
  this.subTitle = subTitle;
  return this;
 }
 
 /**
  * Gets the font size.
  *
  * @return the font size
  */
 public String getFontSize() {
  return fontSize;
 }
 
 /**
  * With font size.
  *
  * @param fontSize the font size
  * @return the caption
  */
 public Caption withFontSize(String fontSize) {
  this.fontSize = fontSize;
  return this;
 }
 
 /**
  * Gets the text alignment.
  *
  * @return the text alignment
  */
 public String getTextAlignment() {
  return textAlignment;
 }
 
 /**
  * With text alignment.
  *
  * @param textAlignment the text alignment
  * @return the caption
  */
 public Caption withTextAlignment(String textAlignment) {
  this.textAlignment = textAlignment;
  return this;
 }
 
 /**
  * Gets the vertical alignment.
  *
  * @return the vertical alignment
  */
 public String getVerticalAlignment() {
  return verticalAlignment;
 }
 
 /**
  * With vertical alignment.
  *
  * @param verticalAlignment the vertical alignment
  * @return the caption
  */
 public Caption withVerticalAlignment(String verticalAlignment) {
  this.verticalAlignment = verticalAlignment;
  return this;
 }
 
 /**
  * Gets the position.
  *
  * @return the position
  */
 public String getPosition() {
  return position;
 }
 
 /**
  * With position.
  *
  * @param position the position
  * @return the caption
  */
 public Caption withPosition(String position) {
  this.position = position;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!this.isValid()) {
   return null;
  }
  Element captionElement = document.createElement("figcaption");
  if (this.title != null && (this.subTitle == null)) {
   captionElement.append(this.title.toDOMElement(document).outerHtml());
  } else if (this.title == null) {
   captionElement.append(this.title.toDOMElement().html());
  }
  
  if (this.subTitle != null) {
   captionElement.append(this.subTitle.toDOMElement(document).outerHtml());
  }
  
  captionElement.append(this.textToDOMDocumentFragment(document));
  
  return captionElement;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#isValid()
  */
 public boolean isValid() {
  return super.isValid() || (this.title != null && this.title.isValid());
 }
 
}
