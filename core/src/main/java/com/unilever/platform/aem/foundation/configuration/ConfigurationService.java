/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.util.Map;

import org.osgi.service.component.ComponentContext;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Interface ConfigurationService.
 */
public interface ConfigurationService {
 
 /**
  * Activates
  *
  * @param ctx
  *         the component context
  */
 public void activate(ComponentContext ctx);
 
 /**
  * Gets the page configurations.
  *
  * @param page
  *         the page
  * @return the page configurations
  */
 public Map<String, Map<String, String>> getPageConfigurations(Page page);
 
 /**
  * Gets the category configuration.
  *
  * @param page
  *         the page
  * @param category
  *         the category
  * @return the category configuration
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 public Map<String, String> getCategoryConfiguration(Page page, String category) throws ConfigurationNotFoundException;
 
 /**
  * Gets the category configuration.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @param category
  *         the category
  * @return the category configuration
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 public Map<String, String> getCategoryConfiguration(BrandMarketBean brandMarketBean, String category) throws ConfigurationNotFoundException;
 
 /**
  * Gets the config value.
  *
  * @param page
  *         the page
  * @param category
  *         the category
  * @param key
  *         the key
  * @return the config value
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 public String getConfigValue(Page page, String category, String key) throws ConfigurationNotFoundException;
 
 /**
  * Gets the config value.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @param category
  *         the category
  * @param key
  *         the key
  * @return the config value
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 public String getConfigValue(BrandMarketBean brandMarketBean, String category, String key) throws ConfigurationNotFoundException;
 
 /**
  * Invalidate cache.
  */
 public void inValidateCache();
 
}
