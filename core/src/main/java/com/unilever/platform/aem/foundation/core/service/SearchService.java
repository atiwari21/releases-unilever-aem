/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.search.QueryBuilder;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TagDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;

/**
 * The Interface SearchService.
 *
 * @author djain5
 */
public interface SearchService {
 
 /**
  * Gets the tags list.
  *
  * @param parentTag
  *         the parent tag
  * @param resourceResolver
  *         the resourceResolver
  * @return List of child tag values
  */
 public List<TagDTO> getTagsList(String parentTag, ResourceResolver resourceResolver);
 
 /**
  * This method populates the list of pages tagged with input tags but not having any negatetag Those pages should have property, value at that pages
  * and pages tagged with featureTag will appear first in list.
  *
  * @param tagID
  *         the tag id
  * @param negateTag
  *         the negate tag
  * @param currentPath
  *         the current path
  * @param propertyValues
  *         the property values
  * @param param
  *         the param
  * @param resourceResolver
  *         the resourceResolver
  * @return List of Results pages.
  */
 public List<SearchDTO> getCreteriaPagesList(List<String> tagID, List<String> negateTag, String currentPath, Map<String, List<String>> propertyValues,
   SearchParam param, ResourceResolver resourceResolver);
 
 /**
  * This method populates the list of pages tagged with input tags list 1 and 2 (wuth different Operations) but not having any negatetag and should
  * have property & value at those pages.
  *
  * @param tagID1
  *         the tagid1
  * @param tagID2
  *         the tagid2
  * @param negateTag
  *         the negate tag
  * @param currentPath
  *         the current path
  * @param propertyValues
  *         the property values
  * @param param
  *         the param
  * @param resourceResolver
  *         the resourceResolver
  * @return the creteria pages list
  */
 public List<SearchDTO> getCreteriaPagesList(List<String> tagID1, List<String> tagID2, List<String> negateTag, String currentPath,
   Map<String, List<String>> propertyValues, SearchParam param, ResourceResolver resourceResolver);
 
 /**
  * Gets the multi tag results.
  *
  * @param tagIDList
  *         the tag id list
  * @param negateTag
  *         the negate tag
  * @param currentPath
  *         the current path
  * @param propertyValues
  *         the property values
  * @param param
  *         the param
  * @param resourceResolver
  *         the resourceResolver
  * @return the multi tag results
  */
 List<SearchDTO> getMultiTagResults(List<TaggingSearchCriteria> tagIDList, List<String> negateTag, String currentPath,
   Map<String, List<String>> propertyValues, SearchParam param, ResourceResolver resourceResolver);
 
 /**
  * Gets the service page.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @param commerceTypePath
  *         the commerceTypePath
  * @param pageType
  *         the pageType
  * @param param
  *         the param
  * @param builder
  *         the query builder
  * @return the service pages
  */
 List<Page> getServicePages(Session session, String path, String commerceTypePath, String pageType, QueryBuilder builder);
 
}
