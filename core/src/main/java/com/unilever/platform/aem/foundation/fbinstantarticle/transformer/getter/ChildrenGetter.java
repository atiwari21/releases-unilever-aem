/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import java.util.Iterator;

import org.jsoup.nodes.Element;

import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class ChildrenGetter.
 */
public class ChildrenGetter extends ElementGetter {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.ElementGetter#get(org.jsoup.nodes.Element)
  */
 public Object get(Element node) {
  Element element = (Element) super.get(node);
  if (element != null) {
   StringBuilder docFragmentStr = new StringBuilder();
   Iterator<Element> itr = element.children().iterator();
   while (itr.hasNext()) {
    Element child = itr.next();
    Transformer.markAsProcessed(child);
    docFragmentStr.append(((Element) Transformer.cloneNode(child)).html());
   }
   return node.ownerDocument().append(docFragmentStr.toString());
  }
  
  return null;
 }
 
}
