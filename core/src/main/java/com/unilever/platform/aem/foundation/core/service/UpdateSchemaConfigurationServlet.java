/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UpdateSchemaConfigurationServlet.
 */
@SlingServlet(label = "Unilever update schema config", methods = { HttpConstants.METHOD_POST, HttpConstants.METHOD_GET }, paths = {
  "/bin/checkSchema" }, extensions = { "html" })
public class UpdateSchemaConfigurationServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -782708002977083726L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSchemaConfigurationServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
  
  ResourceResolver rs = request.getResourceResolver();
  String selector = getSelector(request);
  
  switch (selector) {
   case "view":
    viewSchema(request, response, rs);
    break;
   case "delete":
    deleteSchema(request, response, rs);
    break;
   case "check":
    checkSchema(request, response, rs);
    break;
   default:
    break;
  }
 }
 
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  ResourceResolver rs = request.getResourceResolver();
  String selector = getSelector(request);
  
  switch (selector) {
   case "view":
    viewSchema(request, response, rs);
    break;
   case "delete":
    deleteSchema(request, response, rs);
    break;
   case "check":
    checkSchema(request, response, rs);
    break;
   default:
    response.sendRedirect("/aem/products.html");
    break;
  }
 }
 
 private void checkSchema(final SlingHttpServletRequest request, final SlingHttpServletResponse response, ResourceResolver rs) {
  String path = request.getParameter("path") != null ? request.getParameter("path").toString() : StringUtils.EMPTY;
  Boolean schemaExists = getSchemaResource(path, rs) != null ? true : false;
  
  response.setContentType("text/html;charset=UTF-8");
  try {
   if (schemaExists) {
    response.getWriter().write("found");
   } else {
    response.getWriter().write("notfound");
   }
  } catch (IOException e) {
   LOGGER.error("Schema not found", e);
  }
 }
 
 private void deleteSchema(final SlingHttpServletRequest request, final SlingHttpServletResponse response, ResourceResolver rs) {
  String path = request.getRequestPathInfo().getSuffix();
  Resource schemaResource = getSchemaResource(path, rs);
  Session session = rs.adaptTo(Session.class);
  
  if (schemaResource != null) {
   try {
    rs.delete(schemaResource);
    session.save();
    response.sendRedirect("/aem/products.html" + path);
   } catch (PersistenceException e) {
    LOGGER.error("Error while deleting resource", e);
   } catch (RepositoryException e) {
    LOGGER.error("Error while saving session", e);
   } catch (IOException e) {
    LOGGER.error("Error while redirecting", e);
   }
  }
  
 }
 
 private void viewSchema(final SlingHttpServletRequest request, final SlingHttpServletResponse response, ResourceResolver rs) {
  String path = request.getRequestPathInfo().getSuffix();
  Resource schemaResource = getSchemaResource(path, rs);
  
  if (schemaResource != null) {
   try {
    response.sendRedirect(schemaResource.getPath() + ".html?wcmmode=disabled");
   } catch (IOException e) {
    LOGGER.error("Error while redirecting to schema page", e);
   }
  }
 }
 
 private Resource getSchemaResource(String path, ResourceResolver rs) {
  Resource sprs = null;
  try {
   if (rs != null && StringUtils.isNotBlank(path)) {
    String schemaPath = StringUtils.EMPTY;
    Resource folderResource = rs.getResource(path.substring(path.lastIndexOf("/etc")));
    if (folderResource != null) {
     Node folderNode = folderResource.adaptTo(Node.class);
     if (folderNode.hasProperty("schemaPath")) {
      schemaPath = folderNode.getProperty("schemaPath").getString();
     } else {
      schemaPath = path.substring(path.lastIndexOf("/etc")) + "/schema";
     }
    }
    
    sprs = rs.getResource(schemaPath);
   }
  } catch (RepositoryException e) {
   LOGGER.error("Unable to get schema resource ", e);
  }
  
  return sprs;
 }
 
 private String getSelector(final SlingHttpServletRequest request) {
  String selector = StringUtils.EMPTY;
  String[] selectorsArray = request.getRequestPathInfo().getSelectors();
  if (selectorsArray.length > 0) {
   selector = selectorsArray[0];
  }
  return selector;
 }
}
