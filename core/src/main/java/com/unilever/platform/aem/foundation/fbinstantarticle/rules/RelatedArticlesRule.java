/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Footer;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.RelatedArticles;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class RelatedArticlesRule.
 */
public class RelatedArticlesRule extends ConfigurationSelectorRule {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  InstantArticle instantArticle = ((InstantArticle) container);
  RelatedArticles relatedArticles = instantArticle.getRelatedArticles();
  if (relatedArticles == null) {
   relatedArticles = RelatedArticles.create();
   instantArticle.withRelatedArticles((RelatedArticles) transformer.transform(relatedArticles, node));
  }
  
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName(), Footer.getClassName() };
 }
 
 /**
  * Creates the.
  *
  * @return the related articles rule
  */
 public static RelatedArticlesRule create() {
  return new RelatedArticlesRule();
 }
 
 public static RelatedArticlesRule createFrom(JSONObject configuration) {
  RelatedArticlesRule relatedArticlesRule = create();
  relatedArticlesRule = (RelatedArticlesRule) relatedArticlesRule.withSelector(configuration.getString("selector"));
  return relatedArticlesRule;
 }
 
}
