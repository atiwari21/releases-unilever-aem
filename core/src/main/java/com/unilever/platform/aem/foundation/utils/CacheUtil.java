/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.scripting.SlingBindings;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Options;
import com.google.common.cache.LoadingCache;
import com.unilever.platform.aem.foundation.configuration.FrontEndConfigurationService;

/**
 * The Class CacheUtil.
 */
public class CacheUtil {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheUtil.class);
    
    private static final String ROOT_ETC_PATH = "/etc/ui";
    
    /** The Constant BREAKPOINTS. */
    private static final String BREAKPOINTS = "BREAKPOINTS";
    
    /**
     * The helper's name.
     */
    private static final String NAME = "adaptiveRetina";
    
    /**
     * the constant for UTF-8.
     */
    private static final String UTF_8 = "UTF-8";
    
    private CacheUtil() {
        
    }
    
    /**
     * Gets the breakpointsdata.
     *
     * @param options the options
     * @param configurationService the configuration service
     * @return the breakpointsdata
     */
    @SuppressWarnings("unchecked")
    public static LoadingCache<String, Map<String, String>> getBreakpointsdata(Options options, FrontEndConfigurationService configurationService) {
        String brandName = getBrandName(options);
        LoadingCache<String, Map<String, String>> existingCache = configurationService.getConfigCache();
        SlingHttpServletRequest request = (SlingHttpServletRequest) options.get(SlingBindings.REQUEST);
        Resource resource = request.getResourceResolver().getResource(getConfigFilePath(brandName, "breakpoint.json") + "/jcr:content");
        if (existingCache.getIfPresent(brandName) == null && resource != null) {
            InputStream inputStream = getFileAsInputStream(getConfigFilePath(brandName, "breakpoint.json"), options);
            if (inputStream != null) {
                String jsonString = StringUtils.EMPTY;
                try {
                    jsonString = IOUtils.toString(inputStream, "UTF-8");
                } catch (IOException e) {
                    LOGGER.error("IOException", e);
                }
                Map<String, String> cacheMap = new HashMap<String, String>();
                cacheMap.put(brandName, jsonString);
                
                existingCache.put(brandName, cacheMap);
            }
        } else if (existingCache.getIfPresent(brandName) == null) {
            try {
                ScriptEngineManager manager = new ScriptEngineManager(null);
                ScriptEngine engine = manager.getEngineByName("js");
                Reader reader;
                InputStream inputStream = getFileAsInputStream(getConfigFilePath(brandName, "breakpoints.js"), options);
                if (inputStream != null) {
                    InputStreamReader in = new InputStreamReader(inputStream, UTF_8);
                    reader = new BufferedReader(in);
                    engine.eval(reader);
                    
                    Map<String, Object> adaptiveRetinaMap = (Map<String, Object>) engine.get(NAME);
                    Map<String, Object> breakpointsMap = (Map<String, Object>) engine.get(BREAKPOINTS);
                    
                    Map<String, Object> adaptiveRetinaJsonMap = new LinkedHashMap<String, Object>();
                    adaptiveRetinaJsonMap.put(NAME, adaptiveRetinaMap);
                    adaptiveRetinaJsonMap.put(BREAKPOINTS, breakpointsMap);
                    
                    JSONObject json = new JSONObject();
                    json.put("data", adaptiveRetinaJsonMap);
                    
                    Map<String, String> cacheMap = new HashMap<String, String>();
                    cacheMap.put(brandName, json.toString());
                    
                    existingCache.put(brandName, cacheMap);
                }
            } catch (IOException | ScriptException e) {
                LOGGER.error("IOException", e);
            }
        }
        return existingCache;
    }
    
    /**
     * Update front end config cache.
     *
     * @param resolver the resolver
     * @param configCache the config cache
     */
    @SuppressWarnings("unchecked")
    public static void updateFrontEndConfigCache(ResourceResolver resolver, LoadingCache<String, Map<String, String>> configCache) {
        Map<String, String> configMap = new HashMap<String, String>();
        Resource etcResource = resolver.getResource(ROOT_ETC_PATH);
        Iterator<Resource> etcResourceIterator = etcResource.listChildren();
        try {
            while (etcResourceIterator.hasNext()) {
                Resource brandResource = etcResourceIterator.next();
                String brandName = brandResource.getName();
                Resource resource = resolver.getResource(getConfigFilePath(brandName, "breakpoint.json") + "/jcr:content");
                if (resource != null) {
                    InputStream inputStream = getFileAsInputStream(getConfigFilePath(brandName, "breakpoint.json"), resolver);
                    if (inputStream != null) {
                        String jsonString = StringUtils.EMPTY;
                        jsonString = IOUtils.toString(inputStream, "UTF-8");
                        configMap.put(brandName, jsonString);
                    }
                } else {
                    ScriptEngineManager manager = new ScriptEngineManager(null);
                    ScriptEngine engine = manager.getEngineByName("js");
                    Reader reader;
                    InputStream inputStream = getFileAsInputStream(getConfigFilePath(brandName, "breakpoints.js"), resolver);
                    if (inputStream != null) {
                        InputStreamReader in = new InputStreamReader(inputStream, UTF_8);
                        reader = new BufferedReader(in);
                        engine.eval(reader);
                        Map<String, Object> adaptiveRetinaMap = (Map<String, Object>) engine.get(NAME);
                        Map<String, Object> breakpointsMap = (Map<String, Object>) engine.get(BREAKPOINTS);
                        Map<String, Object> adaptiveRetinaJsonMap = new LinkedHashMap<String, Object>();
                        adaptiveRetinaJsonMap.put(NAME, adaptiveRetinaMap);
                        adaptiveRetinaJsonMap.put(BREAKPOINTS, breakpointsMap);
                        
                        JSONObject json = new JSONObject();
                        json.put("data", adaptiveRetinaJsonMap);
                        configMap.put(brandName, json.toString());
                    }
                }
                configCache.put(brandName, configMap);
            }
        } catch (IOException | ScriptException e) {
            LOGGER.error("IOException", e);
        }
    }
    
    /**
     * Get Brand Name
     *
     * @param options
     *            the options
     * @return brand name.
     */
    @SuppressWarnings("unchecked")
    public static String getBrandName(Options options) {
        SlingHttpServletRequest request = (SlingHttpServletRequest) options.get(SlingBindings.REQUEST);
        Map<String, Object> dataMap = (Map<String, Object>) request.getAttribute("data");
        String componentType = (String) dataMap.get("componentType");
        Map<String, Object> componentMap = (Map<String, Object>) dataMap.get(componentType);
        return (String) componentMap.get("brandName");
    }
    
    /**
     * Prepare the brand specific breakpoints configuration path in CQ.
     *
     * @param brandName
     *            the brand name
     * @param fileName
     *            the fileName
     * @return breakpoints config file path.
     */
    public static String getConfigFilePath(String brandName, String fileName) {
        String tempBrandName = brandName;
        if (StringUtils.isBlank(brandName)) {
            tempBrandName = "unilever-iea";
        }
        return "/etc/ui/" + tempBrandName + "/clientlibs/core/core/config/" + fileName;
    }
    
    /**
     * Reads the file in CQ repository and returns as stream.
     *
     * @param filePath
     *            the file path
     * @param options
     *            the options
     * @return inputsteam
     */
    private static InputStream getFileAsInputStream(String filePath, Options options) {
        SlingHttpServletRequest request = (SlingHttpServletRequest) options.get(SlingBindings.REQUEST);
        InputStream inputStream = null;
        Resource resource = request.getResourceResolver().getResource(filePath + "/jcr:content");
        if (resource == null) {
            resource = request.getResourceResolver().getResource("/etc/ui/unilever-iea/clientlibs/core/core/config/breakpoint.json/jcr:content");
        }
        if (resource == null) {
            resource = request.getResourceResolver().getResource("/etc/ui/unilever-iea/clientlibs/core/core/config/breakpoints.js/jcr:content");
        }
        if (resource != null) {
            inputStream = resource.adaptTo(InputStream.class);
        }
        return inputStream;
        
    }
    
    /**
     * Reads the file in CQ repository and returns as stream.
     *
     * @param filePath
     *            the file path
     * @param options
     *            the options
     * @return inputsteam
     */
    private static InputStream getFileAsInputStream(String filePath, ResourceResolver resolver) {
        InputStream inputStream = null;
        Resource resource = resolver.getResource(filePath + "/jcr:content");
        if (resource == null) {
            resource = resolver.getResource("/etc/ui/unilever-iea/clientlibs/core/core/config/breakpoint.json/jcr:content");
        }
        if (resource == null) {
            resource = resolver.getResource("/etc/ui/unilever-iea/clientlibs/core/core/config/breakpoints.js/jcr:content");
        }
        if (resource != null) {
            inputStream = resource.adaptTo(InputStream.class);
        }
        return inputStream;
        
    }
    
}
