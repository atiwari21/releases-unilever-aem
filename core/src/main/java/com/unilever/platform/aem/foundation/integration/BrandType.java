/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

/**
 * The Class BrandType.
 */
public class BrandType {
 
 /** The name. */
 protected String name;
 
 /** The brandId id. */
 protected String brandId;
 
 /**
  * Gets the value of the name property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getName() {
  return name;
 }
 
 /**
  * Sets the value of the name property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setName(String value) {
  this.name = value;
 }
 
 /**
  * Gets the value of the externalId property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getBrandId() {
  return brandId;
 }
 
 /**
  * Sets the value of the externalId property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setBrandId(String value) {
  this.brandId = value;
 }
 
}
