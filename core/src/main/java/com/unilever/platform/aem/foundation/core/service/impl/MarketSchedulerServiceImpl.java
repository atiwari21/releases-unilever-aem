/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.oak.commons.PropertiesUtil;
import org.apache.felix.scr.annotations.Reference;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.day.cq.workflow.WorkflowService;
import com.day.cq.workflow.exec.Workflow;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

import java.util.List;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.Session;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.scheduler.Scheduler;

/**
 * Scheduler Service
 */
@Component(label = "Unilever Market Workflow Scheduler Service", immediate = true, metatype = true)
@Service(value = Runnable.class)
public class MarketSchedulerServiceImpl implements Runnable {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(MarketSchedulerServiceImpl.class);
 /** The Constant DEFAULT_SERVICE_ENABLED. */
 private static final boolean DEFAULT_SERVICE_ENABLED = false;
 
 /** The Constant DEFAULT_SCHEDULER_EXPRESSION. */
 private static final String DEFAULT_SCHEDULER_EXPRESSION = "0 0 0/8 * * ?";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant SERVICE_ENABLED. */
 @Property(label = "Enabled", description = "Enable/Disable the Scheduled Service", boolValue = true)
 private static final String SERVICE_ENABLED = "service.enabled";
 
 /** The Constant SCHEDULER_EXPRESSION. */
 @Property(label = "Cron expression for recipe scheduleder", value = DEFAULT_SCHEDULER_EXPRESSION)
 private static final String SCHEDULER_EXPRESSION = "scheduler.expression";
 
 /** The Constant SCHEDULER_CONCURRENT. */
 @Property(label = "Allow concurrent executions", boolValue = false)
 private static final String SCHEDULER_CONCURRENT = "scheduler.concurrent";
 
 @Reference
 private WorkflowService workflowService;
 @Reference
 private ResourceResolverFactory resourceResolverFactory;
 @Reference
 Replicator replicator;
 /** The enabled. */
 private boolean enabled = true;
 private static final String MULTI_PRODUCT_MODEL = "/etc/workflow/models/adding-multiple-products-workflow/jcr:content/model";
 private static final String CREATE_PAGE_MODEL = "/etc/workflow/models/create-a-new-article-page-workflow/jcr:content/model";
 private static final String PAGES_LIST = "pageList";
 private static final String TASK_STATUS = "status";
 
 @SuppressWarnings("unchecked")
 @Activate
 protected void activate(final ComponentContext componentContext) throws Exception {
  final Map<String, String> properties = (Map<String, String>) componentContext.getProperties();
  if (properties != null) {
   this.enabled = PropertiesUtil.toBoolean(properties.get(SERVICE_ENABLED), DEFAULT_SERVICE_ENABLED);
  }
 }
 
 @Override
 public void run() {
  
  if (enabled) {
   
   try {
    
    checkPublishedState();
    
   } catch (Exception e) {
    LOGGER.error("error while fetching resource resolver. ", e);
   }
  }
 }
 
 // Logic to check the published states of the page
 private void checkPublishedState() {
  
  try {
   
   String[] states = { "RUNNING" };
   
   ResourceResolver resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
   Session session = resourceResolver.adaptTo(Session.class);
   
   com.day.cq.workflow.WorkflowSession wfSession = workflowService.getWorkflowSession(session);
   com.day.cq.workflow.exec.Workflow[] workflow = wfSession.getWorkflows(states);
   
   for (Workflow wf : workflow) {
    
    if (wf.getWorkflowModel().getId().equalsIgnoreCase(MULTI_PRODUCT_MODEL) || wf.getWorkflowModel().getId().equalsIgnoreCase(CREATE_PAGE_MODEL)) {
     
     for (int k = 0; k < wf.getWorkItems().size(); k++) {
      int count = 0;
      if (wf.getWorkItems().get(0).getMetaDataMap().get("taskId") != null) {
       String taskPath = wf.getWorkItems().get(0).getMetaDataMap().get("taskId").toString();
       Resource taskNodeRes = resourceResolver.getResource(taskPath);
       Node taskNode = taskNodeRes.adaptTo(Node.class);
       List<String> pagesList = new ArrayList<String>(Arrays.asList(taskNode.getProperty(PAGES_LIST).getValue().toString().split(",")));
       Boolean[] allStatus = new Boolean[pagesList.size()];
       for (int l = 0; l < pagesList.size(); l++) {
        ReplicationStatus status = replicator.getReplicationStatus(session, pagesList.get(l));
        allStatus[count] = status.isActivated();
        count++;
       }
       
       if (Arrays.asList(allStatus).contains(true)) {
        
        wfSession.complete(wf.getWorkItems().get(k), wfSession.getRoutes(wf.getWorkItems().get(k)).get(0));
        if (taskNode.getProperty(TASK_STATUS) != null) {
         taskNode.setProperty(TASK_STATUS, "COMPLETE");
         taskNode.setProperty("comment", "Completed By Scheduler");
         taskNode.setProperty("completedBy", "Scheduler Service");
         taskNode.setProperty("endTime", Calendar.getInstance());
         session.save();
        }
       }
       
      }
     }
    }
   }
  } catch (Exception e) {
   LOGGER.error("error while checking the state. ", e.getMessage() + ", Exception trace:" + e);
  }
  
 }
 
}
