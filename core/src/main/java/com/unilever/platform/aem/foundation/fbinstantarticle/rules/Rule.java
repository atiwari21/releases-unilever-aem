/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class Rule.
 */
public abstract class Rule {
 
 /**
  * Matches.
  *
  * @param context
  *         the context
  * @param node
  *         the node
  * @return true, if successful
  */
 public boolean matches(Container context, Node node) {
  return matchesContext(context) && matchesNode(node);
 }
 
 /**
  * Matches context.
  *
  * @param context
  *         the context
  * @return true, if successful
  */
 public abstract boolean matchesContext(Container context);
 
 /**
  * Matches node.
  *
  * @param node
  *         the node
  * @return true, if successful
  */
 public abstract boolean matchesNode(Node node);
 
 /**
  * Apply.
  *
  * @param transformer
  *         the transformer
  * @param container
  *         the container
  * @param node
  *         the node
  * @return the container
  */
 public abstract Container apply(Transformer transformer, Container container, Node node);
 
 /**
  * Gets the context class.
  *
  * @return the context class
  */
 public abstract String[] getContextClass();
 
 /**
  * Retrieve property.
  *
  * @param configuration
  *         the configuration
  * @param propertyName
  *         the property name
  * @return the object
  */
 public static Object retrieveProperty(JSONObject configuration, String propertyName) {
  if (configuration.has(propertyName)) {
   return configuration.get(propertyName);
  } else if (configuration.has("properties")) {
   return configuration.getJSONObject("properties").get(propertyName);
  }
  return null;
 }
 
 /**
  * Auxiliary method to extract full qualified class name.
  * 
  * @return string The full qualified name of class
  */
 public String getClassName() {
  return this.getClassName();
 }
}
