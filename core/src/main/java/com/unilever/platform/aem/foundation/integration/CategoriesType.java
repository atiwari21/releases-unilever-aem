/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class CategoriesType.
 */
public class CategoriesType {
 
 /** The category. */
 protected List<CategoryType> category;
 
 /**
  * Gets the value of the category property.
  * 
  * <p>
  * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be
  * present inside the JAXB object. This is why there is not a <CODE>set</CODE> method for the category property.
  * 
  * <p>
  * For example, to add a new item, do as follows:
  * 
  * <pre>
  * getCategory().add(newItem);
  * </pre>
  * 
  * 
  * <p>
  * Objects of the following type(s) are allowed in the list {@link CategoryType }
  *
  * @return the category
  */
 public List<CategoryType> getCategory() {
  if (category == null) {
   category = new ArrayList<CategoryType>();
  }
  return this.category;
 }
 
}
