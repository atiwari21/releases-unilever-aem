/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.servlet.ServletException;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class UpdateConfig.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "updateconfig" }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UpdateConfig", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the content's properties ", propertyPrivate = false) })
public class UpdateConfig extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 782708002977083726L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UpdateConfig.class);
 
 /** The Constant MSM_MOVED. */
 private static final String MSM_MOVED = "_msm_moved";
 
 /** The Constant PAGE_PATH. */
 private static final String PAGE_PATH = "pagePath";
 
 /** The Constant CATEGORY. */
 private static final String CATEGORY = "category";
 
 /** The Constant CONFIGURATION. */
 private static final String CONFIGURATION = "configuration";
 
 /** The Constant PAR. */
 private static final String PAR = "par";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "text/html";
 
 /**
  * The Constant "<br/>
  * ".
  */
 private static final String BR = "<br/>";
 
 /** The Constant HTML_FORM. */
 private static final String HTML_FORM = "<!DOCTYPE html>" + "<html>" + "<head>" + "<meta charset=\"ISO-8859-1\">"
   + "<title>Content Migration</title>" + "</head>" + "<body>" + "<form action=\"\" method=\"get\">" + "<table>" + "<tr>" + "<tr>"
   + "<td><label>Page Path</label></td>" + "<td><input type=\"text\" name=\"pagePath\"></td>" + "</tr>" + "<tr>" + "<td><input type=\"submit\"></td>"
   + "</tr>" + "</table>" + "</form>" + "</body>" + "</html>";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  
  response.setContentType(CONTENT_TYPE);
  PrintWriter out = response.getWriter();
  String pagePath = request.getParameter(PAGE_PATH);
  if (pagePath == null) {
   out.print(HTML_FORM);
  } else {
   
   ResourceResolver resourceResolver = request.getResource().getResourceResolver();
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   
   Session session = resourceResolver.adaptTo(Session.class);
   Page configPage = pageManager.getPage(pagePath);
   
   Resource jcrContentResource = configPage.getContentResource();
   
   Resource parResource = jcrContentResource.getChild(PAR);
   
   Iterable<Resource> configResourceItr = parResource.getChildren();
   
   List<String> configResourceNameList = new ArrayList<String>();
   
   for (Resource configResource : configResourceItr) {
    configResourceNameList.add(configResource.getName());
    try {
     Node configNode = configResource.adaptTo(Node.class);
     configNode.setProperty("key", (Value[]) null);
     configNode.setProperty("value", (Value[]) null);
     session.save();
    } catch (PathNotFoundException e) {
     out.println(ExceptionUtils.getFullStackTrace(e));
     LOGGER.error("PathNotFoundException ", e);
    } catch (RepositoryException e) {
     out.println(ExceptionUtils.getFullStackTrace(e));
     LOGGER.error("RepositoryException ", e);
    }
   }
   for (Resource configResource : configResourceItr) {
    String name = configResource.getName();
    LOGGER.debug("Name : {}", name);
    String category = MapUtils.getString(configResource.getValueMap(), CATEGORY, null);
    LOGGER.debug("category : {}", category);
    String msmMovedName = name + MSM_MOVED;
    if (configResourceNameList.contains(msmMovedName)) {
     updateNode(out, session, parResource, configResource, name, category, msmMovedName);
    }
   }
  }
  
  out.flush();
  out.close();
 }
 
 /**
  * Update node.
  *
  * @param out
  *         the out
  * @param session
  *         the session
  * @param parResource
  *         the par resource
  * @param configResource
  *         the config resource
  * @param name
  *         the name
  * @param category
  *         the category
  * @param msmMovedName
  *         the msm moved name
  */
 private void updateNode(PrintWriter out, Session session, Resource parResource, Resource configResource, String name, String category,
   String msmMovedName) {
  Resource msmMovedConfigResource = parResource.getChild(msmMovedName);
  ValueMap propertiesMap = msmMovedConfigResource.getValueMap();
  String[] propertyValObj = propertiesMap.get(CONFIGURATION, String[].class);
  String[] oldConfiguration = configResource.getValueMap().get(CONFIGURATION, String[].class);
  LOGGER.debug("msmMovedConfigConfigurationStr : {}", propertyValObj);
  if (category != null && propertyValObj != null && category.equals(MapUtils.getString(propertiesMap, CATEGORY))) {
   Node configNode = configResource.adaptTo(Node.class);
   try {
    
    configNode.setProperty(CONFIGURATION, (Value[]) null);
    configNode.setProperty("key", (Value[]) null);
    configNode.setProperty("value", (Value[]) null);
    configNode.setProperty(CONFIGURATION, (String[]) propertyValObj);
    LOGGER.debug("<b>Node updated and removed msm moved</b>: {}", name);
    LOGGER.debug("<b>category </b>: {}", category);
    session.removeItem(msmMovedConfigResource.getPath());
    session.save();
    if (!StringUtils.join(propertyValObj, ",").equals(StringUtils.join(oldConfiguration, ","))) {
     out.print("<b>Node updated </b>: " + name + BR);
     out.print("<b>Node updated with category </b>: " + category + BR);
     out.print("<b>configuration Value before update</b>: " + StringUtils.join(oldConfiguration, ",") + BR);
     out.print("<b>configuration Value after update &nbsp;&nbsp;</b>: " + StringUtils.join(propertyValObj, ",") + "<br/><br/>");
     
    }
    
   } catch (PathNotFoundException e) {
    out.println(ExceptionUtils.getFullStackTrace(e));
    LOGGER.error("PathNotFoundException ", e);
   } catch (RepositoryException e) {
    out.println(ExceptionUtils.getFullStackTrace(e));
    LOGGER.error("RepositoryException ", e);
   }
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
 
}
