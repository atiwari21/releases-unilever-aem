/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Product.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "sourceId", "category", "identifiers", "names", "descriptions", "pageUrls", "imageUrls" })
public class Product {
 
 /** The source id. */
 @XmlElement(name = "SourceId", required = true)
 protected SourceId sourceId;
 
 /** The category. */
 @XmlElement(name = "Category", required = true)
 protected ProductCategory category;
 
 /** The identifiers. */
 @XmlElement(name = "Identifiers", required = true)
 protected Identifiers identifiers;
 
 /** The names. */
 @XmlElement(name = "Names", required = true)
 protected Names names;
 
 /** The descriptions. */
 @XmlElement(name = "Descriptions", required = true)
 protected Descriptions descriptions;
 
 /** The page urls. */
 @XmlElement(name = "PageUrls", required = true)
 protected PageUrls pageUrls;
 
 /** The image urls. */
 @XmlElement(name = "ImageUrls", required = true)
 protected ImageUrls imageUrls;
 
 /**
  * Gets the source id.
  *
  * @return the source id
  */
 public SourceId getSourceId() {
  return sourceId;
 }
 
 /**
  * Sets the source id.
  *
  * @param value
  *         the new source id
  */
 public void setSourceId(SourceId value) {
  this.sourceId = value;
 }
 
 /**
  * Gets the category.
  *
  * @return the category
  */
 public ProductCategory getCategory() {
  return category;
 }
 
 /**
  * Sets the category.
  *
  * @param value
  *         the new category
  */
 public void setCategory(ProductCategory value) {
  this.category = value;
 }
 
 /**
  * Gets the identifiers.
  *
  * @return the identifiers
  */
 public Identifiers getIdentifiers() {
  return identifiers;
 }
 
 /**
  * Sets the identifiers.
  *
  * @param value
  *         the new identifiers
  */
 public void setIdentifiers(Identifiers value) {
  this.identifiers = value;
 }
 
 /**
  * Gets the names.
  *
  * @return the names
  */
 public Names getNames() {
  return names;
 }
 
 /**
  * Sets the names.
  *
  * @param value
  *         the new names
  */
 public void setNames(Names value) {
  this.names = value;
 }
 
 /**
  * Gets the descriptions.
  *
  * @return the descriptions
  */
 public Descriptions getDescriptions() {
  return descriptions;
 }
 
 /**
  * Sets the descriptions.
  *
  * @param value
  *         the new descriptions
  */
 public void setDescriptions(Descriptions value) {
  this.descriptions = value;
 }
 
 /**
  * Gets the page urls.
  *
  * @return the page urls
  */
 public PageUrls getPageUrls() {
  return pageUrls;
 }
 
 /**
  * Sets the page urls.
  *
  * @param value
  *         the new page urls
  */
 public void setPageUrls(PageUrls value) {
  this.pageUrls = value;
 }
 
 /**
  * Gets the image urls.
  *
  * @return the image urls
  */
 public ImageUrls getImageUrls() {
  return imageUrls;
 }
 
 /**
  * Sets the image urls.
  *
  * @param value
  *         the new image urls
  */
 public void setImageUrls(ImageUrls value) {
  this.imageUrls = value;
 }
 
}
