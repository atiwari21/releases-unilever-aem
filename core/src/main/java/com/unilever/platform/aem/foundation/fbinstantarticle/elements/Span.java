/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The Class Span.
 */

public class Span extends TextContainer {
 
 /** The text. */
 private String text;
 
 /**
  * Instantiates a new span.
  */
 private Span() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static Span create() {
  Span h1 = new Span();
  return h1;
 }
 
 /**
  * Gets the text.
  *
  * @return the text
  */
 public String getText() {
  return text;
 }
 
 /**
  * Sets the unescaped text within the blockquote.
  *
  * @param text
  *         the text
  * @return this
  */
 public Span withText(String text) {
  this.text = text;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement()
  */
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element span = document.createElement("span");
  span.append(textToDOMDocumentFragment(document));
  return span;
  
 }
 
}