/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Class LineBreak.
 */
public class LineBreak extends FormattedText {
 private LineBreak() {
  
 }
 
 public static LineBreak create() {
  return new LineBreak();
 }
 
 @Override
 public TextContainer appendText(Object child) {
  return this;
 }
 
 @Override
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!this.isValid()) {
   return null;
  }
  return document.createElement("br");
 }
 
 @Override
 public boolean isValid() {
  return true;
 }
 
}
