/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 * The Class JSONGetter.
 */
public class JSONGetter extends ChildrenGetter {
 
 /** The attribute. */
 protected String attribute;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.ElementGetter#createFrom(org.json.JSONObject)
  */
 public ElementGetter createFrom(JSONObject configuration) {
  if (configuration.getString("selector") != null) {
   return this.withSelector(configuration.getString("selector"));
  }
  if (configuration.getString("attribute") != null) {
   return this.withAttribute(configuration.getString("attribute"));
  }
  return this;
 }
 
 /**
  * With attribute.
  *
  * @param attribute
  *         the attribute
  * @return the element getter
  */
 public ElementGetter withAttribute(String attribute) {
  this.attribute = attribute;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.ChildrenGetter#get(org.jsoup.nodes.Element)
  */
 public Object get(Node node) {
  String content = null;
  Elements elements = findAll(node, this.selector);
  if (CollectionUtils.isNotEmpty(elements)) {
   Element element = elements.get(0);
   if (this.attribute != null) {
    content = element.attr(this.attribute);
   } else {
    content = element.text();
   }
  }
  if (StringUtils.isNotBlank(content)) {
   return new JSONObject(content);
  }
  return null;
 }
 
}
