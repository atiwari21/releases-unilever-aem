/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The Class Image.
 */
public class ImageDTO {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ImageDTO.class);
 
 /** The Constant EXTERNALIZER_DOMAIN_KEY. */
 private static final String EXTERNALIZER_DOMAIN_KEY = "externalizer_domain_key";
 
 /** The Constant BRAND. */
 private static final String BRAND = "brand";
 
 /** The Constant FULL_STOP. */
 private static final String FULL_STOP = ".";
 
 /** The Constant URL. */
 private static final String URL = "url";
 
 /** The Constant for "/". */
 private static final String FORWARD_SLASH = "/";
 
 /** Constant for jpeg. */
 private static final String JPEG = "jpeg";
 
 /** /** Constant for jpg. */
 private static final String JPG = "jpg";
 
 /** Constant for jpeg. */
 private static final String GIF = "gif";
 
 /** Constant for jpeg. */
 private static final String PNG = "png";
 
 /** The Constant FILE_NAME. */
 private static final String FILE_NAME = "fileName";
 
 /** The Constant IS_NOT_ADAPTIVE_IMAGE. */
 private static final String IS_NOT_ADAPTIVE_IMAGE = "isNotAdaptiveImage";
 
 /** The Constant EXTENSION. */
 private static final String EXTENSION = "extension";
 
 /** The Constant ALT_IMAGE. */
 private static final String ALT_IMAGE = "altImage";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant PATH. */
 private static final String PATH = "path";
 
 /** The url. */
 private String url;
 
 /** The file name. */
 private String fileName;
 
 /** The is not adaptive image. */
 private boolean isNotAdaptiveImage;
 
 /** The extension. */
 private String extension;
 
 /** The alt image. */
 private String altImage;
 
 /** The title. */
 private String title;
 
 /** The path. */
 private String path;
 
 /**
  * Instantiates a new image dto.
  *
  * @param fileReference
  *         the file reference
  * @param altImage
  *         the alt image
  * @param title
  *         the title
  * @param slingRequest
  *         the slingRequest
  * @param currentPage
  *         the current page
  */
 public ImageDTO(String fileReference, String altImage, String title, Page currentPage, SlingHttpServletRequest slingRequest) {
  LOGGER.info("Inside constructor of Image");
  
  this.url = StringUtils.isBlank(fileReference) ? StringUtils.EMPTY : getStaticFileFullPath(fileReference, currentPage, slingRequest);
  
  this.fileName = StringUtils.EMPTY;
  this.isNotAdaptiveImage = false;
  this.extension = StringUtils.isBlank(fileReference) ? StringUtils.EMPTY
    : getExtensionFromMimeType(fileReference.substring(fileReference.lastIndexOf('.') + 1, fileReference.length()));
  this.altImage = StringUtils.isBlank(altImage) ? fileName : altImage;
  this.title = StringUtils.isBlank(title) ? fileName : title;
  this.path = this.url;
 }
 
 /**
  * Fetch image file name.
  *
  * @param fileReference
  *         the file reference
  * @return the string
  */
 public static String fetchImageFileName(String fileReference) {
  String fileName = StringUtils.EMPTY;
  if (StringUtils.isNotEmpty(fileReference) && StringUtils.contains(fileReference, FORWARD_SLASH) && StringUtils.contains(fileReference, FULL_STOP)) {
   fileName = fileReference.substring(fileReference.lastIndexOf(FORWARD_SLASH) + 1, fileReference.lastIndexOf(FULL_STOP));
  }
  return fileName;
 }
 
 /**
  * Gets the extension from mime type.
  *
  * @param mimeType
  *         the mime type
  * @return the extension from mime type
  */
 public static String getExtensionFromMimeType(String mimeType) {
  
  String extension = StringUtils.EMPTY;
  if (StringUtils.isNotEmpty(mimeType)) {
   if (StringUtils.containsIgnoreCase(mimeType, JPEG) || StringUtils.containsIgnoreCase(mimeType, JPG)) {
    extension = JPG;
   } else if (StringUtils.containsIgnoreCase(mimeType, GIF)) {
    extension = GIF;
   } else if (StringUtils.containsIgnoreCase(mimeType, PNG)) {
    extension = PNG;
   }
  }
  return extension;
 }
 
 /**
  * Gets the URL with updated protocol.
  *
  * @param fullURL
  *         the full url
  * @param request
  *         the request
  * @return the URL with updated protocol
  */
 public static String getURLWithUpdatedProtocol(String fullURL, HttpServletRequest request) {
  String protocol = request.getHeader("X-Forwarded-AEM-Proto") != null ? request.getHeader("X-Forwarded-AEM-Proto") : "http";
  return fullURL.replaceAll("http://", protocol + "://");
 }
 
 /**
  * Gets the static file full path.
  *
  * @param resourcePath
  *         the resource path
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the static file full path
  */
 public static String getStaticFileFullPath(String resourcePath, Page page, SlingHttpServletRequest slingRequest) {
  LOGGER.debug("Inside getStaticFileFullPath of ComponentUtil");
  
  String fullPath = StringUtils.EMPTY;
  
  if (StringUtils.startsWith(resourcePath, "/") && page != null) {
   ConfigurationService configurationService = page.adaptTo(ConfigurationService.class);
   ResourceResolver resourceResolver = page.getContentResource().getResourceResolver();
   Externalizer externalizer = page.getContentResource().getResourceResolver().adaptTo(Externalizer.class);
   if (externalizer == null) {
    LOGGER.error("Missing Externalizer service");
    return resourcePath;
   } else {
    String externalizerKey = StringUtils.EMPTY;
    try {
     externalizerKey = configurationService.getConfigValue(page, BRAND, EXTERNALIZER_DOMAIN_KEY);
    } catch (ConfigurationNotFoundException ex) {
     LOGGER.error("ConfigurationNotFoundException ", ex);
    }
    LOGGER.debug("ResourcePath = " + resourcePath);
    if (!StringUtils.isBlank(externalizerKey)) {
     try {
      fullPath = externalizer.externalLink(resourceResolver, externalizerKey, resourcePath);
     } catch (Exception e) {
      LOGGER.error("Exception in getting the externalizer url ", e);
     }
    }
    
   }
  }
  
  if (slingRequest != null) {
   fullPath = getURLWithUpdatedProtocol(fullPath, slingRequest);
  }
  return StringUtils.isBlank(fullPath) ? resourcePath : fullPath;
 }
 
 /**
  * Gets the url.
  *
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Sets the url.
  *
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
 
 /**
  * Gets the file name.
  *
  * @return the file name
  */
 public String getFileName() {
  return fileName;
 }
 
 /**
  * Sets the file name.
  *
  * @param fileName
  *         the new file name
  */
 public void setFileName(String fileName) {
  this.fileName = fileName;
 }
 
 /**
  * Checks if is not adaptive image.
  *
  * @return true, if is not adaptive image
  */
 public boolean isNotAdaptiveImage() {
  return isNotAdaptiveImage;
 }
 
 /**
  * Sets the not adaptive image.
  *
  * @param isNotAdaptiveImage
  *         the new not adaptive image
  */
 public void setNotAdaptiveImage(boolean isNotAdaptiveImage) {
  this.isNotAdaptiveImage = isNotAdaptiveImage;
 }
 
 /**
  * Gets the extension.
  *
  * @return the extension
  */
 public String getExtension() {
  return extension;
 }
 
 /**
  * Sets the extension.
  *
  * @param extension
  *         the new extension
  */
 public void setExtension(String extension) {
  this.extension = extension;
 }
 
 /**
  * Gets the alt image.
  *
  * @return the alt image
  */
 public String getAltImage() {
  return altImage;
 }
 
 /**
  * Sets the alt image.
  *
  * @param altImage
  *         the new alt image
  */
 public void setAltImage(String altImage) {
  this.altImage = altImage;
 }
 
 /**
  * Gets the title.
  *
  * @return the title
  */
 public String getTitle() {
  return title;
 }
 
 /**
  * Sets the title.
  *
  * @param title
  *         the new title
  */
 public void setTitle(String title) {
  this.title = title;
 }
 
 /**
  * Gets the path.
  *
  * @return the path
  */
 public String getPath() {
  return path;
 }
 
 /**
  * Sets the path.
  *
  * @param path
  *         the new path
  */
 public void setPath(String path) {
  this.path = path;
 }
 
 /**
  * Convert to map.
  *
  * @return the map
  */
 public Map<String, Object> convertToMap() {
  Map<String, Object> map = new LinkedHashMap<String, Object>();
  map.put(URL, this.url);
  map.put(FILE_NAME, this.fileName);
  map.put(IS_NOT_ADAPTIVE_IMAGE, this.isNotAdaptiveImage);
  map.put(EXTENSION, this.extension);
  map.put(ALT_IMAGE, this.altImage);
  map.put(TITLE, this.title);
  map.put(PATH, this.path);
  return map;
 }
 
}
