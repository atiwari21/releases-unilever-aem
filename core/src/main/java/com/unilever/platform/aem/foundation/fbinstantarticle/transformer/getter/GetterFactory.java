/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A factory for creating Getter objects.
 */
public class GetterFactory {
 
 private static final Logger LOGGER=LoggerFactory.getLogger(GetterFactory.class);
 
 /** The Constant TYPE_STRING_GETTER. */
 public static final String TYPE_STRING_GETTER = "string";
 
 /** The Constant TYPE_INTEGER_GETTER. */
 public static final String TYPE_INTEGER_GETTER = "int";
 
 /** The Constant TYPE_CHILDREN_GETTER. */
 public static final String TYPE_CHILDREN_GETTER = "children";
 
 /** The Constant TYPE_ELEMENT_GETTER. */
 public static final String TYPE_ELEMENT_GETTER = "element";
 
 /** The Constant TYPE_CONSTANT_GETTER. */
 public static final String TYPE_CONSTANT_GETTER = "constant";
 
 /** The Constant TYPE_FRAGMENT_GETTER. */
 public static final String TYPE_FRAGMENT_GETTER = "fragment";
 
 /** The Constant TYPE_NEXTSIBLING_GETTER. */
 public static final String TYPE_NEXTSIBLING_GETTER = "sibling";
 
 /** The Constant TYPE_NEXTSIBLINGELEMENT_GETTER. */
 public static final String TYPE_NEXTSIBLINGELEMENT_GETTER = "next-sibling-element-of";
 
 /** The Constant TYPE_EXISTS_GETTER. */
 public static final String TYPE_EXISTS_GETTER = "exists";
 
 /** The Constant TYPE_JSON_GETTER. */
 public static final String TYPE_JSON_GETTER = "json";
 
 /** The Constant TYPE_XPATH_GETTER. */
 public static final String TYPE_XPATH_GETTER = "xpath";
 
 /** The Constant TYPE_MULTIPLEELEMENTS_GETTER. */
 public static final String TYPE_MULTIPLEELEMENTS_GETTER = "multiple";
 
 /**
  * Creates the.
  *
  * @param getterConfiguration
  *         the getter configuration
  */
 public static AbstractGetter create(JSONObject getterConfiguration) {
  Map<String, String> getterClasses = new HashMap<String, String>();
  setGetters(getterClasses);
  String classNameType = getterConfiguration.getString("type");
  String className = getterClasses.get(classNameType);
  Class<?>[] paramJSON = new Class[1];
  paramJSON[0] = JSONObject.class;
  AbstractGetter obj = null;
  try {
   Class<?> cls = Class.forName(className);
   Constructor<?> constructor = cls.getConstructor();
   obj = (AbstractGetter) constructor.newInstance(new Object[] {});
   Method method = cls.getDeclaredMethod("createFrom", paramJSON);
   method.invoke(obj, getterConfiguration);
  } catch (Exception e) {
   LOGGER.error("Error in creating Getter", e.getMessage());
   LOGGER.debug("Error in creating Getter", e);
  }
  return obj;
  
 }
 
 private static void setGetters(Map<String, String> getterClasses) {
  getterClasses.put(TYPE_STRING_GETTER, StringGetter.class.getName());
  getterClasses.put(TYPE_INTEGER_GETTER, IntegerGetter.class.getName());
  getterClasses.put(TYPE_CHILDREN_GETTER, ChildrenGetter.class.getName());
  getterClasses.put(TYPE_ELEMENT_GETTER, ElementGetter.class.getName());
  getterClasses.put(TYPE_FRAGMENT_GETTER, FragmentGetter.class.getName());
  getterClasses.put(TYPE_CONSTANT_GETTER, ConstantGetter.class.getName());
  getterClasses.put(TYPE_NEXTSIBLING_GETTER, NextSiblingGetter.class.getName());
  getterClasses.put(TYPE_NEXTSIBLINGELEMENT_GETTER, NextSiblingElementGetter.class.getName());
  getterClasses.put(TYPE_EXISTS_GETTER, ExistsGetter.class.getName());
  getterClasses.put(TYPE_JSON_GETTER, JSONGetter.class.getName());
  getterClasses.put(TYPE_XPATH_GETTER, XpathGetter.class.getName());
  getterClasses.put(TYPE_MULTIPLEELEMENTS_GETTER, MultipleElementsGetter.class.getName());
 }
}
