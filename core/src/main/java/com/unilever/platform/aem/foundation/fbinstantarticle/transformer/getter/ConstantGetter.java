/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

public class ConstantGetter extends AbstractGetter {
 
 protected String value;
 
 @Override
 public AbstractGetter createFrom(JSONObject configuration) {
  return this.withValue(configuration.getString("value"));
 }
 
 public AbstractGetter withValue(String value) {
  this.value = value;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.AbstractGetter#get(org.jsoup.nodes.Element)
  */
 @Override
 public Object get(Node node) {
  return this.value;
 }
 
}
