/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.common.cache.LoadingCache;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Class ConfigurationHelper.
 */
public class ConfigurationHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationServiceImpl.class);
 
 /** The global config page name. */
 private static String globalConfigPageName;
 
 /**
  * Instantiates a new configuration helper.
  */
 private ConfigurationHelper() {
  
 }
 
 /**
  * Gets the page configurations. The method is responsible for returning the all the page configuration category wise
  *
  * @param brandMarketBean
  *         the brand market bean
  * @param configCache
  *         the config cache
  * @return the page configurations
  */
 static Map<String, Map<String, String>> getPageConfigurations(BrandMarketBean brandMarketBean,
   LoadingCache<String, Map<String, String>> configCache) {
  Map<String, Map<String, String>> pageConfigMap = new HashMap<String, Map<String, String>>();
  String pageConfigKey = getPageConfigKey(brandMarketBean);
  Map<String, String> map = new TreeMap<String, String>();
  try {
   map = configCache.get(ConfigurationConstants.CONF_CACHE_NAME);
  } catch (ExecutionException e) {
   LOGGER.error("Exception in getting configuration from cache", e);
  }
  pageConfigMap = generateMap(pageConfigMap, pageConfigKey, map);
  
  return pageConfigMap;
 }
 
 /**
  * Generate map.
  *
  * @param pageConfigMap
  *         the page config map
  * @param pageConfigKey
  *         the page config key
  * @param map
  *         the map
  * @return the map
  */
 private static Map<String, Map<String, String>> generateMap(Map<String, Map<String, String>> pageConfigMap, String pageConfigKey,
   Map<String, String> map) {
  Iterator<String> configKeysItr = map.keySet().iterator();
  while (configKeysItr.hasNext()) {
   String key = configKeysItr.next();
   String value = map.get(key);
   String[] keyTokensArr = key.split(pageConfigKey);
   String catKey = keyTokensArr != null && keyTokensArr.length > 1 ? keyTokensArr[1] : null;
   String[] catKeyArr = catKey != null ? catKey.split("\\.") : null;
   if (catKeyArr != null && catKeyArr.length > 1) {
    String categoryName = catKeyArr[0];
    String originalKey = getOriginalKey(catKeyArr);
    Map<String, String> catKeyValueMap = pageConfigMap.get(categoryName) != null ? pageConfigMap.get(categoryName) : new HashMap<String, String>();
    catKeyValueMap.put(originalKey, value);
    pageConfigMap.put(categoryName, catKeyValueMap);
   }
  }
  return pageConfigMap;
 }
 
 /**
  * Gets the original key.
  *
  * @param catKeyArr
  *         the cat key arr
  * @return the original key
  */
 public static String getOriginalKey(String[] catKeyArr) {
  String originalKey = "";
  for (int i = 1; i < catKeyArr.length; ++i) {
   originalKey = originalKey + catKeyArr[i];
   if (i < catKeyArr.length - 1) {
    originalKey = originalKey + CommonConstants.DOT_CHAR_LITERAL;
   }
  }
  return originalKey;
  
 }
 
 /**
  * Gets the category configuration.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @param category
  *         the category
  * @param configCache
  *         the config cache
  * @return the category configuration
  */
 static Map<String, String> getCategoryConfiguration(BrandMarketBean brandMarketBean, String category,
   LoadingCache<String, Map<String, String>> configCache) {
  Map<String, String> catConfigMap = new LinkedHashMap<String, String>();
  String pageConfigKey = getPageConfigKey(brandMarketBean);
  String catConfigKey = pageConfigKey + category + CommonConstants.DOT_CHAR_LITERAL;
  try {
   Map<String, String> map = configCache.get(ConfigurationConstants.CONF_CACHE_NAME);
   Iterator<String> itr = map.keySet().iterator();
   while (itr.hasNext()) {
    String key = itr.next();
    if (key.startsWith(catConfigKey)) {
     catConfigMap.put(key.split(catConfigKey)[1], map.get(key));
    } else if (!catConfigMap.isEmpty()) {
     break;
    }
   }
   
  } catch (ExecutionException e) {
   LOGGER.error("guawa cahe error {}", pageConfigKey, e);
  }
  
  return catConfigMap;
 }
 
 /**
  * Gets the config value.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @param category
  *         the category
  * @param key
  *         the key
  * @param configCache
  *         the config cache
  * @return the config value
  * @throws ConfigurationNotFoundException
  */
 static String getConfigValue(BrandMarketBean brandMarketBean, String category, String key, LoadingCache<String, Map<String, String>> configCache)
   throws ConfigurationNotFoundException {
  String value = null;
  String pageConfigKey = getPageConfigKey(brandMarketBean);
  String uniqueKey = pageConfigKey + category + CommonConstants.DOT_CHAR_LITERAL + key;
  Map<String, String> map = null;
  try {
   map = configCache.get(ConfigurationConstants.CONF_CACHE_NAME);
   value = map.get(uniqueKey);
   if (value == null) {
    throw new ConfigurationNotFoundException("Configuration not found for category: " + category + " and key: " + key);
   }
  } catch (ExecutionException e) {
   LOGGER.error("Exception in getting configuration from cache", e);
  }
  return value;
 }
 
 /**
  * Gets the page config key.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @return the page config key
  */
 private static String getPageConfigKey(BrandMarketBean brandMarketBean) {
  StringBuilder stb = new StringBuilder();
  return stb.append(brandMarketBean.getBrand()).append(CommonConstants.DOT_CHAR_LITERAL).append(brandMarketBean.getMarket())
    .append(CommonConstants.DOT_CHAR_LITERAL).toString();
 }
 
 /**
  * Gets the config pages. The method is responsible for returning the config pages of a page
  *
  * @param page
  *         the page
  * @return the config pages
  */
 static Page getConfigPage(Resource chidlResource) {
  ResourceResolver resourceResolver = chidlResource.getResourceResolver();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  LOGGER.info("Inside getConfigPages of ConfigurationHelper");
  String path = chidlResource.getPath();
  String globalConfigPagePath = new StringBuilder(path).append(CommonConstants.SLASH_CHAR_LITERAL).append(globalConfigPageName).toString();
  if (pageManager != null) {
   return pageManager.getPage(globalConfigPagePath);
  } else {
   LOGGER.info("pageManager is null, so returning null");
   return null;
  }
 }
 
 /**
  * Gets the resource resolver. The method is responsible for returning the ResourceResolver from ResourceResolverFactory for a perticular service
  *
  * @param resolverFactory
  *         the resolver factory
  * @param subServiceName
  *         the sub service name
  * @return the resource resolver
  * @throws LoginException
  *          the login exception
  */
 public static ResourceResolver getResourceResolver(ResourceResolverFactory resolverFactory, String subServiceName) throws LoginException {
  Map<String, Object> param = new HashMap<String, Object>();
  param.put(ResourceResolverFactory.SUBSERVICE, subServiceName);
  return resolverFactory != null ? resolverFactory.getServiceResourceResolver(param) : null;
 }
 
 /**
  * Gets the global config page name.
  *
  * @return the global config page name
  */
 static String getGlobalConfigPageName() {
  return globalConfigPageName;
 }
 
 /**
  * Sets the global config page name.
  *
  * @param globalConfigPageName
  *         the new global config page name
  */
 static void setGlobalConfigPageName(String globalConfigPageName) {
  ConfigurationHelper.globalConfigPageName = globalConfigPageName;
 }
 
 /**
  * Gets the property value array. The method is responsible for returning a string array of property value
  *
  * @param properties
  *         the properties
  * @param propertyKey
  *         the property key
  * @return the property value array
  */
 static String[] getPropertyValueArray(ValueMap properties, String propertyKey) {
  LOGGER.debug("Inside getPropertyValueArray");
  String[] propertyValObj = properties.get(propertyKey, String[].class);
  String[] propertyValArr = null;
  if (propertyValObj != null) {
   if (propertyValObj instanceof String[]) {
    propertyValArr = (String[]) propertyValObj;
   } else {
    propertyValArr = new String[] { propertyValObj.toString() };
   }
  }
  return propertyValArr;
 }
 
 /**
  * Checks if is valid page.
  *
  * @param page
  *         the page
  * @param unileverSiteRootPath
  *         the unilever site root path
  * @return true, if is valid page
  */
 public static boolean isValidPage(Page page, String unileverSiteRootPath) {
  boolean flag = false;
  String pagePath = page.getPath();
  if (pagePath.startsWith(unileverSiteRootPath)) {
   String[] remainingPathArr = pagePath.split(unileverSiteRootPath)[1].split(CommonConstants.SLASH_CHAR_LITERAL);
   if (remainingPathArr.length > ConfigurationConstants.PAGE_AND_UNI_DIFF) {
    flag = true;
   }
  }
  return flag;
 }
}
