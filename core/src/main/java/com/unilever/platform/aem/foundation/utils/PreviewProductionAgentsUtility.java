/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import org.apache.commons.lang.StringUtils;

import com.unilever.platform.aem.foundation.configuration.GlobalConfiguration;

/**
 * The Class PreviewProductionAgentsUtility.
 */
public class PreviewProductionAgentsUtility {
 /**
  * gets preview agents
  * 
  * @param globalConfiguration
  * @param agentManager
  * @return
  */
 public static String[] getPreviewAgents(GlobalConfiguration globalConfiguration) {
  String previewAgent = globalConfiguration.getConfigValue("publishPreviewAgents");
  String[] previewAgents = StringUtils.isNotBlank(previewAgent) ? StringUtils.split(previewAgent, ",") : new String[] {};
  return previewAgents;
 }
 
 /**
  * gets production agents
  * 
  * @param globalConfiguration
  * @param agentManager
  * @return
  */
 public static String[] getProductionAgents(GlobalConfiguration globalConfiguration) {
  String previewAgent = globalConfiguration.getConfigValue("publishPreviewAgents");
  String[] previewAgents = StringUtils.isNotBlank(previewAgent) ? StringUtils.split(previewAgent, ",") : new String[] {};
  String productionAgent = globalConfiguration.getConfigValue("publishProductionAgents");
  String[] productionAgents = StringUtils.isNotBlank(productionAgent) ? StringUtils.split(productionAgent, ",") : new String[] {};
  String[] agentIds = new String[previewAgents.length + productionAgents.length];
  for (int i = 0; i < previewAgents.length; i++) {
   agentIds[i] = previewAgents[i];
  }
  for (int j = 0; j < productionAgents.length; j++) {
   agentIds[j + previewAgents.length] = productionAgents[j];
  }
  return agentIds;
 }
}
