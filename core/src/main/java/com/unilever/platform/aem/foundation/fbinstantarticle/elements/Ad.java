/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Class Ad.
 */
public class Ad extends ElementWithHTML {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement()
  */
 @Override
 public Element toDOMElement() {
  return null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public Element toDOMElement(Document document) {
  return null;
 }
 
}
