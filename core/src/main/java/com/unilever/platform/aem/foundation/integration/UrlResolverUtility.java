/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The Class UrlResolverUtility.
 */
public final class UrlResolverUtility {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UrlResolverUtility.class);
 
 /** The Constant URL_MAPPINGS. */
 private static final String URL_MAPPINGS = "urlMappings";
 
 /**
  * Instantiates a new url resolver utility.
  */
 private UrlResolverUtility() {
  LOGGER.info("Url Resolver Utility Constructor Called.");
 }
 
 /**
  * Gets the resolved page path.
  *
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the resolved page path
  */
 public static String getResolvedPagePath(ConfigurationService configurationService, Page page) {
  
  Map<String, String> urlResolverMappings = null;
  String unResolvedPagePath = StringUtils.EMPTY;
  String resolvedPagePath = StringUtils.EMPTY;
  
  if (page != null) {
   unResolvedPagePath = getUnResolvedPagePath(page);
   resolvedPagePath = unResolvedPagePath;
   urlResolverMappings = getUrlResolverMap(configurationService, page);
  }
  
  try {
   if (urlResolverMappings != null && StringUtils.isNotBlank(unResolvedPagePath)) {
    for (String key : urlResolverMappings.keySet()) {
     if (unResolvedPagePath.startsWith(key)) {
      resolvedPagePath = unResolvedPagePath.replace(key, urlResolverMappings.get(key));
      break;
     }
    }
   }
  } catch (Exception e) {
   LOGGER.error("Unable to resolve page path : " + unResolvedPagePath, e);
  }
  return resolvedPagePath;
 }
 
 /**
  * This method returns path of a page replaced with alias name (if alias name has been set for that page).
  * 
  * @param page
  *         the page
  * @return pagePath
  */
 public static String getUnResolvedPagePath(Page page) {
  String pagePath = page.getPath();
  String unresolvedPagePath = pagePath;
  int depth = page.getDepth();
  String aliasPageName = StringUtils.EMPTY;
  for (int i = 0; i < depth; i++) {
   String pagePathLastName = unresolvedPagePath.substring(unresolvedPagePath.lastIndexOf("/") + 1);
   if (page != null) {
    aliasPageName = (String) page.getContentResource().getValueMap().get("sling:alias");
    if (StringUtils.isNotEmpty(aliasPageName)) {
     pagePath = pagePath.replace(pagePathLastName, aliasPageName);
    }
   }
   if (page != null) {
    page = page.getParent();
    if (page != null) {
     unresolvedPagePath = page.getPath();
    }
   }
   
  }
  
  return pagePath;
 }
 
 /**
  * Gets the url resolver map.
  *
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the url resolver map
  */
 public static Map<String, String> getUrlResolverMap(ConfigurationService configurationService, Page page) {
  
  Map<String, String> urlResolverMap = null;
  
  try {
   urlResolverMap = configurationService.getCategoryConfiguration(page, URL_MAPPINGS);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Unable to get resolver configurations for pagef : " + page, e);
  }
  return urlResolverMap;
 }
}
