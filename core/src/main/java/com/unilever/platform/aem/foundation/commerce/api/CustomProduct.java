/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api;

import java.util.List;

import com.day.cq.commons.ImageResource;

/**
 * The Class CustomProduct.
 */
public class CustomProduct {
 
 /** The custom image. */
 private ImageResource customImage;
 
 /** The custom total price. */
 private Double customizeProductPrice;
 
 /** The custom description. */
 private String customizeDescription;
 
 /** The wrapping price. */
 private Double wrappingPrice;
 
 /** The wrapping description text. */
 private String customizeGiftWrapDescription;
 
 /** The recipients info. */
 private List<RecipientsInfo> recipientsInfo;
 
 /** The wrapper info. */
 private List<WrapperInfo> wrapperInfo;
 
 /**
  * Gets the custom image.
  *
  * @return the custom image
  */
 public ImageResource getCustomImage() {
  return customImage;
 }
 
 /**
  * Gets the custom total price.
  *
  * @return the custom total price
  */
 public Double getCustomizeProductPrice() {
  return customizeProductPrice;
 }
 
 /**
  * Gets the custom description.
  *
  * @return the custom description
  */
 public String getCustomizeDescription() {
  return customizeDescription;
 }
 
 /**
  * Gets the recipients info.
  *
  * @return the recipients info
  */
 public List<RecipientsInfo> getRecipientsInfo() {
  return recipientsInfo;
 }
 
 /**
  * Gets the wrapper info.
  *
  * @return the wrapper info
  */
 public List<WrapperInfo> getWrapperInfo() {
  return wrapperInfo;
 }
 
 /**
  * Sets the custom total price.
  *
  * @param customTotalPrice
  *         the new custom total price
  */
 public void setCustomizeProductPrice(Double customTotalPrice) {
  this.customizeProductPrice = customTotalPrice;
 }
 
 /**
  * Sets the custom description.
  *
  * @param customDescription
  *         the new custom description
  */
 public void setCustomizeDescription(String customDescription) {
  this.customizeDescription = customDescription;
 }
 
 /**
  * Sets the recipients info.
  *
  * @param recipientsInfo
  *         the new recipients info
  */
 public void setRecipientsInfo(List<RecipientsInfo> recipientsInfo) {
  this.recipientsInfo = recipientsInfo;
 }
 
 /**
  * Sets the wrapper info.
  *
  * @param wrapperInfo
  *         the new wrapper info
  */
 public void setWrapperInfo(List<WrapperInfo> wrapperInfo) {
  this.wrapperInfo = wrapperInfo;
 }
 
 /**
  * Sets the custom image.
  *
  * @param customImage
  *         the new custom image
  */
 public void setCustomImage(ImageResource customImage) {
  this.customImage = customImage;
 }
 
 /**
  * Gets the wrapping price.
  *
  * @return the wrapping price
  */
 public Double getWrappingPrice() {
  return wrappingPrice;
 }
 
 /**
  * Sets the wrapping price.
  *
  * @param wrappingPrice
  *         the new wrapping price
  */
 public void setWrappingPrice(Double wrappingPrice) {
  this.wrappingPrice = wrappingPrice;
 }
 
 /**
  * Gets the wrapping description text.
  *
  * @return the wrapping description text
  */
 public String getCustomizeGiftWrapDescription() {
  return customizeGiftWrapDescription;
 }
 
 /**
  * Sets the wrapping description text.
  *
  * @param wrappingDescriptionText
  *         the new wrapping description text
  */
 public void setCustomizeGiftWrapDescription(String wrappingDescriptionText) {
  this.customizeGiftWrapDescription = wrappingDescriptionText;
 }
}
