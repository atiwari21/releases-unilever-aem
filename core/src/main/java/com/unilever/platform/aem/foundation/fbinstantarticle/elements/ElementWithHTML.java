/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.jsoup.Jsoup;
import org.jsoup.nodes.TextNode;

/**
 * The Class ElementWithHTML.
 */
public abstract class ElementWithHTML extends InstantArticleElement {
 
 /** The html. */
 TextNode html;
 
 /**
  * With html.
  *
  * @param html the html
  * @return the element with html
  */
 public ElementWithHTML withHTML(Object html) {
  if (html instanceof String) {
   
   this.html = new TextNode((String)html,Jsoup.parse((String) html).baseUri()) ;
  }
  return this;
 }
 
 /**
  * Gets the html.
  *
  * @return the html
  */
 public TextNode getHTML() {
  return this.html;
 }
 
}
