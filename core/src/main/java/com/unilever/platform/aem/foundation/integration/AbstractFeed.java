/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

/**
 * The AbstractFeed.
 */
public abstract class AbstractFeed implements Feed {
 
 /** The Constant EIGHTY. */
 private static final int EIGHTY = 80;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(Feed.class);
 
 /** The request. */
 private SlingHttpServletRequest request;
 
 /** The resource resolver. */
 private ResourceResolver resourceResolver;
 
 /** The node path. */
 private String nodePath;
 
 /** The doc. */
 private Document doc;
 
 /** The max child nodes. */
 private int maxChildNodes;
 
 /** The root element. */
 Element rootElement;
 
 /**
  * Instantiates a new abstract feed.
  *
  * @param req
  *         the request
  */
 
 public AbstractFeed(SlingHttpServletRequest req) {
  this.request = req;
  this.resourceResolver = req.getResourceResolver();
  setNodePath(req.getResource());
  try {
   this.maxChildNodes = Integer.parseInt(request.getParameter(MAX_CHILD_NODES) == null ? "-1" : request.getParameter(MAX_CHILD_NODES));
  } catch (NumberFormatException e) {
   LOGGER.error("NumberFormatException occured while handling maxChildNodes");
  }
 }
 
 /**
  * Gets the doc.
  *
  * @return xml document
  */
 public Document getDoc() {
  return doc;
 }
 
 /**
  * Gets the content type.
  *
  * @return content Type
  */
 public String getContentType() {
  return DEFAULT_CONTENT_TYPE;
 }
 
 /**
  * Gets the character encoding.
  *
  * @return character encoding
  */
 public String getCharacterEncoding() {
  return DEFAULT_CHARACTER_ENCODING;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.integration.Feed#printEntry(org.apache.sling.api.resource.ValueMap)
  */
 @Override
 public void printEntry(ValueMap properties) {
  // blank implementation
  
 }
 
 /**
  * Initializes the XML.
  */
 public void initXml() {
  DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
  docFactory.setCoalescing(true);
  docFactory.setValidating(true);
  docFactory.setNamespaceAware(true);
  DocumentBuilder docBuilder;
  try {
   docBuilder = docFactory.newDocumentBuilder();
   this.doc = docBuilder.newDocument();
  } catch (ParserConfigurationException e) {
   LOGGER.error("ParserConfigurationException {}", e);
  }
 }
 
 /**
  * Prints Child Entries.
  */
 public void printChildEntries() {
  Resource resource = this.request.getResource();
  if (resource != null) {
   Iterator<Resource> iter = this.request.getResourceResolver().listChildren(resource);
   List<Resource> children = new ArrayList<>();
   while (iter.hasNext()) {
    Resource res = (Resource) iter.next();
    try {
     if (((Node) res.adaptTo(Node.class)).getName().equals(JcrConstants.JCR_CONTENT)) {
      continue;
     }
    } catch (RepositoryException re) {
     LOGGER.error("RepositoryException {}", re);
    }
    String jcrPath = res.getPath() + "/" + JcrConstants.JCR_CONTENT;
    Resource contentResource = this.request.getResourceResolver().getResource(jcrPath);
    if (contentResource != null) {
     children.add(contentResource);
    }
   }
   printEntries(children.iterator());
  }
 }
 
 /**
  * Prints the entries.
  *
  * @param iterator
  *         the iterator
  */
 public void printEntries(Iterator<Resource> iterator) {
  int counter = 0;
  while (iterator.hasNext()) {
   String noIndex = null;
   if (counter++ == this.maxChildNodes) {
    break;
   }
   Resource resource = iterator.next();
   ValueMap properties = resource.adaptTo(ValueMap.class);
   if (properties.get(NO_INDEX) != null) {
    noIndex = properties.get(NO_INDEX).toString();
   }
   if (noIndex == null) {
    setNodePath(resource.getParent());
    printEntry(resourceResolver.adaptTo(PageManager.class).getContainingPage(resource));
   }
  }
 }
 
 /**
  * Prints the entry.
  *
  * @param page
  *         the page
  */
 public void printEntry(Page page) {
  printEntry(page.adaptTo(ValueMap.class));
 }
 
 /**
  * Gets Node Path.
  *
  * @return node path
  */
 public String getNodePath() {
  return this.nodePath;
 }
 
 /**
  * Sets Node Path.
  *
  * @param res
  *         the resource
  */
 public void setNodePath(Resource res) {
  Resource localResource = res;
  if (localResource == null) {
   localResource = this.request.getResource();
  }
  Node node = (Node) localResource.adaptTo(Node.class);
  if (node == null) {
   LOGGER.error("No node found for resource: " + localResource.getPath());
  } else {
   try {
    this.nodePath = node.getPath();
   } catch (RepositoryException e) {
    LOGGER.error("RepositoryException {}" + e);
   }
  }
 }
 
 /**
  * Gets URL Prefix.
  *
  * @return the URL value
  */
 public String getUrlPrefix() {
  StringBuilder url = new StringBuilder();
  url.append(this.request.getScheme());
  url.append("://");
  url.append(this.request.getServerName());
  if (this.request.getServerPort() != EIGHTY) {
   url.append(":");
   url.append(this.request.getServerPort());
  }
  url.append(this.request.getContextPath());
  return url.toString();
 }
 
 /**
  * Gets the html link.
  *
  * @return the html link
  */
 public String getHtmlLink() {
  return getUrlPrefix() + getNodePath() + SUFFIX_HTML;
 }
 
 /**
  * Prints the node value map in XML.
  *
  * @param properties
  *         the properties
  * @param currentElement
  *         the currentElement
  */
 public void printNodeValueMap(ValueMap properties, Element currentElement) {
  FeedConfiguration feedConfig = getServiceReference(FeedConfiguration.class);
  Map<String, String> keyMapper = feedConfig.getRssMapper();
  Element property;
  for (Entry<String, Object> e : properties.entrySet()) {
   if (propertyValidation(e.getKey())) {
    Object object = e.getValue();
    String key = keyMapper.get(e.getKey());
    if (key == null) {
     key = e.getKey().replaceAll("[^a-zA-Z0-9]", "");
    } else {
     key.replaceAll("[^a-zA-Z0-9]", "");
    }
    
    if (object instanceof Calendar) {
     Calendar calendar = (Calendar) object;
     property = this.doc.createElement(key);
     property.appendChild(doc.createTextNode(formatDate(calendar)));
     currentElement.appendChild(property);
    } else if (object instanceof String[]) {
     String[] values = (String[]) object;
     String combined = "";
     for (String elements : values) {
      combined += elements + " ";
     }
     property = this.doc.createElement(key);
     property.appendChild(doc.createTextNode(combined));
     currentElement.appendChild(property);
    } else {
     String value = null != e.getValue() ? e.getValue().toString().trim().replaceAll("\\<.*?>", "") : "";
     property = this.doc.createElement(key);
     property.appendChild(doc.createTextNode(value));
     currentElement.appendChild(property);
    }
   }
  }
 }
 
 /**
  * Method ised to check property validation.
  *
  * @param validatorProperty
  *         the validatorProperty
  * @return true if property has to be there in RSS
  */
 private boolean propertyValidation(String validatorProperty) {
  boolean validate = true;
  FeedConfiguration feedConfig = getServiceReference(FeedConfiguration.class);
  String[] excludedProperties = feedConfig.getExcludedProperties();
  for (String property : excludedProperties) {
   if (validatorProperty.equals(property)) {
    validate = false;
   }
  }
  return validate;
 }
 
 /**
  * Gets the service reference.
  *
  * @param <T>
  *         the generic type
  * @param serviceClass
  *         the serviceClass
  * @return service reference
  */
 @SuppressWarnings("unchecked")
 private <T> T getServiceReference(final Class<T> serviceClass) {
  T serviceRef;
  final BundleContext bundleContext = FrameworkUtil.getBundle(serviceClass).getBundleContext();
  final ServiceReference osgiRef = bundleContext.getServiceReference(serviceClass.getName());
  serviceRef = (T) bundleContext.getService(osgiRef);
  return serviceRef;
 }
 
 /**
  * Formats the date.
  *
  * @param calendar
  *         the calendar
  * @return formatted date
  */
 protected String formatDate(Calendar calendar) {
  return new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").format(calendar.getTime());
 }
 
 /**
  * Format date.
  *
  * @param date
  *         the date
  * @return the string
  */
 protected String formatDate(Date date) {
  return new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").format(date.getTime());
 }
 
 /**
  * Adds the text node.
  *
  * @param element
  *         the element
  * @param nodeName
  *         the node name
  * @param nodeValue
  *         the node value
  */
 protected void addTextNode(Element element, String nodeName, String nodeValue) {
  Document document = getDoc();
  Element property = document.createElement(nodeName);
  property.appendChild(document.createTextNode(nodeValue));
  element.appendChild(property);
 }
}
