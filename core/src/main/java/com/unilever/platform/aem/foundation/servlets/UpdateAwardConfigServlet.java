/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.tagging.TagConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;

/**
 * The Class UpdateAwardConfigServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "UpdateAwardConfig" }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST })

@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UpdateAwardConfigServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Updates the award config from default to updateconfig ", propertyPrivate = false) })
public class UpdateAwardConfigServlet extends SlingAllMethodsServlet {
 
 /**
  * 
  */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAwardConfigServlet.class);
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "text/html";
 
 /** The count. */
 static int count = 1;
 
 /**  */
 public static final String PRODUCT_NODE = "product";
 
 public static final String AWARD = "/jcr:content/award";
 
 private static final String DEFAULT_CONTENT_PATH = "/content";
 
 private static final String HTML_TABLE_START = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>Update Award Config</title></head>"
   + "<body><table border=\"1\"><tr><th>Page Name</th><th>Action</th><th>Message</th></tr>";
 
 private static final String HTML_TABLE_END = "</table></body></html>";
 
 @Reference
 private ResourceResolverFactory factory;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  response.setContentType(CONTENT_TYPE);
  PrintWriter out = response.getWriter();
  
  RequestParameterMap requestParametrs = request.getRequestParameterMap();
  
  String searchPath = requestParametrs.containsKey("sp") ? requestParametrs.getValue("sp").toString() : StringUtils.EMPTY;
  
  if (StringUtils.isBlank(searchPath)) {
   searchPath = DEFAULT_CONTENT_PATH;
  }
  ResourceResolver resourceResolver = null;
  try {
   resourceResolver = getResourceResolverByService("ieaadmin");
  } catch (LoginException e) {
   LOGGER.error("unable to get resource resolver", e);
  }
  
  Session session = resourceResolver.adaptTo(Session.class);
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (pageManager != null) {
   out.write(HTML_TABLE_START);
   List<Page> awardPagesList = PIMInjestionUtility.getPages(session, searchPath, "jcr:content/sling:resourceType",
     "unilever-aem/components/page/awardConfigPage");
   if (!awardPagesList.isEmpty()) {
    for (Page awardPage : awardPagesList) {
     List<Page> pdpPages = PIMInjestionUtility.getPages(session, awardPage.getParent().getPath(), "jcr:content/product/cq:commerceType", "product");
     List<Page> pdpPages1 = PIMInjestionUtility.getPages(session, awardPage.getParent().getPath(),
       "jcr:content/flexi_hero_par/product/cq:commerceType", "product");
     if (!pdpPages1.isEmpty()) {
      pdpPages.addAll(pdpPages1);
     }
     writeLogEntry(out, awardPage.getPath(), "Find PDP", "Total " + pdpPages.size() + " no of PDP pages found.");
     Page newAwardPage = createCopyPage(awardPage, pageManager, resourceResolver, out);
     int errorCount = 0;
     if (newAwardPage != null) {
      errorCount = createMapping(newAwardPage, pdpPages, resourceResolver, session, out);
      if (0 == errorCount) {
       // Replace the awards in original page with the awards in new page.
       updateOriginalAwards(newAwardPage, awardPage, resourceResolver, session, out);
      }
      deleteNewPage(newAwardPage, resourceResolver, session, out);
     }
    }
    out.write(HTML_TABLE_END);
   } else {
    out.write("There are no award pages" + "<br/><br/>");
   }
  }
  out.flush();
  out.close();
 }
 
 private void writeLogEntry(PrintWriter out, String pageName, String action, String message) {
  if (out != null) {
   out.write("<tr><td>" + pageName + "</td><td>" + action + "</td><td>" + message + "</td></tr>");
  }
 }
 
 private Page createCopyPage(Page page, PageManager pageManager, ResourceResolver resourceResolver, PrintWriter out) {
  Page newPage = null;
  try {
   
   newPage = pageManager.create(page.getParent().getPath(), page.getName() + "-1", page.getTemplate().getName(), page.getTitle(), true);
   if (newPage != null && resourceResolver != null) {
    writeLogEntry(out, newPage.getPath(), "Page Created", "Page Created");
    Resource parResource = resourceResolver.getResource(page.getPath() + "/jcr:content/par");
    if (parResource != null) {
     pageManager.copy(parResource, newPage.getPath() + "/jcr:content/par", null, false, true, true);
     LOGGER.info("New Page is created with path " + newPage.getPath() + " by copying source page " + page.getPath());
    }
   } else {
    writeLogEntry(out, page.getParent().getPath() + "/" + page.getName() + "-1", "Page Creation Failed", "Page Creation failed");
   }
  } catch (WCMException e) {
   LOGGER.error("New Page could not be created by copying page form " + page.getPath(), e);
  }
  return newPage;
 }
 
 private int createMapping(Page page, List<Page> pdpPages, ResourceResolver resourceResolver, Session session, PrintWriter out) {
  int errorCount = 0;
  if (page != null) {
   ConfigurationService confService = null;
   String awardNamespace = StringUtils.EMPTY;
   Resource parResource = resourceResolver != null ? resourceResolver.getResource(page.getPath() + "/jcr:content/par") : null;
   if (parResource != null) {
    for (Resource awardResource : parResource.getChildren()) {
     List<String> pdpAssociatedWithCurrentAward = new ArrayList<String>();
     Set<String> productSet = new LinkedHashSet<String>();
     String componentResourceType = awardResource.getResourceType();
     if (StringUtils.isNotBlank(componentResourceType) && (componentResourceType.equals("unilever-aem/components/content/award")
       || componentResourceType.equals("/apps/unilever-aem/components/content/award"))) {
      // convert this resource as per updated mappings
      for (Page pdpPage : pdpPages) {
       List<String> awardsWithCurrentProduct = new ArrayList<String>();
       if (confService == null) {
        confService = pdpPage.adaptTo(ConfigurationService.class);
       }
       
       if (confService != null) {
        if (StringUtils.isBlank(awardNamespace)) {
         try {
          awardNamespace = confService.getConfigValue(pdpPage, "awardConfig", "awardNamespace");
         } catch (ConfigurationNotFoundException e) {
          LOGGER.error("awardConfig category could not be found", e);
         }
        }
        Product product = CommerceHelper.findCurrentProduct(pdpPage);
        UNIProduct currentProduct = null;
        if (product instanceof UNIProduct) {
         currentProduct = (UNIProduct) product;
        }
        
        if (currentProduct != null) {
         awardsWithCurrentProduct = currentProduct.getAwards(awardNamespace);
         ValueMap properties = awardResource.getValueMap();
         String[] tag = ArrayUtils.EMPTY_STRING_ARRAY;
         if (properties.containsKey(TagConstants.PN_TAGS)) {
          tag = (String[]) properties.get(TagConstants.PN_TAGS);
         }
         
         if (tag != null && !ArrayUtils.isEmpty(tag) && !awardsWithCurrentProduct.isEmpty()) {
          if (awardsWithCurrentProduct.contains(tag[0])) {
           productSet.add(pdpPage.getPath());
           pdpAssociatedWithCurrentAward.add(pdpPage.getPath());
          } else {
           writeLogEntry(out, pdpPage.getPath(), "No Update required", "As no matching tag is found, update is not required for this award");
          }
         }
        }
       }
      }
      
      Node awardResourceNode = awardResource.adaptTo(Node.class);
      if (awardResourceNode != null && !productSet.isEmpty()) {
       String awardResourcePath = StringUtils.EMPTY;
       
       try {
        awardResourcePath = awardResourceNode.getPath();
        
        ValueMap awardValueMap = awardResource.getValueMap();
        if (awardValueMap.containsKey("productPath")) {
         awardResourceNode.getProperty("productPath").remove();
        }
        if (awardValueMap.containsKey("productCount")) {
         awardResourceNode.getProperty("productCount").remove();
        }
        if (awardValueMap.containsKey("product")) {
         awardResourceNode.getProperty("product").remove();
        }
        session.save();
        
        String[] productSetArray = productSet.toArray(new String[productSet.size()]);
        awardResourceNode.setProperty("productPath", productSetArray);
        awardResourceNode.setProperty("productCount", String.valueOf(productSet.size()));
        List<String> prodList = new ArrayList<String>();
        for (String prod : productSet) {
         String productData = "{\"productPath\":\"" + prod + "\"}";
         prodList.add(productData);
        }
        
        String[] productSetListArray = prodList.toArray(new String[prodList.size()]);
        awardResourceNode.setProperty("product", productSetListArray);
        
        session.save();
        if (!pdpAssociatedWithCurrentAward.isEmpty()) {
         writeLogEntry(out, awardResourceNode.getPath(), "Associated PDP found", "The following " + pdpAssociatedWithCurrentAward.size()
           + " PDP are associated with this award </br>" + pdpAssociatedWithCurrentAward.toString() + "</br>");
        }
       } catch (RepositoryException e) {
        writeLogEntry(out, awardResourcePath, "Update Failed", "This award could not be updated");
        LOGGER.error("products could not be added to the award " + awardResourcePath, e);
       }
      }
     } else {
      errorCount++;
     }
    }
   }
  }
  return errorCount;
 }
 
 private void updateOriginalAwards(Page newAwardPage, Page oldAwardPage, ResourceResolver resourceResolver, Session session, PrintWriter out) {
  Resource newAwardParResource = resourceResolver != null ? resourceResolver.getResource(newAwardPage.getPath() + "/jcr:content/par") : null;
  Resource oldAwardParResource = resourceResolver != null ? resourceResolver.getResource(oldAwardPage.getPath() + "/jcr:content/par") : null;
  if (null != newAwardParResource && null != oldAwardParResource) {
   for (Resource newResource : newAwardParResource.getChildren()) {
    int tempCount = 0;
    String newResourceName = newResource.getName();
    Resource oldResource = oldAwardParResource.getChild(newResourceName);
    if (oldResource != null) {
     ValueMap newAwardproperties = newResource.getValueMap();
     Node awardResourceNode = oldResource.adaptTo(Node.class);
     if (awardResourceNode != null) {
      try {
       String[] productSetArray = newAwardproperties.get("productPath", String[].class);
       String[] productSetJsonArray = newAwardproperties.get("product", String[].class);
       String productCount = newAwardproperties.get("productCount", String.class);
       if (null != productCount) {
        if (ArrayUtils.isNotEmpty(productSetArray)) {
         awardResourceNode.setProperty("productPath", productSetArray);
         tempCount++;
        }
        if (ArrayUtils.isNotEmpty(productSetJsonArray)) {
         awardResourceNode.setProperty("product", productSetJsonArray);
         tempCount++;
        }
        if (StringUtils.isNotEmpty(productCount)) {
         awardResourceNode.setProperty("productCount", productCount);
         tempCount++;
        }
        if (3 == tempCount) {
         session.save();
         try {
          String productPathArrayJson = new ObjectMapper().writeValueAsString(productSetArray);
          String productSetArrayJson = new ObjectMapper().writeValueAsString(productSetJsonArray);
          writeLogEntry(out, awardResourceNode.getPath(), "Added the 'product','productPath', 'productCount' properties",
            "Value of 'product' is: " + productSetArrayJson + "</br></br>" + "Value of 'productPath' is: " + productPathArrayJson + "</br></br>"
              + "Value of 'productCount' is: " + productCount);
         } catch (JsonProcessingException e) {
          LOGGER.error("Unable to convert product set to JSON", e);
         }
        } else {
         writeLogEntry(out, awardResourceNode.getPath(), "No Update required", "As no matching is found, update is not required for this award");
        }
       }
      } catch (RepositoryException e) {
       LOGGER.error("Error in updating original award config", e);
      }
     }
    }
   }
  }
 }
 
 private void deleteNewPage(Page newAwardPage, ResourceResolver resourceResolver, Session session, PrintWriter out) {
  Resource resource = resourceResolver != null ? resourceResolver.getResource(newAwardPage.getPath()) : null;
  Node tempNode = resource != null ? resource.adaptTo(Node.class) : null;
  if (null != tempNode) {
   try {
    tempNode.remove();
    writeLogEntry(out, newAwardPage.getPath(), "Page Delete", "New Page is deleted");
    session.save();
   } catch (RepositoryException e) {
    LOGGER.error("Unable to delete new page : ", e);
   }
  }
 }
 
 private ResourceResolver getResourceResolverByService(String serviceName) throws LoginException {
  Map<String, Object> resolverParamMap = new HashMap<String, Object>();
  
  resolverParamMap.put(ResourceResolverFactory.SUBSERVICE, serviceName);
  return factory.getServiceResourceResolver(resolverParamMap);
 }
 
 /**
  * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
 }
}
