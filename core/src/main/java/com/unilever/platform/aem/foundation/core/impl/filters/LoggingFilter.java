/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.impl.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple servlet filter component that logs incoming requests.
 */
@SlingFilter(order = -700, scope = SlingFilterScope.REQUEST)
public class LoggingFilter implements Filter {
 
 private static final Logger LOG = LoggerFactory.getLogger(LoggingFilter.class);
 
 @Override
 public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
   throws IOException, ServletException {
  
  final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
  LOG.debug("request for {}, with selector {}", slingRequest.getRequestPathInfo().getResourcePath(),
    slingRequest.getRequestPathInfo().getSelectorString());
  
  filterChain.doFilter(request, response);
 }
 
 @Override
 public void init(FilterConfig filterConfig) {
  LOG.debug("Inside filterconfig Initialization");
 }
 
 @Override
 public void destroy() {
  LOG.debug("Inside filterconfig destroy method");
 }
}
