/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.io.IOException;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;

/**
 * This is the interface for different services based on the type of template/script that is to be used for rendering the view. All the template types
 * would have a different service implementation which would be implementing this interface. All the services would be based on the path of the
 * templates and the extension of the view, eg. .hbss
 * 
 */
public interface TemplateView {
 
 /**
  * This method would be implemented by all the corresponding service implementations. This method would return the relevant view path along with the
  * view name. It would return a map with view name as key and the view path as the value. It would return all the views of a given component. eg. if
  * adaptive-image is the component, then the following map would be returned: {defaultView = /etc/ui/iea/templates/components/
  * adaptive-image/displayAs_defaultView.hbss}
  * 
  * @param request
  *         The Sling Request Object
  * @return Map of viewName and corresponding paths
  */
 Map<String, String> fetchComponentViewBasePathMap(SlingHttpServletRequest request) throws IOException;
 
 /**
  * This method would be implemented by all the corresponding service implementations.
  * 
  * @param viewType
  *         the viewType
  * @param request
  *         The Sling Request Object
  * 
  * @return fetchTemplatePathForRendering
  */
 String fetchTemplatePathForRendering(final SlingHttpServletRequest request, final String viewType);
 
}
