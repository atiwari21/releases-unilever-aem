/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.dto.SearchDTO;
import com.unilever.platform.aem.foundation.core.dto.TagDTO;
import com.unilever.platform.aem.foundation.core.dto.TaggingSearchCriteria;
import com.unilever.platform.aem.foundation.core.service.SearchParam;
import com.unilever.platform.aem.foundation.core.service.SearchService;

import com.day.cq.wcm.api.NameConstants;

/**
 * The Class SearchServiceImpl.
 */
@Component(label = "Unilever Tagging Search Service", immediate = true, metatype = true)
@Service(value = { SearchService.class })
public class SearchServiceImpl implements SearchService {
 
 private static final String EXCEPTION_IS_ACCESSING_REOSITORY_PATH = "exception is accessing reository path..............";
 
 private static final String EXCEPTION_INSIDE_SEARCH_SERVICE_IMPL = "Exception inside Search Service Impl..............";
 
 private static final String EXCEPTION_IN_GETTING_VALUES_FROM_RESULT_NODE = "Exception in getting values from result node..............";
 
 private static final int FOUR = 4;
 
 /** The Constant DATE_FORMAT. */
 private static final String DATE_FORMAT = "yyyy/MM/dd";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SearchServiceImpl.class);
 
 /** The query builder. */
 @Reference
 private QueryBuilder queryBuilder;
 
 /**
  * This method populates the list of tags with in a input tag.
  *
  * @param searchPath
  *         the search path
  * @return List of tags.
  */
 @Override
 public List<TagDTO> getTagsList(String searchPath, ResourceResolver resourceResolver) {
  // Setup the query based on user input
  String sqlStatement = "";
  TagDTO tagdto = new TagDTO();
  List<TagDTO> tagDataList = null;
  Session session = null;
  try {
   // Invoke the adaptTo method to create a Session used to create a
   // QueryManager
   session = resourceResolver.adaptTo(Session.class);
   // Set the query & Obtain the query manager for the session ...
   QueryManager queryManager = session.getWorkspace().getQueryManager();
   // Setup the query to get all tags of a parent tag at provided path
   sqlStatement = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([" + searchPath + "]) and CONTAINS(s.*, 'cq:tag')";
   Query query = queryManager.createQuery(sqlStatement, "JCR-SQL2");
   // Execute the query and get the results
   QueryResult results = query.execute();
   if (null != results) {
    // Iterate over the nodes in the results
    NodeIterator itr = results.getNodes();
    tagDataList = new ArrayList<TagDTO>();
    if (null != itr) {
     while (itr.hasNext()) {
      Node resultNode = itr.nextNode();
      // Set all tags fields in tagDTO
      tagdto.setTagName(resultNode.getName());
      tagdto.setTagPath(resultNode.getPath());
      tagdto.setTagTitle(resultNode.getProperty(SearchConstants.SEARCH_TITLE).getString());
      // Push tagdto to the list
      tagDataList.add(tagdto);
     }
    }
   }
   
  } catch (RepositoryException re) {
   LOGGER.error(ExceptionUtils.getStackTrace(re));
  }
  return tagDataList;
 }
 
 /**
  * This method populates the list of pages tagged with input tags but not having any negatetag Those pages should have property, value at that pages
  * and pages tagged with featureTag will appear first in list.
  *
  * @param tagID
  *         the tag id
  * @param negateTag
  *         the negate tag
  * @param currentPath
  *         the current path
  * @param propertyValues
  *         the property values
  * @param param
  *         the param
  * @return List of Results pages.
  */
 @Override
 public List<SearchDTO> getCreteriaPagesList(List<String> tagID, List<String> negateTag, String currentPath, Map<String, List<String>> propertyValues,
   SearchParam param, ResourceResolver resourceResolver) {
  List<SearchDTO> finalCollection = new ArrayList<SearchDTO>();
  List<String> tagID2 = null;
  try {
   SearchResult result = this.getNagatedTagResults(tagID, tagID2, negateTag, currentPath, propertyValues, param, resourceResolver);
   setData(currentPath, finalCollection, result);
  } catch (ValueFormatException ve) {
   LOGGER.error(EXCEPTION_IN_GETTING_VALUES_FROM_RESULT_NODE + ExceptionUtils.getStackTrace(ve));
  } catch (PathNotFoundException pe) {
   LOGGER.error(EXCEPTION_IS_ACCESSING_REOSITORY_PATH + ExceptionUtils.getStackTrace(pe));
  } catch (RepositoryException re) {
   LOGGER.error(EXCEPTION_INSIDE_SEARCH_SERVICE_IMPL + ExceptionUtils.getStackTrace(re));
  }
  return finalCollection;
 }
 
 /**
  * Sets the data.
  *
  * @param currentPath
  *         the current path
  * @param finalCollection
  *         the final collection
  * @param result
  *         the result
  * @throws RepositoryException
  *          the repository exception
  * @throws ValueFormatException
  *          the value format exception
  * @throws PathNotFoundException
  *          the path not found exception
  */
 private void setData(String currentPath, List<SearchDTO> finalCollection, SearchResult result) throws RepositoryException {
  for (Hit hit : result.getHits()) {
   SearchDTO searchdto = new SearchDTO();
   if (!hit.getPath().equalsIgnoreCase(currentPath)) {
    searchdto.setResultNodePath(hit.getPath());
    
    searchdto.setResultNodeTags(hit.getNode().getNode(JcrConstants.JCR_CONTENT).getProperty(com.day.cq.tagging.TagConstants.PN_TAGS).getValues());
    Date replicationTime = null;
    
    if (hit.getNode().getNode(JcrConstants.JCR_CONTENT).hasProperty(SearchConstants.PUBLISHDATE)) {
     Object value = (Object) hit.getNode().getNode(JcrConstants.JCR_CONTENT).getProperty(SearchConstants.PUBLISHDATE).getValue();
     try {
      replicationTime = hit.getNode().getNode(JcrConstants.JCR_CONTENT).getProperty(SearchConstants.PUBLISHDATE).getDate().getTime();
     } catch (Exception e) {
      DateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);
      if (value instanceof String && StringUtils.isNotBlank((String)value)) {
       try {
        replicationTime = dateFormatter.parse((String) value);
       } catch (ParseException e1) {
        LOGGER.warn("Exception while parsing publish date" + e1);
       }
      }
      LOGGER.error("Exception while fetching publish date" + e);
     }
    }
    searchdto.setReplicationTime(replicationTime);
    finalCollection.add(searchdto);
   }
  }
 }
 
 /**
  * This method populates the list of pages tagged with input tags but not having any negatetag Those pages should have property, value at that pages
  * and pages tagged with featureTag will appear first in list.
  *
  * @param tagID
  *         the tag id
  * @param negateTag
  *         the negate tag
  * @param currentPath
  *         the current path
  * @param propertyValues
  *         the property values
  * @param offset
  *         the offset
  * @param limit
  *         the limit
  * @param sortOrderag
  *         the sort order
  * @return List of Results pages.
  */
 @Override
 public List<SearchDTO> getCreteriaPagesList(List<String> tagID1, List<String> tagID2, List<String> negateTag, String currentPath,
   Map<String, List<String>> propertyValues, SearchParam param, ResourceResolver resourceResolver) {
  List<SearchDTO> finalCollection = new ArrayList<SearchDTO>();
  try {
   SearchResult result = this.getNagatedTagResults(tagID1, tagID2, negateTag, currentPath, propertyValues, param, resourceResolver);
   setData(currentPath, finalCollection, result);
  } catch (ValueFormatException ve) {
   LOGGER.error(EXCEPTION_IN_GETTING_VALUES_FROM_RESULT_NODE + ExceptionUtils.getStackTrace(ve));
  } catch (PathNotFoundException pe) {
   LOGGER.error(EXCEPTION_IS_ACCESSING_REOSITORY_PATH + ExceptionUtils.getStackTrace(pe));
  } catch (RepositoryException re) {
   LOGGER.error(EXCEPTION_INSIDE_SEARCH_SERVICE_IMPL + ExceptionUtils.getStackTrace(re));
  }
  return finalCollection;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.SearchService# getMultiTagResults(java.util.List, java.util.List, java.lang.String,
  * java.util.Map, com.unilever.platform.aem.foundation.core.service.SearchParam)
  */
 @Override
 public List<SearchDTO> getMultiTagResults(List<TaggingSearchCriteria> tagIDList, List<String> negateTag, String currentPath,
   Map<String, List<String>> propertyValues, SearchParam param, ResourceResolver resourceResolver) {
  List<SearchDTO> finalCollection = new ArrayList<SearchDTO>();
  try {
   SearchResult result = this.getMultiTagListResults(tagIDList, negateTag, currentPath, propertyValues, param, resourceResolver);
   setData(currentPath, finalCollection, result);
  } catch (ValueFormatException ve) {
   LOGGER.error(EXCEPTION_IN_GETTING_VALUES_FROM_RESULT_NODE + ExceptionUtils.getStackTrace(ve));
  } catch (PathNotFoundException pe) {
   LOGGER.error(EXCEPTION_IS_ACCESSING_REOSITORY_PATH + ExceptionUtils.getStackTrace(pe));
  } catch (RepositoryException re) {
   LOGGER.error(EXCEPTION_INSIDE_SEARCH_SERVICE_IMPL + ExceptionUtils.getStackTrace(re));
  }
  return finalCollection;
 }
 
 /**
  * This method populates the list of pages tagged with tags starting with passed parameter tag and have content type property value set on those and
  * send back in search result format.
  *
  * @param tagIDList1
  *         the tag id list1
  * @param tagIDList2
  *         the tag id list2
  * @param negateTag
  *         the negate tag
  * @param currentPath
  *         the current path
  * @param propertyValues
  *         the property values
  * @param param
  *         the param
  * @param resourceResolver
  *         the resourceResolver
  * @return Search Results.
  */
 public SearchResult getNagatedTagResults(List<String> tagIDList1, List<String> tagIDList2, List<String> negateTag, String currentPath,
   Map<String, List<String>> propertyValues, SearchParam param, ResourceResolver resourceResolver) {
  
  int counter = 1;
  SearchResult result = null;
  try {
   PredicateGroup group = new PredicateGroup();
   PredicateGroup pathGroup = new PredicateGroup();
   int position = org.apache.commons.lang3.StringUtils.ordinalIndexOf(currentPath, "/", FOUR);
   String searchpath = currentPath.substring(0, position);
   pathGroup.add(new Predicate(SearchConstants.SEARCH_PATH).set(SearchConstants.SEARCH_PATH, searchpath));
   group.add(pathGroup);
   PredicateGroup typeGroup = new PredicateGroup();
   
   typeGroup.add(new Predicate(SearchConstants.SEARCH_TYPE).set(SearchConstants.SEARCH_TYPE, NameConstants.NT_PAGE));
   group.add(typeGroup);
   
   if (null != tagIDList1) {
    PredicateGroup orGroup = new PredicateGroup();
    orGroup.set(SearchConstants.SEARCH_OR, SearchConstants.SEARCH_TRUE);
    for (String tagID : tagIDList1) {
     orGroup.add(new Predicate(SearchConstants.TAGS + counter, SearchConstants.TAG_ID)
       .set(SearchConstants.SEARCH_PROPERTY, SearchConstants.SEARCH_TAGPATH).set(SearchConstants.TAG_ID, tagID));
     counter++;
    }
    group.add(orGroup);
   }
   if (null != tagIDList2) {
    PredicateGroup andGroup = new PredicateGroup();
    andGroup.set(SearchConstants.SEARCH_AND, SearchConstants.SEARCH_TRUE);
    for (String tagID : tagIDList2) {
     andGroup.add(new Predicate(SearchConstants.TAGS + counter, SearchConstants.TAG_ID)
       .set(SearchConstants.SEARCH_PROPERTY, SearchConstants.SEARCH_TAGPATH).set(SearchConstants.TAG_ID, tagID));
     counter++;
    }
    group.add(andGroup);
   }
   result = setPredicateGroup(negateTag, propertyValues, param, group, resourceResolver);
  } catch (LoginException loginException) {
   LOGGER.error(ExceptionUtils.getStackTrace(loginException));
  }
  
  return result;
 }
 
 private SearchResult setPredicateGroup(List<String> negateTag, Map<String, List<String>> propertyValues, SearchParam param, PredicateGroup group,
   ResourceResolver resourceResolver) throws LoginException {
  int negateCounter = 1;
  int propertyCounter = 1;
  SearchResult result = null;
  PredicateGroup negateGroup = new PredicateGroup();
  for (String negateTagID : negateTag) {
   negateGroup.add(new Predicate(SearchConstants.TAGS + negateCounter, SearchConstants.TAG_ID)
     .set(SearchConstants.SEARCH_PROPERTY, SearchConstants.SEARCH_TAGPATH).set(SearchConstants.TAG_ID, negateTagID));
   negateCounter++;
  }
  negateGroup.setAllRequired(false);
  negateGroup.setNegated(true);
  group.add(negateGroup);
  
  for (Map.Entry<String, List<String>> entry : propertyValues.entrySet()) {
   PredicateGroup propertyGroup = new PredicateGroup();
   propertyGroup.set(SearchConstants.SEARCH_OR, SearchConstants.SEARCH_TRUE);
   List<String> values = entry.getValue();
   for (String value : values) {
    if (StringUtils.equalsIgnoreCase(value, "exists")) {
     propertyGroup.add(new Predicate(propertyCounter + SearchConstants.SEARCH_UNDERSCORE_PROPERTY, SearchConstants.SEARCH_PROPERTY)
       .set(SearchConstants.SEARCH_PROPERTY, entry.getKey()).set(SearchConstants.SEARCH_OPERATION, "exists"));
     propertyCounter++;
    } else {
     propertyGroup.add(new Predicate(propertyCounter + SearchConstants.SEARCH_UNDERSCORE_PROPERTY, SearchConstants.SEARCH_PROPERTY)
       .set(SearchConstants.SEARCH_PROPERTY, entry.getKey()).set(SearchConstants.SEARCH_VALUE, value)
       .set(SearchConstants.SEARCH_OPERATION, SearchConstants.SEARCH_OPERATION_LIKE));
     propertyCounter++;
    }
   }
   group.add(propertyGroup);
  }
  
  group.add(new Predicate(SearchConstants.SEARCH_ORDERBY).set(SearchConstants.SEARCH_ORDERBY, SearchConstants.SORTPROPERTY_SCORE)
    .set(SearchConstants.SEARCH_SORT, param.getSortOrder()));
  LOGGER.debug("Final group Query " + group);
  Session session = resourceResolver.adaptTo(Session.class);
  LOGGER.debug(NameConstants.PN_DT_GROUPS + group.toString());
  com.day.cq.search.Query query = this.queryBuilder.createQuery(group, session);
  query.setStart(param.getOffset());
  query.setHitsPerPage(param.getLimit());
  LOGGER.debug("predicate group" + query.getPredicates());
  result = query.getResult();
  LOGGER.debug("results" + result.getQueryStatement());
  return result;
 }
 
 /**
  * Gets the multi tag list results.
  *
  * @param taggingList
  *         the tagging list
  * @param negateTag
  *         the negate tag
  * @param currentPath
  *         the current path
  * @param propertyValues
  *         the property values
  * @param param
  *         the param
  * @param resourceResolver
  *         the resourceResolver
  * @return the multi tag list results
  */
 public SearchResult getMultiTagListResults(List<TaggingSearchCriteria> taggingList, List<String> negateTag, String currentPath,
   Map<String, List<String>> propertyValues, SearchParam param, ResourceResolver resourceResolver) {
  LOGGER.debug("Entering getMultiTagListResults");
  int counter = 1;
  SearchResult result = null;
  try {
   PredicateGroup group = new PredicateGroup();
   PredicateGroup pathGroup = new PredicateGroup();
   int position = org.apache.commons.lang3.StringUtils.ordinalIndexOf(currentPath, "/", FOUR);
   String searchpath = currentPath.substring(0, position);
   LOGGER.debug("searchpath is" + searchpath);
   pathGroup.add(new Predicate(SearchConstants.SEARCH_PATH).set(SearchConstants.SEARCH_PATH, searchpath));
   group.add(pathGroup);
   PredicateGroup typeGroup = new PredicateGroup();
   typeGroup.add(new Predicate(SearchConstants.SEARCH_TYPE).set(SearchConstants.SEARCH_TYPE, NameConstants.NT_PAGE));
   group.add(typeGroup);
   for (TaggingSearchCriteria tagList : taggingList) {
    if (null != tagList) {
     LOGGER.debug("tag list to operate is" + tagList);
     PredicateGroup operatorGroup = new PredicateGroup();
     operatorGroup.set(tagList.getOperator(), SearchConstants.SEARCH_TRUE);
     for (String tagID : tagList.getTagList()) {
      operatorGroup.add(new Predicate(SearchConstants.TAGS + counter, SearchConstants.TAG_ID)
        .set(SearchConstants.SEARCH_PROPERTY, SearchConstants.SEARCH_TAGPATH).set(SearchConstants.TAG_ID, tagID));
      counter++;
     }
     group.add(operatorGroup);
    }
   }
   
   result = setPredicateGroup(negateTag, propertyValues, param, group, resourceResolver);
  } catch (LoginException loginException) {
   LOGGER.error(ExceptionUtils.getStackTrace(loginException));
  }
  
  return result;
 }
 
 /**
  * Gets the pages.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @param commerceTypePath
  *         the commerce type path
  * @param pageType
  *         the page type
  * @param builder
  *         the builder
  * @return the pages
  */
 public static List<Page> getPages(Session session, String path, String commerceTypePath, String pageType, QueryBuilder builder) {
  
  Map<String, String> map = new HashMap<String, String>();
  
  List<Page> pageList = new ArrayList<Page>();
  
  map.put("path", path);
  map.put("type", NameConstants.NT_PAGE);
  map.put("property", commerceTypePath);
  map.put("property.value", pageType);
  if (builder != null) {
   pageList = getResultfromQuery(session, map, builder);
  }
  return pageList;
 }
 
 /**
  * Gets the resultfrom query.
  *
  * @param session
  *         the session
  * @param map
  *         the map
  * @param builder
  *         the builder
  * @return the resultfrom query
  */
 public static List<Page> getResultfromQuery(Session session, Map<String, String> map, QueryBuilder builder) {
  
  com.day.cq.search.Query query = null;
  SearchResult result = null;
  List<Page> pageList = new ArrayList<Page>();
  
  query = builder.createQuery(PredicateGroup.create(map), session);
  query.setHitsPerPage(0);
  if (query != null) {
   result = query.getResult();
   
   Iterator<Resource> rsIterator = result.getResources();
   while (rsIterator.hasNext()) {
    Page page = rsIterator.next().adaptTo(Page.class);
    if (page != null) {
     pageList.add(page);
    }
   }
  }
  return pageList;
 }
 
 @Override
 public List<Page> getServicePages(Session session, String path, String commerceTypePath, String pageType, QueryBuilder builder) {
  return getPages(session, path, commerceTypePath, pageType, builder);
 }
 
}
