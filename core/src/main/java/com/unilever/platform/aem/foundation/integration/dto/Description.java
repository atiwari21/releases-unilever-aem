/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * The Class Description.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "value" })
public class Description {
 
 /** The value. */
 @XmlValue
 protected String value;
 
 /** The locale. */
 @XmlAttribute(name = "locale", required = true)
 protected String locale;
 
 /**
  * Gets the value.
  *
  * @return the value
  */
 public String getValue() {
  return value;
 }
 
 /**
  * Sets the value.
  *
  * @param value
  *         the new value
  */
 public void setValue(String value) {
  this.value = value;
 }
 
 /**
  * Gets the locale.
  *
  * @return the locale
  */
 public String getLocale() {
  return locale;
 }
 
 /**
  * Sets the locale.
  *
  * @param value
  *         the new locale
  */
 public void setLocale(String value) {
  this.locale = value;
 }
 
}
