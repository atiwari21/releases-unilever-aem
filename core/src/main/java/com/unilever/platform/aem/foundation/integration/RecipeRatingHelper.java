/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.ImageResource;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.msm.api.LiveRelationship;
import com.day.cq.wcm.msm.api.LiveRelationshipManager;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;

/**
 * Utility Helper class for handlebar script helpers.
 * 
 * @author nbhart
 * 
 */
public final class RecipeRatingHelper {
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RecipeRatingHelper.class);
 
 /** The Constant BV_IMAGE_DEFAULT_SIZE. */
 private static final String IMAGE_DEFAULT_SIZE = "220x220";
 
 /** The Constant BV_IMAGE_SIZE. */
 private static final String IMAGE_SIZE = "imageSize";
 
 /** The Constant FEED_CATEGORY. */
 private static final String FEED_CATEGORY = "recipeFeedConfiguration";
 
 /** The Constant IS_ENCODING_REQUIRED. */
 private static final String IS_ENCODING_REQUIRED = "isEncodingRequired";
 
 /** The Constant RECIPE_CONFIG_RESOURCETYPE. */
 private static final String RECIPE_CONFIG_RESOURCETYPE = "unilever-aem/components/content/recipeConfig";
 
 /**
  * private constructor.
  */
 private RecipeRatingHelper() {
  
 }
 
 /**
  * Gets the product page urls.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param configurationService
  *         the configurationService
  * @return the product page urls
  */
 public static String getPageFullUrls(Page page, String domainUrl, ConfigurationService configurationService) {
  String isEncodingRequired = getEncodingRequired(page, configurationService);
  return domainUrl + getEncodedOrSamePath(UrlResolverUtility.getResolvedPagePath(configurationService, page) + ".html", isEncodingRequired);
 }
 
 /**
  * Save feedto file system.
  *
  * @param feed
  *         the feed
  * @return the string
  */
 public static String saveFeedtoFileSystem(ProductFeeds feed) {
  ObjectMapper mapper = new ObjectMapper();
  String jsonInString = "";
  try {
   jsonInString = mapper.writeValueAsString(feed);
  } catch (JsonGenerationException e) {
   LOGGER.error("JsonGenerationException in feed feeds generation ", e);
  } catch (JsonMappingException e) {
   LOGGER.error("Error in mapping json ", e);
  } catch (IOException e) {
   LOGGER.error("IO exception in json feeds generation ", e);
  }
  return jsonInString;
  
 }
 
 /**
  * Gets the identifier.
  *
  * @param currentProduct
  *         the current product
  * @return the identifier
  */
 public static Identifier getIdentifier(UNIProduct currentProduct) {
  Identifier identifier = new Identifier();
  identifier.setIdentifierType(currentProduct.getIdentifierType());
  identifier.setIdentifierValue(currentProduct.getIdentifierValue());
  return identifier;
 }
 
 /**
  * Gets the product category.
  *
  * @param page
  *         the page
  * @return the product category
  */
 public static String getProductCategoryName(Page page) {
  return page.getParent().getTitle();
 }
 
 /**
  * Gets the category feed.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param configurationService
  *         the configuration service
  * @return the category feed
  */
 public static CategoryType getCategoryFeed(Page page, String domainUrl, ConfigurationService configurationService) {
  CategoryType category = new CategoryType();
  category.setCategoryId(getCategoryId(configurationService, page, false));
  String parentCategoryId = getParentCategoryId(configurationService, page);
  if (StringUtils.isBlank(parentCategoryId)) {
   category.setParentCategoryId(null);
  } else {
   category.setParentCategoryId(parentCategoryId);
  }
  category.setFaimlyId(getProductFamilyPrefix(configurationService, page) + category.getCategoryId());
  category.setName(page.getTitle());
  category.setCategoryPageUrl(RecipeRatingHelper.getPageFullUrls(page, domainUrl, configurationService));
  category.setImageUrl(getCategoryImage(page, domainUrl, configurationService));
  return category;
 }
 
 /**
  * Gets the category id.
  *
  * @param configurationService
  *         the configurationService
  * @param page
  *         the page
  * @return the category id
  */
 public static String getCategoryId(ConfigurationService configurationService, Page page, boolean isProduct) {
  Resource jcrResource = null;
  if (isProduct) {
   jcrResource = page.getParent().getContentResource();
  } else {
   jcrResource = page.getContentResource();
  }
  TagManager tagManager = jcrResource.getResourceResolver().adaptTo(TagManager.class);
  Tag[] tags = tagManager.getTags(jcrResource);
  String parentId = getParentIdForCategory(configurationService, page, tags);
  String[] categoryNamespace = getCategoryNamespace(configurationService, page);
  String categoryId = StringUtils.EMPTY;
  Map<String, Tag> namespaceTags = new HashMap<String, Tag>();
  for (String categoryName : categoryNamespace) {
   Tag namespaceTag = getMatchingTag(tags, StringUtils.trim(categoryName));
   if (namespaceTag != null) {
    namespaceTags.put(categoryName, namespaceTag);
   }
  }
  
  if (jcrResource.getValueMap().containsKey("uniquePageId")) {
   categoryId = jcrResource.getValueMap().get("uniquePageId").toString();
  }
  
  if (StringUtils.isBlank(categoryId) && ArrayUtils.isNotEmpty(categoryNamespace) && categoryNamespace.length == namespaceTags.size()) {
   for (String categoryName : categoryNamespace) {
    Tag tag = namespaceTags.get(categoryName);
    categoryId = categoryId + tag.getLocalTagID().toString().replace("/", "").replace("-", "");
   }
  }
  if (StringUtils.isNotBlank(categoryId) && StringUtils.isNotBlank(parentId)) {
   categoryId = parentId + categoryId;
  }
  if (categoryId != null) {
   categoryId.replaceAll("\\s", "");
  }
  return categoryId;
 }
 
 /**
  * Gets the category image.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param configurationService
  *         the configuration service
  * @return the category image
  */
 public static String getCategoryImage(Page page, String domainUrl, ConfigurationService configurationService) {
  String isEncodingRequired = getEncodingRequired(page, configurationService);
  String categoryTeaserImage = StringUtils.EMPTY;
  String imagepath = StringUtils.EMPTY;
  String widgetImageSize = IMAGE_DEFAULT_SIZE;
  try {
   widgetImageSize = configurationService.getConfigValue(page, FEED_CATEGORY, IMAGE_SIZE);
   if (StringUtils.isBlank(widgetImageSize)) {
    widgetImageSize = IMAGE_DEFAULT_SIZE;
   }
  } catch (ConfigurationNotFoundException e) {
   widgetImageSize = IMAGE_DEFAULT_SIZE;
   LOGGER.error("BV Image size configuration not found", e);
  }
  ValueMap properties = page.getProperties();
  categoryTeaserImage = properties.get("teaserImage") != null ? properties.get("teaserImage").toString() : StringUtils.EMPTY;
  
  if (!categoryTeaserImage.isEmpty() && StringUtils.contains(categoryTeaserImage, ".")) {
   String imagepathTemp = categoryTeaserImage + ".ulenscale." + widgetImageSize + categoryTeaserImage.substring(categoryTeaserImage.indexOf("."));
   imagepath = domainUrl + getEncodedOrSamePath(imagepathTemp, isEncodingRequired);
  }
  
  return imagepath;
 }
 
 /**
  * Gets the category data for product.
  *
  * @param configurationService
  *         the configurationService
  * @param page
  *         the page
  * @return the categorydata
  */
 public static List<CategoryDetail> getCategoryDataForProduct(ConfigurationService configurationService, Page page) {
  List<CategoryDetail> categoryList = new ArrayList<CategoryDetail>();
  CategoryDetail mainCategory = new CategoryDetail();
  
  String familyPrefix = getProductFamilyPrefix(configurationService, page);
  mainCategory.setCategoryId(getCategoryId(configurationService, page, true));
  mainCategory.setFamilyId(familyPrefix + mainCategory.getCategoryId());
  if (StringUtils.isNotBlank(mainCategory.getCategoryId())) {
   categoryList.add(mainCategory);
  }
  
  if (categoryList.isEmpty()) {
   final Resource jcrResource = page.getContentResource();
   
   TagManager tagManager = jcrResource.getResourceResolver().adaptTo(TagManager.class);
   Tag[] tags = tagManager.getTags(jcrResource);
   categoryList.addAll(getParentCategoriesFromProduct(configurationService, page, tags));
  }
  
  return categoryList;
  
 }
 
 /**
  * Gets the product image urls.
  *
  * @param page
  *         the page
  * @param currentProduct
  *         the current product
  * @param domainUrl
  *         the domain url
  * @param configurationService
  *         the configuration service
  * @return the product image urls
  */
 public static String getProductImageUrls(Page page, UNIProduct currentProduct, String domainUrl, ConfigurationService configurationService) {
  String isEncodingRequired = getEncodingRequired(page, configurationService);
  String imageurls = "";
  if ((currentProduct != null) && (!currentProduct.getImages().isEmpty())) {
   ImageResource imgResource = currentProduct.getImages().get(0);
   String widgetImageSize = IMAGE_DEFAULT_SIZE;
   try {
    widgetImageSize = configurationService.getConfigValue(page, FEED_CATEGORY, IMAGE_SIZE);
    if (StringUtils.isBlank(widgetImageSize)) {
     widgetImageSize = IMAGE_DEFAULT_SIZE;
    }
   } catch (ConfigurationNotFoundException e) {
    widgetImageSize = IMAGE_DEFAULT_SIZE;
    LOGGER.error("BV Image size configuration not found", e);
   }
   String imageurlsTemp = imgResource.getHref() + ".ulenscale." + widgetImageSize + imgResource.getExtension();
   imageurls = domainUrl + getEncodedOrSamePath(imageurlsTemp, isEncodingRequired);
  }
  
  return imageurls;
 }
 
 /**
  * Gets the categories feed.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @param domainUrl
  *         the domain url
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param builder
  *         the builder
  * @param productPageList
  *         the product page list
  * @return the categories feed
  */
 public static CategoriesType getCategoriesFeed(String path, String domainUrl, ResourceResolver resourceResolver,
   ConfigurationService configurationService, List<Page> productPageList) {
  
  CategoriesType categories = new CategoriesType();
  List<Page> categoryPageList = new ArrayList<Page>();
  
  updateCategoryPages(path, categoryPageList, resourceResolver, configurationService, productPageList);
  
  for (Page page : categoryPageList) {
   String categoryId = getCategoryId(configurationService, page, false);
   if (StringUtils.isNotBlank(categoryId)) {
    categories.getCategory().add(RecipeRatingHelper.getCategoryFeed(page, domainUrl, configurationService));
   }
  }
  
  return categories;
 }
 
 /**
  * Update category pages.
  *
  * @param path
  *         the path
  * @param categoryPageList
  *         the category page list
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param productPageList
  *         the product page list
  */
 private static void updateCategoryPages(String path, List<Page> categoryPageList, ResourceResolver resourceResolver,
   ConfigurationService configurationService, List<Page> productPageList) {
  
  Page page = resourceResolver.getResource(path).adaptTo(Page.class);
  String recipeConfigPagePath = GlobalConfigurationUtility.getValueFromConfiguration(configurationService, page, "recipeConfig", "url");
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  if (pageManager != null && StringUtils.isNotBlank(recipeConfigPagePath) && pageManager.getPage(recipeConfigPagePath) != null) {
   Page recipeConfigPage = pageManager.getPage(recipeConfigPagePath);
   Iterable<Resource> child = recipeConfigPage.getContentResource().getChild("par").getChildren();
   for (Resource res : child) {
    if (RECIPE_CONFIG_RESOURCETYPE.equals(res.getResourceType())) {
     ValueMap properties = res.getValueMap();
     
     String recipeLandingPagePath = MapUtils.getString(properties, "recipeLandingPagePath", StringUtils.EMPTY);
     String recipeDetailTemplatePagePath = MapUtils.getString(properties, "basicRecipeDetailTemplatePagePath", StringUtils.EMPTY);
     String enhancedRecipeDetailTemplatePagePath = MapUtils.getString(properties, "enhancedRecipeDetailTemplatePagePath", StringUtils.EMPTY);
     
     Page p = pageManager.getPage(recipeLandingPagePath);
     if (p != null) {
      categoryPageList.add(p);
     }
     
     updateProductPages(recipeDetailTemplatePagePath, recipeLandingPagePath, pageManager, resourceResolver, productPageList);
     updateProductPages(enhancedRecipeDetailTemplatePagePath, recipeLandingPagePath, pageManager, resourceResolver, productPageList);
    }
   }
  }
 }
 
 /**
  * Update product pages.
  *
  * @param recipeTemplatePagePath
  *         the recipe template page path
  * @param recipeLandingPagePath
  *         the recipe landing page path
  * @param pageManager
  *         the page manager
  * @param resourceResolver
  *         the resource resolver
  * @param productPageList
  *         the product page list
  */
 private static void updateProductPages(String recipeTemplatePagePath, String recipeLandingPagePath, PageManager pageManager,
   ResourceResolver resourceResolver, List<Page> productPageList) {
  if (StringUtils.isNotBlank(recipeTemplatePagePath)) {
   Page recipeTemplatePage = pageManager.getPage(recipeTemplatePagePath);
   String[] recipeTargetFilter = { recipeLandingPagePath };
   if (recipeTemplatePage != null) {
    LiveRelationshipManager liveRelationshipManager = resourceResolver.adaptTo(LiveRelationshipManager.class);
    try {
     Collection<LiveRelationship> liveRelationShipCollection = liveRelationshipManager.getLiveRelationships(recipeTemplatePage, null,
       recipeTargetFilter, false);
     if (!CollectionUtils.isEmpty(liveRelationShipCollection)) {
      for (LiveRelationship lr : liveRelationShipCollection) {
       Resource r = resourceResolver.getResource(lr.getTargetPath());
       if (r != null) {
        Page pg = pageManager.getContainingPage(r);
        productPageList.add(pg);
       }
      }
     }
    } catch (WCMException e) {
     LOGGER.error("Unable to get live copies for the page : " + recipeTemplatePage.getPath(), e);
    }
   }
  }
 }
 
 /**
  * Gets the parent category id.
  *
  * @param page
  *         the page
  * @param configurationService
  *         the configurationService
  * @return the parent category id
  */
 public static String getParentCategoryId(ConfigurationService configurationService, Page page) {
  String categoryId = StringUtils.EMPTY;
  if (page != null) {
   categoryId = getCategoryId(configurationService, page.getParent(), false);
  }
  return categoryId;
 }
 
 /**
  * Gets the product family prefix.
  *
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the product family prefix
  */
 public static String getProductFamilyPrefix(ConfigurationService configurationService, Page page) {
  String prefix = StringUtils.EMPTY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config.get("productFamilyPrefix") != null) {
    prefix = (String) config.get("productFamilyPrefix");
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for {} ", FEED_CATEGORY, e);
  }
  return prefix;
 }
 
 /**
  * Gets the category namespace.
  *
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the category namespace
  */
 private static String[] getCategoryNamespace(ConfigurationService configurationService, Page page) {
  String[] namespace = ArrayUtils.EMPTY_STRING_ARRAY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config.get("categoryNamespace") != null) {
    String categoryNamespace = (String) config.get("categoryNamespace");
    if (StringUtils.isNotBlank(categoryNamespace)) {
     namespace = StringUtils.split(categoryNamespace, ",");
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for {}.", FEED_CATEGORY, e);
  }
  return namespace;
 }
 
 /**
  * Finds the parent and super parent categories for a product based upon the tags associated with the product.
  *
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @param tags
  *         the tags
  * @return list of parent and super parent categories
  */
 public static List<CategoryDetail> getParentCategoriesFromProduct(ConfigurationService configurationService, Page page, Tag[] tags) {
  
  List<CategoryDetail> categoryList = new ArrayList<CategoryDetail>();
  Map<String, Tag> namespaceTags = new HashMap<String, Tag>();
  String[] categoryNamespace = getCategoryNamespace(configurationService, page);
  String parentId = getParentIdForCategory(configurationService, page, tags);
  String familyPrefix = getProductFamilyPrefix(configurationService, page);
  for (String categoryName : categoryNamespace) {
   Tag namespaceTag = getMatchingParentTag(tags, StringUtils.trim(categoryName));
   if (namespaceTag != null) {
    namespaceTags.put(categoryName, namespaceTag);
   }
  }
  if (ArrayUtils.isNotEmpty(categoryNamespace)) {
   while (categoryNamespace.length == namespaceTags.size()) {
    String categoryId = StringUtils.EMPTY;
    for (String categoryName : categoryNamespace) {
     Tag tag = namespaceTags.get(categoryName);
     categoryId = categoryId + tag.getLocalTagID().toString().replace("/", "").replace("-", "");
     tag = tag.getParent();
     if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), categoryName) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), categoryName)) {
      namespaceTags.replace(categoryName, tag);
     } else {
      namespaceTags.remove(categoryName);
     }
    }
    if (StringUtils.isNotBlank(categoryId)) {
     CategoryDetail secondaryCategory = new CategoryDetail();
     secondaryCategory.setCategoryId(parentId + categoryId);
     secondaryCategory.setFamilyId(familyPrefix + parentId + categoryId);
     if (StringUtils.isNotBlank(secondaryCategory.getCategoryId())) {
      categoryList.add(secondaryCategory);
     }
    }
   }
  }
  
  return categoryList;
 }
 
 /**
  * Get parent category namespace.
  *
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return namespace
  */
 private static String getParentCategoryNamespace(ConfigurationService configurationService, Page page) {
  String namespace = StringUtils.EMPTY;
  try {
   Map<String, String> config = configurationService.getCategoryConfiguration(page, FEED_CATEGORY);
   if (config.get("parentCategoryNamespace") != null) {
    namespace = (String) config.get("parentCategoryNamespace");
    
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Could not get config for {}", FEED_CATEGORY, e);
  }
  return namespace;
 }
 
 /**
  * Get parent category id.
  *
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @param tags
  *         the tags
  * @return parent category id.
  */
 private static String getParentIdForCategory(ConfigurationService configurationService, Page page, Tag[] tags) {
  String parentCategory = getParentCategoryNamespace(configurationService, page);
  String categoryId = StringUtils.EMPTY;
  if (StringUtils.isNotBlank(parentCategory)) {
   for (Tag t : tags) {
    if (StringUtils.startsWithIgnoreCase(t.getLocalTagID(), parentCategory) && !StringUtils.equalsIgnoreCase(t.getLocalTagID(), parentCategory)) {
     categoryId = t.getLocalTagID().toString().replace("/", "").replace("-", "");
     break;
    }
   }
  }
  return categoryId;
 }
 
 /**
  * Returns matching parent tag from passed in namespace.
  *
  * @param tags
  *         the tags
  * @param namespace
  *         the namespace
  * @return parent tag of the matching tag
  */
 private static Tag getMatchingParentTag(Tag[] tags, String namespace) {
  Tag matchingTag = null;
  for (Tag tag : tags) {
   if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), namespace) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), namespace)) {
    tag = tag.getParent();
    if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), namespace) && !StringUtils.equalsIgnoreCase(tag.getLocalTagID(), namespace)) {
     matchingTag = tag;
     break;
    }
   }
  }
  return matchingTag;
 }
 
 /**
  * Returns matching tag for passed in namespace.
  *
  * @param tags
  *         the tags
  * @param namespace
  *         the namespace
  * @return matching tag
  */
 private static Tag getMatchingTag(Tag[] tags, String namespace) {
  Tag matchingTag = null;
  for (Tag tag : tags) {
   if (StringUtils.startsWithIgnoreCase(tag.getLocalTagID(), namespace)) {
    matchingTag = tag;
    break;
   }
  }
  return matchingTag;
 }
 
 /**
  * Gets the encodedPath.
  *
  * @param path
  *         the path
  * @param isEncodingRequired
  *         the is encoding required
  * @return the encodedPath
  */
 private static String getEncodedOrSamePath(String path, String isEncodingRequired) {
  
  String encodedPath = StringUtils.EMPTY;
  if (("true").equalsIgnoreCase(isEncodingRequired)) {
   String[] pathArray = path.split("/");
   StringBuilder stringBuilder = new StringBuilder();
   for (int i = 0; i < pathArray.length; i++) {
    try {
     String segment = java.net.URLEncoder.encode(pathArray[i], "UTF-8");
     if (pathArray.length - 1 == i) {
      stringBuilder.append(segment);
     } else {
      stringBuilder.append(segment + "/");
     }
    } catch (UnsupportedEncodingException e) {
     LOGGER.error("Error occured while encoding", e);
    }
   }
   encodedPath = stringBuilder.toString();
  } else {
   encodedPath = path;
  }
  
  return encodedPath;
 }
 
 /**
  * Gets the encodingRequired.
  *
  * @param page
  *         the page
  * @param configurationService
  *         the configurationService
  * @return the encodingRequired
  */
 private static String getEncodingRequired(Page page, ConfigurationService configurationService) {
  
  String encodingRequired = StringUtils.EMPTY;
  try {
   encodingRequired = configurationService.getConfigValue(page, FEED_CATEGORY, IS_ENCODING_REQUIRED);
  } catch (ConfigurationNotFoundException e1) {
   LOGGER.error("isEncodingRequired key not found in the category productFeedConfiguration", e1);
  }
  return encodingRequired;
 }
 
 /**
  * Gets the image pathfrom node.
  *
  * @param recipeId
  *         the recipe id
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param page
  *         the page
  * @return the image pathfrom node
  */
 private static String getImagePathfromNode(String recipeId, ResourceResolver resourceResolver, ConfigurationService configurationService,
   Page page) {
  String imageUrl = StringUtils.EMPTY;
  
  try {
   String metadataPath = configurationService.getConfigValue(page, "recipeConfig", "metadataPath");
   if (StringUtils.isNotBlank(metadataPath)) {
    Resource recipeResource = resourceResolver.getResource(metadataPath + "/" + recipeId);
    if (recipeResource != null) {
     ValueMap map = recipeResource.getValueMap();
     if (map.containsKey("teaserImage")) {
      imageUrl = map.get("teaserImage").toString();
     }
    }
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("metadatapath not found for recipe Id : " + recipeId, e);
  }
  
  return imageUrl;
 }
 
 /**
  * Gets the recipe feed.
  *
  * @param page
  *         the page
  * @param domainUrl
  *         the domain url
  * @param configurationService
  *         the configuration service
  * @param resourceResolver
  *         the resource resolver
  * @return the recipe feed
  */
 public static ProductType getRecipeFeed(Page page, String domainUrl, ConfigurationService configurationService, ResourceResolver resourceResolver) {
  ProductType product = null;
  String recipeId = "";
  String title = "";
  String shortDescription = "";
  String teaserImage = StringUtils.EMPTY;
  
  if (page != null) {
   product = new ProductType();
   try {
    if (page.getProperties().get("recipeId") != null) {
     recipeId = page.getProperties().get("recipeId").toString();
    }
    
    if (page.getProperties().get(JcrConstants.JCR_TITLE) != null) {
     title = page.getProperties().get(JcrConstants.JCR_TITLE).toString();
    }
    
    if (page.getProperties().get("shortDescription") != null) {
     shortDescription = page.getProperties().get("shortDescription").toString();
    }
    
    if (StringUtils.isNotBlank(recipeId)) {
     teaserImage = page.getProperties().get("teaserImage") != null ? page.getProperties().get("teaserImage").toString()
       : getImagePathfromNode(recipeId, resourceResolver, configurationService, page);
    }
    
    product.setProductId(recipeId);
    product.setCategoryDetail(getCategoryDataForProduct(configurationService, page));
    product.setName(title);
    product.setImageUrl(teaserImage);
    product.setDescription(shortDescription);
    product.setProductPageUrl(getPageFullUrls(page, domainUrl, configurationService));
    product.setIdentifier(null);
    product.setCategoryId(getCategoryId(configurationService, page, true));
    product.setProductVariants(null);
    ReplicationStatus status = page.adaptTo(ReplicationStatus.class);
    if (status != null) {
     if (status.isActivated()) {
      product.setActive(true);
     } else if (status.isDeactivated()) {
      product.setActive(false);
     } else {
      product.setActive(false);
     }
    }
    
   } catch (Exception e) {
    LOGGER.error("error while getting product from page : " + page.getPath(), e);
   }
  }
  return product;
 }
 
}
