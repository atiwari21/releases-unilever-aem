/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class SocialChannelServlet.
 */
@SlingServlet(label = "Unilever Social Channel Servlet to Fetch channels list", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = {
  HttpConstants.METHOD_POST }, selectors = { "schemaconfiguration" }, extensions = { "html" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.foundation.components.SchemaConfigurationServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever Social Channel Servlet to Fetch channels list", propertyPrivate = false),

})
public class SchemaConfigurationServlet extends SlingAllMethodsServlet {
 
 private static final String COLUMN = "column";
 
 private static final String PIM_FIELD_DESCRIPTION = "pimFieldDescription";
 
 private static final String DESCRIPTION = "description";
 
 private static final String PIM_FIELD = "pimField";
 
 private static final String DATA_APPLICABLE_BRAND = "dataApplicableBrand";
 
 private static final String REGEX = "regex";
 
 private static final String DATA_MUST_CONTAIN = "dataMustContain";
 
 private static final String MAX_LENGTH = "maxLength";
 
 private static final String MIN_LENGTH = "minLength";
 
 private static final String MANDATORY_DEFAULT_VALUE = "mandatoryDefaultValue";
 
 private static final String MANDATORY_VARIANT = "mandatoryVariant";
 
 private static final String UNIQUE_VALUES = "uniqueValues";
 
 private static final String READ_ONLY = "readOnly";
 
 private static final String MANDATORY = "mandatory";
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(SchemaConfigurationServlet.class);
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
  
  String counter = request.getParameter("count");
  ResourceResolver rs = request.getResourceResolver();
  Session session = rs.adaptTo(Session.class);
  int count = counter != null ? Integer.parseInt(counter) : 0;
  
  for (int start = 1; start <= count; start++) {
   String componentPath = request.getParameter("componentPath" + start);
   String pimFieldDescription = request.getParameter(PIM_FIELD_DESCRIPTION + start);
   String pimField = request.getParameter(PIM_FIELD + start);
   String description = request.getParameter(DESCRIPTION + start);
   String column = request.getParameter(COLUMN + start);
   String dataApplicableBrand = request.getParameter(DATA_APPLICABLE_BRAND + start);
   String mandatory = request.getParameter(MANDATORY + start);
   String mandatoryVariant = request.getParameter(MANDATORY_VARIANT + start);
   String mandatoryDefaultValue = request.getParameter(MANDATORY_DEFAULT_VALUE + start);
   String uniqueValues = request.getParameter(UNIQUE_VALUES + start);
   String dataMustContain = request.getParameter(DATA_MUST_CONTAIN + start);
   String minLength = request.getParameter(MIN_LENGTH + start);
   String maxLength = request.getParameter(MAX_LENGTH + start);
   String readOnly = request.getParameter(READ_ONLY + start);
   String regex = request.getParameter(REGEX + start);
   
   if (StringUtils.isNotBlank(componentPath) && (rs != null) && (pimField != null)) {
    Node currentNode = rs.getResource(componentPath).adaptTo(Node.class);
    
    setBooleanProperty(currentNode, DATA_APPLICABLE_BRAND, dataApplicableBrand, componentPath);
    setBooleanProperty(currentNode, MANDATORY, mandatory, componentPath);
    setBooleanProperty(currentNode, MANDATORY_VARIANT, mandatoryVariant, componentPath);
    setBooleanProperty(currentNode, UNIQUE_VALUES, uniqueValues, componentPath);
    setBooleanProperty(currentNode, READ_ONLY, readOnly, componentPath);
    setTextProperty(currentNode, PIM_FIELD_DESCRIPTION, pimFieldDescription, componentPath);
    setTextProperty(currentNode, PIM_FIELD, pimField, componentPath);
    setTextProperty(currentNode, DESCRIPTION, description, componentPath);
    setTextProperty(currentNode, COLUMN, column, componentPath);
    setTextProperty(currentNode, MANDATORY_DEFAULT_VALUE, mandatoryDefaultValue, componentPath);
    setTextProperty(currentNode, DATA_MUST_CONTAIN, dataMustContain, componentPath);
    setTextProperty(currentNode, MIN_LENGTH, minLength, componentPath);
    setTextProperty(currentNode, MAX_LENGTH, maxLength, componentPath);
    setTextProperty(currentNode, REGEX, regex, componentPath);
    
   }
   
  }
  
  try {
   session.save();
  } catch (RepositoryException e) {
   LOGGER.error("Error while getting resource node", e);
  }
  try {
   response.sendRedirect(request.getPathInfo() + "?wcmmode=disabled");
  } catch (IOException ie) {
   LOGGER.error("Error while redirecting", ie);
  }
 }
 
 private void setBooleanProperty(Node node, String property, String value, String componentPath) {
  try {
   if (value != null) {
    node.setProperty(property, "true");
   } else {
    if (node.hasProperty(property)) {
     node.getProperty(property).remove();
    }
   }
   
  } catch (RepositoryException e) {
   LOGGER.error("Error while fetching resource : " + componentPath, e);
  }
 }
 
 private void setTextProperty(Node node, String property, String value, String componentPath) {
  try {
   node.setProperty(property, value);
  } catch (RepositoryException e) {
   LOGGER.error("Error while fetching resource : " + componentPath, e);
  }
 }
}
