/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Header;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Image;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class HeaderImageRule.
 */
public class HeaderImageRule extends ConfigurationSelectorRule {
 
 private static final String PROPERTY_IMAGE_URL = "image.url";
 
 /**
  * Instantiates a new header image rule.
  */
 private HeaderImageRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the header image rule
  */
 public static HeaderImageRule create() {
  return new HeaderImageRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the header image rule
  */
 public static HeaderImageRule createFrom(JSONObject configuration) {
  HeaderImageRule headerImageRule = create();
  headerImageRule.withSelector(configuration.getString("selector"));
  JSONObject headerImageConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_IMAGE_URL);
  headerImageRule.withProperties(properties, headerImageConfiguration);
  return headerImageRule;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(java.lang.Object)
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer , com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Image image = Image.create();
  String url = (String) this.getProperty(PROPERTY_IMAGE_URL, node);
  
  if (StringUtils.isNotBlank(url)) {
   image.withUrl(url);
   ((Header) container).withCover(image);
  }
  
  transformer.transform(image, node);
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Header.getClassName() };
 }
 
}
