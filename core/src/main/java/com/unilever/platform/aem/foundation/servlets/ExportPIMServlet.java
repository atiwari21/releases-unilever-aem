/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.common.AbstractJcrProduct;
import com.day.cq.commons.ImageResource;
import com.day.cq.i18n.I18n;
import com.day.cq.tagging.TagConstants;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.integration.dto.SchemaConfigurationDTO;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;

/**
 * The Class ExportPIMServlet.
 */
@SlingServlet(label = "Unilever export pim servlet", methods = { HttpConstants.METHOD_GET }, selectors = { "exportunileverpimmatrix" }, extensions = {
  ".html" }, paths = { "/bin/exportpim" })
public class ExportPIMServlet extends SlingAllMethodsServlet {
 
 /** The Constant NO_SCHEMA. */
 private static final String NO_SCHEMA = "noSchema";
 
 private static final String NO_COMMERCE_PROVIDER = "noCommerceProvider";
 
 /** The Constant NO. */
 private static final String NO = "no";
 
 /** The Constant MASTER_SCHEMA. */
 private static final String MASTER_SCHEMA = "masterSchema";
 
 /** The Constant SCHEMA_CONFIGURATION_MASTER_SHEET_NOT_FOUND. */
 private static final String SCHEMA_CONFIGURATION_MASTER_SHEET_NOT_FOUND = "schemaConfiguration.masterSheetNotFound";
 
 /** The Constant SCHEMA_CONFIGURATION_NO_SCHEMA_FOUND. */
 private static final String SCHEMA_CONFIGURATION_NO_SCHEMA_FOUND = "schemaConfiguration.noSchemaFound";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ExportPIMServlet.class);
 
 /** The default column mappings. */
 private String[] defaultColumnMappings = { "content type=type", "status=action", "EAN=sku", "EANparent=skuParent", "productName=name",
   "productImage=imageIds", "productSize=size", "shortTitle=title", "shortProductDescription=description", "productTags=cqTags",
   "smartProductID=smartProductId", "imageType=productSize", "CustomizeImageId=customizeImageIds", "recipientsInfo=recipientsInfo",
   "WrapperInfo=wrapperInfo", "NutritionFacts=nutritionalFacts", "productID=uniqueID", "productPrice=price", "productLaunchDate=productionDate",
   "CustomizeProductPrice=customizeProductPrice", "CustomizeDescription=customizeDescription", "CustomizeImageAltText=customizeImageAltText",
   "CustomizeGiftWrapDescription=customizeGiftWrapDescription", "ChildProducts=childProducts", "Allergy=allergy", "lastmodifiedtime=jcr:lastModified",
   "imageFormat=imageFormat" };
 
 private String[] rewardDefaultColumnMappings = { "status=action", "rewardID=sku", "rewardParentID=skuParent", "rewardName=name",
   "rewardImage=imageIds", "rewardVariant=rewardVariant", "rewardTitle=title", "imageFormat=imageFormat", "rewardPointPrice=price",
   "rewardDescription=description", "rewardStartDate=rewardAvailableFrom", "rewardEndDate=rewardAvailableTo", "rewardStock=quantity",
   "rewardTags=cqTags", "aboutThisRewardDescription=aboutThisRewardDescription", "lastmodifiedtime=jcr:lastModified" };
 
 /** The props. */
 protected Map<String, String> props = new LinkedHashMap<String, String>();
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = -7750634588642660778L;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant THREE. */
 private static final int THREE = 3;
 
 /** The Constant FOUR. */
 private static final int FOUR = 4;
 
 /** The Constant FIVE. */
 private static final int FIVE = 5;
 
 /** The Constant SIX. */
 private static final int SIX = 6;
 
 /** The Constant SEVEN. */
 private static final int SEVEN = 7;
 
 /** The Constant EIGHT. */
 private static final int EIGHT = 8;
 
 /** The Constant NINE. */
 private static final int NINE = 9;
 
 /** The Constant TEN. */
 private static final int TEN = 10;
 
 /** The Constant ELEVEN. */
 private static final int ELEVEN = 11;
 
 /** The Constant TWELVE. */
 private static final int TWELVE = 12;
 private static final String EMPTY_STRING = "";
 
 /**
  * Do get.
  *
  * @param request
  *         the request
  * @param response
  *         the response
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected final void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  ResourceResolver rs = request.getResourceResolver();
  I18n i18n = new I18n(request);
  if (rs != null) {
   try {
    String suffix = request.getRequestPathInfo().getSuffix();
    Session session = rs.adaptTo(Session.class);
    List<UNIProduct> products = new ArrayList<UNIProduct>();
    Map<String, SchemaConfigurationDTO> pimFieldMap = new LinkedHashMap<String, SchemaConfigurationDTO>();
    
    Resource schemaPageResource = PIMInjestionUtility.getSchemaResource(suffix, rs);
    String commerceProvider = PIMInjestionUtility.getCommerceProvider(suffix, rs);
    if (StringUtils.isNotBlank(commerceProvider)) {
     schemaPageResource = schemaPageResource.getChild("jcr:content/par");
     LOGGER.info("schemaConfigurationNode -->" + schemaPageResource);
     if (schemaPageResource != null) {
      PIMInjestionUtility.getPIMSchemaConfiguration(schemaPageResource, pimFieldMap);
      LOGGER.info("pimFieldMap Export-->" + pimFieldMap);
      products = PIMInjestionUtility.getProducts(session, suffix);
      
      InputStream is = PIMInjestionUtility.getInputStreamFromResource(rs, response, commerceProvider);
      if (is != null) {
       setColumnMappings(commerceProvider);
       processPIMMatrix(is, products, response, rs, commerceProvider, pimFieldMap);
      } else {
       setResponseWriterNoSchema(response, MASTER_SCHEMA, NO);
       response.getWriter().write(i18n.get(SCHEMA_CONFIGURATION_MASTER_SHEET_NOT_FOUND));
      }
     } else {
      setResponseWriterNoSchema(response, NO_SCHEMA, NO);
      response.getWriter().write(i18n.get(SCHEMA_CONFIGURATION_NO_SCHEMA_FOUND));
     }
     response.getWriter().close();
    } else {
     setResponseWriterNoSchema(response, NO_COMMERCE_PROVIDER, NO);
     response.getWriter().write("Error : CommerceProvider not found for path : " + suffix);
    }
   } catch (Exception e) {
    LOGGER.error("Exception found while geting selected schema", e);
   }
  }
 }
 
 /**
  * Sets the response writer no schema.
  *
  * @param response
  *         the response
  * @param header
  *         the header
  * @param headerValue
  *         the header value
  */
 private void setResponseWriterNoSchema(final SlingHttpServletResponse response, String header, String headerValue) {
  response.setHeader("Content-Type", "text/html");
  response.setHeader(header, headerValue);
 }
 
 /**
  * Sets the column mappings.
  */
 protected void setColumnMappings(String commerceProvider) {
  String[] columnMappings = ArrayUtils.EMPTY_STRING_ARRAY;
  
  if ("rewards".equals(commerceProvider)) {
   columnMappings = rewardDefaultColumnMappings;
  } else {
   columnMappings = defaultColumnMappings;
  }
  
  for (String s : columnMappings) {
   String[] prop = s.split("=");
   props.put(prop[0], prop[1]);
  }
  
  LOGGER.info("props-->" + props);
 }
 
 /**
  * Process pim matrix.
  *
  * @param is
  *         the is
  * @param products
  *         the products
  * @param response
  *         the response
  * @throws InvalidFormatException
  *          the invalid format exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private void processPIMMatrix(InputStream is, List<UNIProduct> products, SlingHttpServletResponse response, ResourceResolver rs,
   String commerceProvider, Map<String, SchemaConfigurationDTO> pimFieldMap) throws InvalidFormatException, IOException {
  if (is != null) {
   LOGGER.info("processing pim matrix");
   
   BufferedInputStream input = null;
   BufferedOutputStream output = null;
   
   OPCPackage opcPackage = OPCPackage.open(is);
   XSSFWorkbook hssfWorkbook = new XSSFWorkbook(opcPackage);
   
   XSSFSheet masterConfigSheet = hssfWorkbook.getSheet(PIMInjestionUtility.PIM_MASTERCONFIGURATION_SHEET_NAME);
   if (masterConfigSheet != null) {
    LOGGER.info("master config sheet");
    LOGGER.info("pim field map size --> " + pimFieldMap.size());
    Iterator<Row> rowIterator = masterConfigSheet.iterator();
    
    for (int rowCount = 0; rowCount <= pimFieldMap.size() + 1; rowCount++) {
     if (rowIterator.hasNext() && rowCount > 0) {
      Row row = rowIterator.next();
      String category = row.getCell(1).getStringCellValue().trim();
      LOGGER.info("category --> " + category);
      if (pimFieldMap.containsKey(category)) {
       SchemaConfigurationDTO categoryMap = pimFieldMap.get(category);
       setCellValue(row, 0, categoryMap.getPimFieldDescription());
       setCellValue(row, 1, categoryMap.getFieldName());
       setCellValue(row, TWO, categoryMap.getDescription());
       setCellValue(row, THREE, categoryMap.getColumn());
       setCellValue(row, FOUR, categoryMap.getIsApplicable());
       setCellValue(row, FIVE, categoryMap.getMandatory());
       setCellValue(row, SIX, categoryMap.getMandatoryVariant());
       setCellValue(row, SEVEN, categoryMap.getMandatoryDefaultValue());
       setCellValue(row, EIGHT, categoryMap.getUniqueValues());
       setCellValue(row, NINE, categoryMap.getDataMustContain());
       setCellValue(row, TEN, categoryMap.getMinLength());
       setCellValue(row, ELEVEN, categoryMap.getMaxLength());
       setCellValue(row, TWELVE, categoryMap.getRegex());
       
       LOGGER.info("categoryMap --> " + categoryMap);
      }
     }
    }
    masterConfigSheet.enableLocking();
    LOGGER.info("masterConfigSheet enabling locking --> ");
   }
   XSSFSheet pimSheet = null;
   int startRowNo = TEN;
   if ("rewards".equals(commerceProvider)) {
    pimSheet = hssfWorkbook.getSheet(PIMInjestionUtility.RIM_REWARDS_DATA_SHEET_NAME);
    startRowNo = 2;
   } else {
    pimSheet = hssfWorkbook.getSheet(PIMInjestionUtility.PIM_PRODUCT_DATA_SHEET_NAME);
   }
   
   int variantRowNo = 0;
   if (pimSheet != null) {
    for (UNIProduct product : products) {
     for (String field : pimFieldMap.keySet()) {
      SchemaConfigurationDTO scaObj = pimFieldMap.get(field);
      
      int columnNumber = PIMInjestionUtility.titleToNumber(scaObj.getColumn()) - 1;
      
      XSSFRow row = pimSheet.getRow(startRowNo);
      if (row == null) {
       LOGGER.debug("row does not exist at line no : " + startRowNo);
       row = pimSheet.createRow(startRowNo);
       LOGGER.debug("row created for line no : " + startRowNo);
      }
      
      Cell cell = pimSheet.getRow(startRowNo).getCell(columnNumber);
      if (cell == null) {
       cell = pimSheet.getRow(startRowNo).createCell(columnNumber);
      }
      
      // set cell value
      setCellValueForProductProperty(cell, product, scaObj, rs);
      Iterator<Product> varinats;
      try {
       varinats = product.getVariants();
       variantRowNo = startRowNo;
       while (varinats.hasNext()) {
        UNIProduct uniProduct = PIMInjestionUtility.getUniProduct(varinats.next());
        if (!uniProduct.getSKU().equals(product.getSKU())) {
         XSSFRow variantRow = pimSheet.getRow(variantRowNo + 1);
         if (variantRow == null) {
          LOGGER.debug("row does not exist at line no : " + variantRowNo + 1);
          variantRow = pimSheet.createRow(variantRowNo + 1);
          LOGGER.debug("row created for line no : " + variantRowNo + 1);
         }
         
         Cell cellVariant = pimSheet.getRow(variantRowNo + 1).getCell(columnNumber);
         if (cellVariant == null) {
          cellVariant = pimSheet.getRow(variantRowNo + 1).createCell(columnNumber);
         }
         // set variant cell value
         setCellValueForProductProperty(cellVariant, uniProduct, scaObj, rs);
         variantRowNo++;
        }
       }
      } catch (CommerceException e) {
       LOGGER.error("Error while getting product variants", e);
      }
     }
     startRowNo = variantRowNo;
     startRowNo++;
    }
   }
   
   // hide columns in sheet
   for (String field : pimFieldMap.keySet()) {
    SchemaConfigurationDTO scaObj = pimFieldMap.get(field);
    if (!("Y".equals(scaObj.getIsApplicable()))) {
     int columnNumber = PIMInjestionUtility.titleToNumber(scaObj.getColumn()) - 1;
     pimSheet.setColumnHidden(columnNumber, true);
    }
   }
   
   input = new BufferedInputStream(is);
   output = new BufferedOutputStream(response.getOutputStream());
   hssfWorkbook.write(output);
   try {
    hssfWorkbook.close();
   } catch (Exception e) {
    LOGGER.error("error while clossing workbook : ", e);
   }
   if (output != null) {
    output.close();
   }
   
   if (input != null) {
    input.close();
   }
  }
 }
 
 private void setCellValue(Row row, int cellNumber, String value) {
  Cell cell = row.getCell(cellNumber);
  if (cell == null) {
   cell = row.createCell(cellNumber);
  }
  
  if (cell != null) {
   cell.setCellValue(value);
  }
 }
 
 /**
  * Sets the cell value for product property.
  *
  * @param cell
  *         the cell
  * @param product
  *         the product
  * @param scaObj
  *         the sca obj
  */
 private void setCellValueForProductProperty(Cell cell, UNIProduct product, SchemaConfigurationDTO scaObj, ResourceResolver rs) {
  
  String value = StringUtils.EMPTY;
  String pimFieldName = scaObj.getFieldName();
  boolean singleProperty = true;
  boolean multipleValues = false;
  int index = 0;
  String propertyName = StringUtils.EMPTY;
  boolean isVariant = AbstractJcrProduct.isAVariant(product.adaptTo(Resource.class));
  
  if (pimFieldName.contains("#")) {
   singleProperty = false;
   multipleValues = true;
   String[] arr = pimFieldName.split("#");
   index = Integer.parseInt(arr[0]);
   String[] prop = arr[1].split("\\.");
   propertyName = prop[1];
   pimFieldName = prop[0];
   
  } else if (pimFieldName.contains(".")) {
   singleProperty = false;
   String[] prop = pimFieldName.split("\\.");
   index = -1;
   propertyName = prop[1];
   pimFieldName = prop[0];
  }
  Resource productNode = product.adaptTo(Resource.class);
  if (props.containsKey(pimFieldName) && !singleProperty) {
   if (multipleValues) {
    // get property from aem an set based on index
    propertyName = Character.toLowerCase(propertyName.charAt(0)) + propertyName.substring(1);
    if (productNode != null) {
     if (index == 1) {
      value = getProductPropertyValue(value, pimFieldName, propertyName, productNode, -1);
     } else if (index > 1) {
      index = index - TWO;
      value = getProductPropertyValue(value, pimFieldName, propertyName, productNode, index);
     }
    }
   } else if (index == -1 && productNode != null) {
    // get property from aem and set
    value = getProductPropertyValue(value, pimFieldName, propertyName, productNode, index);
   }
  } else if (props.containsKey(pimFieldName)) {
   if (("productImage").equals(pimFieldName)) {
    value = getImageIdsFromProduct(product, rs, isVariant);
   } else if (("productTags").equals(pimFieldName)) {
    value = getTagsFromProduct(product);
   } else if (("lastmodifiedtime").equals(pimFieldName)) {
    value = getDatePropertyFromProduct(product, props.get(pimFieldName), isVariant);
   } else if (("imageFormat").equals(pimFieldName)) {
    value = getImageFormatFromProduct(product, rs, isVariant);
   } else {
    value = getPropertyFromProduct(product, props.get(pimFieldName), isVariant);
    value = (value == null) ? String.valueOf(product.getProperty(props.get(pimFieldName), BigDecimal.class)) : value;
   }
  } else {
   if (("altTextImages").equals(pimFieldName)) {
    value = getAltTextFromProduct(product, isVariant);
   } else {
    value = getPropertyFromProduct(product, pimFieldName, isVariant);
   }
  }
  
  if (StringUtils.isBlank(value) || value == null || ("null").equals(value)) {
   if ("Y".equals(scaObj.getMandatory())) {
    value = scaObj.getMandatoryDefaultValue();
   } else {
    value = StringUtils.EMPTY;
   }
  }
  
  if (cell != null) {
   cell.setCellValue(value);
  }
 }
 
 private String getPropertyFromProduct(UNIProduct product, String pimFieldName, boolean isVariant) {
  String value = null;
  if (isVariant) {
   try {
    Node variantNode = product.adaptTo(Node.class);
    if (variantNode.hasProperty(pimFieldName)) {
     Property p = variantNode.getProperty(pimFieldName);
     if (p.getType() == PropertyType.STRING) {
      value = variantNode.getProperty(pimFieldName).getValue().getString();
     } else if (p.getType() == PropertyType.DECIMAL) {
      value = String.valueOf(variantNode.getProperty(pimFieldName).getValue().getDecimal());
     }
    }
   } catch (RepositoryException e) {
    LOGGER.error("Unable to get property for variant : ", e);
   }
  } else {
   value = product.getProperty(pimFieldName, String.class);
  }
  return value;
 }
 
 private String getDatePropertyFromProduct(UNIProduct product, String pimFieldName, boolean isVariant) {
  String value = StringUtils.EMPTY;
  try {
   if (isVariant) {
    Node variantNode = product.adaptTo(Node.class);
    if (variantNode.hasProperty(pimFieldName)) {
     Property p = variantNode.getProperty(pimFieldName);
     if (p.getType() == PropertyType.STRING) {
      value = variantNode.getProperty(pimFieldName).getValue().getString();
     } else if (p.getType() == PropertyType.DECIMAL) {
      value = String.valueOf(variantNode.getProperty(pimFieldName).getValue().getDecimal());
     } else if (p.getType() == PropertyType.DATE) {
      Calendar cal = variantNode.getProperty(pimFieldName).getValue().getDate();
      if (cal != null) {
       SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.SSSZ");
       fmt.setCalendar(cal);
       value = fmt.format(cal.getTime());
      }
     }
    }
   } else {
    Node productNode = product.adaptTo(Node.class);
    if (productNode.hasProperty(pimFieldName)) {
     Property p = productNode.getProperty(pimFieldName);
     if (p.getType() == PropertyType.STRING) {
      value = productNode.getProperty(pimFieldName).getValue().getString();
     } else if (p.getType() == PropertyType.DECIMAL) {
      value = String.valueOf(productNode.getProperty(pimFieldName).getValue().getDecimal());
     } else if (p.getType() == PropertyType.DATE) {
      Calendar cal = productNode.getProperty(pimFieldName).getValue().getDate();
      if (cal != null) {
       SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.SSSZ");
       fmt.setCalendar(cal);
       value = fmt.format(cal.getTime());
      }
     }
    }
   }
  } catch (RepositoryException e) {
   LOGGER.error("Unable to get property for variant : ", e);
  }
  return value;
 }
 
 /**
  * Gets the product property value.
  *
  * @param value
  *         the value
  * @param pimFieldName
  *         the pim field name
  * @param propertyName
  *         the property name
  * @param productNode
  *         the product node
  * @return the product property value
  */
 private String getProductPropertyValue(String value, String pimFieldName, String propertyName, Resource productNode, int index) {
  Resource propertyNode = null;
  String localValue = value;
  if (index == -1) {
   propertyNode = productNode.getChild(props.get(pimFieldName) + "s/" + props.get(pimFieldName));
  } else {
   propertyNode = productNode.getChild(props.get(pimFieldName) + "s/" + props.get(pimFieldName) + String.valueOf(index));
  }
  if (propertyNode != null) {
   ValueMap map = propertyNode.getValueMap();
   if (map.containsKey(propertyName)) {
    localValue = map.get(propertyName).toString();
   }
  }
  return localValue;
 }
 
 private String getAltTextFromProduct(UNIProduct product, boolean isVariant) {
  StringBuilder altTextValue = new StringBuilder();
  List<String> altTextList = new ArrayList<String>();
  
  if (product != null) {
   List<ImageResource> imageList = new ArrayList<ImageResource>();
   if (isVariant) {
    Resource variantNode = product.adaptTo(Resource.class);
    Resource assetsNode = variantNode.getChild("assets");
    if (assetsNode != null && assetsNode.hasChildren()) {
     imageList = product.getImages();
    }
   } else {
    imageList = product.getImages();
   }
   
   if (!imageList.isEmpty()) {
    for (ImageResource image : imageList) {
     ValueMap map = image.getValueMap();
     if (map != null) {
      if (map.containsKey("altText")) {
       altTextList.add(map.get("altText").toString());
      } else {
       altTextList.add(StringUtils.EMPTY);
      }
     }
    }
   }
  }
  
  for (String id : altTextList) {
   altTextValue.append(id);
   altTextValue.append(",");
  }
  
  if (StringUtils.isNotBlank(altTextValue.toString())) {
   altTextValue = altTextValue.deleteCharAt(altTextValue.length() - 1);
  }
  
  return altTextValue.toString();
 }
 
 private String getImageIdsFromProduct(UNIProduct product, ResourceResolver rs, boolean isVariant) {
  StringBuilder value = new StringBuilder();
  List<String> imageIdList = new ArrayList<String>();
  List<String> videoIdList = new ArrayList<String>();
  
  if (product != null) {
   List<ImageResource> imageList = new ArrayList<ImageResource>();
   if (isVariant) {
    Resource variantNode = product.adaptTo(Resource.class);
    Resource assetsNode = variantNode.getChild("assets");
    if (assetsNode != null && assetsNode.hasChildren()) {
     imageList = product.getImages();
    }
   } else {
    imageList = product.getImages();
   }
   
   if (!imageList.isEmpty()) {
    for (ImageResource image : imageList) {
     String path = image.getFileReference();
     ValueMap map = image.getValueMap();
     if (map != null) {
      if (map.containsKey("videoId")) {
       videoIdList.add(map.get("videoId").toString());
      } else {
       videoIdList.add(StringUtils.EMPTY);
      }
     }
     if (StringUtils.isNotBlank(path)) {
      imageIdList.add(getImageIdFromPath(path, rs));
     } else {
      imageIdList.add(path);
     }
    }
   }
  }
  
  int index = 0;
  for (String id : imageIdList) {
   value.append(id);
   value.append("#");
   if (videoIdList.size() > index) {
    value.append(videoIdList.get(index));
   } else {
    value.append(StringUtils.EMPTY);
   }
   value.append(",");
   index++;
  }
  
  if (StringUtils.isNotBlank(value.toString())) {
   value = value.deleteCharAt(value.length() - 1);
   if ("#".equals(value.toString().substring(value.length() - 1))) {
    value = value.deleteCharAt(value.length() - 1);
   }
  }
  
  return value.toString();
 }
 
 private String getImageFormatFromProduct(UNIProduct product, ResourceResolver rs, boolean isVariant) {
  StringBuilder value = new StringBuilder();
  
  if (product != null) {
   List<ImageResource> imageList = new ArrayList<ImageResource>();
   if (isVariant) {
    Resource variantNode = product.adaptTo(Resource.class);
    Resource assetsNode = variantNode.getChild("assets");
    if (assetsNode != null && assetsNode.hasChildren()) {
     imageList = product.getImages();
    }
   } else {
    imageList = product.getImages();
   }
   
   if (!imageList.isEmpty()) {
    
    String path = imageList.get(0).getFileReference();
    
    if (StringUtils.isNotBlank(path)) {
     value.append(getImageFormatFromPath(path, rs));
    } else {
     value.append(EMPTY_STRING);
    }
   }
  }
  
  return value.toString();
 }
 
 private String getTagsFromProduct(UNIProduct product) {
  StringBuilder value = new StringBuilder();
  String[] tagsArray = ArrayUtils.EMPTY_STRING_ARRAY;
  
  if (product != null) {
   Resource productNode = product.adaptTo(Resource.class);
   ValueMap map = productNode.getValueMap();
   if (map.containsKey(TagConstants.PN_TAGS)) {
    tagsArray = map.get(TagConstants.PN_TAGS, String[].class);
   }
   
   if (!ArrayUtils.isEmpty(tagsArray)) {
    for (String tag : tagsArray) {
     value.append(tag).append(",");
    }
   }
  }
  
  if (StringUtils.isNotBlank(value.toString())) {
   value = value.deleteCharAt(value.length() - 1);
  }
  
  return value.toString();
 }
 
 private String getImageIdFromPath(String path, ResourceResolver rs) {
  Resource resource = rs.getResource(path + "/jcr:content/metadata");
  String imageID = StringUtils.EMPTY;
  if (resource != null) {
   try {
    imageID = resource.adaptTo(Node.class).hasProperty("recordID") ? resource.adaptTo(Node.class).getProperty("recordID").getValue().toString()
      : StringUtils.EMPTY;
   } catch (RepositoryException e) {
    LOGGER.error("Error while getting imageId for path : " + path, e);
   }
  }
  return imageID;
 }
 
 private String getImageFormatFromPath(String path, ResourceResolver rs) {
  Resource resource = rs.getResource(path + "/jcr:content/metadata");
  String imageFormat = StringUtils.EMPTY;
  if (resource != null) {
   try {
    imageFormat = resource.adaptTo(Node.class).hasProperty("dam:Fileformat")
      ? resource.adaptTo(Node.class).getProperty("dam:Fileformat").getValue().toString() : StringUtils.EMPTY;
   } catch (RepositoryException e) {
    LOGGER.error("Error while getting imageformat for specified image at path : " + path, e);
   }
  }
  return imageFormat;
 }
}
