/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class IFrame extends ElementWithHTML {
 
 /**
  * int The height of your soundcloud iframe.
  */
 private int height;
 
 public int getHeight() {
  return height;
 }
 
 public IFrame withHeight(int height) {
  this.height = height;
  return this;
 }
 
 public String getSource() {
  return source;
 }
 
 public IFrame withSource(String source) {
  this.source = source;
  return this;
 }
 
 public int getWidth() {
  return width;
 }
 
 public IFrame withWidth(int width) {
  this.width = width;
  return this;
 }
 
 /**
  * String The source of the content for your soundcloud iframe.
  */
 private String source;
 /**
  * int The width of your soundcloud iframe.
  */
 private int width;
 
 private IFrame() {
  
 }
 
 public static IFrame create() {
  return new IFrame();
 }
 
 @Override
 public Element toDOMElement() {
  
  return toDOMElement(null);
 }
 
 @Override
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  org.jsoup.nodes.Element figureElement = document.createElement("figure");
  org.jsoup.nodes.Element iframeElement = document.createElement("iframe");
  figureElement.appendChild(iframeElement);
  figureElement.attr("class", "op-interactive");
  
  if (StringUtils.isNotBlank(this.source)) {
   iframeElement.attr("src", this.source);
  }
  
  return figureElement;
 }
 
}
