/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import java.util.List;

/**
 * The Class BrandMarketBean.
 */
public class TaggingSearchCriteria {
 
 /** The tagList. */
 private List<String> tagList;
 
 /** The operator. */
 private String operator;
 
 public List<String> getTagList() {
  return tagList;
 }
 
 public void setTagList(List<String> tagList) {
  this.tagList = tagList;
 }
 
 public String getOperator() {
  return operator;
 }
 
 public void setOperator(String operator) {
  this.operator = operator;
 }
 
}
