/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Identifiers.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "identifier" })
public class Identifiers {
 
 /** The identifier. */
 @XmlElement(name = "Identifier")
 private List<Identifier> identifier;
 
 /**
  * Gets the identifier.
  *
  * @return the identifier
  */
 public List<Identifier> getIdentifier() {
  if (identifier == null) {
   identifier = new ArrayList<Identifier>();
  }
  return this.identifier;
 }
 
 /**
  * Sets the identifier.
  *
  * @param identifier
  *         the new identifier
  */
 public void setIdentifier(List<Identifier> identifier) {
  this.identifier = identifier;
 }
 
}
