/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Class Analytics.
 */
public class Analytics extends ElementWithHTML {
 
 /** The source. */
 private String source;
 
 
 /**
  * Instantiates a new analytics.
  */
 private Analytics(){
  
 }
 
 /**
  * Creates the.
  *
  * @return the analytics
  */
 public static Analytics create(){
  return new Analytics();
 }
 
 /**
  * With source.
  *
  * @param source
  *         the source
  * @return the analytics
  */
 public Analytics withSource(String source) {
  this.source = source;
  return this;
 }
 
 /**
  * Gets the source.
  *
  * @return the source
  */
 public String getSource() {
  return source;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement()
  */
 @Override
 public Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!this.isValid()) {
   return null;
  }
  Element figure = document.createElement("figure");
  Element iframe = document.createElement("iframe");
  figure.appendChild(iframe);
  figure.attr("class", "op-tracker");
  if (StringUtils.isNotBlank(this.source)) {
   iframe.attr("src", this.source);
  }
  if (this.html != null) {
   iframe.appendChild(this.html);
  }
  
  return figure;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#isValid()
  */
 @Override
 public boolean isValid() {
  return StringUtils.isNotBlank(this.source) || StringUtils.isNotBlank(this.html.text()) ? true : false;
 }
 
}
