/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SolrConfigurationService.
 */
@Component(name = "com.unilever.platform.aem.foundation.core.service.SolrConfigurationService", label = "AEM Solr Search - Solr Configuration Service", description = "A service for configuring Solr", immediate = true, metatype = true)
@Service(SolrConfigurationService.class)
@Properties({
  @Property(name = SolrAEMConfigurationServiceAdminConstants.AUTHOR_SOLR_DETAILS, value = { "author", "http://localhost:8983/solr", "admin",
    "admin" }, label = "Solr Author server details", description = "Solr Author Server Details"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.PUBLISH_SOLR_DETAILS, value = { "publish", "http://localhost:8983/solr", "admin",
    "admin" }, label = "Solr Publish server details", description = "Solr Publish Server Details"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.ALLOWED_REQUEST_HANDLERS, value = { "/select",
    "/dove" }, label = "Allowed request handlers", description = "Whitelist of allowed request handlers"),
  @Property(name = SolrAEMConfigurationServiceAdminConstants.COLLECTIONS, value = { "dove:en_gb:collection1",
    "knor:en_gb:collection1" }, label = "Brand Collection Mapping", description = "Brand & Collection Mapping List") })
/**
 * SolrConfigurationService provides a services for setting and getting Solr configuration information.
 */
public class SolrConfigurationService {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(SolrConfigurationService.class);
 
 /** The allowed request handlers. */
 private String[] allowedRequestHandlers;
 
 /** The solr author server array. */
 private String[] solrAuthorServerArray;
 
 /** The solr publish server array. */
 private String[] solrPublishServerArray;
 
 /** The solr Collection array. */
 private String[] solrCollectionsArray;
 
 /** The CONSTANT ADMIN. */
 private static final String ADMIN = "admin";
 
 /** The default allowed request handlers. */
 private static final String[] DEFAULT_ALLOWED_REQUEST_HANDLERS = new String[] { "/select", "/dove" };
 
 /** The Constant DEFAULT_AUTHOR_SOLR_DETAILS. */
 private static final String[] DEFAULT_AUTHOR_SOLR_DETAILS = new String[] { "author", "http://localhost:8983/solr", ADMIN, ADMIN };
 
 /** The Constant DEFAULT_PUBLISH_SOLR_DETAILS. */
 private static final String[] DEFAULT_PUBLISH_SOLR_DETAILS = new String[] { "publish", "http://localhost:8983/solr", ADMIN, ADMIN };
 
 /** The Constant DEFAULT_COLLECTIONS. */
 private static final String[] DEFAULT_COLLECTIONS = new String[] { "dove:en_gb:collection1", "knor:en_gb:collection1" };
 
 /**
  * Activate.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  
  allowedRequestHandlers = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.ALLOWED_REQUEST_HANDLERS));
  solrAuthorServerArray = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.AUTHOR_SOLR_DETAILS));
  solrPublishServerArray = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.PUBLISH_SOLR_DETAILS));
  solrCollectionsArray = PropertiesUtil.toStringArray(ctx.getProperties().get(SolrAEMConfigurationServiceAdminConstants.COLLECTIONS));
 }
 
 /**
  * Checks if is request handler allowed.
  *
  * @param requestHandler
  *         the request handler
  * @return true, if is request handler allowed
  */
 public boolean isRequestHandlerAllowed(String requestHandler) {
  
  if (StringUtils.isBlank(requestHandler)) {
   return false;
  }
  
  for (String whitelist : allowedRequestHandlers) {
   if (whitelist.equalsIgnoreCase(requestHandler)) {
    return true;
   }
  }
  
  return false;
 }
 
 /**
  * Activate.
  *
  * @param config
  *         the config
  */
 @Activate
 protected void activate(final Map<String, Object> config) {
  resetService(config);
 }
 
 /**
  * Modified.
  *
  * @param config
  *         the config
  */
 @Modified
 protected void modified(final Map<String, Object> config) {
  resetService(config);
 }
 
 /**
  * Reset service.
  *
  * @param config
  *         the config
  */
 private synchronized void resetService(final Map<String, Object> config) {
  LOG.info("Resetting Solr configuration service using configuration: " + config);
  
  allowedRequestHandlers = config.containsKey(SolrAEMConfigurationServiceAdminConstants.ALLOWED_REQUEST_HANDLERS)
    ? (String[]) config.get(SolrAEMConfigurationServiceAdminConstants.ALLOWED_REQUEST_HANDLERS) : DEFAULT_ALLOWED_REQUEST_HANDLERS;
  
  solrAuthorServerArray = config.containsKey(SolrAEMConfigurationServiceAdminConstants.AUTHOR_SOLR_DETAILS)
    ? (String[]) config.get(SolrAEMConfigurationServiceAdminConstants.AUTHOR_SOLR_DETAILS) : DEFAULT_AUTHOR_SOLR_DETAILS;
  
  solrPublishServerArray = config.containsKey(SolrAEMConfigurationServiceAdminConstants.PUBLISH_SOLR_DETAILS)
    ? (String[]) config.get(SolrAEMConfigurationServiceAdminConstants.PUBLISH_SOLR_DETAILS) : DEFAULT_PUBLISH_SOLR_DETAILS;
  
  solrCollectionsArray = config.containsKey(SolrAEMConfigurationServiceAdminConstants.COLLECTIONS)
    ? (String[]) config.get(SolrAEMConfigurationServiceAdminConstants.COLLECTIONS) : DEFAULT_COLLECTIONS;
 }
 
 /**
  * @return the solrAuthorServerArray
  */
 public String[] getSolrAuthorServerArray() {
  return solrAuthorServerArray;
 }
 
 /**
  * @param solrAuthorServerArray
  *         the solrAuthorServerArray to set
  */
 public void setSolrAuthorServerArray(String[] solrAuthorServerArray) {
  this.solrAuthorServerArray = solrAuthorServerArray;
 }
 
 /**
  * @return the solrPublishServerArray
  */
 public String[] getSolrPublishServerArray() {
  return solrPublishServerArray;
 }
 
 /**
  * @param solrPublishServerArray
  *         the solrPublishServerArray to set
  */
 public void setSolrPublishServerArray(String[] solrPublishServerArray) {
  this.solrPublishServerArray = solrPublishServerArray;
 }
 
 /**
  * @return the solrCollectionsArray
  */
 public String[] getSolrCollectionsArray() {
  return solrCollectionsArray;
 }
 
 /**
  * @param solrCollectionsArray
  *         the solrCollectionsArray to set
  */
 public void setSolrCollectionsArray(String[] solrCollectionsArray) {
  this.solrCollectionsArray = solrCollectionsArray;
 }
 
}
