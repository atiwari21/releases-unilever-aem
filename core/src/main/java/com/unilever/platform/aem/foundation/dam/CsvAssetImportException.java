/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

/**
 * The Class CsvAssetImportException.
 */
public class CsvAssetImportException extends Exception {
 
 /**
  * Instantiates a new csv asset import exception.
  */
 public CsvAssetImportException() {
  super();
 }
 
 /**
  * Instantiates a new csv asset import exception.
  *
  * @param message
  *         the message
  */
 public CsvAssetImportException(String message) {
  super(message);
 }
 
 /**
  * Instantiates a new csv asset import exception.
  *
  * @param e
  *         the e
  */
 public CsvAssetImportException(Exception e) {
  super(e);
 }
 
 /**
  * Instantiates a new csv asset import exception.
  *
  * @param message
  *         the message
  * @param e
  *         the e
  */
 public CsvAssetImportException(String message, Exception e) {
  super(message, e);
 }
 
}
