/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.impl.filters;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class XSSFilter.
 */
@Component(label = "Unilever XSS Filter", metatype = true, immediate = true, description = "Unilever XSS Filter")
@Service(value = Filter.class)
@Properties({
  @Property(name = EngineConstants.SLING_FILTER_SCOPE, value = { EngineConstants.FILTER_SCOPE_REQUEST, EngineConstants.FILTER_SCOPE_INCLUDE }),
  @Property(name = EngineConstants.SLING_FILTER_PATTERN, value = { "^/content/(?!.*jcr:content|dam).*$" }),
  @Property(name = Constants.SERVICE_VENDOR, value = "unilever"), @Property(name = "filter.order", intValue = -607) })
public class XSSFilter implements Filter {
 
 /** The Constant IS Filter Enabled. */
 @Property(label = "Enabled", boolValue = true, description = "Check to enable filter")
 private static final String IS_FILTER_ENABLED = "filter.enabled";
 
 /** The Constant log. */
 private static final Logger LOGGER = LoggerFactory.getLogger(XSSFilter.class);
 
 /** The brand to Error pages Map. */
 Map<String, String> brandToErrorPageMap = new HashMap<String, String>();
 
 /** The is enabled. */
 private boolean isEnabled = true;
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
  */
 public void init(FilterConfig arg0) throws ServletException {
  LOGGER.debug("Intializing the XSS filter");
 }
 
 /**
  * Activate.
  *
  * @param context
  *         the context
  */
 @Activate
 @Modified
 @SuppressWarnings("unchecked")
 protected void activate(ComponentContext context) {
  final Map<String, String> properties = (Map<String, String>) context.getProperties();
  isEnabled = PropertiesUtil.toBoolean(properties.get(IS_FILTER_ENABLED), true);
  if (isEnabled) {
   LOGGER.info("XSS Filter is enabled.");
  } else {
   LOGGER.info("XSSFilter is not enabled.");
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.Filter#destroy()
  */
 public void destroy() {
  LOGGER.debug("Destroying the XSS filter");
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
  */
 @Override
 public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
   throws IOException, ServletException {
  SlingHttpServletRequest req = (SlingHttpServletRequest) request;
  SlingHttpServletResponse res = (SlingHttpServletResponse) response;
  String path = req.getRequestPathInfo().getResourcePath();
  ResourceResolver resourceResolver = req.getResourceResolver();
  
  Resource currentResource = resourceResolver.getResource(path);
  if (isEnabled && !path.endsWith(".html") && !path.contains("/jcr:content") && !path.contains("/projects") && currentResource != null
    && !req.getParameterMap().isEmpty()) {
   String requestURI = req.getRequestURI();
   XSSRequestWrapper requestWrapper = new XSSRequestWrapper(req);
   
   if (!requestURI.equalsIgnoreCase(requestWrapper.getRequestURI())) {
    res.sendError(HttpServletResponse.SC_FORBIDDEN);
   } else if ("GET".equalsIgnoreCase(req.getMethod())) {
    @SuppressWarnings("rawtypes")
    Enumeration en = req.getParameterNames();
    
    while (en.hasMoreElements()) {
     Object objOri = en.nextElement();
     String param = (String) objOri;
     String origValue = req.getParameter(param);
     String cleanValue = requestWrapper.getParameter(param);
     if (!origValue.equalsIgnoreCase(cleanValue) || !param.equalsIgnoreCase(requestWrapper.stripXSS(param))) {
      res.sendError(HttpServletResponse.SC_FORBIDDEN);
      break;
     }
     
    }
    
    filterChain.doFilter(requestWrapper, res);
   }
   
  } else {
   filterChain.doFilter(request, response);
  }
 }
 
}
