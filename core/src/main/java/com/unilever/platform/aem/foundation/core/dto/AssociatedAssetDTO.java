/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

/**
 * The Class AssociatedAssetDTO.
 */
public class AssociatedAssetDTO {
 
 /** The tweet copy. */
 private String tweetCopy;
 
 /** The author name. */
 private String authorName;
 
 /** The approved date. */
 private String approvedDate;
 
 /** The social channel name. */
 private String socialChannelName;
 
 /** The post url. */
 private String postUrl;
 
 /**
  * Gets the tweet copy.
  *
  * @return the tweet copy
  */
 public String getTweetCopy() {
  return tweetCopy;
 }
 
 /**
  * Sets the tweet copy.
  *
  * @param tweetCopy
  *         the new tweet copy
  */
 public void setTweetCopy(String tweetCopy) {
  this.tweetCopy = tweetCopy;
 }
 
 /**
  * Gets the author name.
  *
  * @return the author name
  */
 public String getAuthorName() {
  return authorName;
 }
 
 /**
  * Sets the author name.
  *
  * @param authorName
  *         the new author name
  */
 public void setAuthorName(String authorName) {
  this.authorName = authorName;
 }
 
 /**
  * Gets the approved date.
  *
  * @return the approved date
  */
 public String getApprovedDate() {
  return approvedDate;
 }
 
 /**
  * Sets the approved date.
  *
  * @param approvedDate
  *         the new approved date
  */
 public void setApprovedDate(String approvedDate) {
  this.approvedDate = approvedDate;
 }
 
 /**
  * Gets the social channel name.
  *
  * @return the social channel name
  */
 public String getSocialChannelName() {
  return socialChannelName;
 }
 
 /**
  * Sets the social channel name.
  *
  * @param socialChannelName
  *         the new social channel name
  */
 public void setSocialChannelName(String socialChannelName) {
  this.socialChannelName = socialChannelName;
 }
 
 /**
  * Gets the post url.
  *
  * @return the post url
  */
 public String getPostUrl() {
  return postUrl;
 }
 
 /**
  * Sets the post url.
  *
  * @param postUrl
  *         the new post url
  */
 public void setPostUrl(String postUrl) {
  this.postUrl = postUrl;
 }
}
