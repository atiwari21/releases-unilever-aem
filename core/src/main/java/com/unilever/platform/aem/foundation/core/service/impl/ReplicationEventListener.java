/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.event.jobs.JobProcessor;
import org.apache.sling.event.jobs.JobUtil;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageEvent;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.PageModification;
import com.unilever.platform.aem.foundation.configuration.ConfigurationHelper;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.dto.IndexingContentFactory;
import com.unilever.platform.aem.foundation.core.service.AEMConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SolrConfigurationService;
import com.unilever.platform.aem.foundation.core.service.SolrIndexCreater;

/**
 * This is an replication event listner to index the content pages on solr during activation event and deactivate the pages on deactivate event.
 * 
 * @see ReplicationEventEvent
 */

@SuppressWarnings("deprecation")
@Component(name = "com.unilever.platform.aem.foundation.core.service.impl.ReplicationEventListener", label = "Unilever Solr Indexing Service", description = "A service for Indexing Solr", immediate = true, metatype = true)
@Service(value = { EventHandler.class, SolrIndexCreater.class })
@Property(name = org.osgi.service.event.EventConstants.EVENT_TOPIC, value = { ReplicationAction.EVENT_TOPIC,
  PageEvent.EVENT_TOPIC }, label = "Events", description = "Event to be tracked")
public class ReplicationEventListener implements EventHandler, JobProcessor, SolrIndexCreater {
 
 /** The Constant NOINDEX_SOLR. */
 private static final String NOINDEX_SOLR = "noindexSolr";
 
 /** The Constant log. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ReplicationEventListener.class);
 
 /** The Constant TWO. */
 private static final int NUMERIC_TWO = 2;
 
 /** The Constant THREE. */
 private static final int NUMERIC_THREE = 3;
 
 /** The Constant FOUR. */
 private static final int NUMERIC_FOUR = 4;
 
 /** The Constant HUNDRED. */
 private static final int HUNDRED = 100;
 
 /** The Constant SEARCH_MAPPING. */
 private static final String SEARCH_MAPPING = "searchUrlMappings";
 /** The resolver factory. */
 @Reference
 private ResourceResolverFactory resolverFactory;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /** The aem config. */
 @Reference
 private AEMConfigurationService aemConfig;
 
 /** The solr config. */
 @Reference
 private SolrConfigurationService solrConfig;
 
 /** The admin. */
 @Reference
 EventAdmin admin;
 
 /** The notification on tag publish service. */
 @Reference
 private NotificationOnTagPublishImpl notificationOnTagPublishService;
 
 /** The page index count. */
 private int pageIndexCount = 0;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.osgi.service.event.EventHandler#handleEvent(org.osgi.service.event. Event)
  */
 @SuppressWarnings("deprecation")
 @Override
 public void handleEvent(Event event) {
  JobUtil.processJob(event, this);
 }
 
 /**
  * Gets the collection.
  * 
  * @param collectionsBrandsArray
  *         the collections brands array
  * @param path
  *         the path
  * @return the collection
  */
 private String getCollection(String[] collectionsBrandsArray, String path) {
  String collection = "";
  String defaultCollection = "collection1";
  int brandindex = StringUtils.ordinalIndexOf(path, "/", NUMERIC_TWO);
  int beginindex = StringUtils.ordinalIndexOf(path, "/", NUMERIC_THREE);
  int endIndex = StringUtils.ordinalIndexOf(path, "/", NUMERIC_FOUR);
  String locale = "";
  if (endIndex > 0) {
   locale = path.substring(beginindex + 1, endIndex);
  } else {
   locale = path.substring(beginindex + 1, path.length());
  }
  LOGGER.debug("locale is-- " + locale);
  String brand = path.substring(brandindex + 1, beginindex);
  for (String collectionName : collectionsBrandsArray) {
   StringTokenizer str1 = new StringTokenizer(collectionName, ":");
   while (str1.hasMoreTokens()) {
    String collectionBrand = str1.nextToken();
    String collectionLocale = str1.nextToken();
    collection = str1.nextToken();
    LOGGER.debug("collection is {} ", collection);
    if (locale.equalsIgnoreCase(collectionLocale) && brand.equalsIgnoreCase(collectionBrand)) {
     return collection;
    }
   }
  }
  return defaultCollection;
 }
 
 /**
  * Determines whether the page modification is a candidate for indexing.
  * 
  * @param status
  *         the status
  * @return <code>true</code> if the page modification should be indexed, and <code>false</code> if the page should be excluded from indexing.
  */
 protected boolean pageIsIndexable(boolean status) {
  
  if (!status) {
   LOGGER.debug("DefaultSolrIndexListenerService not enabled. Ignoring indexing event");
   return false;
  }
  
  return true;
 }
 
 /**
  * Page is not indexable.
  * 
  * @param status
  *         the status
  * @return true, if successful
  */
 protected boolean pageIsNotIndexable(boolean status) {
  return !pageIsIndexable(status);
 }
 
 /**
  * Checks if page is configured for no-index in config.
  *
  * @param modificationPath
  *         the modification path
  * @return true, if page is configured for no-index in config
  */
 protected boolean isNonIndexablePageInConfig(String modificationPath) {
  String[] doNotIndexPages = aemConfig.getDoNotIndexPages();
  for (String s : doNotIndexPages) {
   if (s.equals(modificationPath)) {
    return true;
   }
  }
  return false;
 }
 
 /**
  * Checks if is non indexable content type.
  *
  * @param vm
  *         the vm
  * @return true, if is non indexable content type
  */
 protected boolean isNonIndexableContentType(ValueMap vm) {
  String contentType = MapUtils.getString(vm, "contentType", StringUtils.EMPTY);
  String[] doNotIndexContentTypes = aemConfig.getRestrcitedContentTypesArray();
  for (String s : doNotIndexContentTypes) {
   if (StringUtils.equalsIgnoreCase(contentType, s)) {
    return true;
   }
  }
  return false;
 }
 
 /**
  * Checks if is having No-index flag for Solr search.
  * 
  * @param modificationPath
  *         the modification path
  * @return true, if is page is having no-index flag
  */
 protected boolean isNonIndexablePage(String modificationPath) {
  boolean isNonIndexable = true;
  ResourceResolver resourceResolver = null;
  try {
   resourceResolver = setServiceType();
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page currentPage = pageManager.getPage(modificationPath);
   ValueMap vm = null;
   if (currentPage != null) {
    vm = currentPage.getProperties();
   }
   if (vm != null) {
    if (MapUtils.getBoolean(vm, NOINDEX_SOLR, false)) {
     LOGGER.info("Non indexable page found as No-index flag is set in page property:" + modificationPath);
     isNonIndexable = true;
    } else if (isNonIndexablePageInConfig(modificationPath)) {
     LOGGER.info("Non indexable page found as page is configured for No-index in config :" + modificationPath);
     isNonIndexable = true;
    } else if (isNonIndexableContentType(vm)) {
     LOGGER.info("Non indexable page found as content type is configured for No-index in config :" + modificationPath);
     isNonIndexable = true;
    } else {
     LOGGER.debug("Indexable page found :" + modificationPath);
     isNonIndexable = false;
    }
    
   } else {
    LOGGER.info("Unable to get valuemap of resource while indexing a page in solr..............");
   }
   
  } catch (LoginException e) {
   LOGGER.error("Exception in indexing a page in solr.........", e);
  } finally {
   if (resourceResolver != null) {
    resourceResolver.close();
   }
  }
  
  return isNonIndexable;
 }
 
 /**
  * Determines whether the page modification is in the allowed list of observed paths.
  * 
  * @param pagePath
  *         Path of the modified page.
  * @return <code>true</code> if the page modification is in the observed path, and <code>false</code> otherwise.
  */
 protected boolean pageIsInObservedPath(String pagePath) {
  
  if (null == pagePath || null == aemConfig.getIndexingPath()) {
   return false;
  }
  
  for (String observedPath : aemConfig.getIndexingPath()) {
   if (pagePath.startsWith(observedPath)) {
    LOGGER.debug("Page '{}' is in observed path '{}'", pagePath, observedPath);
    return true;
   }
  }
  
  return false;
 }
 
 /**
  * Page is not in observed path.
  * 
  * @param pagePath
  *         the page path
  * @return true, if successful
  */
 protected boolean pageIsNotInObservedPath(String pagePath) {
  return !pageIsInObservedPath(pagePath);
 }
 
 /**
  * Adds the or update page.
  * 
  * @param modification
  *         the modification
  * @param hosturl
  *         the hosturl
  * @param solrServer
  *         the solr server
  * @param collection
  *         the collection
  */
 protected void addOrUpdatePage(PageModification modification, String[] hosturl, String[] solrServer, String collection) {
  final String modificationPath = (modification.getType() == PageModification.ModificationType.MOVED) ? modification.getDestination()
    : modification.getPath();
  ResourceResolver resourceResolver = null;
  try {
   resourceResolver = setServiceType();
   Resource resource = null;
   resource = resourceResolver.getResource(modificationPath);
   if (null != resource) {
    
    getSolrDoc(resource, hosturl, solrServer, collection);
   }
  } catch (LoginException e) {
   LOGGER.error("Exception in indexing a page in solr............", e);
  } finally {
   if (resourceResolver != null) {
    resourceResolver.close();
   }
  }
 }
 
 /**
  * Adds the or update page.
  * 
  * @param path
  *         the path
  * @param hosturl
  *         the hosturl
  * @param solrServer
  *         the solr server
  * @param collection
  *         the collection
  */
 protected void addOrUpdatePage(String path, String[] hosturl, String[] solrServer, String collection) {
  ResourceResolver resourceResolver = null;
  try {
   resourceResolver = setServiceType();
   Resource resource = null;
   resource = resourceResolver.getResource(path);
   if (null != resource) {
    
    getSolrDoc(resource, hosturl, solrServer, collection);
   }
  } catch (LoginException e) {
   LOGGER.error("Exception in indexing a page in solr..............", e);
  } finally {
   if (resourceResolver != null) {
    resourceResolver.close();
   }
  }
  LOGGER.debug("Exiting  replicationevent listener method addOrUpdatePage");
  
 }
 
 /**
  * Sets the service type.
  * 
  * @return the resource resolver
  * @throws LoginException
  *          the login exception
  */
 private ResourceResolver setServiceType() throws LoginException {
  return ConfigurationHelper.getResourceResolver(resolverFactory, "ieaAdminService");
 }
 
 /**
  * Gets the solr doc.
  * 
  * @param resource
  *         the resource
  * @param hostUrl
  *         the host url
  * @param solrServer
  *         the solr server
  * @param collection
  *         the collection
  * @return the solr doc
  */
 public SolrInputDocument getSolrDoc(Resource resource, String[] hostUrl, String[] solrServer, String collection) {
  SolrInputDocument doc = new SolrInputDocument();
  try {
   SolrClient server = new HttpSolrClient(solrServer[1] + "/" + collection);
   Map<String, String> searchurlMappings = new HashMap<String, String>();
   String pagePath = resource.getPath();
   PageManager pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
   Page currentPagePath = pageManager.getContainingPage(pagePath);
   searchurlMappings = configurationService.getCategoryConfiguration(currentPagePath, SEARCH_MAPPING);
   doc = IndexingContentFactory.getSolrDoc(resource, hostUrl, aemConfig.getBreadcrumbLevel(), searchurlMappings);
   LOGGER.debug("Solr server in use is" + solrServer[1]);
   LOGGER.debug("Current Time Before indexing is" + System.currentTimeMillis());
   int commitWithin = aemConfig.getCommitWithinMS();
   UpdateResponse addResponse = null;
   UpdateResponse response = null;
   int commitStatus = -1;
   int status = -1;
   if (commitWithin == 0) {
    addResponse = server.add(doc);
    status = addResponse.getStatus();
    if (status != 0) {
     LOGGER.debug(String.format("update of doc is %s failed with status %s", doc.getField("id"), status));
    }
    LOGGER.debug("Current Time after adding doc is " + System.currentTimeMillis());
    boolean softCommit = aemConfig.isSoftCommit();
    if (softCommit) {
     response = server.commit(true, true, softCommit);
    } else {
     response = server.commit();
    }
    commitStatus = response.getStatus();
    if (commitStatus != 0) {
     LOGGER.debug(String.format("update(commit) of doc is %s failed with status %s", doc.getField("id"), commitStatus));
    }
    LOGGER.debug("Document indexing is successful at Time " + System.currentTimeMillis());
    LOGGER.debug("response is", response);
   } else {
    addResponse = server.add(doc, commitWithin);
    status = addResponse.getStatus();
    if (status != 0) {
     LOGGER.debug(String.format("update of doc is %s failed with status %s", doc.getField("id"), status));
    }
    LOGGER.debug("Current Time after adding doc is " + System.currentTimeMillis());
   }
   server.close();
  } catch (SolrServerException e) {
   LOGGER.error("Exception in accessing  solr server to index doc..............", e);
  } catch (IOException e) {
   LOGGER.error("Exception in creating solr json doc for indexing..............", e);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Exception in getting search configuration for indexing..............", e);
  }
  return doc;
 }
 
 /**
  * Gets the solr doc.
  * 
  * @param path
  *         the path
  * @param solrServer
  *         the solr server
  * @param collection
  *         the collection
  * @return the solr doc
  */
 
 public void deleteTreeAndCommit(String path, String[] solrServer, String collection) {
  try {
   SolrClient client = new HttpSolrClient(solrServer[1] + "/" + collection);
   LOGGER.debug("page to be deleted is--------------" + path);
   if (client.getById(path) != null) {
    client.deleteById(path);
    client.commit();
   } else {
    LOGGER.debug("No Document by id {} exists in solr", path);
   }
   client.close();
   LOGGER.debug("page has been deleted successfully");
  } catch (SolrServerException e) {
   LOGGER.error("Solr server Exception in deleting a page from solr..............", e);
  } catch (IOException e) {
   LOGGER.error("IOException in deleting a page from solr..............", e);
  }
  LOGGER.debug("Exiting delete tree---------------");
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.event.jobs.JobProcessor#process(org.osgi.service.event.Event)
  */
 @Override
 public boolean process(Event event) {
  Boolean status = aemConfig.isIndexingStatusEnabled();
  if (pageIsNotIndexable(status)) {
   return false;
  }
  PageEvent pageEvent = PageEvent.fromEvent(event);
  LOGGER.debug("Inside replication event listener handleEvent method");
  String[] prodAEMHostUrl = aemConfig.getPublishAEMServerArray();
  String[] solrPublishDetails = solrConfig.getSolrPublishServerArray();
  String[] collectionsBrandsArray = solrConfig.getSolrCollectionsArray();
  String collection = "";
  
  try {
   String workflowEvent = notificationOnTagPublishService.getWorkflowEvent(resolverFactory.getAdministrativeResourceResolver(null), event);
   if (StringUtils.isNotBlank(workflowEvent)) {
    notificationOnTagPublishService.getNoticationOnTagPublish(resolverFactory.getAdministrativeResourceResolver(null), event, workflowEvent);
   }
  } catch (LoginException e) {
   LOGGER.debug("Resource Resolver not administered", e);
  }
  
  if (pageEvent != null) {
   Iterator<PageModification> modifications = pageEvent.getModifications();
   
   while (modifications.hasNext()) {
    
    PageModification modification = modifications.next();
    String modificationPath = modification.getPath();
    
    if (pageIsNotInObservedPath(modificationPath)) {
     return false;
    }
    collection = getCollection(collectionsBrandsArray, modificationPath);
    
    PageModification.ModificationType type = modification.getType();
    if (modification.getVersionId() != null) {
     if (isNonIndexablePage(modificationPath)) {
      return false;
     }
     LOGGER.debug("In side Page activate event {}", modificationPath, type);
     ReplicationAction action = ReplicationAction.fromEvent(event);
     if (action == null) {
      addOrUpdatePage(modification, prodAEMHostUrl, solrPublishDetails, collection);
      LOGGER.debug("Page activated with Page action {} ", type);
     }
     
    }
    
   }
  } else if (null == pageEvent) {
   ReplicationAction action = ReplicationAction.fromEvent(event);
   String path = action.getPath();
   String actionName = action.getType().getName();
   if (isNonIndexablePage(path) && !StringUtils.equalsIgnoreCase(actionName, "DELETE")) {
    return false;
   }
   collection = getCollection(collectionsBrandsArray, path);
   if (action != null && ("Deactivate".equalsIgnoreCase(actionName))) {
    deleteTreeAndCommit(action.getPath(), solrPublishDetails, collection);
    LOGGER.debug("Page Deactivated with action {} ", actionName);
    
   }
   if (action != null && (StringUtils.equalsIgnoreCase(actionName, "delete"))) {
    String[] paths = (String[]) event.getProperty("paths");
    for (String perPath : paths) {
     deleteTreeAndCommit(perPath, solrPublishDetails, collection);
     LOGGER.debug("Page deleted with action {} ", actionName);
    }
   }
   if (action != null && "activate".equalsIgnoreCase(action.getType().getName())) {
    addOrUpdatePage(action.getPath(), prodAEMHostUrl, solrPublishDetails, collection);
    LOGGER.debug("Page activated with replication action {} ", action.getType().getName());
    
   }
  }
  return true;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.SolrIndexCreater# createIndex(java.lang.String)
  */
 @Override
 public String createIndex(String treepath) {
  StringBuilder report = new StringBuilder();
  String meassge = "No index created";
  try {
   
   Boolean status = aemConfig.isIndexingStatusEnabled();
   if (pageIsNotIndexable(status)) {
    meassge = INDEXING_IS_NOT_ENABLE_FOR_SERVER;
    return meassge;
   }
   if (pageIsNotInObservedPath(treepath)) {
    meassge = SITE_IS_NOT_ENABLED_FOR_INDEXING;
    return meassge;
   }
   
   LOGGER.debug("Inside solor index creater");
   String[] prodAEMHostUrl = aemConfig.getPublishAEMServerArray();
   String[] solrPublishDetails = solrConfig.getSolrPublishServerArray();
   String[] collectionsBrandsArray = solrConfig.getSolrCollectionsArray();
   String collection = "";
   collection = getCollection(collectionsBrandsArray, treepath);
   // check solr server connection first
   String pingResponse = solrPing(solrPublishDetails[1] + "/" + collection);
   boolean isSolrUp = isSolrServerUp(pingResponse);
   if (isSolrUp) {
    this.setPageIndexCount(0);
    report = addOrUpdateTree(treepath, prodAEMHostUrl, solrPublishDetails, collection);
   } else {
    meassge = UNABLE_TO_CONNECT_SOLR_SERVER + solrPublishDetails[1] + "/" + collection + ": Getting response :" + pingResponse + H2_SPAN;
    return meassge;
   }
   
  } catch (Exception e) {
   meassge = ERROR_WHILE_CREATING_SOLR_INDEX + e;
   return meassge;
  }
  return report.toString();
 }
 
 /**
  * Adds the or update tree.
  *
  * @param path
  *         the path
  * @param hosturl
  *         the hosturl
  * @param solrServer
  *         the solr server
  * @param collection
  *         the collection
  * @return the string builder
  */
 protected StringBuilder addOrUpdateTree(String path, String[] hosturl, String[] solrServer, String collection) {
  ResourceResolver resourceResolver = null;
  StringBuilder report = new StringBuilder();
  try {
   resourceResolver = setServiceType();
   Resource resource = null;
   resource = resourceResolver.getResource(path);
   if (null != resource) {
    StringBuilder runReport = new StringBuilder();
    ReplicationStatus rs = resource.adaptTo(ReplicationStatus.class);
    Boolean isDeactivated = rs.isDeactivated();
    int indexCount = 0;
    if (isDeactivated) {
     LOGGER.debug("Page is not actaiveted so ignored for indexing" + resource.getPath());
     runReport.append(resource.getPath() + INDEX_NOT_CREATED_AS_PAGE_IS_DEACTIVATED_SPAN).append(BR);
    } else if (isNonIndexablePage(resource.getPath())) {
     LOGGER.debug("Page is not indexable ,property is enabled" + resource.getPath());
     runReport.append(resource.getPath() + INDEX_NOT_CREATED_AS_PAGE_IS_NOT_INDEXABLE_SPAN).append(BR);
    } else {
     // create index for parent
     getSolrDoc(resource, hosturl, solrServer, collection);
     runReport.append(resource.getPath() + INDEX_IS_SENT_SPAN).append(BR);
     indexCount++;
     int tempCount = this.getPageIndexCount();
     this.setPageIndexCount(tempCount + indexCount);
    }
    
    // create indexes for child of child pages
    SolrClient server = new HttpSolrClient(solrServer[1] + "/" + collection);
    report = getSolrDocTree(resource, hosturl, server, collection, runReport);
    server.commit();
    server.close();
    
   } else {
    report.append(INCORRECT_SITES_PATH);
   }
  } catch (LoginException | IOException | SolrServerException e) {
   LOGGER.error("Exception in indexing a page tree in solr..............", e);
  } finally {
   if (resourceResolver != null) {
    resourceResolver.close();
   }
  }
  LOGGER.debug("Exiting  replicationevent listener method addOrUpdateTree");
  return report;
 }
 
 /**
  * Gets the solr doc tree.
  *
  * @param resource
  *         the resource
  * @param hostUrl
  *         the host url
  * @param server
  *         the server
  * @param collection
  *         the collection
  * @param sb
  *         the sb
  * @return the solr doc tree
  */
 public StringBuilder getSolrDocTree(Resource resource, String[] hostUrl, SolrClient server, String collection, StringBuilder sb) {
  SolrInputDocument doc = new SolrInputDocument();
  int childCount = 0;
  try {
   Map<String, String> searchurlMappings = new HashMap<String, String>();
   String pagePath = resource.getPath();
   PageManager pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
   Page currentPagePath = pageManager.getContainingPage(pagePath);
   searchurlMappings = configurationService.getCategoryConfiguration(currentPagePath, SEARCH_MAPPING);
   Iterator<Resource> iter = resource.listChildren();
   LOGGER.debug("Current Time Before tree indexing is" + System.currentTimeMillis());
   
   while (iter.hasNext()) {
    Resource tempResource = iter.next();
    ReplicationStatus rs = tempResource.adaptTo(ReplicationStatus.class);
    Boolean isDeactivated = rs.isDeactivated();
    
    if (tempResource != null) {
     if (tempResource.getPath().contains(JcrConstants.JCR_CONTENT)) {
      continue;
     } else if (isDeactivated) {
      LOGGER.debug("Page is not actaiveted so ignored for indexing" + tempResource.getPath());
      sb.append(tempResource.getPath() + PAGE_IS_DEACTIVATED_SPAN).append(BR);
     } else if (isNonIndexablePage(tempResource.getPath())) {
      LOGGER.debug("Page is not indexable ,property is enabled" + tempResource.getPath());
      sb.append(tempResource.getPath() + PAGE_IS_NOT_INDEXABLE_SPAN).append(BR);
     } else {
      try {
       LOGGER.debug("Page is sent for indexing" + tempResource.getPath());
       doc = IndexingContentFactory.getSolrDoc(tempResource, hostUrl, aemConfig.getBreadcrumbLevel(), searchurlMappings);
       server.add(doc);
       childCount++;
       updateIndexCount();
       if (childCount % HUNDRED == 0) {
        // periodically flush
        server.commit();
       }
       sb.append(tempResource.getPath() + INDEX_IS_SENT_SPAN).append(BR);
      } catch (Exception e) {
       LOGGER.debug("There is some error during tree activation", e);
      }
      
     }
    }
    // call recursively for tree child
    getSolrDocTree(tempResource, hostUrl, server, collection, sb);
   }
   
  } catch (Exception e) {
   LOGGER.error("Exception in getting search configuration for indexing..............", e);
  }
  return new StringBuilder(SPAN_B_NUMBER_OF_PAGES_ARE_SENT_TO_SOLR + this.getPageIndexCount() + B_SPAN_BR).append(sb);
 }
 
 /**
  * Update index count.
  */
 private void updateIndexCount() {
  int tempCount = this.getPageIndexCount();
  tempCount = tempCount + 1;
  this.setPageIndexCount(tempCount);
 }
 
 /**
  * Gets the page index count.
  *
  * @return pageIndexCount
  */
 public int getPageIndexCount() {
  return pageIndexCount;
 }
 
 /**
  * Sets the page index count.
  *
  * @param pageIndexCount
  *         the new page index count
  */
 public void setPageIndexCount(int pageIndexCount) {
  this.pageIndexCount = pageIndexCount;
 }
 
 /**
  * Solr ping.
  *
  * @param collectionPath
  *         the collection path
  * @return response String from Solr
  */
 private String solrPing(String collectionPath) {
  
  SolrClient server = new HttpSolrClient(collectionPath);
  SolrPingResponse response = null;
  String messgage = null;
  try {
   response = server.ping();
   messgage = response.getResponse().get("status").toString();
   server.close();
  } catch (SolrServerException | IOException e) {
   messgage = e.getMessage();
   LOGGER.warn("There is some issue while connecting solr server", collectionPath);
   LOGGER.warn("There is some issue while connecting solr server", e);
  }
  return messgage;
 }
 
 /**
  * Checks if is solr server up.
  *
  * @param pingResponse
  *         the ping response
  * @return true, if is solr server up
  */
 private boolean isSolrServerUp(String pingResponse) {
  boolean isSolrUp = false;
  isSolrUp = pingResponse != null && pingResponse.equalsIgnoreCase(CONNECTION_IS_OK);
  return isSolrUp;
 }
 
}
