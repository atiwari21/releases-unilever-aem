/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

/**
 * The Enum ContentType.
 */
public enum ContentType {
 
 /** The product. */
 PRODUCT("Product"),
 /** The article. */
 ARTICLE("Article"),
 /** The homepage. */
 HOMEPAGE("Home page"),
 /** The landingpage. */
 LANDINGPAGE("Landing page"),
 /** The sublandingpage. */
 SUBLANDINGPAGE("Sublanding page"),
 /** The hygiene. */
 HYGIENE("Hygiene");
 
 /** The content type. */
 private final String contType;
 
 /**
  * Instantiates a new content type.
  *
  * @param type
  *         the type
  */
 ContentType(String type) {
  this.contType = type;
 }
 
 /**
  * Gets the value.
  *
  * @return the value
  */
 public String getValue() {
  return this.contType;
 }
}
