/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class VerifyRecaptcha.
 */
public class VerifyRecaptcha {
 
 /**
  * Identifier to hold value of logger.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(VerifyRecaptcha.class);
 
 /** The Constant secret. */
 public static final String SECRET = "6LdSEw0TAAAAABoiFnq9ilzrHR0GmWwdkrwvr53r";
 
 /** The Constant url. */
 public static final String URL = "https://www.google.com/recaptcha/api/siteverify";
 
 /** The Constant USER_AGENT. */
 private static final String USER_AGENT = "Mozilla/5.0";
 
 /**
  * Instantiates a new verify recaptcha.
  */
 private VerifyRecaptcha() {
  
 }
 
 /**
  * Verify.
  *
  * @param gRecaptchaResponse
  *         the g recaptcha response
  * @return true, if successful
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 public static boolean verify(String gRecaptchaResponse) throws IOException {
  if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
   return false;
  }
  
  try {
   URL obj = new URL(URL);
   HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
   
   // add reuqest header
   con.setRequestMethod("POST");
   con.setRequestProperty("User-Agent", USER_AGENT);
   con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
   
   String postParams = "secret=" + SECRET + "&response=" + gRecaptchaResponse;
   
   // Send post request
   con.setDoOutput(true);
   DataOutputStream wr = new DataOutputStream(con.getOutputStream());
   wr.writeBytes(postParams);
   wr.flush();
   wr.close();
   
   int responseCode = con.getResponseCode();
   LOGGER.debug("\nSending 'POST' request to URL : " + URL);
   LOGGER.debug("Post parameters : " + postParams);
   LOGGER.debug("Response Code : " + responseCode);
   
   BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
   String inputLine;
   StringBuilder response = new StringBuilder();
   
   while ((inputLine = in.readLine()) != null) {
    response.append(inputLine);
   }
   in.close();
   
   // print result
   LOGGER.debug(response.toString());
   
   JSONObject jsnobject = new JSONObject(response.toString());
   // parse JSON response and return 'success' value
   
   return jsnobject.getBoolean("success");
  } catch (Exception e) {
   LOGGER.debug("Captcha failed:", e);
   return false;
  }
 }
 
}
