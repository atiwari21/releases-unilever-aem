/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class ProductsType.
 */
public class ProductVariantList {
 
 /** The product. */
 protected List<ProductVariantType> productVariants;
 
 /**
  * Gets the product.
  *
  * @return the product
  */
 public List<ProductVariantType> getProductVariants() {
  if (productVariants == null) {
   productVariants = new ArrayList<ProductVariantType>();
  }
  return this.productVariants;
 }
 
 /**
  * @param productVariants
  *         the productVariants to set
  */
 public void setProductVariants(List<ProductVariantType> productVariants) {
  this.productVariants = productVariants;
 }
 
}
