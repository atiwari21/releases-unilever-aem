/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Caption;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Image;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class AnchorRule.
 */
public class CaptionRule extends ConfigurationSelectorRule {
 
 /** The Constant PROPERTY_ANCHOR_HREF. */
 public static final String PROPERTY_DEFAULT = "caption.default";
 
 /**
  * Instantiates a new anchor rule.
  */
 private CaptionRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor rule
  */
 public static CaptionRule create() {
  return new CaptionRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Caption caption = Caption.create();
  
  caption = setCaptionProperties(node, caption);
  
  Object title = getProperty(PROPERTY_DEFAULT, node);
  
  if (title != null) {
   caption.withTitle(title);
  } else {
   transformer.transform(caption, node);
  }
  
  return container;
 }
 
 /**
  * Sets the caption properties.
  *
  * @param node
  *         the node
  * @param caption
  *         the caption
  * @return the caption
  */
 private Caption setCaptionProperties(Node node, Caption caption) {
  if (this.getProperty(Caption.POSITION_BELOW, node) != null) {
   caption.withPosition(Caption.POSITION_BELOW);
  }
  if (this.getProperty(Caption.POSITION_CENTER, node) != null) {
   caption.withPosition(Caption.POSITION_CENTER);
  }
  if (this.getProperty(Caption.POSITION_ABOVE, node) != null) {
   caption.withPosition(Caption.POSITION_ABOVE);
  }
  if (this.getProperty(Caption.ALIGN_LEFT, node) != null) {
   caption.withTextAlignment(Caption.ALIGN_LEFT);
  }
  if (this.getProperty(Caption.ALIGN_CENTER, node) != null) {
   caption.withTextAlignment(Caption.ALIGN_CENTER);
  }
  if (this.getProperty(Caption.ALIGN_RIGHT, node) != null) {
   caption.withTextAlignment(Caption.ALIGN_RIGHT);
  }
  if (this.getProperty(Caption.SIZE_MEDIUM, node) != null) {
   caption.withFontSize(Caption.SIZE_MEDIUM);
  }
  if (this.getProperty(Caption.SIZE_LARGE, node) != null) {
   caption.withFontSize(Caption.SIZE_LARGE);
  }
  if (this.getProperty(Caption.SIZE_XLARGE, node) != null) {
   caption.withFontSize(Caption.SIZE_XLARGE);
  }
  return caption;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Image.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the anchor rule
  */
 public static CaptionRule createFrom(JSONObject configuration) {
  CaptionRule captionRule = create();
  captionRule = (CaptionRule) captionRule.withSelector(configuration.getString("selector"));
  JSONObject captionConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(Caption.POSITION_BELOW);
  properties.add(Caption.POSITION_CENTER);
  properties.add(Caption.POSITION_ABOVE);
  properties.add(Caption.ALIGN_LEFT);
  properties.add(Caption.ALIGN_CENTER);
  properties.add(Caption.ALIGN_RIGHT);
  properties.add(Caption.SIZE_MEDIUM);
  properties.add(Caption.SIZE_LARGE);
  properties.add(Caption.SIZE_XLARGE);
  properties.add(PROPERTY_DEFAULT);
  
  captionRule.withProperties(properties, captionConfiguration);
  return captionRule;
 }
 
 /**
  * Load from.
  *
  * @param configuration
  *         the configuration
  */
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
