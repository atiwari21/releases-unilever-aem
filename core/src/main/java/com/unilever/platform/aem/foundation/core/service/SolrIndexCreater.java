/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

/**
 * @author Sku293
 *
 */
public interface SolrIndexCreater {
 
 String ERROR_WHILE_CREATING_SOLR_INDEX = "There is some error while creating solr index ";
 
 String H2_SPAN = "</h2></span>";
 
 String SITE_IS_NOT_ENABLED_FOR_INDEXING = "Site is not enabled for Indexing, Get this enabled first !!";
 
 String INDEXING_IS_NOT_ENABLE_FOR_SERVER = "Indexing is not enable for server";
 
 String CONNECTION_IS_OK = "ok";
 
 String UNABLE_TO_CONNECT_SOLR_SERVER = "<span style='color:red'><h2> Unable to connect solr server ";
 
 String INDEX_NOT_CREATED_AS_PAGE_IS_NOT_INDEXABLE_SPAN = " : <span style='color:red'>Index not created as Page is not indexable</span>";
 
 String INDEX_NOT_CREATED_AS_PAGE_IS_DEACTIVATED_SPAN = "  : <span style='color:red'>Index not created as Page is deactivated</span>";
 
 String BR = "</br>";
 
 String B_SPAN_BR = "</b></span></br>";
 
 String SPAN_B_NUMBER_OF_PAGES_ARE_SENT_TO_SOLR = "<span><b>Number of Pages are sent to Solr: ";
 
 String INDEX_IS_SENT_SPAN = "  : <span style='color:Green'>Index is sent</span>";
 
 String PAGE_IS_DEACTIVATED_SPAN = "  : <span style='color:Red'>Index not created as Page is deactivated</span>";
 
 String PAGE_IS_NOT_INDEXABLE_SPAN = " : <span style='color:Red'>Index not created as Page is not indexable</span>";
 String INCORRECT_SITES_PATH = "incorrect path please check and try again !!";
 
 /**
  *
  * @param path
  * @return
  */
 String createIndex(String path);
 
}
