/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class ProductCategory.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "primarySourceId", "secondarySourceId" })
public class ProductCategory {
 
 /** The primary source id. */
 @XmlElement(name = "PrimarySourceId", required = true)
 protected String primarySourceId;
 
 /** The secondary source id. */
 @XmlElement(name = "SecondarySourceId")
 private List<String> secondarySourceId;
 
 /**
  * Gets the primary source id.
  *
  * @return the primary source id
  */
 public String getPrimarySourceId() {
  return primarySourceId;
 }
 
 /**
  * Sets the primary source id.
  *
  * @param value
  *         the new primary source id
  */
 public void setPrimarySourceId(String value) {
  this.primarySourceId = value;
 }
 
 /**
  * Gets the secondary source id.
  *
  * @return the secondary source id
  */
 public List<String> getSecondarySourceId() {
  if (secondarySourceId == null) {
   secondarySourceId = new ArrayList<String>();
  }
  return this.secondarySourceId;
 }
 
 /**
  * Sets the secondary source id.
  *
  * @param secondarySourceId
  *         the new secondary source id
  */
 public void setSecondarySourceId(List<String> secondarySourceId) {
  this.secondarySourceId = secondarySourceId;
 }
 
}
