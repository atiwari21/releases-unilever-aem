/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * The Class GenericHelper.
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class MathHelper implements HandlebarsHelperService<Object> {
 
 /**
  * Logger Instance.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(MathHelper.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new MathHelper();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "math";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant THREE. */
 private static final int THREE = 3;
 
 /** The Constant FOUR. */
 private static final int FOUR = 4;
 
 /** The Constant FIVE. */
 private static final int FIVE = 5;
 
 /** The Constant SIX. */
 private static final int SIX = 6;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.github.jknack.handlebars.Helper#apply(java.lang.Object, com.github.jknack.handlebars.Options)
  */
 @SuppressWarnings("unchecked")
 @Override
 public CharSequence apply(Object path, Options options) throws IOException {
  
  CharSequence chars = null;
  String operator = null;
  double operand1 = 0.0;
  double operand2 = 0.0;
  
  LOGGER.debug("path = ", path.toString());
  try {
   if (options.params != null) {
    int size = options.params.length;
    if (size == TWO) {
     operator = (String) options.param(0);
     operand1 = Double.parseDouble(path.toString());
     operand2 = Double.parseDouble(options.param(1).toString());
    } else if (size == FOUR) {
     if (options.param(TWO) instanceof String) {
      operator = (String) options.param(TWO);
      operand1 = Double.parseDouble(path.toString() + "." + String.valueOf((int) options.param(1)));
      operand2 = Double.parseDouble(String.valueOf((int) options.param(THREE)));
      
     } else if (options.param(0) instanceof String) {
      operator = (String) options.param(0);
      operand1 = Double.parseDouble(path.toString());
      operand2 = Double.parseDouble(String.valueOf((int) options.param(1)) + "." + String.valueOf((int) options.param(THREE)));
     }
    } else if (size == SIX) {
     operator = (String) options.param(TWO);
     operand1 = Double.parseDouble(path.toString() + "." + String.valueOf((int) options.param(1)));
     operand2 = Double.parseDouble(String.valueOf((int) options.param(THREE)) + "." + String.valueOf((int) options.param(FIVE)));
    }
    
    if (operator != null) {
     BigDecimal decimal = null;
     switch (operator) {
      case "+":
       decimal = BigDecimal.valueOf(operand1 + operand2);
       decimal = decimal.setScale(TWO, BigDecimal.ROUND_HALF_EVEN);
       chars = decimal.toPlainString();
       break;
      case "-":
       decimal = BigDecimal.valueOf(operand1 - operand2);
       decimal = decimal.setScale(TWO, BigDecimal.ROUND_HALF_EVEN);
       chars = decimal.toPlainString();
       break;
      case "*":
       decimal = BigDecimal.valueOf(operand1 * operand2);
       decimal = decimal.setScale(TWO, BigDecimal.ROUND_HALF_EVEN);
       chars = decimal.toPlainString();
       break;
      case "/":
       decimal = BigDecimal.valueOf(operand1 / operand2);
       decimal = decimal.setScale(TWO, BigDecimal.ROUND_HALF_EVEN);
       chars = decimal.toPlainString();
       break;
      case "%":
       int op1 = (int) operand1;
       int op2 = (int) operand2;
       int rem = op1 % op2;
       chars = String.valueOf(rem);
       break;
      default:
       chars = "Unsupportive oprand";
     }
    }
   }
  } catch (Exception e) {
   LOGGER.error("Error occurred in Math Helper.", e);
  }
  return chars;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getHelper()
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getName()
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#setCache(com.sapient.platform.iea.aem.scripting.
  * handlebars.api.HandlebarsTemplateCache)
  */
 @Override
 public void setCache(HandlebarsTemplateCache templateCache) {
  LOGGER.info("Cache is not implemented for Math handler.");
 }
}
