/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

/**
 * The Class CategoryDetail.
 */
public class CategoryDetail {
 
 /** The category id. */
 protected String categoryId;
 
 /** The familyId id. */
 protected String familyId;
 
 /**
  * Gets the category id.
  *
  * @return the category id
  */
 public String getCategoryId() {
  return categoryId;
 }
 
 /**
  * Sets the category id.
  *
  * @param categoryId
  *         the new category id
  */
 public void setCategoryId(String categoryId) {
  this.categoryId = categoryId;
 }
 
 /**
  * @return the familyId
  */
 public String getFamilyId() {
  return familyId;
 }
 
 /**
  * @param familyId
  *         the familyId to set
  */
 public void setFamilyId(String familyId) {
  this.familyId = familyId;
 }
 
}
