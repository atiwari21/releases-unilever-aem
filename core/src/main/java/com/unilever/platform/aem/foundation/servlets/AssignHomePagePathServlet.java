/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class AssignAssetPathServlet
 *
 */
@SlingServlet(label = "Unilever assign home page path", paths = { "/bin/assignhomepagepath" }, methods = { HttpConstants.METHOD_POST })
public class AssignHomePagePathServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 782708002977083730L;
 
 /** The Constant STORE_PATH. */
 private static final String STORE_PATH = "storePath";
 
 private static final String HOME_PAGE_PATH = "homePagePath";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AssignHomePagePathServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  
  ResourceResolver resourseResolver = request.getResource().getResourceResolver();
  
  String currentPagPath = request.getParameter(STORE_PATH).toString();
  String homePagePath = request.getParameter(HOME_PAGE_PATH).toString();  
  Session session = resourseResolver.adaptTo(Session.class);
  
  try {
   Node currentPageProperties = session.getNode(currentPagPath);
   currentPageProperties.setProperty("configPagePath", homePagePath);   
   session.save();
  } catch (RepositoryException re) {
   LOGGER.error("Error while setting property on folder", re);
  }
  response.sendRedirect("/aem/products.html" + currentPagPath);
 }
 
}
