/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jsoup.nodes.Document;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * The header of the article. A header can hold an Image,
 * Title, Authors and Dates for publishing and modification of the article.
 *
 * <header>
 *     <figure>
 *         <ui:image src={$this->getHeroBackground()} />
 *     </figure>
 *     <h1>{$this->name}</h1>
 *     <address>
 *         <a rel="facebook" href="http://facebook.com/everton.rosario">Everton</a>
 *         Everton Rosario is a passionate mountain biker on Facebook
 *     </address>
 *     <time
 *         class="op-published"
 *         datetime={date('c', $this->time)}>
 *         {date('F jS, g:ia', $this->time)}
 *     </time>
 *     <time
 *         class="op-modified"
 *         datetime={date('c', $last_update)}>
 *         {date('F jS, g:ia', $last_update)}
 *     </time>
 * </header>
 */
/**
 * The Class Header.
 */
public class Header extends InstantArticleElement implements Container {
 
 /** The header. */
 private static Header header = new Header();
 
 /** The cover. */
 private InstantArticleElement cover;
 
 /** The title. */
 private H1 title;
 
 /** The sub title. */
 private H2 subTitle;
 
 /** The authors. */
 private List<Author> authors;
 
 /** The author. */
 private Author author;
 
 /** The published. */
 private Time published;
 
 /** The modified. */
 private Time modified;
 
 /** The kicker. */
 private String kicker;
 
 /** The ads. */
 private List<Ad> ads;
 
 /** The sponser. */
 private String sponsor;
 
 /**
  * Creates the.
  *
  * @return the header
  */
 public static Header create() {
  return header;
 }
 
 /**
  * Instantiates a new header.
  */
 private Header() {
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.Container#getContainerChildren()
  */
 @Override
 public List<Object> getContainerChildren() {
  List<Object> children = new ArrayList<Object>();
  if (this.cover != null) {
   children.add(this.cover);
  }
  if (this.title != null) {
   children.add(this.title);
  }
  if (this.subTitle != null) {
   children.add(this.subTitle);
  }
  if (this.published != null) {
   children.add(this.published);
  }
  if (this.modified != null) {
   children.add(this.modified);
  }
  if (this.authors != null) {
   children.add(this.authors);
  }
  if (this.kicker != null) {
   children.add(this.kicker);
  }
  if (this.ads != null) {
   children.add(this.ads);
  }
  if (this.sponsor != null) {
   children.add(this.sponsor);
  }
  return children;
 }
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }
 
 /**
  * With title.
  *
  * @param title
  *         the title
  * @return the header
  */
 public Header withTitle(Object title) {
  if (title instanceof String) {
   this.title = (H1) ((H1) H1.create()).appendText(title);
  } else if (title instanceof H1) {
   this.title = (H1) title;
  }
  return this;
 }
 
 /**
  * With sub title.
  *
  * @param subTitle the sub title
  * @return the header
  */
 public Header withSubTitle(Object subTitle) {
  if (subTitle instanceof String) {
   this.subTitle = (H2) ((H2) H2.create()).appendText(subTitle);
  } else if (subTitle instanceof H2) {
   this.subTitle = (H2) subTitle;
  }
  return this;
 }
 
 /**
  * With author.
  *
  * @param author the author
  * @return the header
  */
 public Header withAuthor(Object author) {
  if (author instanceof String) {
   this.author = (Author) ((Author) Author.create()).withName((String) author);
  } else if (author instanceof Author) {
   this.author = (Author) author;
  }
  return this;
 }
 
 /**
  * With publish time.
  *
  * @param pubDate the pub date
  * @return the header
  */
 public Header withPublishTime(Date pubDate) {
  Time time = Time.create(Time.PUBLISHED);
  this.published = time.withTime(pubDate);
  return this;
 }
 
 /**
  * With modify time.
  *
  * @param modified the modified
  * @return the header
  */
 public Header withModifyTime(Date modified) {
  Time time = Time.create(Time.MODIFIED);
  this.modified = time.withTime(modified);
  return this;
 }
 
 /**
  * Sets the cover of InstantArticle with Image or Video.
  *
  * @param cover the cover
  * @return $this
  */
 public Header withCover(Image cover) {
  
  this.cover = cover;
  return this;
 }
 
 /**
  * Gets the ads.
  *
  * @return the ads
  */
 public List<Ad> getAds() {
  return this.ads;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement()
  */
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
  
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#toDOMElement(org.jsoup.nodes.Document)
  */
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = new Document("");
  }
  if (!isValid()) {
   return null;
  }
  org.jsoup.nodes.Element element = document.createElement("header");
  if (this.cover != null && this.cover.isValid()) {
   element.appendChild(this.cover.toDOMElement(document));
  }
  if (this.title != null && this.title.isValid()) {
   element.appendChild(this.title.toDOMElement(document));
  }
  
  if (this.subTitle != null && this.subTitle.isValid()) {
   element.appendChild(this.subTitle.toDOMElement(document));
  }
  
  if (this.published != null && this.published.isValid()) {
   element.appendChild(this.published.toDOMElement(document));
  }
  
  if (this.modified != null && this.modified.isValid()) {
   element.appendChild(this.modified.toDOMElement(document));
  }
  if (this.author != null && this.author.isValid()) {
   element.appendChild(this.author.toDOMElement(document));
  }
  if (CollectionUtils.isNotEmpty(this.authors)) {
   Iterator<Author> itr = this.authors.iterator();
   while (itr.hasNext()) {
    Author author = itr.next();
    if (author.isValid()) {
     element.appendChild(author.toDOMElement(document));
    }
    
   }
  }
  if (this.kicker != null) {
   
  }
  if (CollectionUtils.isNotEmpty(this.ads)) {
   
  }
  if (this.sponsor != null) {
   
  }
  
  return element;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement#isValid()
  */
 @Override
 public boolean isValid() {
  boolean hasAds = CollectionUtils.isNotEmpty(this.ads) ? true : false;
  boolean hasValidAd = false;
  if (hasAds) {
   Iterator<Ad> itr = this.ads.iterator();
   while (itr.hasNext()) {
    Ad ad = itr.next();
    if (ad.isValid()) {
     hasValidAd = true;
     break;
    }
   }
   
  }
  boolean flag = false;
  if ((this.title != null && this.title.isValid()) || hasValidAd) {
   flag = true;
  }
  return flag;
 }
}
