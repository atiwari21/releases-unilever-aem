/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Session;
import javax.servlet.ServletException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.common.CommerceHelper;
import com.day.cq.commons.ImageResource;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.integration.dto.Categories;
import com.unilever.platform.aem.foundation.integration.dto.Category;
import com.unilever.platform.aem.foundation.integration.dto.Description;
import com.unilever.platform.aem.foundation.integration.dto.Descriptions;
import com.unilever.platform.aem.foundation.integration.dto.Feed;
import com.unilever.platform.aem.foundation.integration.dto.Identifier;
import com.unilever.platform.aem.foundation.integration.dto.Identifiers;
import com.unilever.platform.aem.foundation.integration.dto.ImageUrls;
import com.unilever.platform.aem.foundation.integration.dto.Name;
import com.unilever.platform.aem.foundation.integration.dto.Names;
import com.unilever.platform.aem.foundation.integration.dto.ObjectFactory;
import com.unilever.platform.aem.foundation.integration.dto.PageUrls;
import com.unilever.platform.aem.foundation.integration.dto.Product;
import com.unilever.platform.aem.foundation.integration.dto.ProductCategory;
import com.unilever.platform.aem.foundation.integration.dto.Products;
import com.unilever.platform.aem.foundation.integration.dto.SourceId;
import com.unilever.platform.aem.foundation.integration.dto.Url;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;

/**
 * The Class KQFeedProviderService.
 */
@SlingServlet(resourceTypes = { SearchConstants.SLING_SERVLET_RESOURCETYPE_VALUE }, methods = { HttpConstants.METHOD_GET }, selectors = {
  "feed" }, extensions = { "xml" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.integration.KQFeedProviderService", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "KQ Feed provider service call for unilever", propertyPrivate = false),

})
public class KQFeedProviderService extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(KQFeedProviderService.class);
 
 /** The Constant KQ_FILE_PATH. */
 private static final String KQ_FILE_PATH = "D:\\KQ\\KQFeed.xml";
 
 /** The Constant CATEGORY. */
 private static final String CATEGORY = "section";
 
 /** The Constant PRODUCT. */
 private static final String PRODUCT = "product";
 
 /** The Constant PRODUCT_COMMERCE_PATH. */
 private static final String PRODUCT_COMMERCE_PATH = "jcr:content/product/cq:commerceType";
 
 /** The Constant CATEGORY_COMMERCE_PATH. */
 private static final String CATEGORY_COMMERCE_PATH = "jcr:content/cq:commerceType";
 
 /** The Constant BRAND_NAME_KEY. */
 private static final String BRAND_NAME_KEY = "brandName";
 
 private static final String LOCALE_NAME_KEY = "localeName";
 
 /** The Constant FEED_DOMAIN_URL. */
 private static final String FEED_DOMAIN_URL = "feedDomainUrl";
 
 /** The Constant KQ_CATEGORY_KEY. */
 private static final String KQ_CATEGORY_KEY = "kritique";
 
 /** The Constant KQ_IMAGE_DEFAULT_SIZE. */
 private static final String KQ_IMAGE_DEFAULT_SIZE = "220x220";
 
 /** The Constant KQ_IMAGE_SIZE. */
 private static final String KQ_IMAGE_SIZE = "imageSize";
 
 private static String locale = StringUtils.EMPTY;
 
 /** The Constant THOUSAND */
 private static final int THOUSAND = 1000;
 
 /** The Constant TEN */
 private static final int TEN = 10;
 
 /** The Constant 1024 */
 private static final int SIZE_BYTES = 1024;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * Do get.
  *
  * @param slingRequest
  *         the sling request
  * @param slingResponse
  *         the sling response
  * @throws ServletException
  *          the servlet exception
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  
  Session session = null;
  String path = null;
  /** The resource resolver. */
  ResourceResolver resourceResolver;
  
  /** The domain url. */
  String domainUrl;
  
  /** The brand name. */
  String brandName;
  
  try {
   session = slingRequest.getResourceResolver().adaptTo(Session.class);
   resourceResolver = slingRequest.getResourceResolver();
   path = slingRequest.getRequestPathInfo().getResourcePath();
   
   Page page = resourceResolver.getResource(path).adaptTo(Page.class);
   if (page != null) {
    domainUrl = configurationService.getConfigValue(page, KQ_CATEGORY_KEY, FEED_DOMAIN_URL);
    brandName = configurationService.getConfigValue(page, KQ_CATEGORY_KEY, BRAND_NAME_KEY);
    locale = configurationService.getConfigValue(page, KQ_CATEGORY_KEY, LOCALE_NAME_KEY);
   } else {
    domainUrl = slingRequest.getScheme() + "://" + slingRequest.getServerName();
    brandName = path.substring(path.lastIndexOf("/") + 1, path.length());
   }
   
   if ((session != null) && (path != null)) {
    
    Feed feed = createFeeds(session, path, brandName, domainUrl);
    saveFeedtoFileSystem(feed);
   }
   
   slingResponse.setContentType("text/xml");
   slingResponse.getWriter().write(readFileToCharArray(KQ_FILE_PATH));
   
  } catch (Exception e) {
   LOGGER.error("Error while fetching feed file.", e);
  }
 }
 
 /**
  * Save feedto file system.
  *
  * @param feed
  *         the feed
  */
 private void saveFeedtoFileSystem(Feed feed) {
  try {
   JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
   Marshaller marshaller = jaxbContext.createMarshaller();
   marshaller.marshal(feed, new File(KQ_FILE_PATH));
  } catch (JAXBException e) {
   LOGGER.error("Error while saving feed file.", e);
  }
 }
 
 /**
  * Creates the feeds.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @param brandName
  *         the brand name
  * @return the feed
  */
 private Feed createFeeds(Session session, String path, String brandName, String domainUrl) {
  
  Feed feed = new Feed();
  
  feed.setBrand(brandName);
  feed.setIncremental("false");
  
  feed.setCategories(getCategoryFeed(session, path, domainUrl));
  feed.setProducts(getProductsFeed(session, path, domainUrl));
  
  return feed;
 }
 
 /**
  * Gets the category feed.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @return the category feed
  */
 private Categories getCategoryFeed(Session session, String path, String domainUrl) {
  
  Categories categories = new Categories();
  List<Category> categoryList = new ArrayList<Category>();
  List<Page> categoryPageList = PIMInjestionUtility.getPages(session, path, CATEGORY_COMMERCE_PATH, CATEGORY);
  
  for (Page page : categoryPageList) {
   if (page != null) {
    categoryList.add(getCategoryFeed(page, domainUrl));
   }
  }
  categories.setCategory(categoryList);
  
  return categories;
 }
 
 /**
  * Gets the category feed.
  *
  * @param page
  *         the page
  * @return the category feed
  */
 private Category getCategoryFeed(Page page, String domainUrl) {
  Category category = new Category();
  
  category.setSourceId(getCategorySourceId(page));
  category.setNames(getCategoryNames(page));
  category.setPageUrls(getProductPageUrls(page, domainUrl));
  
  return category;
 }
 
 /**
  * Gets the category source id.
  *
  * @param page
  *         the page
  * @return the category source id
  */
 private String getCategorySourceId(Page page) {
  return page.getName();
 }
 
 /**
  * Gets the category names.
  *
  * @param page
  *         the page
  * @return the category names
  */
 private Names getCategoryNames(Page page) {
  
  Names names = new Names();
  List<Name> nameList = new ArrayList<Name>();
  
  Name name = new Name();
  name.setLocale(locale);
  name.setValue(page.getName());
  
  nameList.add(name);
  names.setName(nameList);
  
  return names;
 }
 
 /**
  * Gets the products feed.
  *
  * @param session
  *         the session
  * @param path
  *         the path
  * @return the products feed
  */
 private Products getProductsFeed(Session session, String path, String domainUrl) {
  
  Products products = new Products();
  List<Product> productList = new ArrayList<Product>();
  List<Page> productPageList = PIMInjestionUtility.getPages(session, path, PRODUCT_COMMERCE_PATH, PRODUCT);
  
  for (Page page : productPageList) {
   if (page != null) {
    productList.add(getProductFeed(page, domainUrl));
   }
  }
  
  products.setProduct(productList);
  
  return products;
 }
 
 /**
  * Gets the product feed.
  *
  * @param page
  *         the page
  * @return the product feed
  */
 private Product getProductFeed(Page page, String domainUrl) {
  
  Product product = new Product();
  UNIProduct currentProduct = (UNIProduct) (CommerceHelper.findCurrentProduct(page));
  
  product.setSourceId(getSourceId(currentProduct));
  product.setCategory(getProductCategory(page));
  product.setIdentifiers(getProductIdentifiers(currentProduct));
  product.setNames(getProductNames(currentProduct));
  product.setImageUrls(getProductImageUrls(page, currentProduct, domainUrl));
  product.setDescriptions(getProductDescriptions(currentProduct));
  product.setPageUrls(getProductPageUrls(page, domainUrl));
  
  return product;
 }
 
 /**
  * Gets the source id.
  *
  * @param currentProduct
  *         the current product
  * @return the source id
  */
 private SourceId getSourceId(UNIProduct currentProduct) {
  
  SourceId source = new SourceId();
  
  if (currentProduct != null) {
   source.setValue(currentProduct.getSKU());
   source.setActive("true");
  }
  return source;
 }
 
 /**
  * Gets the product category.
  *
  * @param page
  *         the page
  * @return the product category
  */
 private ProductCategory getProductCategory(Page page) {
  
  ProductCategory productCategory = new ProductCategory();
  
  productCategory.setPrimarySourceId(page.getParent().getName());
  
  return productCategory;
 }
 
 /**
  * Gets the product descriptions.
  *
  * @param currentProduct
  *         the current product
  * @return the product descriptions
  */
 private Descriptions getProductDescriptions(UNIProduct currentProduct) {
  
  Descriptions descriptions = new Descriptions();
  List<Description> descriptionList = new ArrayList<Description>();
  
  Description description = new Description();
  description.setLocale(locale);
  
  if (currentProduct != null) {
   description.setValue(currentProduct.getDescription());
  }
  
  descriptionList.add(description);
  descriptions.setDescription(descriptionList);
  
  return descriptions;
 }
 
 /**
  * Gets the product names.
  *
  * @param currentProduct
  *         the current product
  * @return the product names
  */
 private Names getProductNames(UNIProduct currentProduct) {
  
  Names names = new Names();
  List<Name> nameList = new ArrayList<Name>();
  
  Name name = new Name();
  name.setLocale(locale);
  
  if (currentProduct != null) {
   name.setValue(currentProduct.getName());
  }
  
  nameList.add(name);
  names.setName(nameList);
  
  return names;
 }
 
 /**
  * Gets the product identifiers.
  *
  * @param currentProduct
  *         the current product
  * @return the product identifiers
  */
 private Identifiers getProductIdentifiers(UNIProduct currentProduct) {
  
  Identifiers identifiers = new Identifiers();
  List<Identifier> identifierList = new ArrayList<Identifier>();
  
  Identifier identifier = new Identifier();
  identifier.setType(currentProduct.getIdentifierType());
  identifier.setValue(currentProduct.getIdentifierValue());
  
  identifierList.add(identifier);
  identifiers.setIdentifier(identifierList);
  
  return identifiers;
 }
 
 /**
  * Gets the product page urls.
  *
  * @param page
  *         the page
  * @return the product page urls
  */
 private PageUrls getProductPageUrls(Page page, String domainUrl) {
  
  PageUrls pageurls = new PageUrls();
  List<Url> urlList = new ArrayList<Url>();
  String pagePath = page.getPath();
  
  Url url = new Url();
  url.setLocale(locale);
  
  pagePath = UrlResolverUtility.getResolvedPagePath(configurationService, page);
  
  url.setValue(domainUrl + pagePath + ".html");
  
  urlList.add(url);
  pageurls.setUrl(urlList);
  
  return pageurls;
 }
 
 /**
  * Gets the product image urls.
  *
  * @param page
  *         the page
  * @param currentProduct
  *         the current product
  * @return the product image urls
  */
 private ImageUrls getProductImageUrls(Page page, UNIProduct currentProduct, String domainUrl) {
  
  ImageUrls imageurls = new ImageUrls();
  List<Url> urlList = new ArrayList<Url>();
  
  Url url = new Url();
  url.setLocale(locale);
  
  if ((currentProduct != null) && (!currentProduct.getImages().isEmpty())) {
   ImageResource imgResource = currentProduct.getImages().get(0);
   String widgetImageSize = KQ_IMAGE_DEFAULT_SIZE;
   try {
    widgetImageSize = configurationService.getConfigValue(page, KQ_CATEGORY_KEY, KQ_IMAGE_SIZE);
    if (widgetImageSize == null) {
     widgetImageSize = KQ_IMAGE_DEFAULT_SIZE;
    }
   } catch (ConfigurationNotFoundException e) {
    widgetImageSize = KQ_IMAGE_DEFAULT_SIZE;
    LOGGER.error("KQ Image size configuration not found", e);
   }
   url.setValue(domainUrl + imgResource.getHref() + ".ulenscale." + widgetImageSize + imgResource.getExtension());
  }
  
  urlList.add(url);
  imageurls.setUrl(urlList);
  
  return imageurls;
 }
 
 /**
  * Read file to char array.
  *
  * @param filePath
  *         the file path
  * @return the char[]
  * @throws IOException
  *          Signals that an I/O exception has occurred.
  */
 private static char[] readFileToCharArray(String filePath) throws IOException {
  StringBuilder fileData = new StringBuilder(THOUSAND);
  BufferedReader reader = new BufferedReader(new FileReader(filePath));
  
  char[] buf = new char[TEN];
  int numRead = 0;
  while ((numRead = reader.read(buf)) != -1) {
   String readData = String.valueOf(buf, 0, numRead);
   fileData.append(readData);
   buf = new char[SIZE_BYTES];
  }
  
  reader.close();
  
  return fileData.toString().toCharArray();
 }
}
