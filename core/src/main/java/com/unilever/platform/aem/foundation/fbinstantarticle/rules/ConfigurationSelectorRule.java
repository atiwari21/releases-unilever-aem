/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.AbstractGetter;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.GetterFactory;

/**
 * The Class ConfigurationSelectorRule.
 */
public abstract class ConfigurationSelectorRule extends Rule {
 
 /** The selector. */
 protected String selector;
 
 /** The properties. */
 protected Map<String, AbstractGetter> properties = new HashMap<String, AbstractGetter>();
 
 /**
  * With selector.
  *
  * @param selector
  *         the selector
  * @return the rule
  */
 public Rule withSelector(String selector) {
  this.selector = selector.replaceAll("'", "\"");
  return this;
 }
 
 /**
  * With property.
  *
  * @param property
  *         the property
  * @param getterConfiguration
  *         the getter configuration
  * @return the rule
  */
 public Rule withProperty(String property, JSONObject getterConfiguration) {
  if (getterConfiguration != null) {
   this.properties.put(property, GetterFactory.create(getterConfiguration));
  }
  return this;
 }
 
 /**
  * With properties.
  *
  * @param properties
  *         the properties
  * @param configuration
  *         the configuration
  * @return the rule
  */
 public Rule withProperties(List<String> properties, JSONObject configuration) {
  Iterator<String> itr = properties.iterator();
  while (itr.hasNext()) {
   String property = itr.next();
   withProperty(property, (JSONObject) retrieveProperty(configuration, property));
  }
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container)
  */
 public boolean matchesContext(Container context) {
  return Arrays.asList(this.getContextClass()).contains(context.getClass().getCanonicalName());
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesNode(org.jsoup.nodes.Node)
  */
 public boolean matchesNode(Node node) {
  String[] selectors = this.selector.split(",");
  if (selectors != null && selectors.length > 0) {
   for (String selector : selectors) {
//  Handles selector = tag
    if (node.nodeName().equals(selector)) {
     return true;
    }
    // Handles selector = .class
    Pattern pattern1 = Pattern.compile("^\\.[a-zA-Z][a-zA-Z0-9-]*$");
    Matcher matcher1 = pattern1.matcher(selector);
    if (matcher1.matches()) {
     String[] cssClasses = node.attr("class").split(" ");
     // Tries every class
     for (String cssClass : cssClasses) {
      if (("." + cssClass).equals(selector)) {
       return true;
      }
     }
     // No match!
     return false;
    }
    if (node instanceof Element) {
     Elements elements = node.ownerDocument().select(selector);
     
     if (CollectionUtils.isNotEmpty(elements)) {
      Iterator<Element> itr = elements.iterator();
      while (itr.hasNext()) {
       Element ele = itr.next();
       if (node instanceof Element && ele.equals((Element) node)) {
        return true;
       }
      }
     }
    }
   }
  }
  
  return false;
 }
 
 
 /**
  * Gets the properties.
  *
  * @return the properties
  */
 public Map<String, AbstractGetter> getProperties() {
  return this.properties;
 }
 
 /**
  * Gets the property.
  *
  * @param propertyName
  *         the property name
  * @param node
  *         the node
  * @return null
  */
 public Object getProperty(String propertyName, Node node) {
  Object value = null;
  if (this.properties.containsKey(propertyName) && this.properties.get(propertyName) != null) {
   value = this.properties.get(propertyName).get(node);
  }
  return value;
 }
 
 /**
  * Escape html.
  *
  * @param html
  *         the html
  * @return the string
  */
 protected String escapeHTML(String html) {
  return html.replaceAll("\\<.*?>", "").trim();
 }
 
}
