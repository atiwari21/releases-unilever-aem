/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * This class defines the svg Helper. . <b> Usage :</b> {{svg icon="arrow-right-medium" class="arrow-right" brand="dove"}}
 * 
 * 
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class SVGHelper implements HandlebarsHelperService<Object> {
 /**
  * Logger Instance.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(SVGHelper.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new SVGHelper();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "svg";
 
 /**
  * This method is responsible for generating svg value either the if block or the else block.
  * 
  * @see Helper#apply(Object, Options)
  * @param context
  *         The object for context.
  * @param options
  *         The handle bar options.
  * @return CharSequence
  * @throws IOException
  *          The i/o exception.
  */
 @Override
 public CharSequence apply(final Object context, final Options options) throws IOException {
  
  String str = "<svg class=\"c-svg " + options.hash.get("class") + "\">   <use xlink:href=\"/etc/ui/" + options.hash.get("brand")
    + "/clientlibs/core/core/svgs/symbol/svg/sprite.symbol.svg#" + options.hash.get("icon") + "\"></use> </svg>";
  
  LOGGER.debug("output is = " + str);
  
  return new Handlebars.SafeString(str);
  
 }
 
 /**
  * @see HandlebarsHelperService#getName()
  * @return String
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /**
  * @see HandlebarsHelperService#getHelper()
  * @return Helper<Object>
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
 /**
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService#setCache
  *      (com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache)
  * @param templateCache
  *         The object for template cache.
  */
 @Override
 public void setCache(final HandlebarsTemplateCache templateCache) {
  // Not setting the cache instance as it is not required in this helper.
 }
 
}
