/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.Iterator;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.w3c.dom.Document;

/**
 * The Feed Interface
 */
public abstract interface Feed {
 public static final String DEFAULT_CONTENT_TYPE = "text/xml";
 public static final String RSS_CONTENT_TYPE = "application/rss+xml";
 public static final String ATOM_CONTENT_TYPE = "application/atom+xml";
 public static final String DEFAULT_CHARACTER_ENCODING = "utf-8";
 public static final String SELECTOR_FEED = "feeds";
 public static final String SELECTOR_RSS = "rss";
 public static final String SUFFIX_XML = ".xml";
 public static final String SELECTOR_ATOM = "atom";
 public static final String SUFFIX_HTML = ".html";
 public static final String SUFFIX_FEED = ".feeds.xml";
 public static final String MAX_CHILD_NODES = "maxEntries";
 public static final String NO_INDEX = "noindex";
 
 /**
  * 
  * @return
  */
 public abstract String getContentType();
 
 /**
  * @return
  */
 public abstract Document getDoc();
 
 /**
  * @return
  */
 public abstract String getCharacterEncoding();
 
 /**
  * @param properties
  */
 public abstract void printHeader(ValueMap properties);
 
 /**
  * @param properties
  */
 public abstract void printEntry(ValueMap properties);
 
 /**
  * @param iter
  */
 public abstract void printEntries(Iterator<Resource> iter);
 
 /**
  * To print child enteries.
  */
 public abstract void printChildEntries();
 
}
