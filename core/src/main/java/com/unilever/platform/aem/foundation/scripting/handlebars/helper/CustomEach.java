/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * This class defines the Include Helper. It compiles output of the Handlebars template specified in the & binds it with the context data. <b> Usage
 * :</b> {{include contextData path='template.hbss' }}
 * 
 * @author ntyag1
 * 
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class CustomEach implements HandlebarsHelperService<Object> {
 /**
  * Logger Instance.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(CustomEach.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new CustomEach();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "customEach";
 
 /**
  * @see Helper#apply(Object, Options)
  * @param context
  *         The object for context.
  * @param options
  *         The handle bar options.
  * @return CharSequence
  * @throws IOException
  *          The i/o exception.
  */
 @Override
 public CharSequence apply(Object context, Options options) throws IOException {
  if (context == null) {
   return "";
  }
  if (context instanceof Iterable) {
   return iterableContext((Iterable) context, options);
  }
  return hashContext(context, options);
 }
 
 /**
  * This method is responsible to fetch the base template Path. If looks for the template in tenant folder & returns appropriate base path.
  * 
  * @param options
  * @return
  */
 private CharSequence hashContext(Object context, Options options) throws IOException {
  LOGGER.info("Entering #hashContext method of EachUptoHelper class.");
  Set<Map.Entry<String, Object>> propertySet = options.propertySet(context);
  StringBuilder buffer = new StringBuilder();
  Context parent = options.context;
  for (Map.Entry<String, Object> entry : propertySet) {
   Context current = Context.newContext(parent, entry.getValue()).data("key", entry.getKey());
   
   buffer.append(options.fn(current));
  }
  return buffer.toString();
 }
 
 /**
  * This method binds the dataMap with the template.
  * 
  * @param template
  *         handlebar template.
  * @param dataMap
  *         data Map.
  * @return
  */
 private CharSequence iterableContext(Iterable<Object> context, Options options) throws IOException {
  StringBuilder buffer = new StringBuilder();
  if (options != null) {
   if (options.isFalsy(context)) {
    buffer.append(options.inverse());
   } else {
    buffer = iterableContextHelper(context, options, buffer);
   }
  }
  return buffer.toString();
 }
 
 private StringBuilder iterableContextHelper(Iterable<Object> context, Options options, StringBuilder charbuffer) throws IOException {
  
  StringBuilder buffer = charbuffer;
  Iterator<Object> iterator = context.iterator();
  int index = -1;
  int first = 0;
  int last = 0;
  Context parent = options.context;
  Map<String, Object> map = getMapUsingKeyName("data", (Map<String, Object>) options.context.model());
  
  while (iterator.hasNext()) {
   index++;
   
   int loop = options.params.length;
   if ((loop > 0) && (options.param(0) != null && index == (Integer) options.param(0))) {
    break;
   }
   
   Object element = iterator.next();
   
   last = index + 1;
   Context current = Context.newBuilder(parent, element).combine("index", Integer.valueOf(index)).combine("@index", Integer.valueOf(index))
     .combine("@root", map).combine("first", first).combine("@first", first).combine("last", last).combine("@last", last).build();
   
   buffer.append(options.fn(current));
   current.destroy();
  }
  
  return buffer;
  
 }
 
 @SuppressWarnings("unchecked")
 private Map<String, Object> getMapUsingKeyName(String keyName, Map<String, Object> contextMap) {
  
  Map<String, Object> map = null;
  for (Map.Entry<String, Object> entry : contextMap.entrySet()) {
   if (keyName.equals(entry.getKey())) {
    map = (Map<String, Object>) entry.getValue();
   }
  }
  return map;
 }
 
 /**
  * @see HandlebarsHelperService#getName()
  * @return String
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /**
  * @see HandlebarsHelperService#getHelper()
  * @return Helper<Object>
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
 /**
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService#setCache
  *      (com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache)
  * @param templateCache
  *         The object for template cache.
  */
 @Override
 public void setCache(final HandlebarsTemplateCache templateCache) {
  LOGGER.info("Cache is not implemented for renderTemplate handler.");
 }
}
