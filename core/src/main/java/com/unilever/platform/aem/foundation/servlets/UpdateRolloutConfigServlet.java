/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class UpdateRolloutConfigServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "updaterolloutconfig" }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UpdateRolloutConfigServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Updates the rollout config from default to updateconfig ", propertyPrivate = false) })
public class UpdateRolloutConfigServlet extends SlingAllMethodsServlet {
 
 /**
     * 
     */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UpdateRolloutConfigServlet.class);
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "text/html";
 
 /** The Constant JSON. */
 private static final String JSON = "json";
 
 /** The Constant SITE_PATH. */
 private static final String SITE_PATH = com.day.cq.wcm.api.NameConstants.PN_SITE_PATH;
 
 /** The Constant UPDATE_TYPE. */
 private static final String UPDATE_TYPE = "updateType";
 
 /** The Constant PAGE_PATH_LIST. */
 private static final String PAGE_PATH_LIST = "pagePathList";
 
 /** The Constant CHECKBOX_SPECIFIC_PAGES. */
 private static final String CHECKBOX_SPECIFIC_PAGES = "specificCheckbox";
 
 /** The Constant CHECKBOX_SPECIFIC_VALUE. */
 private static final String CHECKBOX_SPECIFIC_VALUE = "specificPage";
 
 /** The Constant Node Path. */
 private static final String NODE_PATH = "Node Path";
 
 /** The Constant Page Path. */
 private static final String PAGE_PATH = "Page Path";
 
 /** The Constant HTML_FORM. */
 private static final String HTML_FORM = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\">"
   + "<title>Update Properties</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">"
   + "       <table>         <tr>                <td><label></label></td>                <td>"
   + "<input type=\"hidden\" name=\"updateType\" value=\"dryRun\" id=\"updateType\"></td>         </tr>           <tr>"
   + "                <td><label>Site Path*</label></td>               <td><input type=\"text\" name=\"sitePath\"></td>"
   + "            </tr>           <tr>                <td><input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\">"
   + "</td>                <td><input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>           </tr>"
   + "       </table>    </form></body><script>  function dryRun(){      var oForm = "
   + "document.forms[\"updatePropertyForm\"];     oForm.submit(); }   function execute(){     "
   + "var updateTypeEle=document.getElementById(\"updateType\");"
   + "      updateTypeEle.value=\"execute\";        var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }</script></html>";
 
 /** The Constant HTML_FORM_NEW. */
 private static final String HTML_FORM_NEW = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\">"
   + "<title>Update Properties</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">"
   + "       <table>         <tr>                <td><label></label></td>                <td>"
   + "<input type=\"hidden\" name=\"updateType\" value=\"dryRun\" id=\"updateType\"></td>         </tr>           <tr>"
   + "                <td><label>Site Path*</label></td>               <td><input type=\"text\" name=\"sitePath\"></td>"
   + "            </tr>           <tr>                <td><input type=\"checkbox\" name=\"specificCheckbox\" "
   + "value=\"specificPage\">Update specific pages from Dry Run list? </td>            </tr>           <tr>                <td>"
   + "<label>Provide pages from list obtained after Dry Run in Comma separated format</label></td>                <td>"
   + "<textarea rows=\"10\" cols=\"100\" name=\"pagePathList\"></textarea></td>           </tr>           <tr>                <td>"
   + "<input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\"></td>                <td>"
   + "<input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>           </tr>       </table>    </form>"
   + "</body><script>  function dryRun(){      var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }"
   + "   function execute(){     var updateTypeEle=document.getElementById(\"updateType\");      updateTypeEle.value=\"execute\"; "
   + "       var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }</script></html>";
 
 /** The count. */
 static int count = 1;
 
 /** The Constant UNIQUE_PAGE_ID. */
 public static final String ROLLOUT_CONFIG = "cq:rolloutConfigs";
 
 /** The Constant LIVE_SYNC_COPY. */
 public static final String LIVE_SYNC_COPY = "/jcr:content/cq:LiveSyncConfig";
 
 /** The Constant UNIQUE_PAGE_ID_VALUE. */
 public static final String UNIQUE_PAGE_ID_VALUE = "uniquePageId property's value";
 
 /** The Constant UPDATE_CONFIG. */
 public static final String UPDATE_CONFIG = "/etc/msm/rolloutconfigs/updateconfig";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String configJSON = request.getParameter(JSON);
  LOGGER.info("configJSON in UpdateRolloutConfigServlet {} ", configJSON);
  response.setContentType(CONTENT_TYPE);
  PrintWriter out = response.getWriter();
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  LOGGER.info("sitePath in UpdateRolloutConfigServlet {} ", request.getParameter(SITE_PATH));
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = request.getParameter(SITE_PATH) != null ? pageManager.getPage(request.getParameter(SITE_PATH)) : null;
  String specificPage = request.getParameter(CHECKBOX_SPECIFIC_PAGES) != null ? request.getParameter(CHECKBOX_SPECIFIC_PAGES) : StringUtils.EMPTY;
  if (page == null) {
   out.write("<b>" + "Please provide the valid site path" + "</b>");
   out.append(HTML_FORM);
  } else {
   try {
    String updateType = request.getParameter(UPDATE_TYPE) != null ? request.getParameter(UPDATE_TYPE) : StringUtils.EMPTY;
    Map<String, String> map = new LinkedHashMap<String, String>();
    StringBuilder successMessage = new StringBuilder("");
    setNestedChildResourceList(page, resourceResolver, map);
    if ("dryRun".equals(updateType)) {
     dryRunUpdate(out, map);
    } else if (specificPage.equals(CHECKBOX_SPECIFIC_VALUE)) {
     specificPageUpdate(out, request, resourceResolver, pageManager, successMessage);
    } else if (updatePropertyNames(map, resourceResolver, successMessage)) {
     out.write("Below Nodes are updated Successfully" + "<br/><br/>");
     out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>"
       + "<th>" + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessage.toString() + "</table>");
     LOGGER.info(successMessage.toString());
     resourceResolver.commit();
    }
   } catch (Exception e) {
    out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
    LOGGER.error("Error in updating the content", e);
    resourceResolver.revert();
   }
   
  }
  out.flush();
  out.close();
 }
 
 /**
  * Dry run update.
  *
  * @param out
  *         the out
  * @param map
  *         the map
  */
 private void dryRunUpdate(PrintWriter out, Map<String, String> map) {
  out.append(HTML_FORM_NEW);
  int dryRunCount = 1;
  StringBuilder successMessageDryRun = new StringBuilder("");
  Iterator<Entry<String, String>> mapIterator = map.entrySet().iterator();
  while (mapIterator.hasNext()) {
   String nodePath = mapIterator.next().getKey();
   String[] parts = nodePath.split("/jcr:content/", TWO);
   String pagPath = parts[0];
   successMessageDryRun.append("<tr>" + "<td>" + dryRunCount + ". " + "</td>" + "<td>" + pagPath + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
   dryRunCount++;
  }
  out.write("<h2 style=" + "'color:red;'" + ">" + "Before clicking Execute Button, Please take the backup of all the page that you want to update"
    + "</h2>" + "<br/>");
  out.write("The below pages have " + "<b>" + ROLLOUT_CONFIG + "</b>" + " property." + "<br/><br/>");
  out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>" + "<th>"
    + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessageDryRun.toString() + "</table>");
  LOGGER.info(successMessageDryRun.toString());
  
 }
 
 /**
  * Specific page update.
  *
  * @param out
  *         the out
  * @param request
  *         the request
  * @param resourceResolver
  *         the resource resolver
  * @param pageManager
  *         the page manager
  * @param successMessage
  *         the success message
  */
 private void specificPageUpdate(PrintWriter out, SlingHttpServletRequest request, ResourceResolver resourceResolver, PageManager pageManager,
   StringBuilder successMessage) {
  String pagesToBeUpdatedList = request.getParameter(PAGE_PATH_LIST) != null ? request.getParameter(PAGE_PATH_LIST) : StringUtils.EMPTY;
  pagesToBeUpdatedList = pagesToBeUpdatedList.replaceAll("\\s+", "");
  if (!pagesToBeUpdatedList.isEmpty()) {
   String[] pagesToBeUpdatedArr = pagesToBeUpdatedList.split(",");
   for (int i = 0; i < pagesToBeUpdatedArr.length; i++) {
    Page newPage = pageManager.getPage(pagesToBeUpdatedArr[i]);
    if (newPage != null) {
     try {
      Map<String, String> mapNew = new LinkedHashMap<String, String>();
      setNestedChildResourceList(newPage, resourceResolver, mapNew);
      updatePropertyNames(mapNew, resourceResolver, successMessage);
     } catch (Exception e) {
      out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
      LOGGER.error("Error in updating the content", e);
      resourceResolver.revert();
     }
    } else {
     out.write("Please provide the valid site path");
     out.append(HTML_FORM_NEW);
    }
   }
   out.write("Below Nodes are updated Successfully" + "<br/><br/>");
   out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>"
     + "<th>" + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessage.toString() + "</table>");
   LOGGER.info(successMessage.toString());
   try {
    resourceResolver.commit();
   } catch (PersistenceException e) {
    LOGGER.error("Error in updating the content", e);
   }
  } else {
   out.write("<b>" + "Please provide the page path from Dry Run List if specific pages need to be updated" + "</b>");
   out.append(HTML_FORM_NEW);
  }
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
 
 /**
  * set resource list and resource map.
  *
  * @param res
  *         the res
  * @param map
  *         the list
  */
 private void setNestedChildResourceList(Page parentPage, ResourceResolver resourceResolver, Map<String, String> map) {
  LOGGER.debug("inside setNestedChildResourceList of UpdateRolloutConfigServlet");
  
  if (null != parentPage) {
   Resource resource = resourceResolver.getResource(parentPage.getPath() + LIVE_SYNC_COPY);
   if (null != resource) {
    if (!map.containsKey(resource.getPath())) {
     map.put(resource.getPath(), resource.getPath());
    }
   } else {
    LOGGER.info("No " + LIVE_SYNC_COPY + "Node exists for the page " + parentPage.getPath());
   }
   Iterator<Page> itr = parentPage.listChildren();
   while (itr.hasNext()) {
    Page childPage = itr.next();
    Resource childResource = resourceResolver.getResource(childPage.getPath() + LIVE_SYNC_COPY);
    if (null != childResource) {
     if (!map.containsKey(childResource.getPath())) {
      map.put(childResource.getPath(), childResource.getPath());
     }
    } else {
     LOGGER.info("No " + LIVE_SYNC_COPY + "Node exists for the page " + childPage.getPath());
    }
    setNestedChildResourceList(childPage, resourceResolver, map);
   }
  }
 }
 
 /**
  * Update page properties.
  *
  * @param map
  *         the map
  * @param resourceResolver
  *         the resource resolver
  * @param successMessage
  *         the success message
  * @return true, if successful
  * @throws RepositoryException
  *          the repository exception
  */
 public static boolean updatePropertyNames(Map<String, String> map, ResourceResolver resourceResolver, StringBuilder successMessage)
   throws RepositoryException {
  LOGGER.debug("inside updatePagePropertyNames of UpdateRolloutConfigServlet");
  Iterator<String> itr = map.keySet().iterator();
  while (itr.hasNext()) {
   String nodePath = itr.next();
   Resource resource = resourceResolver.getResource(nodePath);
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page page = pageManager.getContainingPage(nodePath);
   if (resource != null) {
    
    Node node = resource.adaptTo(Node.class);
    String[] strArr = { UPDATE_CONFIG };
    if (resource.getValueMap().containsKey(ROLLOUT_CONFIG)) {
     Value[] rolloutConfigValues = node.getProperty(ROLLOUT_CONFIG).getValues();
     String[] newRolloutConfigValues = new String[rolloutConfigValues.length + 1];
     List<String> list = new ArrayList<String>();
     for (int i = 0; i < rolloutConfigValues.length; i++) {
      newRolloutConfigValues[i] = rolloutConfigValues[i].toString();
      list.add(rolloutConfigValues[i].toString());
     }
     if (!list.contains(UPDATE_CONFIG)) {
      newRolloutConfigValues[rolloutConfigValues.length] = UPDATE_CONFIG;
      list.add(UPDATE_CONFIG);
     }
     
     node.setProperty(ROLLOUT_CONFIG, newRolloutConfigValues);
     successMessage.append("<tr>" + "<td>" + count + ". " + "</td>" + "<td>" + page.getPath() + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
     count++;
    } else {
     node.setProperty(ROLLOUT_CONFIG, strArr);
     successMessage.append("<tr>" + "<td>" + count + ". " + "</td>" + "<td>" + page.getPath() + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
     count++;
    }
   }
  }
  return true;
 }
 
}
