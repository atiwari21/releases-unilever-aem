/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle;

import java.util.List;

/**
 * The Class Container.
 */
public interface Container {
 
 /**
  * Gets the container children.
  *
  * @return the container children
  */
 public List<Object> getContainerChildren();
 
 /**
  * Gets the class name.
  *
  * @return the class name
  */
 public static String getClassName() {
  return Thread.currentThread().getStackTrace()[1].getClassName();
 }
}
