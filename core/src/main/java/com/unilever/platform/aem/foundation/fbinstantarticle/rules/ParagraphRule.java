/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.ListItem;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Paragraph;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class ParagraphRule.
 */
public class ParagraphRule extends ConfigurationSelectorRule {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Paragraph p = (Paragraph) Paragraph.create();
  if (container instanceof InstantArticle) {
   ((InstantArticle) container).addChild(p);
  } else if (container instanceof ListItem) {
   ((ListItem) container).addChild(p);
  }
  transformer.transform(p, node);
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName(), TextContainer.getClassName() };
 }
 
 /**
  * Creates the.
  *
  * @return the paragraph rule
  */
 public static ParagraphRule create() {
  return new ParagraphRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the paragraph rule
  */
 public static ParagraphRule createFrom(JSONObject configuration) {
  return (ParagraphRule) create().withSelector(configuration.getString("selector"));
 }
 
}
