/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The listener interface for receiving configUpdate events. The class that is interested in processing a configUpdate event implements this
 * interface, and the object created with that class is registered with a component using the component's <code>addConfigUpdateListener<code> method.
 * When the configUpdate event occurs, that object's appropriate method is invoked.
 *
 * @see ConfigUpdateEvent
 */
@Component(configurationFactory = true, label = "Unilever Config Update Service", description = "Unilever Config Update Service configurations", metatype = true, immediate = true, enabled = true)
@Service(value = EventHandler.class)
@Properties({ @Property(name = EventConstants.EVENT_TOPIC, value = { "org/apache/sling/api/resource/Resource/*" }, propertyPrivate = true),
  @Property(name = EventConstants.EVENT_FILTER, value = {
    "(&(resourceType=unilever-aem/components/page/platformConfigPage))" }, propertyPrivate = true) })
public class ConfigUpdateListener implements EventHandler {
 /** The logger. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ConfigUpdateListener.class);
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.osgi.service.event.EventHandler#handleEvent(org.osgi.service.event.Event)
  */
 @Override
 public void handleEvent(Event event) {
  LOGGER.info("inside ConfigUpdateListener handleEvent");
  
  LOGGER.info("Event Topic " + event.getTopic());
  
  String propPath = (String) event.getProperty(SlingConstants.PROPERTY_PATH);
  LOGGER.info("propPath " + propPath);
  String pagePath = propPath.split(CommonConstants.SLASH_CHAR_LITERAL + JcrConstants.JCR_CONTENT).length > 0
    ? propPath.split(CommonConstants.SLASH_CHAR_LITERAL + JcrConstants.JCR_CONTENT)[0] : null;
  LOGGER.info("pagePath " + pagePath);
  if (pagePath != null) {
   LOGGER.info("Configuration Updated " + pagePath);
   LOGGER.info("Invalidating the configuration Cache");
   configurationService.inValidateCache();
  } else {
   LOGGER.info("Configuration cache couldn't refreshed");
  }
 }
}
