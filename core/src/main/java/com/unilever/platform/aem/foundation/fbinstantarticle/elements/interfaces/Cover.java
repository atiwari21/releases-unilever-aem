/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements.interfaces;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Interface Cover.
 */
public interface Cover {
 
 /**
  * Checks if is valid.
  *
  * @return true, if is valid
  */
 public boolean isValid();
 
 /**
  * To dom element.
  *
  * @param document
  *         the document
  * @return the element
  */
 public Element toDOMElement(Document document);
 
}
