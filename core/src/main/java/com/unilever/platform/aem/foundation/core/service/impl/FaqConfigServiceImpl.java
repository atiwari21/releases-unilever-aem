/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import com.unilever.platform.aem.foundation.core.service.FaqConfigService;

/**
 * Class FaqConfigServiceImpl
 * 
 * @author asri54
 *
 */
@Component(immediate = true, metatype = false)
@Service(value = FaqConfigService.class)
public class FaqConfigServiceImpl implements FaqConfigService {
 
 private static final String WEB_SERVICE_URL = "webServiceUrl";
 /**
  * the web service url
  */
 private String webServiceUrl;
 
 /**
  * Activate
  * 
  * @param ctx
  *         {@link ComponentContext}
  */
 @Activate
 public void activate(ComponentContext ctx) {
  this.webServiceUrl = ctx.getProperties().get(WEB_SERVICE_URL) != null ? (String) ctx.getProperties().get(WEB_SERVICE_URL) : "";
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.FaqConfigService#getWebServiceUrl()
  */
 @Override
 public String getWebServiceUrl() {
  return this.webServiceUrl;
 }
 
}
