/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class ProductsType.
 */
public class ProductsType {
 
 /** The product. */
 protected List<ProductType> product;
 
 /**
  * Gets the product.
  *
  * @return the product
  */
 public List<ProductType> getProduct() {
  if (product == null) {
   product = new ArrayList<ProductType>();
  }
  return this.product;
 }
 
}
