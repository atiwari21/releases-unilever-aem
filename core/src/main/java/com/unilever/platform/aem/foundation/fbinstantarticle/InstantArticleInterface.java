/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle;

import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;

/**
 * The Interface InstantArticleInterface.
 */
public interface InstantArticleInterface {
 
 /**
  * Render.
  *
  * @return the string
  */
 public String render();
 
 /**
  * Adds the child.
  *
  * @param t
  *         the t
  * @return the instant article
  */
 public InstantArticle addChild(InstantArticleElement t);
}
