/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.resource.JcrPropertyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.common.AbstractJcrProduct;
import com.day.cq.commons.ImageResource;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.unilever.platform.aem.foundation.commerce.UNIProductHelper;
import com.unilever.platform.aem.foundation.commerce.api.CustomProduct;
import com.unilever.platform.aem.foundation.commerce.api.RecipientsInfo;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.commerce.api.WrapperInfo;

/**
 * The Class UNIProductImpl.
 */
public class UNIProductImpl extends AbstractJcrProduct implements UNIProduct {
 
 private static final String AEM_PAGE_NAME = "aemPageName";
 
 private static final String PRODUCT_NAME = "name";
 
 /** The Constant PRODUCT_DATA. */
 private static final String PRODUCT_DATA = "productData";
 
 /**
  * The Constant LOG.
  */
 private static final Logger LOG = LoggerFactory.getLogger(UNIProductImpl.class);
 
 /** The formatter. */
 private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
 
 /** The Constant SLASH. */
 private static final String SLASH = "/";
 
 /**
  * Instantiates a new Uni product impl.
  * 
  * @param resource
  *         the resource
  */
 public UNIProductImpl(final Resource resource) {
  super(resource);
  LOG.debug("Inside UniProduct implementation");
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.Product#getTitle()
  */
 @Override
 public String getTitle() {
  
  String title = StringUtils.EMPTY;
  String aemPageName = getProperty(AEM_PAGE_NAME, String.class);
  String name = getProperty(PRODUCT_NAME, String.class);
  String jcrTitle = getProperty(JcrConstants.JCR_TITLE, String.class);
  
  if (StringUtils.isNotBlank(aemPageName)) {
   title = aemPageName;
  } else if (StringUtils.isNotBlank(name)) {
   title = name;
  } else if (StringUtils.isNotBlank(jcrTitle)) {
   title = jcrTitle;
  }
  
  return title;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.api.Product#getSKU()
  */
 public String getSKU() {
  try {
   return getProperty("sku", String.class);
  } catch (Exception e) {
   LOG.warn("Error while fetching sku", e);
   return String.valueOf(getProperty("sku", Long.class));
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getName()
  */
 @Override
 public String getName() {
  
  String productName = StringUtils.EMPTY;
  String name = getProperty(PRODUCT_NAME, String.class);
  String jcrTitle = getProperty(JcrConstants.JCR_TITLE, String.class);
  
  if (StringUtils.isNotBlank(name)) {
   productName = name;
  } else if (StringUtils.isNotBlank(jcrTitle)) {
   productName = jcrTitle;
  }
  
  return productName;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getSubcategory ()
  */
 @Override
 public String getCategory() {
  return getProperty("category", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getProductionDate()
  */
 @Override
 public Date getProductionDate() {
  Date date = null;
  String prodDate = getProperty("productionDate", String.class);
  LOG.debug("production date = ", prodDate);
  if (StringUtils.isNotBlank(prodDate)) {
   try {
    date = formatter.parse(prodDate);
   } catch (ParseException e) {
    LOG.warn("Unable to parse production date. productionDate = " + prodDate, e);
   }
  }
  return date;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.common.AbstractJcrProduct#getDescription()
  */
 @Override
 public String getDescription() {
  return getProperty("description", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getBrandName ()
  */
 @Override
 public String getSize() {
  return getProperty("size", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getOfferPrice()
  */
 @Override
 public String getPrice() {
  String price = getProperty("price", String.class);
  if (StringUtils.isNotBlank(price)) {
   LOG.warn("Error while fetching price");
   price = String.valueOf(getProperty("price", Double.class));
  }
  return price;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getTags()
  */
 @Override
 public Tag[] getTags(ResourceResolver resourceResolver) {
  
  Tag[] tagArray = null;
  if (resourceResolver != null) {
   Resource productData = null;
   TagManager tagMgr = resourceResolver.adaptTo(TagManager.class);
   ValueMap map = ResourceUtil.getValueMap(this.resource);
   
   if (map.containsKey(PRODUCT_DATA)) {
    String productDataPath = map.get(PRODUCT_DATA, String.class);
    productData = this.resource.getResourceResolver().getResource(productDataPath);
    if (productData == null) {
     LOG.error("Product data not found at [{}].", productDataPath);
     return tagArray;
    }
   } else {
    productData = this.resource;
   }
   tagArray = tagMgr.getTags(productData);
  }
  return tagArray;
 }
 
 /**
  * Gets the imagescustom.
  * 
  * @param resource
  *         the resource
  * @param folderName
  *         the folder name
  * @return the imagescustom
  */
 private List<ImageResource> getImagescustom(Resource resource, String folderName) {
  List<ImageResource> result = new ArrayList<ImageResource>();
  SortedMap<String, ImageResource> sortedMap = new TreeMap<String, ImageResource>();
  Resource imagesResource = resource.getChild(folderName);
  if (imagesResource != null) {
   Iterator<Resource> images = imagesResource.listChildren();
   while (images.hasNext()) {
    ImageResource image = new ImageResource(images.next());
    sortedMap.put(image.getName(), image);
   }
  }
  result.addAll(sortedMap.values());
  return result;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.adobe.cq.commerce.common.AbstractJcrProduct#getImages()
  */
 /*
  * Gets the images in the order they are saved in crx
  */
 @Override
 public List<ImageResource> getImages() {
  List<ImageResource> images = getImagescustom(this.resource, "assets");
  if (!images.isEmpty()) {
   return images;
  }
  
  String productDataPath = ResourceUtil.getValueMap(this.resource).get(PRODUCT_DATA, String.class);
  Resource ancestor;
  if (StringUtils.isNotEmpty(productDataPath)) {
   ancestor = this.resource.getResourceResolver().getResource(productDataPath);
  } else {
   ancestor = this.resource.getParent();
  }
  while ((ancestor != null) && (isAProductOrVariant(ancestor))) {
   images = getImages(ancestor);
   if (!images.isEmpty()) {
    return images;
   }
   ancestor = ancestor.getParent();
  }
  
  return Collections.emptyList();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getIdentifierType()
  */
 @Override
 public String getIdentifierType() {
  return getProperty("identifierType", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getIdentifierValue()
  */
 @Override
 public String getIdentifierValue() {
  return getProperty("identifierValue", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getPageTitle ()
  */
 @Override
 public String getPageTitle() {
  return getProperty("title", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getSmartProductId()
  */
 @Override
 public String getSmartProductId() {
  try {
   return getProperty("smartProductId", String.class);
  } catch (Exception e) {
   LOG.error("Error while fetching smartProductId", e);
   return String.valueOf(getProperty("smartProductId", Long.class));
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getShortPageDescription()
  */
 @Override
 public String getShortPageDescription() {
  return getProperty("shortPageDescription", String.class);
 }
 
 private void logWarningMessage(String key, String value) {
  if (StringUtils.isNotBlank(value)) {
   LOG.warn("Error while fetching " + key);
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getCustomProduct ()
  */
 @Override
 public CustomProduct getCustomProduct() {
  
  CustomProduct customProduct = new CustomProduct();
  
  String customDescription = StringUtils.EMPTY;
  Double customTotalPrice = 0.0;
  Double wrappingPrice = 0.0;
  String wrappingDescriptionText = StringUtils.EMPTY;
  List<ImageResource> customImage = null;
  
  List<RecipientsInfo> recipientsInfo = new ArrayList<RecipientsInfo>();
  List<WrapperInfo> wrapperInfo = new ArrayList<WrapperInfo>();
  
  customDescription = getProperty("customizeDescription", String.class);
  logWarningMessage("customizeDescription", customDescription);
  
  wrappingDescriptionText = getProperty("customizeGiftWrapDescription", String.class);
  logWarningMessage("customizeGiftWrapDescription", wrappingDescriptionText);
  
  try {
   customTotalPrice = getProperty("customizeProductPrice", Double.class);
  } catch (Exception e) {
   LOG.warn("Error while fetching customizeProductPrice", e);
  }
  
  try {
   wrappingPrice = getProperty("wrappingPrice", Double.class);
  } catch (Exception e) {
   LOG.warn("Error while fetching wrappingPrice", e);
  }
  
  customImage = getImagescustom(this.resource, "customassets");
  
  customProduct.setCustomizeDescription(customDescription);
  customProduct.setCustomizeProductPrice(customTotalPrice);
  customProduct.setWrappingPrice(wrappingPrice);
  customProduct.setCustomizeGiftWrapDescription(wrappingDescriptionText);
  if (!customImage.isEmpty()) {
   customProduct.setCustomImage(customImage.get(0));
  } else {
   customProduct.setCustomImage(null);
  }
  
  Map<String, Object> recipientsInfoMap = getSubDataMap("recipientsInfos");
  if (recipientsInfoMap != null && !recipientsInfoMap.isEmpty()) {
   for (String key : recipientsInfoMap.keySet()) {
    JcrPropertyMap rs = (JcrPropertyMap) recipientsInfoMap.get(key);
    if (rs != null) {
     RecipientsInfo recipient = new RecipientsInfo();
     
     try {
      recipient.setCharacterCount(rs.get("characterCount", Long.class));
      recipient.setType(rs.get("type", String.class));
     } catch (Exception e) {
      LOG.warn("Error while fetching recipient", e);
     }
     
     recipientsInfo.add(recipient);
    }
   }
  }
  
  Map<String, Object> wrapperInfoMap = getSubDataMap("wrapperInfos");
  if (wrapperInfoMap != null && !wrapperInfoMap.isEmpty()) {
   for (String key : wrapperInfoMap.keySet()) {
    JcrPropertyMap rs = (JcrPropertyMap) wrapperInfoMap.get(key);
    if (rs != null) {
     WrapperInfo wrapper = new WrapperInfo();
     try {
      
      wrapper.setId(rs.get("id", String.class));
      wrapper.setText(rs.get("text", String.class));
      
      ImageResource wrapperImage = getWraperImageObject(rs.get("wrapperImage", String.class), rs.get("imageAltText", String.class));
      if (wrapperImage != null) {
       wrapper.setWrapperImageResource(wrapperImage);
      }
     } catch (Exception e) {
      LOG.warn("Error while fetching wrapperInfo", e);
     }
     
     wrapperInfo.add(wrapper);
    }
   }
  }
  
  customProduct.setRecipientsInfo(recipientsInfo);
  customProduct.setWrapperInfo(wrapperInfo);
  
  return customProduct;
 }
 
 /**
  * Gets the sub data map.
  * 
  * @param propertyMapName
  *         the property map name
  * @return the sub data map
  */
 private Map<String, Object> getSubDataMap(String propertyMapName) {
  ValueMap map = ResourceUtil.getValueMap(this.resource);
  Resource productData = null;
  if (map.containsKey("productData")) {
   String productDataPath = (String) map.get("productData", String.class);
   productData = this.resource.getResourceResolver().getResource(productDataPath);
   if (productData == null) {
    log.warn("Product data not found at [{}].", productDataPath);
    return null;
   } else {
    productData = productData.getChild(propertyMapName);
    if (productData == null) {
     log.warn("Product data not found at [{}].", productDataPath);
     return null;
    }
   }
  } else {
   productData = this.resource.getChild(propertyMapName);
  }
  
  if (productData != null) {
   Map<String, Object> childMap = getChildNodesMap(productData);
   if (childMap.isEmpty()) {
    return productData.getValueMap();
   } else {
    return childMap;
   }
  }
  return null;
 }
 
 /**
  * Gets the child nodes map.
  * 
  * @param productData
  *         the product data
  * @return the child nodes map
  */
 private Map<String, Object> getChildNodesMap(Resource productData) {
  
  Map<String, Object> map = new HashMap<String, Object>();
  for (Resource child : productData.getChildren()) {
   map.put(child.getName(), child.getValueMap());
  }
  return map;
 }
 
 /**
  * Gets the wraper image object.
  * 
  * @param imageId
  *         the image id
  * @param altText
  *         the alt text
  * @return the wraper image object
  */
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getWraperImageObject()
  */
 public ImageResource getWraperImageObject(String imageId, String altText) {
  
  ImageResource imageResource = null;
  if (StringUtils.isNotBlank(imageId)) {
   String query = "SELECT * FROM [dam:AssetContent] WHERE [metadata/recordID] IN ('" + imageId.trim() + "')";
   final Iterator<Resource> resourceIterator = this.resource.getResourceResolver().findResources(query, "JCR-SQL2");
   
   while (resourceIterator.hasNext()) {
    Resource res = resourceIterator.next();
    Asset asset = DamUtil.resolveToAsset((Resource) res);
    
    imageResource = new ImageResource((Resource) res.getParent());
    imageResource.addAttribute("fileReference", asset.getPath());
    imageResource.addAttribute("altText", altText);
    
    break;
   }
  }
  return imageResource;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#isPack()
  */
 @Override
 public boolean isPack() {
  
  boolean isPack = false;
  String childProductIds = getProperty("childProducts", String.class);
  if (StringUtils.isNotBlank(childProductIds)) {
   isPack = true;
  }
  return isPack;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getChildProducts()
  */
 @Override
 public List<UNIProduct> getChildProducts() {
  
  List<UNIProduct> childProductList = new ArrayList<UNIProduct>();
  if (isPack()) {
   String childProductIds = getProperty("childProducts", String.class);
   ValueMap map = ResourceUtil.getValueMap(this.resource);
   if (map.containsKey("productData")) {
    String productDataPath = (String) map.get("productData", String.class);
    String sku = getSKU();
    productDataPath = productDataPath.substring(0, productDataPath.indexOf(getProductPath(sku)));
    String[] childProductArray = childProductIds.split("#");
    for (String id : childProductArray) {
     UNIProduct product = UNIProductHelper.findProduct(this.resource.getResourceResolver(), id, productDataPath);
     if (product != null) {
      childProductList.add(product);
     }
    }
   }
  }
  return childProductList;
 }
 
 /**
  * Gets the product path.
  * 
  * @param sku
  *         the sku
  * @return the product path
  */
 private String getProductPath(String sku) {
  String path = "/" + sku.substring(0, 2) + "/" + sku.substring(0, 4) + "/" + sku;
  return path;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getNutritionalFacts()
  */
 @Override
 public Map<String, Object> getNutritionalFacts() {
  return getSubDataMap("nutritionalFactss/nutritionalFacts");
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getAllergy()
  */
 @Override
 public String getAllergy() {
  String allergy = getProperty("allergy", String.class);
  if (allergy != null) {
   return allergy;
  } else {
   return StringUtils.EMPTY;
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getIngredients()
  */
 @Override
 public String getIngredients() {
  String ingredients = getProperty("ingredients", String.class);
  if (ingredients != null) {
   return ingredients;
  } else {
   return StringUtils.EMPTY;
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getUniqueID( )
  */
 @Override
 public String getUniqueID() {
  return getProperty("uniqueID", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getShortIdentifierValue()
  */
 @Override
 public String getShortIdentifierValue() {
  return getProperty("shortIdentifierValue", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getAwards()
  */
 @Override
 public List<String> getAwards(String awardNamespace) {
  ResourceResolver resourceResolver = this.resource.getResourceResolver();
  Tag[] getTagList = getTags(resourceResolver);
  List<String> awardTags = new LinkedList<String>();
  
  if (getTagList != null) {
   for (Tag tagname : getTagList) {
    String tagId = tagname.getTagID();
    if (StringUtils.isNotBlank(tagId) && StringUtils.isNotBlank(awardNamespace)) {
     String awards = tagId.split(SLASH)[1];
     if (awardNamespace.equals(awards)) {
      awardTags.add(tagId);
     }
    } else {
     awardTags.add(tagId);
    }
   }
  }
  
  return awardTags;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getIlsProductName()
  */
 @Override
 public String getIlsProductName() {
  return getProperty("ilsProductName", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getLabelInsightId()
  */
 @Override
 public String getLabelInsightId() {
  return getProperty("labelInsightId", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getProductSize()
  */
 @Override
 public String getProductSize() {
  return getProperty("productSize", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getAboutThisProductTitle()
  */
 @Override
 public String getAboutThisProductTitle() {
  return getProperty("aboutThisProductTitle", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getAboutThisProductBullets()
  */
 @Override
 public String getAboutThisProductBullets() {
  return getProperty("aboutThisProductBullets", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getAboutThisProductDescription()
  */
 @Override
 public String getAboutThisProductDescription() {
  return getProperty("aboutThisProductDescription", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getPerfectForTitle()
  */
 @Override
 public String getPerfectForTitle() {
  return getProperty("perfectForTitle", String.class);
 }
 
 /*
  * (non-Javadoc)j
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getPerfectForHeadingsAndDescription()
  */
 @Override
 public String getPerfectForHeadingsAndDescription() {
  return getProperty("perfectForHeadingsAndDescription", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getHowToUseTitle()
  */
 @Override
 public String getHowToUseTitle() {
  return getProperty("howToUseTitle", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getHowToUseDescription()
  */
 @Override
 public String getHowToUseDescription() {
  return getProperty("howToUseDescription", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getTryThisTitle()
  */
 @Override
 public String getTryThisTitle() {
  return getProperty("tryThisTitle", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getTryThisDesctiption()
  */
 @Override
 public String getTryThisDesctiption() {
  return getProperty("tryThisDesctiption", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getIngredientsDisclaimer()
  */
 @Override
 public String getIngredientsDisclaimer() {
  return getProperty("ingredientsDisclaimer", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getShortProductDescription()
  */
 @Override
 public String getShortProductDescription() {
  return getProperty("shortProductDescription", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getProductImage()
  */
 @Override
 public String getProductImage() {
  return getProperty("productImage", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getAltTextImages()
  */
 @Override
 public String getAltTextImages() {
  return getProperty("altTextImages", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getImageType ()
  */
 @Override
 public String getImageType() {
  return getProperty("imageType", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct#getEANparent ()
  */
 @Override
 public String getEANparent() {
  return getProperty("EANparent", String.class);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.commerce.api.UNIProduct# getLongPageDescription()
  */
 @Override
 public String getLongPageDescription() {
  return getProperty("longPageDescription", String.class);
 }
 
 @Override
 public String getCqTags() {
  return getProperty("CqTags", String.class);
 }
 
 @Override
 public String disableBuyItNow() {
  return getProperty("DisableBuyItNow", String.class);
 }
 
 @Override
 public String getRewardAvailableFrom() {
  return getProperty("rewardAvailableFrom", String.class);
 }
 
 @Override
 public String getRewardAvailableTo() {
  return getProperty("rewardAvailableTo", String.class);
 }
 
 @Override
 public String getQuantity() {
  return getProperty("quantity", String.class);
 }
}
