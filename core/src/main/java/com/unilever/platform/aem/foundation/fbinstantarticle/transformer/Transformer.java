/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.core.helper.InheritenceHelper;
import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.rules.RelatedArticlesRule;
import com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule;

/**
 * The Class Transformer.
 */
public class Transformer {
 
 /** The Constant INSTANT_ARTICLES_PARSED_FLAG. */
 private static final String INSTANT_ARTICLES_PARSED_FLAG = "data-instant-articles-element-processed";
 
 /** The Constant PACKAGE_NAME. */
 private static final String PACKAGE_NAME = "com.unilever.platform.aem.foundation.fbinstantarticle.rules";
 
 /** The rules. */
 private Map<String, List<Rule>> rules = new LinkedHashMap<String, List<Rule>>();
 
 private InstantArticle instantArticle;
 
 private static final Logger LOGGER = LoggerFactory.getLogger(Transformer.class);
 
 /**
  * Adds the rule.
  *
  * @param rule
  *         the rule
  */
 public void addRule(Rule rule) {
  String[] contexts = rule.getContextClass();
  for (String context : contexts) {
   List<Rule> ruleList = rules.get(context) != null ? rules.get(context) : new LinkedList<Rule>();
   ruleList.add(rule);
   rules.put(context, ruleList);
  }
  
 }
 
 /**
  * Load rules.
  *
  * @param configuration
  *         the configuration
  */
 public void loadRules(JSONObject configuration) {
  Class<?>[] paramJSON = new Class[1];
  paramJSON[0] = JSONObject.class;
  JSONArray rules = configuration.getJSONArray("rules");
  for (int i = 0; i < rules.length(); ++i) {
   JSONObject rule = rules.getJSONObject(i);
   String className = rule.getString("class");
   if (StringUtils.isNotEmpty(className)) {
    className = PACKAGE_NAME + "." + className;
    try {
     Class<?> cls = Class.forName(className);
     Method method = cls.getDeclaredMethod("createFrom", paramJSON);
     addRule((Rule) method.invoke(null, rule));
    } catch (Exception e) {
      LOGGER.error("Error in creating object of class {}", className, e.getMessage());
      LOGGER.debug("Error ingetting Release version", e);
     
    }
   }
   
  }
 }
 
 /**
  * Gets the all class types.
  *
  * @param class1
  *         the class1
  * @return the all class types
  */
 public List<String> getAllClassTypes(Class<? extends Container> class1) {
  List<String> classNames = new ArrayList<String>();
  String className = class1.getName();
  classNames.add(className);
  
  Set<Class<?>> classes = InheritenceHelper.getInheritance(class1);
  Iterator<Class<?>> itr = classes.iterator();
  while (itr.hasNext()) {
   Class<?> parentClass = itr.next();
   if (!classNames.contains(parentClass.getName())) {
    classNames.add(parentClass.getName());
   }
  }
  return classNames;
 }
 
 /**
  * Checks if is processed.
  *
  * @param childNode
  *         the child node
  * @return true, if is processed
  */
 public boolean isProcessed(Node childNode) {
  return childNode.hasAttr(INSTANT_ARTICLES_PARSED_FLAG);
 }
 
 /**
  * Transform.
  *
  * @param context
  *         the context
  * @param node
  *         the node
  * @return the container
  */
 public Container transform(Container context, Node node) {
  if (node == null) {
   return context;
  }
  
  if (context instanceof InstantArticle) {
   this.instantArticle = (InstantArticle) context;
  }
  
  Iterator<Node> itr = node.childNodes().iterator();
  Container currentContext = context;
  while (itr.hasNext()) {
   Node childNode = itr.next();
   if (isProcessed(childNode) || (childNode instanceof TextNode && StringUtils.isBlank(((TextNode) childNode).text()))) {
    continue;
   }
   // Get all classes and interfaces this context extends/implements
   List<String> contextClassNames = getAllClassTypes(context.getClass());
   List<Rule> matchingContextRules = new LinkedList<Rule>();
   // Look for rules applying to any of them as context
   for (int i = 0; i < contextClassNames.size(); ++i) {
    List<Rule> matchedRules = rules.get(contextClassNames.get(i));
    if (matchedRules != null) {
     Collections.reverse(matchedRules);
     matchingContextRules.addAll(matchedRules);
     
    }
   }
   Rule[] matchingContextRulesArr = (Rule[]) matchingContextRules.toArray(new Rule[matchingContextRules.size()]);
   
   inner: for (int i = 0; i < matchingContextRulesArr.length; ++i) {
    Rule matchedRule = matchingContextRulesArr[i];
    if (matchedRule.matchesNode(childNode)) {
     if (matchedRule instanceof RelatedArticlesRule && this.instantArticle.getRelatedArticles() != null) {
      continue;
     }
     context = matchedRule.apply(this, currentContext, childNode);
     // Just a single rule for each node, so move on
     break inner;
    }
   }
   
  }
  return context;
 }
 
 /**
  * Mark as processed.
  *
  * @param node
  *         the node
  */
 public static void markAsProcessed(Element node) {
  node.attr(INSTANT_ARTICLES_PARSED_FLAG, "true");
 }
 
 /**
  * Clone node.
  *
  * @param node
  *         the node
  * @return the node
  */
 public static Node cloneNode(Node node) {
  Node cloneNode = node.clone();
  if (cloneNode.hasAttr(INSTANT_ARTICLES_PARSED_FLAG)) {
   cloneNode.removeAttr(INSTANT_ARTICLES_PARSED_FLAG);
  }
  return cloneNode;
 }
 
 /**
  * @return InstantArticle the initial context of this Transformer
  */
 public InstantArticle getInstantArticle() {
  return this.instantArticle;
 }
}
