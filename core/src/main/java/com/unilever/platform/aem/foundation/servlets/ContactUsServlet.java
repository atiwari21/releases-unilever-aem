/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class ContactUsServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "contactus" }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.ContactUsServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "updates the contact us properties ", propertyPrivate = false) })
public class ContactUsServlet extends SlingAllMethodsServlet {
 
 /**
  * The Constant serialVersionUID.
  */
 private static final long serialVersionUID = 8307445907585795134L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ContactUsServlet.class);
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "text/html";
 
 /** The Constant JSON. */
 private static final String JSON = "json";
 
 /** The Constant SITE_PATH. */
 private static final String SITE_PATH = com.day.cq.wcm.api.NameConstants.PN_SITE_PATH;
 
 /** The Constant UPDATE_TYPE. */
 private static final String UPDATE_TYPE = "updateType";
 
 /** The Constant typeOne. */
 private static final String TYPE_ONE = "/components/unilever-forms/formElement";
 
 /** The Constant typeTwo. */
 private static final String TYPE_TWO = "/components/unilever-forms/formhidden";
 
 /** The Constant propertyOneValue. */
 private static final String PROPERTY_ONE_VALUE = "contact-phoneNumbers-value";
 
 /** The Constant propertyTwoValue. */
 private static final String PROPERTY_TWO_VALUE = "contact-phoneNumbers-type";
 
 /** The Constant propertyOneValue_new. */
 private static final String PROPERYT_ONE_VALUE_NEW = "contact-phoneNumbers-0-value";
 
 /** The Constant propertyTwoValue_new. */
 private static final String PROPERTY_TWO_VALUE_NEW = "contact-phoneNumbers-0-type";
 
 /** The Constant PAGE_PATH_LIST. */
 private static final String PAGE_PATH_LIST = "pagePathList";
 
 /** The Constant CHECKBOX_SPECIFIC_PAGES. */
 private static final String CHECKBOX_SPECIFIC_PAGES = "specificCheckbox";
 
 /** The Constant Node Path. */
 private static final String NODE_PATH = "Node Path";
 
 /** The Constant Page Path. */
 private static final String PAGE_PATH = "Page Path";
 
 /** The Constant CHECKBOX_SPECIFIC_VALUE. */
 private static final String CHECKBOX_SPECIFIC_VALUE = "specificPage";
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant HTML_FORM. */
 private static final String HTML_FORM = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\">"
   + "<title>Update Properties</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">"
   + "       <table>         <tr>                <td><label></label></td>                "
   + "<td><input type=\"hidden\" name=\"updateType\" value=\"dryRun\" id=\"updateType\"></td>"
   + "         </tr>           <tr>                <td><label>Site Path*</label></td>               "
   + "<td><input type=\"text\" name=\"sitePath\"></td>            </tr>           <tr>                <td>"
   + "<input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\"></td>                "
   + "<td><input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>"
   + "           </tr>       </table>    </form></body><script>  function dryRun(){      "
   + "var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }"
   + "   function execute(){     var updateTypeEle=document.getElementById(\"updateType\");     "
   + " updateTypeEle.value=\"execute\";        var oForm = document.forms[\"updatePropertyForm\"];" + "     oForm.submit(); }</script></html>";
 
 /** The Constant HTML_FORM_NEW. */
 private static final String HTML_FORM_NEW = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>"
   + "Update Properties</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">"
   + "       <table>         <tr>                <td><label></label></td>                <td>"
   + "<input type=\"hidden\" name=\"updateType\" value=\"dryRun\" id=\"updateType\"></td>         </tr>"
   + "           <tr>                <td><label>Site Path*</label></td>               <td>"
   + "<input type=\"text\" name=\"sitePath\"></td>            </tr>           <tr>                <td>"
   + "<input type=\"checkbox\" name=\"specificCheckbox\" value=\"specificPage\">Update specific pages from Dry Run list? </td>"
   + "            </tr>           <tr>                <td><label>Provide pages from list obtained after Dry Run in Comma "
   + "separated format</label></td>" + "                <td><textarea rows=\"10\" cols=\"100\" name=\"pagePathList\"></textarea></td>"
   + "           </tr>           <tr>                <td><input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\"></td>"
   + "                <td><input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>           </tr>"
   + "       </table>    </form></body><script>  function dryRun(){      "
   + "var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }"
   + "   function execute(){     var updateTypeEle=document.getElementById(\"updateType\");      updateTypeEle.value=\"execute\";  "
   + "      var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }</script></html>";
 
 /** The count. */
 static int count = 1;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  String configJSON = request.getParameter(JSON);
  LOGGER.info("configJSON in ContactUsServlet {} ", configJSON);
  response.setContentType(CONTENT_TYPE);
  PrintWriter out = response.getWriter();
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  LOGGER.info("sitePath in ContactUsServlet {} ", request.getParameter(SITE_PATH));
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = request.getParameter(SITE_PATH) != null ? pageManager.getPage(request.getParameter(SITE_PATH)) : null;
  String specificPage = request.getParameter(CHECKBOX_SPECIFIC_PAGES) != null ? request.getParameter(CHECKBOX_SPECIFIC_PAGES) : StringUtils.EMPTY;
  if (page == null) {
   out.write("<b>" + "Please provide the valid site path" + "</b>");
   out.append(HTML_FORM);
  } else if (page != null) {
   try {
    String updateType = request.getParameter(UPDATE_TYPE) != null ? request.getParameter(UPDATE_TYPE) : StringUtils.EMPTY;
    Resource resource = resourceResolver.getResource(page.getPath());
    Map<String, String> map = new LinkedHashMap<String, String>();
    StringBuilder successMessage = new StringBuilder("");
    setNestedChildResourceList(resource, map);
    if ("dryRun".equals(updateType)) {
     dryRunUpdate(out, map);
    } else if (specificPage.equals(CHECKBOX_SPECIFIC_VALUE)) {
     specificPageUpdate(out, request, resourceResolver, pageManager, successMessage);
    } else if (updatePropertyNames(map, resourceResolver, successMessage)) {
     out.write("Below Nodes are updated Successfully" + "<br/><br/>");
     out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>"
       + "<th>" + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessage.toString() + "</table>");
     LOGGER.info(successMessage.toString());
     resourceResolver.commit();
    }
   } catch (Exception e) {
    out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
    LOGGER.error("Error in updating the content", e);
    resourceResolver.revert();
   }
   
  }
  out.flush();
  out.close();
 }
 
 /**
  * Dry run update.
  *
  * @param out
  *         the out
  * @param map
  *         the map
  */
 private void dryRunUpdate(PrintWriter out, Map<String, String> map) {
  out.append(HTML_FORM_NEW);
  int dryRunCount = 1;
  StringBuilder successMessageDryRun = new StringBuilder("");
  Iterator<Entry<String, String>> mapIterator = map.entrySet().iterator();
  while (mapIterator.hasNext()) {
   String nodePath = mapIterator.next().getKey();
   String[] parts = nodePath.split("/jcr:content/", TWO);
   String pagPath = parts[0];
   successMessageDryRun.append("<tr>" + "<td>" + dryRunCount + ". " + "</td>" + "<td>" + pagPath + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
   dryRunCount++;
  }
  out.write("<h2 style=" + "'color:red;'" + ">" + "Before clicking Execute Button, Please take the backup of all the page that you want to update"
    + "</h2>" + "<br/>");
  out.write("The below pages have " + "<b>" + PROPERTY_ONE_VALUE + "</b>" + " and " + "<b>" + PROPERTY_TWO_VALUE + "</b>" + "<br/><br/>");
  out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>" + "<th>"
    + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessageDryRun.toString() + "</table>");
  LOGGER.info(successMessageDryRun.toString());
 }
 
 /**
  * Specific page update.
  *
  * @param out
  *         the out
  * @param request
  *         the request
  * @param resourceResolver
  *         the resource resolver
  * @param pageManager
  *         the page manager
  * @param successMessage
  *         the success message
  * @throws PersistenceException
  *          the persistence exception
  */
 private void specificPageUpdate(PrintWriter out, SlingHttpServletRequest request, ResourceResolver resourceResolver, PageManager pageManager,
   StringBuilder successMessage) throws PersistenceException {
  
  String pagesToBeUpdatedList = request.getParameter(PAGE_PATH_LIST) != null ? request.getParameter(PAGE_PATH_LIST) : StringUtils.EMPTY;
  pagesToBeUpdatedList = pagesToBeUpdatedList.replaceAll("\\s+", "");
  if (!pagesToBeUpdatedList.isEmpty()) {
   String[] pagesToBeUpdatedArr = pagesToBeUpdatedList.split(",");
   for (int i = 0; i < pagesToBeUpdatedArr.length; i++) {
    Page newPage = pageManager.getPage(pagesToBeUpdatedArr[i]);
    if (newPage != null) {
     try {
      Resource resourceNew = resourceResolver.getResource(newPage.getPath());
      Map<String, String> mapNew = new LinkedHashMap<String, String>();
      
      setNestedChildResourceList(resourceNew, mapNew);
      updatePropertyNames(mapNew, resourceResolver, successMessage);
     } catch (Exception e) {
      out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
      LOGGER.error("Error in updating the content", e);
      resourceResolver.revert();
     }
    } else {
     out.write("Please provide the valid site path");
     out.append(HTML_FORM_NEW);
    }
   }
   out.write("Below Nodes are updated Successfully" + "<br/><br/>");
   out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>"
     + "<th>" + "SNo." + "</th>" + "<th>" + PAGE_PATH + "</th>" + "<th>" + NODE_PATH + "</th>" + "</tr>" + successMessage.toString() + "</table>");
   LOGGER.info(successMessage.toString());
   resourceResolver.commit();
  } else {
   out.write("<b>" + "Please provide the page path from Dry Run List if specific pages need to be updated" + "</b>");
   out.append(HTML_FORM_NEW);
  }
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
 
 /**
  * set resource list and resource map.
  *
  * @param res
  *         the res
  * @param map
  *         the list
  */
 private void setNestedChildResourceList(Resource res, Map<String, String> map) {
  LOGGER.debug("inside setNestedChildResourceList of ContactUsServlet");
  Iterator<Resource> itr = res.listChildren();
  while (itr.hasNext()) {
   Resource childRes = itr.next();
   String resourceType = childRes.getResourceType();
   LOGGER.debug("Resource Type of current iterating resource : " + resourceType);
   
   if (resourceType.endsWith(TYPE_ONE)) {
    if (childRes.getValueMap().containsValue(PROPERTY_ONE_VALUE)) {
     map.put(childRes.getPath(), childRes.getResourceType());
    }
   } else if (resourceType.endsWith(TYPE_TWO) && (childRes.getValueMap().containsValue(PROPERTY_TWO_VALUE))) {
    map.put(childRes.getPath(), childRes.getResourceType());
   }
   setNestedChildResourceList(childRes, map);
  }
 }
 
 /**
  * Update page properties.
  *
  * @param map
  *         the map
  * @param resourceResolver
  *         the resource resolver
  * @param successMessage
  *         the success message
  * @return true, if successful
  * @throws RepositoryException
  *          the repository exception
  */
 public static boolean updatePropertyNames(Map<String, String> map, ResourceResolver resourceResolver, StringBuilder successMessage)
   throws RepositoryException {
  LOGGER.debug("inside updatePagePropertyNames of ContactUsServlet");
  Iterator<String> itr = map.keySet().iterator();
  while (itr.hasNext()) {
   String nodePath = itr.next();
   String[] parts = nodePath.split("/jcr:content/", TWO);
   String pagPath = parts[0];
   String resType = map.get(nodePath);
   Resource resource = resourceResolver.getResource(nodePath);
   if (resource != null) {
    Node node = resource.adaptTo(Node.class);
    if (resType.endsWith(TYPE_ONE)) {
     node.setProperty("name", PROPERYT_ONE_VALUE_NEW);
     successMessage.append("<tr>" + "<td>" + count + ". " + "</td>" + "<td>" + pagPath + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
     count++;
    } else if (resType.endsWith(TYPE_TWO)) {
     node.setProperty("name", PROPERTY_TWO_VALUE_NEW);
     successMessage.append("<tr>" + "<td>" + count + ". " + "</td>" + "<td>" + pagPath + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
     count++;
    }
   }
  }
  return true;
 }
 
}
