/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.IFrame;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class IFrameRule.
 */
public class IFrameRule extends ConfigurationSelectorRule {
 
 /** The Constant PROPERTY_ANCHOR_HREF. */
 public static final String PROPERTY_IFRAME_SRC = "iframe.src";
 
 /**
  * Instantiates a new anchor rule.
  */
 private IFrameRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the image rule
  */
 public static IFrameRule create() {
  return new IFrameRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  IFrame iframe = IFrame.create();
  String source = this.getProperty(PROPERTY_IFRAME_SRC, node) != null ? this.getProperty(PROPERTY_IFRAME_SRC, node).toString() : null;
  
  if (StringUtils.isNotBlank(source)) {
   iframe.withSource(source);
  }
  transformer.getInstantArticle().addChild(iframe);
  return container;
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the audio rule
  */
 public static IFrameRule createFrom(JSONObject configuration) {
  IFrameRule iframeRule = create();
  iframeRule = (IFrameRule) iframeRule.withSelector(configuration.getString("selector"));
  JSONObject anchorConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_IFRAME_SRC);
  iframeRule.withProperties(properties, anchorConfiguration);
  return iframeRule;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
