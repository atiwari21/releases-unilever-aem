/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ValueMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * The RssFeed Class
 */
public class RssFeed extends AbstractFeed {
 /**
  * @param req
  * @param resp
  */
 public RssFeed(SlingHttpServletRequest req) {
  super(req);
 }
 
 /**
  * Prints the XMl header for RSS feed
  * 
  * @param properties
  *         the properties
  */
 public void printHeader(ValueMap properties) {
  initXml();
  Document doc = this.getDoc();
  this.rootElement = doc.createElement("rss");
  rootElement.setAttribute("version", "2.0");
  rootElement.setAttribute("xmlns:atom", "http://www.w3.org/2005/Atom");
  doc.appendChild(rootElement);
  Element channel = doc.createElement("channel");
  rootElement.appendChild(channel);
  Element property = doc.createElement("link");
  property.appendChild(doc.createTextNode(getHtmlLink()));
  channel.appendChild(property);
  if (properties != null) {
   printNodeValueMap(properties, channel);
  }
 }
 
 /**
  * Prints the XMl Child Elements for RSS feed
  * 
  * @param properties
  *         the properties
  */
 public void printEntry(ValueMap properties) {
  Document doc = this.getDoc();
  Element item = doc.createElement("item");
  rootElement.appendChild(item);
  Element link = doc.createElement("link");
  link.appendChild(doc.createTextNode(getHtmlLink()));
  item.appendChild(link);
  Element guid = doc.createElement("guid");
  guid.appendChild(doc.createTextNode(getFeedLink()));
  item.appendChild(guid);
  if (properties != null) {
   printNodeValueMap(properties, item);
  }
 }
 
 /**
  * @return the content type
  */
 public String getContentType() {
  return RSS_CONTENT_TYPE;
 }
 
 /**
  * @return feed link
  */
 public String getFeedLink() {
  return getUrlPrefix() + getNodePath() + "." + SELECTOR_FEED + "." + SELECTOR_RSS + SUFFIX_XML;
 }
 
}
