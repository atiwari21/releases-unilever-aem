/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

/**
 * The Class ProductVariantType.
 */
public class ProductVariantType {
 
 /** The name. */
 protected String name;
 
 /** The variantId id. */
 protected String variantId;
 
 /** The variantSize id. */
 protected String variantSize;
 
 /** The identifier. */
 protected Identifier identifier;
 
 /**
  * @return the name
  */
 public String getName() {
  return name;
 }
 
 /**
  * @param name
  *         the name to set
  */
 public void setName(String name) {
  this.name = name;
 }
 
 /**
  * @return the variantId
  */
 public String getVariantId() {
  return variantId;
 }
 
 /**
  * @param variantId
  *         the variantId to set
  */
 public void setVariantId(String variantId) {
  this.variantId = variantId;
 }
 
 /**
  * @return the variantSize
  */
 public String getVariantSize() {
  return variantSize;
 }
 
 /**
  * @param variantSize
  *         the variantSize to set
  */
 public void setVariantSize(String variantSize) {
  this.variantSize = variantSize;
 }
 
 /**
  * @return the identifier
  */
 public Identifier getIdentifier() {
  return identifier;
 }
 
 /**
  * @param identifier
  *         the identifier to set
  */
 public void setIdentifier(Identifier identifier) {
  this.identifier = identifier;
 }
 
}
