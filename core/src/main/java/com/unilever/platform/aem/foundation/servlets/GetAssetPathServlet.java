/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.Writer;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.Resource;

/**
 * The Class AssignAssetPathServlet
 *
 */
@SlingServlet(label = "Unilever assign base asset path", paths = { "/bin/getassetpath" }, methods = { HttpConstants.METHOD_GET })
public class GetAssetPathServlet extends SlingAllMethodsServlet {
 
 /** The Constant GLOBAL_ASSET_BASE_PATH. */
 private static final String GLOBAL_ASSET_BASE_PATH = "globalAssetBasePath";
 
 /** The Constant LOCAL_ASSET_BASE_PATH. */
 private static final String LOCAL_ASSET_BASE_PATH = "localAssetBasePath";
 
 private static final String IMAGE_FORMAT = "imageFormat";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(GetAssetPathServlet.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  try {
   ResourceResolver resourceResolver = request.getResource().getResourceResolver();
   Writer out = response.getWriter();
   JSONWriter writer = new JSONWriter(out);
   response.setContentType("application/json");
   response.setCharacterEncoding("UTF-8");
   String path = request.getRequestParameter("path").toString();
   Resource target = resourceResolver.getResource(path);
   Node productNode = target.adaptTo(Node.class);
   String globalAssetBasePath = productNode.getProperty(GLOBAL_ASSET_BASE_PATH).getValue().toString();
   String localAssetBasePath = productNode.getProperty(LOCAL_ASSET_BASE_PATH).getValue().toString();
   String imageFormat = productNode.getProperty(IMAGE_FORMAT).getValue().toString();
   writer.array();
   addWriterObj(writer, globalAssetBasePath);
   addWriterObj(writer, localAssetBasePath);
   addWriterObj(writer, imageFormat);
   writer.endArray();
  } catch (Exception re) {
   LOGGER.error("Error while getting property", re);
  }
  
 }
 
 private void addWriterObj(JSONWriter writer, String value) throws JSONException {
  writer.object();
  writer.key("text").value(value.trim());
  writer.key("value").value(value.trim());
  writer.endObject();
 }
 
}
