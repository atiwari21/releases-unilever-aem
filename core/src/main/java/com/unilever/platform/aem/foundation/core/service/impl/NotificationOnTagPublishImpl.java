/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import javax.jcr.Session;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationAction;
import com.day.cq.tagging.TagConstants;
import com.day.cq.workflow.WorkflowService;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.model.WorkflowModel;
import com.unilever.platform.aem.foundation.core.service.NotificationOnTagPublish;

/**
 * NotificationOnTagPublishImpl Service
 */
@Component(immediate = true, metatype = true, enabled = true)
@Service(value = NotificationOnTagPublishImpl.class)
public class NotificationOnTagPublishImpl implements NotificationOnTagPublish {
 
 private static final String TAG_WORKFLOW = "/etc/workflow/models/unilever-tag-publish-notification/jcr:content/model";
 
 /** The Constant log. */
 private static final Logger LOGGER = LoggerFactory.getLogger(NotificationOnTagPublishImpl.class);
 
 @Reference
 private WorkflowService workflowService;
 
 /**
  * Gets the notication on tag publish.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param event
  *         the event
  * @param modelPath
  *         the model path
  * @return the notication on tag publish
  */
 @Override
 public void getNoticationOnTagPublish(ResourceResolver resourceResolver, Event event, String modelPath) {
  try {
   
   ReplicationAction action = ReplicationAction.fromEvent(event);
   String path = action.getPath();
   
   Resource res = resourceResolver.resolve(path);
   ValueMap vmap = res.getValueMap();
   
   String primaryType = MapUtils.getString(vmap, JcrConstants.JCR_PRIMARYTYPE);
   
   if ((TagConstants.NT_TAG).equals(primaryType)) {
    
    Session session = resourceResolver.adaptTo(Session.class);
    // Create a workflow session
    if (session != null) {
     WorkflowSession wfSession = workflowService.getWorkflowSession(session);
     
     // Get the workflow model
     WorkflowModel wfModel = wfSession.getModel(modelPath);
     
     // Get the workflow data
     // The first param in the newWorkflowData method is the payloadType. Just a fancy name to let it know what type of workflow it is
     // working
     // with.
     WorkflowData wfData = wfSession.newWorkflowData("JCR_PATH", path);
     
     // Run the Workflow.
     wfSession.startWorkflow(wfModel, wfData);
     session.logout();
    }
   }
  } catch (Exception e) {
   LOGGER.debug("No action found{}", e);
  }
  
 }
 
 /**
  * Gets the workflow event.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param event
  *         the event
  * @return the workflow event
  */
 @Override
 public String getWorkflowEvent(ResourceResolver resourceResolver, Event event) {
  ReplicationAction action = ReplicationAction.fromEvent(event);
  String path = action != null ? action.getPath() : StringUtils.EMPTY;
  
  Resource res = resourceResolver.resolve(path);
  ValueMap vmap = res.getValueMap();
  
  String primaryType = MapUtils.getString(vmap, JcrConstants.JCR_PRIMARYTYPE);
  
  if ((TagConstants.NT_TAG).equals(primaryType)) {
   return TAG_WORKFLOW;
  }
  
  return null;
 }
}
