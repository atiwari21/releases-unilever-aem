/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.tags;

import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * This class holds methods which interacts with global configuration and returns the configuration for requested category. This is typically meant to
 * be used in JSP via tag library.
 * 
 * @author nbhart
 *
 */
public final class GlobalConfiguration {
 
 /**
  * Holds the instance of {@link Logger} for logging information.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(GlobalConfiguration.class);
 
 /**
     * 
     */
 private GlobalConfiguration() {
  
 }
 
 /**
  * Read global configurations for passed in category and return the configuration map.
  * 
  * @param resource
  * @param categoryName
  * @return configuration map.
  */
 public static Map<String, String> getCategoryConfig(final Resource resource, final String categoryName) {
  Map<String, String> config = null;
  if (resource != null) {
   PageManager pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
   
   Page page = pageManager.getContainingPage(resource);
   ConfigurationService configurationService = page.adaptTo(ConfigurationService.class);
   if (page != null) {
    try {
     config = configurationService.getCategoryConfiguration(page, categoryName);
    } catch (ConfigurationNotFoundException e) {
     LOGGER.error("Cound not read configurations for key {}", categoryName, e);
    }
   }
   
  }
  return config;
 }
}
