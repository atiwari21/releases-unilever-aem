/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;

/**
 * Abstract class Audible This class represents all elements that can contain Audio
 * <ul>
 * <li>Image</li>
 * <li>SlideShow</li>
 * </ul>
 * .
 *
 * Example: <audio> <source src="http://mydomain.com/path/to/audio.mp3" /> </audio>
 *
 */
public abstract class Audible extends InstantArticleElement implements Container {
 /**
  * Adds audio to this image.
  *
  * @param Audio
  *         $audio The audio object
  *
  * @return $this
  */
 abstract public Audible withAudio(Audio audio);
}
