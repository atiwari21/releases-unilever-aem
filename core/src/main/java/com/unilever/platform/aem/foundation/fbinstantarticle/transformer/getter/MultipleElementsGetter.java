/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

/**
 * The Class MultipleElementsGetter.
 */
public class MultipleElementsGetter extends AbstractGetter {
 
 protected List<AbstractGetter> childrens = new ArrayList<AbstractGetter>();
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.AbstractGetter#createFrom(org.json.JSONObject)
  */
 @Override
 public AbstractGetter createFrom(JSONObject configuration) {
  JSONArray getterConfigurationArr = configuration.getJSONArray("children");
  Iterator<Object> itr = getterConfigurationArr.iterator();
  while (itr.hasNext()) {
   JSONObject getterConfiguration = (JSONObject) itr.next();
   childrens.add(GetterFactory.create(getterConfiguration));
  }
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.AbstractGetter#get(org.jsoup.nodes.Element)
  */
 @Override
 public Object get(Node node) {
  StringBuilder docFragmentStr = new StringBuilder();
  Iterator<AbstractGetter> itr = childrens.iterator();
  while (itr.hasNext()) {
   AbstractGetter child = itr.next();
   Element clone = (Element) child.get(node);
   docFragmentStr.append(clone.html());
  }
  return docFragmentStr;
 }
 
}
