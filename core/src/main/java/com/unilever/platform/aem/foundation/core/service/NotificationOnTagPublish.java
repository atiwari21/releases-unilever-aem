/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.core.service;

import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.event.Event;

/**
 * The Interface NotificationOnTagPublish.
 */
public interface NotificationOnTagPublish {
 
 /**
  * Gets the notication on tag publish.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param event
  *         the event
  * @param modelPath
  *         the model path
  * @return the notication on tag publish
  */
 public void getNoticationOnTagPublish(ResourceResolver resourceResolver, Event event, String modelPath);
 
 /**
  * Gets the workflow event.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param event
  *         the event
  * @return the workflow event
  */
 public String getWorkflowEvent(ResourceResolver resourceResolver, Event event);
 
}
