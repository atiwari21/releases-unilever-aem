/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The Class UpdateIFramePropertyServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { "updateiframeresourceproperty" }, methods = {
  HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UpdateIFramePropertyServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Updates the resource node property to vlaue specified in the form ", propertyPrivate = false) })

public class UpdateIFramePropertyServlet extends SlingAllMethodsServlet {
 
 /**
  * The default version uid.
  */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(UpdateIFramePropertyServlet.class);
 
 /** The Constant SANDBOX. */
 private static final String SANDBOX = "sandbox";
 
 /** The Constant SANDBOX. */
 private static final String ENABLE_RESTRICTIONS = "enableRestrictions";
 
 /** The Constant CONTENT_TYPE. */
 private static final String CONTENT_TYPE = "text/html";
 
 /** The Constant SITE_PATH. */
 private static final String SITE_PATH = com.day.cq.wcm.api.NameConstants.PN_SITE_PATH;
 
 /** The Constant UPDATE_TYPE. */
 private static final String UPDATE_TYPE = "updateType";
 
 /** The Constant HTML_FORM. */
 private static final String HTML_FORM = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>Update Properties</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">       <table>         <tr>                <td><label></label></td>                <td><input type=\"hidden\" name=\"updateType\" value=\"dryRun\" id=\"updateType\"></td>         </tr>           <tr>                <td><label>Site Path*</label></td>               <td><input type=\"text\" name=\"sitePath\"></td>            </tr>           <tr>                <td><input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\"></td>                <td><input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>           </tr>       </table>    </form></body><script>  function dryRun(){      var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }   function execute(){     var updateTypeEle=document.getElementById(\"updateType\");      updateTypeEle.value=\"execute\";        var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }</script></html>";
 
 /** The Constant HTML_FORM_NEW. */
 private static final String HTML_FORM_NEW = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>Update Properties</title></head><body>  <form action=\"\" method=\"get\" name=\"updatePropertyForm\">       <table>         <tr>                <td><label></label></td>                <td><input type=\"hidden\" name=\"updateType\" value=\"dryRun\" id=\"updateType\"></td>         </tr>           <tr>                <td><label>Site Path*</label></td>               <td><input type=\"text\" name=\"sitePath\"></td>            </tr><tr>                <td><input type=\"button\" value=\"Dry Run\" onclick=\"dryRun();\"></td>                <td><input type=\"button\" value=\"Execute\" onclick=\"execute();\"></td>           </tr>       </table>    </form></body><script>  function dryRun(){      var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }   function execute(){     var updateTypeEle=document.getElementById(\"updateType\");      updateTypeEle.value=\"execute\";        var oForm = document.forms[\"updatePropertyForm\"];     oForm.submit(); }</script></html>";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  response.setContentType(CONTENT_TYPE);
  PrintWriter out = response.getWriter();
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  Page page = request.getParameter(SITE_PATH) != null ? pageManager.getPage(request.getParameter(SITE_PATH)) : null;
  if (page == null) {
   out.write("<b>" + "Please provide the valid site path" + "</b>");
   out.append(HTML_FORM);
  } else {
   try {
    String updateType = request.getParameter(UPDATE_TYPE) != null ? request.getParameter(UPDATE_TYPE) : StringUtils.EMPTY;
    Map<String, String> map = new LinkedHashMap<String, String>();
    getUpdatedResourceList(page, pageManager, resourceResolver, map);
    if (StringUtils.equals("dryRun", updateType)) {
     out.append(HTML_FORM_NEW);
     int dryRunCount = 1;
     StringBuilder successMessageDryRun = new StringBuilder("");
     
     for (Map.Entry<String, String> entry : map.entrySet()) {
      successMessageDryRun
        .append("<tr>" + "<td>" + dryRunCount + ". " + "</td>" + "<td>" + entry.getValue() + "</td>" + "<td>" + entry.getKey() + "</td>" + "</tr>");
      dryRunCount++;
     }
     out.write("<h2 style=" + "'color:red;'" + ">" + "Before clicking Execute Button, Please take the backup of all the page that you want to update"
       + "</h2>" + "<br/>");
     out.write("The below pages have used" + "<b> IFrame </b>" + " component." + "<br/><br/>");
     out.write("<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>"
       + "<th>" + "SNo." + "</th>" + "<th>" + "Page Path" + "</th>" + "<th>" + "Node Path" + "</th>" + "</tr>" + successMessageDryRun.toString()
       + "</table>");
     LOGGER.info(successMessageDryRun.toString());
    } else if (StringUtils.equals("execute", updateType)) {
     StringBuilder successMessage = new StringBuilder("");
     updateContentNodeProperty(map, resourceResolver, successMessage);
     out.write("Below Nodes are updated Successfully" + "<br/><br/>");
     out.write(
       "<table width=" + "'100%'" + " " + "cellspacing=" + "'0'" + " " + "cellpadding=" + "'0'" + " " + "border=" + "'1'" + ">" + "<tr>" + "<th>"
         + "SNo." + "</th>" + "<th>" + "Page Path" + "</th>" + "<th>" + "Node Path" + "</th>" + "</tr>" + successMessage.toString() + "</table>");
     LOGGER.info(successMessage.toString());
     resourceResolver.commit();
    }
   } catch (Exception e) {
    out.write("Error in updating the content  " + ExceptionUtils.getStackTrace(e));
    LOGGER.error("Error in updating the content", e);
    resourceResolver.revert();
   }
   
  }
  out.flush();
  out.close();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache. sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  doPost(request, response);
  
 }
 
 /**
  * set resource list and resource map.
  *
  * @param parentPage
  *         the parentPage
  * @param pageManager
  *         the pageManager
  * @param resourceResolver
  *         the resourceResolver
  * @param map
  *         the list
  */
 private void getUpdatedResourceList(Page parentPage, PageManager pageManager, ResourceResolver resourceResolver, Map<String, String> map) {
  LOGGER.debug("inside getUpdatedResourceList of UpdateIFramePropertyServlet.");
  String queryStatement = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([" + parentPage.getPath() + "]) and "
    + "(s.[sling:resourceType] = '/apps/unilever-iea/components/iframe' OR " + "s.[sling:resourceType] = 'unilever-iea/components/iframe') "
    + "and s.[sandbox] IS NOT NULL";
  LOGGER.info("Query Statment::" + queryStatement);
  Session session = resourceResolver.adaptTo(Session.class);
  // Query to get the iframe content node.
  QueryManager queryManager;
  try {
   queryManager = session.getWorkspace().getQueryManager();
   Query query = queryManager.createQuery(queryStatement, Query.JCR_SQL2);
   QueryResult result = query.execute();
   NodeIterator nodes;
   nodes = result.getNodes();
   while (nodes.hasNext()) {
    Node node = nodes.nextNode();
    Page page = pageManager.getContainingPage(node.getPath());
    map.put(node.getPath(), page.getPath());
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in getUpdatedResourceList method of UpdateIFramePropertyServlet Class", e);
  }
 }
 
 /**
  * Update page properties.
  *
  * @param map
  *         the map
  * @param resourceResolver
  *         the resource resolver
  * @param successMessage
  *         the success message
  * @return true, if successful
  * @throws RepositoryException
  *          the repository exception
  */
 public static void updateContentNodeProperty(Map<String, String> map, ResourceResolver resourceResolver, StringBuilder successMessage)
   throws RepositoryException {
  LOGGER.debug("inside updateContentNodeProperty of UpdateIFramePropertyServlet");
  Iterator<String> itr = map.keySet().iterator();
  int count = 1;
  while (itr.hasNext()) {
   String nodePath = itr.next();
   Resource resource = resourceResolver.getResource(nodePath);
   PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
   Page page = pageManager.getContainingPage(nodePath);
   if (resource != null) {
    Node node = resource.adaptTo(Node.class);
    if (node.hasProperty(SANDBOX)) {
     String sandbox = node.getProperty(SANDBOX).getString();
     if (StringUtils.isNotEmpty(sandbox)) {
      node.setProperty(ENABLE_RESTRICTIONS, "true");
      String[] sanboxArray = StringUtils.split(sandbox, "-");
      if (sanboxArray.length > 0) {
       sandbox = StringUtils.EMPTY;
       for (String s : sanboxArray) {
        sandbox = StringUtils.isNotEmpty(sandbox) ? sandbox + WordUtils.capitalize(s) : s;
       }
       node.setProperty(sandbox, "true");
       // Remove old property
       node.getProperty(SANDBOX).remove();
       successMessage.append("<tr>" + "<td>" + count + ". " + "</td>" + "<td>" + page.getPath() + "</td>" + "<td>" + nodePath + "</td>" + "</tr>");
       count++;
      }
     }
    }
   }
  }
 }
}
