/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;

/**
 * This class defines the Include Helper. It compiles output of the Handlebars template specified in the & binds it with the context data. <b> Usage
 * :</b> {{include contextData path='template.hbss' }}
 *
 * @author ntyag1
 * 
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class PatternHelper implements HandlebarsHelperService<Object> {
 /**
  * Logger Instance.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(PatternHelper.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new PatternHelper();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "getPattern";
 
 /** The Constant ONE. */
 private static final int ONE = 1;
 
 /** The Constant TWO. */
 private static final int TWO = 2;
 
 /** The Constant THREE. */
 private static final int THREE = 3;
 
 /** The Constant FOUR. */
 private static final int FOUR = 4;
 
 /**
  * The Enum Pattern.
  */
 public enum Pattern {
  
  A(ONE, TWO, TWO, TWO, THREE, TWO, TWO, TWO), B(ONE, TWO, TWO, TWO, TWO, FOUR, TWO, TWO, TWO, THREE, TWO, TWO, TWO), C(TWO, TWO, TWO, THREE, TWO,
    TWO, TWO, ONE), KEY('a', 'b', 'c', 'd');
  
  /** The array. */
  int[] array;
  
  /**
   * Instantiates a new pattern.
   *
   * @param code
   *         the code
   */
  private Pattern(int... code) {
   this.array = code;
  }
 }
 
 /**
  * Apply.
  *
  * @param context
  *         The object for context.
  * @param options
  *         The handle bar options.
  * @return CharSequence
  * @throws IOException
  *          The i/o exception.
  * @see Helper#apply(Object, Options)
  */
 
 @Override
 public CharSequence apply(Object context, Options options) throws IOException {
  
  int length = 0;
  int count = 0;
  int index = 0;
  String variant = null;
  CharSequence chars = null;
  Pattern var = null;
  
  if (context != null) {
   length = (Integer) context;
  }
  
  if (options.params[0] != null) {
   variant = (String) options.params[0];
  }
  
  if (options.params[1] != null) {
   index = (Integer) options.params[1];
  }
  
  for (Pattern p : Pattern.values()) {
   if (variant.equals(p.name())) {
    var = p;
   }
  }
  
  for (int i = 0; i < length; i++) {
   
   if (i == index) {
    return Character.toString((char) Pattern.KEY.array[var.array[count] - 1]);
   }
   count++;
   
   if (count == var.array.length) {
    count = 0;
   }
   
  }
  return chars;
 }
 
 /**
  * Gets the name.
  *
  * @return String
  * @see HandlebarsHelperService#getName()
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /**
  * Gets the helper.
  *
  * @return Helper<Object>
  * @see HandlebarsHelperService#getHelper()
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
 /**
  * Sets the cache.
  *
  * @param templateCache
  *         The object for template cache.
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService#setCache
  *      (com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache)
  */
 @Override
 public void setCache(final HandlebarsTemplateCache templateCache) {
  LOGGER.info("Cache is not implemented for renderTemplate handler.");
 }
}
