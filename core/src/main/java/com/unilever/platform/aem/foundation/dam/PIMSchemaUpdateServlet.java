/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.dam;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

import org.apache.sling.jcr.resource.JcrResourceConstants;

/**
 * PIMSchemaUpdateServlet
 */
@SlingServlet(label = "Unilever PIM schema Update Proprties Servlet", resourceTypes = { CommonConstants.DEFAULT_SERVLET_NAME }, methods = {
  HttpConstants.METHOD_GET }, selectors = { "updatepimschema" }, extensions = { "json" })
@Properties({
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.dam.PIMSchemaUpdateServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Unilever PIM schema Update Proprties Servlet", propertyPrivate = false),

})
public class PIMSchemaUpdateServlet extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(PIMSchemaUpdateServlet.class);
 
 /** The Constant DEFAULT_CONTENT_PATH. */
 private static final String DEFAULT_CONTENT_PATH = "/content/pimschema";
 
 private static final String DEFAULT_SCHEMA_PATH = "/etc/commerce/products";
 
 /** The builder. */
 @Reference
 private QueryBuilder builder;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws IOException {
  
  response.setContentType("application/json");
  response.setCharacterEncoding("UTF-8");
  
  ResourceResolver resourceResolver = request.getResourceResolver();
  Session session = resourceResolver.adaptTo(Session.class);
  
  RequestParameterMap requestParametrs = request.getRequestParameterMap();
  
  String searchPath = requestParametrs.containsKey("sp") ? requestParametrs.getValue("sp").toString() : StringUtils.EMPTY;
  
  if (StringUtils.isBlank(searchPath)) {
   searchPath = DEFAULT_SCHEMA_PATH;
  }
  
  Query query = builder.createQuery(PredicateGroup.create(getQueryMap(searchPath)), resourceResolver.adaptTo(Session.class));
  SearchResult result = query.getResult();
  
  int counterPages = 0;
  // iterating over the results
  for (Hit hit : result.getHits()) {
   Page page = getSchemaPagesFromSearchHit(hit, resourceResolver);
   if (page != null) {
    counterPages = moveSchemaPage(page, session, counterPages, resourceResolver, response);
   }
  }
  response.getWriter().write("Total no of pages moved : " + String.valueOf(counterPages));
 }
 
 private String getMovedPagePath(Node pageNode, ResourceResolver resourceResolver, Session session) {
  
  String location = StringUtils.EMPTY;
  try {
   if (pageNode != null) {
    location = pageNode.getParent().getPath().replace(DEFAULT_SCHEMA_PATH, DEFAULT_CONTENT_PATH);
    if (resourceResolver.getResource(location) == null) {
     JcrUtil.createPath(location, true, JcrResourceConstants.NT_SLING_FOLDER, JcrResourceConstants.NT_SLING_FOLDER, session, true);
    }
   }
  } catch (RepositoryException e) {
   LOGGER.error("unable to get move page path ", e);
  }
  
  return location;
 }
 
 private int moveSchemaPage(Page page, Session session, int counterAssets, ResourceResolver resourceResolver, SlingHttpServletResponse response) {
  
  try {
   if (page != null) {
    Node pageNode = page.adaptTo(Node.class);
    Node parentNode = pageNode.getParent();
    String newPath = getMovedPagePath(pageNode, resourceResolver, session);
    if (StringUtils.isNotBlank(newPath)) {
     pageNode.getSession().move(pageNode.getPath(), newPath + "/" + pageNode.getName());
     parentNode.setProperty("schemaPath", newPath + "/" + pageNode.getName());
     session.save();
     response.getWriter().write("Moved page from Old path -> " + pageNode.getPath() + " to New path -> " + newPath + ".\n");
     counterAssets++;
    } else {
     response.getWriter().write("Unable to move pages for -> " + pageNode.getPath() + ".\n");
    }
   }
  } catch (RepositoryException e) {
   LOGGER.error("Exception occured while moving schema page to new location ", e);
  } catch (IOException e) {
   LOGGER.error("Exception occured while writting to response ", e);
  }
  
  return counterAssets;
 }
 
 private Map<String, String> getQueryMap(String path) {
  Map<String, String> map = new HashMap<String, String>();
  
  map.put("path", path);
  map.put("type", "cq:PageContent");
  map.put("1_property", JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY);
  map.put("1_property.value", "unilever-aem/components/page/schemaConfigurationPage");
  
  map.put("p.offset", "0");
  map.put("p.limit", "-1");
  
  return map;
 }
 
 private Page getSchemaPagesFromSearchHit(Hit h, ResourceResolver resourceResolver) {
  String pagePath = StringUtils.EMPTY;
  Page page = null;
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  try {
   pagePath = h.getPath();
   if (StringUtils.isNotBlank(pagePath) && (pageManager != null)) {
    page = pageManager.getContainingPage(resourceResolver.getResource(pagePath));
   }
  } catch (RepositoryException e) {
   LOGGER.error("Error in getting page from search result : ", e);
  }
  return page;
 }
}
