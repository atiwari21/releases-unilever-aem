/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Footer;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.ListElement;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class H1Rule.
 */
public class ListElementRule extends ConfigurationSelectorRule {
 private ListElementRule() {
  
 }
 
 public static ListElementRule create() {
  return new ListElementRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName(), Footer.getClassName() };
 }
 
 public static ListElementRule createFrom(JSONObject configuration) {
  return (ListElementRule) create().withSelector(configuration.getString("selector"));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Element)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  ListElement listElement;
  if (node.nodeName().equalsIgnoreCase("ol")) {
   listElement = ListElement.createOrdered();
  } else {
   listElement = ListElement.createUnOrdered();
  }
  if (container instanceof InstantArticle) {
   ((InstantArticle) container).addChild(listElement);
  }
  transformer.transform(listElement, node);
  return container;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
 
}
