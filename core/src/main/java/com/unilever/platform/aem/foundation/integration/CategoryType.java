/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

/**
 * The Class CategoryType.
 */
public class CategoryType {
 
 /** The external id. */
 protected String categoryId;
 
 /** The name. */
 protected String name;
 
 /** The category page url. */
 protected String categoryPageUrl;
 
 /** The image url. */
 protected String imageUrl;
 
 /** The faimlyId id. */
 protected String faimlyId;
 
 protected String parentCategoryId;
 
 /**
  * Gets the value of the externalId property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getCategoryId() {
  return categoryId;
 }
 
 /**
  * Sets the value of the externalId property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setCategoryId(String value) {
  this.categoryId = value;
 }
 
 /**
  * Gets the value of the name property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getName() {
  return name;
 }
 
 /**
  * Sets the value of the name property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setName(String value) {
  this.name = value;
 }
 
 /**
  * Gets the value of the categoryPageUrl property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getCategoryPageUrl() {
  return categoryPageUrl;
 }
 
 /**
  * Sets the value of the categoryPageUrl property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setCategoryPageUrl(String value) {
  this.categoryPageUrl = value;
 }
 
 /**
  * Gets the value of the imageUrl property.
  * 
  * @return possible object is {@link String }
  * 
  */
 public String getImageUrl() {
  return imageUrl;
 }
 
 /**
  * Sets the value of the imageUrl property.
  * 
  * @param value
  *         allowed object is {@link String }
  * 
  */
 public void setImageUrl(String value) {
  this.imageUrl = value;
 }
 
 /**
  * @return the faimlyId
  */
 public String getFaimlyId() {
  return faimlyId;
 }
 
 /**
  * @param faimlyId
  *         the faimlyId to set
  */
 public void setFaimlyId(String faimlyId) {
  this.faimlyId = faimlyId;
 }
 
 /**
  * @return the parentCategoryId
  */
 public String getParentCategoryId() {
  return parentCategoryId;
 }
 
 /**
  * @param parentCategoryId
  *         the parentCategoryId to set
  */
 public void setParentCategoryId(String parentCategoryId) {
  this.parentCategoryId = parentCategoryId;
 }
}
