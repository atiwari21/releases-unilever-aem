/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.redfin.sitemapgenerator.ChangeFreq;
import com.redfin.sitemapgenerator.WebSitemapGenerator;
import com.redfin.sitemapgenerator.WebSitemapUrl;
import com.sapient.platform.iea.aem.core.tenant.Tenant;
import com.sapient.platform.iea.aem.core.utils.HTTPWebServiceUtils;
import com.sapient.platform.iea.core.exception.SystemException;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.servlets.UnileverSitemap.Link;

/**
 *
 * This servlet generates the sitemap.xml for a site. This is activated when "sitemap" selector and "xml" extension is found inside the requested URL.
 *
 *
 * sample URL: http://localhost:4502/content/dove/en_gb.sitemap.xml
 *
 * In this URL the selector is sitemap, which causes this servlet to execute
 *
 *
 * @author sku131
 * 
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { UnileverSiteMapGenerationServlet.SELECTOR_SITEMAP }, extensions = {
  UnileverSiteMapGenerationServlet.XML }, methods = { HttpConstants.METHOD_GET })
@Properties(value = {
  @Property(name = Constants.SERVICE_PID, value = "com.unilever.platform.aem.foundation.servlets.UnileverSiteMapGenerationServlet", propertyPrivate = false),
  @Property(name = Constants.SERVICE_DESCRIPTION, value = "Generates the unilever specific sitemap", propertyPrivate = false) })
public class UnileverSiteMapGenerationServlet extends SlingAllMethodsServlet {
 
 private static final String PROTOCOL_END_STRING = "://";
 
 private static final String STACK_TRACE = "Stack Trace: ";
 
 private static final int SEVENTY_FIVE = 75;
 
 /**
  * serialVersionUID for Serialization.
  */
 private static final long serialVersionUID = -5424105031079224414L;
 
 /**
  * Constant for "sitemap" selector.
  */
 public static final String SELECTOR_SITEMAP = "sitemaps";
 
 /**
  * Constant for "sitemap" selector.
  */
 public static final String SITEMAP = "SiteMap: ";
 
 /**
  * Constant for "xml" extension.
  */
 public static final String XML = "xml";
 
 /**
  * Constant for user agent
  */
 public static final String USER_AGENT = "User-agent: *";
 
 /**
  * Constant for sitemap XML
  */
 public static final String SITEMAP_XML = "sitemaps.xml";
 
 /**
  * Constant for sitemap TXT
  */
 public static final String SITEMAPXML = "sitemapXML";
 
 /**
  * Constant for SECURE
  */
 public static final String SECURE = "/secure/";
 
 /**
  * Constant for HTTP
  */
 public static final String HTTP = "http";
 
 /**
  * Constant for HTTPS
  */
 public static final String HTTPS = "https";
 /**
  * Constant for HTTPS
  */
 public static final String NEWLINE = "\n";
 
 /**
  * Constant for HTTPS
  */
 public static final String HTMLEXT = ".html";
 
 /**
  * Constant for SLASH
  */
 public static final String DOT = ".";
 
 /**
  * Constant for Content type
  */
 public static final String CONTENT_TYPE_XML = "text/xml";
 
 /**
  * Constant for Content type
  */
 public static final String CONTENT_TYPE_HTML = "text/html";
 
 /**
  * Constant for TENANTCONFIG
  */
 public static final String TENANTCONFIG = "tenantConfig";
 
 /**
  * Constant for TENANTCONFIG
  */
 public static final String SITEMAPCHANGREFREQUENCY = "SitemapChangeFrequency";
 
 /**
  * Identifier to hold value of logger.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(UnileverSiteMapGenerationServlet.class);
 
 /**
  * (non-Javadoc)
  *
  * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest , javax.servlet.http.HttpServletResponse)
  **/
 @Override
 protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) throws ServletException, IOException {
  
  String output = null;
  PrintWriter pw = null;
  String[] selectors = request.getRequestPathInfo().getSelectors();
  int indexOfSitemap = ArrayUtils.indexOf(selectors, SELECTOR_SITEMAP);
  
  Map<String, Object> result = new HashMap<String, Object>(SEVENTY_FIVE);
  ResourceResolver resourceResolver = request.getResource().getResourceResolver();
  
  String rootPath = request.getRequestPathInfo().getResourcePath();
  
  Page rootPage = resourceResolver.adaptTo(PageManager.class).getPage(rootPath);
  
  if (null != rootPage) {
   
   UnileverSitemap sitemap = null;
   if (indexOfSitemap > -1) {
    
    sitemap = new UnileverSitemap(rootPage);
    List<Link> linkList = sitemap.getLinks();
    result.putAll(createXML(request, linkList));
    response.setContentType(CONTENT_TYPE_XML);
    pw = response.getWriter();
    output = (String) result.get(SITEMAPXML);
    LOGGER.debug("Create SiteMap from: " + rootPage.getPath());
   }
   
  } else {
   response.setContentType(CONTENT_TYPE_HTML);
   pw = response.getWriter();
   output = "Unable to generate Sitemap/robot please check domain name";
  }
  
  pw.write(output);
  pw.flush();
  pw.close();
  
 }
 
 /**
  * This method will generate the sitemap XML using sitemapgen4j utility.
  *
  * @param resources
  *         - map of all data/properties in request and will hold any <key,value> that needs to be sent back to the view
  * @param linkList
  *         - containing the list of Pages.
  * @return - Map having XML format of sitemap.
  */
 private Map<String, Object> createXML(final SlingHttpServletRequest request, final List<Link> linkList) {
  
  StringBuilder xmlData = new StringBuilder();
  Map<String, Object> result = new HashMap<String, Object>(SEVENTY_FIVE);
  
  try {
   File file = new File("./");
   
   String serverPath = HTTPWebServiceUtils.getServerPath(request);
   String protocol = request.getHeader("X-Forwarded-AEM-Proto") != null ? request.getHeader("X-Forwarded-AEM-Proto") : "http";
   int indexOfEndProtocol = StringUtils.indexOf(serverPath, PROTOCOL_END_STRING);
   if (StringUtils.isNotBlank(protocol) && indexOfEndProtocol <= CommonConstants.FIVE) {
    serverPath = protocol + StringUtils.substring(serverPath, indexOfEndProtocol, serverPath.length());
   }
   LOGGER.info("Server path for generating sitemap: " + serverPath);
   WebSitemapGenerator webSitemapGenerator = WebSitemapGenerator.builder(serverPath, file).build();
   WebSitemapUrl webSitemapUrl;
   
   Link link;
   Iterator<Link> linkIterator = linkList.iterator();
   
   Tenant tenantInfo = (Tenant) request.getAttribute(TENANTCONFIG);
   
   ResourceResolver resourceResolver = request.getResource().getResourceResolver();
   
   /*
    * Getting the change frequency (sitemapChangeFreq) from tenant configuration
    * 
    * Corresponding changes made to isValidFilterURL method of com.sapient.platform.iea.aem.core.utils.FilterUtils to include the filter for sitemap
    */
   
   Map<String, Object> tenantConfigMap = tenantInfo.getTenantConfigMap();
   String sitemapChangeFreq = (String) tenantConfigMap.get(SITEMAPCHANGREFREQUENCY);
   ChangeFreq changeFrequency = getChangeFrequency(sitemapChangeFreq);
   
   while (linkIterator.hasNext()) {
    link = linkIterator.next();
    
    webSitemapUrl = new WebSitemapUrl.Options(serverPath + StringEscapeUtils.escapeXml(resourceResolver.map(link.getPath() + HTMLEXT)))
      .lastMod(new Date()).priority(1.0).changeFreq(changeFrequency).build();
    webSitemapGenerator.addUrl(webSitemapUrl);
   }
   
   List<File> fileList = webSitemapGenerator.write();
   
   BufferedReader in = new BufferedReader(new FileReader(fileList.get(0)));
   
   String line = in.readLine();
   while (line != null) {
    xmlData.append(line.contains(SECURE) ? line.replace(HTTP + PROTOCOL_END_STRING, HTTPS + PROTOCOL_END_STRING) : line);
    xmlData.append(NEWLINE);
    line = in.readLine();
   }
   in.close();
   
  } catch (MalformedURLException e) {
   LOGGER.error(STACK_TRACE + ExceptionUtils.getFullStackTrace(e));
   throw new SystemException("Malformed Sitemap XML", e);
   
  } catch (SystemException e) {
   LOGGER.error(STACK_TRACE + ExceptionUtils.getFullStackTrace(e));
   throw new SystemException("Could not create Sitemap XML", e);
   
  } catch (IOException e) {
   LOGGER.error(STACK_TRACE + ExceptionUtils.getFullStackTrace(e));
   throw new SystemException("Could not create Sitemap XML", e);
  }
  result.put(SITEMAPXML, xmlData.toString());
  return result;
 }
 
 private ChangeFreq getChangeFrequency(String sitemapChangeFreq) {
  
  ChangeFreq changeFreq = ChangeFreq.DAILY;
  if (null != sitemapChangeFreq && !sitemapChangeFreq.isEmpty()) {
   switch (sitemapChangeFreq.toLowerCase()) {
    case "always":
     changeFreq = ChangeFreq.ALWAYS;
     break;
    case "hourly":
     changeFreq = ChangeFreq.HOURLY;
     break;
    case "daily":
     changeFreq = ChangeFreq.DAILY;
     break;
    case "weekly":
     changeFreq = ChangeFreq.WEEKLY;
     break;
    case "monthly":
     changeFreq = ChangeFreq.MONTHLY;
     break;
    case "yearly":
     changeFreq = ChangeFreq.YEARLY;
     break;
    case "never":
     changeFreq = ChangeFreq.NEVER;
     break;
    default:
     changeFreq = ChangeFreq.DAILY;
     break;
   }
   
  } else {
   changeFreq = ChangeFreq.DAILY;
  }
  return changeFreq;
  
 }
 
}
