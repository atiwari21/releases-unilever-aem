/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

/**
 * The Class FragmentGetter.
 */
public class FragmentGetter extends AbstractGetter {
 
 /** The fragment. */
 protected String fragment;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.AbstractGetter#createFrom(org.json.JSONObject)
  */
 @Override
 public AbstractGetter createFrom(JSONObject configuration) {
  return null;
 }
 
 /**
  * With fragment.
  *
  * @param fragment
  *         the fragment
  * @return the abstract getter
  */
 public AbstractGetter withFragment(String fragment) {
  this.fragment = fragment;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.AbstractGetter#get(org.jsoup.nodes.Element)
  */
 @Override
 public Object get(Node node) {
  StringBuilder docFragmentStr = new StringBuilder();
  docFragmentStr.append(this.fragment);
  Node cloneNode = node.clone();
  ((Element) cloneNode).html(docFragmentStr.toString());
  return cloneNode;
 }
 
}
