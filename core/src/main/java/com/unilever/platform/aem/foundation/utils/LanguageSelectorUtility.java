/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.variants.PageVariant;
import com.day.cq.wcm.api.variants.PageVariantsProvider;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;

/**
 * The Class LanguageSelectorUtility.
 */
public final class LanguageSelectorUtility {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(LanguageSelectorUtility.class);
 
 /** The Constant LINK. */
 private static final String LINK = "link";
 
 /** The Constant LINK. */
 private static final String LANGUAGES_COUNT = "languagesCount";
 
 /** The Constant TITLE. */
 private static final String TITLE = "title";
 
 /** The Constant LANGUAGE_CODE. */
 private static final String LANGUAGE_CODE = "languageCode";
 
 /** The Constant SELECTED. */
 private static final String SELECTED = "selected";
 
 /** The Constant LANGUAGE_SELECTOR_CATEGORY. */
 private static final String LANGUAGE_SELECTOR_CATEGORY = "languageSelector";
 
 /** The Constant LANGUAGE_SELECTOR_CATEGORY. */
 private static final String FOOTER_CATEGORY = "footer";
 
 /** The Constant EXCLUDED_PAGES_LIST. */
 private static final String EXCLUDED_PAGES_LIST = "excludedPagesList";
 
 /** enabled key used in map. */
 private static final String FOOTER_LANGUAGE_SELCTOR_ENABLED = "languageSelectorEnabled";
 
 /** isEnabled key used in map. */
 public static final String IS_ENABLED = "isEnabled";
 
 /** The Constant LANGUAGES. */
 private static final String LANGUAGES = "languages";
 
 /**
  * Instantiates a new language selector utility.
  */
 private LanguageSelectorUtility() {
  
 }
 
 /**
  * Gets the page variants.
  *
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @param pageVariant
  *         the page variant
  * @return the page variants
  */
 private static List<PageVariant> getPageVariants(Page page, SlingHttpServletRequest slingRequest, PageVariantsProvider pageVariant) {
  
  List<PageVariant> variantsList = null;
  
  if (pageVariant != null) {
   variantsList = pageVariant.getVariants(page, slingRequest);
  }
  
  return variantsList;
 }
 
 /**
  * Gets the languages by country.
  *
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param pageVariant
  *         the page variant
  * @param fullPath
  *         the full path
  * @return the languages by country
  */
 public static List<Map<String, String>> getLanguagesByCountry(Page currentPage, SlingHttpServletRequest slingRequest,
   ResourceResolver resourceResolver, ConfigurationService configurationService, PageVariantsProvider pageVariant, boolean fullPath) {
  
  List<Map<String, String>> languageList = new ArrayList<Map<String, String>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  int count = 0;
  try {
   count = (Integer.parseInt(configurationService.getConfigValue(currentPage, LANGUAGE_SELECTOR_CATEGORY, LANGUAGES_COUNT)));
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Configuration Not found for Category: " + LANGUAGE_SELECTOR_CATEGORY + " and key: " + LANGUAGES_COUNT);
   LOGGER.error(ExceptionUtils.getStackTrace(e));
  }
  try {
   languageList = getLanguageMap(currentPage, slingRequest, resourceResolver, configurationService, count, pageVariant, fullPath);
  } catch (Exception e) {
   LOGGER.error("Error while fetching languages for same country ", e);
  }
  
  return languageList;
 }
 
 /**
  * Gets the language map.
  *
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param count
  *         the count
  * @param pageVariant
  *         the page variant
  * @param isFullPath
  *         the is full path
  * @return the language map
  */
 public static List<Map<String, String>> getLanguageMap(Page currentPage, SlingHttpServletRequest slingRequest, ResourceResolver resourceResolver,
   ConfigurationService configurationService, int count, PageVariantsProvider pageVariant, boolean isFullPath) {
  List<PageVariant> variantPages;
  List<Map<String, String>> languageList = new ArrayList<Map<String, String>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  variantPages = getPageVariants(currentPage, slingRequest, pageVariant);
  int localeCount = 0;
  
  if (variantPages != null) {
   if (count == -1) {
    localeCount = variantPages.size();
   } else {
    localeCount = count;
   }
   String country = currentPage.getLanguage(true).getCountry();
   String[] excludedList = getExcludedUrls(currentPage, configurationService);
   
   languageList = getLanguageList(currentPage, resourceResolver, isFullPath, variantPages, localeCount, country, excludedList);
  }
  return languageList;
 }
 
 /**
  * @param currentPage
  * @param resourceResolver
  * @param isFullPath
  * @param variantPages
  * @param localeCount
  * @param country
  * @param pageManager
  * @param excludedList
  * @return
  */
 private static List<Map<String, String>> getLanguageList(Page currentPage, ResourceResolver resourceResolver, boolean isFullPath,
   List<PageVariant> variantPages, int localeCount, String country, String[] excludedList) {
  List<Map<String, String>> languageList = new ArrayList<Map<String, String>>();
  PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
  for (PageVariant variant : variantPages) {
   if (languageList.size() < localeCount) {
    Map<String, String> langPagesMap = null;
    
    Page page = pageManager.getPage(variant.getPath());
    if ((page != null) && (country.equals(page.getLanguage(true).getCountry()))) {
     
     langPagesMap = getLanguagePageMap(currentPage, resourceResolver, isFullPath, excludedList, page);
    }
    if (langPagesMap != null) {
     languageList.add(langPagesMap);
    }
   }
   
  }
  
  return languageList;
 }
 
 private static Map<String, String> getLanguagePageMap(Page currentPage, ResourceResolver resourceResolver, boolean isFullPath, String[] excludedList,
   Page page) {
  Boolean isExclude = false;
  Map<String, String> langPagesMap = new HashMap<String, String>();
  for (String exPage : excludedList) {
   if (page.getPath().startsWith(exPage)) {
    isExclude = true;
    break;
   }
  }
  if (!isExclude) {
   
   langPagesMap.put(TITLE, page.getTitle());
   if (isFullPath) {
    langPagesMap.put(LINK, page.getPath());
   } else {
    langPagesMap.put(LINK, AdaptiveUtil.getFullURL(resourceResolver, page.getPath()));
   }
   langPagesMap.put(LANGUAGE_CODE, page.getLanguage(true).getLanguage());
   if (page.equals(currentPage)) {
    langPagesMap.put(SELECTED, "true");
   } else {
    langPagesMap.put(SELECTED, "false");
   }
  }
  return langPagesMap;
 }
 
 /**
  * Gets the excluded urls.
  *
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return the excluded urls
  */
 public static String[] getExcludedUrls(Page currentPage, ConfigurationService configurationService) {
  
  String[] excludedPagesList = {};
  String excludedPages = null;
  
  try {
   excludedPages = configurationService.getConfigValue(currentPage, LANGUAGE_SELECTOR_CATEGORY, EXCLUDED_PAGES_LIST);
   if (StringUtils.isNotBlank(excludedPages)) {
    excludedPagesList = excludedPages.split(",");
   }
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Unable to find excluded Pages List Category ", e);
  }
  
  return excludedPagesList;
 }
 
 /**
  * Returns the languages list corresponding to current page's country along with the language.
  *
  * @param currentPage
  *         the current page
  * @param slingRequest
  *         the sling request
  * @param resourceResolver
  *         the resource resolver
  * @param configurationService
  *         the configuration service
  * @param pageVariant
  *         the page variant
  * @param fullPath
  *         the full path
  * @return the languages
  */
 public static Map<String, Object> getLanguages(Page currentPage, SlingHttpServletRequest slingRequest, ResourceResolver resourceResolver,
   ConfigurationService configurationService, PageVariantsProvider pageVariant, boolean fullPath) {
  Map<String, Object> languageSelector = new HashMap<String, Object>();
  languageSelector.put(LANGUAGES, LanguageSelectorUtility
    .getLanguagesByCountry(currentPage, slingRequest, resourceResolver, configurationService, pageVariant, fullPath).toArray());
  String isEnabled = "true";
  try {
   isEnabled = configurationService.getConfigValue(currentPage, LANGUAGE_SELECTOR_CATEGORY, FOOTER_LANGUAGE_SELCTOR_ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Unable to find enabled config ", e);
  }
  
  languageSelector.put(IS_ENABLED, BooleanUtils.toBoolean(isEnabled));
  return languageSelector;
  
 }
 
 /**
  * Returns true/false based on if the language selector enabled in footer.
  *
  * @param currentPage
  *         the current page
  * @param configurationService
  *         the configuration service
  * @return true/false
  */
 public static boolean isLanguageSelectorEnabledInFooter(Page currentPage, ConfigurationService configurationService) {
  String isEnabled = "true";
  try {
   isEnabled = configurationService.getConfigValue(currentPage, FOOTER_CATEGORY, FOOTER_LANGUAGE_SELCTOR_ENABLED);
  } catch (ConfigurationNotFoundException e) {
   LOGGER.error("Unable to find isEnabled Category ", e);
  }
  return BooleanUtils.toBoolean(isEnabled);
 }
 
}
