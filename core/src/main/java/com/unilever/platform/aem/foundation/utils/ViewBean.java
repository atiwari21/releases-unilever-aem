/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.utils;

import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;

/**
 * The class ViewBean
 * 
 */
public class ViewBean {
 String superResourceType;
 String scriptPath;
 String jsonNameSpace;
 ViewHelper viewHelper;
 
 /**
  * @param scriptPath
  *         the scriptPath
  * @param superResourceType
  *         the superResourceType
  * @param jsonNameSpace
  *         the jsonNameSpace
  */
 public ViewBean(String scriptPath, String superResourceType, String jsonNameSpace) {
  super();
  this.superResourceType = superResourceType;
  this.scriptPath = scriptPath;
  this.jsonNameSpace = jsonNameSpace;
  
 }
 
 public String getSuperResourceType() {
  return superResourceType;
 }
 
 public void setSuperResourceType(String superResourceType) {
  this.superResourceType = superResourceType;
 }
 
 public String getScriptPath() {
  return scriptPath;
 }
 
 public void setScriptPath(String scriptPath) {
  this.scriptPath = scriptPath;
  
 }
 
 public String getJsonNameSpace() {
  return jsonNameSpace;
 }
 
 public void setJsonNameSpace(String jsonNameSpace) {
  this.jsonNameSpace = jsonNameSpace;
 }
 
 public void setViewHelper(ViewHelper viewHelper) {
  this.viewHelper = viewHelper;
 }
 
 public ViewHelper getViewHelper() {
  return viewHelper;
 }
 
}
