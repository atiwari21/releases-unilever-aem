/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.codehaus.jackson.map.ObjectMapper;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.commons.WCMUtils;
import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.utils.CollectionUtils;
import com.sapient.platform.iea.aem.core.utils.FilterUtils;
import com.sapient.platform.iea.aem.core.utils.SlingUtils;
import com.unilever.platform.aem.foundation.core.service.TemplateView;
import com.unilever.platform.aem.foundation.utils.AdaptiveUtil;
import com.unilever.platform.aem.foundation.utils.ViewBean;

/**
 * This is the service which is called when the template is of type hbss or html or jsp.
 */
@Component(immediate = true, metatype = true, label = "Template View", description = "Service that is used to fetch the template view path")
@Service()
@Properties({ @Property(propertyPrivate = true, name = "componentViewBasePath", value = { "/apps/iea/components" }),
  @Property(name = org.osgi.framework.Constants.SERVICE_RANKING, intValue = 6000, propertyPrivate = true) })
public class TemplateViewImpl implements TemplateView {
 
 /**
  * Logger Object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(TemplateViewImpl.class);
 
 private static final String ETC_UI = "/etc/ui/";
 
 private static final String HBSS_REMAIN_PATH = "/templates/components/";
 
 private static final String REGEX = "^(.*)(.html|.jsp|.hbss)$";
 
 private static final int TEN = 10;
 /**
  * The componentBaseTemplateViewPath.
  */
 private String componentTemplateBasePath;
 
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /**
  * This method retrieves the views along with their corresponding values under a given tenant path. If the tenant path does not exist , then the
  * views are picked from the path /etc/ui/iea/templates/components. It would return all the views of a given component. eg. if adaptive-image is the
  * component, then the following map would be returned: {defaultView = /etc/ui/iea/templates/components/adaptive-image/ displayAs_defaultView.hbss}
  * 
  * @param request
  *         The SlingHttpServletRequest object
  * @return The map containing the view and it is corresponding template path as key-value pair respectively
  * @throws IOException
  */
 public Map<String, String> fetchComponentViewBasePathMap(final SlingHttpServletRequest request) throws IOException {
  
  LOGGER.info("Entering fetchComponentViewBasePathMap Method");
  StringBuilder componentViewBasePath = new StringBuilder();
  componentViewBasePath.append(componentTemplateBasePath);
  
  Map<String, String> optionMap = new HashMap<String, String>();
  
  String componentResourceType = WCMUtils.getComponent(request.getResource()).getResourceType();
  String componentName = WCMUtils.getComponent(request.getResource()).getName();
  componentResourceType = SlingUtils.getFullResourcePath(componentResourceType);
  Map<String, ViewBean> viewsBeanMap = new HashMap<String, ViewBean>();
  String adaptiveTenantName = AdaptiveUtil.getAdaptiveTenantName(request);
  String componentViewPath = ETC_UI + adaptiveTenantName + HBSS_REMAIN_PATH + componentName;
  if (StringUtils.isNotEmpty(componentViewPath)) {
   /** updated for demo site tenant issue. **/
   Resource resource = request.getResourceResolver().getResource(componentViewPath);
   if (resource != null) {
    Iterator<Resource> parentIterator = resource.listChildren();
    while (parentIterator.hasNext()) {
     Resource parentResource = parentIterator.next();
     if (parentResource.getName().matches(REGEX)) {
      String nodeName = parentResource.getName().replaceFirst(REGEX, "$1");
      String key = CollectionUtils.getKeyForValue(nodeName);
      int stringLength = key.length();
      String newKey = key.substring(TEN, stringLength);
      viewsBeanMap.put(newKey, new ViewBean(parentResource.getPath(), "", ""));
     }
    }
   }
  }
  for (Map.Entry<String, ViewBean> view : viewsBeanMap.entrySet()) {
   optionMap.put(view.getKey(), view.getValue().getScriptPath());
  }
  
  LOGGER.debug("Views to be fetched for " + componentResourceType);
  LOGGER.debug("The views and its corresponding view path for " + componentResourceType + CommonConstants.HYPHEN + optionMap);
  
  LOGGER.info("The Option Map returned is :- " + optionMap);
  
  return optionMap;
 }
 
 /**
  * This method is responsible for fetching the HBSS Path for rendering from the cache
  * 
  * @param request
  *         SlingHttpServletRequest instance
  * @param viewType
  *         The component viewType
  * @return
  */
 public String fetchTemplatePathForRendering(final SlingHttpServletRequest request, String viewType) {
  LOGGER.info("Entering fetchTemplatePathForRendering Method");
  ResourceResolver resourceResolver = request.getResourceResolver();
  StringBuilder componentViewBasePath = new StringBuilder();
  String componentName;
  String tenantName = FilterUtils.getTenantNameFromRequest(request);
  String[] selectorArray = request.getRequestPathInfo().getSelectors();
  String finalTemplatePath = null;
  componentViewBasePath.append(componentTemplateBasePath);
  if (selectorArray.length > 1 && "displayOptions".equals(selectorArray[0])) {
   
   componentName = selectorArray[1];
   LOGGER.debug("Component name caught in selector :- " + componentName);
  } else {
   ObjectMapper objectMapper = new ObjectMapper();
   Map<String, Object> valueMap = request.getResource().adaptTo(ValueMap.class);
   Map<String, Object> map = objectMapper.convertValue(valueMap, Map.class);
   String compNameForService = MapUtils.getString(map, "componentNameForService");
   if (StringUtils.isNotEmpty(compNameForService) && compNameForService != null) {
    componentName = compNameForService;
   } else {
    componentName = WCMUtils.getComponent(request.getResource()).getName();
   }
   
   LOGGER.debug("Getting Component name from WCMUtils:- " + componentName);
  }
  componentViewBasePath.append(CommonConstants.FORWARD_SLASH + componentName);
  componentViewBasePath.replace(componentViewBasePath.indexOf(CommonConstants.iea),
    componentViewBasePath.indexOf(CommonConstants.iea) + CommonConstants.iea.length(), tenantName);
  finalTemplatePath = SlingUtils.getTemplatePath(resourceResolver, componentViewBasePath.toString(), viewType);
  if (StringUtils.isEmpty(finalTemplatePath)) {
   componentViewBasePath.replace(componentViewBasePath.indexOf(tenantName), componentViewBasePath.indexOf(tenantName) + tenantName.length(),
     CommonConstants.iea);
   LOGGER.debug("The component view path specific to tenant " + tenantName + CommonConstants.HYPHEN + componentViewBasePath);
   finalTemplatePath = SlingUtils.getTemplatePath(resourceResolver, componentViewBasePath.toString(), viewType);
  }
  LOGGER.info("The base template view path is " + componentViewBasePath);
  return finalTemplatePath;
 }
 
 /**
  * Method to initialize the configuration properties which can be changed from admin console.
  * 
  * @param context
  *         - Component context which holds objects to get the configuration property from.
  */
 protected void activate(final ComponentContext context) {
  componentTemplateBasePath = (String) context.getProperties().get("componentViewBasePath");
 }
}
