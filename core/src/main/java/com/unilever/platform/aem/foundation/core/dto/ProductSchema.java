/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.dto;

import java.util.Date;

/**
 * The Class ProductSchema.
 */
public class ProductSchema extends IndexDTO {
 
 /** The publish date. */
 private Date publishDate;
 
 /** The sku. */
 private String sku;
 
 /** The Identifier type. */
 private String identifierType;
 
 /** The Identifier value. */
 private String identifierValue;
 
 /**
  * Instantiates a new product schema.
  */
 public ProductSchema() {
  super();
 }
 
 /**
  * Gets the identifier type.
  *
  * @return the identifier type
  */
 public String getIdentifierType() {
  return identifierType;
 }
 
 /**
  * Sets the identifier type.
  *
  * @param identifierType
  *         the new identifier type
  */
 public void setIdentifierType(String identifierType) {
  this.identifierType = identifierType;
 }
 
 /**
  * Gets the identifier value.
  *
  * @return the identifier value
  */
 public String getIdentifierValue() {
  return identifierValue;
 }
 
 /**
  * Sets the identifier value.
  *
  * @param identifierValue
  *         the new identifier value
  */
 public void setIdentifierValue(String identifierValue) {
  this.identifierValue = identifierValue;
 }
 
 /**
  * Gets the sku.
  *
  * @return the sku
  */
 public String getSKU() {
  return sku;
 }
 
 /**
  * Sets the sku.
  *
  * @param sKU
  *         the new sku
  */
 public void setSKU(String sKU) {
  sku = sKU;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.dto.IndexDTO#getPublishDate()
  */
 public Date getPublishDate() {
  return publishDate;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.dto.IndexDTO#setPublishDate(java.util.Date)
  */
 public void setPublishDate(Date date) {
  this.publishDate = date;
 }
 
}
