/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * The Class Url.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "value" })
public class Url {
 
 /** The value. */
 @XmlSchemaType(name = "anyURI")
 @XmlValue
 private String value;
 
 /** The locale. */
 @XmlAttribute(name = "locale", required = true)
 private String locale;
 
 /**
  * Instantiates a new url.
  */
 public Url() {
 }
 
 /**
  * Gets the value.
  *
  * @return the value
  */
 public String getValue() {
  return value;
 }
 
 /**
  * Sets the value.
  *
  * @param value
  *         the new value
  */
 public void setValue(String value) {
  this.value = value;
 }
 
 /**
  * Gets the locale.
  *
  * @return the locale
  */
 public String getLocale() {
  return locale;
 }
 
 /**
  * Sets the locale.
  *
  * @param locale
  *         the new locale
  */
 public void setLocale(String locale) {
  this.locale = locale;
 }
}
