/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 * The Class ExistsGetter.
 */
public class ExistsGetter extends StringGetter {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.StringGetter#createFrom(org.json.JSONObject)
  */
 public ElementGetter createFrom(JSONObject configuration) {
  if (configuration.has("selector")) {
   return this.withSelector(configuration.getString("selector"));
  }
  if (configuration.has("attribute")) {
   return this.withAttribute(configuration.getString("attribute"));
  }
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.StringGetter#get(org.jsoup.nodes.Element)
  */
 public Object get(Node node) {
  Elements elements = findAll(node, this.selector);
  if (CollectionUtils.isEmpty(elements)) {
   if (StringUtils.isBlank(this.attribute)) {
    return true;
   }
   return elements.get(0).hasAttr(this.attribute);
  }
  return false;
  
 }
 
}
