/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * The AtomFeed Class
 */
public class AtomFeed extends AbstractFeed {
 /**
  * 
  * @param req
  * @param resp
  */
 public AtomFeed(SlingHttpServletRequest req) {
  super(req);
 }
 
 /**
  * Prints the XMl header for ATOM feed
  * 
  * @param properties
  *         the properties
  */
 public void printHeader(ValueMap properties) {
  initXml();
  Document doc = this.getDoc();
  this.rootElement = doc.createElement("feed");
  rootElement.setAttribute("xmlns", "http://www.w3.org/2005/Atom");
  doc.appendChild(rootElement);
  Element property = doc.createElement("link");
  property.appendChild(doc.createTextNode(getHtmlLink()));
  this.rootElement.appendChild(property);
  if (properties != null) {
   printNodeValueMap(properties, this.rootElement);
  }
 }
 
 /**
  * Prints the XMl Child Elements for Atom feed
  * 
  * @param properties
  *         the properties
  */
 public void printEntry(ValueMap properties) {
  Document doc = this.getDoc();
  Element item = doc.createElement("entry");
  this.rootElement.appendChild(item);
  Element link = doc.createElement("link");
  link.appendChild(doc.createTextNode(getHtmlLink()));
  item.appendChild(link);
  Element guid = doc.createElement("id");
  guid.appendChild(doc.createTextNode(getFeedLink()));
  item.appendChild(guid);
  if (properties != null) {
   printNodeValueMap(properties, item);
  }
 }
 
 /**
  * @return the content type
  */
 public String getContentType() {
  return ATOM_CONTENT_TYPE;
 }
 
 /**
  * @return the feed link
  */
 public String getFeedLink() {
  return getUrlPrefix() + getNodePath() + "." + SELECTOR_FEED + SUFFIX_XML;
 }
 
}
