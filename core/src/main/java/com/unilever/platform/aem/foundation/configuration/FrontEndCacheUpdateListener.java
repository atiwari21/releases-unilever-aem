/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;

/**
 * The listener interface for receiving UI configUpdate events. The class that is interested in processing a configUpdate event implements this
 * interface, and the object created with that class is registered with a component using the component's <code>addConfigUpdateListener<code> method.
 * When the configUpdate event occurs, that object's appropriate method is invoked.
 *
 * @see ConfigUpdateEvent
 */
@Component(configurationFactory = true, label = "Unilever Front end Cache Update Service", description = "Unilever Front end Cache update Listener", metatype = true, immediate = true, enabled = true)
@Service(value = EventHandler.class)
@Properties({ @Property(name = EventConstants.EVENT_TOPIC, value = { SlingConstants.TOPIC_RESOURCE_CHANGED }, propertyPrivate = true),
        @Property(name = EventConstants.EVENT_FILTER, value = {
                "(|(path=/etc/ui/*/clientlibs/core/core/config/breakpoints.js)(path=/etc/ui/*/clientlibs/core/core/config/breakpoint.json))" }, propertyPrivate = true) })
public class FrontEndCacheUpdateListener implements EventHandler {
    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontEndCacheUpdateListener.class);
    
    /** The configuration service. */
    @Reference
    FrontEndConfigurationService configurationService;

    
    /*
     * (non-Javadoc)
     * 
     * @see org.osgi.service.event.EventHandler#handleEvent(org.osgi.service.event.Event)
     */
    @Override
    public void handleEvent(Event event) {
        String propPath = (String) event.getProperty(SlingConstants.PROPERTY_PATH);
        LOGGER.debug("propPath " + propPath);
        String tenant = getTenant(propPath);
        configurationService.inValidateCache(tenant);
    }
    
    private String getTenant(String propPath) {
        String tenant = StringUtils.EMPTY;
        try {
            String[] pathArr = propPath.split(CommonConstants.SLASH_CHAR_LITERAL);
            tenant = pathArr[3];
        } catch (ArrayIndexOutOfBoundsException e) {
            LOGGER.error("propPath " + propPath,e);
        }
        return tenant;
        
    }
}
