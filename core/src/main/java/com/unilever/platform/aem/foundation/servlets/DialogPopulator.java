/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.platform.iea.aem.core.cache.ComponentViewAndViewHelperCache;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.core.exception.SystemException;
import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.service.TemplateView;

/**
 * 
 * This class is used to generate the JSON for the incoming request. It looks for the files named displayAs_<viewNames>.html and creates a key value
 * pair out of it.
 * 
 * Input:- A configurable componentViewBasePath under which to search for the adaptive views to create the JSON Output: A JSON containing the
 * key:value pairs.
 * 
 *
 * 
 * Note: Supports only html.
 */
@SlingServlet
@Properties({ @Property(name = org.osgi.framework.Constants.SERVICE_DESCRIPTION, value = "Populating dialogs using JSON"),
  @Property(propertyPrivate = true, name = SearchConstants.SLING_SERVLET_RESOURCETYPE, value = SearchConstants.SLING_SERVLET_RESOURCETYPE_VALUE),
  @Property(propertyPrivate = true, name = SearchConstants.SLING_SERVLET_SELECTOR, value = "displayFeatureOptions"),
  @Property(propertyPrivate = true, name = SearchConstants.SLING_SERVLET_EXTENSION, value = "json") })
public class DialogPopulator extends SlingSafeMethodsServlet {
 
 /**
  * logger object.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(DialogPopulator.class);
 /**
  * Variable to hold serial version ID.
  */
 private static final long serialVersionUID = 1L;
 
 /**
  * Variable to hold the component context.
  */
 
 /**
  * Reference for TemplateView.
  */
 @Reference
 private TemplateView templateView;
 
 @Reference
 ComponentViewAndViewHelperCache componentViewAndViewHelperCache;
 
 /**
  * This method is used to set componentContext in the instance variable.
  * 
  * @param componentContext
  *         The component context object.
  */
 /**
  * commented as a part of sonar fix. The object componentContext not being used in the code
  **/
 
 /*
  * 
  * This method is used to get the JSON based on the displayAs keys present under the parent node.
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache .sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  response.setContentType(CommonConstants.APP_JSON);
  response.setCharacterEncoding(CommonConstants.UTF);
  StringBuilder componentViewBasePath = new StringBuilder();
  
  Map<String, String> optionMap = templateView.fetchComponentViewBasePathMap(request);
  
  LOGGER.debug("For path :- " + componentViewBasePath.toString() + "\nThe optionMap obtained are :- " + optionMap);
  
  Writer out = response.getWriter();
  JSONWriter writer = new JSONWriter(out);
  
  try {
   
   Iterator<Entry<String, String>> it = (optionMap.entrySet()).iterator();
   writer.array();
   while (it.hasNext()) {
    Entry<String, String> entry = it.next();
    writer.object();
    writer.key("text").value(entry.getKey());
    writer.key("value").value(entry.getKey());
    writer.endObject();
   }
   writer.endArray();
  } catch (JSONException e) {
   LOGGER.error("RepositoryException occurred " + ExceptionUtils.getFullStackTrace(e));
   throw new SystemException("Error creating JSON for dialog. The option keys got from the path " + componentViewBasePath.toString() + "is"
     + optionMap + "Please check the felix console configuration for componentViewBasePath." + e.getMessage());
  }
  
 }
 
}
