/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.constants.SearchConstants;
import com.unilever.platform.aem.foundation.core.service.SolrConfigurationService;

/**
 * The Class SolrSearchSuggestionService.
 */
@SlingServlet(label = "Unilver Search - Solr Suggestion Servlet", methods = { org.apache.sling.api.servlets.HttpConstants.METHOD_GET }, paths = {
  SolrSearchSuggestionService.SERVLET_PATH })
public class SolrSearchSuggestionService extends SlingAllMethodsServlet {
 
 /** The solr config. */
 @Reference
 private SolrConfigurationService solrConfig;
 
 protected static final String SERVLET_PATH = "/bin/search/suggestionhandler";
 
 private static final long serialVersionUID = -6520445102937607753L;
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(SolrSearchSuggestionService.class);
 
 /** The Constant REQ_PARAM_TYPE. */
 public static final String SEARCH_TERM = "q";
 
 public static final String HANDLER = "handler";
 public static final String SEARCH_HANDLER = "/select";
 public static final String LOCALE = "Locale";
 public static final String BRANDNAME = "BrandName";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  LOG.debug("inside search suggestion service");
  response.setContentType("application/json");
  response.setCharacterEncoding("utf-8");
  String[] solrPublishDetails = solrConfig.getSolrPublishServerArray();
  String[] collectionsBrandsList = solrConfig.getSolrCollectionsArray();
  final Map<String, String> requestedfields = getSlingResourceType(request);
  
  SolrClient client = new HttpSolrClient(solrPublishDetails[1]);
  // Build query.
  final SolrQuery query = new SolrQuery();
  try {
   query.setRequestHandler(SEARCH_HANDLER);
   String param = ClientUtils
     .escapeQueryChars(new String(requestedfields.get(SEARCH_TERM).getBytes(SearchConstants.ISO_8859_1), SearchConstants.UTF_8));
   final String brandName = new String(requestedfields.get(BRANDNAME).getBytes(SearchConstants.UTF_8), SearchConstants.UTF_8);
   final String locale = new String(requestedfields.get(LOCALE).getBytes(SearchConstants.UTF_8), SearchConstants.UTF_8);
   query.setQuery("*:*");
   query.addFilterQuery(SearchConstants.LOCALECOLON + locale);
   query.addFilterQuery(SearchConstants.BRANDNAMECOLON + brandName);
   query.add("indent", "on");
   query.add("rows", "0");
   query.add("fl", "SearchSuggestions");
   query.add("facet", "true");
   query.add("facet.mincount", "1");
   query.add("facet.field", "SearchSuggestions");
   query.add("facet.prefix", param.toLowerCase());
   String collection = "";
   collection = getCollectionName(collectionsBrandsList, requestedfields);
   final QueryResponse queryResponse = client.query(collection, query);
   
   if (null != queryResponse) {
    String json = "";
    
    List<FacetField> spellCheckResponse = queryResponse.getFacetFields();
    ObjectMapper mapper = new ObjectMapper();
    
    if (null != spellCheckResponse && !spellCheckResponse.isEmpty()) {
     LOG.debug("found suggestions successfully");
     List<FacetField> map = spellCheckResponse;
     List<String> updatedSuggestor = new ArrayList<String>();
     
     /* Suggest only words not the sentences */
     Iterator<FacetField> itr = map.iterator();
     while (itr.hasNext()) {
      FacetField suggestedSentence = itr.next();
      Iterator<Count> itrString = suggestedSentence.getValues().iterator();
      while (itrString.hasNext()) {
       Count suggestedWord = itrString.next();
       /*
        * \S*$ means a space, followed by some non-space characters, followed by end of string. (That is, replace the characters after the last
        * space.)
        */
       String word = StringUtils.capitalize(suggestedWord.toString().replaceAll("\\S*$", "").trim());
       if (!updatedSuggestor.contains(word)) {
        updatedSuggestor.add(word);
       }
      }
     }
     if (!updatedSuggestor.isEmpty()) {
      Collections.sort(updatedSuggestor);
     }
     json = mapper.writeValueAsString(updatedSuggestor);
     LOG.debug("found {} suggestions successfully", json);
     response.getWriter().write(json);
    }
   }
  } catch (SolrServerException e) {
   LOG.error("Exception in accessing  solr server .............." + ExceptionUtils.getStackTrace(e));
  } finally {
   client.close();
  }
 }
 
 /**
  * Obtains the sling:resourceType from the request.
  *
  * @param request
  *         the request
  * @return the sling:resourceType on success and an empty string otherwise.
  */
 protected Map<String, String> getSlingResourceType(SlingHttpServletRequest request) {
  
  final Map requestMap = request.getParameterMap();
  Map<String, String> requestedData = new HashMap<String, String>();
  requestedData.put(SEARCH_TERM, requestMap.containsKey(SEARCH_TERM) ? request.getParameter(SEARCH_TERM) : "");
  requestedData.put(LOCALE, requestMap.containsKey(LOCALE) ? request.getParameter(LOCALE) : "");
  requestedData.put(BRANDNAME, requestMap.containsKey(BRANDNAME) ? request.getParameter(BRANDNAME) : "");
  return requestedData;
  
 }
 
 /**
  * Gets the collection.
  *
  * @param collectionsBrandsArray
  *         the collections brands array
  * @param collection
  *         the collection
  * @param path
  *         the path
  * @return the collection
  */
 private String getCollectionName(String[] collectionsBrandsList, Map<String, String> requestedfields) {
  String collection = "";
  String defaultCollection = "collection1";
  for (String collectionName : collectionsBrandsList) {
   StringTokenizer str1 = new StringTokenizer(collectionName, ":");
   while (str1.hasMoreTokens()) {
    String collectionBrand = str1.nextToken();
    String collectionLocale = str1.nextToken();
    collection = str1.nextToken();
    if (requestedfields.get(LOCALE).equalsIgnoreCase(collectionLocale) && requestedfields.get(BRANDNAME).equalsIgnoreCase(collectionBrand)) {
     return collection;
    }
   }
  }
  return defaultCollection;
 }
 
 /**
  * @return
  */
 public static String getServletPath() {
  
  return SERVLET_PATH;
 }
 
}
