/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.commerce.api;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.cq.commerce.api.Product;
import com.day.cq.commons.ImageResource;
import com.day.cq.tagging.Tag;

/**
 * The Interface UNIProduct.
 */
public interface UNIProduct extends Product {
 
 /*
  * (non-Javadoc)
  *
  * @see com.adobe.cq.commerce.api.Product#getTitle()
  */
 @Override
 public String getTitle();
 
 /*
  * (non-Javadoc)
  *
  * @see com.adobe.cq.commerce.api.Product#getSKU()
  */
 @Override
 public String getSKU();
 
 /**
  * Gets the name.
  *
  * @return the name
  */
 public String getName();
 
 /*
  * (non-Javadoc)
  *
  * @see com.adobe.cq.commerce.api.Product#getDescription()
  */
 @Override
 public String getDescription();
 
 /**
  * Gets the subcategory.
  *
  * @return the subcategory
  */
 public String getCategory();
 
 /**
  * Gets the production date.
  *
  * @return the production date
  */
 public Date getProductionDate();
 
 /**
  * Gets the brand name.
  *
  * @return the brand name
  */
 public String getSize();
 
 /**
  * Gets the allergy.
  *
  * @return the allergy
  */
 public String getAllergy();
 
 /**
  * Gets the ingredients.
  *
  * @return the ingredients
  */
 public String getIngredients();
 
 /**
  * Gets the offer price.
  *
  * @return the offer price
  */
 public String getPrice();
 
 /**
  * Gets the identifier type.
  *
  * @return the identifier type
  */
 public String getIdentifierType();
 
 /**
  * Gets the identifier value.
  *
  * @return the identifier value
  */
 public String getIdentifierValue();
 
 /**
  * Gets the page title.
  *
  * @return the page title
  */
 public String getPageTitle();
 
 /**
  * Gets the short page description.
  *
  * @return the short page description
  */
 public String getShortPageDescription();
 
 /**
  * Gets the smart product id.
  *
  * @return the smart product id
  */
 public String getSmartProductId();
 
 /**
  * Checks if is pack.
  *
  * @return true, if is pack
  */
 public boolean isPack();
 
 /**
  * Gets the child products.
  *
  * @return the child products
  */
 public List<UNIProduct> getChildProducts();
 
 /**
  * Gets the tags.
  *
  * @param resourceResolver
  *         the resource resolver
  * @return the tags
  */
 public Tag[] getTags(ResourceResolver resourceResolver);
 
 /*
  * (non-Javadoc)
  *
  * @see com.adobe.cq.commerce.api.Product#getImages()
  */
 /*
  * return the list of images in the order they are in crx
  */
 @Override
 public List<ImageResource> getImages();
 
 /**
  * Gets the custom product.
  *
  * @return the custom product
  */
 public CustomProduct getCustomProduct();
 
 /**
  * Gets the nutritional facts.
  *
  * @return the nutritional facts
  */
 public Map<String, Object> getNutritionalFacts();
 
 /**
  * Gets the unique id.
  *
  * @return the unique id
  */
 public String getUniqueID();
 
 /**
  * Gets the short identifier value.
  *
  * @return the short identifier value
  */
 public String getShortIdentifierValue();
 
 /**
  * Gets the ils product name.
  *
  * @return the ils product name
  */
 public String getIlsProductName();
 
 /**
  * Gets the label insight id.
  *
  * @return the label insight id
  */
 public String getLabelInsightId();
 
 /**
  * Gets the product size.
  *
  * @return the product size
  */
 public String getProductSize();
 
 /**
  * Gets the about this product title.
  *
  * @return the about this product title
  */
 public String getAboutThisProductTitle();
 
 /**
  * Gets the about this product bullets.
  *
  * @return the about this product bullets
  */
 public String getAboutThisProductBullets();
 
 /**
  * Gets the about this product description.
  *
  * @return the about this product description
  */
 public String getAboutThisProductDescription();
 
 /**
  * Gets the perfect for title.
  *
  * @return the perfect for title
  */
 public String getPerfectForTitle();
 
 /**
  * Gets the perfect for headings and description.
  *
  * @return the perfect for headings and description
  */
 public String getPerfectForHeadingsAndDescription();
 
 /**
  * Gets the how to use title.
  *
  * @return the how to use title
  */
 public String getHowToUseTitle();
 
 /**
  * Gets the how to use description.
  *
  * @return the how to use description
  */
 public String getHowToUseDescription();
 
 /**
  * Gets the try this title.
  *
  * @return the try this title
  */
 public String getTryThisTitle();
 
 /**
  * Gets the try this desctiption.
  *
  * @return the try this desctiption
  */
 public String getTryThisDesctiption();
 
 /**
  * Gets the ingredients disclaimer.
  *
  * @return the ingredients disclaimer
  */
 public String getIngredientsDisclaimer();
 
 /**
  * Gets the short product description.
  *
  * @return the short product description
  */
 public String getShortProductDescription();
 
 /**
  * Gets the product image.
  *
  * @return the product image
  */
 public String getProductImage();
 
 /**
  * Gets the alt text images.
  *
  * @return the alt text images
  */
 public String getAltTextImages();
 
 /**
  * Gets the image type.
  *
  * @return the image type
  */
 public String getImageType();
 
 /**
  * Gets the EA nparent.
  *
  * @return the EA nparent
  */
 public String getEANparent();
 
 /**
  * Gets the long page description.
  *
  * @return the long page description
  */
 public String getLongPageDescription();
 
 /**
  * Gets the cq tags.
  *
  * @return the cq tags
  */
 public String getCqTags();
 
 /**
  * Gets the BIN Settings.
  *
  * @return the BIN Settings
  */
 public String disableBuyItNow();
 
 public String getRewardAvailableFrom();
 
 public String getRewardAvailableTo();
 
 public String getQuantity();
 
 /**
  * Gets the awards.
  *
  * @param awardNamespace
  *         the award namespace
  * @return the awards
  */
 public List<String> getAwards(String awardNamespace);
 
}
