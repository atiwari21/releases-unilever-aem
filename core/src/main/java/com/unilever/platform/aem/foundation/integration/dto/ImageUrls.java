/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class ImageUrls.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "url" })
public class ImageUrls {
 
 /** The url. */
 @XmlElement(name = "Url")
 private List<Url> url;
 
 /**
  * Gets the url.
  *
  * @return the url
  */
 public List<Url> getUrl() {
  if (url == null) {
   url = new ArrayList<Url>();
  }
  return this.url;
 }
 
 /**
  * Sets the url.
  *
  * @param url
  *         the new url
  */
 public void setUrl(List<Url> url) {
  this.url = url;
 }
}
