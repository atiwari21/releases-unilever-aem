/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.util.Dictionary;
import java.util.Map;

import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Interface GlobalConfiguration.
 */
public interface GlobalConfiguration {
 
 /**
  * Gets the page configurations.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @return the page configurations
  */
 public Map<String, Map<String, String>> getPageConfigurations(BrandMarketBean brandMarketBean);
 
 /**
  * Gets the category configuration.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @param category
  *         the category
  * @return the category configuration
  */
 public Map<String, String> getCategoryConfiguration(BrandMarketBean brandMarketBean, String category);
 
 /**
  * Gets the config value.
  *
  * @param brandMarketBean
  *         the brand market bean
  * @param category
  *         the category
  * @param key
  *         the key
  * @return the config value
  */
 public String getConfigValue(BrandMarketBean brandMarketBean, String category, String key);
 
 /**
  * Gets the config value.
  *
  * @param key
  *         the key
  * @return the config value
  */
 public String getConfigValue(String key);
 
 /**
  * Gets the config value array.
  *
  * @param key
  *         the key
  * @return the config value array
  */
 public String[] getConfigValueArray(String key);
 
 /**
  * Gets the common global config value array.
  *
  * @param key
  *         the key
  * @return the common global config value array
  */
 public String[] getCommonGlobalConfigValueArray(String key);
 
 /**
  * Gets the config map.
  *
  * @return the config map
  */
 @SuppressWarnings("rawtypes")
 public Dictionary getConfigMap();
 
 /**
  * Value of.
  *
  * @param <K>
  *         the key type
  * @param <V>
  *         the value type
  * @param dictionary
  *         the dictionary
  * @return the map
  */
 public <K, V> Map<K, V> valueOf(Dictionary<K, V> dictionary);
 
 /**
  * Gets the common global config value.
  *
  * @param key
  *         the key
  * @return config value
  */
 public String getCommonGlobalConfigValue(String key);
 
 /**
  * Gets the common global config map.
  *
  * @return the common global config map
  */
 @SuppressWarnings("rawtypes")
 public Dictionary getCommonGlobalConfigMap();
 
}
