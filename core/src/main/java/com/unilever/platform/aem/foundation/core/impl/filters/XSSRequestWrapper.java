/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.impl.filters;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The Class XSSRequestWrapper.
 */
public class XSSRequestWrapper extends HttpServletRequestWrapper {
 
 /** Logger Instance. */
 private static final Logger LOGGER = LoggerFactory.getLogger(XSSRequestWrapper.class);
 
 /**
  * Instantiates a new XSS request wrapper.
  *
  * @param servletRequest
  *         the servlet request
  */
 public XSSRequestWrapper(final HttpServletRequest servletRequest) {
  super(servletRequest);
 }
 
 /**
  * Gets the patterns.
  *
  * @return the patterns
  */
 private Pattern[] getPatterns() {
  Pattern[] patterns = null;
  DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
  try {
   DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
   Document doc = dBuilder.parse(getPatternStream());
   doc.getDocumentElement().normalize();
   NodeList nList = doc.getElementsByTagName("pattern");
   patterns = new Pattern[nList.getLength()];
   
   for (int temp = 0; temp < nList.getLength(); temp++) {
    Node nNode = nList.item(temp);
    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
     Element eElement = (Element) nNode;
     String patternString = rebuildPattern(eElement.getAttribute("value"));
     boolean isCaseInSensitive = Boolean.parseBoolean(eElement.getElementsByTagName("IsCaseInSensitive").item(0).getTextContent());
     boolean enableCanonicalEquivalence = Boolean.parseBoolean(eElement.getElementsByTagName("EnableCanonicalEquivalence").item(0).getTextContent());
     boolean permitComments = Boolean.parseBoolean(eElement.getElementsByTagName("PermitComments").item(0).getTextContent());
     boolean enableDOTALLMode = Boolean.parseBoolean(eElement.getElementsByTagName("EnableDOTALLMode").item(0).getTextContent());
     boolean enableLiteralParsing = Boolean.parseBoolean(eElement.getElementsByTagName("EnableLiteralParsing").item(0).getTextContent());
     boolean enableMultiLineMode = Boolean.parseBoolean(eElement.getElementsByTagName("EnableMultiLineMode").item(0).getTextContent());
     boolean enableUnicodeCase = Boolean.parseBoolean(eElement.getElementsByTagName("EnableUnicodeCase").item(0).getTextContent());
     boolean enableUnicodeCharacterClass = Boolean
       .parseBoolean(eElement.getElementsByTagName("EnableUnicodeCharacterClass").item(0).getTextContent());
     boolean enableUniXLines = Boolean.parseBoolean(eElement.getElementsByTagName("EnableUniXLines").item(0).getTextContent());
     int patternFlags = 0;
     
     if (isCaseInSensitive) {
      if (patternFlags != 0) {
       patternFlags = Pattern.CASE_INSENSITIVE;
      } else {
       patternFlags = patternFlags | Pattern.CASE_INSENSITIVE;
      }
     }
     
     if (enableCanonicalEquivalence) {
      if (patternFlags != 0) {
       patternFlags = Pattern.CANON_EQ;
      } else {
       patternFlags = patternFlags | Pattern.CANON_EQ;
      }
     }
     
     if (permitComments) {
      if (patternFlags != 0) {
       patternFlags = Pattern.COMMENTS;
      } else {
       patternFlags = patternFlags | Pattern.COMMENTS;
      }
     }
     
     if (enableDOTALLMode) {
      if (patternFlags != 0) {
       patternFlags = Pattern.DOTALL;
      } else {
       patternFlags = patternFlags | Pattern.DOTALL;
      }
     }
     
     if (enableLiteralParsing) {
      if (patternFlags != 0) {
       patternFlags = Pattern.LITERAL;
      } else {
       patternFlags = patternFlags | Pattern.LITERAL;
      }
     }
     
     if (enableMultiLineMode) {
      if (patternFlags != 0) {
       patternFlags = Pattern.MULTILINE;
      } else {
       patternFlags = patternFlags | Pattern.MULTILINE;
      }
     }
     
     if (enableUnicodeCase) {
      if (patternFlags != 0) {
       patternFlags = Pattern.UNICODE_CASE;
      } else {
       patternFlags = patternFlags | Pattern.UNICODE_CASE;
      }
     }
     
     if (enableUnicodeCharacterClass) {
      if (patternFlags != 0) {
       patternFlags = Pattern.UNICODE_CHARACTER_CLASS;
      } else {
       patternFlags = patternFlags | Pattern.UNICODE_CHARACTER_CLASS;
      }
     }
     
     if (enableUniXLines) {
      if (patternFlags != 0) {
       patternFlags = Pattern.UNIX_LINES;
      } else {
       patternFlags = patternFlags | Pattern.UNIX_LINES;
      }
     }
     if (patternFlags == 0) {
      patterns[temp] = Pattern.compile(patternString);
     } else {
      patterns[temp] = Pattern.compile(patternString, patternFlags);
     }
    }
   }
   
  } catch (Exception e) {
   LOGGER.error("Error while compiling patterns : ", e);
  }
  return patterns;
 }
 
 /**
  * Rebuild pattern.
  *
  * @param attribute
  *         the attribute
  * @return the string
  */
 private String rebuildPattern(String attribute) {
  attribute.replaceAll("&quot;", "\"");
  attribute.replaceAll("&lt;", "<");
  attribute.replaceAll("&gt;;", ">");
  return attribute;
 }
 
 /**
  * Gets the pattern stream.
  *
  * @return the pattern stream
  */
 private InputStream getPatternStream() {
  return this.getClass().getClassLoader().getResourceAsStream("filter-pattern/filter-pattern.xml");
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.ServletRequestWrapper#getParameterValues(java.lang.String)
  */
 @Override
 public String[] getParameterValues(final String parameter) {
  
  final String[] values = super.getParameterValues(parameter);
  if (values == null) {
   return ArrayUtils.EMPTY_STRING_ARRAY;
  }
  
  final int count = values.length;
  
  final String[] encodedValues = new String[count];
  for (int i = 0; i < count; i++) {
   encodedValues[i] = stripXSS(values[i]);
  }
  
  return encodedValues;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.ServletRequestWrapper#getParameterNames()
  */
 @Override
 public Enumeration getParameterNames() {
  Enumeration e = super.getParameterNames();
  List arrayList = new ArrayList();
  while (e.hasMoreElements()) {
   String paramName = (String) e.nextElement();
   paramName = stripXSS(paramName);
   arrayList.add(paramName);
  }
  Enumeration enumeration = Collections.enumeration(arrayList);
  while (enumeration.hasMoreElements()) {
   String paramName = (String) enumeration.nextElement();
  }
  return enumeration;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.ServletRequestWrapper#getParameter(java.lang.String)
  */
 @Override
 public String getParameter(final String parameter) {
  
  final String value = super.getParameter(parameter);
  return stripXSS(value);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.http.HttpServletRequestWrapper#getHeader(java.lang.String)
  */
 @Override
 public String getHeader(final String name) {
  
  final String value = super.getHeader(name);
  return stripXSS(value);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.http.HttpServletRequestWrapper#getRequestURI()
  */
 @Override
 public String getRequestURI() {
  
  final String value = super.getRequestURI();
  return stripXSS(value);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see javax.servlet.http.HttpServletRequestWrapper#getRequestURL()
  */
 @Override
 public StringBuffer getRequestURL() {
  
  final StringBuffer value = super.getRequestURL();
  return new StringBuffer(stripXSS(value.toString()));
 }
 
 /**
  * Strip xss.
  *
  * @param value
  *         the value
  * @return the string
  */
 public String stripXSS(String value1) {
  String value = value1;
  if (value != null) {
   value = value.intern();
   // Avoid null characters
   value = value.replaceAll("\0", "");
   // Remove all sections that match a pattern
   for (final Pattern scriptPattern : getPatterns()) {
    value = scriptPattern.matcher(value).replaceAll("");
   }
  }
  return value;
 }
}
