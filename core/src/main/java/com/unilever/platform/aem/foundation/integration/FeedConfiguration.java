/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.integration;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

/**
 * The Class FeedConfig.
 *
 */
@Component(metatype = true)
@Service(value = FeedConfiguration.class)
@Properties({
  @Property(unbounded = PropertyUnbounded.ARRAY, name = "feed.ExcludedProperties", value = {
    "" }, label = "Excluded Properties", description = "Properties To Be Excluded From RSS Feed"),
  @Property(unbounded = PropertyUnbounded.ARRAY, name = "feed.RssMappings", value = { "" }, label = "RSS Mappings", description = "RSS Mappings"),
  @Property(unbounded = PropertyUnbounded.ARRAY, name = "feed.AtomMappings", value = {
    "" }, label = "ATOM Mappings", description = "ATOM Mappings") })
public class FeedConfiguration {
 
 /** The excluded properties. */
 protected String[] excludedProperties;
 
 /** The rss mapper. */
 protected Map<String, String> rssMapper = new LinkedHashMap<String, String>();
 
 /** The atom mapper. */
 protected Map<String, String> atomMapper = new LinkedHashMap<String, String>();
 
 /** The child node limit. */
 protected int childNodeLimit;
 
 /** The default values in case no configuration is present. */
 private String[] defaultExcludedProperties = { "" };
 
 /** The default rss mapping. */
 private String[] defaultRssMapping = { "jcr:title=title", "jcr:description=description", "cq:lastReplicated=pubDate" };
 
 /** The default atom mapping. */
 private String[] defaultAtomMapping = { "jcr:title=title", "cq:lastModified=updated", "cq:lastReplicated=published" };
 
 /** The Constant PROPERTY_MAPPINGS. */
 private static final String EXCLUDED_PROPERTIES = "feed.ExcludedProperties";
 
 /** The Constant RSS_MAPPING. */
 private static final String RSS_MAPPING = "feed.RssMappings";
 
 /** The Constant ATOM_MAPPING. */
 private static final String ATOM_MAPPING = "feed.AtomMappings";
 
 /**
  * Activator for the service. Reads default mappings and store them in map.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 protected void activate(ComponentContext ctx) {
  excludedProperties = PropertiesUtil.toStringArray(ctx.getProperties().get(EXCLUDED_PROPERTIES), defaultExcludedProperties);
  
  rssMapper = PropertiesUtil.toMap(ctx.getProperties().get(RSS_MAPPING), defaultRssMapping);
  
  atomMapper = PropertiesUtil.toMap(ctx.getProperties().get(ATOM_MAPPING), defaultAtomMapping);
 }
 
 /**
  * Gets the excluded properties.
  *
  * @return the excluded properties
  */
 public String[] getExcludedProperties() {
  return excludedProperties;
  
 }
 
 /**
  * Gets the rss mapper.
  *
  * @return the rss mapper
  */
 public Map<String, String> getRssMapper() {
  return rssMapper;
 }
 
 /**
  * Gets the atom mapper.
  *
  * @return the atom mapper
  */
 public Map<String, String> getAtomMapper() {
  return atomMapper;
 }
}
