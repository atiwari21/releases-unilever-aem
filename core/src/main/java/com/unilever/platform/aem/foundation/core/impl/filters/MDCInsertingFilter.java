/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.impl.filters;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * The Class MDCInsertingFilter.
 */
@Component(immediate = true, description = "Filter to generate unique id and insert  into MDC.", label = "Unilever MDC Insert Filter", enabled = true)
@Service(Filter.class)
@Properties({ @Property(name = EngineConstants.SLING_FILTER_SCOPE, value = { EngineConstants.FILTER_SCOPE_REQUEST }),
  @Property(name = Constants.SERVICE_VENDOR, value = "unilever"), @Property(name = "filter.order", intValue = -601) })
public class MDCInsertingFilter implements Filter {
 
 /**
  * Label holding request Id variable.
  */
 private static final String REQUEST_ID = "requestID";
 
 /**
  * Logger for the MDCUpdateFilter.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(MDCInsertingFilter.class);
 
 /**
  * Destroy.
  *
  * @see javax.servlet.Filter#destroy()
  */
 @Override
 public void destroy() {
  // Not using this method in this class.
 }
 
 /**
  * This method unique is for the request and set it in the MDC logger to filter out the logs based on the this parameter.
  *
  * @param request
  *         - http servlet request object.
  * @param response
  *         - http servlet response object.
  * @param filterChain
  *         - filter chain object to achieve chaining.
  * @throws IOException
  *          - In case of any IO exception.
  * @throws ServletException
  *          - In case of any system error.
  * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
  */
 @Override
 public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
   throws IOException, ServletException {
  
  if (MDC.get(REQUEST_ID) == null) {
   String httpRequestIdentifier = UUID.randomUUID().toString();
   LOGGER.info("The requestID generated is " + httpRequestIdentifier);
   MDC.put(REQUEST_ID, httpRequestIdentifier);
  }
  
  filterChain.doFilter(request, response);
 }
 
 /**
  * Inits the.
  *
  * @param arg0
  *         filterConfig.
  * @throws ServletException
  *          servletException
  * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
  */
 @Override
 public void init(FilterConfig arg0) throws ServletException {
  // Not using this method in this class.
  
 }
}
