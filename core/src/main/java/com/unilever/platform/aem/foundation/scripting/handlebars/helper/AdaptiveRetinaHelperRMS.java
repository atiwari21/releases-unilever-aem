/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.WCMMode;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Template;
import com.google.common.cache.LoadingCache;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;
import com.unilever.platform.aem.foundation.configuration.FrontEndConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.utils.CacheUtil;

/**
 * This class defines the Adaptive Image Helper for Unilever. It combines the image specific data with the mark up and adds it to the template. :</b>
 * {{adaptiveRetina url isNotAdaptiveImage '490,490,490,490,640,640,460,460' }}
 *
 * @author nbhart
 * 
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class AdaptiveRetinaHelperRMS implements HandlebarsHelperService<Object> {
    
    private FrontEndConfigurationService configurationService;
    /**
     * Logger Instance.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AdaptiveRetinaHelperRMS.class);
    
    /**
     * A singleton instance of this helper.
     */
    private static final Helper<Object> INSTANCE = new AdaptiveRetinaHelperRMS();
    
    /**
     * The helper's name.
     */
    private static final String NAME = "adaptiveRetinaRMS";
    
    /**
     * the constant for ",".
     */
    private static final String COMMA = ",";
    ScriptEngineManager manager = new ScriptEngineManager(null);
    /**
     * Adaptive Image Template Source.
     *
     */
    private static final String TEMPLATE_SOURCE = "<picture class=\"<lazyLoadClass>\">" + "<!--[if IE 9]><video style=\"display: none;\"><![endif]-->"
            + "<source <sourceSrc>=\"tabL, tabL2x 2x\" " + "media=\"screen and (min-width: tabLMinpx) and (max-width: tabLMaxpx)\">"
            + "<source <sourceSrc>=\"tabP, tabP2x 2x\" " + "media=\"screen and (min-width: tabPMinpx) and (max-width: tabPMaxpx)\">"
            + "<source <sourceSrc>=\"mobL, mob2x 2x\" media=\"screen and (min-width: mobMinpx) and (max-width: mobMaxpx)\">"
            + "<source <sourceSrc>=\"desktop\" media=\"screen and (min-width: desktopMinpx) \">" + "<!--[if IE 9]></video><![endif]-->"
            + "<img class=\"<className>\" <title> alt=\"<altText>\" <imageSrc>=\"mobL\"/>" + "</picture>";
    
    /**
     * The integer constant ZERO.
     */
    private static final int ZERO = 0;
    
    /**
     * Apply.
     *
     * @param context
     *            The object for context.
     * @param options
     *            The handle bar options.
     * @return CharSequence
     * @throws IOException
     *             The i/o exception.
     * @see Helper#apply(Object, Options)
     */
    @Override
    public CharSequence apply(final Object context, final Options options) throws IOException {
        
        SlingScriptHelper sling = (SlingScriptHelper) options.context.get("sling");
        configurationService = sling.getService(FrontEndConfigurationService.class);
        Map<String, Object> imageMap = options.param(CommonConstants.ONE);
        int numberOfOptions = options.params.length;
        String[] sizes = StringUtils.split((String) options.param(CommonConstants.TWO), COMMA);
        if (numberOfOptions > CommonConstants.FOUR) {
            String dimensionsString = getValueFromComponentConfig(options, "dimensions");
            sizes = (StringUtils.isNotBlank(dimensionsString)) ? StringUtils.split(dimensionsString, COMMA) : sizes;
        }
        String templateString = setTemplateStringForAdaptiveBehavior(options, imageMap, sizes, context);
        LOGGER.debug("Final Template" + templateString);
        
        Handlebars handlebar = options.handlebars;
        Template handlebarTemplate = handlebar.compileInline(templateString);
        String template = handlebarTemplate.apply(options.context);
        return new Handlebars.SafeString(template);
    }
    
    /**
     * Gets the name.
     *
     * @return String
     * @see HandlebarsHelperService#getName()
     */
    @Override
    public String getName() {
        return NAME;
    }
    
    /**
     * Gets the helper.
     *
     * @return Helper<Object>
     * @see HandlebarsHelperService#getHelper()
     */
    @Override
    public Helper<Object> getHelper() {
        return INSTANCE;
    }
    
    /**
     * Sets the cache.
     *
     * @param templateCache
     *            The object for template cache.
     * @see com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService#setCache
     *      (com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache)
     */
    @Override
    public void setCache(final HandlebarsTemplateCache templateCache) {
        // Not setting the cache instance as it is not required in this helper.
    }
    
    /**
     * This method sets templateString based on adaptive behavior.
     *
     * @param options
     *            the options
     * @param imageMap
     *            the image url
     * @param sizes
     *            the sizes
     * @param context
     * @return the string
     */
    public String setTemplateStringForAdaptiveBehavior(final Options options, final Map<String, Object> imageMap, String[] sizes, Object context) {
        String templateString = null;
        
        String title = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.ZERO), StringUtils.EMPTY);
        LOGGER.debug("Image title is :" + title);
        String altText = context.toString();
        LOGGER.debug("Image altText is :" + altText);
        
        String titleAttribute = StringUtils.EMPTY;
        String className = getClassName(title);
        if (StringUtils.isNotBlank(title)) {
            titleAttribute = "title=\"" + title + "\"";
        }
        
        templateString = TEMPLATE_SOURCE.replace("<altText>", altText);
        templateString = templateString.replace("<className>", className);
        templateString = templateString.replace("<title>", titleAttribute);
        
        templateString = replaceLazyLoadClass(templateString, options);
        templateString = replaceViewPorts(templateString, options);
        
        return replaceSizes(imageMap, templateString, sizes);
    }
    
    /**
     * Returns class name as per FE standard. Following filters are applied: 1. Remove all special characters. 2. Replace all intermediate spaces with
     * "-". 3. Convert all text to lowercase. 4. "c-" is added to the class name at beginning.
     * 
     * @param title
     *            the title
     * @return className
     */
    private String getClassName(String title) {
        String className = title;
        String prefix = "c-";
        if (StringUtils.isNotBlank(className)) {
            className = className.trim().toLowerCase();
            className = className.replaceAll("[^a-zA-Z ]", "");
            className = className.replaceAll("\\s+", "-");
            className = prefix.concat(className);
        }
        return className;
    }
    
    /**
     * Replaces the size constant with actual sizes.
     * 
     * @param imageMap
     *
     * @param templateString
     *            the template string
     * @param sizes
     *            the sizes
     * @return template string with actual image sizes.
     */
    private String replaceSizes(Map<String, Object> imageMap, String templateString, String[] sizes) {
        String template = templateString;
        if (StringUtils.isNotBlank(template) && !ArrayUtils.isEmpty(sizes)) {
            
            int desktopWidth = CommonConstants.ZERO;
            int tabLWidth = CommonConstants.ZERO;
            int tabPWidth = CommonConstants.ZERO;
            int mobLWidth = CommonConstants.ZERO;
            if (sizes.length > CommonConstants.ONE) {
                desktopWidth = Integer.parseInt(sizes[CommonConstants.ZERO]);
            }
            
            if (sizes.length > CommonConstants.THREE) {
                tabLWidth = Integer.parseInt(sizes[CommonConstants.TWO]);
            }
            
            if (sizes.length > CommonConstants.FIVE) {
                tabPWidth = Integer.parseInt(sizes[CommonConstants.FOUR]);
            }
            
            if (sizes.length > CommonConstants.SEVEN) {
                mobLWidth = Integer.parseInt(sizes[CommonConstants.SIX]);
            }
            
            int tabL2xWidth = tabLWidth * CommonConstants.TWO;
            int tabP2xWidth = tabPWidth * CommonConstants.TWO;
            int mob2xWidth = mobLWidth * CommonConstants.TWO;
            
            String desktop = getImageURL(imageMap, desktopWidth);
            String tabL = getImageURL(imageMap, tabLWidth);
            String tabP = getImageURL(imageMap, tabPWidth);
            String mobL = getImageURL(imageMap, mobLWidth);
            String tabL2x = getImageURL(imageMap, tabL2xWidth);
            String tabP2x = getImageURL(imageMap, tabP2xWidth);
            String mob2x = getImageURL(imageMap, mob2xWidth);
            
            template = template.replace("tabL2x", tabL2x);
            template = template.replace("tabP2x", tabP2x);
            template = template.replace("mob2x", mob2x);
            template = template.replace("desktop", desktop);
            template = template.replace("tabL", tabL);
            template = template.replace("tabP", tabP);
            template = template.replace("mobL", mobL);
            
        }
        return template;
    }
    
    /**
     * This method selects the correct url of image acording to it's width. Given a particular width, this method iterates over all availabe set of
     * images in defaultImagesList and originalImageList. Nearest matching image map w.r.t. width is selected and it's URL is returned.
     * 
     * @param imageMap
     *            the map contining images
     * @param width
     *            width of image
     * @param height
     *            height of image
     * @return url of image
     */
    @SuppressWarnings("unchecked")
    private String getImageURL(Map<String, Object> imageMap, int width) {
        List<Map<String, Object>> finalImageList = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> defaultMap = (List<Map<String, Object>>) imageMap.get("default");
        if (CollectionUtils.isNotEmpty(defaultMap)) {
            finalImageList.addAll((List<Map<String, Object>>) imageMap.get("default"));
        }
        List<Map<String, Object>> originalMap = (List<Map<String, Object>>) imageMap.get("original");
        if (CollectionUtils.isNotEmpty(originalMap)) {
            finalImageList.addAll((List<Map<String, Object>>) imageMap.get("original"));
        }
        
        Collections.sort(finalImageList, new Comparator<Map<String, Object>>() {
            public int compare(final Map<String, Object> map1, final Map<String, Object> map2) {
                return getImageWidth(map1) - getImageWidth(map2);
            }
        });
        
        Object[] images = finalImageList.toArray();
        
        Map<String, Object> image = getNearestMatchingImage(images, width);
        return replaceSpace(MapUtils.getString(image, "url", StringUtils.EMPTY));
        
    }
    
    /**
     * This method fills up "%20" in spaces found in file name.
     * 
     * @param fileName
     * @return
     */
    private String replaceSpace(String fileName) {
        String tempFileName = fileName;
        tempFileName.replaceAll(" ", "%20");
        return tempFileName;
    }
    
    /**
     * Returns nearest matching image map w.r.t. width. It picks image whose width is equal (if available), else nearest high value. In case all
     * images have lower width, it picks one with highest width.
     * 
     * @param images
     *            the array of image maps
     * @param width
     *            width of image
     * @param height
     *            height of image
     * @return the nearest matching image map
     */
    private Map<String, Object> getNearestMatchingImage(Object[] images, int width) {
        for (int index = 0; index < images.length; index++) {
            if (width <= getImageWidth((Map<String, Object>) images[index])) {
                return (Map<String, Object>) images[index];
            }
        }
        return images.length > 0 ? (Map<String, Object>) images[images.length - 1] : new LinkedHashMap<String, Object>();
    }
    
    /**
     * Returns width of image.
     * 
     * @param map
     *            the image map
     * @return the width of image
     */
    private int getImageWidth(Map<String, Object> map) {
        return MapUtils.getIntValue(map, "width", ZERO);
    }
    
    /**
     * Sets the lazy loading related attributes.
     * 
     * @param original
     *            template string
     * @param options
     * @return updated template string
     */
    private String replaceLazyLoadClass(String templateString, Options options) {
        String template = templateString;
        String isLazyLoad = null;
        String imageSrc = StringUtils.EMPTY;
        String sourceSrc = StringUtils.EMPTY;
        String lazyLoadClass = StringUtils.EMPTY;
        if (options.params.length > CommonConstants.FOUR) {
            isLazyLoad = getValueFromComponentConfig(options, "isLazyLoad");
        }
        if (StringUtils.isBlank(isLazyLoad) && options.params.length > CommonConstants.THREE) {
            if (options.param(CommonConstants.THREE) instanceof Boolean) {
                isLazyLoad = options.param(CommonConstants.THREE).toString();
            } else {
                isLazyLoad = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.THREE), StringUtils.EMPTY);
            }
        }
        SlingHttpServletRequest request = (SlingHttpServletRequest) options.get(SlingBindings.REQUEST);
        boolean enableLazyLoading = WCMMode.fromRequest(request) == WCMMode.EDIT || WCMMode.fromRequest(request) == WCMMode.DESIGN;
        if (enableLazyLoading || WCMMode.fromRequest(request) == WCMMode.PREVIEW
                || (StringUtils.isNotEmpty(isLazyLoad) && StringUtils.equalsIgnoreCase(isLazyLoad, "false"))) {
            imageSrc = "src";
            sourceSrc = "srcset";
        } else {
            imageSrc = "data-src";
            sourceSrc = "data-srcset";
            lazyLoadClass = "lazy-load is-loading is-lazyload";
        }
        template = template.replace("<sourceSrc>", sourceSrc);
        template = template.replace("<imageSrc>", imageSrc);
        template = template.replace("<lazyLoadClass>", lazyLoadClass);
        return template;
    }
    
    /**
     * Gets the value from component config.
     * 
     * @param options
     *            the options
     * @param key
     *            the key
     * @return the value from component config
     */
    @SuppressWarnings("unchecked")
    private String getValueFromComponentConfig(final Options options, String key) {
        String configValue = null;
        String brandName = CacheUtil.getBrandName(options);
        try {
            LoadingCache<String, Map<String, String>> existingcache = CacheUtil.getBreakpointsdata(options, configurationService);
            Map<String, String> cacheMap = existingcache.get(brandName);
            if(cacheMap!=null && cacheMap.containsKey(brandName)){
            String breakPointData = cacheMap.get(brandName);
            JSONObject dataJson = (new JSONObject(breakPointData)).getJSONObject("data");
            String configSizesName = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.FOUR), StringUtils.EMPTY);
            String[] componentAttributes = StringUtils.split(configSizesName, CommonConstants.DOT_CHAR_LITERAL);
            configValue = getConfigValue(key, configValue, dataJson, componentAttributes);
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException occured ", e);
        } catch (ExecutionException e) {
            LOGGER.error("ExecutionException occured ", e);
        }
        return configValue;
    }

    /**
     * @param key
     * @param configValue
     * @param dataJson
     * @param componentAttributes
     * @return
     * @throws JSONException
     */
    private String getConfigValue(String key, String config, JSONObject dataJson, String[] componentAttributes) throws JSONException {
        JSONObject adaptiveRetinaMap = null;
        JSONObject componentAdaptiveMap = null;
        JSONObject imageMap = null;
        String configValue = config;
        if (dataJson.get(NAME) != null) {
            adaptiveRetinaMap = dataJson.getJSONObject(NAME);
            if (componentAttributes.length > CommonConstants.ZERO) {
                componentAdaptiveMap = (adaptiveRetinaMap.has(componentAttributes[CommonConstants.ZERO]))
                        ? adaptiveRetinaMap.getJSONObject(componentAttributes[CommonConstants.ZERO]) : null;
                if (componentAttributes.length > CommonConstants.ONE && componentAdaptiveMap != null) {
                    imageMap = (componentAdaptiveMap.has(componentAttributes[CommonConstants.ONE]))
                            ? componentAdaptiveMap.getJSONObject(componentAttributes[CommonConstants.ONE]) : null;
                    Object componentAttribute = (imageMap != null) ? imageMap.get(key) : null;
                    configValue = (componentAttribute != null) ? componentAttribute.toString() : StringUtils.EMPTY;
                }
            }
        }
        return configValue;
    }
    
    /**
     * Replaces the brand specific break points in adaptive image tags.
     * 
     * @param templateString
     * @param options
     * @return image template.
     */
    private String replaceViewPorts(String templateStr, Options options) {
        String templateString = templateStr;
        String brandName = CacheUtil.getBrandName(options);
        
        try {
            LoadingCache<String, Map<String, String>> existingcache = CacheUtil.getBreakpointsdata(options, configurationService);
            Map<String, String> cacheMap = existingcache.get(brandName);
            if(cacheMap!=null && cacheMap.containsKey(brandName)){
            String breakPointData = cacheMap.get(brandName);
            JSONObject dataJson = (new JSONObject(breakPointData)).getJSONObject("data");
            JSONObject breakpoints = null;
            if (dataJson.has("breakpoints") || dataJson.has("BREAKPOINTS")) {
                breakpoints = dataJson.has("breakpoints") ? dataJson.getJSONObject("breakpoints") : dataJson.getJSONObject("BREAKPOINTS");
                templateString = templateString.replace("desktopMin", breakpoints.get("desktopMin").toString());
                templateString = templateString.replace("tabLMin", breakpoints.get("tabLMin").toString());
                templateString = templateString.replace("tabLMax", breakpoints.get("tabLMax").toString());
                templateString = templateString.replace("tabPMin", breakpoints.get("tabPMin").toString());
                templateString = templateString.replace("tabPMax", breakpoints.get("tabPMax").toString());
                templateString = templateString.replace("mobMin", breakpoints.get("mobMin").toString());
                templateString = templateString.replace("mobMax", breakpoints.get("mobMax").toString());
            }
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException occured ", e);
        } catch (ExecutionException e) {
            LOGGER.error("ExecutionException occured ", e);
        }
        
        return templateString;
    }
    
}
