/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.context.FieldValueResolver;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.context.MapValueResolver;
import com.github.jknack.handlebars.context.MethodValueResolver;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;
import com.sapient.platform.iea.aem.scripting.handlebars.engine.HandlebarsScriptEngineFactory;

/**
 * The Class GenericHelper.
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class RenderTemplateHelper implements HandlebarsHelperService<Object> {
 
 /**
  * , Logger Instance.
  */
 private static final Logger LOG = LoggerFactory.getLogger(RenderTemplateHelper.class);
 
 /**
  * A singleton instance of this helper.
  */
 private static final Helper<Object> INSTANCE = new RenderTemplateHelper();
 
 /**
  * The helper's name.
  */
 private static final String NAME = "renderTemplate";
 
 /** The Constant FRWD_SLASH. */
 private static final String FRWD_SLASH = "/";
 
 /** The Constant TWO */
 private static final int NUMERIC_TWO = 2;
 /** The Constant THREE */
 private static final int NUMERIC_THREE = 3;
 
 /**
  * The Enum Error.
  */
 public enum Error {
  
  /** The template path not exists. */
  TEMPLATE_PATH_NOT_EXISTS("Error Code : 5001", "Template path does not exists."),
  
  /** The incorrect template user. */
  INCORRECT_TEMPLATE_USER("Error Code : 5002", "Template path given is incorrect.");
  
  /** The code. */
  private final String code;
  
  /** The description. */
  private final String description;
  
  /**
   * Instantiates a new error.
   *
   * @param code
   *         the code
   * @param description
   *         the description
   */
  private Error(String code, String description) {
   this.code = code;
   this.description = description;
  }
  
  /**
   * Gets the description.
   *
   * @return the description
   */
  public String getDescription() {
   return description;
  }
  
  /**
   * Gets the code.
   *
   * @return the code
   */
  public String getCode() {
   return code;
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
   return code + ": " + description;
  }
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.github.jknack.handlebars.Helper#apply(java.lang.Object, com.github.jknack.handlebars.Options)
  */
 @SuppressWarnings("unchecked")
 @Override
 public CharSequence apply(Object path, Options options) throws IOException {
  
  CharSequence chars = null;
  String hbssPath = null;
  Map<String, Object> parentContextData = (Map<String, Object>) options.context.model();
  Map<String, Object> contextData = new HashMap<String, Object>();
  String componentPath = getComponentPath(contextData);
  
  if (path != null) {
   hbssPath = path.toString();
   int index = hbssPath.lastIndexOf("/");
   String folderPath = hbssPath.substring(0, index);
   if (folderPath.contains("-")) {
    String[] folderArr = folderPath.split("-");
    folderPath = "";
    for (String s : folderArr) {
     folderPath = folderPath + s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
    }
    folderPath = folderPath.substring(0, 1).toLowerCase() + folderPath.substring(1, folderPath.length());
   }
   hbssPath = folderPath + "/displayAs_" + hbssPath.substring(index + 1, hbssPath.length());
  }
  
  for (Object entryObj : parentContextData.entrySet()) {
   Map.Entry<?, ?> entry = (Map.Entry<?, ?>) entryObj;
   contextData.put((String) entry.getKey(), entry.getValue());
  }
  
  if (options.param(0) != null) {
   contextData = getContextMAP(contextData, options.param(0));
  }
  
  Map<String, Object> entries = options.hash;
  contextData = ScriptHelperUtils.addParamsToContextMap(contextData, options);
  if (hbssPath == null) {
   hbssPath = getTemplatePath(entries);
  }
  
  for (String key : entries.keySet()) {
   contextData = getContextMAP(contextData, key, entries.get(key).toString());
  }
  
  if (hbssPath != null) {
   LOG.debug("Template path provided in hbss is " + hbssPath);
   chars = getScript(contextData, hbssPath, options, componentPath, options.context);
  } else {
   LOG.error("Error in : " + componentPath + Error.TEMPLATE_PATH_NOT_EXISTS.description);
   chars = Error.TEMPLATE_PATH_NOT_EXISTS.code;
  }
  
  return chars;
  
 }
 
 /**
  * Gets the component path.
  *
  * @param contextData
  *         the context data
  * @return the component path
  */
 private String getComponentPath(Map<String, Object> contextData) {
  String componentPath = "";
  
  Map<String, Object> map = getMapUsingKeyName("data", contextData);
  if (map != null) {
   componentPath = map.get("resourceType").toString();
  }
  return componentPath;
 }
 
 /**
  * Gets the template path.
  *
  * @param entries
  *         the entries
  * @return the template path
  */
 private String getTemplatePath(Map<String, Object> entries) {
  String templatePath = null;
  for (String key : entries.keySet()) {
   if (("templatePath").equals(key)) {
    templatePath = entries.get(key).toString();
   } else {
    key = entries.get(key).toString();
    String hbss = key.substring(key.indexOf(".") + 1, key.length());
    if (("hbss").equals(hbss)) {
     templatePath = key;
    }
   }
  }
  return templatePath;
 }
 
 /**
  * Gets the context map.
  *
  * @param contextMap
  *         the context map
  * @param obj
  *         the obj
  * @return the context map
  */
 @SuppressWarnings("unchecked")
 private Map<String, Object> getContextMAP(Map<String, Object> contextMap, Object obj) {
  
  Map<String, Object> map = null;
  Map<String, Object> contextData = contextMap;
  
  map = getMapUsingKeyName("data", contextData);
  
  if (obj instanceof ArrayList<?>) {
   int i = 0;
   for (Object o : (ArrayList<Object>) obj) {
    if (o instanceof HashMap<?, ?>) {
     contextData.put(Integer.valueOf(i).toString(), o);
    }
    i++;
   }
  } else if (obj instanceof HashMap<?, ?>) {
   map = (Map<String, Object>) obj;
   for (String key : map.keySet()) {
    contextData.put(key, map.get(key));
   }
  } else if (obj instanceof Object[]) {
   int i = 0;
   for (Object o : (Object[]) obj) {
    if (o instanceof HashMap<?, ?>) {
     contextData.put(Integer.valueOf(i).toString(), o);
    }
    i++;
   }
  }
  
  return contextData;
 }
 
 /**
  * Gets the context map.
  *
  * @param contextMap
  *         the context map
  * @param key
  *         the key
  * @param keyValue
  *         the key value
  * @return the context map
  */
 private Map<String, Object> getContextMAP(Map<String, Object> contextMap, String key, String keyValue) {
  
  Map<String, Object> map = null;
  Map<String, Object> contextData = contextMap;
  String[] valueArray = keyValue.split("\\.");
  
  map = getMapUsingKeyName("data", contextData);
  if (valueArray.length == 1) {
   map = getMapUsingKeyName(keyValue, map);
   if (map == null) {
    contextData.put(key, keyValue);
   } else {
    contextData.put(key, map);
   }
  } else if (valueArray.length > 1) {
   for (int i = 0; i < valueArray.length; i++) {
    if (map != null) {
     map = getMapUsingKeyName(valueArray[i], map);
    }
   }
   if (map != null) {
    contextData.put(key, map);
   }
  }
  
  return contextData;
 }
 
 /**
  * Gets the map using key name.
  *
  * @param keyName
  *         the key name
  * @param contextMap
  *         the context map
  * @return the map using key name
  */
 @SuppressWarnings("unchecked")
 private Map<String, Object> getMapUsingKeyName(String keyName, Map<String, Object> contextMap) {
  
  Map<String, Object> map = null;
  for (Map.Entry<String, Object> entry : contextMap.entrySet()) {
   if (keyName.equals(entry.getKey())) {
    map = (Map<String, Object>) entry.getValue();
   }
  }
  return map;
 }
 
 /**
  * Gets the script.
  *
  * @param dataMap
  *         the data map
  * @param templatePath
  *         the template path
  * @param options
  *         the options
  * @param componentPath
  *         the component path
  * @return the script
  */
 private CharSequence getScript(final Map<String, Object> dataMap, final String templatePath, final Options options, String componentPath,
   final Context parentDataMap) {
  
  String slingScriptView = null;
  SlingHttpServletRequest request = (SlingHttpServletRequest) options.get(SlingBindings.REQUEST);
  SlingScriptHelper sling = (SlingScriptHelper) options.get(SlingBindings.SLING);
  if (sling != null) {
   slingScriptView = sling.getScript().getScriptResource().getPath();
   request.setAttribute("slingScriptView", slingScriptView);
  }
  ResourceResolver resourceResolver = request.getResourceResolver();
  
  String scriptPath = getTemplatePath(templatePath, request, resourceResolver);
  LOG.debug("11111111 scriptPath : ",scriptPath);
  String templateMarkup = getTemplateSource(resourceResolver, scriptPath);
  LOG.debug("22222222 templateMarkup : ",templateMarkup);
  if (templateMarkup == null) {
   LOG.error("Error in : " + componentPath + Error.INCORRECT_TEMPLATE_USER.description);
   return Error.INCORRECT_TEMPLATE_USER.code;
  }
  
  LOG.debug("Script path is " + scriptPath);
  LOG.debug("Template markup is " + templateMarkup);
  
  return getCompiledTemplate(dataMap, options, templateMarkup, parentDataMap);
 }
 
 /**
  * Gets the template path.
  *
  * @param path
  *         the path
  * @param sling
  *         the sling
  * @param resourceResolver
  *         the resource resolver
  * @return the template path
  */
 private String getTemplatePath(final String path, final SlingHttpServletRequest request, final ResourceResolver resourceResolver) {
  String scriptPath = "";
  String parentPath = ResourceUtil.getParent((String) request.getAttribute("slingScriptView"));
  for (String sp : resourceResolver.getSearchPath()) {
   if (parentPath.startsWith(sp)) {
    parentPath = parentPath.substring(sp.length());
    break;
   }
  }
  LOG.debug("getTemplatePath path {}",path);
  if (!path.startsWith(FRWD_SLASH)) {
   LOG.debug("if ");
   parentPath = parentPath.substring(0, parentPath.lastIndexOf("/"));
   scriptPath = parentPath + FRWD_SLASH + path;
   LOG.debug("if scriptPath {}",scriptPath);
  } else {
   scriptPath = parentPath + path;
   LOG.debug("else scriptPath {}",scriptPath);
  }
  
  scriptPath = resolveExtension(scriptPath);
  LOG.debug("resolveExtension scriptPath {}",scriptPath);
  if (resourceResolver.getResource(scriptPath) == null) {
   LOG.debug("if (resourceResolver.getResource(scriptPath) == null) ");
   scriptPath = getParentScriptPath(scriptPath, request.getResource());
   LOG.debug("getParentScriptPath {} ",scriptPath);
  }
  return scriptPath;
 }
 
 /**
  * Gets the template source.
  *
  * @param resourceResolver
  *         the resource resolver
  * @param templatePath
  *         the template path
  * @return the template source
  */
 private static String getTemplateSource(final ResourceResolver resourceResolver, final String templatePath) {
  Resource rs = resourceResolver.getResource(templatePath);
  String templateMarkup = "";
  if (rs != null) {
   Node templateNode = (Node) rs.adaptTo(Node.class);
   try {
    Node jcrContent = templateNode.getNode(JcrConstants.JCR_CONTENT);
    InputStream stream = jcrContent.getProperty(JcrConstants.JCR_DATA).getBinary().getStream();
    templateMarkup = IOUtils.toString(stream);
   } catch (RepositoryException repEx) {
    LOG.error("Error Encountered while Obtaining Template Node." + repEx.getMessage() + "Exception : " + repEx);
   } catch (IOException ioExc) {
    LOG.error("Error Encountered while generating template Markup." + ioExc.getMessage() + "Exception : " + ioExc);
   }
  } else {
   return null;
  }
  return templateMarkup;
 }
 
 /**
  * Gets the compiled template.
  *
  * @param dataMap
  *         the data map
  * @param options
  *         the options
  * @param templateMarkup
  *         the template markup
  * @return the compiled template
  */
 private CharSequence getCompiledTemplate(final Map<String, Object> dataMap, final Options options, final String templateMarkup,
   final Context parentDataMap) {
  
  CharSequence chars = "";
  try {
   
   String templateSource = templateMarkup;
   Template hbtemplate = options.handlebars.compileInline(templateSource);
   chars = evaluateTemplate(hbtemplate, dataMap, (SlingHttpServletRequest) options.get(SlingBindings.REQUEST), parentDataMap);
   
  } catch (IOException e) {
   LOG.error("Error Encountered while applying the context data to Template. ", e);
  }
  return chars;
 }
 
 /**
  * Resolve extension.
  *
  * @param uri
  *         the uri
  * @return the string
  */
 private String resolveExtension(final String uri) {
  String suffix = "";
  int lastDotIndex = StringUtils.lastIndexOf(uri, ".");
  if (lastDotIndex == -1 || lastDotIndex < StringUtils.lastIndexOf(uri, FRWD_SLASH)) {
   suffix = "." + HandlebarsScriptEngineFactory.HANDLEBARS_SCRIPT_EXTENSION;
  }
  return uri + suffix;
 }
 
 /**
  * Evaluate template.
  *
  * @param template
  *         the template
  * @param dataMap
  *         the data map
  * @param request
  *         the request
  * @return the char sequence
  */
 private CharSequence evaluateTemplate(final Template template, final Map<String, Object> dataMap, final SlingHttpServletRequest request,
   final Context parentDataMap) {
  String htmlString = "";
  try {
   Map<String, Object> model = new HashMap<String, Object>();
   for (Object entryObj : dataMap.entrySet()) {
    Map.Entry<?, ?> entry = (Map.Entry<?, ?>) entryObj;
    model.put((String) entry.getKey(), entry.getValue());
   }
   model.put(SlingBindings.REQUEST, request);
   Context hbContext = Context.newBuilder(parentDataMap, model)
     .resolver(FieldValueResolver.INSTANCE, MapValueResolver.INSTANCE, JavaBeanValueResolver.INSTANCE, MethodValueResolver.INSTANCE).build();
   htmlString = template.apply(hbContext);
   hbContext.destroy();
  } catch (IOException e) {
   LOG.error("Error Encountered while applying the context data to Template. ", e);
  }
  LOG.info("Exiting evaluateTemplate method");
  return new Handlebars.SafeString(htmlString);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getName()
  */
 @Override
 public String getName() {
  return NAME;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#getHelper()
  */
 @Override
 public Helper<Object> getHelper() {
  return INSTANCE;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.sapient.platform.iea.aem.scripting.handlebars.api. HandlebarsHelperService#setCache(com.sapient.platform.iea.aem.scripting.
  * handlebars.api.HandlebarsTemplateCache)
  */
 @Override
 public void setCache(HandlebarsTemplateCache templateCache) {
  LOG.info("Cache is not implemented for renderTemplate handler.");
 }
 
 /**
  * Gets the parent script path.
  *
  * @param path
  *         the path
  * @param resource
  *         the resource
  * @return the parent script path
  */
 private String getParentScriptPath(String path, Resource resource) {
  String scriptPath = path;
  
  if (resource != null) {
   String superType = resource.getResourceResolver().getParentResourceType(resource);
   String[] superResourceTypeArray = StringUtils.split(superType, FRWD_SLASH);
   String templateContext = StringUtils.EMPTY;
   if (StringUtils.startsWith(superType, FRWD_SLASH)) {
    templateContext = superResourceTypeArray[1];
   } else {
    templateContext = superResourceTypeArray[0];
   }
   
   if (path.startsWith(FRWD_SLASH)) {
    scriptPath = path.replaceAll(path.split(FRWD_SLASH)[NUMERIC_THREE], templateContext);
   } else {
    scriptPath = path.replaceAll(path.split(FRWD_SLASH)[NUMERIC_TWO], templateContext);
   }
   if (resource.getResourceResolver().getResource(scriptPath) == null) {
    if (!StringUtils.startsWith(superType, FRWD_SLASH)) {
     superType = "/apps/" + superType;
    }
    scriptPath = getParentScriptPath(path, resource.getResourceResolver().getResource(superType));
   }
  }
  return scriptPath;
 }
}
