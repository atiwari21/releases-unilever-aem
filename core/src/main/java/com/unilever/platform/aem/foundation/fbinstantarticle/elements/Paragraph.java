/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * The Class Paragraph.
 */
public class Paragraph extends TextContainer {
 
 /** The children. */
 private List<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> children = null;
 
 private Paragraph() {
  children = new ArrayList<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement>();
 }
 
 /**
  * Creates the.
  *
  * @return the text container
  */
 public static Paragraph create() {
  Paragraph p = new Paragraph();
  return p;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  Element paragraph = document.createElement("p");/*
                                                   * paragraph.html(textToDOMDocumentFragment(document));
                                                   */
  if (CollectionUtils.isNotEmpty(this.children)) {
   Iterator<com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement> childItr = this.children.iterator();
   while (childItr.hasNext()) {
    com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement child = childItr.next();
    if (child instanceof TextContainer) {
     TextContainer textContainer = (TextContainer) child;
     if (CollectionUtils.isEmpty(textContainer.getTextChildren())) {
      continue;
     } else if (textContainer.getTextChildren().size() == 1) {
      Object content = textContainer.getTextChildren().get(0);
      if (content instanceof String && content.toString().equals("")) {
       continue;
      }
     }
    }
    Element domElement = child.toDOMElement();
    if (domElement != null) {
     paragraph.appendChild(domElement);
    }
    
   }
  }
  if (CollectionUtils.isNotEmpty(this.textChildren)) {
   Iterator<Object> itr = this.textChildren.iterator();
   while (itr.hasNext()) {
    paragraph.appendText("" + itr.next());
   }
  }
  return paragraph;
 }
 
 /**
  * Adds new child elements to this InstantArticle.
  *
  * @param child
  *         the child
  * @return $this
  */
 public Paragraph addChild(com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticleElement child) {
  children.add(child);
  return this;
 }
 
 @Override
 public List<Object> getContainerChildren() {
  List<Object> children = new ArrayList<Object>();
  if (!this.children.isEmpty()) {
   children.addAll(this.children);
  }
  return children;
 }
}
