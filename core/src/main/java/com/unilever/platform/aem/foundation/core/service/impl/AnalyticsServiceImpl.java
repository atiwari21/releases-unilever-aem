/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.core.service.AnalyticsService;

/**
 * The Class AnalyticsServiceImpl.
 */
@Component(label = "Unilever Analytics Cloud Service", immediate = true, metatype = true)
@Service(value = { AnalyticsService.class })
public class AnalyticsServiceImpl implements AnalyticsService {
 @Reference
 ResourceResolverFactory factory;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticsServiceImpl.class);
 
 /**
  * Configuration Service object.
  */
 @Reference
 ConfigurationService configurationService;
 
 /** The Analytics properties. */
 Map<String, String> analyticsProperties = new HashMap<String, String>();
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.AnalyticsService# getCloudConfiguration(com.day.cq.wcm.api.Page)
  */
 @Override
 public Map<String, String> getAnalyticsConfiguration(Page currentPage, Node currentNode) {
  Map<String, String> domainList = new HashMap<String, String>();
  String containerProperty = "unilever";
  InheritanceValueMap inVM = new HierarchyNodeInheritanceValueMap(currentPage.getContentResource());
  String[] cloudservices = inVM.getInherited(com.day.cq.wcm.webservicesupport.ConfigurationConstants.PN_CONFIGURATIONS, String[].class);
  if (cloudservices != null) {
   for (String service : cloudservices) {
    LOGGER.debug("service {}", service);
    try {
     getCloudAnalyticsValues(domainList, containerProperty, service, currentPage);
    } catch (RepositoryException re) {
     LOGGER.error("Exception while getting cloud configurations: ", re);
    }
    
   }
  }
  
  return domainList;
  
 }
 
 /**
  * Gets the cloud analytics values.
  *
  * @param domainList
  *         the domain list
  * @param containerProperty
  *         the container property
  * @param service
  *         the service
  * @return the cloud analytics values
  * @throws RepositoryException
  *          the repository exception
  * @throws PathNotFoundException
  *          the path not found exception
  * @throws ValueFormatException
  *          the value format exception
  */
 private void getCloudAnalyticsValues(Map<String, String> domainList, String containerProperty, String service, Page currentPage)
   throws RepositoryException {
  if (service.contains(containerProperty)) {
   String unileverConfiglocation = service + CommonConstants.JCR_CONSTANTS;
   ResourceResolver resourceResolver = currentPage.getContentResource().getResourceResolver();
   if (resourceResolver != null) {
    Resource resource = resourceResolver.getResource(unileverConfiglocation);
    
    if (resource != null) {
     ValueMap properties = resource.adaptTo(ValueMap.class);
     if (properties.get(CommonConstants.NONPROD_DOMAIN) != null) {
      domainList.put("nonProdDomain", properties.get(CommonConstants.NONPROD_DOMAIN, String.class));
     }
     if (properties.get(CommonConstants.PROD_DOMAIN) != null) {
      domainList.put("prodDomain", properties.get(CommonConstants.PROD_DOMAIN, String.class));
     }
    }
   } else {
    LOGGER.debug("resourceResolver is null");
   }
  }
 }
 
 /**
  * Gets the analytics properties.
  *
  * @param currentPage
  *         the current page
  * @return the analytics properties Map
  */
 @Override
 public Map<String, String> getAnalyticsBasicProperties(Page currentPage) {
  try {
   
   analyticsProperties = configurationService.getCategoryConfiguration(currentPage, CommonConstants.ANALYTICS);
  } catch (ConfigurationNotFoundException e) {
   
   ExceptionUtils.getStackTrace(e);
  }
  return analyticsProperties;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.AnalyticsService#getAnalyticsValues(com.day.cq.wcm.api.Page, java.lang.String)
  */
 @Override
 public Map<String, String> getAnalyticsValues(Page currentPage, String category) {
  Map<String, String> analytcsvalueMap = new LinkedHashMap<String, String>();
  InheritanceValueMap inVM = new HierarchyNodeInheritanceValueMap(currentPage.getContentResource());
  String containerProperty = "unilever";
  String[] cloudservices = inVM.getInherited(com.day.cq.wcm.webservicesupport.ConfigurationConstants.PN_CONFIGURATIONS, String[].class);
  if (cloudservices != null) {
   for (String service : cloudservices) {
    if (service.contains(containerProperty)) {
     return populateAnalyticsValueList(currentPage, service, category, analytcsvalueMap);
    }
   }
  }
  return analytcsvalueMap;
  
 }
 
 /**
  * Populate analytics value list.
  *
  * @param service
  *         the service
  * @param category
  *         the category
  * @param analytcsvalueMap
  * @return the map
  */
 private Map<String, String> populateAnalyticsValueList(Page currentPage, String service, String category, Map<String, String> analytcsvalueMap) {
  
  if (currentPage != null) {
   ResourceResolver resourceResolver = currentPage.getContentResource().getResourceResolver();
   String analyticValuesNodeLocation = service + "/" + CommonConstants.JCR_CONSTANTS + "/analyticConfig";
   Resource resource = resourceResolver.getResource(analyticValuesNodeLocation);
   if (resource != null) {
    ValueMap vm = resource.getValueMap();
    String[] configurationArr = getPropertyValueArray(vm, "configuration");
    for (String configuration : configurationArr) {
     JSONObject obj = new JSONObject(configuration);
     if (obj.get("category").equals(category)) {
      JSONArray categoryConfiguration = obj.getJSONArray("categoryConfiguration");
      for (int i = 0; i < categoryConfiguration.length(); ++i) {
       JSONObject conf = categoryConfiguration.getJSONObject(i);
       String key = conf.getString("key");
       String value = conf.getString("value");
       analytcsvalueMap.put(key, value);
      }
      break;
     }
    }
   }
   
  }
  
  return analytcsvalueMap;
 }
 
 /**
  * Gets the property value array.
  *
  * @param properties
  *         the properties
  * @param propertyKey
  *         the property key
  * @return the property value array
  */
 public static String[] getPropertyValueArray(ValueMap properties, String propertyKey) {
  String[] propertyValObj = properties.get(propertyKey, String[].class);
  String[] propertyValArr = null;
  if (propertyValObj != null) {
   if (propertyValObj instanceof String[]) {
    propertyValArr = propertyValObj;
   } else {
    propertyValArr = new String[] { propertyValObj.toString() };
   }
  }
  return propertyValArr;
 }
 
}
