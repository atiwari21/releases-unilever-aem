/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service.impl;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService;

/**
 * The Class DCSConnectionServiceImpl.
 */
@Component(immediate = true, metatype = false)
@Service(value = EGiftingConnectionService.class)
public class EGiftingConnectionServiceImpl implements EGiftingConnectionService {
 
 /** The config cache. */
 private String postEntityURL;
 
 /** The client token url. */
 private String clientTokenURL;
 
 /** The generate pdfurl. */
 private String generatePDFURL;
 
 /** The timeout time. */
 private String timeoutTime;
 
 /** The Constant DEAFULT_CONNECTION_TIMEOUT. */
 private static final int DEAFULT_CONNECTION_TIMEOUT = 30;
 
 private static final int THOUSEND = 1000;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(EGiftingConnectionServiceImpl.class);
 
 /**
  * Activate.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 public void activate(ComponentContext ctx) {
  this.postEntityURL = ctx.getProperties().get("postEntityURL") != null ? ctx.getProperties().get("postEntityURL").toString() : null;
  this.clientTokenURL = ctx.getProperties().get("clientTokenURL") != null ? ctx.getProperties().get("clientTokenURL").toString() : null;
  this.generatePDFURL = ctx.getProperties().get("generatePDFURL") != null ? ctx.getProperties().get("generatePDFURL").toString() : null;
  this.timeoutTime = ctx.getProperties().get("timeoutTime") != null ? ctx.getProperties().get("timeoutTime").toString() : null;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService#getPostEntityURL()
  */
 @Override
 public String getPostEntityURL() {
  return postEntityURL;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService#getClientTokenURL()
  */
 @Override
 public String getClientTokenURL() {
  return clientTokenURL;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService#getGeneratePDFURL()
  */
 @Override
 public String getGeneratePDFURL() {
  return generatePDFURL;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService#getTimeoutTime()
  */
 @Override
 public int getTimeoutTime() {
  int finalTimeoutTime = DEAFULT_CONNECTION_TIMEOUT * THOUSEND;
  if (this.timeoutTime != null) {
   try {
    finalTimeoutTime = Integer.parseInt(this.timeoutTime) * THOUSEND;
   } catch (Exception nfe) {
    LOGGER.error("timeout value is not integer ", nfe);
   }
  }
  return finalTimeoutTime;
 }
 
}
