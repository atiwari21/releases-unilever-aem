/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.ListElement;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.ListItem;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class H1Rule.
 */
public class ListItemRule extends ConfigurationSelectorRule {
 private ListItemRule() {
  
 }
 
 public static ListItemRule create() {
  return new ListItemRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 public String[] getContextClass() {
  return new String[] { ListElement.getClassName() };
 }
 
 public static ListItemRule createFrom(JSONObject configuration) {
  return (ListItemRule) create().withSelector(configuration.getString("selector"));
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Element)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  ListItem listItem = ListItem.create();
  ((ListElement) container).addItem(listItem);
  
  transformer.transform(listItem, node);
  return container;
 }
 
}
