/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class URLConnectionHelper.
 */
public final class URLConnectionHelper {
 
 private static final String UTF_8 = "UTF-8";
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(URLConnectionHelper.class);
 
 /**
  * Instantiates a new URL connection helper.
  */
 private URLConnectionHelper() {
  
 }
 
 /**
  * Gets the response from post.
  *
  * @param request
  *         the request
  * @param url
  *         the url
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from post
  */
 public static HttpResponse getResponseFromPost(HttpServletRequest request, String url, HttpClientBuilderFactory factory, int timeout) {
  return getResponseFromPost(request, url, null, factory, timeout);
 }
 
 /**
  * Gets the response from payload post.
  *
  * @param request
  *         the request
  * @param url
  *         the url
  * @param keyName
  *         the key name
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from payload post
  */
 public static HttpResponse getResponseFromPost(HttpServletRequest request, String url, String keyName, HttpClientBuilderFactory factory,
   int timeout) {
  @SuppressWarnings("unchecked")
  Map<String, String[]> map = request.getParameterMap();
  LOGGER.debug("ParameterMap: " + map);
  JSONObject jsonParams = new JSONObject();
  if (map != null) {
   Iterator<String> itr = map.keySet().iterator();
   JSONObject jsonObj = new JSONObject();
   
   while (itr.hasNext()) {
    String paramName = itr.next();
    String val = map.get(paramName) != null ? map.get(paramName)[0] : "";
    jsonObj.put(paramName, val);
   }
   if (StringUtils.isNotBlank(keyName)) {
    jsonParams.put(keyName, jsonObj);
   } else {
    jsonParams = jsonObj;
   }
   LOGGER.debug("parameterJSON: " + jsonParams.toString());
  }
  
  return getResponseFromPostRequestToURL(url, jsonParams.toString(), factory, timeout);
 }
 
 /**
  * Gets the response from post.
  *
  * @param request
  *         the request
  * @param url
  *         the url
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from post
  */
 public static HttpResponse getResponseFromPayloadPost(HttpServletRequest request, String url, HttpClientBuilderFactory factory, int timeout) {
  
  StringBuilder jb = new StringBuilder();
  String line = null;
  try {
   BufferedReader reader = request.getReader();
   while ((line = reader.readLine()) != null) {
    jb.append(line);
   }
  } catch (IOException e) {
   LOGGER.error("Error while reading request", e);
  }
  LOGGER.debug("parameterJSON: " + jb);
  try {
   JSONObject jsonParams = new JSONObject(jb.toString());
   return getResponseFromPostRequestToURL(url, jsonParams.toString(), factory, timeout);
   
  } catch (JSONException e) {
   LOGGER.error("Error while reading json", e);
  }
  return null;
 }
 
 /**
  * Gets the response from post request to url.
  *
  * @param url
  *         the url
  * @param jsonData
  *         the json data
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from post request to url
  */
 public static HttpResponse getResponseFromPostRequestToURL(String url, String jsonData, HttpClientBuilderFactory factory, int timeout) {
  LOGGER.debug("inside getResponseFromPostRequestToURL url: {}", url);
  LOGGER.debug("inside getResponseFromPostRequestToURL jsonData: {}", jsonData);
  HttpResponse response = null;
  try {
   CloseableHttpClient httpclient = getHttpCloseableClient(factory, timeout);
   if (!StringUtils.isBlank(url)) {
    HttpPost httppost = new HttpPost(url);
    StringEntity params = new StringEntity(jsonData, UTF_8);
    httppost.setEntity(params);
    response = httpclient.execute(httppost);
    LOGGER.debug("Response from " + url + " : " + response);
   }
  } catch (IOException ioe) {
   LOGGER.error("I/O Error in getting response from : {}", url, ioe);
  } catch (Exception e) {
   LOGGER.error("Error in getting response from : {}", url, e);
  }
  return response;
 }
 
 public static HttpResponse getResponseFromPostRequestToURLIncludingAuthToken(String url, String jsonData, HttpClientBuilderFactory factory,
   int timeout, String authToken) {
  LOGGER.debug("inside getResponseFromPostRequestToURL url: {}", url);
  LOGGER.debug("inside getResponseFromPostRequestToURL jsonData: {}", jsonData);
  HttpResponse response = null;
  try {
   CloseableHttpClient httpclient = getHttpCloseableClient(factory, timeout);
   if (!StringUtils.isBlank(url)) {
    HttpPost httppost = new HttpPost(url);
    httppost.addHeader("Authorization", authToken);
    httppost.addHeader("content-type", "application/json");
    StringEntity params = new StringEntity(jsonData, UTF_8);
    httppost.setEntity(params);
    response = httpclient.execute(httppost);
    LOGGER.debug("Response from " + url + " : " + response);
   }
  } catch (IOException ioe) {
   LOGGER.error("I/O Error in getting response from : {}", url, ioe);
  } catch (Exception e) {
   LOGGER.error("Error in getting response from : {}", url, e);
  }
  return response;
 }
 
 /**
  * Gets the response from get.
  *
  * @param request
  *         the request
  * @param url
  *         the url
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from get
  */
 public static HttpResponse getResponseFromGet(HttpServletRequest request, String url, HttpClientBuilderFactory factory, int timeout) {
  try {
   StringBuilder urlStr = new StringBuilder(url);
   String queryString = request.getQueryString();
   if (url != null && !StringUtils.contains(url, "?")) {
    urlStr.append("?");
   }
   urlStr.append(queryString);
   return getResponseFromGet(urlStr.toString(), factory, timeout);
  } catch (Exception e) {
   LOGGER.error("Error in getting response from URL { } ", url, e);
  }
  return null;
 }
 
 /**
  * Builds the query param from url.
  *
  * @param url
  *         the url
  * @param map
  *         the map
  * @return the string builder
  */
 public static StringBuilder buildUrlWithQueryParam(String url, Map<String, String> map) {
  LOGGER.debug("Parameters Map: " + map);
  StringBuilder urlStr = new StringBuilder(url);
  String queryString = null;
  try {
   queryString = getQueryStringFromMap(map);
  } catch (UnsupportedEncodingException e) {
   LOGGER.error("Error in getting response from URL { } ", url, e);
  }
  if (url != null && !StringUtils.contains(url, "?")) {
   urlStr.append("?");
  } else if (url != null && StringUtils.contains(url, "?")) {
   urlStr.append("&");
  }
  urlStr.append(queryString);
  
  return urlStr;
 }
 
 /**
  * Gets the response from get.
  *
  * @param url
  *         the url
  * @param map
  *         the map
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from get
  */
 public static HttpResponse getResponseFromGet(String url, Map<String, String> map, HttpClientBuilderFactory factory, int timeout) {
  LOGGER.debug("Parameters Map: " + map);
  try {
   StringBuilder builder = buildUrlWithQueryParam(url, map);
   return getResponseFromGet(builder.toString(), factory, timeout);
  } catch (Exception e) {
   LOGGER.error("Error in getting response from URL {} ", url, e);
  }
  return null;
 }
 
 /**
  * Gets the response from get.
  *
  * @param urlWithQueryString
  *         the url with query string
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from get
  */
 public static HttpResponse getResponseFromGet(String urlWithQueryString, HttpClientBuilderFactory factory, int timeout) {
  return getResponseFromGet(urlWithQueryString, null, null, factory, timeout);
 }
 
 /**
  * Gets the response from get.
  *
  * @param urlWithQueryString
  *         the url with query string
  * @param userName
  *         the user name
  * @param pwd
  *         the pwd
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the response from get
  */
 public static HttpResponse getResponseFromGet(String urlWithQueryString, String userName, String pwd, HttpClientBuilderFactory factory,
   int timeout) {
  HttpGet httpget = new HttpGet(urlWithQueryString);
  try {
   
   HttpResponse response = getHttpCloseableClient(factory, userName, pwd, timeout).execute(httpget);
   LOGGER.debug("Response from " + urlWithQueryString + " : " + response);
   return response;
  } catch (Exception e) {
   LOGGER.error("Error in getting response from URL {} ", urlWithQueryString, e);
   
  }
  return null;
 }
 
 /**
  * Gets the http closeable client.
  *
  * @param factory
  *         the factory
  * @param timeout
  *         the timeout
  * @return the http closeable client
  */
 public static CloseableHttpClient getHttpCloseableClient(HttpClientBuilderFactory factory, int timeout) {
  return getHttpCloseableClient(factory, null, null, timeout);
 }
 
 /**
  * Gets the http closeable client.
  *
  * @param factory
  *         the factory
  * @param userName
  *         the user name
  * @param password
  *         the password
  * @param timeout
  *         the timeout
  * @return the http closeable client
  */
 public static CloseableHttpClient getHttpCloseableClient(HttpClientBuilderFactory factory, String userName, String password, int timeout) {
  RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout).setConnectionRequestTimeout(timeout).setSocketTimeout(timeout)
    .build();
  CredentialsProvider credsProvider = null;
  if (StringUtils.isNotBlank(userName) && StringUtils.isNotBlank(password)) {
   
   credsProvider = new BasicCredentialsProvider();
   Credentials c = new UsernamePasswordCredentials(userName, password);
   credsProvider.setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT), c);
  }
  return factory.newBuilder().disableAutomaticRetries().disableConnectionState().disableRedirectHandling().setDefaultRequestConfig(requestConfig)
    .setDefaultCredentialsProvider(credsProvider).build();
 }
 
 /**
  * Gets the query string from map.
  *
  * @param map
  *         the map
  * @return the query string from map
  * @throws UnsupportedEncodingException
  *          the unsupported encoding exception
  */
 public static String getQueryStringFromMap(Map<String, String> map) throws UnsupportedEncodingException {
  StringBuilder sb = new StringBuilder();
  if (map != null) {
   for (Map.Entry<String, String> e : map.entrySet()) {
    if (sb.length() > 0) {
     sb.append('&');
    }
    sb.append(URLEncoder.encode(e.getKey(), UTF_8)).append('=').append(URLEncoder.encode(e.getValue(), UTF_8));
   }
  }
  return sb.toString();
 }
 
}
