/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class NextSiblingElementGetter.
 */
public class NextSiblingElementGetter extends ElementGetter {
 
 /** The sibling selector. */
 protected String siblingSelector;
 
 /**
  * With sibling selector.
  *
  * @param siblingSelector
  *         the sibling selector
  * @return the abstract getter
  */
 public AbstractGetter withSiblingSelector(String siblingSelector) {
  this.siblingSelector = siblingSelector;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.ElementGetter#createFrom(org.json.JSONObject)
  */
 public ElementGetter createFrom(JSONObject configuration) {
  String selector = configuration.getString("selector");
  if (StringUtils.isNotBlank(selector)) {
   this.withSelector(selector);
  }
  String siblingSelector = configuration.getString("sibling.selector");
  if (StringUtils.isNotBlank(siblingSelector)) {
   this.withSiblingSelector(siblingSelector);
  }
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.ElementGetter#get(org.jsoup.nodes.Node)
  */
 public Object get(Node node) {
  Element siblingElement = null;
  
  Elements elements = this.findAll(node, this.selector);
  if (CollectionUtils.isNotEmpty(elements)) {
   Element element = elements.get(0);
   element = element.nextElementSibling();
   if (this.siblingSelector != null) {
    Elements siblings = findAll(element, siblingSelector);
    if (CollectionUtils.isNotEmpty(siblings)) {
     siblingElement = siblings.get(0);
    } else {
     return null;
    }
   } else {
    siblingElement = element;
   }
   Transformer.markAsProcessed(siblingElement);
   return Transformer.cloneNode(siblingElement);
  }
  return null;
  
 }
}
