/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.net.URI;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.configuration.GlobalConfigurationUtility;
import com.unilever.platform.aem.foundation.core.helper.URLConnectionHelper;

/**
 * The Class DynamicStoreDataPopulator.
 */
@SlingServlet(label = "Unilever DynamicStore Servlet", paths = { "/bin/profilestoredata", "/bin/weatherstoredata", "/bin/locationstoredata" })
public class DynamicStoreDataPopulator extends SlingSafeMethodsServlet {
 
 /**
  * 
  */
 private static final long serialVersionUID = 1L;
 
 /** The config admin. */
 @Reference
 private ConfigurationAdmin configAdmin;
 
 /** The factory. */
 @Reference
 private HttpClientBuilderFactory factory;
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(DynamicStoreDataPopulator.class);
 
 /** The Constant COOKIE. */
 private static final String COOKIE = "Cookie";
 
 /** The Constant EMPTY_STRING. */
 private static final String EMPTY_STRING = "";
 
 /** The Constant EQUAL_STRING. */
 private static final String EQUAL_STRING = "=";
 
 /** The Constant STORE_LOCATOR_CONFIG. */
 private static final String STORE_LOCATOR_CONFIG = "com.unilever.platform.aem.foundation.core.service.impl.servlets.DynamicStoreDataPopulator";
 
 /** The Constant PROFILE_OPTIONS. */
 private static final String PROFILE_OPTIONS = "/bin/profilestoredata.json";
 
 /** The Constant LOCATION_OPTIONS. */
 private static final String LOCATION_OPTIONS = "/bin/locationstoredata.json";
 
 /** The Constant WEATHER_OPTIONS. */
 private static final String WEATHER_OPTIONS = "/bin/weatherstoredata.json";
 
 /** The Constant DUMMY_LATITUDE. */
 private static final String DUMMY_LATITUDE = "22.5";
 
 /** The Constant DUMMY_LONGITUDE. */
 private static final String DUMMY_LONGITUDE = "2";
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
  try {
   Object timeOut = GlobalConfigurationUtility.getConfig(configAdmin, "timeOut", STORE_LOCATOR_CONFIG);
   int timeOutValue = Integer.parseInt(timeOut.toString());
   if (request.getPathInfo().equals(PROFILE_OPTIONS)) {
    String output = EMPTY_STRING;
    Object profileUrl = GlobalConfigurationUtility.getConfig(configAdmin, "envProfileUrl", STORE_LOCATOR_CONFIG);
    Object md5Token = GlobalConfigurationUtility.getConfig(configAdmin, "MD5token", STORE_LOCATOR_CONFIG);
    Object jwToken = GlobalConfigurationUtility.getConfig(configAdmin, "Jwttoken", STORE_LOCATOR_CONFIG);
    String requestUrl = profileUrl.toString();
    HttpGet httpget = new HttpGet(requestUrl);
    httpget.setHeader(COOKIE, "Jwttoken" + EQUAL_STRING + jwToken.toString() + "," + "MD5token" + EQUAL_STRING + md5Token.toString());
    HttpResponse httpResponse = URLConnectionHelper.getHttpCloseableClient(factory, timeOutValue).execute(httpget);
    LOGGER.debug("Response from " + requestUrl + " : " + httpResponse);
    output = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
    setStoreData(output, "profile", response);
    
   }
   
   if (request.getPathInfo().equals(LOCATION_OPTIONS)) {
    String output = EMPTY_STRING;
    Object locationUrl = GlobalConfigurationUtility.getConfig(configAdmin, "locationUrl", STORE_LOCATOR_CONFIG);
    String requestUrl = locationUrl.toString();
    URI uri = new URIBuilder(requestUrl).addParameter("lat", DUMMY_LATITUDE).addParameter("lng", DUMMY_LONGITUDE).addParameter("entity", "location")
      .build();
    requestUrl = uri.toString();
    HttpGet httpget = new HttpGet(requestUrl);
    HttpResponse httpResponse = URLConnectionHelper.getHttpCloseableClient(factory, timeOutValue).execute(httpget);
    LOGGER.debug("Response from " + requestUrl + " : " + httpResponse);
    output = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
    setStoreData(output, "location", response);
    
   }
   if (request.getPathInfo().equals(WEATHER_OPTIONS)) {
    String output = EMPTY_STRING;
    Object locationUrl = GlobalConfigurationUtility.getConfig(configAdmin, "locationUrl", STORE_LOCATOR_CONFIG);
    String requestUrl = locationUrl.toString();
    URI uri = new URIBuilder(requestUrl).addParameter("lat", DUMMY_LATITUDE).addParameter("lng", DUMMY_LONGITUDE).addParameter("entity", "weather")
      .build();
    requestUrl = uri.toString();
    HttpGet httpget = new HttpGet(requestUrl);
    HttpResponse httpResponse = URLConnectionHelper.getHttpCloseableClient(factory, timeOutValue).execute(httpget);
    LOGGER.debug("Response from " + requestUrl + " : " + httpResponse);
    output = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
    setStoreData(output, "weather", response);
   }
   
  } catch (Exception e) {
   LOGGER.error("Error while making connection: ", e);
  }
 }
 
 /**
  * Sets the store data.
  *
  * @param output
  *         the output
  * @param optionsRoot
  *         the options root
  * @param response
  *         the response
  */
 public void setStoreData(String output, String optionsRoot, SlingHttpServletResponse response) {
  
  try {
   JSONObject outObject = new JSONObject(output);
   JSONArray headerKeys = outObject.names();
   JSONArray optionsArray = new JSONArray();
   JSONObject finalObject;
   
   for (int i = 0; i < headerKeys.length(); i++) {
    
    if (outObject.optJSONObject(headerKeys.getString(i)) != null) {
     for (int k = 0; k < outObject.getJSONObject(headerKeys.getString(i)).names().length(); k++) {
      finalObject = new JSONObject();
      finalObject.put("text", headerKeys.getString(i) + "-" + outObject.getJSONObject(headerKeys.getString(i)).names().getString(k));
      finalObject.put("value", headerKeys.getString(i) + "-" + outObject.getJSONObject(headerKeys.getString(i)).names().getString(k));
      optionsArray.put(finalObject);
     }
    }
    
    if (outObject.optJSONArray(headerKeys.getString(i)) != null) {
     for (int l = 0; l < outObject.optJSONArray(headerKeys.getString(i)).length(); l++) {
      for (int z = 0; z < outObject.optJSONArray(headerKeys.getString(i)).getJSONObject(l).names().length(); z++) {
       finalObject = new JSONObject();
       finalObject.put("text", headerKeys.getString(i) + "-" + outObject.optJSONArray(headerKeys.getString(i)).getJSONObject(l).names().getString(z));
       finalObject
         .put("value", headerKeys.getString(i) + "-" + outObject.optJSONArray(headerKeys.getString(i)).getJSONObject(l).names().getString(z));
       optionsArray.put(finalObject);
      }
     }
    }
    if (outObject.optJSONObject(headerKeys.getString(i)) == null && outObject.optJSONArray(headerKeys.getString(i)) == null) {
     finalObject = new JSONObject();
     finalObject.put("text", headerKeys.getString(i));
     finalObject.put("value", headerKeys.getString(i));
     optionsArray.put(finalObject);
    }
    
   }
   
   // optionsArray.put(tempjsonObj);
   JSONObject finalJsonResponse = new JSONObject();
   // Adding this finalJsonResponse object to showcase optionsRoot property functionality
   finalJsonResponse.put(optionsRoot, optionsArray);
   response.getWriter().println(finalJsonResponse.toString());
  } catch (JSONException e) {
   LOGGER.error("Error while setting Data to JSON :", e);
  } catch (IOException e) {
   LOGGER.error("Error while doing I/O :", e);
  }
 }
}
