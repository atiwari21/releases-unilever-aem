/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.unilever.platform.aem.foundation.core.dto.BrandMarketBean;

/**
 * The Class ConfigurationServiceImpl.
 */
@Component(immediate = true, metatype = false)
@Service(value = ConfigurationService.class)
public class ConfigurationServiceImpl implements ConfigurationService {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationServiceImpl.class);
 
 /** The config cache. */
 private LoadingCache<String, Map<String, String>> configCache;
 
 /** The resolver factory. */
 // Inject a Sling ResourceResolverFactory
 @Reference
 private ResourceResolverFactory resolverFactory;
 
 @Reference
 SlingSettingsService slingSettingService;
 
 /** The sites paths. */
 private String[] sitesPaths;
 
 /** The Constant DEFAULT_MAX_NUMBER_IN_CACHE. */
 private static final int DEFAULT_MAX_NUMBER_IN_CACHE = 100;
 
 /** The Constant DEFAULT_TTL_FOR_CONF_CACHE. */
 private static final int DEFAULT_TTL_FOR_CONF_CACHE = 30;
 
 /** The run modes. */
 private Set<String> runModes;
  
 /** The error map list. */
 private Map<String, List<Map<String, String>>> errorMapList;
 
 /**
  * Gets the error map list.
  *
  * @return the error map list
  */
 public Map<String, List<Map<String, String>>> getErrorMapList() {
  return errorMapList;
 }

 /**
  * Sets the error map list.
  *
  * @param errorMapList the error map list
  */
 public void setErrorMapList(Map<String, List<Map<String, String>>> errorMapList) {
  this.errorMapList = errorMapList;
 }

/**
  * Activate.
  *
  * @param ctx
  *         the ctx
  */
 @Activate
 public void activate(ComponentContext ctx) {
  LOGGER.info("Inside ConfigurationServiceImpl activate " + ctx);
  ResourceResolver resourceResolver = null;
  runModes = slingSettingService.getRunModes();
  errorMapList = new LinkedHashMap<>();
  
  int maxRecord = ctx.getProperties().get(ConfigurationConstants.MAX_RECORD_IN_CACHE) != null
    ? Integer.parseInt(ctx.getProperties().get(ConfigurationConstants.MAX_RECORD_IN_CACHE).toString()) : DEFAULT_MAX_NUMBER_IN_CACHE;
  int ttlInMin = ctx.getProperties().get(ConfigurationConstants.TTL_FOR_CONF_CACHE) != null
    ? Integer.parseInt(ctx.getProperties().get(ConfigurationConstants.TTL_FOR_CONF_CACHE).toString()) : DEFAULT_TTL_FOR_CONF_CACHE;
  
  sitesPaths = ctx.getProperties().get(ConfigurationConstants.SITES_PATHS_CONFIG_PROPERTY_KEY) != null
    ? PropertiesUtil.toStringArray(ctx.getProperties().get(ConfigurationConstants.SITES_PATHS_CONFIG_PROPERTY_KEY)) : null;
  
  String globalConfigPageName = ctx.getProperties().get(ConfigurationConstants.GLOBAL_CONFIG_PAGE_NAME) != null
    ? ctx.getProperties().get(ConfigurationConstants.GLOBAL_CONFIG_PAGE_NAME).toString() : "config";
  ConfigurationHelper.setGlobalConfigPageName(globalConfigPageName);
  configCache = CacheBuilder.newBuilder().maximumSize(maxRecord).expireAfterAccess(ttlInMin, TimeUnit.MINUTES)
    .build(new CacheLoader<String, Map<String, String>>() {
     @Override
     public Map<String, String> load(String cacheName) throws Exception {
      LOGGER.info("Inside Guava CacheBuilder load method");
      return startConfigurationCaching(resolverFactory);
     }
    });
  
  try {
   Map<String, String> configurationMap = configCache.get(ConfigurationConstants.CONF_CACHE_NAME);
   
   if (configurationMap.isEmpty()) {
    LOGGER.info("Configuration Map is empty so invalidating the cache");
    this.configCache.invalidate(ConfigurationConstants.CONF_CACHE_NAME);
   }
  } catch (ExecutionException e) {
   LOGGER.error("Exception in getting configuration from cache", e);
  } finally {
   if (resourceResolver != null && resourceResolver.isLive()) {
    resourceResolver.close();
   }
  }
 }
 
 /**
  * Start configuration caching.
  *
  * @param resolverFactory
  *         the resolver factory
  * @return the map
  */
 private Map<String, String> startConfigurationCaching(ResourceResolverFactory resolverFactory) {
  LOGGER.info("Inside startConfigurationCaching of ConfigurationServiceImpl");
  ResourceResolver resourceResolver = null;
  
  Map<String, String> configurationMap = new TreeMap<String, String>();
  try {
   resourceResolver = ConfigurationHelper.getResourceResolver(resolverFactory, "configurationService");
   ConfigurationMgr confMgr = new ConfigurationMgr(resourceResolver);
   configurationMap = confMgr.loadConfiguration(sitesPaths);
   
   // config Error key Map
   configurationMap.put("configError", StringUtils.EMPTY);
   LOGGER.info("map = " + configurationMap);
   
  } catch (LoginException e) {
   LOGGER.error("Error getting ResourceResolver in ConfigurationServiceImpl ", e);
  } finally {
   if (resourceResolver != null && resourceResolver.isLive()) {
    resourceResolver.close();
   }
  }
  
  return configurationMap;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.ConfigurationService#getPageConfigurations(com.day.cq.wcm.api.Page)
  */
 @Override
 public Map<String, Map<String, String>> getPageConfigurations(Page page) {
  return getPageConfigurations(page, true);
 }
 
 /**
  * Gets the page configurations.
  *
  * @param page
  *         the page
  * @param isConfigStorelocal
  *         the is config storelocal
  * @return the page configurations
  */
 private Map<String, Map<String, String>> getPageConfigurations(Page page, boolean isConfigStorelocal) {
  BrandMarketBean brandMarketBean = new BrandMarketBean(page);
  Map<String, Map<String, String>> pageMap = new HashMap<String, Map<String, String>>();
  if (!isConfigStorelocal) {
   GlobalConfiguration gc = new GlobalConfigurationImpl();
   pageMap = gc.getPageConfigurations(brandMarketBean);
  } else {
   pageMap = ConfigurationHelper.getPageConfigurations(brandMarketBean, this.configCache);
  }
  return pageMap;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.configuration.ConfigurationService#getCategoryConfiguration(com.unilever.platform.aem.foundation.core.dto.
  * BrandMarketBean, java.lang.String)
  */
 @Override
 public Map<String, String> getCategoryConfiguration(BrandMarketBean brandMarketBean, String category) throws ConfigurationNotFoundException {
  return ConfigurationHelper.getCategoryConfiguration(brandMarketBean, category, this.configCache);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.ConfigurationService#getCategoryConfiguration(com.day.cq.wcm.api.Page, java.lang.String)
  */
 @Override
 public Map<String, String> getCategoryConfiguration(Page page, String category) throws ConfigurationNotFoundException {
  return getCategoryConfiguration(page, category, true);
 }
 
 /**
  * Gets the category configuration.
  *
  * @param page
  *         the page
  * @param category
  *         the category
  * @param isConfigStorelocal
  *         the is config storelocal
  * @return the category configuration
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 private Map<String, String> getCategoryConfiguration(Page page, String category, boolean isConfigStorelocal) {
  BrandMarketBean brandMarketBean = new BrandMarketBean(page);
  Map<String, String> configMap = new HashMap<String, String>();
  if (!isConfigStorelocal) {
   GlobalConfiguration gc = new GlobalConfigurationImpl();
   configMap = gc.getCategoryConfiguration(brandMarketBean, category);
  } else {
   try {
    configMap = getCategoryConfiguration(brandMarketBean, category);
   } catch (ConfigurationNotFoundException e) {
    LOGGER.debug("ConfigurationNotFoundException ", e);
   }
  }
  return configMap;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.ConfigurationService#getConfigValue(com.unilever.platform.aem.foundation.core.dto.
  * BrandMarketBean, java.lang.String, java.lang.String)
  */
 @Override
 public String getConfigValue(BrandMarketBean bm, String category, String key) {
  try {
   return ConfigurationHelper.getConfigValue(bm, category, key, this.configCache);
  } catch (ConfigurationNotFoundException e) {
	//Logic to show config error.
	if (this.runModes.contains("author")){
		StringBuilder strBuilder = new StringBuilder(bm.getBrand());
		strBuilder.append("_"+bm.getMarket());
		String mapKey = strBuilder.toString();
		if(this.errorMapList != null){
		 if(this.errorMapList.containsKey(mapKey)){
		  List<Map<String, String>> errorList = this.errorMapList.get(mapKey);
          this.errorMapList.put(mapKey, getErrorMapList(category, key, errorList));
		  }else{
           this.errorMapList.put(mapKey, getErrorMapList(category, key, null));
		  }
	   }
	}
   LOGGER.debug("ConfigurationNotFoundException ", e);
  }
  return StringUtils.EMPTY;
 }
 
 /**
  * Gets the error map list.
  *
  * @param category the category
  * @param key the key
  * @param errorList the error list
  * @return the error map list
  */
 private List<Map<String, String>> getErrorMapList(String category, String key, List<Map<String, String>> errorList){
  StringBuilder strBuilder = new StringBuilder();
  strBuilder.append("Please Provide Category:");
  strBuilder.append(category);
  strBuilder.append(" and Key:");
  strBuilder.append(key);
  Map<String, String> errorMap = new LinkedHashMap<>();
  errorMap.put("error", strBuilder.toString());
  if(errorList == null){
   errorList = new ArrayList<>();
  }
  boolean flag = false;
  Iterator<Map<String, String>> errorListItr = errorList.iterator();
  while (errorListItr.hasNext()) {
   Map<String, String> type = (Map<String, String>) errorListItr.next();
   if(type.get("error").equals(strBuilder.toString())){
    flag = true;
    break;
   }
  }
  if(!flag){
   errorList.add(errorMap);
  }
  return errorList;
 }
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.ConfigurationService#getConfigValue(com.day.cq.wcm.api.Page, java.lang.String,
  * java.lang.String)
  */
 @Override
 public String getConfigValue(Page page, String category, String key) throws ConfigurationNotFoundException {
  return getConfigValue(page, category, key, true);
 }
 
 /**
  * Gets the config value.
  *
  * @param page
  *         the page
  * @param category
  *         the category
  * @param key
  *         the key
  * @param isConfigStorelocal
  *         the is config storelocal
  * @return the config value
  * @throws ConfigurationNotFoundException
  *          the configuration not found exception
  */
 private String getConfigValue(Page page, String category, String key, boolean isConfigStorelocal) throws ConfigurationNotFoundException {
  String value = StringUtils.EMPTY;
  BrandMarketBean brandMarketBean = new BrandMarketBean(page);
  if (!isConfigStorelocal) {
   GlobalConfiguration gc = new GlobalConfigurationImpl();
   value = gc.getConfigValue(brandMarketBean, category, key);
  } else {
   value = getConfigValue(brandMarketBean, category, key);
  }
  return value;
 }
 
 /**
  * Gets the config cache.
  *
  * @return the config cache
  */
 LoadingCache<String, Map<String, String>> getConfigCache() {
  return this.configCache;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.configuration.ConfigurationService#inValidateCache(java.lang.String)
  */
 @Override
 public void inValidateCache() {
  LOGGER.debug("configCache before invalidating is, " + this.configCache);
  this.configCache.refresh(ConfigurationConstants.CONF_CACHE_NAME);
  LOGGER.debug("configCache after invalidating is, " + this.configCache);
 }
}
