/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 * The Class IntegerGetter.
 */
public class IntegerGetter extends StringGetter {
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.StringGetter#createFrom(org.json.JSONObject)
  */
 @Override
 public ElementGetter createFrom(JSONObject configuration) {
  if (configuration.getString("selector") != null) {
   return this.withSelector(configuration.getString("selector"));
  }
  if (configuration.getString("attribute") != null) {
   return this.withAttribute(configuration.getString("attribute"));
  }
  return null;
 }
 
 /**
  * Gets the integer.
  *
  * @param node
  *         the node
  * @return the integer
  */
 public int getInteger(Node node) {
  int result = -1;
  Elements elements = this.findAll(node, this.selector);
  if (CollectionUtils.isNotEmpty(elements)) {
   Element element = elements.get(0);
   if (this.attribute != null) {
    result = Integer.parseInt(element.attr(this.attribute));
   } else {
    result = Integer.parseInt(element.text());
   }
   
   return result;
  }
  return -1;
  
 }
}
