/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.scripting.handlebars.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingBindings;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.WCMMode;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Template;
import com.google.common.cache.LoadingCache;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService;
import com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache;
import com.unilever.platform.aem.foundation.configuration.FrontEndConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.utils.CacheUtil;

/**
 * This class defines the Adaptive Image Helper for Unilever. It combines the image specific data with the mark up and adds it to the template. :</b>
 * {{adaptiveRetina path extension altImage title fileName url isNotAdaptiveImage '490,490,490,490,640,640,460,460' }}
 *
 * @author nbhart
 * 
 */
@Component(enabled = true, immediate = true)
@Service(value = HandlebarsHelperService.class)
public class AdaptiveRetinaIncludeHelper implements HandlebarsHelperService<Object> {
    
    private FrontEndConfigurationService configurationService;
    
    /** The Constant BREAKPOINTS. */
    private static final String BREAKPOINTS = "BREAKPOINTS";
    
    /**
     * Logger Instance.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AdaptiveRetinaIncludeHelper.class);
    
    /**
     * A singleton instance of this helper.
     */
    private static final Helper<Object> INSTANCE = new AdaptiveRetinaIncludeHelper();
    
    /**
     * The helper's name.
     */
    private static final String NAME = "adaptiveRetina";
    
    /**
     * the constant for ",".
     */
    private static final String COMMA = ",";
    
    /**
     * the constant for "<className>".
     */
    private static final String CLASS_NAME = "<className>";
    
    /**
     * the constant for "<title>".
     */
    private static final String TITLE = "<title>";
    
    /**
     * the constant for UTF-8.
     */
    private static final String UTF_8 = "UTF-8";
    
    ScriptEngineManager manager = new ScriptEngineManager(null);
    
    /**
     * Adaptive Image Template Source.
     *
     */
    private static final String TEMPLATE_SOURCE = "<picture class=\"<lazyLoadClass>\">" + "<!--[if IE 9]><video style=\"display: none;\"><![endif]-->"
            + "<source <sourceSrc>=\"<fileName>.ulenscale.tabL.<extn>, <fileName>.ulenscale.tabL2x.<extn> 2x\" "
            + "media=\"screen and (min-width: tabLMinpx) and (max-width: tabLMaxpx)\">"
            + "<source <sourceSrc>=\"<fileName>.ulenscale.tabP.<extn>, <fileName>.ulenscale.tabP2x.<extn> 2x\" "
            + "media=\"screen and (min-width: tabPMinpx) and (max-width: tabPMaxpx)\">" + "<source <sourceSrc>=\"<fileName>.ulenscale.mobL.<extn>, "
            + "<fileName>.ulenscale.mob2x.<extn> 2x\" media=\"screen and (min-width: mobMinpx) and (max-width: mobMaxpx)\">"
            + "<source <sourceSrc>=\"<fileName>.ulenscale.desktop.<extn>\" media=\"screen and (min-width: desktopMinpx) \">"
            + "<!--[if IE 9]></video><![endif]-->"
            + "<img class=\"<className>\" <title> alt=\"<altText>\" <imageSrc>=\"<fileName>.ulenscale.mobL.<extn>\"/>" + "</picture>";
    
    /** The Constant TEMPLATE_SOURCE_ORIGINAL_IMAGE. */
    private static final String TEMPLATE_SOURCE_ORIGINAL_IMAGE = "<img class=\"non-adaptive <title>\" src=\"<imageURL>\" <title> alt=\"<alt>\">";
    
    /**
     * Apply.
     *
     * @param context
     *            The object for context.
     * @param options
     *            The handle bar options.
     * @return CharSequence
     * @throws IOException
     *             The i/o exception.
     * @see Helper#apply(Object, Options)
     */
    @Override
    public CharSequence apply(final Object context, final Options options) throws IOException {
        
        SlingScriptHelper sling = (SlingScriptHelper) options.context.get("sling");
        configurationService = sling.getService(FrontEndConfigurationService.class);
        String fileName = StringUtils.defaultIfEmpty((String) context, StringUtils.EMPTY);
        LOGGER.debug("Image FileName is :" + fileName);
        String extn = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.ZERO), StringUtils.EMPTY);
        LOGGER.debug("Image Extension is :" + extn);
        
        Object[] optionObjects = options.params;
        int numberOfOptions = optionObjects.length;
        
        if (StringUtils.isEmpty(extn) && StringUtils.isNotEmpty(fileName)) {
            extn = fileName.substring(fileName.lastIndexOf('.') + CommonConstants.ONE);
        }
        
        String imageUrl = "";
        if (numberOfOptions >= CommonConstants.FIVE) {
            imageUrl = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.FOUR), StringUtils.EMPTY);
        }
        
        LOGGER.debug("Image URL is :" + imageUrl);
        String isNotAdaptive = null;
        
        if (numberOfOptions > CommonConstants.FIVE) {
            if (options.param(CommonConstants.FIVE) instanceof Boolean) {
                isNotAdaptive = options.param(CommonConstants.FIVE).toString();
            } else {
                isNotAdaptive = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.FIVE), StringUtils.EMPTY);
            }
        }
        String[] sizes = null;
        String templateString = StringUtils.EMPTY;
        sizes = getOriginalSizesFromHandlebarHelper(options, numberOfOptions);
        if (numberOfOptions > CommonConstants.EIGHT) {
            String dimensionsString = getValueFromComponentConfig(options, "dimensions");
            sizes = (StringUtils.isNotBlank(dimensionsString)) ? StringUtils.split(dimensionsString, COMMA) : sizes;
        }
        LOGGER.debug("Adaptive?<blank is adaptive> :" + isNotAdaptive);
        templateString = setTemplateStringForAdaptiveBehavior(isNotAdaptive, options, imageUrl, fileName, extn, sizes);
        LOGGER.debug("Final Template" + templateString);
        
        Handlebars handlebar = options.handlebars;
        Context hbContext = options.context;
        Template handlebarTemplate = handlebar.compileInline(templateString);
        String template = handlebarTemplate.apply(hbContext);
        return new Handlebars.SafeString(template);
    }
    
    /**
     * Gets the original sizes from handlebar helper.
     *
     * @param options
     *            the options
     * @param numberOfOptions
     *            the number of options
     * @return the original sizes from handlebar helper
     */
    private String[] getOriginalSizesFromHandlebarHelper(final Options options, int numberOfOptions) {
        return (numberOfOptions > CommonConstants.SIX) ? StringUtils.split((String) options.param(CommonConstants.SIX), COMMA) : null;
    }
    
    /**
     * Gets the value from component config.
     *
     * @param options
     *            the options
     * @param key
     *            the key
     * @return the value from component config
     */
    @SuppressWarnings("unchecked")
    private String getValueFromComponentConfig(final Options options, String key) {
        String configValue = null;
        String brandName = CacheUtil.getBrandName(options);
        try {
            LoadingCache<String, Map<String, String>> existingcache = CacheUtil.getBreakpointsdata(options,configurationService);
            Map<String, String> cacheMap = existingcache.get(brandName);
            if(cacheMap!=null && cacheMap.containsKey(brandName)){
            String breakPointData = cacheMap.get(brandName);
            JSONObject dataJson = (new JSONObject(breakPointData)).getJSONObject("data");
            
            String configSizesName = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.EIGHT), StringUtils.EMPTY);
            String[] componentAttributes = StringUtils.split(configSizesName, CommonConstants.DOT_CHAR_LITERAL);
            configValue = getConfigvalue(key, configValue, dataJson, componentAttributes);
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException occured ", e);
        } catch (ExecutionException e) {
            LOGGER.error("ExecutionException occured ", e);
        }
        return configValue;
    }

    /**
     * @param key
     * @param configValue
     * @param dataJson
     * @param componentAttributes
     * @return
     * @throws JSONException
     */
    private String getConfigvalue(String key, String config, JSONObject dataJson, String[] componentAttributes) throws JSONException {
        JSONObject adaptiveRetinaMap;
        JSONObject componentAdaptiveMap;
        JSONObject imageMap;
        String configValue = config;
        if (dataJson.get(NAME) != null) {
            adaptiveRetinaMap = dataJson.getJSONObject(NAME);
            if (componentAttributes.length > CommonConstants.ZERO) {
                componentAdaptiveMap = (adaptiveRetinaMap.has(componentAttributes[CommonConstants.ZERO]))
                        ? adaptiveRetinaMap.getJSONObject(componentAttributes[CommonConstants.ZERO]) : null;
                if (componentAttributes.length > CommonConstants.ONE && componentAdaptiveMap != null) {
                    imageMap = (componentAdaptiveMap.has(componentAttributes[CommonConstants.ONE]))
                            ? componentAdaptiveMap.getJSONObject(componentAttributes[CommonConstants.ONE]) : null;
                    Object componentAttribute = (imageMap != null) ? imageMap.get(key) : null;
                    configValue = (componentAttribute != null) ? componentAttribute.toString() : StringUtils.EMPTY;
                }
            }
        }
        return configValue;
    }
    
    /**
     * Gets the name.
     *
     * @return String
     * @see HandlebarsHelperService#getName()
     */
    @Override
    public String getName() {
        return NAME;
    }
    
    /**
     * Gets the helper.
     *
     * @return Helper<Object>
     * @see HandlebarsHelperService#getHelper()
     */
    @Override
    public Helper<Object> getHelper() {
        return INSTANCE;
    }
    
    /**
     * Sets the cache.
     *
     * @param templateCache
     *            The object for template cache.
     * @see com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsHelperService#setCache
     *      (com.sapient.platform.iea.aem.scripting.handlebars.api.HandlebarsTemplateCache)
     */
    @Override
    public void setCache(final HandlebarsTemplateCache templateCache) {
        // Not setting the cache instance as it is not required in this helper.
    }
    
    /**
     * This method sets templateString based on adaptive behavior.
     *
     * @param isNotAdaptive
     *            the is not adaptive
     * @param options
     *            the options
     * @param imageUrl
     *            the image url
     * @param fileName
     *            the file name
     * @param extn
     *            the extn
     * @param sizes
     *            the sizes
     * @return the string
     */
    public String setTemplateStringForAdaptiveBehavior(final String isNotAdaptive, final Options options, final String imageUrl, String fileName,
            final String extn, String[] sizes) {
        String templateString = null;
        // Introduced temporary variable as method parameters should not be reassigned.
        String tempFileName = fileName;
        String title = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.TWO), StringUtils.EMPTY);
        LOGGER.debug("Image title is :" + title);
        String altText = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.ONE), StringUtils.EMPTY);
        LOGGER.debug("Image altText is :" + altText);
        String baseName = StringUtils.isBlank(options.param(CommonConstants.THREE)) ? (String) options.param(CommonConstants.THREE)
                : StringUtils.EMPTY;
        if (StringUtils.isNotEmpty((String) options.param(CommonConstants.THREE))) {
            baseName = getBaseName(options, baseName);
        }
        
        String titleAttribute = StringUtils.EMPTY;
        String className = getClassName(title);
        if (StringUtils.isNotBlank(title)) {
            titleAttribute = "title=\"" + title + "\"";
        }
        
        if (null != isNotAdaptive) {
            if ("true".equals(isNotAdaptive)) {
                templateString = TEMPLATE_SOURCE_ORIGINAL_IMAGE.replace(TITLE, titleAttribute);
                templateString = templateString.replace("<imageURL>", imageUrl);
                templateString = templateString.replace("<alt>", altText);
            } else {
                templateString = TEMPLATE_SOURCE.replace("<fileName>", replaceSpace(tempFileName));
                templateString = templateString.replace("<extn>", extn);
                templateString = templateString.replace("<altText>", altText);
                templateString = templateString.replace(CLASS_NAME, className);
                templateString = templateString.replace(TITLE, titleAttribute);
                if (StringUtils.isNotEmpty(baseName)) {
                    templateString = templateString.replace("<name>", baseName);
                } else {
                    templateString = templateString.replace(".<name>", StringUtils.EMPTY);
                }
                templateString = replaceLazyLoadClass(templateString, options);
                templateString = replaceViewPorts(templateString, options);
            }
        } else {
            templateString = updateTemplate(options, extn, tempFileName, altText, baseName, className);
        }
        
        if ((null != isNotAdaptive && "true".equals(isNotAdaptive)) || "gif".equals(extn)) {
            templateString = TEMPLATE_SOURCE_ORIGINAL_IMAGE.replace(TITLE, titleAttribute);
            templateString = templateString.replace("<imageURL>", imageUrl);
            templateString = templateString.replace("<alt>", altText);
            templateString = templateString.replace(CLASS_NAME, className);
        }
        
        return replaceSizes(templateString, sizes);
    }

    /**
     * @param options
     * @param baseName
     * @return
     */
    private String getBaseName(final Options options, String baseName) {
        String base = baseName;
        try {
            base = URLEncoder.encode((String) options.param(CommonConstants.THREE), UTF_8);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(" Caught UnsupportedEncodingException in setTemplateStringForAdaptiveBehavior method. " + e);
        }
        LOGGER.debug("Image basename after encoding is :" + baseName);
        return base;
    }

    /**
     * @param options
     * @param extn
     * @param tempFileName
     * @param altText
     * @param baseName
     * @param className
     * @return
     */
    private String updateTemplate(final Options options, final String extn, String tempFileName, String altText, String baseName, String className) {
        String templateString;
        templateString = TEMPLATE_SOURCE.replace("<fileName>", tempFileName);
        templateString = templateString.replace("<extn>", extn);
        templateString = templateString.replace("<altText>", altText);
        templateString = templateString.replace(CLASS_NAME, className);
        if (StringUtils.isNotEmpty(baseName)) {
            templateString = templateString.replace("<name>", baseName);
        } else {
            templateString = templateString.replace(".<name>", StringUtils.EMPTY);
        }
        templateString = replaceLazyLoadClass(templateString, options);
        templateString = replaceViewPorts(templateString, options);
        return templateString;
    }
    
    /**
     * Returns class name as per FE standard. Following filters are applied: 1. Remove all special characters. 2. Replace all intermediate spaces with
     * "-". 3. Convert all text to lowercase. 4. "c-" is added to the class name at beginning.
     * 
     * @param title
     *            the title
     * @return className
     */
    private String getClassName(String title) {
        String className = title;
        String prefix = "c-";
        if (StringUtils.isNotBlank(className)) {
            className = className.trim().toLowerCase();
            className = className.replaceAll("[^a-zA-Z ]", "");
            className = className.replaceAll("\\s+", "-");
            className = prefix.concat(className);
        }
        return className;
    }
    
    /**
     * Replaces the size constant with actual sizes.
     *
     * @param templateString
     *            the template string
     * @param sizes
     *            the sizes
     * @return template string with actual image sizes.
     */
    private String replaceSizes(String templateString, String[] sizes) {
        String template = templateString;
        if (StringUtils.isNotBlank(template) && !ArrayUtils.isEmpty(sizes)) {
            String desktop = sizes[CommonConstants.ZERO] + CommonConstants.CROSS + sizes[CommonConstants.ONE];
            String tabL = sizes[CommonConstants.TWO] + CommonConstants.CROSS + sizes[CommonConstants.THREE];
            String tabP = sizes[CommonConstants.FOUR] + CommonConstants.CROSS + sizes[CommonConstants.FIVE];
            String mobL = sizes[CommonConstants.SIX] + CommonConstants.CROSS + sizes[CommonConstants.SEVEN];
            String tabL2x = Integer.valueOf(sizes[CommonConstants.TWO]) * CommonConstants.TWO + CommonConstants.CROSS
                    + Integer.valueOf(sizes[CommonConstants.THREE]) * CommonConstants.TWO;
            String tabP2x = Integer.valueOf(sizes[CommonConstants.FOUR]) * CommonConstants.TWO + CommonConstants.CROSS
                    + Integer.valueOf(sizes[CommonConstants.FIVE]) * CommonConstants.TWO;
            String mob2x = Integer.valueOf(sizes[CommonConstants.SIX]) * CommonConstants.TWO + CommonConstants.CROSS
                    + Integer.valueOf(sizes[CommonConstants.SEVEN]) * CommonConstants.TWO;
            template = template.replace("tabL2x", tabL2x);
            template = template.replace("tabP2x", tabP2x);
            template = template.replace("mob2x", mob2x);
            template = template.replace("desktop", desktop);
            template = template.replace("tabL", tabL);
            template = template.replace("tabP", tabP);
            template = template.replace("mobL", mobL);
            
        }
        return template;
    }
    
    /**
     * Sets the lazy loading related attributes.
     *
     * @param templateString
     *            the template string
     * @param options
     *            the options
     * @return updated template string
     */
    private String replaceLazyLoadClass(String templateString, Options options) {
        String template = templateString;
        String isLazyLoad = null;
        String imageSrc = StringUtils.EMPTY;
        String sourceSrc = StringUtils.EMPTY;
        String lazyLoadClass = StringUtils.EMPTY;
        if (options.params.length > CommonConstants.EIGHT) {
            isLazyLoad = getValueFromComponentConfig(options, "isLazyLoad");
        }
        if (StringUtils.isBlank(isLazyLoad) && options.params.length > CommonConstants.SEVEN) {
            if (options.param(CommonConstants.SEVEN) instanceof Boolean) {
                isLazyLoad = options.param(CommonConstants.SEVEN).toString();
            } else {
                isLazyLoad = StringUtils.defaultIfEmpty((String) options.param(CommonConstants.SEVEN), StringUtils.EMPTY);
            }
        }
        SlingHttpServletRequest request = (SlingHttpServletRequest) options.get(SlingBindings.REQUEST);
        boolean enableLazyLoading = WCMMode.fromRequest(request) == WCMMode.EDIT || WCMMode.fromRequest(request) == WCMMode.DESIGN;
        if (enableLazyLoading || WCMMode.fromRequest(request) == WCMMode.PREVIEW
                || (StringUtils.isNotEmpty(isLazyLoad) && StringUtils.equalsIgnoreCase(isLazyLoad, "false"))) {
            imageSrc = "src";
            sourceSrc = "srcset";
        } else {
            imageSrc = "data-src";
            sourceSrc = "data-srcset";
            lazyLoadClass = "lazy-load is-loading is-lazyload";
        }
        template = template.replace("<sourceSrc>", sourceSrc);
        template = template.replace("<imageSrc>", imageSrc);
        template = template.replace("<lazyLoadClass>", lazyLoadClass);
        return template;
    }
    
    /**
     * Replaces the brand specific break points in adaptive image tags.
     *
     * @param templateStr
     *            the template str
     * @param options
     *            the options
     * @return image template.
     */
    @SuppressWarnings("unchecked")
    private String replaceViewPorts(String templateStr, Options options) {
        String templateString = templateStr;
        String brandName = CacheUtil.getBrandName(options);
        try {
            LoadingCache<String, Map<String, String>> existingcache = CacheUtil.getBreakpointsdata(options,configurationService);
            Map<String, String> cacheMap = existingcache.get(brandName);
            if(cacheMap!=null && cacheMap.containsKey(brandName)){
            String breakPointData = cacheMap.get(brandName);
            JSONObject dataJson = (new JSONObject(breakPointData)).getJSONObject("data");
            JSONObject breakpoints = null;
                if (dataJson.has("breakpoints")||dataJson.has(BREAKPOINTS)) {
                    breakpoints = dataJson.has("breakpoints")?dataJson.getJSONObject("breakpoints"):dataJson.getJSONObject(BREAKPOINTS);
                    templateString = templateString.replace("desktopMin", breakpoints.get("desktopMin").toString());
                    templateString = templateString.replace("tabLMin", breakpoints.get("tabLMin").toString());
                    templateString = templateString.replace("tabLMax", breakpoints.get("tabLMax").toString());
                    templateString = templateString.replace("tabPMin", breakpoints.get("tabPMin").toString());
                    templateString = templateString.replace("tabPMax", breakpoints.get("tabPMax").toString());
                    templateString = templateString.replace("mobMin", breakpoints.get("mobMin").toString());
                    templateString = templateString.replace("mobMax", breakpoints.get("mobMax").toString());
                }
            }
        } catch (JSONException e) {
            LOGGER.error("JSONException occured ", e);
        } catch (ExecutionException e) {
            LOGGER.error("ExecutionException occured ", e);
        }
        
        return templateString;
    }
    /**
     * This method fills up "%20" in spaces found in file name.
     * 
     * @param fileName
     * @return
     */
    private String replaceSpace(String fileName) {
        String tempFileName = fileName;
        tempFileName = tempFileName.replaceAll("\\s", "%20");
        return tempFileName;
    }
    

}
