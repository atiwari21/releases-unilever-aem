/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Anchor;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Caption;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Image;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.ListItem;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Paragraph;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class AnchorRule.
 */
public class ImageRule extends ConfigurationSelectorRule {
 
 /** The Constant PROPERTY_IMAGE_URL. */
 private static final String PROPERTY_IMAGE_URL = "image.url";
 
 /** The Constant PROPERTY_LAZY_LOAD_IMAGE_URL. */
 private static final String PROPERTY_LAZY_LOAD_IMAGE_URL = "lazy.load.image.url";
 
 /** The Constant PROPERTY_LIKE. */
 private static final String PROPERTY_LIKE = "image.like";
 
 /** The Constant PROPERTY_COMMENTS. */
 private static final String PROPERTY_COMMENTS = "image.comments";
 
 /** The Constant PROPERTY_IMAGE_TITLE. */
 private static final String PROPERTY_IMAGE_TITLE = "image.title";
 
 /** The Constant ASPECT_FIT. */
 private static final String ASPECT_FIT = "aspect-fit";
 
 /** The Constant ASPECT_FIT_ONLY. */
 private static final String ASPECT_FIT_ONLY = "aspect-fit-only";
 
 /** The Constant FULLSCREEN. */
 private static final String FULLSCREEN = "fullscreen";
 
 /** The Constant NON_INTERACTIVE. */
 private static final String NON_INTERACTIVE = "non-interactive";
 
 /**
  * Instantiates a new anchor rule.
  */
 private ImageRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the image rule
  */
 public static ImageRule create() {
  return new ImageRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container1, Node node) {
  Container container = container1;
  Image image = Image.create();
  InstantArticle instantArticle = transformer.getInstantArticle();
  if (container instanceof InstantArticle) {
   instantArticle = (InstantArticle) container;
  } else if (instantArticle != null) {
   container = Paragraph.create();
  }
  
  String url = (String) getProperty(PROPERTY_IMAGE_URL, node);
  String lazyLoadUrl = (String) getProperty(PROPERTY_LAZY_LOAD_IMAGE_URL, node);
  String imageTitle = (String) getProperty(PROPERTY_IMAGE_TITLE, node);
  if (StringUtils.isNotBlank(url)) {
   image.withUrl(url);
   instantArticle.addChild(image);
   if (!(container instanceof InstantArticle)) {
    instantArticle.addChild((Paragraph) container);
   }
  } else if (StringUtils.isNotBlank(lazyLoadUrl)) {
   image.withLazyLoadUrl(lazyLoadUrl);
   instantArticle.addChild(image);
   if (!(container instanceof InstantArticle)) {
    instantArticle.addChild((Paragraph) container);
   }
  }
  
  image = setImageProperties(node, image, imageTitle);
  
  transformer.transform(image, node);
  
  return container;
  
 }
 
 /**
  * Sets the image properties.
  *
  * @param node
  *         the node
  * @param image
  *         the image
  * @param imageTitle
  *         the image title
  * @return
  */
 private Image setImageProperties(Node node, Image image, String imageTitle) {
  if (StringUtils.isNotBlank(imageTitle)) {
   Caption caption = Caption.create();
   caption.withTitle(imageTitle);
   image.withCaption(caption);
  }
  
  if (StringUtils.isNotBlank(String.valueOf(getProperty(ASPECT_FIT, node)))) {
   image.withPresentation(String.valueOf(getProperty(ASPECT_FIT, node)));
  } else if (StringUtils.isNotBlank(String.valueOf(getProperty(ASPECT_FIT_ONLY, node)))) {
   image.withPresentation(String.valueOf(getProperty(ASPECT_FIT_ONLY, node)));
  } else if (StringUtils.isNotBlank(String.valueOf(getProperty(FULLSCREEN, node)))) {
   image.withPresentation(String.valueOf(getProperty(FULLSCREEN, node)));
  } else if (StringUtils.isNotBlank(String.valueOf(getProperty(NON_INTERACTIVE, node)))) {
   image.withPresentation((String) (getProperty(NON_INTERACTIVE, node)));
  }
  
  if (StringUtils.isNotBlank(String.valueOf(getProperty(PROPERTY_LIKE, node)))) {
   image.enableLike();
  }
  if (StringUtils.isNotBlank(String.valueOf(getProperty(PROPERTY_COMMENTS, node)))) {
   image.enableComments();
  }
  return image;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { InstantArticle.getClassName(), Paragraph.getClassName(), Anchor.getClassName(), ListItem.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the audio rule
  */
 public static ImageRule createFrom(JSONObject configuration) {
  ImageRule imageRule = create();
  imageRule = (ImageRule) imageRule.withSelector(configuration.getString("selector"));
  JSONObject anchorConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_IMAGE_URL);
  properties.add(PROPERTY_LAZY_LOAD_IMAGE_URL);
  properties.add(PROPERTY_LIKE);
  properties.add(PROPERTY_COMMENTS);
  properties.add(ASPECT_FIT);
  properties.add(ASPECT_FIT_ONLY);
  properties.add(FULLSCREEN);
  properties.add(NON_INTERACTIVE);
  properties.add(PROPERTY_IMAGE_TITLE);
  imageRule.withProperties(properties, anchorConfiguration);
  return imageRule;
 }
 
 /**
  * Load from.
  *
  * @param configuration
  *         the configuration
  */
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
