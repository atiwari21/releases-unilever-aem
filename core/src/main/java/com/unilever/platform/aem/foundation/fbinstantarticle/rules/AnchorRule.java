/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Anchor;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.InstantArticle;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.ListItem;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Paragraph;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.TextContainer;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class AnchorRule.
 */
public class AnchorRule extends ConfigurationSelectorRule {
 
 /** The Constant PROPERTY_ANCHOR_HREF. */
 public static final String PROPERTY_ANCHOR_HREF = "anchor.href";
 
 /** The Constant PROPERTY_ANCHOR_REL. */
 public static final String PROPERTY_ANCHOR_REL = "anchor.rel";
 
 /**
  * Instantiates a new anchor rule.
  */
 private AnchorRule() {
  
 }
 
 /**
  * Creates the.
  *
  * @return the anchor rule
  */
 public static AnchorRule create() {
  return new AnchorRule();
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see
  * com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#matchesContext(com.unilever.platform.aem.foundation.fbinstantarticle.Container )
  */
 @Override
 public boolean matchesContext(Container context) {
  return false;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.
  * Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Anchor anchor = Anchor.create();
  String url = this.getProperty(PROPERTY_ANCHOR_HREF, node) != null ? this.getProperty(PROPERTY_ANCHOR_HREF, node).toString() : null;
  String rel = this.getProperty(PROPERTY_ANCHOR_REL, node) != null ? this.getProperty(PROPERTY_ANCHOR_REL, node).toString() : null;
  if (StringUtils.isNotBlank(url)) {
   anchor.withHref(url);
  }
  if (StringUtils.isNotBlank(rel)) {
   anchor.withRel(rel);
  }
  
  if (container instanceof Paragraph) {
   ((Paragraph) container).addChild(anchor);
  } else if (container instanceof ListItem) {
   ((ListItem) container).addChild(anchor);
  }  else {
   transformer.getInstantArticle().addChild(anchor);
  }
  transformer.transform(anchor, node);
  
  Elements imgTags = ((Element) node).select("img");
  if (CollectionUtils.isNotEmpty(imgTags)) {
   Element imgTag = imgTags.get(imgTags.size() - 1);
   String anchorText = StringUtils.isNotBlank(imgTag.attr("title")) ? imgTag.attr("title") : imgTag.attr("alt");
   anchor.appendText(anchorText);
  }
  return container;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { TextContainer.getClassName(), Paragraph.getClassName(), InstantArticle.getClassName(), ListItem.getClassName() };
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the anchor rule
  */
 public static AnchorRule createFrom(JSONObject configuration) {
  AnchorRule anchorRule = create();
  anchorRule = (AnchorRule) anchorRule.withSelector(configuration.getString("selector"));
  JSONObject anchorConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_ANCHOR_HREF);
  properties.add(PROPERTY_ANCHOR_REL);
  anchorRule.withProperties(properties, anchorConfiguration);
  return anchorRule;
 }
 
 public void loadFrom(JSONObject configuration) {
  this.selector = configuration.getString("selector");
 }
}
