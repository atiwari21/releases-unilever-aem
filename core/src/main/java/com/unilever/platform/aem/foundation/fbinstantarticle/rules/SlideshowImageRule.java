/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.rules;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Node;

import com.unilever.platform.aem.foundation.fbinstantarticle.Container;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Caption;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Image;
import com.unilever.platform.aem.foundation.fbinstantarticle.elements.Slideshow;
import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class SlideshowImageRule.
 */
public class SlideshowImageRule extends ConfigurationSelectorRule {
 
 /** The Constant PROPERTY_IMAGE_URL. */
 private static final String PROPERTY_IMAGE_URL = "image.url";
 
 /** The Constant PROPERTY_LAZY_LOAD_IMAGE_URL. */
 private static final String PROPERTY_LAZY_LOAD_IMAGE_URL = "lazy.load.image.url";
 
 /** The Constant PROPERTY_CAPTION_TITLE. */
 private static final String PROPERTY_CAPTION_TITLE = "caption.title";
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#apply(com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer, com.unilever.platform.aem.foundation.fbinstantarticle.Container, org.jsoup.nodes.Node)
  */
 @Override
 public Container apply(Transformer transformer, Container container, Node node) {
  Image image = Image.create();
  
  String url = (String) getProperty(PROPERTY_IMAGE_URL, node);
  String lazyLoadUrl = (String) getProperty(PROPERTY_LAZY_LOAD_IMAGE_URL, node);
  String imageTitle = (String) getProperty(PROPERTY_CAPTION_TITLE, node);
  if (StringUtils.isNotBlank(url)) {
   image.withUrl(url);
   ((Slideshow) container).addImage(image);
  } else if (StringUtils.isNotBlank(lazyLoadUrl)) {
   image.withLazyLoadUrl(lazyLoadUrl);
   ((Slideshow) container).addImage(image);
  }
  if (StringUtils.isNotBlank(imageTitle)) {
   Caption caption = Caption.create();
   caption.withTitle(imageTitle);
   image.withCaption(caption);
  }
  
  return container;
 }
 
 /**
  * Creates the.
  *
  * @return the Slideshow rule
  */
 public static SlideshowImageRule create() {
  return new SlideshowImageRule();
 }
 
 /**
  * Creates the from.
  *
  * @param configuration
  *         the configuration
  * @return the Slideshow rule
  */
 public static SlideshowImageRule createFrom(JSONObject configuration) {
  SlideshowImageRule imageRule = create();
  imageRule = (SlideshowImageRule) imageRule.withSelector(configuration.getString("selector"));
  JSONObject anchorConfiguration = configuration.getJSONObject("properties");
  List<String> properties = new ArrayList<String>();
  properties.add(PROPERTY_IMAGE_URL);
  properties.add(PROPERTY_LAZY_LOAD_IMAGE_URL);
  properties.add(PROPERTY_CAPTION_TITLE);
  imageRule.withProperties(properties, anchorConfiguration);
  return imageRule;
 }
 
 /* (non-Javadoc)
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.rules.Rule#getContextClass()
  */
 @Override
 public String[] getContextClass() {
  return new String[] { Slideshow.getClassName() };
 }
 
}
