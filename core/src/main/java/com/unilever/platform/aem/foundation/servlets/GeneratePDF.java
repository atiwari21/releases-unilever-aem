/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.servlets;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.core.helper.URLConnectionHelper;
import com.unilever.platform.aem.foundation.core.service.EGiftingConnectionService;

/**
 * The Class GeneratePDF.
 */
@SlingServlet(paths = { GeneratePDF.PATH }, methods = { HttpConstants.METHOD_GET, HttpConstants.METHOD_POST })
@Properties(value = { @Property(name = Constants.SERVICE_DESCRIPTION, value = "Servlet to get the PDF from DCS", propertyPrivate = false),
  @Property(name = "sling.auth.requirements", value = "/system/sling/login")

})
public class GeneratePDF extends SlingAllMethodsServlet {
 
 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The Constant ERROR_WHILE_GENERATING_PDF. */
 private static final String ERROR_WHILE_GENERATING_PDF = "error while generating PDF: ";
 
 /** The Constant SELECTOR. */
 public static final String PATH = "/bin/gifting/generatepdf";
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(GeneratePDF.class);
 
 private static final String FILE_NAME = "orderConfirmation.pdf";
 
 /**
  * ConfigurationService service reference.
  */
 @Reference
 ConfigurationService configurationService;
 
 @Reference
 EGiftingConnectionService eGiftingConnectionService;
 
 @Reference
 HttpClientBuilderFactory factory;
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  doPost(slingRequest, slingResponse);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  ServletOutputStream sout = null;
  try {
   String url = eGiftingConnectionService.getGeneratePDFURL();
   LOGGER.debug("EGifting Transaction Service generate pdf URL: {}", url);
   int timeout = eGiftingConnectionService.getTimeoutTime();
   HttpResponse response = StringUtils.isNotBlank(url) ? URLConnectionHelper.getResponseFromPost(slingRequest, url, "order", factory, timeout) : null;
   if (response != null) {
    int responseCode = response.getStatusLine().getStatusCode();
    LOGGER.debug("Response Code : " + responseCode);
    HttpEntity responseEntity = response.getEntity();
    InputStream is = null;
    if (responseEntity != null) {
     is = responseEntity.getContent();
    }
    
    slingResponse.setContentType("application/pdf");
    slingResponse.addHeader("Content-Disposition", "attachment; filename=" + FILE_NAME + "");
    slingResponse.setStatus(responseCode);
    sout = slingResponse.getOutputStream();
    IOUtils.copy(is, sout);
   }
   
  } catch (Exception e) {
   LOGGER.error(ERROR_WHILE_GENERATING_PDF, e);
  } finally {
   if (sout != null) {
    sout.flush();
   }
  }
 }
 
}
