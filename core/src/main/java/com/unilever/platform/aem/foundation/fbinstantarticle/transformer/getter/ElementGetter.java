/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.unilever.platform.aem.foundation.fbinstantarticle.transformer.Transformer;

/**
 * The Class ElementGetter.
 */
public class ElementGetter extends AbstractGetter {
 
 /** The selector. */
 protected String selector;
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.transformer.getter.AbstractGetter#createFrom(org.json.JSONObject)
  */
 @Override
 public AbstractGetter createFrom(JSONObject configuration) {
  if (configuration.getString("selector") != null) {
   return this.withSelector(configuration.getString("selector"));
  }
  return this;
 }
 
 /**
  * Find all.
  *
  * @param node
  *         the node
  * @return the list
  */
 public Elements findAll(Node node, String selector) {
  return ((Element) node).select(selector);
 }
 
 /**
  * With selector.
  *
  * @param selector
  *         the selector
  * @return the element getter
  */
 public ElementGetter withSelector(String selector) {
  this.selector = selector;
  return this;
 }
 
 /**
  * Gets the.
  *
  * @param node
  *         the node
  * @return the element
  */
 public Object get(Node node) {
  List<Element> nodeList = findAll(node, this.selector);
  if (CollectionUtils.isNotEmpty(nodeList)) {
   Transformer.markAsProcessed(nodeList.get(0));
   return Transformer.cloneNode(node);
  }
  return null;
 }
 
}
