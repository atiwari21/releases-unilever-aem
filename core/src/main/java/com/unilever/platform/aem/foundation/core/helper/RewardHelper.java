package com.unilever.platform.aem.foundation.core.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.Product;
import com.day.cq.commons.ImageResource;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.unilever.platform.aem.foundation.commerce.api.UNIProduct;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.dam.DAMAssetUtility;
import com.unilever.platform.aem.foundation.utils.PIMInjestionUtility;

public final class RewardHelper {
 
 /** The Constant LOGGER. */
 private static final Logger LOGGER = LoggerFactory.getLogger(RewardHelper.class);
 
 /** The Constant Reward. */
 public static final String REWARD = "rewardConfg";
 
 private static final String TOTAL_COUNT = "totalCount";
 
 @Reference
 private static QueryBuilder builder;
 
 /** The configuration service. */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * @param rewardLocation
  *         the reward location
  * @param resourceResolver
  *         the resource resolver
  * @param session
  *         the session
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the reward list
  */
 public static List<Map<String, Object>> getRewardList(String rewardLocation, ResourceResolver resourceResolver, Session session, Page page,
   SlingHttpServletRequest slingRequest, int startIndex, int noOfRecords, Map<String, Object> rewardRedemptionMap) {
  List<Map<String, Object>> rewardList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
  
  List<UNIProduct> productList = getRewards(rewardLocation, startIndex, noOfRecords, resourceResolver, rewardRedemptionMap);
  
  for (UNIProduct uniProduct : productList) {
   Map<String, Object> map = getRewardMap(uniProduct, "product", page, slingRequest);
   List<Map<String, Object>> variantList = new ArrayList<Map<String, Object>>(CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
   try {
    Iterator<Product> iterator = uniProduct.getVariants();
    String productSkuId = uniProduct.getSKU();
    while (iterator.hasNext()) {
     UNIProduct productVariant = (UNIProduct) iterator.next();
     String productVariantSkuId = productVariant.getSKU();
     
     if (StringUtils.isNotBlank(productSkuId) && StringUtils.isNotBlank(productVariantSkuId) && !productSkuId.equals(productVariantSkuId)) {
      variantList.add(getRewardMap(productVariant, "variant", page, slingRequest));
     }
    }
   } catch (CommerceException ce) {
    LOGGER.error("CommerceException while getting the product variants:", ce);
   }
   
   map.put("variantList", variantList.toArray());
   rewardList.add(map);
  }
  return rewardList;
 }
 
 /**
  * The Constant LOGGER.
  * 
  * @param uniProduct
  *         the uniProduct
  * @param type
  *         the type
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the reward properties
  */
 
 public static Map<String, Object> getRewardMap(UNIProduct uniProduct, String type, Page page, SlingHttpServletRequest slingRequest) {
  /** The product properties. */
  Map<String, Object> rewardProperties = new LinkedHashMap<String, Object>();
  if (uniProduct != null) {
   rewardProperties.put("endDate", uniProduct.getRewardAvailableTo());
   rewardProperties.put("startDate", uniProduct.getRewardAvailableFrom());
   rewardProperties.put("rewardTitle", uniProduct.getTitle());
   rewardProperties.put("rewardId", uniProduct.getSKU());
   rewardProperties.put("points", uniProduct.getQuantity());
   rewardProperties.put("rewardDescription", uniProduct.getDescription());
   rewardProperties.put("rewardType", type);
   rewardProperties.put("image", getProductImagesList(uniProduct, page, slingRequest));
  }
  return rewardProperties;
 }
 
 public static Map<String, String> getQueryStringMap(String path) {
  Map<String, String> map = new HashMap<String, String>();
  
  map.put("path", path);
  map.put("type", JcrConstants.NT_UNSTRUCTURED);
  map.put("property", "cq:commerceType");
  map.put("property.value", "product");
  
  map.put("p.offset", "0");
  map.put("p.limit", "-1");
  
  return map;
 }
 
 public static List<UNIProduct> getRewards(String searchPath, int startIndex, int noOfRecords, ResourceResolver resolver,
   Map<String, Object> rewardRedemptionMap) {
  
  List<UNIProduct> productList = new ArrayList<UNIProduct>();
  Iterator<Resource> rsIterator = null;
  Map<String, String> map = RewardHelper.getQueryStringMap(searchPath);
  
  builder = PIMInjestionUtility.getServiceReference(QueryBuilder.class);
  Query query = builder.createQuery(PredicateGroup.create(map), resolver.adaptTo(Session.class));
  
  query.setStart(startIndex);
  query.setHitsPerPage(noOfRecords);
  
  SearchResult result = query.getResult();
  long totalSize = result.getTotalMatches();
  
  rewardRedemptionMap.put(TOTAL_COUNT, totalSize);
  
  rsIterator = result.getResources();
  
  while (rsIterator.hasNext()) {
   Product product = rsIterator.next().adaptTo(Product.class);
   UNIProduct uniProduct = PIMInjestionUtility.getUniProduct(product);
   if (uniProduct != null) {
    productList.add(uniProduct);
   }
  }
  
  return productList;
 }
 
 /**
  * The Constant LOGGER.
  * 
  * @param uniProduct
  *         the uniProduct
  * @param page
  *         the page
  * @param slingRequest
  *         the sling request
  * @return the image
  */
 
 public static Map<String, Object> getProductImagesList(Product uniProduct, Page page, SlingHttpServletRequest slingRequest) {
  ImageResource imageResource = uniProduct.getImage();
  Map<String, Object> image = new LinkedHashMap<String, Object>();
  if (imageResource != null && StringUtils.isNotBlank(imageResource.getFileReference())) {
   image = DAMAssetUtility.getImageMap(imageResource.getFileReference(), page, slingRequest);
  }
  return image;
 }
}
