/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.service;

import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.dam.api.Asset;

/**
 * The Interface DAMAssetSearchService.
 */
public interface DAMAssetSearchService {
 
 /**
  * Gets the asset.
  *
  * @param recordId
  *         the record id
  * @param searchPath
  *         the search path
  * @param resolver
  *         the resolver
  * @return the asset
  */
 Asset getAssetByRecordId(String recordId, String searchPath, ResourceResolver resolver);
 
 /**
  * Gets the assets by record id.
  *
  * @param recordId
  *         the record id
  * @param searchPath
  *         the search path
  * @param resolver
  *         the resolver
  * @return the assets by record id
  */
 List<Asset> getAssetsByRecordIds(String[] recordId, String searchPath, ResourceResolver resolver);
 
 /**
  * Gets the assets by hash tag.
  *
  * @param hashTag
  *         the hash tag
  * @param channelList
  *         the channel list
  * @param searchPath
  *         the search path
  * @param startIndex
  *         the start index
  * @param noOfRecords
  *         the no of records
  * @param resolver
  *         the resource resolver
  * @return the assets by hash tag
  */
 List<Asset> getAssetsByHashTag(String hashTag, String[] channelList, String searchPath, int startIndex, int noOfRecords, ResourceResolver resolver);
}
