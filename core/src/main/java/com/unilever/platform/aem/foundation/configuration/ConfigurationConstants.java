/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.configuration;

/**
 * The Class ConfigurationConstants.
 */
public final class ConfigurationConstants {
 
 /** The Constant CATEGORY_CONFIGURATION_NUMBER_PROPERTY_NAME. */
 public static final String CATEGORY_CONFIGURATION_NUMBER_PROPERTY_NAME = "categoryConfigurationNumber";
 
 /** The Constant CATEGORY_PROPERTY_NAME. */
 public static final String CATEGORY_PROPERTY_NAME = "category";
 
 /** The Constant CONF_CACHE_NAME. */
 public static final String CONF_CACHE_NAME = "confCacheName";
 
 /** The Constant CONFIG_KEY_SPLITTER. */
 public static final String CONFIG_KEY_SPLITTER = "/content/";
 
 /** The Constant CONFIGURATION_CACHE_NAME. */
 public static final String CONFIGURATION_CACHE_NAME = "confCacheName";
 
 /** The Constant CONFIGURATION_NUMBER_PROPERTY_NAME. */
 public static final String CONFIGURATION_NUMBER_PROPERTY_NAME = "configurationNumber";
 
 /** The Constant COUNTRY_PAGE_AND_UNI_PAGE_DIFF. */
 public static final int COUNTRY_PAGE_AND_UNI_PAGE_DIFF = 2;
 
 /** The Constant GLOBAL_CONFIG_PAGE_NAME. */
 public static final String GLOBAL_CONFIG_PAGE_NAME = "globalConfigPageName";
 
 /** The Constant KEY_PROPERTY_NAME. */
 public static final String KEY_PROPERTY_NAME = "key";
 
 /** The Constant LAST_CONFIG_PAGE_AND_UNI_DIFF. */
 public static final int LAST_CONFIG_PAGE_AND_UNI_DIFF = 3;
 
 /** The Constant MAX_RECORD_IN_CACHE. */
 public static final String MAX_RECORD_IN_CACHE = "maxRecordInConfCache";
 
 /** The Constant LAST_CONFIG_PAGE_AND_UNI_DIFF. */
 public static final int PAGE_AND_UNI_DIFF = 3;
 
 /** The Constant PAGE_NAME_POSITION_FROM_LAST_IN_CONFIG_KEY. */
 public static final int PAGE_NAME_POSITION_FROM_LAST_IN_CONFIG_KEY = 4;
 
 /** The Constant SITE_CONFIG_COMPONENT_NAME. */
 public static final String SITE_CONFIG_COMPONENT_NAME = "siteConfig";
 
 public static final String SITES_PATHS_CONFIG_PROPERTY_KEY = "sitesPaths";
 
 /** The Constant TTL_FOR_CONF_CACHE. */
 public static final String TTL_FOR_CONF_CACHE = "ttlInMin";
 
 /** The Constant TTL_FOR_CONF_CACHE. */
 public static final String TTL_FOR_CONF_CACHE_DAYS = "ttlInDays";
 
 /** The Constant UNILEVER_SITE_ROOT_PATH_CONFIG_PROPERTY_KEY. */
 public static final String UNILEVER_SITE_ROOT_PATH_CONFIG_PROPERTY_KEY = "unileverSiteRootPath";
 
 /** The Constant VALUE_PROPERTY_NAME. */
 public static final String VALUE_PROPERTY_NAME = "value";
 
 /**
  * Instantiates a new configuration constants.
  */
 private ConfigurationConstants() {
  
 }
 
}
