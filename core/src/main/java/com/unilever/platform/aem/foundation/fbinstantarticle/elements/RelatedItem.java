/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.fbinstantarticle.elements;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The Class RelatedItem.
 */
public class RelatedItem extends InstantArticleElement {
 
 /** The url. */
 private String url;
 
 /** The sponsored. */
 private boolean sponsored;
 
 /**
  * Instantiates a new related item.
  */
 private RelatedItem() {
 }
 
 /**
  * Gets the url.
  *
  * @return the url
  */
 public String getUrl() {
  return url;
 }
 
 /**
  * Checks if is sponsored.
  *
  * @return true, if is sponsored
  */
 public boolean isSponsored() {
  return sponsored;
 }
 
 /**
  * Creates the.
  *
  * @return the related item
  */
 public static RelatedItem create() {
  return new RelatedItem();
 }
 
 /**
  * With url.
  *
  * @param url
  *         the url
  * @return the related item
  */
 public RelatedItem withURL(String url) {
  this.url = url;
  return this;
 }
 
 /**
  * Enable sponsored.
  *
  * @return the related item
  */
 public RelatedItem enableSponsored() {
  this.sponsored = true;
  return this;
 }
 
 /**
  * Disable sponsored.
  *
  * @return the related item
  */
 public RelatedItem disableSponsored() {
  this.sponsored = false;
  return this;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement()
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement() {
  return toDOMElement(null);
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#toDOMElement(org.jsoup.nodes.Document)
  */
 @Override
 public org.jsoup.nodes.Element toDOMElement(Document document) {
  if (document == null) {
   document = Jsoup.parse("");
  }
  if (!this.isValid()) {
   return null;
  }
  org.jsoup.nodes.Element li = document.createElement("li");
  if (this.sponsored) {
   li.attr("data-sponsored", "" + true);
  }
  li.appendChild(Anchor.create().withHref(this.url).toDOMElement(document));
  return li;
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see com.unilever.platform.aem.foundation.fbinstantarticle.elements.Element#isValid()
  */
 public boolean isValid() {
  return StringUtils.isNotBlank(this.url) ? true : false;
 }
 
}
