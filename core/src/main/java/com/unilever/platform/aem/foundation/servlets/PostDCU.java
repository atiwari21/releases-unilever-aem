/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/

package com.unilever.platform.aem.foundation.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.unilever.platform.aem.foundation.configuration.ConfigurationNotFoundException;
import com.unilever.platform.aem.foundation.configuration.ConfigurationService;
import com.unilever.platform.aem.foundation.constants.CommonConstants;
import com.unilever.platform.aem.foundation.utils.TokenUtility;

/**
 * The Class UnileverImageRenditionsServlet.
 */
@SlingServlet(resourceTypes = CommonConstants.DEFAULT_SERVLET_NAME, selectors = { PostDCU.SELECTOR }, methods = { HttpConstants.METHOD_GET,
  HttpConstants.METHOD_POST })
@Properties(value = { @Property(name = Constants.SERVICE_DESCRIPTION, value = "Util to get DCS header token", propertyPrivate = false),
  @Property(name = "sling.auth.requirements", value = "/system/sling/login")

})
public class PostDCU extends SlingAllMethodsServlet {
 
 private static final String CONTACTUS = "contactus";
 
 private static final String ERROR_WHILE_GENERATING_DCU_TOKEN = "error while generating DCU token: ";
 
 private static final int HTTP_500 = 500;
 
 private static final int TWO_HUNDRED = 200;
 
 /**
  * serial version number.
  */
 private static final long serialVersionUID = 1L;
 
 /**
  * Constant for "ulecscale" selector.
  */
 public static final String SELECTOR = "dcspost";
 
 /**
  * ConfigurationService service reference.
  */
 @Reference
 ConfigurationService configurationService;
 
 /**
  * Identifier to hold value of logger.
  */
 private static final Logger LOGGER = LoggerFactory.getLogger(PostDCU.class);
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doGet(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  
  slingResponse.setStatus(HTTP_500);
  
 }
 
 /*
  * (non-Javadoc)
  * 
  * @see org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.sling.api.SlingHttpServletRequest,
  * org.apache.sling.api.SlingHttpServletResponse)
  */
 @Override
 protected void doPost(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) throws ServletException, IOException {
  
  Page page = slingRequest.getResource().adaptTo(Page.class);
  
  StringBuilder jb = new StringBuilder();
  String line = null;
  try {
   BufferedReader reader = slingRequest.getReader();
   while ((line = reader.readLine()) != null) {
    jb.append(line);
   }
  } catch (Exception e) {
   LOGGER.error("Error while reading request", e);
  }
  
  // Send post request
  String entity = CONTACTUS;
  JSONObject jsnobject = null;
  try {
   jsnobject = new JSONObject(jb.toString());
   entity = jsnobject.get("entity").toString();
   
  } catch (JSONException e) {
   LOGGER.error("Error while reading json", e);
  }
  if (!checkGoogeCaptcha(jsnobject, slingResponse)) {
   
   return;
   
  }
  
  String token = TokenUtility.gettoken(page, entity, configurationService);
  String url = "";
  try {
   url = configurationService.getConfigValue(page, entity, "url");
   
  } catch (ConfigurationNotFoundException e1) {
   LOGGER.error(ERROR_WHILE_GENERATING_DCU_TOKEN, e1);
  }
  
  CredentialsProvider credsProvider = new BasicCredentialsProvider();
  CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
  HttpPost httppost = new HttpPost(url);
  httppost.addHeader("Authorization", token);
  StringEntity params = new StringEntity(jb.toString());
  httppost.setEntity(params);
  HttpResponse response = httpclient.execute(httppost);
  
  int responseCode = response.getStatusLine().getStatusCode();
  LOGGER.debug("Post json : " + jb.toString());
  LOGGER.debug("Response Code : " + responseCode);
  HttpEntity responseEntity = response.getEntity();
  InputStream is = null;
  if (responseEntity != null) {
   is = responseEntity.getContent();
   
  }
  slingResponse.setStatus(responseCode);
  slingResponse.setContentType("application/json");
  BufferedReader in = new BufferedReader(new InputStreamReader(is));
  String inputLine;
  StringBuilder response1 = new StringBuilder();
  
  ServletOutputStream sout = slingResponse.getOutputStream();
  
  while ((inputLine = in.readLine()) != null) {
   response1.append(inputLine);
   sout.write(inputLine.getBytes());
  }
  in.close();
  
  sout.flush();
  
 }
 
 private boolean checkGoogeCaptcha(JSONObject jsnobject, SlingHttpServletResponse slingResponse) {
  if (jsnobject == null) {
   setErrorResponse(slingResponse);
  }
  try {
   String capcha = jsnobject.getJSONObject("g").getJSONObject("recaptcha").get("response").toString();
   if ("perfskip1234134".equalsIgnoreCase(capcha)) {
    return true;
   }
   if (VerifyRecaptcha.verify(capcha)) {
    return true;
   } else {
    setErrorResponse(slingResponse);
   }
  } catch (Exception e) {
   LOGGER.error("Error while posting data to DCS app {}", e.getMessage(), e);
   setErrorResponse(slingResponse);
  }
  return false;
 }
 
 private void setErrorResponse(SlingHttpServletResponse slingResponse) {
  slingResponse.setStatus(TWO_HUNDRED);
  ServletOutputStream sout;
  try {
   sout = slingResponse.getOutputStream();
   
   sout.write(
     "{ \"code\": 867, \"status\": \"Unprocessable Entity\",  \"errors\": [  {     \"code\": 40,    \"error\": \"Please enter valid captcha code\"   }  ],  \"responseData\": null}"
       .getBytes());
   sout.flush();
  } catch (Exception e) {
   slingResponse.setStatus(HTTP_500);
   LOGGER.error("Error while posting data to DCS app {}", e.getMessage(), e);
   
  }
 }
 
}
