/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.core.helper;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * The Class InheritenceHelper.
 */
public class InheritenceHelper {
 
 /**
  * Instantiates a new inheritence helper.
  */
 InheritenceHelper() {
 }
 
 /**
  * Gets the inheritance.
  *
  * @param in
  *         the in
  * @return the inheritance
  */
 public static Set<Class<?>> getInheritance(Class<?> in) {
  Set<Class<?>> result = new LinkedHashSet<Class<?>>();
  result.add(in);
  getInheritance(in, result);
  
  return result;
 }
 
 /**
  * Get inheritance of type.
  *
  * @param in
  *         the in
  * @param result
  *         the result
  * @return the inheritance
  */
 private static void getInheritance(Class<?> in, Set<Class<?>> result) {
  Class<?> superclass = getSuperclass(in);
  
  if (superclass != null) {
   result.add(superclass);
   getInheritance(superclass, result);
  }
  
  getInterfaceInheritance(in, result);
 }
 
 /**
  * Get interfaces that the type inherits from.
  *
  * @param in
  *         the in
  * @param result
  *         the result
  * @return the interface inheritance
  */
 private static void getInterfaceInheritance(Class<?> in, Set<Class<?>> result) {
  for (Class<?> c : in.getInterfaces()) {
   result.add(c);
   
   getInterfaceInheritance(c, result);
  }
 }
 
 /**
  * Get superclass of class.
  *
  * @param in
  *         the in
  * @return the superclass
  */
 private static Class<?> getSuperclass(Class<?> in) {
  if (in == null) {
   return null;
  }
  
  if (in.isArray() && in != Object[].class) {
   Class<?> type = in.getComponentType();
   
   while (type.isArray()) {
    type = type.getComponentType();
   }
   
   return type;
  }
  
  return in.getSuperclass();
 }
}
