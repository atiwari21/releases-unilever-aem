/*******************************************************************************
 * Copyright (c) 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.aem.foundation.servlets;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Base64;

/**
 * The Class FileUploadHelperServlet will be called with file object and image sizelimit request parameter. Servlet will first check for image size
 * and if it is under provided limit,a base64 string will be generated and sent in json to front end
 */
@SlingServlet(label = "Unilver File - file upload Servlet", methods = { org.apache.sling.api.servlets.HttpConstants.METHOD_POST,
  org.apache.sling.api.servlets.HttpConstants.METHOD_GET }, paths = { FileUploadHelperServlet.SERVLET_PATH })
public class FileUploadHelperServlet extends SlingAllMethodsServlet {
 
 /** The Constant LOG. */
 private static final Logger LOG = LoggerFactory.getLogger(FileUploadHelperServlet.class);
 
 private static final long serialVersionUID = -7750338588642660778L;
 public static final String SERVLET_PATH = "/bin/file/upload";
 public static final String SIZELIMIT = "sizelimit";
 public static final String UPLOADFILE = "UploadFile";
 public static final String SUCCESS = "success";
 public static final String FAIL = "fail";
 public static final String KB = "kb";
 public static final String STATUS = "status";
 public static final String NAME = "name";
 public static final String SIZE = "size";
 public static final String ENCODEDSTRING = "encodedString";
 public static final String MESSAGE = "message";
 /** The Constant 1024 */
 private static final int SIZE_BYTES = 1024;
 
 @Override
 protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
  LOG.info("inside file upload servlet");
  Map<String, Object> campaignImageMap = new HashMap<String, Object>();
  int maxSize = 0;
  long size = 0;
  InputStream stream = null;
  String fileName = "";
  String encode = StringUtils.EMPTY;
  response.setContentType("text/html");
  final Map<String, RequestParameter[]> params = request.getRequestParameterMap();
  for (final Map.Entry<String, RequestParameter[]> pairs : params.entrySet()) {
   
   final RequestParameter[] pArr = pairs.getValue();
   final RequestParameter param = pArr[0];
   maxSize = (Integer.parseInt(request.getParameter(SIZELIMIT))) * SIZE_BYTES;
   if (!param.isFormField()) {
    size = (param.getSize() / SIZE_BYTES);
    stream = param.getInputStream();
    fileName = param.getFileName();
   }
  }
  if (findSize(size, maxSize)) {
   encode = findBase64(fileName, stream);
   if (!StringUtils.isEmpty(encode)) {
    campaignImageMap.put(STATUS, SUCCESS);
    campaignImageMap.put(NAME, fileName);
    campaignImageMap.put(SIZE, size + KB);
    campaignImageMap.put(ENCODEDSTRING, encode);
    campaignImageMap.put(MESSAGE, "Image Encoded successfully");
    LOG.info("Successfully generated base64 string");
   } else {
    campaignImageMap.put(STATUS, FAIL);
    campaignImageMap.put(MESSAGE, "There is a problem with encoding");
    LOG.info("There is a problem with encoding");
   }
  } else {
   campaignImageMap.put(STATUS, FAIL);
   campaignImageMap.put(MESSAGE, "Image size is bigger");
   campaignImageMap.put(NAME, fileName);
   campaignImageMap.put(SIZE, size + KB);
   LOG.info("Uploaded image is bigger");
  }
  
  JSONObject obj = new JSONObject(campaignImageMap);
  response.getWriter().write(obj.toString());
  LOG.info("exiting file upload servlet");
 }
 
 /**
  * Calculates size of the file.
  *
  * @param size
  *         the size
  * @param maxMemSize
  *         the max mem size
  * @return true, if successful
  */
 private boolean findSize(long size, int maxMemSize) {
  boolean returnval = false;
  if (size > maxMemSize) {
   return returnval;
  } else {
   returnval = true;
   return returnval;
  }
 }
 
 /**
  * Encodes the file into Base 64.
  *
  * @param param
  *         the param
  * @return the string
  */
 protected String findBase64(String fileName, InputStream stream) {
  BufferedImage img = null;
  String encode = "";
  try {
   if (null != stream) {
    img = ImageIO.read(stream);
    String extenstion = StringUtils.substringAfter(fileName, ".");
    encode = encodeToString(img, extenstion);
   }
  } catch (IOException e) {
   LOG.error("Exception in reading the image stream .............." + ExceptionUtils.getStackTrace(e));
  }
  
  return encode;
 }
 
 /**
  * Encode to string.
  *
  * @param image
  *         the image
  * @param type
  *         the type
  * @return the string
  */
 public static String encodeToString(BufferedImage image, String type) {
  String imageString = null;
  ByteArrayOutputStream bos = new ByteArrayOutputStream();
  try {
   ImageIO.write(image, type, bos);
   byte[] imageBytes = bos.toByteArray();
   imageString = Base64.getEncoder().encodeToString(imageBytes);
   LOG.debug("Image has been encoded as  .............." + imageString);
   bos.close();
  } catch (IOException e) {
   LOG.error("Exception in encoding the string .............." + ExceptionUtils.getStackTrace(e));
  }
  return imageString;
 }
 
}
