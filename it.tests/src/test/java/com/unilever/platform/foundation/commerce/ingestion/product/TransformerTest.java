/*******************************************************************************
 * Copyright (c) Sep 3, 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion.product;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.unilever.platform.foundation.commerce.dto.ResultReport;

/**
 * The Class TransformerTest test the JsonArray against the jsonSchema.
 * 
 */
public class TransformerTest {
	
	/** The transformer. */
	private static Transformer transformer;
	
	/** The json obj. */
	private static JSONArray jsonObj;
	
	/** The classloader. */
	private static ClassLoader classloader;
	
	/**
	 * Initialization of the transformer.
	 */
	@BeforeClass
	public static void initTransformer() {
		transformer = new Transformer();
		classloader = Thread.currentThread().getContextClassLoader();
		jsonObj = new JSONArray(getStringFromInputStream(classloader.getResourceAsStream("SampleJson.txt")));
	}
	
	/**
	 * Test transformer.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testTransformer() throws IOException {
		ResultReport report = new ResultReport();
		report = transformer.validateProducts(jsonObj);
		assertEquals("Error report is blank.", report.getErrorReport().isEmpty(), true);
	}
	
	/**
	 * Gets the string from input stream.
	 *
	 * @param is
	 *            the is
	 * @return the string from input stream
	 */
	private static String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}
}
