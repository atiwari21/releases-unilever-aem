/*******************************************************************************
 * Copyright (c) Sep 3, 2015 Unilever.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Unilever.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.unilever.platform.foundation.commerce.ingestion.product;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * The Class CSVExtractorTest.
 *
 * @author atiw22
 */
public class CSVExtractorTest {
	
	/** The extractor. */
	private static CSVExtractor extractor;
	
	/** The props. */
	private static Map<String, String> props = new LinkedHashMap<String, String>();
	
	/** The default column mappings. */
	private static String[] defaultColumnMappings = { "Type=type", "Action=action", "ProductID=productID", "Gtin14=gtin14", "Sku=sku", "Name=name",
			"Category=category", "Subcategory=subcategory", "Title=title", "Description=description", "ProductionDate=productionDate",
			"ReleaseDate=releaseDate", "Size=size", "Offer=offer", "Image=images", "Brand=brand", "Enhancedcopy=enhancedcopy",
			"Ingredient=ingredients", "Quote=quote" };
			
	/**
	 * Inits the extractor.
	 */
	@BeforeClass
	public static void initExtractor() {
		extractor = new CSVExtractor();
		if (defaultColumnMappings != null) {
			for (String s : defaultColumnMappings) {
				String[] prop = s.split("=");
				props.put(prop[0], prop[1]);
			}
		}
		extractor.setProps(props);
	}
	
	/**
	 * Test extractor.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testExtractor() throws IOException {
		
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("SampleCSV.csv");
		JSONArray json = extractor.extractData(is, null);
		
		JSONArray jsonObj = new JSONArray(getStringFromInputStream(classloader.getResourceAsStream("SampleJson.txt")));
		
		compareJsonObjects(json.getJSONObject(0), jsonObj.getJSONObject(0));
	}
	
	/**
	 * Compare json objects.
	 *
	 * @param actualJsonObj
	 *            the actual json obj
	 * @param expectedJsonObj
	 *            the expected json obj
	 */
	private void compareJsonObjects(JSONObject actualJsonObj, JSONObject expectedJsonObj) {
		
		Iterator<?> keys = expectedJsonObj.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (expectedJsonObj.get(key) instanceof JSONObject) {
				
				JSONObject eJsonObj = expectedJsonObj.getJSONObject(key);
				JSONObject aJsonObj = actualJsonObj.getJSONObject(key);
				
				compareJsonObjects(aJsonObj, eJsonObj);
				
			} else if (expectedJsonObj.get(key) instanceof JSONArray) {
				
				JSONArray eJsonArr = expectedJsonObj.getJSONArray(key);
				JSONArray aJsonArr = actualJsonObj.getJSONArray(key);
				if (eJsonArr.length() > 0) {
					for (int i = 0; i < eJsonArr.length(); i++) {
						compareJsonObjects(eJsonArr.getJSONObject(i), aJsonArr.getJSONObject(i));
					}
				}
			} else if (expectedJsonObj.get(key) instanceof String) {
				
				String eValue = expectedJsonObj.getString(key);
				String aValue = actualJsonObj.getString(key);
				assertEquals("Expected json and obtained json are not equal.", eValue, aValue);
			}
		}
	}
	
	/**
	 * Gets the string from input stream.
	 *
	 * @param is
	 *            the is
	 * @return the string from input stream
	 */
	private static String getStringFromInputStream(InputStream is) {
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		
		String line;
		try {
			
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return sb.toString();
		
	}
}
